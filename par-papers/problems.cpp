// https://www2.eecs.berkeley.edu/Pubs/TechRpts/2006/EECS-2006-1.pdf
// What are the problems with threads ?
// 
// IDEA
//
// 1. Threads are a seemingly straightfoward adaptation of the dominanant sequential model of computation.
// 2. Languages require little or no syntactic changes to support threads, and operating systems
// 3. Architectures have evolved to efficiently support them.
// 4. Many technologists are pushing for increased use of multithreading in software in order
//    to take advantage of the predicted increases in parallelism in computer architectures.
//
// ARGUMENT : IT IS NOT A GOOD IDEA
// 
// 1. Although, Threads seem to be a small step from sequential computation
//    In fact they are, they represent a huge step.
//    They discard the most essential and appealing properties of sequential computation
//    - understantability
//    - predictability
//    - determinism
//
// 2. Threads, as model of compution,
//    - are wildly nondeterministic
//    - the job of the programer becomes one of pruning that nondeterminism.
//
// 3. Although many research techniques improve the model by offering more
//    effective pruning.
//    - ARGUMENT
//    - This is approaching the problem backwards
//    - Rather than pruning non-determinism,
//      we should build from essential determinitic, 
//      composaple components
//
// 4. PRINCIPLE
//    Nondeterminism should be explicitly and judiciously introduced
//    where needed, rather than removed where not needed.
//    - The consequences of this principal are PROFOUND
//    - ARGUMENT
//    - For the development of concurrent coordination languages based on sound
//      composable formalisms.
//    - SUCH languages will yield much 
//    - more reliable
//    - more concurrent programs
