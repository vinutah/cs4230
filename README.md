# CS4230 Parallel Programming

## par-tools

### Data Races

* A ***prime number*** is a natural number that has exactly 2 distinct natural number
  divisors, itself and 1.
* Those numbers that do not satisfy this are ***crime numbers*** or Criminals
* The ***sieve*** of Eratosthenes is a simple, ancient algorithm for getting rid
  of Criminals, by separating or sieveing aways the crime from the prime.

* ***Data races*** are one of the most common and hardest to debug types of bugs in 
  concurrent systems. 
* A data race occurs when two threads access the same variable concurrently and 
  ***at least one of the accesses is write***.

#### Tool-1 ThreadSanitizer

* ***ThreadSanitizer*** (aka TSan) is a data race detector for C/C++. 
* If you want to use TSan for this Class, You can use this framework as follows

* ***Correct Path*** Working Directory
    * ```cd tsan```
    * Place your c code into ```src``` directory

* ***Compile*** with tsan
    * ```make clean all```

* ***Run*** Now we are ready to Thread Sanitize your code
    * ```make tsan SRC=simple_race THREADS=2```
    * ```make tsan SRC=bPc THREADS=4```
    * ```make tsan SRC=iP THREADS=6```
    * ```make tsan SRC=iPnl THREADS=8```

#### Tool-2 archer

* ***Archer*** is a data race detector for OpenMP programs.
* Combines static and dynamic techniques to identify data races in large OpenMP applications, 
* leading to low runtime and memory overheads, while still offering high accuracy and precision. 
* To Provide portability, It builds on open-source tools infrastructure such as 
    * LLVM, 
    * ThreadSanitizer, 
    * OMPT



### PIPE

PIPE is an open source, 
* platform independent tool for 
  * creating, 
  * simulating and 
  * analysing 
* Petri nets including 
  * Generalised Stochastic Petri nets. 
  * Petri nets are a popular way for modelling 
    * concurrency and 
    * synchronisation in distributed systems and 
    * to learn more about Petri nets, 
    * you can start by reading the 2006/7 MSc. project report available
      From [here](https://github.com/sarahtattersall/PIPE)

#### Download

* [Link](https://github.com/sarahtattersall/PIPE/releases)

#### Run

* ```java -jar PIPE-gui-5.0.2.jar``` 

## par-lang

### GO Programming Language

#### INSTALL
sudo apt install golang

#### WATCH
https://www.youtube.com/watch?v=CF9S4QZuV30

#### PRACTICE
http://www.newthinktank.com/2015/02/go-programming-tutorial/

#### READ
TextBook in this Repository, is considered the best

#### Quick Start

```go run hello.go```

```
Hello World
1
40   1.61803398875
0.0010000000000000009
6 + 4 = 10
6 - 4 = 2
6 * 4 = 24
6 / 4 = 1
6 % 4 = 2
```
