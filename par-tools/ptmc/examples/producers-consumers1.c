#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>
#include <assert.h>

const int N = 10;
pthread_t pthp, pthc;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cv1 = PTHREAD_COND_INITIALIZER;
pthread_cond_t cv2 = PTHREAD_COND_INITIALIZER;
int count;

void *producer(void *arg)
{
	while (true) {
		pthread_mutex_lock(&mutex);
		while (count == N) {
			pthread_cond_wait(&cv1, &mutex);
		}
		count++;
		pthread_cond_signal(&cv2);
		pthread_mutex_unlock(&mutex);
	}
	return NULL;
}

void *consumer(void *arg)
{
	while (true) {
		pthread_mutex_lock(&mutex);
		while (count == 0) {
			pthread_cond_wait(&cv2, &mutex);
		}
		count--;
		pthread_cond_signal(&cv1);
		pthread_mutex_unlock(&mutex);
	}
	return NULL;
}

int main(void)
{
	void *r1, *r2;
	pthread_create(&pthp, NULL, &producer, NULL);
	pthread_create(&pthc, NULL, &consumer, NULL);
	pthread_join(pthp, &r1);
	pthread_join(pthc, &r2);
	return 0;
}
