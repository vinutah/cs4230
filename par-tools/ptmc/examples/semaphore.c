#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>
#include <assert.h>

#define M 2
#define N 4

pthread_t pth[N];
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t m2 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cv = PTHREAD_COND_INITIALIZER;
int count;
int n;

// @ASSERT(n <= M)

void xsem_wait(void)
{
	pthread_mutex_lock(&m);
    while (count == 0) {
		pthread_cond_wait(&cv, &m);
	}
	count--;
	pthread_mutex_unlock(&m);
}

void xsem_signal(void)
{
	pthread_mutex_lock(&m);
	count++;
    pthread_cond_signal(&cv);
	pthread_mutex_unlock(&m);
}

void *thread(void *arg)
{
    xsem_wait();

	pthread_mutex_lock(&m2);
    n++;
	pthread_mutex_unlock(&m2);

	pthread_mutex_lock(&m2);
    n--;
	pthread_mutex_unlock(&m2);

    xsem_signal();

	return NULL;
}

int main(void)
{
    count = M;

    for (int i = 0; i < N; ++i) {
        pthread_create(&pth[i], NULL, &thread, NULL);
    }

    for (int i = 0; i < N; ++i) {
        pthread_join(pth[i], NULL);
    }

	return 0;
}
