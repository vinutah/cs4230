#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>
#include <assert.h>

#define N   1
#define NP  1
#define NC  2

pthread_t pthp[NP];
pthread_t pthc[NC];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cv = PTHREAD_COND_INITIALIZER;
int count;

void *producer(void *arg)
{
	int id = (int)arg;
	while (true) {
		pthread_mutex_lock(&mutex);
		while (count == N) {
			printf("producer %d wait\n", id);
			pthread_cond_wait(&cv, &mutex);
			printf("producer %d up\n", id);
		}
		count++;
		printf("producer %d %d\n", id, count);
		pthread_cond_broadcast(&cv);
		pthread_mutex_unlock(&mutex);
	}
	return NULL;
}

void *consumer(void *arg)
{
	int id = (int)arg;
	while (true) {
		pthread_mutex_lock(&mutex);
		while (count == 0) {
			printf("consumer %d wait\n", id);
			pthread_cond_wait(&cv, &mutex);
			printf("consumer %d up\n", id);
		}
		count--;
		printf("consumer %d %d\n", id, count);
		pthread_cond_signal(&cv);
		pthread_mutex_unlock(&mutex);
	}
	return NULL;
}

int main(void)
{
    int i;

    for (i = 0; i < NP; ++i) {
        pthread_create(&pthp[i], NULL, &producer, (void *)(intptr_t)i);
    }

    for (i = 0; i < NC; ++i) {
        pthread_create(&pthc[i], NULL, &consumer, (void *)(intptr_t)i);
    }

    for (i = 0; i < NP; ++i) {
        pthread_join(pthp[i], NULL);
    }

    for (i = 0; i < NC; ++i) {
        pthread_join(pthc[i], NULL);
    }

	return 0;
}
