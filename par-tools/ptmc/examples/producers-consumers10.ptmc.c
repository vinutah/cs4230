#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <limits.h>
#include <alloca.h>

typedef uint32_t bits_t;
typedef uint64_t bitvec_t;
typedef uint32_t id_t;
typedef uint32_t trans_t;
typedef struct context *context_t;

#define BV1         UINT64_C(1)
#define ID_NIL      UINT32_MAX
#define SID_OMEGA   0
#define PRIid       PRIu32
#define PRItrans    PRIx32

enum {
    TRANS_NIL,                  /* 0 */
    TRANS_ASSERT,               /* 1 */
    TRANS_TAU,                  /* 2 */
    TRANS_LOAD,                 /* 3 */
    TRANS_STORE,                /* 4 */
    TRANS_EXIT,                 /* 5 */
    TRANS_CREATE,               /* 6 */
    TRANS_JOIN,                 /* 7 */
    TRANS_LOCK,                 /* 8 */
    TRANS_UNLOCK,               /* 9 */
    TRANS_WAIT,                 /* 10 */
    TRANS_SIGNAL,               /* 11 */
    TRANS_BROADCAST,            /* 12 */
    TRANS_SPURIOUS,             /* 1 */
};

typedef struct thst thst_t;
struct thst {
  id_t sid;
  id_t cid;
  const void *lp;
};

typedef struct varinfo varinfo_t;
struct varinfo {
  int size;
  int dim;
  const char *name;
};

typedef struct local_varinfo local_varinfo_t;
struct local_varinfo {
  int n;
  const varinfo_t *varinfo;
};

typedef void (*pf_gtor_t)(context_t, bitvec_t, const void *, const thst_t [], int, int,
                          const id_t mtxv[]);

void assertion_violation(context_t, uint16_t);
void error_mutex_not_owner(context_t cc, int k, id_t mutex_id);

uint64_t *alloc_state(context_t);
void register_state(context_t, void *, trans_t, uint16_t);

uint64_t *enc_bv(uint64_t *sv, bitvec_t bv);
bits_t enc_thst(uint64_t *sv, bits_t bp, id_t sid, id_t cid);
bits_t enc_thst_v(uint64_t *sv, bits_t bp, const thst_t v[], int i, int j);
bits_t enc_thst_update(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, id_t cid);
bits_t enc_thst_create(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, int p, id_t sid0);
bits_t enc_thst_join(uint64_t *sv, bits_t bp,
					 const thst_t v[], int n, int k, id_t sid, int p);
bits_t enc_thst_signal(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, int p);
bits_t enc_thst_broadcast(uint64_t *sv, bits_t bp,
						  const thst_t v[], int n, int k, id_t sid, id_t cid);
bits_t enc_mtx(uint64_t *sv, bits_t bp, const id_t mtxv[]);
bits_t enc_mtx_update(uint64_t *sv, bits_t bp, const id_t mtxv[], id_t mid, id_t tid);
void *enc_align(uint64_t *sv, bits_t bp);
void *enc_genv(void *env, const void *gp);
void *enc_var(void *env, const void *p, unsigned size);
void *enc_val(void *env, uint64_t x, unsigned size);
void *enc_lenv(void *env, const thst_t v[], int i, int j);

static inline trans_t enc_trans(int32_t who, int32_t tag, int32_t arg0, int32_t arg1)
{
    return who | (tag << 8) | (arg0 << 16) | (arg1 << 24);
}
int ver_maj = 1;
int ver_min = 4;
int ver_mnt = 1;
const char model_filename[] = "producers-consumers10.c";

unsigned num_tid = 4;
unsigned num_sid = 71;
unsigned num_mid = 1;
unsigned num_cid = 2;
bits_t bits_tid = 2;
bits_t bits_tidx = 3;
bits_t bits_sid = 7;
bits_t bits_mid = 0;
bits_t bits_cid = 1;
unsigned size_global_env = 10;
unsigned size_local_env[] = {
1,
0,
4,
4,
0,
4,
4,
4,
0,
4,
4,
4,
0,
4,
1,
1,
0,
1,
2,
1,
2,
2,
1,
2,
1,
1,
2,
1,
1,
1,
4,
4,
0,
4,
8,
10,
8,
0,
0,
1,
1,
2,
1,
2,
2,
1,
2,
1,
1,
2,
1,
1,
1,
1,
1,
1,
2,
1,
2,
2,
2,
3,
2,
2,
3,
2,
2,
2,
2,
0,
5,
};
void gtor_70(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int16_t t35 = *(int16_t *)(lp + 0);
int8_t x15 = *(int8_t *)(lp + 2);
uint8_t t34 = *(uint8_t *)(lp + 3);
int8_t x14 = *(int8_t *)(lp + 4);
{
int16_t t41 = ((t35) + (x15));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 54, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
int i = t34;
*((int16_t *)(gp2 + 6) + i) = t41;
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x14, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 6 + i, 0), 60);
}
}

void gtor_69(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, SID_OMEGA, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_EXIT, 0, 0), 62);
}
}

void gtor_68(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x15 = *(int8_t *)(lp + 0);
int8_t x14 = *(int8_t *)(lp + 1);
uint8_t t34 = x14;
{
int i = t34;
int16_t t35 = *((int16_t *)(gp + 6) + i);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 70, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t35, 2);
r = enc_var(r, &x15, 1);
r = enc_var(r, &t34, 1);
r = enc_var(r, &x14, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 6 + i, 0), 60);
}
}

void gtor_67(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x15 = *(int8_t *)(lp + 0);
int8_t x14 = *(int8_t *)(lp + 1);
if ((! (((x15) == (INT32_C(-1)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 68, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x15, 1);
r = enc_var(r, &x14, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 57);
}
if (((x15) == (INT32_C(-1)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 69, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 57);
}
}

void gtor_66(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x14 = *(int8_t *)(lp + 0);
int8_t x15 = *(int8_t *)(lp + 1);
{
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 67, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x15, 1);
r = enc_var(r, &x14, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 0, 0), 56);
}
}

void gtor_65(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x15 = *(int8_t *)(lp + 0);
int8_t x14 = *(int8_t *)(lp + 1);
{
id_t cond_id = 0;
int c = 0;
for (int i = 0; i < n; ++i) {
if (v[i].cid == cond_id) {
c++;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_signal(sv, bp, v, n, k, 66, i);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x14, 1);
r = enc_var(r, &x15, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_SIGNAL, 0, 0), 55);
}
}
if (c == 0) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 66, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x14, 1);
r = enc_var(r, &x15, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_SIGNAL, 0, 0), 55);
}
}
}

void gtor_64(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t33 = *(int8_t *)(lp + 0);
int8_t x14 = *(int8_t *)(lp + 1);
int8_t x15 = *(int8_t *)(lp + 2);
{
int8_t t42 = ((t33) - (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 65, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 0) = t42;
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x15, 1);
r = enc_var(r, &x14, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 0, 0), 54);
}
}

void gtor_63(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x15 = *(int8_t *)(lp + 0);
int8_t x14 = *(int8_t *)(lp + 1);
{
int8_t t33 = *(int8_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 64, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t33, 1);
r = enc_var(r, &x14, 1);
r = enc_var(r, &x15, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 54);
}
}

void gtor_62(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x14 = *(int8_t *)(lp + 0);
int8_t x15 = *(int8_t *)(lp + 1);
{
int8_t t43 = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 63, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 2) = t43;
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x15, 1);
r = enc_var(r, &x14, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 2, 0), 53);
}
}

void gtor_61(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t32 = *(int8_t *)(lp + 0);
int8_t x14 = *(int8_t *)(lp + 1);
int8_t x15 = *(int8_t *)(lp + 2);
if ((! (((t32) == (INT32_C(3)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 63, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x15, 1);
r = enc_var(r, &x14, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 52);
}
if (((t32) == (INT32_C(3)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 62, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x14, 1);
r = enc_var(r, &x15, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 52);
}
}

void gtor_60(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x14 = *(int8_t *)(lp + 0);
int8_t t31 = *(int8_t *)(lp + 1);
int8_t x15 = t31;
{
int8_t t32 = *(int8_t *)(gp + 2);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 61, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t32, 1);
r = enc_var(r, &x14, 1);
r = enc_var(r, &x15, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 2, 0), 52);
}
}

void gtor_59(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t30 = *(int8_t *)(lp + 0);
int8_t x14 = *(int8_t *)(lp + 1);
{
int i = t30;
int8_t t31 = *((int8_t *)(gp + 3) + i);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 60, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x14, 1);
r = enc_var(r, &t31, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 3 + i, 0), 51);
}
}

void gtor_58(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t30 = *(int8_t *)(lp + 0);
int8_t x14 = *(int8_t *)(lp + 1);
{
int8_t t44 = ((t30) + (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 59, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 2) = t44;
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t30, 1);
r = enc_var(r, &x14, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 2, 0), 51);
}
}

void gtor_57(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x14 = *(int8_t *)(lp + 0);
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 55, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x14, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 49);
}
}
}

void gtor_56(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t29 = *(int8_t *)(lp + 0);
int8_t x14 = *(int8_t *)(lp + 1);
if (((t29) == (INT32_C(0)))) {
id_t cond_id = 1;
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 57, cond_id);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x14, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_WAIT, 1, 0), 48);
}
if ((! (((t29) == (INT32_C(0)))))) {
int8_t t30 = *(int8_t *)(gp + 2);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 58, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t30, 1);
r = enc_var(r, &x14, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 2, 0), 48);
}
}

void gtor_55(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x14 = *(int8_t *)(lp + 0);
{
int8_t t29 = *(int8_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 56, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t29, 1);
r = enc_var(r, &x14, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 48);
}
}

void gtor_54(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x14 = *(int8_t *)(lp + 0);
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 55, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x14, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 47);
}
}
}

void gtor_53(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t a13 = *(int8_t *)(lp + 0);
int8_t x14 = a13;
int8_t x15 = 0;
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 54, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x14, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 46);
}
}

void gtor_52(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x17 = *(int8_t *)(lp + 0);
x17 = ((x17) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 39, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 27);
}
}

void gtor_51(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x17 = *(int8_t *)(lp + 0);
{
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 52, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 0, 0), 37);
}
}

void gtor_50(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x17 = *(int8_t *)(lp + 0);
{
id_t cond_id = 1;
int c = 0;
for (int i = 0; i < n; ++i) {
if (v[i].cid == cond_id) {
c++;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_signal(sv, bp, v, n, k, 51, i);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_SIGNAL, 1, 0), 36);
}
}
if (c == 0) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 51, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_SIGNAL, 1, 0), 36);
}
}
}

void gtor_49(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t40 = *(int8_t *)(lp + 0);
int8_t x17 = *(int8_t *)(lp + 1);
{
int8_t t45 = ((t40) + (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 50, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 0) = t45;
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 0, 0), 35);
}
}

void gtor_48(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x17 = *(int8_t *)(lp + 0);
{
int8_t t40 = *(int8_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 49, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t40, 1);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 35);
}
}

void gtor_47(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x17 = *(int8_t *)(lp + 0);
{
int8_t t46 = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 48, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 1) = t46;
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 1, 0), 34);
}
}

void gtor_46(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t39 = *(int8_t *)(lp + 0);
int8_t x17 = *(int8_t *)(lp + 1);
if ((! (((t39) == (INT32_C(3)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 48, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 33);
}
if (((t39) == (INT32_C(3)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 47, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 33);
}
}

void gtor_45(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x17 = *(int8_t *)(lp + 0);
{
int8_t t39 = *(int8_t *)(gp + 1);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 46, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t39, 1);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 1, 0), 33);
}
}

void gtor_44(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x17 = *(int8_t *)(lp + 0);
int8_t t38 = *(int8_t *)(lp + 1);
{
int8_t t47 = x17;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 45, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
int i = t38;
*((int8_t *)(gp2 + 3) + i) = t47;
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 3 + i, 0), 32);
}
}

void gtor_43(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t38 = *(int8_t *)(lp + 0);
int8_t x17 = *(int8_t *)(lp + 1);
{
int8_t t48 = ((t38) + (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 44, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 1) = t48;
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x17, 1);
r = enc_var(r, &t38, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 1, 0), 32);
}
}

void gtor_42(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x17 = *(int8_t *)(lp + 0);
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 40, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 30);
}
}
}

void gtor_41(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t37 = *(int8_t *)(lp + 0);
int8_t x17 = *(int8_t *)(lp + 1);
if (((t37) == (INT32_C(3)))) {
id_t cond_id = 0;
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 42, cond_id);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_WAIT, 0, 0), 29);
}
if ((! (((t37) == (INT32_C(3)))))) {
int8_t t38 = *(int8_t *)(gp + 1);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 43, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t38, 1);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 1, 0), 29);
}
}

void gtor_40(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x17 = *(int8_t *)(lp + 0);
{
int8_t t37 = *(int8_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 41, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t37, 1);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 29);
}
}

void gtor_39(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x17 = *(int8_t *)(lp + 0);
if (((x17) < (INT32_C(6)))) {
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 40, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 27);
}
}
if ((! (((x17) < (INT32_C(6)))))) {
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, SID_OMEGA, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_EXIT, 0, 0), 27);
}
}

void gtor_38(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x17 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 39, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 27);
}
}

void gtor_37(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, SID_OMEGA, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_EXIT, 0, 0), 103);
}
}

void gtor_36(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x12 = *(int32_t *)(lp + 0);
int32_t x10 = *(int32_t *)(lp + 4);
x10 = ((x10) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 34, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_var(r, &x12, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 96);
}
}

void gtor_35(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
int16_t t28 = *(int16_t *)(lp + 4);
int32_t x12 = *(int32_t *)(lp + 6);
x12 = ((x12) + (t28));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 36, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x12, 4);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 96);
}
}

void gtor_34(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
int32_t x12 = *(int32_t *)(lp + 4);
if (((x10) < (INT32_C(2)))) {
int i = x10;
int16_t t28 = *((int16_t *)(gp + 6) + i);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 35, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_var(r, &t28, 2);
r = enc_var(r, &x12, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 6 + i, 0), 96);
}
if ((! (((x10) < (INT32_C(2)))))) {
if (!(((((INT32_C(2)) * (x12))) == (INT32_C(30))))) assertion_violation(cc, 96);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 37, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_ASSERT, 0, 0), 96);
}
}

void gtor_33(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
x10 = ((x10) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 30, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 91);
}
}

void gtor_32(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x12 = INT32_C(0);
int32_t x10 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 34, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_var(r, &x12, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 96);
}
}

void gtor_31(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
{
uint8_t i = x10;
uint8_t p = 2 + i;
if (v[p].sid == SID_OMEGA) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv & ~(BV1 << p));
bp = enc_thst_join(sv, bp, v, n, k, 33, p);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p + 1, n);
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p + 1, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_JOIN, 2 + i, 0), 92);
}
}
}

void gtor_30(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
if (((x10) < (INT32_C(2)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 31, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 91);
}
if ((! (((x10) < (INT32_C(2)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 32, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 91);
}
}

void gtor_29(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x11 = *(int8_t *)(lp + 0);
x11 = ((x11) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 14, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 78);
}
}

void gtor_28(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x11 = *(int8_t *)(lp + 0);
{
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 29, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 0, 0), 88);
}
}

void gtor_27(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x11 = *(int8_t *)(lp + 0);
{
id_t cond_id = 1;
int c = 0;
for (int i = 0; i < n; ++i) {
if (v[i].cid == cond_id) {
c++;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_signal(sv, bp, v, n, k, 28, i);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_SIGNAL, 1, 0), 87);
}
}
if (c == 0) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 28, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_SIGNAL, 1, 0), 87);
}
}
}

void gtor_26(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t25 = *(int8_t *)(lp + 0);
int8_t x11 = *(int8_t *)(lp + 1);
{
int8_t t49 = ((t25) + (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 27, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 0) = t49;
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 0, 0), 86);
}
}

void gtor_25(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x11 = *(int8_t *)(lp + 0);
{
int8_t t25 = *(int8_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 26, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t25, 1);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 86);
}
}

void gtor_24(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x11 = *(int8_t *)(lp + 0);
{
int8_t t50 = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 25, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 1) = t50;
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 1, 0), 85);
}
}

void gtor_23(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t24 = *(int8_t *)(lp + 0);
int8_t x11 = *(int8_t *)(lp + 1);
if ((! (((t24) == (INT32_C(3)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 25, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 84);
}
if (((t24) == (INT32_C(3)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 24, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 84);
}
}

void gtor_22(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x11 = *(int8_t *)(lp + 0);
{
int8_t t24 = *(int8_t *)(gp + 1);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 23, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t24, 1);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 1, 0), 84);
}
}

void gtor_21(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t23 = *(int8_t *)(lp + 0);
int8_t x11 = *(int8_t *)(lp + 1);
{
int8_t t51 = INT32_C(-1);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 22, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
int i = t23;
*((int8_t *)(gp2 + 3) + i) = t51;
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 3 + i, 0), 83);
}
}

void gtor_20(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t23 = *(int8_t *)(lp + 0);
int8_t x11 = *(int8_t *)(lp + 1);
{
int8_t t52 = ((t23) + (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 21, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 1) = t52;
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t23, 1);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 1, 0), 83);
}
}

void gtor_19(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x11 = *(int8_t *)(lp + 0);
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 17, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 81);
}
}
}

void gtor_18(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t22 = *(int8_t *)(lp + 0);
int8_t x11 = *(int8_t *)(lp + 1);
if (((t22) == (INT32_C(3)))) {
id_t cond_id = 0;
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 19, cond_id);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_WAIT, 0, 0), 80);
}
if ((! (((t22) == (INT32_C(3)))))) {
int8_t t23 = *(int8_t *)(gp + 1);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 20, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t23, 1);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 1, 0), 80);
}
}

void gtor_17(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x11 = *(int8_t *)(lp + 0);
{
int8_t t22 = *(int8_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 18, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t22, 1);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 80);
}
}

void gtor_16(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 30, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 91);
}
}

void gtor_15(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x11 = *(int8_t *)(lp + 0);
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 17, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 79);
}
}
}

void gtor_14(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x11 = *(int8_t *)(lp + 0);
if (((x11) < (INT32_C(2)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 15, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 78);
}
if ((! (((x11) < (INT32_C(2)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 16, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 78);
}
}

void gtor_13(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
x10 = ((x10) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 10, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 74);
}
}

void gtor_12(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t x11 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 14, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 78);
}
}

void gtor_11(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
{
uint8_t i = x10;
uint8_t p = 1 + i;
if (v[p].sid == SID_OMEGA) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv & ~(BV1 << p));
bp = enc_thst_join(sv, bp, v, n, k, 13, p);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p + 1, n);
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p + 1, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_JOIN, 1 + i, 0), 75);
}
}
}

void gtor_10(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
if (((x10) < (INT32_C(1)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 11, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 74);
}
if ((! (((x10) < (INT32_C(1)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 12, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 74);
}
}

void gtor_9(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
x10 = ((x10) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 6, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 71);
}
}

void gtor_8(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 10, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 74);
}
}

void gtor_7(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
{
uint8_t i = x10;
uint8_t p = 1 + i;
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv | (BV1 << p));
bp = enc_thst_create(sv, bp, v, n, k, 9, p, 38);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
if (p < n) {
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p, n);
} else {
r = enc_lenv(r, v, k + 1, n);
}
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_CREATE, 1 + i, 0), 72);
}
}

void gtor_6(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
if (((x10) < (INT32_C(1)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 7, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 71);
}
if ((! (((x10) < (INT32_C(1)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 8, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 71);
}
}

void gtor_5(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
x10 = ((x10) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 2, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 68);
}
}

void gtor_4(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 6, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 71);
}
}

void gtor_3(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
{
uint8_t i = x10;
uint8_t p = 2 + i;
int8_t tmp = x10;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv | (BV1 << p));
bp = enc_thst_create(sv, bp, v, n, k, 5, p, 53);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
if (p < n) {
r = enc_lenv(r, v, k + 1, p);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, p, n);
} else {
r = enc_lenv(r, v, k + 1, n);
r = enc_var(r, &tmp, 1);
}
} else {
r = enc_lenv(r, v, 0, p);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, p, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_CREATE, 2 + i, 0), 69);
}
}

void gtor_2(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
if (((x10) < (INT32_C(2)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 3, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 68);
}
if ((! (((x10) < (INT32_C(2)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 4, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 68);
}
}

void gtor_1(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = 0;
x10 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 2, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 68);
}
}

void gtor_0(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t retval = *(int8_t *)(lp + 0);
}

pf_gtor_t gtor_table[] = {
&gtor_0,
&gtor_1,
&gtor_2,
&gtor_3,
&gtor_4,
&gtor_5,
&gtor_6,
&gtor_7,
&gtor_8,
&gtor_9,
&gtor_10,
&gtor_11,
&gtor_12,
&gtor_13,
&gtor_14,
&gtor_15,
&gtor_16,
&gtor_17,
&gtor_18,
&gtor_19,
&gtor_20,
&gtor_21,
&gtor_22,
&gtor_23,
&gtor_24,
&gtor_25,
&gtor_26,
&gtor_27,
&gtor_28,
&gtor_29,
&gtor_30,
&gtor_31,
&gtor_32,
&gtor_33,
&gtor_34,
&gtor_35,
&gtor_36,
&gtor_37,
&gtor_38,
&gtor_39,
&gtor_40,
&gtor_41,
&gtor_42,
&gtor_43,
&gtor_44,
&gtor_45,
&gtor_46,
&gtor_47,
&gtor_48,
&gtor_49,
&gtor_50,
&gtor_51,
&gtor_52,
&gtor_53,
&gtor_54,
&gtor_55,
&gtor_56,
&gtor_57,
&gtor_58,
&gtor_59,
&gtor_60,
&gtor_61,
&gtor_62,
&gtor_63,
&gtor_64,
&gtor_65,
&gtor_66,
&gtor_67,
&gtor_68,
&gtor_69,
&gtor_70,
};

unsigned num_vars_pth = 3;
const varinfo_t var_info_pth[] = {
{ 0, -1, "main" },
{ 0, 1, "pthp" },
{ 0, 2, "pthc" },
};

unsigned num_vars_mutex = 1;
const varinfo_t var_info_mutex[] = {
{ 0, -1, "mutex" },
};

unsigned num_vars_cond = 2;
const varinfo_t var_info_cond[] = {
{ 0, -1, "cv" },
{ 0, -1, "cv2" },
};

unsigned num_gvars = 5;
const varinfo_t global_var_info[] = {
{ 1, -1, "count" },
{ 1, -1, "putp" },
{ 1, -1, "getp" },
{ 1, 3, "buf" },
{ 2, 2, "sum" },
};

const varinfo_t local_var_info_0[] = {
{ 1, -1, "retval" },};

const varinfo_t local_var_info_2[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_3[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_5[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_6[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_7[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_9[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_10[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_11[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_13[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_14[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_15[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_17[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_18[] = {
{ 1, -1, "t22" },
{ 1, -1, "i" },};

const varinfo_t local_var_info_19[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_20[] = {
{ 1, -1, "t23" },
{ 1, -1, "i" },};

const varinfo_t local_var_info_21[] = {
{ 1, -1, "t23" },
{ 1, -1, "i" },};

const varinfo_t local_var_info_22[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_23[] = {
{ 1, -1, "t24" },
{ 1, -1, "i" },};

const varinfo_t local_var_info_24[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_25[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_26[] = {
{ 1, -1, "t25" },
{ 1, -1, "i" },};

const varinfo_t local_var_info_27[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_28[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_29[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_30[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_31[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_33[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_34[] = {
{ 4, -1, "i" },
{ 4, -1, "s" },};

const varinfo_t local_var_info_35[] = {
{ 4, -1, "i" },
{ 2, -1, "t28" },
{ 4, -1, "s" },};

const varinfo_t local_var_info_36[] = {
{ 4, -1, "s" },
{ 4, -1, "i" },};

const varinfo_t local_var_info_39[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_40[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_41[] = {
{ 1, -1, "t37" },
{ 1, -1, "i" },};

const varinfo_t local_var_info_42[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_43[] = {
{ 1, -1, "t38" },
{ 1, -1, "i" },};

const varinfo_t local_var_info_44[] = {
{ 1, -1, "i" },
{ 1, -1, "t38" },};

const varinfo_t local_var_info_45[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_46[] = {
{ 1, -1, "t39" },
{ 1, -1, "i" },};

const varinfo_t local_var_info_47[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_48[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_49[] = {
{ 1, -1, "t40" },
{ 1, -1, "i" },};

const varinfo_t local_var_info_50[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_51[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_52[] = {
{ 1, -1, "i" },};

const varinfo_t local_var_info_53[] = {
{ 1, -1, "arg" },};

const varinfo_t local_var_info_54[] = {
{ 1, -1, "k" },};

const varinfo_t local_var_info_55[] = {
{ 1, -1, "k" },};

const varinfo_t local_var_info_56[] = {
{ 1, -1, "t29" },
{ 1, -1, "k" },};

const varinfo_t local_var_info_57[] = {
{ 1, -1, "k" },};

const varinfo_t local_var_info_58[] = {
{ 1, -1, "t30" },
{ 1, -1, "k" },};

const varinfo_t local_var_info_59[] = {
{ 1, -1, "t30" },
{ 1, -1, "k" },};

const varinfo_t local_var_info_60[] = {
{ 1, -1, "k" },
{ 1, -1, "t31" },};

const varinfo_t local_var_info_61[] = {
{ 1, -1, "t32" },
{ 1, -1, "k" },
{ 1, -1, "x" },};

const varinfo_t local_var_info_62[] = {
{ 1, -1, "k" },
{ 1, -1, "x" },};

const varinfo_t local_var_info_63[] = {
{ 1, -1, "x" },
{ 1, -1, "k" },};

const varinfo_t local_var_info_64[] = {
{ 1, -1, "t33" },
{ 1, -1, "k" },
{ 1, -1, "x" },};

const varinfo_t local_var_info_65[] = {
{ 1, -1, "x" },
{ 1, -1, "k" },};

const varinfo_t local_var_info_66[] = {
{ 1, -1, "k" },
{ 1, -1, "x" },};

const varinfo_t local_var_info_67[] = {
{ 1, -1, "x" },
{ 1, -1, "k" },};

const varinfo_t local_var_info_68[] = {
{ 1, -1, "x" },
{ 1, -1, "k" },};

const varinfo_t local_var_info_70[] = {
{ 2, -1, "t35" },
{ 1, -1, "x" },
{ 1, -1, "t34" },
{ 1, -1, "k" },};

const local_varinfo_t local_var_info_table[] = {
{ 1, local_var_info_0 },
{ 0, NULL },
{ 1, local_var_info_2 },
{ 1, local_var_info_3 },
{ 0, NULL },
{ 1, local_var_info_5 },
{ 1, local_var_info_6 },
{ 1, local_var_info_7 },
{ 0, NULL },
{ 1, local_var_info_9 },
{ 1, local_var_info_10 },
{ 1, local_var_info_11 },
{ 0, NULL },
{ 1, local_var_info_13 },
{ 1, local_var_info_14 },
{ 1, local_var_info_15 },
{ 0, NULL },
{ 1, local_var_info_17 },
{ 2, local_var_info_18 },
{ 1, local_var_info_19 },
{ 2, local_var_info_20 },
{ 2, local_var_info_21 },
{ 1, local_var_info_22 },
{ 2, local_var_info_23 },
{ 1, local_var_info_24 },
{ 1, local_var_info_25 },
{ 2, local_var_info_26 },
{ 1, local_var_info_27 },
{ 1, local_var_info_28 },
{ 1, local_var_info_29 },
{ 1, local_var_info_30 },
{ 1, local_var_info_31 },
{ 0, NULL },
{ 1, local_var_info_33 },
{ 2, local_var_info_34 },
{ 3, local_var_info_35 },
{ 2, local_var_info_36 },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_39 },
{ 1, local_var_info_40 },
{ 2, local_var_info_41 },
{ 1, local_var_info_42 },
{ 2, local_var_info_43 },
{ 2, local_var_info_44 },
{ 1, local_var_info_45 },
{ 2, local_var_info_46 },
{ 1, local_var_info_47 },
{ 1, local_var_info_48 },
{ 2, local_var_info_49 },
{ 1, local_var_info_50 },
{ 1, local_var_info_51 },
{ 1, local_var_info_52 },
{ 1, local_var_info_53 },
{ 1, local_var_info_54 },
{ 1, local_var_info_55 },
{ 2, local_var_info_56 },
{ 1, local_var_info_57 },
{ 2, local_var_info_58 },
{ 2, local_var_info_59 },
{ 2, local_var_info_60 },
{ 3, local_var_info_61 },
{ 2, local_var_info_62 },
{ 2, local_var_info_63 },
{ 3, local_var_info_64 },
{ 2, local_var_info_65 },
{ 2, local_var_info_66 },
{ 2, local_var_info_67 },
{ 2, local_var_info_68 },
{ 0, NULL },
{ 4, local_var_info_70 },
};

int check_global_assertions(const void *gp)
{
int8_t g5 = *(int8_t *)(gp + 0);
int8_t g6 = *(int8_t *)(gp + 1);
int8_t g7 = *(int8_t *)(gp + 2);
int8_t *gv8 = (int8_t *)(gp + 3);
int16_t *gv9 = (int16_t *)(gp + 6);
return 0;
}

const char *source_line[] = {
"#include <stdio.h>",
"#include <stdbool.h>",
"#include <stdint.h>",
"#include <inttypes.h>",
"#include <pthread.h>",
"#include <assert.h>",
"",
"#define M  6",
"#define N  3",
"#define NP 1",
"#define NC 2",
"",
"pthread_t pthp[NP];",
"pthread_t pthc[NC];",
"pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;",
"pthread_cond_t cv = PTHREAD_COND_INITIALIZER;",
"pthread_cond_t cv2 = PTHREAD_COND_INITIALIZER;",
"",
"int8_t count;",
"int8_t putp;",
"int8_t getp;",
"int8_t buf[N];",
"int16_t sum[NC];",
"",
"void *producer(void *arg)",
"{",
"\tfor (int8_t i = 0; i < M; ++i) {",
"\t\tpthread_mutex_lock(&mutex);",
"\t\twhile (count == N) {",
"\t\t\tpthread_cond_wait(&cv, &mutex);",
"\t\t}",
"\t\tbuf[putp++] = i;",
"\t\tif (putp == N)",
"\t\t  putp = 0;",
"\t\tcount++;",
"\t\tpthread_cond_signal(&cv2);",
"\t\tpthread_mutex_unlock(&mutex);",
"\t}",
"\treturn NULL;",
"}",
"",
"void *consumer(void *arg)",
"{",
"\tint8_t k = (int8_t)(intptr_t)arg;",
"\tint8_t x;",
"\twhile (true) {",
"\t\tpthread_mutex_lock(&mutex);",
"\t\twhile (count == 0) {",
"\t\t\tpthread_cond_wait(&cv2, &mutex);",
"\t\t}",
"\t\tx = buf[getp++];",
"\t\tif (getp == N)",
"\t\t  getp = 0;",
"\t\tcount--;",
"\t\tpthread_cond_signal(&cv);",
"\t\tpthread_mutex_unlock(&mutex);",
"\t\tif (x == -1)",
"\t\t  break;",
"\t\tprintf(\"%\" PRId8 \" %\" PRId8 \"\\n\", k, x);",
"\t\tsum[k] += x;",
"\t}",
"\treturn NULL;",
"}",
"",
"int main(void)",
"{",
"    int i;",
"    for (i = 0; i < NC; ++i) {",
"        pthread_create(&pthc[i], NULL, &consumer, (void *)(intptr_t)i);",
"    }",
"    for (i = 0; i < NP; ++i) {",
"        pthread_create(&pthp[i], NULL, &producer, NULL);",
"    }",
"    for (i = 0; i < NP; ++i) {",
"        pthread_join(pthp[i], NULL);",
"    }",
"",
"\tfor (int8_t i = 0; i < NC; ++i) {",
"\t\tpthread_mutex_lock(&mutex);",
"\t\twhile (count == N) {",
"\t\t\tpthread_cond_wait(&cv, &mutex);",
"\t\t}",
"\t\tbuf[putp++] = -1;",
"\t\tif (putp == N)",
"\t\t  putp = 0;",
"\t\tcount++;",
"\t\tpthread_cond_signal(&cv2);",
"\t\tpthread_mutex_unlock(&mutex);",
"\t}",
"",
"    for (i = 0; i < NC; ++i) {",
"        pthread_join(pthc[i], NULL);",
"    }",
"",
"\tint s = 0;",
"    for (i = 0; i < NC; ++i) {",
"\t\ts += sum[i];",
"\t}",
"\tprintf(\"%d\\n\", s);",
"\tassert(2 * s == M * (M - 1) * NP);",
"//\tassert(sum[0] != 0);",
"",
"\treturn 0;",
"}",
};
