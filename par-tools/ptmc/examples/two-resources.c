#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>
#include <assert.h>

pthread_t pth1, pth2;
pthread_mutex_t m1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t m2 = PTHREAD_MUTEX_INITIALIZER;

void *thread1(void *arg)
{
    pthread_mutex_lock(&m1); // ###
    pthread_mutex_lock(&m2);
    pthread_mutex_unlock(&m2);
    pthread_mutex_unlock(&m1);
    return NULL;
}

void *thread2(void *arg)
{
    pthread_mutex_lock(&m2); // ###
    pthread_mutex_lock(&m1);
    pthread_mutex_unlock(&m1);
    pthread_mutex_unlock(&m2);
    return NULL;
}

int main(void)
{
    pthread_create(&pth1, NULL, &thread1, NULL);
    pthread_create(&pth2, NULL, &thread2, NULL);
    pthread_join(pth1, NULL);
    pthread_join(pth2, NULL);
    return 0;
}
