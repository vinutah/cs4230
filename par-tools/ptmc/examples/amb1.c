#include <assert.h>
#include "amb.h"

int main(void)
{
	int x;
	AMB {
		x = 0;
	} OR {
		x = 1;
	}
	assert(x == 0 || x == 1);
	return 0;
}
