#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h>
#include <assert.h>

#define M  6
#define N  3
#define NP 1
#define NC 2

pthread_t pthp[NP];
pthread_t pthc[NC];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cv = PTHREAD_COND_INITIALIZER;
pthread_cond_t cv2 = PTHREAD_COND_INITIALIZER;

int8_t count;
int8_t putp;
int8_t getp;
int8_t buf[N];
int16_t sum[NC];

void *producer(void *arg)
{
	for (int8_t i = 0; i < M; ++i) {
		pthread_mutex_lock(&mutex);
		while (count == N) {
			pthread_cond_wait(&cv, &mutex);
		}
		buf[putp++] = i;
		if (putp == N)
		  putp = 0;
		count++;
		pthread_cond_signal(&cv2);
		pthread_mutex_unlock(&mutex);
	}
	return NULL;
}

void *consumer(void *arg)
{
	int8_t k = (int8_t)(intptr_t)arg;
	int8_t x;
	while (true) {
		pthread_mutex_lock(&mutex);
		while (count == 0) {
			pthread_cond_wait(&cv2, &mutex);
		}
		x = buf[getp++];
		if (getp == N)
		  getp = 0;
		count--;
		pthread_cond_signal(&cv);
		pthread_mutex_unlock(&mutex);
		if (x == -1)
		  break;
		printf("%" PRId8 " %" PRId8 "\n", k, x);
		sum[k] += x;
	}
	return NULL;
}

int main(void)
{
    int i;
    for (i = 0; i < NC; ++i) {
        pthread_create(&pthc[i], NULL, &consumer, (void *)(intptr_t)i);
    }
    for (i = 0; i < NP; ++i) {
        pthread_create(&pthp[i], NULL, &producer, NULL);
    }
    for (i = 0; i < NP; ++i) {
        pthread_join(pthp[i], NULL);
    }

	for (int8_t i = 0; i < NC; ++i) {
		pthread_mutex_lock(&mutex);
		while (count == N) {
			pthread_cond_wait(&cv, &mutex);
		}
		buf[putp++] = -1;
		if (putp == N)
		  putp = 0;
		count++;
		pthread_cond_signal(&cv2);
		pthread_mutex_unlock(&mutex);
	}

    for (i = 0; i < NC; ++i) {
        pthread_join(pthc[i], NULL);
    }

	int s = 0;
    for (i = 0; i < NC; ++i) {
		s += sum[i];
	}
	printf("%d\n", s);
	assert(2 * s == M * (M - 1) * NP);
//	assert(sum[0] != 0);

	return 0;
}
