#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>
#include <assert.h>

pthread_t pth1, pth2;
int count;

void *thread(void *arg)
{
    count++;
    return NULL;
}

int main(void)
{
    void *r;
    pthread_create(&pth1, NULL, &thread, NULL);
    pthread_create(&pth2, NULL, &thread, NULL);
    pthread_join(pth1, &r);
    pthread_join(pth2, &r);
    assert(count == 2);
    return 0;
}
