#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <limits.h>
#include <alloca.h>

typedef uint32_t bits_t;
typedef uint64_t bitvec_t;
typedef uint32_t id_t;
typedef uint32_t trans_t;
typedef struct context *context_t;

#define BV1         UINT64_C(1)
#define ID_NIL      UINT32_MAX
#define SID_OMEGA   0
#define PRIid       PRIu32
#define PRItrans    PRIx32

enum {
    TRANS_NIL,                  /* 0 */
    TRANS_ASSERT,               /* 1 */
    TRANS_TAU,                  /* 2 */
    TRANS_LOAD,                 /* 3 */
    TRANS_STORE,                /* 4 */
    TRANS_EXIT,                 /* 5 */
    TRANS_CREATE,               /* 6 */
    TRANS_JOIN,                 /* 7 */
    TRANS_LOCK,                 /* 8 */
    TRANS_UNLOCK,               /* 9 */
    TRANS_WAIT,                 /* 10 */
    TRANS_SIGNAL,               /* 11 */
    TRANS_BROADCAST,            /* 12 */
    TRANS_SPURIOUS,             /* 1 */
};

typedef struct thst thst_t;
struct thst {
  id_t sid;
  id_t cid;
  const void *lp;
};

typedef struct varinfo varinfo_t;
struct varinfo {
  int size;
  int dim;
  const char *name;
};

typedef struct local_varinfo local_varinfo_t;
struct local_varinfo {
  int n;
  const varinfo_t *varinfo;
};

typedef void (*pf_gtor_t)(context_t, bitvec_t, const void *, const thst_t [], int, int,
                          const id_t mtxv[]);

void assertion_violation(context_t, uint16_t);
void error_mutex_not_owner(context_t cc, int k, id_t mutex_id);

uint64_t *alloc_state(context_t);
void register_state(context_t, void *, trans_t, uint16_t);

uint64_t *enc_bv(uint64_t *sv, bitvec_t bv);
bits_t enc_thst(uint64_t *sv, bits_t bp, id_t sid, id_t cid);
bits_t enc_thst_v(uint64_t *sv, bits_t bp, const thst_t v[], int i, int j);
bits_t enc_thst_update(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, id_t cid);
bits_t enc_thst_create(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, int p, id_t sid0);
bits_t enc_thst_join(uint64_t *sv, bits_t bp,
					 const thst_t v[], int n, int k, id_t sid, int p);
bits_t enc_thst_signal(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, int p);
bits_t enc_thst_broadcast(uint64_t *sv, bits_t bp,
						  const thst_t v[], int n, int k, id_t sid, id_t cid);
bits_t enc_mtx(uint64_t *sv, bits_t bp, const id_t mtxv[]);
bits_t enc_mtx_update(uint64_t *sv, bits_t bp, const id_t mtxv[], id_t mid, id_t tid);
void *enc_align(uint64_t *sv, bits_t bp);
void *enc_genv(void *env, const void *gp);
void *enc_var(void *env, const void *p, unsigned size);
void *enc_val(void *env, uint64_t x, unsigned size);
void *enc_lenv(void *env, const thst_t v[], int i, int j);

static inline trans_t enc_trans(int32_t who, int32_t tag, int32_t arg0, int32_t arg1)
{
    return who | (tag << 8) | (arg0 << 16) | (arg1 << 24);
}
int ver_maj = 1;
int ver_min = 4;
int ver_mnt = 1;
const char model_filename[] = "two-resources2.c";

unsigned num_tid = 3;
unsigned num_sid = 16;
unsigned num_mid = 2;
unsigned num_cid = 0;
bits_t bits_tid = 2;
bits_t bits_tidx = 2;
bits_t bits_sid = 4;
bits_t bits_mid = 1;
bits_t bits_cid = 0;
unsigned size_global_env = 0;
unsigned size_local_env[] = {
1,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
};
void gtor_15(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, SID_OMEGA, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_EXIT, 0, 0), 17);
}
}

void gtor_14(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 15, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 0, 0), 16);
}
}

void gtor_13(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 1;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 14, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 1, 0), 15);
}
}

void gtor_12(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 1;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 13, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 1, 0), 14);
}
}
}

void gtor_11(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 12, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 13);
}
}
}

void gtor_10(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, SID_OMEGA, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_EXIT, 0, 0), 26);
}
}

void gtor_9(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 10, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 0, 0), 25);
}
}

void gtor_8(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 1;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 9, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 1, 0), 24);
}
}

void gtor_7(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 1;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 8, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 1, 0), 23);
}
}
}

void gtor_6(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 7, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 22);
}
}
}

void gtor_5(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, SID_OMEGA, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_EXIT, 0, 0), 35);
}
}

void gtor_4(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
uint8_t p = 2;
if (v[p].sid == SID_OMEGA) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv & ~(BV1 << p));
bp = enc_thst_join(sv, bp, v, n, k, 5, p);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p + 1, n);
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p + 1, k);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_JOIN, 2, 0), 34);
}
}
}

void gtor_3(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
uint8_t p = 1;
if (v[p].sid == SID_OMEGA) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv & ~(BV1 << p));
bp = enc_thst_join(sv, bp, v, n, k, 4, p);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p + 1, n);
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p + 1, k);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_JOIN, 1, 0), 33);
}
}
}

void gtor_2(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
uint8_t p = 2;
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv | (BV1 << p));
bp = enc_thst_create(sv, bp, v, n, k, 3, p, 6);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
if (p < n) {
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p, n);
} else {
r = enc_lenv(r, v, k + 1, n);
}
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p, k);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_CREATE, 2, 0), 32);
}
}

void gtor_1(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
uint8_t p = 1;
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv | (BV1 << p));
bp = enc_thst_create(sv, bp, v, n, k, 2, p, 11);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
if (p < n) {
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p, n);
} else {
r = enc_lenv(r, v, k + 1, n);
}
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p, k);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_CREATE, 1, 0), 31);
}
}

void gtor_0(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t retval = *(int8_t *)(lp + 0);
}

pf_gtor_t gtor_table[] = {
&gtor_0,
&gtor_1,
&gtor_2,
&gtor_3,
&gtor_4,
&gtor_5,
&gtor_6,
&gtor_7,
&gtor_8,
&gtor_9,
&gtor_10,
&gtor_11,
&gtor_12,
&gtor_13,
&gtor_14,
&gtor_15,
};

unsigned num_vars_pth = 3;
const varinfo_t var_info_pth[] = {
{ 0, -1, "main" },
{ 0, -1, "pth1" },
{ 0, -1, "pth2" },
};

unsigned num_vars_mutex = 2;
const varinfo_t var_info_mutex[] = {
{ 0, -1, "m1" },
{ 0, -1, "m2" },
};

unsigned num_vars_cond = 0;
const varinfo_t var_info_cond[] = {
};

unsigned num_gvars = 0;
const varinfo_t global_var_info[] = {
};

const varinfo_t local_var_info_0[] = {
{ 1, -1, "retval" },};

const local_varinfo_t local_var_info_table[] = {
{ 1, local_var_info_0 },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
};

int check_global_assertions(const void *gp)
{
return 0;
}

const char *source_line[] = {
"#include <stdio.h>",
"#include <stdbool.h>",
"#include <stdint.h>",
"#include <pthread.h>",
"#include <assert.h>",
"",
"pthread_t pth1, pth2;",
"pthread_mutex_t m1 = PTHREAD_MUTEX_INITIALIZER;",
"pthread_mutex_t m2 = PTHREAD_MUTEX_INITIALIZER;",
"",
"void *thread1(void *arg)",
"{",
"    pthread_mutex_lock(&m1);",
"    pthread_mutex_lock(&m2);",
"    pthread_mutex_unlock(&m2);",
"    pthread_mutex_unlock(&m1);",
"    return NULL;",
"}",
"",
"void *thread2(void *arg)",
"{",
"    pthread_mutex_lock(&m1);",
"    pthread_mutex_lock(&m2);",
"    pthread_mutex_unlock(&m2);",
"    pthread_mutex_unlock(&m1);",
"    return NULL;",
"}",
"",
"int main(void)",
"{",
"    pthread_create(&pth1, NULL, &thread1, NULL);",
"    pthread_create(&pth2, NULL, &thread2, NULL);",
"    pthread_join(pth1, NULL);",
"    pthread_join(pth2, NULL);",
"    return 0;",
"}",
};
