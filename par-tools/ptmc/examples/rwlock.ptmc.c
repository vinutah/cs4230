#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <limits.h>
#include <alloca.h>

typedef uint32_t bits_t;
typedef uint64_t bitvec_t;
typedef uint32_t id_t;
typedef uint32_t trans_t;
typedef struct context *context_t;

#define BV1         UINT64_C(1)
#define ID_NIL      UINT32_MAX
#define SID_OMEGA   0
#define PRIid       PRIu32
#define PRItrans    PRIx32

enum {
    TRANS_NIL,                  /* 0 */
    TRANS_ASSERT,               /* 1 */
    TRANS_TAU,                  /* 2 */
    TRANS_LOAD,                 /* 3 */
    TRANS_STORE,                /* 4 */
    TRANS_EXIT,                 /* 5 */
    TRANS_CREATE,               /* 6 */
    TRANS_JOIN,                 /* 7 */
    TRANS_LOCK,                 /* 8 */
    TRANS_UNLOCK,               /* 9 */
    TRANS_WAIT,                 /* 10 */
    TRANS_SIGNAL,               /* 11 */
    TRANS_BROADCAST,            /* 12 */
    TRANS_SPURIOUS,             /* 1 */
};

typedef struct thst thst_t;
struct thst {
  id_t sid;
  id_t cid;
  const void *lp;
};

typedef struct varinfo varinfo_t;
struct varinfo {
  int size;
  int dim;
  const char *name;
};

typedef struct local_varinfo local_varinfo_t;
struct local_varinfo {
  int n;
  const varinfo_t *varinfo;
};

typedef void (*pf_gtor_t)(context_t, bitvec_t, const void *, const thst_t [], int, int,
                          const id_t mtxv[]);

void assertion_violation(context_t, uint16_t);
void error_mutex_not_owner(context_t cc, int k, id_t mutex_id);

uint64_t *alloc_state(context_t);
void register_state(context_t, void *, trans_t, uint16_t);

uint64_t *enc_bv(uint64_t *sv, bitvec_t bv);
bits_t enc_thst(uint64_t *sv, bits_t bp, id_t sid, id_t cid);
bits_t enc_thst_v(uint64_t *sv, bits_t bp, const thst_t v[], int i, int j);
bits_t enc_thst_update(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, id_t cid);
bits_t enc_thst_create(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, int p, id_t sid0);
bits_t enc_thst_join(uint64_t *sv, bits_t bp,
					 const thst_t v[], int n, int k, id_t sid, int p);
bits_t enc_thst_signal(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, int p);
bits_t enc_thst_broadcast(uint64_t *sv, bits_t bp,
						  const thst_t v[], int n, int k, id_t sid, id_t cid);
bits_t enc_mtx(uint64_t *sv, bits_t bp, const id_t mtxv[]);
bits_t enc_mtx_update(uint64_t *sv, bits_t bp, const id_t mtxv[], id_t mid, id_t tid);
void *enc_align(uint64_t *sv, bits_t bp);
void *enc_genv(void *env, const void *gp);
void *enc_var(void *env, const void *p, unsigned size);
void *enc_val(void *env, uint64_t x, unsigned size);
void *enc_lenv(void *env, const thst_t v[], int i, int j);

static inline trans_t enc_trans(int32_t who, int32_t tag, int32_t arg0, int32_t arg1)
{
    return who | (tag << 8) | (arg0 << 16) | (arg1 << 24);
}
int ver_maj = 1;
int ver_min = 4;
int ver_mnt = 1;
const char model_filename[] = "rwlock.c";

unsigned num_tid = 5;
unsigned num_sid = 63;
unsigned num_mid = 3;
unsigned num_cid = 1;
bits_t bits_tid = 3;
bits_t bits_tidx = 3;
bits_t bits_sid = 6;
bits_t bits_mid = 2;
bits_t bits_cid = 0;
unsigned size_global_env = 4;
unsigned size_local_env[] = {
1,
0,
4,
4,
0,
4,
4,
4,
0,
4,
4,
4,
0,
4,
4,
4,
1,
0,
0,
1,
2,
0,
1,
0,
0,
0,
1,
0,
0,
1,
0,
1,
0,
0,
1,
0,
0,
0,
1,
0,
0,
1,
0,
0,
1,
0,
1,
0,
0,
0,
1,
0,
0,
1,
0,
0,
1,
0,
0,
0,
1,
0,
0,
};
void gtor_62(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 42, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 0, 0), 41);
}
}

void gtor_61(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t cond_id = 0;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_broadcast(sv, bp, v, n, k, 62, cond_id);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_BROADCAST, 0, 0), 40);
}
}

void gtor_60(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t24 = *(int8_t *)(lp + 0);
{
int8_t t36 = ((t24) - (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 61, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 0) = t36;
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 0, 0), 39);
}
}

void gtor_59(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t t24 = *(int8_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 60, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t24, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 39);
}
}

void gtor_58(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 59, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 38);
}
}
}

void gtor_57(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 1;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 58, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 1, 0), 79);
}
}

void gtor_56(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t27 = *(int8_t *)(lp + 0);
{
int8_t t37 = ((t27) - (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 57, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 2) = t37;
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 2, 0), 78);
}
}

void gtor_55(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t t27 = *(int8_t *)(gp + 2);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 56, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t27, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 2, 0), 78);
}
}

void gtor_54(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 1;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 55, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 1, 0), 77);
}
}
}

void gtor_53(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t26 = *(int8_t *)(lp + 0);
{
if (!(((t26) == (INT32_C(0))))) assertion_violation(cc, 74);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 54, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_ASSERT, 0, 0), 74);
}
}

void gtor_52(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t t26 = *(int8_t *)(gp + 3);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 53, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t26, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 3, 0), 74);
}
}

void gtor_51(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 1;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 52, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 1, 0), 70);
}
}

void gtor_50(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t25 = *(int8_t *)(lp + 0);
{
int8_t t38 = ((t25) + (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 51, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 2) = t38;
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 2, 0), 69);
}
}

void gtor_49(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t t25 = *(int8_t *)(gp + 2);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 50, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t25, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 2, 0), 69);
}
}

void gtor_48(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 1;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 49, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 1, 0), 68);
}
}
}

void gtor_47(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 48, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 0, 0), 33);
}
}

void gtor_46(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t23 = *(int8_t *)(lp + 0);
{
int8_t t39 = ((t23) + (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 47, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 0) = t39;
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 0, 0), 32);
}
}

void gtor_45(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 43, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 30);
}
}
}

void gtor_44(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t22 = *(int8_t *)(lp + 0);
if (((t22) > (INT32_C(0)))) {
id_t cond_id = 0;
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 45, cond_id);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_WAIT, 0, 0), 29);
}
if ((! (((t22) > (INT32_C(0)))))) {
int8_t t23 = *(int8_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 46, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t23, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 29);
}
}

void gtor_43(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t t22 = *(int8_t *)(gp + 1);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 44, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t22, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 1, 0), 29);
}
}

void gtor_42(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 43, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 28);
}
}
}

void gtor_41(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t a14 = *(int8_t *)(lp + 0);
int8_t x15 = a14;
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 42, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 65);
}
}

void gtor_40(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 17, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 0, 0), 59);
}
}

void gtor_39(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t cond_id = 0;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_broadcast(sv, bp, v, n, k, 40, cond_id);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_BROADCAST, 0, 0), 58);
}
}

void gtor_38(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t31 = *(int8_t *)(lp + 0);
{
int8_t t40 = ((t31) - (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 39, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 1) = t40;
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 1, 0), 57);
}
}

void gtor_37(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t t31 = *(int8_t *)(gp + 1);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 38, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t31, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 1, 0), 57);
}
}

void gtor_36(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 37, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 56);
}
}
}

void gtor_35(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 2;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 36, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 2, 0), 103);
}
}

void gtor_34(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t35 = *(int8_t *)(lp + 0);
{
int8_t t41 = ((t35) - (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 35, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 3) = t41;
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 3, 0), 102);
}
}

void gtor_33(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t t35 = *(int8_t *)(gp + 3);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 34, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t35, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 3, 0), 102);
}
}

void gtor_32(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 2;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 33, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 2, 0), 101);
}
}
}

void gtor_31(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t34 = *(int8_t *)(lp + 0);
{
if (!(((t34) == (INT32_C(0))))) assertion_violation(cc, 99);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 32, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_ASSERT, 0, 0), 99);
}
}

void gtor_30(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t t34 = *(int8_t *)(gp + 2);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 31, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t34, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 2, 0), 99);
}
}

void gtor_29(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t33 = *(int8_t *)(lp + 0);
{
if (!(((t33) == (INT32_C(1))))) assertion_violation(cc, 98);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 30, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_ASSERT, 0, 0), 98);
}
}

void gtor_28(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t t33 = *(int8_t *)(gp + 3);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 29, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t33, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 3, 0), 98);
}
}

void gtor_27(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 2;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 28, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 2, 0), 94);
}
}

void gtor_26(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t32 = *(int8_t *)(lp + 0);
{
int8_t t42 = ((t32) + (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 27, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 3) = t42;
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 3, 0), 93);
}
}

void gtor_25(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t t32 = *(int8_t *)(gp + 3);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 26, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t32, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 3, 0), 93);
}
}

void gtor_24(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 2;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 25, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 2, 0), 92);
}
}
}

void gtor_23(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 24, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 0, 0), 51);
}
}

void gtor_22(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t30 = *(int8_t *)(lp + 0);
{
int8_t t43 = ((t30) + (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 23, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 1) = t43;
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 1, 0), 50);
}
}

void gtor_21(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 18, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 48);
}
}
}

void gtor_20(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t28 = *(int8_t *)(lp + 0);
int8_t t29 = *(int8_t *)(lp + 1);
if (((((t28) > (INT32_C(0)))) || (((t29) > (INT32_C(0)))))) {
id_t cond_id = 0;
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 21, cond_id);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_WAIT, 0, 0), 47);
}
if ((! (((((t28) > (INT32_C(0)))) || (((t29) > (INT32_C(0)))))))) {
int8_t t30 = *(int8_t *)(gp + 1);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 22, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t30, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 1, 0), 47);
}
}

void gtor_19(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t28 = *(int8_t *)(lp + 0);
{
int8_t t29 = *(int8_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 20, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t28, 1);
r = enc_var(r, &t29, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 47);
}
}

void gtor_18(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t t28 = *(int8_t *)(gp + 1);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 19, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t28, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 1, 0), 47);
}
}

void gtor_17(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 18, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 46);
}
}
}

void gtor_16(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t a16 = *(int8_t *)(lp + 0);
int8_t x17 = a16;
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 17, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 89);
}
}

void gtor_15(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x13 = *(int32_t *)(lp + 0);
x13 = ((x13) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 14, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x13, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 124);
}
}

void gtor_14(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x13 = *(int32_t *)(lp + 0);
if (((x13) < (INT32_C(2)))) {
uint8_t i = x13;
uint8_t p = 3 + i;
if (v[p].sid == SID_OMEGA) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv & ~(BV1 << p));
bp = enc_thst_join(sv, bp, v, n, k, 15, p);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x13, 4);
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p + 1, n);
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p + 1, k);
r = enc_var(r, &x13, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_JOIN, 3 + i, 0), 124);
}
}
if ((! (((x13) < (INT32_C(2)))))) {
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, SID_OMEGA, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_EXIT, 0, 0), 124);
}
}

void gtor_13(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x12 = *(int32_t *)(lp + 0);
x12 = ((x12) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 10, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x12, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 120);
}
}

void gtor_12(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x13 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 14, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x13, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 124);
}
}

void gtor_11(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x12 = *(int32_t *)(lp + 0);
{
uint8_t i = x12;
uint8_t p = 1 + i;
if (v[p].sid == SID_OMEGA) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv & ~(BV1 << p));
bp = enc_thst_join(sv, bp, v, n, k, 13, p);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x12, 4);
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p + 1, n);
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p + 1, k);
r = enc_var(r, &x12, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_JOIN, 1 + i, 0), 121);
}
}
}

void gtor_10(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x12 = *(int32_t *)(lp + 0);
if (((x12) < (INT32_C(2)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 11, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x12, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 120);
}
if ((! (((x12) < (INT32_C(2)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 12, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 120);
}
}

void gtor_9(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x11 = *(int32_t *)(lp + 0);
x11 = ((x11) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 6, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 116);
}
}

void gtor_8(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x12 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 10, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x12, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 120);
}
}

void gtor_7(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x11 = *(int32_t *)(lp + 0);
{
uint8_t i = x11;
uint8_t p = 3 + i;
int8_t tmp = x11;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv | (BV1 << p));
bp = enc_thst_create(sv, bp, v, n, k, 9, p, 16);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 4);
if (p < n) {
r = enc_lenv(r, v, k + 1, p);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, p, n);
} else {
r = enc_lenv(r, v, k + 1, n);
r = enc_var(r, &tmp, 1);
}
} else {
r = enc_lenv(r, v, 0, p);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, p, k);
r = enc_var(r, &x11, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_CREATE, 3 + i, 0), 117);
}
}

void gtor_6(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x11 = *(int32_t *)(lp + 0);
if (((x11) < (INT32_C(2)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 7, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 116);
}
if ((! (((x11) < (INT32_C(2)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 8, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 116);
}
}

void gtor_5(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
x10 = ((x10) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 2, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 112);
}
}

void gtor_4(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x11 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 6, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x11, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 116);
}
}

void gtor_3(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
{
uint8_t i = x10;
uint8_t p = 1 + i;
int8_t tmp = x10;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv | (BV1 << p));
bp = enc_thst_create(sv, bp, v, n, k, 5, p, 41);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
if (p < n) {
r = enc_lenv(r, v, k + 1, p);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, p, n);
} else {
r = enc_lenv(r, v, k + 1, n);
r = enc_var(r, &tmp, 1);
}
} else {
r = enc_lenv(r, v, 0, p);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, p, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_CREATE, 1 + i, 0), 113);
}
}

void gtor_2(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = *(int32_t *)(lp + 0);
if (((x10) < (INT32_C(2)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 3, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 112);
}
if ((! (((x10) < (INT32_C(2)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 4, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 112);
}
}

void gtor_1(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x10 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 2, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x10, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 112);
}
}

void gtor_0(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t retval = *(int8_t *)(lp + 0);
}

pf_gtor_t gtor_table[] = {
&gtor_0,
&gtor_1,
&gtor_2,
&gtor_3,
&gtor_4,
&gtor_5,
&gtor_6,
&gtor_7,
&gtor_8,
&gtor_9,
&gtor_10,
&gtor_11,
&gtor_12,
&gtor_13,
&gtor_14,
&gtor_15,
&gtor_16,
&gtor_17,
&gtor_18,
&gtor_19,
&gtor_20,
&gtor_21,
&gtor_22,
&gtor_23,
&gtor_24,
&gtor_25,
&gtor_26,
&gtor_27,
&gtor_28,
&gtor_29,
&gtor_30,
&gtor_31,
&gtor_32,
&gtor_33,
&gtor_34,
&gtor_35,
&gtor_36,
&gtor_37,
&gtor_38,
&gtor_39,
&gtor_40,
&gtor_41,
&gtor_42,
&gtor_43,
&gtor_44,
&gtor_45,
&gtor_46,
&gtor_47,
&gtor_48,
&gtor_49,
&gtor_50,
&gtor_51,
&gtor_52,
&gtor_53,
&gtor_54,
&gtor_55,
&gtor_56,
&gtor_57,
&gtor_58,
&gtor_59,
&gtor_60,
&gtor_61,
&gtor_62,
};

unsigned num_vars_pth = 3;
const varinfo_t var_info_pth[] = {
{ 0, -1, "main" },
{ 0, 2, "pthr" },
{ 0, 2, "pthw" },
};

unsigned num_vars_mutex = 3;
const varinfo_t var_info_mutex[] = {
{ 0, -1, "m" },
{ 0, -1, "mr" },
{ 0, -1, "mw" },
};

unsigned num_vars_cond = 1;
const varinfo_t var_info_cond[] = {
{ 0, -1, "cv" },
};

unsigned num_gvars = 4;
const varinfo_t global_var_info[] = {
{ 1, -1, "rcount" },
{ 1, -1, "wcount" },
{ 1, -1, "nr" },
{ 1, -1, "nw" },
};

const varinfo_t local_var_info_0[] = {
{ 1, -1, "retval" },};

const varinfo_t local_var_info_2[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_3[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_5[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_6[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_7[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_9[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_10[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_11[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_13[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_14[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_15[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_16[] = {
{ 1, -1, "arg" },};

const varinfo_t local_var_info_19[] = {
{ 1, -1, "t28" },};

const varinfo_t local_var_info_20[] = {
{ 1, -1, "t28" },
{ 1, -1, "t29" },};

const varinfo_t local_var_info_22[] = {
{ 1, -1, "t30" },};

const varinfo_t local_var_info_26[] = {
{ 1, -1, "t32" },};

const varinfo_t local_var_info_29[] = {
{ 1, -1, "t33" },};

const varinfo_t local_var_info_31[] = {
{ 1, -1, "t34" },};

const varinfo_t local_var_info_34[] = {
{ 1, -1, "t35" },};

const varinfo_t local_var_info_38[] = {
{ 1, -1, "t31" },};

const varinfo_t local_var_info_41[] = {
{ 1, -1, "arg" },};

const varinfo_t local_var_info_44[] = {
{ 1, -1, "t22" },};

const varinfo_t local_var_info_46[] = {
{ 1, -1, "t23" },};

const varinfo_t local_var_info_50[] = {
{ 1, -1, "t25" },};

const varinfo_t local_var_info_53[] = {
{ 1, -1, "t26" },};

const varinfo_t local_var_info_56[] = {
{ 1, -1, "t27" },};

const varinfo_t local_var_info_60[] = {
{ 1, -1, "t24" },};

const local_varinfo_t local_var_info_table[] = {
{ 1, local_var_info_0 },
{ 0, NULL },
{ 1, local_var_info_2 },
{ 1, local_var_info_3 },
{ 0, NULL },
{ 1, local_var_info_5 },
{ 1, local_var_info_6 },
{ 1, local_var_info_7 },
{ 0, NULL },
{ 1, local_var_info_9 },
{ 1, local_var_info_10 },
{ 1, local_var_info_11 },
{ 0, NULL },
{ 1, local_var_info_13 },
{ 1, local_var_info_14 },
{ 1, local_var_info_15 },
{ 1, local_var_info_16 },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_19 },
{ 2, local_var_info_20 },
{ 0, NULL },
{ 1, local_var_info_22 },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_26 },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_29 },
{ 0, NULL },
{ 1, local_var_info_31 },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_34 },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_38 },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_41 },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_44 },
{ 0, NULL },
{ 1, local_var_info_46 },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_50 },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_53 },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_56 },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_60 },
{ 0, NULL },
{ 0, NULL },
};

int check_global_assertions(const void *gp)
{
int8_t g6 = *(int8_t *)(gp + 0);
int8_t g7 = *(int8_t *)(gp + 1);
int8_t g8 = *(int8_t *)(gp + 2);
int8_t g9 = *(int8_t *)(gp + 3);
if (!(((((g7) == (INT32_C(0)))) || (((g7) == (INT32_C(1))))))) return 20;
if (!(((((g6) == (INT32_C(0)))) || (((g7) == (INT32_C(0))))))) return 21;
return 0;
}

const char *source_line[] = {
"#include <stdio.h>",
"#include <stdbool.h>",
"#include <stdint.h>",
"#include <inttypes.h>",
"#include <pthread.h>",
"#include <assert.h>",
"",
"#define NR 2",
"#define NW 2",
"",
"pthread_t pthr[NR];",
"pthread_t pthw[NW];",
"pthread_cond_t cv = PTHREAD_COND_INITIALIZER;",
"pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;",
"pthread_mutex_t mr = PTHREAD_MUTEX_INITIALIZER;",
"pthread_mutex_t mw = PTHREAD_MUTEX_INITIALIZER;",
"int8_t rcount;",
"int8_t wcount;",
"",
"// @ASSERT(wcount == 0 || wcount == 1)",
"// @ASSERT(rcount == 0 || wcount == 0)",
"",
"int8_t nr;",
"int8_t nw;",
"",
"void rdlock(void)",
"{",
"\tpthread_mutex_lock(&m);",
"    while (wcount > 0) {",
"\t\tpthread_cond_wait(&cv, &m);",
"    }",
"    rcount++;",
"    pthread_mutex_unlock(&m);",
"}",
"",
"void rdunlock(void)",
"{",
"\tpthread_mutex_lock(&m);",
"    rcount--;",
"    pthread_cond_broadcast(&cv);",
"    pthread_mutex_unlock(&m);",
"}",
"",
"void wrlock(void)",
"{",
"\tpthread_mutex_lock(&m);",
"    while (wcount > 0 || rcount > 0) {",
"\t\tpthread_cond_wait(&cv, &m);",
"    }",
"    wcount++;",
"    pthread_mutex_unlock(&m);",
"}",
"",
"void wrunlock(void)",
"{",
"\tpthread_mutex_lock(&m);",
"    wcount--;",
"    pthread_cond_broadcast(&cv);",
"    pthread_mutex_unlock(&m);",
"}",
"",
"void *reader(void *arg)",
"{",
"    int8_t k = (int8_t)(intptr_t)arg;",
"    while (true) {",
"        rdlock();",
"",
"        pthread_mutex_lock(&mr);",
"        nr++;",
"        pthread_mutex_unlock(&mr);",
"",
"        printf(\"R%\" PRId8 \" nr=%\" PRId8 \" nw=%\" PRId8 \"\\n\", k, nr, nw);",
"",
"        assert(nw == 0);",
"//        assert(nr != 2);",
"",
"        pthread_mutex_lock(&mr);",
"        nr--;",
"        pthread_mutex_unlock(&mr);",
"",
"        rdunlock();",
"    }",
"    return NULL;",
"}",
"",
"void *writer(void *arg)",
"{",
"    int8_t k = (int8_t)(intptr_t)arg;",
"    while (true) {",
"        wrlock();",
"",
"        pthread_mutex_lock(&mw);",
"        nw++;",
"        pthread_mutex_unlock(&mw);",
"",
"        printf(\"W%\" PRId8 \" nr=%\" PRId8 \" nw=%\" PRId8 \"\\n\", k, nr, nw);",
"",
"        assert(nw == 1);",
"        assert(nr == 0);",
"",
"        pthread_mutex_lock(&mw);",
"        nw--;",
"        pthread_mutex_unlock(&mw);",
"",
"        wrunlock();",
"    }",
"    return NULL;",
"}",
"",
"int main(void)",
"{",
"    for (int i = 0; i < NR; ++i) {",
"        pthread_create(&pthr[i], NULL, &reader, (void *)(intptr_t)i);",
"    }",
"",
"    for (int i = 0; i < NW; ++i) {",
"        pthread_create(&pthw[i], NULL, &writer, (void *)(intptr_t)i);",
"    }",
"",
"    for (int i = 0; i < NR; ++i) {",
"        pthread_join(pthr[i], NULL);",
"    }",
"",
"    for (int i = 0; i < NW; ++i) {",
"        pthread_join(pthw[i], NULL);",
"    }",
"",
"\treturn 0;",
"}",
};
