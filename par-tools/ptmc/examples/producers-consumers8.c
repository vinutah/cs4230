#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>
#include <assert.h>

#define N   3
#define NP  3
#define NC  3

pthread_t pthp[NP];
pthread_t pthc[NC];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cv = PTHREAD_COND_INITIALIZER;
pthread_cond_t cv2 = PTHREAD_COND_INITIALIZER;
int count;

void *producer(void *arg)
{
	while (true) {
		pthread_mutex_lock(&mutex);
		while (count == N) {
			pthread_cond_wait(&cv, &mutex);
		}
		count++;
        if (count == 1) {
            pthread_cond_signal(&cv2);
        }
		pthread_mutex_unlock(&mutex);
	}
	return NULL;
}

void *consumer(void *arg)
{
	while (true) {
		pthread_mutex_lock(&mutex);
		while (count == 0) {
			pthread_cond_wait(&cv2, &mutex);
		}
		count--;
		if (count == 1) {
            pthread_cond_signal(&cv);
        }
		pthread_mutex_unlock(&mutex);
	}
	return NULL;
}

int main(void)
{
    int i;

    for (i = 0; i < NP; ++i) {
        pthread_create(&pthp[i], NULL, &producer, (void *)(intptr_t)i);
    }

    for (i = 0; i < NC; ++i) {
        pthread_create(&pthc[i], NULL, &consumer, (void *)(intptr_t)i);
    }

    for (i = 0; i < NP; ++i) {
        pthread_join(pthp[i], NULL);
    }

    for (i = 0; i < NC; ++i) {
        pthread_join(pthc[i], NULL);
    }
	return 0;
}
