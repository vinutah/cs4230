#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <limits.h>
#include <alloca.h>

typedef uint32_t bits_t;
typedef uint64_t bitvec_t;
typedef uint32_t id_t;
typedef uint32_t trans_t;
typedef struct context *context_t;

#define BV1         UINT64_C(1)
#define ID_NIL      UINT32_MAX
#define SID_OMEGA   0
#define PRIid       PRIu32
#define PRItrans    PRIx32

enum {
    TRANS_NIL,                  /* 0 */
    TRANS_ASSERT,               /* 1 */
    TRANS_TAU,                  /* 2 */
    TRANS_LOAD,                 /* 3 */
    TRANS_STORE,                /* 4 */
    TRANS_EXIT,                 /* 5 */
    TRANS_CREATE,               /* 6 */
    TRANS_JOIN,                 /* 7 */
    TRANS_LOCK,                 /* 8 */
    TRANS_UNLOCK,               /* 9 */
    TRANS_WAIT,                 /* 10 */
    TRANS_SIGNAL,               /* 11 */
    TRANS_BROADCAST,            /* 12 */
    TRANS_SPURIOUS,             /* 1 */
};

typedef struct thst thst_t;
struct thst {
  id_t sid;
  id_t cid;
  const void *lp;
};

typedef struct varinfo varinfo_t;
struct varinfo {
  int size;
  int dim;
  const char *name;
};

typedef struct local_varinfo local_varinfo_t;
struct local_varinfo {
  int n;
  const varinfo_t *varinfo;
};

typedef void (*pf_gtor_t)(context_t, bitvec_t, const void *, const thst_t [], int, int,
                          const id_t mtxv[]);

void assertion_violation(context_t, uint16_t);
void error_mutex_not_owner(context_t cc, int k, id_t mutex_id);

uint64_t *alloc_state(context_t);
void register_state(context_t, void *, trans_t, uint16_t);

uint64_t *enc_bv(uint64_t *sv, bitvec_t bv);
bits_t enc_thst(uint64_t *sv, bits_t bp, id_t sid, id_t cid);
bits_t enc_thst_v(uint64_t *sv, bits_t bp, const thst_t v[], int i, int j);
bits_t enc_thst_update(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, id_t cid);
bits_t enc_thst_create(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, int p, id_t sid0);
bits_t enc_thst_join(uint64_t *sv, bits_t bp,
					 const thst_t v[], int n, int k, id_t sid, int p);
bits_t enc_thst_signal(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, int p);
bits_t enc_thst_broadcast(uint64_t *sv, bits_t bp,
						  const thst_t v[], int n, int k, id_t sid, id_t cid);
bits_t enc_mtx(uint64_t *sv, bits_t bp, const id_t mtxv[]);
bits_t enc_mtx_update(uint64_t *sv, bits_t bp, const id_t mtxv[], id_t mid, id_t tid);
void *enc_align(uint64_t *sv, bits_t bp);
void *enc_genv(void *env, const void *gp);
void *enc_var(void *env, const void *p, unsigned size);
void *enc_val(void *env, uint64_t x, unsigned size);
void *enc_lenv(void *env, const thst_t v[], int i, int j);

static inline trans_t enc_trans(int32_t who, int32_t tag, int32_t arg0, int32_t arg1)
{
    return who | (tag << 8) | (arg0 << 16) | (arg1 << 24);
}
int ver_maj = 1;
int ver_min = 4;
int ver_mnt = 1;
const char model_filename[] = "producers-consumers5.c";

unsigned num_tid = 7;
unsigned num_sid = 34;
unsigned num_mid = 1;
unsigned num_cid = 1;
bits_t bits_tid = 3;
bits_t bits_tidx = 3;
bits_t bits_sid = 6;
bits_t bits_mid = 0;
bits_t bits_cid = 0;
unsigned size_global_env = 4;
unsigned size_local_env[] = {
1,
0,
4,
4,
0,
4,
4,
4,
0,
4,
4,
4,
0,
4,
4,
4,
1,
0,
0,
4,
0,
4,
0,
4,
0,
0,
1,
0,
0,
4,
0,
4,
0,
0,
};
void gtor_33(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 27, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 0, 0), 30);
}
}

void gtor_32(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t cond_id = 0;
int c = 0;
for (int i = 0; i < n; ++i) {
if (v[i].cid == cond_id) {
c++;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_signal(sv, bp, v, n, k, 33, i);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_SIGNAL, 0, 0), 29);
}
}
if (c == 0) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 33, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_SIGNAL, 0, 0), 29);
}
}
}

void gtor_31(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t t18 = *(int32_t *)(lp + 0);
{
int32_t t19 = ((t18) + (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 32, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int32_t *)(gp2 + 0) = t19;
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 0, 0), 27);
}
}

void gtor_30(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 28, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 24);
}
}
}

void gtor_29(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t t17 = *(int32_t *)(lp + 0);
if (((t17) == (INT32_C(3)))) {
id_t cond_id = 0;
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 30, cond_id);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_WAIT, 0, 0), 22);
}
if ((! (((t17) == (INT32_C(3)))))) {
int32_t t18 = *(int32_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 31, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t18, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 22);
}
}

void gtor_28(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int32_t t17 = *(int32_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 29, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t17, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 22);
}
}

void gtor_27(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 28, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 21);
}
}
}

void gtor_26(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t a8 = *(int8_t *)(lp + 0);
int32_t x9 = a8;
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 27, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 20);
}
}

void gtor_25(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 17, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 0, 0), 50);
}
}

void gtor_24(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t cond_id = 0;
int c = 0;
for (int i = 0; i < n; ++i) {
if (v[i].cid == cond_id) {
c++;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_signal(sv, bp, v, n, k, 25, i);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_SIGNAL, 0, 0), 48);
}
}
if (c == 0) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 25, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_SIGNAL, 0, 0), 48);
}
}
}

void gtor_23(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t t16 = *(int32_t *)(lp + 0);
if ((! (((t16) == (INT32_C(2)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 25, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 47);
}
if (((t16) == (INT32_C(2)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 24, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 47);
}
}

void gtor_22(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int32_t t16 = *(int32_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 23, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t16, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 47);
}
}

void gtor_21(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t t15 = *(int32_t *)(lp + 0);
{
int32_t t20 = ((t15) - (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 22, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int32_t *)(gp2 + 0) = t20;
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 0, 0), 45);
}
}

void gtor_20(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 18, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 42);
}
}
}

void gtor_19(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t t14 = *(int32_t *)(lp + 0);
if (((t14) == (INT32_C(0)))) {
id_t cond_id = 0;
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 20, cond_id);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_WAIT, 0, 0), 40);
}
if ((! (((t14) == (INT32_C(0)))))) {
int32_t t15 = *(int32_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 21, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t15, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 40);
}
}

void gtor_18(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int32_t t14 = *(int32_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 19, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t14, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 40);
}
}

void gtor_17(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 18, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 39);
}
}
}

void gtor_16(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t a6 = *(int8_t *)(lp + 0);
int32_t x7 = a6;
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 17, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 38);
}
}

void gtor_15(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = *(int32_t *)(lp + 0);
x5 = ((x5) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 14, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 71);
}
}

void gtor_14(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = *(int32_t *)(lp + 0);
if (((x5) < (INT32_C(3)))) {
uint8_t i = x5;
uint8_t p = 4 + i;
if (v[p].sid == SID_OMEGA) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv & ~(BV1 << p));
bp = enc_thst_join(sv, bp, v, n, k, 15, p);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p + 1, n);
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p + 1, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_JOIN, 4 + i, 0), 71);
}
}
if ((! (((x5) < (INT32_C(3)))))) {
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, SID_OMEGA, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_EXIT, 0, 0), 71);
}
}

void gtor_13(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = *(int32_t *)(lp + 0);
x5 = ((x5) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 10, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 67);
}
}

void gtor_12(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 14, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 71);
}
}

void gtor_11(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = *(int32_t *)(lp + 0);
{
uint8_t i = x5;
uint8_t p = 1 + i;
if (v[p].sid == SID_OMEGA) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv & ~(BV1 << p));
bp = enc_thst_join(sv, bp, v, n, k, 13, p);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p + 1, n);
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p + 1, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_JOIN, 1 + i, 0), 68);
}
}
}

void gtor_10(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = *(int32_t *)(lp + 0);
if (((x5) < (INT32_C(3)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 11, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 67);
}
if ((! (((x5) < (INT32_C(3)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 12, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 67);
}
}

void gtor_9(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = *(int32_t *)(lp + 0);
x5 = ((x5) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 6, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 63);
}
}

void gtor_8(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 10, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 67);
}
}

void gtor_7(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = *(int32_t *)(lp + 0);
{
uint8_t i = x5;
uint8_t p = 4 + i;
int8_t tmp = x5;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv | (BV1 << p));
bp = enc_thst_create(sv, bp, v, n, k, 9, p, 16);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
if (p < n) {
r = enc_lenv(r, v, k + 1, p);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, p, n);
} else {
r = enc_lenv(r, v, k + 1, n);
r = enc_var(r, &tmp, 1);
}
} else {
r = enc_lenv(r, v, 0, p);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, p, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_CREATE, 4 + i, 0), 64);
}
}

void gtor_6(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = *(int32_t *)(lp + 0);
if (((x5) < (INT32_C(3)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 7, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 63);
}
if ((! (((x5) < (INT32_C(3)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 8, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 63);
}
}

void gtor_5(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = *(int32_t *)(lp + 0);
x5 = ((x5) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 2, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 59);
}
}

void gtor_4(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 6, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 63);
}
}

void gtor_3(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = *(int32_t *)(lp + 0);
{
uint8_t i = x5;
uint8_t p = 1 + i;
int8_t tmp = x5;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv | (BV1 << p));
bp = enc_thst_create(sv, bp, v, n, k, 5, p, 26);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
if (p < n) {
r = enc_lenv(r, v, k + 1, p);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, p, n);
} else {
r = enc_lenv(r, v, k + 1, n);
r = enc_var(r, &tmp, 1);
}
} else {
r = enc_lenv(r, v, 0, p);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, p, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_CREATE, 1 + i, 0), 60);
}
}

void gtor_2(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = *(int32_t *)(lp + 0);
if (((x5) < (INT32_C(3)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 3, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 59);
}
if ((! (((x5) < (INT32_C(3)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 4, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 59);
}
}

void gtor_1(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x5 = 0;
x5 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 2, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x5, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 59);
}
}

void gtor_0(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t retval = *(int8_t *)(lp + 0);
}

pf_gtor_t gtor_table[] = {
&gtor_0,
&gtor_1,
&gtor_2,
&gtor_3,
&gtor_4,
&gtor_5,
&gtor_6,
&gtor_7,
&gtor_8,
&gtor_9,
&gtor_10,
&gtor_11,
&gtor_12,
&gtor_13,
&gtor_14,
&gtor_15,
&gtor_16,
&gtor_17,
&gtor_18,
&gtor_19,
&gtor_20,
&gtor_21,
&gtor_22,
&gtor_23,
&gtor_24,
&gtor_25,
&gtor_26,
&gtor_27,
&gtor_28,
&gtor_29,
&gtor_30,
&gtor_31,
&gtor_32,
&gtor_33,
};

unsigned num_vars_pth = 3;
const varinfo_t var_info_pth[] = {
{ 0, -1, "main" },
{ 0, 3, "pthp" },
{ 0, 3, "pthc" },
};

unsigned num_vars_mutex = 1;
const varinfo_t var_info_mutex[] = {
{ 0, -1, "mutex" },
};

unsigned num_vars_cond = 1;
const varinfo_t var_info_cond[] = {
{ 0, -1, "cv" },
};

unsigned num_gvars = 1;
const varinfo_t global_var_info[] = {
{ 4, -1, "count" },
};

const varinfo_t local_var_info_0[] = {
{ 1, -1, "retval" },};

const varinfo_t local_var_info_2[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_3[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_5[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_6[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_7[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_9[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_10[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_11[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_13[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_14[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_15[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_16[] = {
{ 1, -1, "arg" },};

const varinfo_t local_var_info_19[] = {
{ 4, -1, "t14" },};

const varinfo_t local_var_info_21[] = {
{ 4, -1, "t15" },};

const varinfo_t local_var_info_23[] = {
{ 4, -1, "t16" },};

const varinfo_t local_var_info_26[] = {
{ 1, -1, "arg" },};

const varinfo_t local_var_info_29[] = {
{ 4, -1, "t17" },};

const varinfo_t local_var_info_31[] = {
{ 4, -1, "t18" },};

const local_varinfo_t local_var_info_table[] = {
{ 1, local_var_info_0 },
{ 0, NULL },
{ 1, local_var_info_2 },
{ 1, local_var_info_3 },
{ 0, NULL },
{ 1, local_var_info_5 },
{ 1, local_var_info_6 },
{ 1, local_var_info_7 },
{ 0, NULL },
{ 1, local_var_info_9 },
{ 1, local_var_info_10 },
{ 1, local_var_info_11 },
{ 0, NULL },
{ 1, local_var_info_13 },
{ 1, local_var_info_14 },
{ 1, local_var_info_15 },
{ 1, local_var_info_16 },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_19 },
{ 0, NULL },
{ 1, local_var_info_21 },
{ 0, NULL },
{ 1, local_var_info_23 },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_26 },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_29 },
{ 0, NULL },
{ 1, local_var_info_31 },
{ 0, NULL },
{ 0, NULL },
};

int check_global_assertions(const void *gp)
{
int32_t g4 = *(int32_t *)(gp + 0);
return 0;
}

const char *source_line[] = {
"#include <stdio.h>",
"#include <stdbool.h>",
"#include <stdint.h>",
"#include <pthread.h>",
"#include <assert.h>",
"",
"#define N   3",
"#define NP  3",
"#define NC  3",
"",
"pthread_t pthp[NP];",
"pthread_t pthc[NC];",
"pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;",
"pthread_cond_t cv = PTHREAD_COND_INITIALIZER;",
"int count;",
"",
"void *producer(void *arg)",
"{",
"\tint id = (int)arg;",
"\twhile (true) {",
"\t\tpthread_mutex_lock(&mutex);",
"\t\twhile (count == N) {",
"\t\t\tprintf(\"producer %d wait\\n\", id);",
"\t\t\tpthread_cond_wait(&cv, &mutex);",
"\t\t\tprintf(\"producer %d up\\n\", id);",
"\t\t}",
"\t\tcount++;",
"\t\tprintf(\"producer %d %d\\n\", id, count);",
"\t\tpthread_cond_signal(&cv);",
"\t\tpthread_mutex_unlock(&mutex);",
"\t}",
"\treturn NULL;",
"}",
"",
"void *consumer(void *arg)",
"{",
"\tint id = (int)arg;",
"\twhile (true) {",
"\t\tpthread_mutex_lock(&mutex);",
"\t\twhile (count == 0) {",
"\t\t\tprintf(\"consumer %d wait\\n\", id);",
"\t\t\tpthread_cond_wait(&cv, &mutex);",
"\t\t\tprintf(\"consumer %d up\\n\", id);",
"\t\t}",
"\t\tcount--;",
"\t\tprintf(\"consumer %d %d\\n\", id, count);",
"        if (count == N - 1) {",
"            pthread_cond_signal(&cv);",
"        }",
"\t\tpthread_mutex_unlock(&mutex);",
"\t}",
"\treturn NULL;",
"}",
"",
"int main(void)",
"{",
"    int i;",
"",
"    for (i = 0; i < NP; ++i) {",
"        pthread_create(&pthp[i], NULL, &producer, (void *)(intptr_t)i);",
"    }",
"",
"    for (i = 0; i < NC; ++i) {",
"        pthread_create(&pthc[i], NULL, &consumer, (void *)(intptr_t)i);",
"    }",
"",
"    for (i = 0; i < NP; ++i) {",
"        pthread_join(pthp[i], NULL);",
"    }",
"",
"    for (i = 0; i < NC; ++i) {",
"        pthread_join(pthc[i], NULL);",
"    }",
"\treturn 0;",
"}",
};
