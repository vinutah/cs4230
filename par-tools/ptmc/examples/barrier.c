#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>
#include <assert.h>

#define N 4

pthread_t pth[N];
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t m2 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cv = PTHREAD_COND_INITIALIZER;
int count;
int a;
int b;

// @ASSERT(b == 0 || a == N)

void barrier(void)
{
    pthread_mutex_lock(&m);
    count++;
    if (count == N) {
        pthread_cond_broadcast(&cv); // ### spurious wakeups?
    } else {
        pthread_cond_wait(&cv, &m);
    }
    pthread_mutex_unlock(&m);
}

void *thread(void *arg)
{
    pthread_mutex_lock(&m2);
    a++;
    pthread_mutex_unlock(&m2);

    barrier();

    pthread_mutex_lock(&m2);
    b++;
    pthread_mutex_unlock(&m2);

    return NULL;
}

int main(void)
{
    for (int i = 0; i < N; ++i) {
        pthread_create(&pth[i], NULL, &thread, NULL);
    }
    for (int i = 0; i < N; ++i) {
        pthread_join(pth[i], NULL);
    }
    return 0;
}
