#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h>
#include <assert.h>

#define NR 2
#define NW 2

pthread_t pthr[NR];
pthread_t pthw[NW];
pthread_cond_t cv = PTHREAD_COND_INITIALIZER;
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mr = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mw = PTHREAD_MUTEX_INITIALIZER;
int8_t rcount;
int8_t wcount;

// @ASSERT(wcount == 0 || wcount == 1)
// @ASSERT(rcount == 0 || wcount == 0)

int8_t nr;
int8_t nw;

void rdlock(void)
{
	pthread_mutex_lock(&m);
    while (wcount > 0) {
		pthread_cond_wait(&cv, &m);
    }
    rcount++;
    pthread_mutex_unlock(&m);
}

void rdunlock(void)
{
	pthread_mutex_lock(&m);
    rcount--;
    pthread_cond_broadcast(&cv);
    pthread_mutex_unlock(&m);
}

void wrlock(void)
{
	pthread_mutex_lock(&m);
    while (wcount > 0 || rcount > 0) {
		pthread_cond_wait(&cv, &m);
    }
    wcount++;
    pthread_mutex_unlock(&m);
}

void wrunlock(void)
{
	pthread_mutex_lock(&m);
    wcount--;
    pthread_cond_broadcast(&cv);
    pthread_mutex_unlock(&m);
}

void *reader(void *arg)
{
    int8_t k = (int8_t)(intptr_t)arg;
    while (true) {
        rdlock();

        pthread_mutex_lock(&mr);
        nr++;
        pthread_mutex_unlock(&mr);

        printf("R%" PRId8 " nr=%" PRId8 " nw=%" PRId8 "\n", k, nr, nw);

        assert(nw == 0);
//        assert(nr != 2);

        pthread_mutex_lock(&mr);
        nr--;
        pthread_mutex_unlock(&mr);

        rdunlock();
    }
    return NULL;
}

void *writer(void *arg)
{
    int8_t k = (int8_t)(intptr_t)arg;
    while (true) {
        wrlock();

        pthread_mutex_lock(&mw);
        nw++;
        pthread_mutex_unlock(&mw);

        printf("W%" PRId8 " nr=%" PRId8 " nw=%" PRId8 "\n", k, nr, nw);

        assert(nw == 1);
        assert(nr == 0);

        pthread_mutex_lock(&mw);
        nw--;
        pthread_mutex_unlock(&mw);

        wrunlock();
    }
    return NULL;
}

int main(void)
{
    for (int i = 0; i < NR; ++i) {
        pthread_create(&pthr[i], NULL, &reader, (void *)(intptr_t)i);
    }

    for (int i = 0; i < NW; ++i) {
        pthread_create(&pthw[i], NULL, &writer, (void *)(intptr_t)i);
    }

    for (int i = 0; i < NR; ++i) {
        pthread_join(pthr[i], NULL);
    }

    for (int i = 0; i < NW; ++i) {
        pthread_join(pthw[i], NULL);
    }

	return 0;
}
