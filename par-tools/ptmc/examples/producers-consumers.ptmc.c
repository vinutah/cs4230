#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <limits.h>
#include <alloca.h>

typedef uint32_t bits_t;
typedef uint64_t bitvec_t;
typedef uint32_t id_t;
typedef uint32_t trans_t;
typedef struct context *context_t;

#define BV1         UINT64_C(1)
#define ID_NIL      UINT32_MAX
#define SID_OMEGA   0
#define PRIid       PRIu32
#define PRItrans    PRIx32

enum {
    TRANS_NIL,                  /* 0 */
    TRANS_ASSERT,               /* 1 */
    TRANS_TAU,                  /* 2 */
    TRANS_LOAD,                 /* 3 */
    TRANS_STORE,                /* 4 */
    TRANS_EXIT,                 /* 5 */
    TRANS_CREATE,               /* 6 */
    TRANS_JOIN,                 /* 7 */
    TRANS_LOCK,                 /* 8 */
    TRANS_UNLOCK,               /* 9 */
    TRANS_WAIT,                 /* 10 */
    TRANS_SIGNAL,               /* 11 */
    TRANS_BROADCAST,            /* 12 */
    TRANS_SPURIOUS,             /* 1 */
};

typedef struct thst thst_t;
struct thst {
  id_t sid;
  id_t cid;
  const void *lp;
};

typedef struct varinfo varinfo_t;
struct varinfo {
  int size;
  int dim;
  const char *name;
};

typedef struct local_varinfo local_varinfo_t;
struct local_varinfo {
  int n;
  const varinfo_t *varinfo;
};

typedef void (*pf_gtor_t)(context_t, bitvec_t, const void *, const thst_t [], int, int,
                          const id_t mtxv[]);

void assertion_violation(context_t, uint16_t);
void error_mutex_not_owner(context_t cc, int k, id_t mutex_id);

uint64_t *alloc_state(context_t);
void register_state(context_t, void *, trans_t, uint16_t);

uint64_t *enc_bv(uint64_t *sv, bitvec_t bv);
bits_t enc_thst(uint64_t *sv, bits_t bp, id_t sid, id_t cid);
bits_t enc_thst_v(uint64_t *sv, bits_t bp, const thst_t v[], int i, int j);
bits_t enc_thst_update(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, id_t cid);
bits_t enc_thst_create(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, int p, id_t sid0);
bits_t enc_thst_join(uint64_t *sv, bits_t bp,
					 const thst_t v[], int n, int k, id_t sid, int p);
bits_t enc_thst_signal(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, int p);
bits_t enc_thst_broadcast(uint64_t *sv, bits_t bp,
						  const thst_t v[], int n, int k, id_t sid, id_t cid);
bits_t enc_mtx(uint64_t *sv, bits_t bp, const id_t mtxv[]);
bits_t enc_mtx_update(uint64_t *sv, bits_t bp, const id_t mtxv[], id_t mid, id_t tid);
void *enc_align(uint64_t *sv, bits_t bp);
void *enc_genv(void *env, const void *gp);
void *enc_var(void *env, const void *p, unsigned size);
void *enc_val(void *env, uint64_t x, unsigned size);
void *enc_lenv(void *env, const thst_t v[], int i, int j);

static inline trans_t enc_trans(int32_t who, int32_t tag, int32_t arg0, int32_t arg1)
{
    return who | (tag << 8) | (arg0 << 16) | (arg1 << 24);
}
int ver_maj = 1;
int ver_min = 4;
int ver_mnt = 1;
const char model_filename[] = "producers-consumers.c";

unsigned num_tid = 9;
unsigned num_sid = 30;
unsigned num_mid = 1;
unsigned num_cid = 2;
bits_t bits_tid = 4;
bits_t bits_tidx = 4;
bits_t bits_sid = 5;
bits_t bits_mid = 0;
bits_t bits_cid = 1;
unsigned size_global_env = 1;
unsigned size_local_env[] = {
1,
0,
4,
4,
0,
4,
4,
4,
0,
4,
4,
4,
0,
4,
4,
4,
0,
0,
1,
0,
1,
0,
0,
0,
0,
1,
0,
1,
0,
0,
};
void gtor_29(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 23, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 0, 0), 27);
}
}

void gtor_28(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t cond_id = 1;
int c = 0;
for (int i = 0; i < n; ++i) {
if (v[i].cid == cond_id) {
c++;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_signal(sv, bp, v, n, k, 29, i);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_SIGNAL, 1, 0), 26);
}
}
if (c == 0) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 29, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_SIGNAL, 1, 0), 26);
}
}
}

void gtor_27(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t19 = *(int8_t *)(lp + 0);
{
int8_t t20 = ((t19) + (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 28, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 0) = t20;
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 0, 0), 25);
}
}

void gtor_26(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 24, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 23);
}
}
}

void gtor_25(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t18 = *(int8_t *)(lp + 0);
if (((t18) == (INT32_C(4)))) {
id_t cond_id = 0;
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 26, cond_id);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_WAIT, 0, 0), 22);
}
if ((! (((t18) == (INT32_C(4)))))) {
int8_t t19 = *(int8_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 27, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t19, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 22);
}
}

void gtor_24(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t t18 = *(int8_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 25, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t18, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 22);
}
}

void gtor_23(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 24, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 21);
}
}
}

void gtor_22(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 16, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_UNLOCK, 0, 0), 41);
}
}

void gtor_21(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t cond_id = 0;
int c = 0;
for (int i = 0; i < n; ++i) {
if (v[i].cid == cond_id) {
c++;
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_signal(sv, bp, v, n, k, 22, i);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_SIGNAL, 0, 0), 40);
}
}
if (c == 0) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 22, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_SIGNAL, 0, 0), 40);
}
}
}

void gtor_20(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t17 = *(int8_t *)(lp + 0);
{
int8_t t21 = ((t17) - (INT32_C(1)));
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 21, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *gp2 = enc_align(sv, bp);
void *r = enc_genv(gp2, gp);
*(int8_t *)(gp2 + 0) = t21;
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_STORE, 0, 0), 39);
}
}

void gtor_19(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 17, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 37);
}
}
}

void gtor_18(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t t16 = *(int8_t *)(lp + 0);
if (((t16) == (INT32_C(0)))) {
id_t cond_id = 1;
id_t mutex_id = 0;
if (mtxv[mutex_id] != k) error_mutex_not_owner(cc, k, mutex_id);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 19, cond_id);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, ID_NIL);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_WAIT, 1, 0), 36);
}
if ((! (((t16) == (INT32_C(0)))))) {
int8_t t17 = *(int8_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 20, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t17, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 36);
}
}

void gtor_17(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t t16 = *(int8_t *)(gp + 0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 18, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &t16, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOAD, 0, 0), 36);
}
}

void gtor_16(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
id_t mutex_id = 0;
if (mtxv[mutex_id] == ID_NIL && v[k].cid == ID_NIL) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 17, ID_NIL);
bp = enc_mtx_update(sv, bp, mtxv, mutex_id, k);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_LOCK, 0, 0), 35);
}
}
}

void gtor_15(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x9 = *(int32_t *)(lp + 0);
x9 = ((x9) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 14, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x9, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 57);
}
}

void gtor_14(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x9 = *(int32_t *)(lp + 0);
if (((x9) < (INT32_C(4)))) {
uint8_t i = x9;
uint8_t p = 5 + i;
if (v[p].sid == SID_OMEGA) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv & ~(BV1 << p));
bp = enc_thst_join(sv, bp, v, n, k, 15, p);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x9, 4);
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p + 1, n);
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p + 1, k);
r = enc_var(r, &x9, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_JOIN, 5 + i, 0), 57);
}
}
if ((! (((x9) < (INT32_C(4)))))) {
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, SID_OMEGA, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_EXIT, 0, 0), 57);
}
}

void gtor_13(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x8 = *(int32_t *)(lp + 0);
x8 = ((x8) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 10, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x8, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 54);
}
}

void gtor_12(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x9 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 14, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x9, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 57);
}
}

void gtor_11(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x8 = *(int32_t *)(lp + 0);
{
uint8_t i = x8;
uint8_t p = 1 + i;
if (v[p].sid == SID_OMEGA) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv & ~(BV1 << p));
bp = enc_thst_join(sv, bp, v, n, k, 13, p);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x8, 4);
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p + 1, n);
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p + 1, k);
r = enc_var(r, &x8, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_JOIN, 1 + i, 0), 55);
}
}
}

void gtor_10(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x8 = *(int32_t *)(lp + 0);
if (((x8) < (INT32_C(4)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 11, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x8, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 54);
}
if ((! (((x8) < (INT32_C(4)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 12, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 54);
}
}

void gtor_9(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x7 = *(int32_t *)(lp + 0);
x7 = ((x7) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 6, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x7, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 51);
}
}

void gtor_8(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x8 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 10, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x8, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 54);
}
}

void gtor_7(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x7 = *(int32_t *)(lp + 0);
{
uint8_t i = x7;
uint8_t p = 5 + i;
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv | (BV1 << p));
bp = enc_thst_create(sv, bp, v, n, k, 9, p, 16);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x7, 4);
if (p < n) {
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p, n);
} else {
r = enc_lenv(r, v, k + 1, n);
}
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p, k);
r = enc_var(r, &x7, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_CREATE, 5 + i, 0), 52);
}
}

void gtor_6(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x7 = *(int32_t *)(lp + 0);
if (((x7) < (INT32_C(4)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 7, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x7, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 51);
}
if ((! (((x7) < (INT32_C(4)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 8, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 51);
}
}

void gtor_5(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x6 = *(int32_t *)(lp + 0);
x6 = ((x6) + (INT32_C(1)));
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 2, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x6, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 48);
}
}

void gtor_4(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x7 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 6, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x7, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 51);
}
}

void gtor_3(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x6 = *(int32_t *)(lp + 0);
{
uint8_t i = x6;
uint8_t p = 1 + i;
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv | (BV1 << p));
bp = enc_thst_create(sv, bp, v, n, k, 5, p, 23);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
if (k < p) {
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x6, 4);
if (p < n) {
r = enc_lenv(r, v, k + 1, p);
r = enc_lenv(r, v, p, n);
} else {
r = enc_lenv(r, v, k + 1, n);
}
} else {
r = enc_lenv(r, v, 0, p);
r = enc_lenv(r, v, p, k);
r = enc_var(r, &x6, 4);
r = enc_lenv(r, v, k + 1, n);
}
register_state(cc, r, enc_trans(k, TRANS_CREATE, 1 + i, 0), 49);
}
}

void gtor_2(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x6 = *(int32_t *)(lp + 0);
if (((x6) < (INT32_C(4)))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 3, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x6, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 48);
}
if ((! (((x6) < (INT32_C(4)))))) {
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 4, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 48);
}
}

void gtor_1(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x6 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 2, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x6, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 48);
}
}

void gtor_0(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t retval = *(int8_t *)(lp + 0);
}

pf_gtor_t gtor_table[] = {
&gtor_0,
&gtor_1,
&gtor_2,
&gtor_3,
&gtor_4,
&gtor_5,
&gtor_6,
&gtor_7,
&gtor_8,
&gtor_9,
&gtor_10,
&gtor_11,
&gtor_12,
&gtor_13,
&gtor_14,
&gtor_15,
&gtor_16,
&gtor_17,
&gtor_18,
&gtor_19,
&gtor_20,
&gtor_21,
&gtor_22,
&gtor_23,
&gtor_24,
&gtor_25,
&gtor_26,
&gtor_27,
&gtor_28,
&gtor_29,
};

unsigned num_vars_pth = 3;
const varinfo_t var_info_pth[] = {
{ 0, -1, "main" },
{ 0, 4, "pthp" },
{ 0, 4, "pthc" },
};

unsigned num_vars_mutex = 1;
const varinfo_t var_info_mutex[] = {
{ 0, -1, "mutex" },
};

unsigned num_vars_cond = 2;
const varinfo_t var_info_cond[] = {
{ 0, -1, "cv" },
{ 0, -1, "cv2" },
};

unsigned num_gvars = 1;
const varinfo_t global_var_info[] = {
{ 1, -1, "count" },
};

const varinfo_t local_var_info_0[] = {
{ 1, -1, "retval" },};

const varinfo_t local_var_info_2[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_3[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_5[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_6[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_7[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_9[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_10[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_11[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_13[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_14[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_15[] = {
{ 4, -1, "i" },};

const varinfo_t local_var_info_18[] = {
{ 1, -1, "t16" },};

const varinfo_t local_var_info_20[] = {
{ 1, -1, "t17" },};

const varinfo_t local_var_info_25[] = {
{ 1, -1, "t18" },};

const varinfo_t local_var_info_27[] = {
{ 1, -1, "t19" },};

const local_varinfo_t local_var_info_table[] = {
{ 1, local_var_info_0 },
{ 0, NULL },
{ 1, local_var_info_2 },
{ 1, local_var_info_3 },
{ 0, NULL },
{ 1, local_var_info_5 },
{ 1, local_var_info_6 },
{ 1, local_var_info_7 },
{ 0, NULL },
{ 1, local_var_info_9 },
{ 1, local_var_info_10 },
{ 1, local_var_info_11 },
{ 0, NULL },
{ 1, local_var_info_13 },
{ 1, local_var_info_14 },
{ 1, local_var_info_15 },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_18 },
{ 0, NULL },
{ 1, local_var_info_20 },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_25 },
{ 0, NULL },
{ 1, local_var_info_27 },
{ 0, NULL },
{ 0, NULL },
};

int check_global_assertions(const void *gp)
{
int8_t g5 = *(int8_t *)(gp + 0);
return 0;
}

const char *source_line[] = {
"#include <stdio.h>",
"#include <stdbool.h>",
"#include <stdint.h>",
"#include <pthread.h>",
"#include <assert.h>",
"",
"#define N   4",
"#define NP  4",
"#define NC  4",
"",
"pthread_t pthp[NP];",
"pthread_t pthc[NC];",
"pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;",
"pthread_cond_t cv = PTHREAD_COND_INITIALIZER;",
"pthread_cond_t cv2 = PTHREAD_COND_INITIALIZER;",
"int8_t count;",
"",
"void *producer(void *arg)",
"{",
"    while (true) {",
"        pthread_mutex_lock(&mutex);",
"        while (count == N) {",
"            pthread_cond_wait(&cv, &mutex);",
"        }",
"        count++;",
"        pthread_cond_signal(&cv2);",
"        pthread_mutex_unlock(&mutex);",
"    }",
"    return NULL;",
"}",
"",
"void *consumer(void *arg)",
"{",
"    while (true) {",
"        pthread_mutex_lock(&mutex);",
"        while (count == 0) {",
"            pthread_cond_wait(&cv2, &mutex);",
"        }",
"        count--;",
"        pthread_cond_signal(&cv);",
"        pthread_mutex_unlock(&mutex);",
"    }",
"    return NULL;",
"}",
"",
"int main(void)",
"{",
"    for (int i = 0; i < NP; ++i) {",
"        pthread_create(&pthp[i], NULL, &producer, NULL);",
"    }",
"    for (int i = 0; i < NC; ++i) {",
"        pthread_create(&pthc[i], NULL, &consumer, NULL);",
"    }",
"    for (int i = 0; i < NP; ++i) {",
"        pthread_join(pthp[i], NULL);",
"    }",
"    for (int i = 0; i < NC; ++i) {",
"        pthread_join(pthc[i], NULL);",
"    }",
"    return 0;",
"}",
};
