#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <limits.h>
#include <alloca.h>

typedef uint32_t bits_t;
typedef uint64_t bitvec_t;
typedef uint32_t id_t;
typedef uint32_t trans_t;
typedef struct context *context_t;

#define BV1         UINT64_C(1)
#define ID_NIL      UINT32_MAX
#define SID_OMEGA   0
#define PRIid       PRIu32
#define PRItrans    PRIx32

enum {
    TRANS_NIL,                  /* 0 */
    TRANS_ASSERT,               /* 1 */
    TRANS_TAU,                  /* 2 */
    TRANS_LOAD,                 /* 3 */
    TRANS_STORE,                /* 4 */
    TRANS_EXIT,                 /* 5 */
    TRANS_CREATE,               /* 6 */
    TRANS_JOIN,                 /* 7 */
    TRANS_LOCK,                 /* 8 */
    TRANS_UNLOCK,               /* 9 */
    TRANS_WAIT,                 /* 10 */
    TRANS_SIGNAL,               /* 11 */
    TRANS_BROADCAST,            /* 12 */
    TRANS_SPURIOUS,             /* 1 */
};

typedef struct thst thst_t;
struct thst {
  id_t sid;
  id_t cid;
  const void *lp;
};

typedef struct varinfo varinfo_t;
struct varinfo {
  int size;
  int dim;
  const char *name;
};

typedef struct local_varinfo local_varinfo_t;
struct local_varinfo {
  int n;
  const varinfo_t *varinfo;
};

typedef void (*pf_gtor_t)(context_t, bitvec_t, const void *, const thst_t [], int, int,
                          const id_t mtxv[]);

void assertion_violation(context_t, uint16_t);
void error_mutex_not_owner(context_t cc, int k, id_t mutex_id);

uint64_t *alloc_state(context_t);
void register_state(context_t, void *, trans_t, uint16_t);

uint64_t *enc_bv(uint64_t *sv, bitvec_t bv);
bits_t enc_thst(uint64_t *sv, bits_t bp, id_t sid, id_t cid);
bits_t enc_thst_v(uint64_t *sv, bits_t bp, const thst_t v[], int i, int j);
bits_t enc_thst_update(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, id_t cid);
bits_t enc_thst_create(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, int p, id_t sid0);
bits_t enc_thst_join(uint64_t *sv, bits_t bp,
					 const thst_t v[], int n, int k, id_t sid, int p);
bits_t enc_thst_signal(uint64_t *sv, bits_t bp,
					   const thst_t v[], int n, int k, id_t sid, int p);
bits_t enc_thst_broadcast(uint64_t *sv, bits_t bp,
						  const thst_t v[], int n, int k, id_t sid, id_t cid);
bits_t enc_mtx(uint64_t *sv, bits_t bp, const id_t mtxv[]);
bits_t enc_mtx_update(uint64_t *sv, bits_t bp, const id_t mtxv[], id_t mid, id_t tid);
void *enc_align(uint64_t *sv, bits_t bp);
void *enc_genv(void *env, const void *gp);
void *enc_var(void *env, const void *p, unsigned size);
void *enc_val(void *env, uint64_t x, unsigned size);
void *enc_lenv(void *env, const thst_t v[], int i, int j);

static inline trans_t enc_trans(int32_t who, int32_t tag, int32_t arg0, int32_t arg1)
{
    return who | (tag << 8) | (arg0 << 16) | (arg1 << 24);
}
int ver_maj = 1;
int ver_min = 4;
int ver_mnt = 1;
const char model_filename[] = "amb1.c";

unsigned num_tid = 1;
unsigned num_sid = 6;
unsigned num_mid = 0;
unsigned num_cid = 0;
bits_t bits_tid = 0;
bits_t bits_tidx = 1;
bits_t bits_sid = 3;
bits_t bits_mid = 0;
bits_t bits_cid = 0;
unsigned size_global_env = 0;
unsigned size_local_env[] = {
1,
0,
0,
0,
4,
0,
};
void gtor_5(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
{
int8_t tmp = INT32_C(0);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, SID_OMEGA, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &tmp, 1);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_EXIT, 0, 0), 13);
}
}

void gtor_4(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x0 = *(int32_t *)(lp + 0);
{
if (!(((((x0) == (INT32_C(0)))) || (((x0) == (INT32_C(1))))))) assertion_violation(cc, 12);
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 5, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_ASSERT, 0, 0), 12);
}
}

void gtor_3(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x0 = INT32_C(1);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 4, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x0, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 0);
}
}

void gtor_2(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x0 = INT32_C(0);
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 4, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_var(r, &x0, 4);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 0);
}
}

void gtor_1(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int32_t x0 = 0;
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 3, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 7);
}
{
uint64_t *sv = alloc_state(cc);
bits_t bp = 0;
sv = enc_bv(sv, bv);
bp = enc_thst_update(sv, bp, v, n, k, 2, ID_NIL);
bp = enc_mtx(sv, bp, mtxv);
void *r = enc_align(sv, bp);
r = enc_genv(r, gp);
r = enc_lenv(r, v, 0, k);
r = enc_lenv(r, v, k + 1, n);
register_state(cc, r, enc_trans(k, TRANS_TAU, 0, 0), 7);
}
}

void gtor_0(context_t cc, bitvec_t bv, const void *gp, const thst_t v[], int n, int k, const id_t mtxv[])
{
const void *lp = v[k].lp;
int8_t retval = *(int8_t *)(lp + 0);
}

pf_gtor_t gtor_table[] = {
&gtor_0,
&gtor_1,
&gtor_2,
&gtor_3,
&gtor_4,
&gtor_5,
};

unsigned num_vars_pth = 1;
const varinfo_t var_info_pth[] = {
{ 0, -1, "main" },
};

unsigned num_vars_mutex = 0;
const varinfo_t var_info_mutex[] = {
};

unsigned num_vars_cond = 0;
const varinfo_t var_info_cond[] = {
};

unsigned num_gvars = 0;
const varinfo_t global_var_info[] = {
};

const varinfo_t local_var_info_0[] = {
{ 1, -1, "retval" },};

const varinfo_t local_var_info_4[] = {
{ 4, -1, "x" },};

const local_varinfo_t local_var_info_table[] = {
{ 1, local_var_info_0 },
{ 0, NULL },
{ 0, NULL },
{ 0, NULL },
{ 1, local_var_info_4 },
{ 0, NULL },
};

int check_global_assertions(const void *gp)
{
return 0;
}

const char *source_line[] = {
"#include <assert.h>",
"#include \"amb.h\"",
"",
"int main(void)",
"{",
"\tint x;",
"\tAMB {",
"\t\tx = 0;",
"\t} OR {",
"\t\tx = 1;",
"\t}",
"\tassert(x == 0 || x == 1);",
"\treturn 0;",
"}",
};
