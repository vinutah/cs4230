#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <civlc.cvh>

#define QUEUE_FULL_SIZE 1

// global variables
pthread_mutex_t mux;
pthread_cond_t  cond_full;
pthread_cond_t  cond_empty;
int go;
static int qsize = 0;

// producer thread
void *producer(void *procid)
{
  int i;
  int pid = *((int*)procid);
  $when (go) pid = *((int*)procid);
  for (i = 0; i < 1; i ++) {
    pthread_mutex_lock(&mux);
    
    while (qsize == QUEUE_FULL_SIZE)
      pthread_cond_wait(&cond_full, &mux);

    assert(qsize >= 0);
    assert(qsize <= QUEUE_FULL_SIZE);
    
      printf("PID %d           produces item %d\n",
	   pid,               qsize);

    pthread_cond_signal(&cond_empty);

    qsize ++;
    
    pthread_mutex_unlock(&mux);
  }
  return NULL;
}


// consumer thread
void *consumer(void *procid)
{
  int i;
  int pid = *((int*)procid);
  $when (go) pid = *((int*)procid);
    for (i = 0; i < 1; i ++) {
      
    pthread_mutex_lock(&mux);
    
    while (qsize == 0) // was 'if' in the buggy code
      pthread_cond_wait(&cond_empty, &mux);

    assert(qsize >= 0); 
    assert(qsize <= QUEUE_FULL_SIZE);    

    printf("PID %d           consumes item %d\n",
	   pid,               qsize);
    pthread_cond_signal(&cond_full);
    
    qsize --;
    
    pthread_mutex_unlock(&mux);
    }
    return NULL;
}

void f() {
}

// main procedure
int main()
{
  int i, ret1, ret2;
  go = 0;
  pthread_t prod[2];
  pthread_t cons[2];
  qsize = 0;

  pthread_mutex_init(&mux, // pointer to mutex 
		     NULL); // default attributes
  pthread_cond_init(&cond_full, // pointer to cond var
		    NULL);      // default attributes
  pthread_cond_init(&cond_empty, // pointer to cond var
		    NULL);       // default attributes

  for (i = 0; i < 2; i ++) {
    printf("Creating producer %d and consumer %d\n", i, i);
    ret1 = pthread_create(&prod[i], // Collect PID 
			  NULL,     // Default attributes
			  producer, // Thread function
			  (void *)&i);// Sole arg passed in
    ret2 = pthread_create(&cons[i],
			  NULL,
			  consumer,
			  (void *)&i);

    if (ret1 != 0 || ret2 != 0) {
      //cout << "Error creating threads." << endl;
      exit(-1);
    }
  }
  //  pthread_exit(NULL);
  for (i = 0; i < 2; i ++) {
    if (i == 0) { go = 1; }
    pthread_join(prod[i], NULL); // NULL = 
    pthread_join(cons[i], NULL); // not collecting a value back
  }

  pthread_mutex_destroy(&mux);
  pthread_cond_destroy(&cond_full);
  pthread_cond_destroy(&cond_empty);
  return(0);
}
