# Shared-Memory Proramming with OpenMP

## OpenMP

  * API for shared-memory parallel programming
  * MP in OpenMP --> Multiprocessing
  * Mutiprocessing --> shared-memory parallel computing.
  * Designed for systems in which each thread or process
  * can potentially have access to all available memory
  * When we are programming with OpenMP
  * view our system as a collection of cores or CPUs.
  * all of these have access to main memory


## OpenMP and PThreads Similarities

  * Both APIs for shared-memory programming

## OpenMP and PThreads Differences

  * Ptherad requires that Programmer explicitly specifies
  the behaviour of each thread.
  OpenMP, sometimes allows the programmer to simply state
  that a block of code should be executed in parallel
  The precise determination of the tasks and which thread
  should execute them is left to the compiler and the run-time systems.

  * PThread, like MPI is a library of functions that can be linked
  to a C program
  Any Pthreads program can be used with any C compiler.
  provided the system has a Ptherad library.
  OpenMP requires compiler support for some operations
  possible that I may have a C compiler that cannot compile
  OpenMP programs into parallel programs

## Why there are 2 standard APIs for shared memory programming.

  * Pthread is lower level
  Provides power to program virtually any conceivable thread behaviour
  this power comes with associated cost
  cost -- up to us to specify every detail of the behaviour of each thread

  *OpenMP
  Allows compiler and run-time system to determine some of the details
  of thread behaviours
  cost -- lower, simpler to code some parallel behaviours using OpenMP
       -- different, some low-level thread interactions can be
          more difficult to program

## History

  * Developed by group or programmers and computer scientists
  felt writing large-scale high-performance programs using APIs
  like Pthreads was too difficult.
  They defined the OpenMP specification
  Goal
  --> Shared-memory program could be developed at a higher level
  --> Explicitly designed to allow programmers to incrementally parallellize
      existing serial programs.
      --> this is virtually impossible with MPI
      --> fairly difficult with Pthreads

## The Plan

  * Basics of OpenMP
  * How to write a program that can use OpenMP
  * How to compile and run OpenMP programs
  * How to exploit one of the most powerful features of OpenMP
   -- Its ability to parallelize many serial for loops
      with only small changes to the source code.
  * Other features of OpenMP
    -- Task parallelism
    -- Explicit Thread synchronization
  * Standard Problems in shared memory programming
    -- Effect of cache memories in shared memory programming
    -- Problems that can be encounterd when serial code/library is used
       in a shared-memoy program

## The OpenMP source code

  * C programming
    * When we start a program from command line
    * The operating system starts a single-threaded process
      and the process executes the code in the main function.

  * What is a structured block for code in C
