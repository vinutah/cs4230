/*
 * learn new features in c++ 2011 standard
 * called the c++11 standard
 * its used in the mccool book
 *
 * these features are available in several widely used
 * c++ compilers.
 *
 * learn suitable substitues for use with c++
 */

/*
 * c++11 permits the auto keyword to be used
 * in place of a type in some contexts where
 * the type can be deduced by the compiler.
 *
 * example
 */

std::vector<double> v;
...
for (auto i = v.begin(); i != v.end(); ++i) {
    auto& x = *i;
    ...
}

/*
 * the c++98 equivalent would be
 */

std::vector<double> v;
...
for (std::) {
}

