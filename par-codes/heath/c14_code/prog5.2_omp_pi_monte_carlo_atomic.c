/* File:      prog5.2_omp_pi_monte_carlo.c
 * Purpose:   Estimate pi using OpenMP and a monte carlo method
 * 
 * Compile:   g++ -std=c++11 -pthread -g -Wall -o prog5.2_omp_pi_monte_carlo_atomic 
 *                  prog5.2_omp_pi_monte_carlo_atomic.c my_rand.c
 *            *needs my_rand.c, my_rand.h
 *
 * Run:       ./prog5.2_omp_pi_monte_carlo_atomic <number of threads>
 *                  <number of tosses>
 *
 * Input:     None
 * Output:    Estimate of pi
 *
 * Note:      The estimated value of pi depends on both the number of 
 *            threads and the number of "tosses".  
 */

#include <stdio.h>
#include <stdlib.h>
#include "my_rand.h"
#include <thread>
#include <atomic>

/* Serial function */
void Get_args(char* argv[], int* thread_count_p, 
      long long int* number_of_tosses_p);
void Usage(char* prog_name);

/* Parallel function */
long long int Count_hits(long long int number_of_tosses, int thread_count);

/* For loop implementation */
void Calculate_Tosses(long long int number_of_tosses, std::atomic<long long int> *toss, std::atomic<long long int> *number_in_circle, int id);

/*---------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
   double pi_estimate;
   int thread_count;
   long long int number_in_circle;
   long long int number_of_tosses;
   
   if (argc != 3) Usage(argv[0]);
   Get_args(argv, &thread_count, &number_of_tosses);
   
   number_in_circle = Count_hits(number_of_tosses, thread_count);

   pi_estimate = 4*number_in_circle/((double) number_of_tosses);
   printf("Estimated pi: %e\n", pi_estimate);

   return 0;
}

/*---------------------------------------------------------------------
 * Function:      Count_hits
 * Purpose:       Calculate number of hits in the unit circle
 * In arg:        number_of_tosses, thread_count
 * Return val:    number_in_circle
 *
 *c++ 11 thread version of Count_hits
 */
long long int Count_hits(long long int number_of_tosses, int thread_count) {

	// creates atomic version of these variables.
	// makes sure that certain instructions are thread safe.
	std::atomic<long long int> number_in_circle(0);
	std::atomic<long long int> toss(0);

	std::thread t[thread_count];

    //Launch a group of threads
    for (int i = 0; i < thread_count; ++i) {
        t[i] = std::thread(Calculate_Tosses, number_of_tosses, &toss, &number_in_circle, i);
		printf("Launched thread\n");
	}

    //Join the threads with the main thread
    for (int i = 0; i < thread_count; ++i) {
        t[i].join();
	}

#  ifdef DEBUG
	printf("Total number in circle = %lld\n", number_in_circle);
#  endif

	return number_in_circle;
}  /* Count_hits */

/* Function run by each thread until all iterations have been met */
void Calculate_Tosses(long long int number_of_tosses, std::atomic<long long int> *toss, std::atomic<long long int> *number_in_circle, int id) {
	int my_rank = id;
	unsigned seed = my_rank + 1;
	double x, y, distance_squared;

	// this version can actually allow more than the
	// requested number of tosses. Why?
	while(*toss < number_of_tosses){

		// since it is atomic, toss will read and write
		// without letting other threads access the toss.
		(*toss)++;
		x = 2 * my_drand(&seed) - 1;
		y = 2 * my_drand(&seed) - 1;
		distance_squared = x*x + y*y;
		if (distance_squared <= 1) (*number_in_circle)++;
//#        ifdef DEBUG
		printf("Th %d > toss = %lld, x = %.3f, y = %.3f, dist = %.3f\n",
			my_rank, (long long int)(*toss), x, y, distance_squared);
//#        endif
	}
}

/*---------------------------------------------------------------------
 * Function:  Usage 
 * Purpose:   Print a message showing how to run program and quit
 * In arg:    prog_name:  the name of the program from the command line
 */

void Usage(char prog_name[] /* in */) {
   fprintf(stderr, "usage: %s ", prog_name); 
   fprintf(stderr, "<number of threads> <total number of tosses>\n");
   exit(0);
}  /* Usage */

/*------------------------------------------------------------------
 * Function:    Get_args
 * Purpose:     Get the command line args
 * In args:     argv
 * Out args:    thread_count_p, number_of_tosses_p
 */

void Get_args(
           char*           argv[]              /* in  */,
           int*            thread_count_p      /* out */,
           long long int*  number_of_tosses_p  /* out */) {
   
   *thread_count_p = strtol(argv[1], NULL, 10);  
   *number_of_tosses_p = strtoll(argv[2], NULL, 10);
}  /* Get_args */

