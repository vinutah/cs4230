#include <sstream>
#include <iostream>

template<typename Target=string,typename Source=string>
Target to(Source arg) // convert Source to target
{
    stringstream interpreter;
    Target result;
    
    if (   !((interpreter << arg))            // write argument to stream
        || !(interpreter >> result)           // read result from stream
        || !(interpreter >> std::ws).eof())   // stuff left in stream ?
        throw runtime_error{"to<>() failed"};

    return result;

}

Target to(Source arg);// convert Source to target

int main(int argc, char *argv[])
{   
    auto x1 = to<string, double>(1.2);
    std::cout << x1 << std::endl;
}

