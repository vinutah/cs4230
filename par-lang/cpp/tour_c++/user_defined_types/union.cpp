// 1. a union is a struct
//
// 2. all members are allocated at the same address
//      - so that the union occupies only as much space as its largets member
//
// 3. union occupies only as much space as its largest member.


// example
//
// symbol table entry
// holds a name and a value
//

enum Type { str, num };

struct entry {
    char* name;
    Type t;
    char* s;    // use s if t == str
    int i;      // use i if t == num
};


void f(Entry* p)
{
    if (p->t == str)
        cout << p-> s;
    // ...
}

// problem
//
// the members s and i can never be used at the same time
// space is wasted
//
// solution
//
// make them members of the union

union Value {
    char* s;
    int i;
};

//
// the language does not keep track of which kind of value is held by a union,
// the programmer must do that

struct Entry {
    char* name;
    Type t;
    Value v; // use v.s if t==str; use v.i of t==num
};

void f(Entry* p)
{
    if (p->t == str)
        cout << p->v.s;
    // ...
}









































