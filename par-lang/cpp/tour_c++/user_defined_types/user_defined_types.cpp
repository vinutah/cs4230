/*
 * user defined types
 *
 * Introduction
 *
 * Structures
 *
 * Classes
 *
 * Unions
 *
 * Advice
 *
 */

// Introduction
//
// built-in types
//  - fundamental types
//  - the const modifiers
//  - the declarator operators
//
// rich
// low level
// computer hardware
//
// abstraction mechanism
//
// user defined types
//
//
// design and implementation of 
// user-defined types
//  - classes
//  - enums 
//
//

struct Vector {
    int sq;         // number of elements
    double *elem;   // pointer to elements
}

// The first step in building a new type 
// is or organize the elements it needs
// into a data structure.
//
// a struct

// Vector version - 1
// 
// Consists of an integer
// and a double*
//
// defining a variable of this type

Vector v;

// v's elem pointer points to nothing
// need to make this useful
// to be useful lets give v some elements to point to
//
// for example we can construct a Vector like this.


void vector_init(Vector& v, int s)
{
    v.elem = new double[s]; // allocate an array of s doubles
    v.sz = s;
}

// v's elem pointers gets a pointer produced by the new operator
// v's sz member gets the number of elements

// the & in Vector& says we pass v by non-const reference
// vector_init() can modify the vector passed to it.
//
// new operator
// - allocates memory from an area called the free store
//   or dynamic memory and heap
// - objects allocated from this free store are independed
//   of the scope from which they are created 
// - live until they are destroyed using the delete 
//   operator.
//
// simple use of vector

double read_and_sum(int s)
    // read s integers from cin
    // return their sum;
    // assume s to be positive
{
    Vector v;

    vector_init(v,s); //allocates s elements for v
    for(int i=0; i!=s; ++i)
        cin >> v.elem[i]; //read into elements

    double sum=0;
    for(int i=0; i!=s; ++i)
        sum += v.elem[i]; // take the sum of the elements

    return sum;
}

// .  access struct members through a name 
// .  access struct members through a reference
// -> access struct members through a pointer

void f(Vector v, Vector& rv, Vector* pv)
{
    int i1 = v.sz;    // access through name
    int i2 = rv.sz;   // access through reference
    int i4 = pv->sz;  // access through pointer
}

// classes

// language mechanism to distinguish between 
// - interface to a type 
//      used by all
// - implementation of a type
//      access to the otherwise inaccessible data
//
// public
// - interface is defined by these members
//
// private
// - accessible only through that interface
//

// constructor
// 
// what
//      a fucntion used to construct objects of a class
//      not an ordinary function.
//
// why
//      defining a constructor eliminates the problem of uninitialized variables for a class
//      a constructor is guaranteed to be used to initialize objects of its class
//
// how
//
// 1.   A function with the same name as its class 
//      Vector() replaces vector_init()
//
// 2.   How objects needs to be constucted ?
//      Vector(int s) --> says it needs an int!
// 
// member initialization list
//
// what
//
// why
//
// how
// 
// 1.   the constructor initializes the Vector members
//      using a member initializer list.
//
// 2.   :elem{?},sz{?}
//

class Vector {
    public:
        Vector(int s) :elem{new double[s]}, sz{s} {}; // construct a Vector
        double& operator[](int i) { return elem[i]; } // element access: subscripting
        int size(){ return sz; }
    private:
        double* elem; // pointer to the elements
        int sz; // the number of elements
};

Vector v(6); // a Vector with 6 elements

// the Vector object is a handle
// containing a pointer to the elements (elem)
// and the number of elements (sz)
//
//
// interface provided by the public members
//
// Vector()
//  1. constuctor
//  2. member initialization list
//
// operator[]()
//  1. access to elements is provided by a subscript function
//     returns a reference to the appropriate element
//     a double&
//
// size()
//  1. supplied to give users the number of elements

// read_and_sum example

double read_and_sum(int s)
{
    Vector v(s); // make a vector of s elements
    for (int i=0; i!=v.size(); ++i)
        cin >> v[i]; // read into elements

    double sum = 0;
    for (int i=0; i!=v.size(); ++i)
        sum += v[i]; // take the sum of elements

    return sum;
}



