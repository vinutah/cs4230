// enumerations
//
// in addition to classes
// c++ supports a simple form of user-defined type
// for which we can enumerate the values


enum class Color {red, bule, green};
enum class Traffic_light {green, yello, red};


Color col = Color::red;
traffic_light light = Traffic_light::red;

// enumerators are in the scope of their enum class,
// so that they can be used repeatedly in different
// enum_classes without confusion.


