/*
 * Introduction
 * Programs
 * Hello, World
 * Functions
 * Types
 * Variables
 * Arithmetic
 * Scope
 * Lifetime
 * Constants
 * Pointers
 * Arrays
 * References
 * Tests
 */

// Introduction

// Informal presentation of the notation of C++
// C++ model of memory
// C++ model of computation
// Basic mechanism of organizing code into a program
//
// C language gives facilities
// supports a style called procedural progarmming

// Programs


// Hello, World!

// the minimal c++ program

int main(){}

// hello world

#include <iostream>

int main()
{
  std::cout << "Hello, World\n";
}

// std:: specifies that the name cout is to be found
// in the standard-library namespace
//
// all the executable code is placed in functions
// and are called directly or indirectly from
// main()

#include <iostream>
using namespace std;

double square(double x)
{
  return x*x;
}

void print_square(double x)
{
  cout << "the square of" << x << "is" << square(x) << "\n";
}

int main()
{
  print_square(1.234)
}

// functions

// no argument; return Elem*, a pointer to Elem
Elem* next_elem();

// int argument; return nothing
void exit(int);

// double argument; return a double
double sqrt(double);

// function declaration

// call sqrt() with the argument double{2}
double s2 = sqrt(2);

// error: sqrt() requires an argument of type double
double s3 = sqrt("three");

// return the square rool of d
double sqrt(double d);

// return the square of the argument
double square(double)

// type: double(const vector<double>&, int
double get(const vector<double>& vec, int index);

// type: char& String::(int)
char& String::operator[](int index);

// function overloading

// takes an interger argument
void print(int);

// takes a floating-point argument
void print(double);

// takes a string argument
void print(string);

void user()
{
    // calls print(int)
    print(42);

    // calls print(double)
    print(9.65);

    // calls print(string)
    print("D is for Digital");
}

// ambiquity

void print(int,double);

void print(double,int);

void user2()
{
    print(0,0); // error ambiguouse
}


// type

int inch;

// declaration

// fundamental types

// Boolean, possible values are true and false

bool

// Character

char

// Interger

int

// Double precision floating-point number

double

// Non-Negative number

unsigned

// each fundamental type corresponds directly
// hardware facilities, fixed size that determines
// the range of values that can be stored in it.

// plus
x+y

// unary plus
+x

// minus
x - y

// 
-x

// multiply
x*y

// divide
x/y

// remainder (modulus) for intergers
x%y

// comparison

// equal
x == y

// not-equal
x!=y

// less than
x < y

// greater than
x > y

// less than of equal
x <= y

// greater than of equal
x >= y

// logical operators

x&y //bitwise and
x|y //bitwise or
x^y //bitwise exclusive or
^x //bitwise complement
x&&y // logical and
x||y // logical or

// conversions
// c++ performs all meaningful conversions between
// the basic types so that they can be mixed freely


// function that does not return a value
void some_function()
{
    // initialize floating-point number
    double d = 2.2
    
    // initialize interger
    int i = 7;

    // assign sum to d
    d = d + i;

    // assign product to i (truncating the double d*i to an int)
    i = d*i
}


// variables


// initialization

// initialize d1 to double
double d1 = 2.3; 

// initialize d2 to 2.3
double d2 {2.3}

// a complex number with double-precision floating-point scalars
complex<double> z = 1;

complex<double> z2 {d1,d2};

// the = is optional with {...}
complex<double> z3 = {1,2};

// a vector of ints
vector<int> v {1,2,3,4,5,6};

// the = form is traditional and dates back to C
// use the general {} -list form
// it saves you from conversion that lose information 

int i1 = 7.8 // i1 becomes 7 surprise ?
int i2 {7.2} // error: floating-point to interger conversion
int i3 = {7.2} // error: floating-point to integer conversion, the = is redundant

// narrowing conversions are allowed and implicitly applied, c compatibility

// contants

// user defined types


// auto

// when dealing with a variable, dont need to state its type explicitly
// its deduced from the initializer

// with auto, we use =
// because there is no potentially troublesome type conversion involved

auto b = true; // a bool
auto ch = 'x'; // a char
auto i = 123; // an int
auto d = 1.2; // a double
auto z = sqrt(y) // z has the type of whatever sqrt(y) returns

// constants


// c++ more specific operations for modifying a variable
// concise, convenient and frequently used
x+=y
++x
x-=y
--x
x*=y
x/y
x%=y

// scope and lifetime

// objects without namespace
// temporaries and objects, created using new

void fct(int arg) // fct is global ( a global function)
                  // arg is local (an integer argument)
{
    string motto {"who dares win"}; // motto is local
    auto p = new Record{"Hume"}; // p points to an unnames Record (created by new)
    // ...
}

// local scope

// class scope

// namespace scope

// global

vector<int> vec; // vec is global (a global vector of integers)

struct Record{
    string name; // name is a member , a string member
    // ... 
};

// constants

// 2 notions of immutability

const int dmv = 17; // dmv is a named constant
int var = 17; // var is not a constant 

constexpr double max1 = 1.4*square(dmv); // OK if square(17) is a constant 
constexpr double max2 = 1.4*square(var); // error: var is not a constant expression

const double max3 = 1.4*square(var); // OK, may be evaluated at run time

double sum(const vector<double>&); // sum will not modify its argument

vector<double> v {1.2, 3.4, 4.5}; // v is not a constant

const double s1 = sum(v); // OK: evaluated at run time

constexpr double s2 = sum(v); // error: sum(v) not constant expression

// for a function to be usable in a constant expression ...
// page 9

constexpr double square(double x) { return x*x; }


// pointers

// array of 6 characters

char v[6];

// pointer to character

char* p;



// arrays

// all arrays have 0 as their lower bound

char* p = &v[3]; // p points to v's fourth element

char x = *p; // *p is the object that p points to

// in an expression, 
// prefix unary * means "contents of"
// prefix unary & means "address of"
//
// graphical picture

// page 10

// copying ten elemments from one array to another

void copy_fct()
{
    int v1[10] = {0,1,2,3,4,5,6,7,8,9};
    int v2[10]; // to become a copy of v1

    for (auto i=0; i!=10; ++i) // copy elements
        v2[i] = v1[i];

    // ...
}

// simpler for loop


void print()
{
    int v[] = {0,1,2,3,4,5,6,7,8,9};

    for(auto x : v) // for each x in v
        cout << x << '\n';

    for(auto x : {10,21,32,43,54,65})
        cout << x << '\n';
    // ...
}


// dont copy into x

void increment()
{
    int v[] = {0,1,2,3,4,5,6,7,8,9};

    for (auto& x:v)
        ++x;

    // ...
}


// references

// the unary suffix & means "reference to"
// a reference is similar to a pointer
// except that i dont need to use a prefix *
// to access the value referred to by the reference
//
// also a reference cannot be made to refer to a different object
// after its initialization.

// references are particularly useful for specifying function arguments

// by using a reference, we ensure that for a call sort(my_vec)
// we do not copy my_vec and that it really is my_vec that is sorted 
// and not a copy of it.


void sort(vector<double>& v); //sort v

// when we do not want to modify an argument
// but still dont want to the cost of copying
// we use a const reference.

double sum(const vector<double> &)
 
// functions taking const references are very common.

// When used in declarations
// operators such as &, * and [] are called declarator operators

// T[n]: array of n Ts

T a[n];

// T*: pointer to T

T* p;

// T&: reference to T

T& r;

// T(A): function taking an argument of type A returning a result of type T

T f(A);

// ensure that a pointer always points to an object
// why ?
// referencing it is valid
// when we dont have an object to point to 
// or
// if we need to represent the notion of "no object available"
// example end of a list
// we give the pointer the value nullptr
// there is only one nullptr shared by all pointer types:

double* pd = nullptr;

// pointer to a Link to a a Record

Link<Record>* lst = nullptr;

// error: nullptr is a pointer not an integer

int x = nullptr;

// wise
// check that a pointer argument
// that is supposed to point to something 
// is acutually pointing to something

// count the number of occurrences of x in p[]

// p is assumed to point to 
// a zero-terminated array of char
// or to nothing

int count_x(char* p, char x)
{
    if (p==nullptr) return 0;
    int count = 0;
    for(; p!=nullptr; ++p)
        if (*p==x)
            ++count;
    return count;
}

// notes
// we can move a pointer to point to the next element
// of an array using the ++
// we can leave out the initializer in a for-statement
// if we do not need it.

// the definition of count_x() assumes that the char*
// is a c-style string
// c-style string --> the pointer points to a zero-terminated array of char

// in older code,
// 0 or NULL is typically used instead of nullptr.
// however, using nullptr eliminates confusion between integers
// such as 0 or NULL and pointers such as nullptr

// in the count_x() example, we are not using the initalizer part
// of the for-statement, so we can use the simpler while-statement

int count_x(char* p, char x)
{
    int count = 0;
    while(p){
        if (*p == x)
            ++count;
        ++p;
    }
    return count;
}

// the while statment executes until its condition becomes false.

// a test of a pointer while(p) is equivalent to comparing the pointer

// to the null pointer

while(p!=nullptr)

// tests

// c++ provides a conventional set of statements for selection and looping

// simple function that prompts the user and returns a boolean indicating
// the response

bool accept()
{
    cout << "do you wanna proceed (y or n)?\n" // write question

    // read ansewr
    char answer = 0;
    cin >> answer;

    if (answer == 'y')
        return true;
    return false;
}

// improved version

bool accept2()
{
    cout << "Do you want to proceed (y or n)\n" // write question

    char answer = 0;
    cin >> answer;

    switch(answer){
    case 'y':
        return true;
    case 'n':
        return false;
    default:
        cout << "I'll take that for a no\n";
        return false;
    }
}


// something clever about this - bs

void action()
{
    while (true) {
        cout << "enter action\n"; //request action
        string act;
        cin >> act; // read characters into a string
        Point delta {0,0}; // Point holds as {x,y} pair

        for (char ch : act) {
            switch (ch) {
                case 'u': //up
                case 'n': //north
                    ++delta.y;
                    break;
                case 'r': //right
                case 'e': //east
                    ++ delta.x;
                    break;
               // ...more actions...
                default:
                    cout << "I freeze!";
            }
        }
    }
}

