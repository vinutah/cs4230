// 
// terminology
//
// class hierarchy
// a set of classes ordered in a lattice created by derivation
//
// example

class Container{
    public:
        virtual double& operator[](int) = 0;
        virtual int size() const = 0;
        virtual ~Container() {}
};

// the Container example is a very simple example of a class hierarchy.
class List_container:public Container {
        std::list<double> ld;
    public:
        List_container(){}
        List_container(initializer_list<double> ii) : ls{ii} {}
        ~List_container() {}

        double& operator[](int i);
        int size() const { return ld.size; }
};

void use(Container& c)
{
    const int sz = c.size();

    for(int i=0; i!=sz; ++i)    
        cout << c[i] << '\n';
}

void h()
{
    List_container ic = {1,2,3,4,5,6,7,8,9};
    use(ic);
}

// make simple diagram into code
// smiley diagram.

// First specify a class that defines the 
// general proterties of all shapes

// This interface is an abstract class
// 
// represenrtation
//
// specify a class that defines the general properties
// of all shapes

class Shape {
    public:
        virtual Point center() const =0; // pure virtual
        virtual void move(Point to) =0;

        virtual void draw() const = 0; // draw on current "Canvas"
        virtual void rotate(int angle) = 0;

        virtual ~Shape() {} // destructor
        // ...
};

// this interface is an abstract class
// as far as representation is concerned,
// nothing except the locatotion of the pointer 
// to the vtbl is common for every Shape.

// we can write general functions manipulating vectors
// of pointers to shapes.

void rotate_all(vector<Shape>& v, int angle) 
    //rotate v's elements by angle degrees
{
    for (auto p : v)
        p->rotate(angle);
}

// to define a particular shape
// we must say that it is a Shape
// and specify its particular properties
// including its virtual functions

class Circle : public Shape {
    public:
        Circle(Point p. int rr); //constructor

        Point center() const {return x;}
        void move(Point to){x = to;}
        void draw() const;
        void rotate(int) {} // nice simple algorithm
    private:
        Point x;    // center
        int r;      // radius
}

// build further

class Smiley : public Circle {  // use the circle as the base for a face 
    public:
        Smiley() : Circle{p,r} , mouth{nullptr} {}

        ~Smiley() 
        {
            delete mouth;
            for (auto p : eyes)
                delete p;
        }

        void move(Point to);

        void draw() const;
        void rotate(int);
       
       // the push_back() member function adds its assignment 
       // to the vector
       // increasing that vectors size by 1.
        void add_eye(Shape* s) {eyes.push_back(s)};
       
        void set_mouth(Shape* s);
        virtual void wink(int i); // wink eye number

        // ...

    private:
        vector<Shape*> eyes // usually 2 eyes.
            Shape* mouth;
};

// We can now define Smiley::draw
// using calls to Smiley's base
// and member draw()

// note the way that Smiley
// keeps its eyes in a standard-library vector
// and deletes them in its destructor

// shape's destructor is virtual
// and Smiley's destructor overrides it.

// a virtual destructor is essential for 
// an abstract class because an object of a derived
// class is usually manipulated through
// the interface provided by its abstract base class.
//
// derivation
//
// we can add 
// - data members
// - operations
// - or both
// - as we define a new class by derivation
// great flexibility with corresponding opportunities
// for confusion and poor design

void Smiley::draw()
{
    Circle::draw();

    for (auto p : eyes)
        p -> draw();

    mouth -> draw();
}









































