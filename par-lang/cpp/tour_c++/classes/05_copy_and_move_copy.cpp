// copy constructors

// when a class is a resource handle
// - when the class is responsible
//   for an object accessed through 
//   a pointer
// - the default memberwise copy
//   is typically a disaster
// - memberwise copy would violate
//   the resource handle's invariant

// example

// the default copy would leave a copy
// of a Vector referring to the same
// elements as the original

void bad_copy(Vector v1)
{
    Vector v2 = v1; // copy v1's representation into v2
    v1[0] = 2; //v2[0] is now also 2!
    v2[1] = 3; // v1[1] is now also 3!
}

// graphical representation

// Vector has a destructor
// - strong hint that the default
//   memberwise copy semantics is wrong
// - and the compiler should warn

// need better copy semantics

// copying of an object of a class
// is defined by two members.
// 1. a copy constructor
// 2. a copy assignment

class Vector {
    private:
        // elem points to an array of sz doubles
        double* elem;
        int sz;
    public:
        // constructor: establish invariant, acquire resources
        // TODO: what invariant ?
        Vector(int s);            

        // destructor: release resources
        ~Vector(){delete[] elem;}  

        // copy constructor
        Vector(const Vector& a);

        // copy assignment
        Vector& operator=(const Vector& a);

        double& operator[](int i);
        const double& operator[](int i) const;

        int size() const;
};


// copy constructor

// a suitable definition of a copy constructor for Vector 
// 1. allocates the space for the required number of elements
// 2. copies the elements into it.
// - after a copy each Vector has its own copy of the elements

Vector::Vector(const Vector& a) // copy constuctor
    :elem{new double[a.sz]}, // allocate space of elements
     sz{a.sz}
{
    // copy elements
    for (int i=0; i!=sz; ++i)
        elem[i] = a.elem[i];
}

// the result of v2=v1 example can now be presented as:

// copy assignment in addition to the copy constructor

// this
// - predefined in a member function 
// - points to the object for which the member function is called

Vector& Vector::operator=(const Vector& a) //copy assignment
{
    // 1.
    double* p = new double[a.sz];

    // 2.
    for(int i=0; i!=a.sz; ++i)
        p[i] = a.elem[i];
    
    // 3.
    delete[] elem; // delete old elements

    // 4.
    elem = p;
    sz   = a.sz;

    // 5.
    return *this;
} 




































