// problem
// 
// We want to use a member function that is only provided
// by a particular derived class
//
// Is this Shape a kind of Smiley ?
//
// solution

Shape*ps {read_shape(cin)};

if (Smiley* p = dynamic_cast<Smiley*> (ps)) {
    // ...is the Smiley pointer to by p ...
}
else {
    // ... not a Smiley, try something else ...
}

Shape* ps {read_shape(cin)};
Smiley& r {dynamic_cast<Smiley&> (*ps)};  // somewhere, catch std::bad_cast


















