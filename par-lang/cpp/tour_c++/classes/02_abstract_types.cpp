//
// concrete types
//
// Types like complex and Vector are called concrete types because 
// their representation is part of their definition.
// They resemble built-in types
//
// abstract type
//
// what 
// a type that completely insulates a user from implementation details
// 
// how
// how to get this insulation !
// - develop the interface from the representation
// - give up genuine local variables
// - since we do not know anything about the representation of 
//   an abstract type, not even its size
//   we must allocate objects on the free store
//   access them through references or pointers.
//
// example  
//
// first
// - define the interface of a class Container
// - a more abstract version of our Vector
//

// This container class is a pure interface to specfic containers defined later.
// virtual
// - may be redefined later in a class derived from this one.
//
// virtual function
// - a function declared virtual
//
// a class derived from Container
// - provides an implementation for the Container interface.
// - =0 syntax says the function is pure virtual
// - some class derived from Container must define the function
// - not possible to define an object that is
//   just a Container;
// - A Container can only serve as the interface to a class that implements
//   its operator[]() and size() functions.
//
// abstract class
// - a class with a pure virtual function

// No constructor
// - its common for abstract classes
// - why? 
// - does not have any data to initialize
// - they tend to be manipulated through references or pointers

// Yes destructor
// - Container does have a destructor
// - destructor is virtual
// - they tend to be maniputalted through references and pointers

class Container { 
    virtual double& operator[]() = 0; 
    virtual int size() const =0;
    virtual ~Container() {}
};


// Using the above container

// without any idea of exactly which type provides their 
// implementation

// use() uses size()
// use() uses []
// no idea of how exactly which type provides their implentation
//
// polymorphic type
// - a class that provides the interface to a variety of other classes


void use(Container& c)
{
    const int sz = c.size();

    for(int i=0; i!=sz; ++1)
        cout << c[i] << '\n';
}


// A container that implements the functions required by the interface
// defined by the abstract class Container
// could use the concrete class Vector

// : public
// - is derived from
// - is a subtype of
// - class Vector_container is said to have 
//   been derived from class Container
// - class Container is said to be a base of class
//   Vector_container.
// - alternate terminology
//    Container        : super class
//    Vector_container : sub class
//
// - derived class
//   is said to inherit members from its base class
//   the use of base and derived classes --> inheritance

// overrides
// - operator[]() 
// - size() are said to override
//   the corresponding members in the base class Container
// TODO: add the remaining overrides here.


class Vector_container : public Container {
    Vector v;
    public:
        Vector_container(int s): v(s) {} 
        ~Vector_container() {}

        double& operator[](int i){return v[i];}
        int size() const { return v.size(); }
};

// TODO:
// About use(Container)

// For a function like use() to use a Container
// in complete ignorance of implementation details
// - some other function will have to make an object
//   on which it can operate
//
void g()
{
    Vector_container vc {10,9,8,7,6,5,4,3,2,1,0};
    use(vc);
}

// use() 
//
// - does not know about Vector_containers but only
//   knows the Container interface.
//
// - It will work just as well for a different implementation of
//   a Container.


// TODO Add comments here about the lines of codes
// Here is a differnet implementation of Container

// represenatation is a standard-library list<double>
//
// bs has implemented a container with a
// subscript operator using a list.
//
// - cons
//   performace of list subscripting is atrocious
//   compared to vector subscripting
//

class List_container:public Container{
        std::list<double> ld; 
    public:
        List_containe(){}
        List_container(initializer_list<double> il) : ld{il} {}
        ~List_container() {}

        double& operator[](int i);
        int size() const {return ld.size();}
};

double& List_container::operator[](int i)
{

    for(auto& x: ld){
        if(i==0) return x;
        --i;
    }
    throw out_of_range{"List container"};
}

// A function can create a List_container and have use
// use() use it.
//
// point
// - use(Container&) has no idea if its arguments is
//   a Vector_container
//   a List_container
//   or some other kind of container
//   it does not need to know.
// - it can use any kind of container.
// - it knows only the interface defined by Container
//
// use(Container&) need not be recompiled
// - if the implementation of List_container changes
// - or a brand-new class derived from Container is used
//
// cons
// - this is a great flexibility
// - objects must be manipulated through pointers or references.

void h()
{ 
    List_container lc = {1,2,3,4,5,6,7,8,9};
    use(lc);
}




















