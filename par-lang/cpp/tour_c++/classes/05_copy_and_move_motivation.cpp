// why ?
//
// 1. By default, objects can be copied
// - this is true for objects
// - of user-defined types
// - as well as for built-in types.
//
// 2. the default meaning of copy is memberwise copy.
// - copy each member
// 
// example
 
void test(complex z1)
{
    complex z2 {z1};
    complex z3;
    z3 = z2;
    // ...
}

// Now z1, z2 and z3 have the same value
// because both 
// - the assignment and 
// - the initialization 
// copied both members

// when we design a class
// - always consider if and how an object might be copied
// - for simple concrete types memberwise copy is not the right semantics for copy
// - for abstract types it almost never is.


