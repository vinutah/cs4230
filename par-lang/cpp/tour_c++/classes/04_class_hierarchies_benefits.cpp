// class hierarchies
//
// benefit-1  interface inheritance
// - an object of a derived class
//   can be used whenever an object of a base
//   class is required
//
// - the base class acts as an interface for 
//   the derived class.
//
// - the Container and Shape classes are examples
//   Such classes are abstract classes
//
// benefit-2 implementation inheritance
//
// - A base class provides functions 
// - A base class provides data
// - these simplify the implementation of the derived classes.
//
// example
//
// Smiley's uses of Circle's constructor
// and of Circle::draw()
//
// Such base classes often have data members and constuctors

// Concrete classes
//
// especially classes with small representaions
// are much like built-in types
//
// we define them as local variables
//
// access them using their 
// - names
// - copy them around 
// - etc.
//
// classes in class hierarchies are different
// - allocate them on the free store using heap
// - we access them through pointers or references
//
// example
//
// - A function that reads data describing shapes
//   from an input stream
// - constucts the appropriate Shape objects 
 
enum class Kind {
    circle, 
    triangle, 
    smiley 
};

// read shape descriptions from input stream is
Shape* read_shape(istream& is)
{
    switch (k) {
        case Kind::circle:
            // read circle data {Point,int} into p and r
            return new Circle{p,r};
        case Kind::triangle:
            // read triangle data {Point,Point,Point} into p1,p2 and p3
            return new Triangle{p1,p2,p3};
        case Kind::smiley:
            // read smiley data {Point, int, Shape, Shape, Shape} into p, r, e1, e2 and m
            Smiley* ps = new Smiley;
            ps->add_eye(e1); 
            ps->add_eye(e2);
            ps->set_mouth(m);
            return ps;
    }
}

void user()
{
    std::vector<Shape*> v;
    
    while(cin)
        v.push_back(read_shape(cin));
    
    // call draw for each element
    draw_all(v); 

    // call rotate(45) for each element
    rotate_all(v);

    // remember to delete emelemts
    for (auto p: v)
        delete p;
}

// TODO: VIVIDLY ILLUSTRATES THAT user()
