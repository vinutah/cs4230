//
// problem
// 1. A user fails to delete the pointer returned by read_shape()
// 2. The owner of a container of Shape pointer might not delete
//    the objects pointed to.
// 
// Both are about resource leaks !
// - Functions returning a pointer to an object 
//   allocated --> on the free store
//   are dangerous !!
//
// solution
// - One solution to both problems is to return a standard-library
//   unique_ptr
//   rether than a "naked pointer"
// - and Store unique_ptr in the container.


unique_ptr<Shape> read_shape(istream& is)
// read shape descriptions from input stream is
{
    // read shape header from is and find its Kind k
    
    switch(k){
        case Kind::circle:
            // read circle data {Point,int} into p and r
            return unique_ptr<Shape> {new Circle{p,r}} ;

        // ...
    }

}


// the object is now owned by the unique_ptr
// which will delete the object when it is
// no longer needed
// --> when its unique_ptr goes out of scope

// for the unique_ptr version of user() to work,
// we need versions of draw_all() and rotate_all()
// that accept vector<unique_ptr<Shape>>s.
//
// writing many such _all() functions could
// become tedious so there is a alternative later

void user()
{   
    vector<unique_ptr<Shape>> v;
    while (cin)
        v.push_back(read_shape(cin));
    draw_all(v);        // call draw() for each element
    rotate_all(v,45);   // call rotate(45) for each element
} // all shapes implicitly destroyed


