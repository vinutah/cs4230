// classes
//
// these types not abstract
// they are as real as int
// and float
//
// introduction
//
// get an idea of c++ support 
// for abstraction
// and resource management
// dont get into a lot of detail
//
// how to define new types, user defined types
//
// basic proterties
// implementation techniques
// language facilities
// used for
// - concrete classes
// - abstract classes
// - class hierarchies

// the central language feature of c++ is the class

// a class 
//  - user defined type 
//  - provided to represent 
//  - a concept in the code of a program
//
//  classes are often what libraries offer.
//
//  all for classes !
//
//  1. Concrete classes
//  2. Abstract classes
//  3. Classes in class hierarchies



// concrete types

// just like built-in types
// complex number type
// infitine precision integer
// just like built-in int
// own semantics and set of operations

// concrete types

// defining characterisics of a concrete classes
//
// representation is part of its definition
//
// implementation has to be efficient 
// in both time and space
//
// place objects of concrete types on the stacks
// in statically allocated memory
// and in other object
//
// refer to objects directly
// and not just through pointers or references
//
// initialize objects immediately and completely
// using constructors
// 
// and copy objects
//
// what is the cost of having concrete types


// an arithmetic type
//
// the classical user-defined arithmeric type is complex

class complex{
        // representation: two doubles
        double re,im;
    public:
        // interface ?
        // construct complex from two scalars
        complex(double r, double i) :re{r}, im{i} {}

        // construct complex from one scalar
        complex(double r) :re{r}, im{0} {}

        // default complex: {0,0}
        complex() :re{0}, im{0} {}

        double real() const { return re;}
        void real(double d) { re=d; }

        double imag() const { return im; }
        void imag(double d) {im=d;}
    
        // add to re and im
        // and return the result
        complex& operator+=(complex z) {re+=z.re,im+=z.im;return *this}
        complex& operator-=(complex z) {re-=z.re,im-=z.im;return *this}

        // defined out-of-class somewhere
        complex& operator*=(complex);
        complex& operator/=(complex);
};

// standard library has complex
// its an arithmetic type
// concrete type

// class definition itself contains
// only the operations requiring access to the representation.
//
// the representaion is simple and conventional.
//
// logical demands
//
// complex must be efficient - or it will remain unused.
// --> simple operations must be inlined without function calls
// in the generated machine code.
//
// functions defined in class are inlined by default.
//
// carefully implemented to do appropriate inlining
//
// default constuctor
// a constuctor that can be invoked without an argument
// eliminate the possibility of uninitializes variables of that type
//
complex() // is complex's default constructor.

// const specifier

// the const specifiers on the functions returning the real
// and imaginary parts indicate that these functions
// do not modify the objects for which they are called.
//
// many useful operations do not require direct access
// to the representation of complex,
//
// so they can be defined separately from the class
// definition.

complex operator+(complex a,complex b){return a+=b;}
complex operator-(complex a,complex b){return a-=b;}
complex operator-(complex a){return {-a.real(),-a.imag()};} // unary minus
complex operator*(complex a,complex b){;}
complex operator/(complex a,complex b){return a/=b;}

// arguments passed by value are copied
// why use it
// so that i can modify an argument without affecting the caller's copy
// use the return as return value ?

// definition of ==

bool operator==(complex a, complex b)
{
    return a.rea;()==b.real() && a.imag()==b.imag();
}

// definition of !=

bool operator!=(complex a, complex b)
{
    return !(a==b)
}

complex sqrt(complex); // the definition is elsewhere

// ...

// using an arithmetic type

void f(complex z)
{
    complex a {2.3};
    complex b {1/a};
    complex c {a+z*complex{1,2.3}};
    // ...
    
    if (c!=b)
        c = -(b/a) + 2*b;
}


// note on overloading operators

// user-defined operators -> overload operators should 
// be used cautiously and conventionally
//
// cannot change a few operators


// container

// object holding a collection of elements

// we need a mechanism to ensure that the memory
// allocated by the constructor is deallocated
//
// need to add a destroctor

class Vector{
private:    
    // elem points to an array of sz doubles
    double* elem;
    int sz;

    Vector(int s):elem{new double[s]}, sz{s} 
    {
        for(int i=0; i!=s; i++) // initialize the elements 
            elem[i]=0; 
    }

    ~Vector(){delete[] elem;} // destructor: release resources
        
    double& operator[](int i)
        int size() const;
};

// the name of a destructor is the complement operator ~
// followed by the name of the class.
// it is the complement of a constructor.

void fct(int n)
{
    Vector v(n);

    //...use v...

    {
        Vector v2(2*n);
        // ...use v and v2 ...
    } //v2 is destroyed here

    // ...use v...

} // v is destroyed here

// 
// Vector obeys the same rules for naming, scope, allocation
// lifetime, etc.
//
// The constructor/destructor combination
// is the basis of many elegant techniques.
//
// basis for most C++ general resource management techniques
//
// constructor allocates the elements and initializes the Vector
// members approptriately.
//
// destructor deallocates the elements
//
// This handle-to-data model is very commonly used to manage
// data that can vary in size during the lifetime of an object
//
// The technique of acquiting resources in a constructor and
// releasing them in a destructor, = RAII 
// allows to eliminate - naked new opetations 

// RAII - far easier to keep free of resource leaks.


// initializing container

// a container exists to hold elements
// we need convenient ways of getting elements into
// a container
//
// create a vector with an appropriate number of elements
// and then assigning to them.
//
// more elegnant ways
// - initizalizer-list constructor 
//   initialize with a list of elements
//
// - push_back()
//   add a new element at the end
//   at the back of the sequence

class Vector {
    public:
        Vector(std::initializer_list<double>); // initilize with a list of doubles
        void push_back(double); //add element at the end, increasing the size by one
};

// push_back() is useful for input of arbitrary number of elements

// example snippet to show the use of v.push_back(d);

Vector read(istream& is)
{
    Vector v;
    for(double d; is >> d) // read floating-point values into d
        v.push_back(d); // add d to v;
    return v;
}

// The input loop is terminated by an end-of-file or a formatting error.
// until that happens
// each number read is added to the Vector so that at the end
// v's size is the number of elements read 
// page 38

// the std::initializer_list 
// its used to define the initializer-list constructor
// is a standard-library type known to the compiler:
// when we use a {}-list such as {1,2,3,4}
// the compiler will create an object of type initializer_list
// to give to the program.


// v1 has 5 elements
Vector v1 = {1,2,3,4,5}; 

// v2 has 4 elements
Vector v2 = {1.23,3.45,6.7,8};

Vector's initializer-list constructor can be defined like this

Vector::Vector(std::initializer_list<double> lst)
    :elem{new double[lst.size()]}, sz{static_list<int>(lst.size())}
{
    copy(lst.begin(), lst.end(), elem); // copy from lst into elem
}

// the type system has no common sense
//
// static_cast does not check the value it is converting
//
// the programmer is trusted to use it correctly
//
// casts -> explicit type conversion

// abstract types

// complex and Vector are called concrete types
// why
// because their representation is part of their definition
// they resemble the built-in types
