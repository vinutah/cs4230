// essential operations

// fact:
//
// construction
// - of objects plays a key role in many designs
// - this wide variety of uses is reflected 
//   in the range 
//   and flexibility
//   of the language features supporting initialization

// problem:
//
// constructors
// destructors
// copy operations
// move operations
// for a type are not logically separate !
//
// solution:
//
// define them as a matched set
// - or suffer logical or performance problems
//
// example

// if a class x
// - has a destructor that performs
//   a nontrivial task
// - such as free-store deallocation or 
// - lock release.
// - the class is likely to need the full complement of functions


class X {
    public:
        // "ordinary constructor": create an object
        X(Sometype);

        // default constructor
        X();

        // copy constructor
        X(const X&);

        // move constructor
        X(&&);

        // copy assignment: clean up target and copy
        X& operator=(const X&);

        // move assignment: clean up target and move
        X& operator=(X&&);

        // destructor: clean up
        ~X();
        // ...
};























