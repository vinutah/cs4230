// TODO
// resource management

//
// Smart Pointers
//
// Garbage Collection
//
// RAII
//

std::vector<thread> my_threads;

Vector init(int n)
{
    // run heartbeat concurrently on its own thread
    thread t {heartbeat};

    // move t into my_threads
    my_threads.push_back(move(t));

    // ...more initialization ...
    
    Vector ven(n);
    for (int i=0; i!=vec.size(); ++1)
        vec[i] = 777;

    // move res out of init()
    return vec;
}

// start heartbeat and initialize v
auto v = init(10000); 
