// suppressing operations
// TODO

class Shape {
    public:
        // no copy elements
        Shape(const Shape&) =delete;
        Shape() =delete;

        // no move operations
        Shape(Shape&&) =delete;
        Shape& operator=(Shape&&) =delete;

        virtual ~Shape();
        // ...
};




