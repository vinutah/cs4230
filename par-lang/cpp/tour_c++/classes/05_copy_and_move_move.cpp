// moving containers

class Vector {
    private:
        // elem pointing to an array of sz doubles
        double *elem;
        int sz;
    public:
        // constructor: establish invariant, acquire resources
        Vector(int s);

        // destructor: release resources
        ~Vector(){delete[] elem;}

        // copy constructor
        Vector(const Vector& a);

        // copy assignment
        Vector& operator=(const Vector& a);
        
        // operator[]
        double& operator[](int i);

        // operator[]
        const double& operator[](int i) const;

        // method : size()
        int size() const;
};

// problem
// - copying can be costly for large containers
// - want to avoid the cost of copying
// - when we pass objects to a function by using references
// - we cannot return reference to a local object as the result
// - the local object would be destroyed, by the time
// - the caller got a chance to look at it.
//
// solution

Vector operator+(const Vector& a,const Vector& b)
{
    if(a.size() != b.size())
        throw Vector_size_mismatch{}

    Vector res(a.size())

    for(int  i=0; i!=a.size; ++i)
        res[i] = a[i] + b[i]

    return res;
}

// returning from a + involves copying the result
// out of the local variable res
// and into some place where the caller
// can access it.
//
// we might use this + like this.

void f(const Vector& x, const Vector& y, const Vector& z )
{
    Vector r;

    // ...

    r = x + y + z;

    // ...
}

// That would be copying a Vector at least twice
// one for each use of the + operator.
//
// If a Vector is large, say, 10,000 doubles
// its embarrassing.
//
// The most rebarrassing part is that res in
// operator+() is never used again after the copy
//
// we did not want a copy
// fortunately, we can state that intent

class Vector
{
    // ...

    // copy constructor
    Vector(const Vector& a);

    // copy assignment
    Vector& operator=(cont Vector& a);

    // move constructor
    Vector(Vector&& a);

    // move assignment
    Vector& operator=(Vector&& a);
};

// given that condition
// the compiler will choose the move constructor to 
// implement the transfer of the return value out of the function
// 
// This means that r = x + y + z will involve no copying of Vectors
// Instead, Vector's are just moved.
//

// Vector's move constructor is trivial to define:

// lvalue
// - something that can appear on the left-hand side of an assignment
//
// rvalue
// - Intended to complement lvalue
// - to a first approximation 
// - is a value that you cannot assign to
// - such as an integer returned by a function call
//
// rvalue reference
// - Its a reference to which we can bind an rvalue
// - a reference to something that nobody else can assign to
// - so that we can safely "steal" its value
// - the res local variable in operator+() for Vector is an example
//
// && means "rvalue reference"

Vector::Vector(Vector&& a)
    :elem{a.elem}  // grab the elements from a
    :sz{a.sz}
{
    a.elem = nullptr;
    a.sz = 0;
}

// a move constructor does not take a const argument
// - after all 
// - a move constructor is supposed to
// - remove the value from its argument
// - a move assignment is defined similarly

// the move operation 
// - applied when an rvalue reference is used an an initializer or
// - as the right hand side of an assignment

// after a move
// - a moved-from object should be
// - in a state that allows a destructor to be run
// - typically, we should also allow assignment
// - to be moved-from object.

// where the programmer
// - knows that a value will not be used again
// - but the compiler cannot be expected to be smart enough
// - to figure that out, but the programmer can be specific

Vector f()
{
    Vector x(1000);
    Vector y(1000);
    Vector z(1000);
    
    // we get a copy
    z = x;
    
    // we get a copy
    y = std::move(x); 
    
    // we get a move
    return z; 

};

// The standard-library function move()
// - returns does not actually move anything
// - instead it returns a reference to its arguments
// - from which we may move - an ravlue reference.




































