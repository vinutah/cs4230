// explicit overriding
//
// a function in a derived class overrides
// a virtual function in a base class
// if that function has exactly
// the same name and type
//
// problem
//
// in large hierarcies, its not always obvious if
// overriding was intended
// a function with a slightly different name or slightly
// different type may be intended to override 
// or
// it may be intended to be a separate function
//
// solution
//
// override keyword
// to avoid confusion in such cases, a programmer can 
// explicitly state that a function is meant to override
//
// example


class Smiley : public Circle {
    public:
        Smiley(Point p, int r) :Circle{p,r}, mouth{nullptr} {}

        ~Smiley()
        {
            delete mouth;

            for(auto p : eyes)
                delete p;
        }

        // soppose move --> mve
        // then error
        // because no base of Smiley has a virtual
        // function called mve
        void move(Point to) override;

        void draw() const override;
        void rotate(int) override;

        void add_eye(Shape* s) { eyes.push_back(s); }
        void set_mouth(Shape* s);

        // cannot suffix with override
        // to the declaration of wink()
        virtual void wink(int i);

        // ...

    private:
        vector<Shape> eyes;
        Shape* mouth;
};

