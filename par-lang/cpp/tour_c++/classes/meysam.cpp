#include <iostream>
using namespace std;

class A
{
    public:
        int a1;
        int a2;
        A(int f1, int f2):
           a1{f1},
           a2{f2} 
        {}
};

class B
{
    public:
        int b;
        A ai{1,2};
        void C(void);
        B() { 
            b=2;
            C();
        }
};

void B::C(void)
{
    cout << this->ai.a1 << endl ;
    cout << this->ai.a2 << endl ;
}


int main(int argc, char* argv[])
{
    A ai(11,22);
    cout << ai.a1 << endl ;
    cout << ai.a2 << endl ;
    B bi;
    return 0;
}



