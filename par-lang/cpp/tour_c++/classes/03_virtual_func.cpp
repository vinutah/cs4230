// virtual functions

// use of container

void use(Container& c)
{
    const int sz = c.size();

    for(int i=-; i!=sz; ++i)
        cout << c[i] << '\n';
}

// how is the call c[i] in use()
// resolved to the right operator[]() ?
//
// h() --> user() --> List_container::operator[]()
// g() --> user() --> Vector_containter

// how to achieve this resolution?
//
// A Container object must contain information
// to allow it to select the right function to call
// at run time.

// implementation
//
// the usual implementation is for the compiler to 
// convert the name of a virtual function
// into an index into a table of pointers to functions
// table of pointers to functions --> vtbl, virtual table
//
// each class with virtual functions
// has its own vtbl identifying its virtual functions.

// The functions in the vtbl
// all the object to be used correctly even when the 
// - size of the object
// - layout of its data 
// are unknown to the caller.

// implementation of the caller
// needs only to know the location of the pointer
// to the vtbl in a Container 
// and the index used for each virtual function.

// Overhead
//
// - This virtual call mechanism can be make almost
//   as efficient as the "normal function call"
// - within 25% overhead
// - space overhead is one pointer
//   in each object of a class with virtual function plus one vtbl for each class.
























