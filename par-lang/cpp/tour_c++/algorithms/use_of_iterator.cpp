// Problem
//
// Simply and Efficiently sort a vector
// of Entrys and place a copy of each
// each unique vector element on a list

class Entry{
    string Name;
    int Number;

    bool operator<(const Entry& x,const Entry& y)
    {
        return x.name < y.name
    }
};

void f(vector<Entry>& vec, list<Entry>& lst)
{
    sort(vec.begin(),vec.end());
    unique_copy(vec.begin(),vec.end(),lst.begin());
}

// Problem
//
// Does a string s contain charecter c ?

bool has_c(const string& s, char c)
{
    // find is a standard algorithm
    // looks for a value in a sequence and returns
    // an iterator to the element found
    // Returns end() to indicate "not found"
    auto p = find(s.begin(), s.end(), c);
    
    // does string s contain c? Just say Yes or No
    if(p!=s.end())
        return true;
    else
        return false;
}

// Problem
//
// Write s equivalent, shorter definition of has_c()

bool has_c(const string& s, char c)
{
    // does string s contain c? Just say Yes or No
    return find(s.begin(),s.end(),c) != s.end();
}
