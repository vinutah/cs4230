// Templates
//
// 02. Use templates to express algorithms that apply to many arguments
//
// 03. Use templates to express containers
//
// 04. Use templates to raise the level of abstraction of code.
//
// 05. When defining a template
//
//      - First  : Design and debud a non-template version
//      - Second : Generalize by adding parameters
//
// 06. Templates are type-safe, but checking happens too late
//
// 07. A template can pass argument types without loss of information
//
// 08. Use function template argument to deduce class template argument types
//
// 09. Template provide a general mechanism for compile-time programming
//
// 10. When designing a template, 
//      
//      - Carefully consider the concepts (requirements) assumed
//        for its template argument
//
// 11. Use concepts as a design tool.
//
// 12. Use function objects as arguments to algorithm
//
// 13. Use a lambda if you need a simple function object in one place only
//
// 14. A virtual function member cannot be a template member function
//
// 15. Use template aliases to simplify notation and hide implementation details
//
// 16. Use variadic templates when you need a function that takes a variable number of arguments of a variety of types
//
// 17. Do not use variadic templates for homogeneous argument lists,  prefer initializer list for that
//
// 18. To use template, make sure that its definition, not just declaration is in scope
//
// 19. Templates offer compile time "duck typing"
//
// 20. There is no separate compilation of templates: #include template definition in every translation unit that uses them
 
// Parameterizes Types

class Vector {
    private:
        double* elem; // elem points to an array of sz elements of type T
        int sz;
    public:
        explicit Vector(int s);    // constructor: establish invariant, acquire resources
        ~Vector(){ delete[] elem;} // destructor: release resources

        // copy and move operations

        double& operator[] (int i);
        const T& operator[]() const;
        int size() const {return sz};
};

template <typename T>
class Vector {
    public:
        T* elem; //
    private:
        explicit Vector(int s);   // constructor: establish invariant, acquire resources
        ~Vector(){delete[] elem;} // destructor: release resources

        // copy and move operations

        T& operator[] (int i);
        const T& operator[] (int i) const ;
        int size() const {return sz};
        
}

// define Vectors
