#include <vector>
#include <iostream>
#include <exception>
using namespace std;

template <typename T>
class Vec : public std::vector<T> {
    public:
        using vector<T>::vector;

        T& operator[](int i)
        {return vector<T>::at(i);}
};

Vec<int> t1 {1,2,3};

int main(int argc, char *argv[])
{
    try{
        cout << t1[3];
    }
    catch(out_of_range){cerr << "mesam is sleepy";}
    catch(...){cerr << "i was just hit by a ufo";}

    return 0;
}

