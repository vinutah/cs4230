//Do not wish for an easy life. Wish for the strength to endure a difficult one. - Bruce Lee


// modularity

// map
//
// introduction
//
// separate compilation
//
// namespace
//
// error handling
//  
// exceptions
//
// invariants 
//
// static assertions
//
// advice


// introduction

// a c++ program consists of many separately developed parts
//  functions
//  user defined types
//  class hierarchies
//  templates

// how to manage all these ?
//
// kep = clearly define the interactions amoung those parts
// 
// 1. step-1
// the first and most important step is to distinguish between 
// the interface to a part and 
// its implementation.
//
// c++ represents interfaces by declarations.
//
// a declaration specifies all that is needed to use a function
// or a type

// example

// the square root function takes a double and returns a double
double sqrt(double);

class Vector {
    public:
        Vector(int s);
        double& operator[](int i);
        int size;
    private:
        double* elem; // elem points to an array of sz doubles
        int sz;
};


// KEY POINT
// the function bodies, the function definitions are elsewhere
// represenation of Vector elsewhere
// definition of sqrt() 

// definition of sqrt()

double sqrt(double d) // definition of sqrt()
{
    // ...algorithm as found in the math textbook ...
}


For Vector, we need to define all three member functions:

Vector::Vector(int s) //definition of the constructor
    :elem{new double[s]},sz{s} // initialize members
{
}

double& Vector::operator[](int i) // defitnitio of subscripting
{
    return elem[i];
}

int Vector::size()  // definition of size()
{
    return sz;
}

// separate compilation

// c++ supports a notion of separate compilation
//  user code sees only declarations of the types
//  and functions are in a separate source files
//  compiled separately.
//
//  used to organize a program into a set of semi
//  independent code fragments.
//
//  separation - minimize compilation times
//  bla - bla .. 

// Vector.h

class Vector {
    public:
        Vector(int s);
        double& operator[](int i);
        int size();
    private:
        double* elem; // elem points to an array of sz doubles
        int sz;
};

// this declaration would be placed in a file Vector.h
// and users will include that file, called header file
// to access that interface.
//
// user.cpp


#include "Vector.h" //get Vector's interface
#include <cmath> // get the standard-library math function interface including sqrt()

using namespace std; // make std members visible

double sqrt_sum(Vector& v)
{
    double sum = 0;
    for(int i = 0; i!=v.size(); ++i)
        sum += sqrt(v[i]); // sum of square roots
    return sum;
}


// help the compiler ensure consistency
// .cpp file providing the implementation of Vector
// will also include the .h file providing its interface

// Vector.cpp:

#include "Vector" // get the interface

Vector::Vector(int s)
    :elem{new double[s]},sz{s} // initialize members
{
}

double& Vector::operation[](int i)
{
    return elem[i];
}

int Vector::size()
{
    return sz;
}

// the code in user.cpp
// and Vector.cpp
// shares the Vector interface information
// presented in Vector.h
//
// maximize modularity
//
// effective separate compilation

// namespace

// in addition to 
// - functions, 
// - classes
// - enumerations
// 
// c++ offers namespace 
// as a mechanism for expressing
// that some declarations delong togerher
// that their names should not clash with other names

// examples

// experiment with my own complex number type



namespace My_code {
    class complex {
        // ...
    };

    complex sqrt(complex);
    // ...

    int main();
}

int My_code::main()
{
    complex z {1,2};
    auto z2 = sqrt(z);
    std::cout << '{' << z2.real() << ',' << z2.imag()  << "}\n" ;
    // ...
}

int main()
{
    return My_code::main();
}

// put my code in namespace My_code

// I make sure that my names do not conflict
// with the standard-library names in the namespace std

// the precation is wise.
// because I am make sure that
// my names do not conflict with
// the standard-library names in namespace std

// the precaution is wise, the standard library
// does provide support for complex arithmetic

// the simplest way to access a name in another namespace
// is to qualify it with the namespace name
// std::cout and My_code::main

// the real main() is defined is the glocal namespace
// not local to a defined 
// namespace 
// class
// or function

// how to gain access to names in the standard-library namespace
// using-directive

using namespace std;


// error handling

// c++ features to help this topic

// major tool
// type system itself.

// its good idea
// articulate a strategy for error handling
// early on in the development of a program.

// exceptions

// vector example

// what ought to be done when we try to access
// an element that is out of range ?

// the writer of Vector does not know
// what the  ...

double& Vector::operator[](int i)
{
    if(i < 0 || size() <=i )
        throw out_of_range{"Vector::operator[]"};
    return elem[i];
}

// throw transfers control to a handler
// for exceptions of type out_of_range
// in some function that called Vector::operator[]()

void f(Vector& v)
{
    // ...
    try { // exceptions here are handled by the handler defined below

        v[v.size()] = 7; // try to access beyond the end of v
    }
    catch (out_of_range) { // oops: out_of_range error
        // ...handle range error ...
    }
    // ...
}

// try-block

// we put code for which we are interested in handling
// exceptions into a try-block.

// catch-clause

// provides a handler for out_of_range
// enter the handler

// <stdexcept>
// The out_of_range type is defined in the standard library
// <stdexcept> used in standard library

// exception handling makes error handling simpler
// more systematic
// more readable
// dont overuse try-statements.
// the main technique for making error handling simple and systematic
// called RAII

// a function that should never throw an exception can be declared
// noexcept.

void user(int sz) noexcept
{
    Vector v(sz);
    iota(&v[0],&v[sz],1); // fill v with 1,2,3,4...
    // ...
}

// if all good intent and planning fails
// so that user() still throws, the standard-library function
// terminate() is called to immediately terminate the program

// invariants

Vector v(-27);

Vector::Vector(int s)
{
    if (s<0)
        throw length_error{"Vector constuctor: negative size"};
    elem = new double[s];
    sz = s;
}

void test()
{
    try{
        Vector v(-27);
    }
    catch (std::length_error){
        // handle negative size
    }
    catch (std::bad_alloc){
        // handle memory exhaustion
    }
}

// you can

// no way of completing its assigned task
// after an exception is thown.

// rethow

void test()
{
    try{
        Vector v(-27);
    }
    catch (std::length_error){
        cout << "test failed: length error\n";
        throw;  // rethrow
    }
    catch (std::bad_alloc){
        // ouch! test() is not designed to handle memory exaustion
        std::termiate(); // terminate the program

    }
}

// often a function has no way of completing
// its assigned task after an exception is thrown.

// the notion of invatiants..
// helps us to understand precisily what we want
// forces us to be specific
// that gives us a better chance of getting our code
// correct , after debugging and testing

// static assertions

// 1. Exceptions report errors found at run time.
// 2. If an error can be found at compile time
//      - It is usually preferable to do so.
// 3. That is what much of the type system and the facilities
// for specifying the interfaces to user-defined types are for.

// however
// we can also perform simple checks on other properties
// that are know at compile time and
// report failures as compiler error messages/

// examples

statis_assert(4<=sizeof(int),"Intergers are too small"); // check interger size

// checks if the int on this system does not 
// have atleast 4 bytes.
// these are assertions.

// static_assertion mechanism can be used for anything
// that can be expressed in terms of constant expressions

// example

constexpr double C = 299792.458; // km/s

void f(double speed)
{
    const double local_max = 160.0/(60*60);
    static_assert(speed<C, "cannont go that fast"); // error: speed must be a constant
    static_assert(local_max<C,"cannot go that fast"); // OK
}

// in general, static_assert(A,S) prints S
// as a compiler error message if A is not true.

// the most important uses of static_assert come
// when we make assertions about types used as
// paramters in generic programming
















