# Install script for directory: /home/vinu/cs4230/par-lang/cpp/cling-on/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/vinu/cs4230/par-lang/cpp/cling-on/inst")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "llvm-headers")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES
    "/home/vinu/cs4230/par-lang/cpp/cling-on/src/include/llvm"
    "/home/vinu/cs4230/par-lang/cpp/cling-on/src/include/llvm-c"
    FILES_MATCHING REGEX "/[^/]*\\.def$" REGEX "/[^/]*\\.h$" REGEX "/[^/]*\\.td$" REGEX "/[^/]*\\.inc$" REGEX "/LICENSE\\.TXT$" REGEX "/\\.svn$" EXCLUDE)
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "llvm-headers")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/vinu/cs4230/par-lang/cpp/cling-on/obj/include/llvm" FILES_MATCHING REGEX "/[^/]*\\.def$" REGEX "/[^/]*\\.h$" REGEX "/[^/]*\\.gen$" REGEX "/[^/]*\\.inc$" REGEX "/CMakeFiles$" EXCLUDE REGEX "/config\\.h$" EXCLUDE REGEX "/\\.svn$" EXCLUDE)
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/Demangle/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/Support/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/TableGen/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/utils/TableGen/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/include/llvm/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/utils/FileCheck/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/utils/PerfectShuffle/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/utils/count/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/utils/not/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/utils/llvm-lit/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/utils/yaml-bench/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/projects/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/tools/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/runtimes/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/examples/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/test/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/unittests/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/utils/unittest/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/docs/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/cmake/modules/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/vinu/cs4230/par-lang/cpp/cling-on/obj/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
