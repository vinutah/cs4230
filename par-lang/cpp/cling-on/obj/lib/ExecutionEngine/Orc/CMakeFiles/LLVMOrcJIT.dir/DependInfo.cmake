# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/lib/ExecutionEngine/Orc/ExecutionUtils.cpp" "/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/ExecutionUtils.cpp.o"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/lib/ExecutionEngine/Orc/IndirectionUtils.cpp" "/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/IndirectionUtils.cpp.o"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/lib/ExecutionEngine/Orc/NullResolver.cpp" "/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/NullResolver.cpp.o"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/lib/ExecutionEngine/Orc/OrcABISupport.cpp" "/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/OrcABISupport.cpp.o"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/lib/ExecutionEngine/Orc/OrcCBindings.cpp" "/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/OrcCBindings.cpp.o"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/lib/ExecutionEngine/Orc/OrcError.cpp" "/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/OrcError.cpp.o"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/lib/ExecutionEngine/Orc/OrcMCJITReplacement.cpp" "/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/OrcMCJITReplacement.cpp.o"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/lib/ExecutionEngine/Orc/RPCUtils.cpp" "/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/ExecutionEngine/Orc/CMakeFiles/LLVMOrcJIT.dir/RPCUtils.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "LLVM_BUILD_GLOBAL_ISEL"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "lib/ExecutionEngine/Orc"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/lib/ExecutionEngine/Orc"
  "include"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
