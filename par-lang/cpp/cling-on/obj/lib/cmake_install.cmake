# Install script for directory: /home/vinu/cs4230/par-lang/cpp/cling-on/src/lib

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/vinu/cs4230/par-lang/cpp/cling-on/inst")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/IR/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/IRReader/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/CodeGen/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/BinaryFormat/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/Bitcode/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/Transforms/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/Linker/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/Analysis/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/LTO/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/MC/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/Object/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/ObjectYAML/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/Option/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/DebugInfo/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/ExecutionEngine/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/Target/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/AsmParser/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/LineEditor/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/ProfileData/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/Fuzzer/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/Passes/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/ToolDrivers/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/XRay/cmake_install.cmake")
  include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/Testing/cmake_install.cmake")

endif()

