# This file allows users to call find_package(Clang) and pick up our targets.



find_package(LLVM REQUIRED CONFIG
             HINTS "/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/cmake/llvm")

set(CLANG_EXPORTED_TARGETS "clangBasic;clangLex;clangParse;clangAST;clangDynamicASTMatchers;clangASTMatchers;clangSema;clangCodeGen;clangAnalysis;clangEdit;clangRewrite;clangARCMigrate;clangDriver;clangSerialization;clangRewriteFrontend;clangFrontend;clangFrontendTool;clangToolingCore;clangToolingRefactor;clangTooling;clangIndex;clangStaticAnalyzerCore;clangStaticAnalyzerCheckers;clangStaticAnalyzerFrontend;clangFormat;clang;clang-format;clang-import-test;libclang")
set(CLANG_CMAKE_DIR "/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/cmake/clang")
set(CLANG_INCLUDE_DIRS "/home/vinu/cs4230/par-lang/cpp/cling-on/src/tools/clang/include;/home/vinu/cs4230/par-lang/cpp/cling-on/obj/tools/clang/include")

# Provide all our library targets to users.
include("/home/vinu/cs4230/par-lang/cpp/cling-on/obj/lib/cmake/clang/ClangTargets.cmake")
