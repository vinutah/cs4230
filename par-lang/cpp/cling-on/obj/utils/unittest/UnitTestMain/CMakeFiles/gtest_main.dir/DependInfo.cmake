# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/utils/unittest/UnitTestMain/TestMain.cpp" "/home/vinu/cs4230/par-lang/cpp/cling-on/obj/utils/unittest/UnitTestMain/CMakeFiles/gtest_main.dir/TestMain.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "LLVM_BUILD_GLOBAL_ISEL"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "utils/unittest/UnitTestMain"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/utils/unittest/UnitTestMain"
  "include"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/include"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/utils/unittest/googletest/include"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/utils/unittest/googletest"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/utils/unittest/googlemock/include"
  "/home/vinu/cs4230/par-lang/cpp/cling-on/src/utils/unittest/googlemock"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
