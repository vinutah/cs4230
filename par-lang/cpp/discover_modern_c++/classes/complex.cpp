class complex
{
  public:
    double r, i;
};


#include <iostream>
int main()
{
  // do not differ from intrinsic types, this defines objects z and c
  // type followed by variable name, or a list of variable names
  //
  complex z, c;

  // members accessed by dot operator
  // written as ordinary variables
  //
  z.r = 3.5; z.i = 2;

  // read as ordinary variables
  std::cout << "z is (" << z.r << ", " << z.i << " )\n";
}
