/*
   c++ also contains the struct keyword
   it declares a class, has all its features
   all members are public by default

*/

#include <iostream>
using namespace std;

struct xyz
{
  public:
    int test;
  private:
    int solutions;
};

class pqr
{
  public:
    int test;
};

int main(int argc, char *argv[])
{
  xyz one;
  cout << one.test << endl;
  cout << one.solutions << endl;
  return 0;
}
