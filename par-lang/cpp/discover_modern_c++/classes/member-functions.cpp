/*
 * functions in classes are called member functions or methods
 *
 * typical member functions in object oriennted software
 * are getters and setters
 *
 * listing 2-1
 *
 */

class complex
{
  public:
    double get_r() {return r;} // causes clumsy
    void set_r(double newr) { r = newr; } //code
};

at the beginning of our main
