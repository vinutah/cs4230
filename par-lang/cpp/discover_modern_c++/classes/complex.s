GAS LISTING /tmp/ccZP9VrL.s 			page 1


   1              		.file	"complex.c"
   2              		.text
   3              	.Ltext0:
   4              		.local	_ZStL8__ioinit
   5              		.comm	_ZStL8__ioinit,1,1
   6              		.section	.rodata
   7              	.LC2:
   8 0000 7A206973 		.string	"z is ("
   8      202800
   9              	.LC3:
  10 0007 2C2000   		.string	", "
  11              	.LC4:
  12 000a 20290A00 		.string	" )\n"
  13              		.text
  14              		.globl	main
  15              		.type	main, @function
  16              	main:
  17              	.LFB1021:
  18              		.file 1 "complex.c"
   1:complex.c     **** class complex
   2:complex.c     **** {
   3:complex.c     ****   public:
   4:complex.c     ****     double r, i;
   5:complex.c     **** };
   6:complex.c     **** 
   7:complex.c     **** 
   8:complex.c     **** #include <iostream>
   9:complex.c     **** int main()
  10:complex.c     **** {
  19              		.loc 1 10 0
  20              		.cfi_startproc
  21 0000 55       		pushq	%rbp
  22              		.cfi_def_cfa_offset 16
  23              		.cfi_offset 6, -16
  24 0001 4889E5   		movq	%rsp, %rbp
  25              		.cfi_def_cfa_register 6
  26 0004 4154     		pushq	%r12
  27 0006 53       		pushq	%rbx
  28 0007 4883EC20 		subq	$32, %rsp
  29              		.cfi_offset 12, -24
  30              		.cfi_offset 3, -32
  11:complex.c     ****   // do not differ from intrinsic types, this defines objects z and c
  12:complex.c     ****   // type followed by variable name, or a list of variable names
  13:complex.c     ****   //
  14:complex.c     ****   complex z, c;
  15:complex.c     **** 
  16:complex.c     ****   // members accessed by dot operator
  17:complex.c     ****   // written as ordinary variables
  18:complex.c     ****   //
  19:complex.c     ****   z.r = 3.5; z.i = 2;
  31              		.loc 1 19 0
  32 000b F20F1005 		movsd	.LC0(%rip), %xmm0
  32      00000000 
  33 0013 F20F1145 		movsd	%xmm0, -32(%rbp)
  33      E0
  34 0018 F20F1005 		movsd	.LC1(%rip), %xmm0
  34      00000000 
GAS LISTING /tmp/ccZP9VrL.s 			page 2


  35 0020 F20F1145 		movsd	%xmm0, -24(%rbp)
  35      E8
  20:complex.c     **** 
  21:complex.c     ****   // read as ordinary variables
  22:complex.c     ****   std::cout << "z is (" << z.r << ", " << z.i << " )\n";
  36              		.loc 1 22 0
  37 0025 488B5DE8 		movq	-24(%rbp), %rbx
  38 0029 4C8B65E0 		movq	-32(%rbp), %r12
  39 002d BE000000 		movl	$.LC2, %esi
  39      00
  40 0032 BF000000 		movl	$_ZSt4cout, %edi
  40      00
  41 0037 E8000000 		call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
  41      00
  42 003c 4C8965D8 		movq	%r12, -40(%rbp)
  43 0040 F20F1045 		movsd	-40(%rbp), %xmm0
  43      D8
  44 0045 4889C7   		movq	%rax, %rdi
  45 0048 E8000000 		call	_ZNSolsEd
  45      00
  46 004d BE000000 		movl	$.LC3, %esi
  46      00
  47 0052 4889C7   		movq	%rax, %rdi
  48 0055 E8000000 		call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
  48      00
  49 005a 48895DD8 		movq	%rbx, -40(%rbp)
  50 005e F20F1045 		movsd	-40(%rbp), %xmm0
  50      D8
  51 0063 4889C7   		movq	%rax, %rdi
  52 0066 E8000000 		call	_ZNSolsEd
  52      00
  53 006b BE000000 		movl	$.LC4, %esi
  53      00
  54 0070 4889C7   		movq	%rax, %rdi
  55 0073 E8000000 		call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
  55      00
  23:complex.c     **** }
  56              		.loc 1 23 0
  57 0078 B8000000 		movl	$0, %eax
  57      00
  58 007d 4883C420 		addq	$32, %rsp
  59 0081 5B       		popq	%rbx
  60 0082 415C     		popq	%r12
  61 0084 5D       		popq	%rbp
  62              		.cfi_def_cfa 7, 8
  63 0085 C3       		ret
  64              		.cfi_endproc
  65              	.LFE1021:
  66              		.size	main, .-main
  67              		.type	_Z41__static_initialization_and_destruction_0ii, @function
  68              	_Z41__static_initialization_and_destruction_0ii:
  69              	.LFB1026:
  70              		.loc 1 23 0
  71              		.cfi_startproc
  72 0086 55       		pushq	%rbp
  73              		.cfi_def_cfa_offset 16
  74              		.cfi_offset 6, -16
GAS LISTING /tmp/ccZP9VrL.s 			page 3


  75 0087 4889E5   		movq	%rsp, %rbp
  76              		.cfi_def_cfa_register 6
  77 008a 4883EC10 		subq	$16, %rsp
  78 008e 897DFC   		movl	%edi, -4(%rbp)
  79 0091 8975F8   		movl	%esi, -8(%rbp)
  80              		.loc 1 23 0
  81 0094 837DFC01 		cmpl	$1, -4(%rbp)
  82 0098 7527     		jne	.L5
  83              		.loc 1 23 0 is_stmt 0 discriminator 1
  84 009a 817DF8FF 		cmpl	$65535, -8(%rbp)
  84      FF0000
  85 00a1 751E     		jne	.L5
  86              		.file 2 "/usr/include/c++/5/iostream"
   1:/usr/include/c++/5/iostream **** // Standard iostream objects -*- C++ -*-
   2:/usr/include/c++/5/iostream **** 
   3:/usr/include/c++/5/iostream **** // Copyright (C) 1997-2015 Free Software Foundation, Inc.
   4:/usr/include/c++/5/iostream **** //
   5:/usr/include/c++/5/iostream **** // This file is part of the GNU ISO C++ Library.  This library is free
   6:/usr/include/c++/5/iostream **** // software; you can redistribute it and/or modify it under the
   7:/usr/include/c++/5/iostream **** // terms of the GNU General Public License as published by the
   8:/usr/include/c++/5/iostream **** // Free Software Foundation; either version 3, or (at your option)
   9:/usr/include/c++/5/iostream **** // any later version.
  10:/usr/include/c++/5/iostream **** 
  11:/usr/include/c++/5/iostream **** // This library is distributed in the hope that it will be useful,
  12:/usr/include/c++/5/iostream **** // but WITHOUT ANY WARRANTY; without even the implied warranty of
  13:/usr/include/c++/5/iostream **** // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  14:/usr/include/c++/5/iostream **** // GNU General Public License for more details.
  15:/usr/include/c++/5/iostream **** 
  16:/usr/include/c++/5/iostream **** // Under Section 7 of GPL version 3, you are granted additional
  17:/usr/include/c++/5/iostream **** // permissions described in the GCC Runtime Library Exception, version
  18:/usr/include/c++/5/iostream **** // 3.1, as published by the Free Software Foundation.
  19:/usr/include/c++/5/iostream **** 
  20:/usr/include/c++/5/iostream **** // You should have received a copy of the GNU General Public License and
  21:/usr/include/c++/5/iostream **** // a copy of the GCC Runtime Library Exception along with this program;
  22:/usr/include/c++/5/iostream **** // see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
  23:/usr/include/c++/5/iostream **** // <http://www.gnu.org/licenses/>.
  24:/usr/include/c++/5/iostream **** 
  25:/usr/include/c++/5/iostream **** /** @file include/iostream
  26:/usr/include/c++/5/iostream ****  *  This is a Standard C++ Library header.
  27:/usr/include/c++/5/iostream ****  */
  28:/usr/include/c++/5/iostream **** 
  29:/usr/include/c++/5/iostream **** //
  30:/usr/include/c++/5/iostream **** // ISO C++ 14882: 27.3  Standard iostream objects
  31:/usr/include/c++/5/iostream **** //
  32:/usr/include/c++/5/iostream **** 
  33:/usr/include/c++/5/iostream **** #ifndef _GLIBCXX_IOSTREAM
  34:/usr/include/c++/5/iostream **** #define _GLIBCXX_IOSTREAM 1
  35:/usr/include/c++/5/iostream **** 
  36:/usr/include/c++/5/iostream **** #pragma GCC system_header
  37:/usr/include/c++/5/iostream **** 
  38:/usr/include/c++/5/iostream **** #include <bits/c++config.h>
  39:/usr/include/c++/5/iostream **** #include <ostream>
  40:/usr/include/c++/5/iostream **** #include <istream>
  41:/usr/include/c++/5/iostream **** 
  42:/usr/include/c++/5/iostream **** namespace std _GLIBCXX_VISIBILITY(default)
  43:/usr/include/c++/5/iostream **** {
  44:/usr/include/c++/5/iostream **** _GLIBCXX_BEGIN_NAMESPACE_VERSION
GAS LISTING /tmp/ccZP9VrL.s 			page 4


  45:/usr/include/c++/5/iostream **** 
  46:/usr/include/c++/5/iostream ****   /**
  47:/usr/include/c++/5/iostream ****    *  @name Standard Stream Objects
  48:/usr/include/c++/5/iostream ****    *
  49:/usr/include/c++/5/iostream ****    *  The &lt;iostream&gt; header declares the eight <em>standard stream
  50:/usr/include/c++/5/iostream ****    *  objects</em>.  For other declarations, see
  51:/usr/include/c++/5/iostream ****    *  http://gcc.gnu.org/onlinedocs/libstdc++/manual/io.html
  52:/usr/include/c++/5/iostream ****    *  and the @link iosfwd I/O forward declarations @endlink
  53:/usr/include/c++/5/iostream ****    *
  54:/usr/include/c++/5/iostream ****    *  They are required by default to cooperate with the global C
  55:/usr/include/c++/5/iostream ****    *  library's @c FILE streams, and to be available during program
  56:/usr/include/c++/5/iostream ****    *  startup and termination. For more information, see the section of the
  57:/usr/include/c++/5/iostream ****    *  manual linked to above.
  58:/usr/include/c++/5/iostream ****   */
  59:/usr/include/c++/5/iostream ****   //@{
  60:/usr/include/c++/5/iostream ****   extern istream cin;		/// Linked to standard input
  61:/usr/include/c++/5/iostream ****   extern ostream cout;		/// Linked to standard output
  62:/usr/include/c++/5/iostream ****   extern ostream cerr;		/// Linked to standard error (unbuffered)
  63:/usr/include/c++/5/iostream ****   extern ostream clog;		/// Linked to standard error (buffered)
  64:/usr/include/c++/5/iostream **** 
  65:/usr/include/c++/5/iostream **** #ifdef _GLIBCXX_USE_WCHAR_T
  66:/usr/include/c++/5/iostream ****   extern wistream wcin;		/// Linked to standard input
  67:/usr/include/c++/5/iostream ****   extern wostream wcout;	/// Linked to standard output
  68:/usr/include/c++/5/iostream ****   extern wostream wcerr;	/// Linked to standard error (unbuffered)
  69:/usr/include/c++/5/iostream ****   extern wostream wclog;	/// Linked to standard error (buffered)
  70:/usr/include/c++/5/iostream **** #endif
  71:/usr/include/c++/5/iostream ****   //@}
  72:/usr/include/c++/5/iostream **** 
  73:/usr/include/c++/5/iostream ****   // For construction of filebuffers for cout, cin, cerr, clog et. al.
  74:/usr/include/c++/5/iostream ****   static ios_base::Init __ioinit;
  87              		.loc 2 74 0 is_stmt 1
  88 00a3 BF000000 		movl	$_ZStL8__ioinit, %edi
  88      00
  89 00a8 E8000000 		call	_ZNSt8ios_base4InitC1Ev
  89      00
  90 00ad BA000000 		movl	$__dso_handle, %edx
  90      00
  91 00b2 BE000000 		movl	$_ZStL8__ioinit, %esi
  91      00
  92 00b7 BF000000 		movl	$_ZNSt8ios_base4InitD1Ev, %edi
  92      00
  93 00bc E8000000 		call	__cxa_atexit
  93      00
  94              	.L5:
  95              		.loc 1 23 0
  96 00c1 90       		nop
  97 00c2 C9       		leave
  98              		.cfi_def_cfa 7, 8
  99 00c3 C3       		ret
 100              		.cfi_endproc
 101              	.LFE1026:
 102              		.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destructi
 103              		.type	_GLOBAL__sub_I_main, @function
 104              	_GLOBAL__sub_I_main:
 105              	.LFB1027:
 106              		.loc 1 23 0
 107              		.cfi_startproc
GAS LISTING /tmp/ccZP9VrL.s 			page 5


 108 00c4 55       		pushq	%rbp
 109              		.cfi_def_cfa_offset 16
 110              		.cfi_offset 6, -16
 111 00c5 4889E5   		movq	%rsp, %rbp
 112              		.cfi_def_cfa_register 6
 113              		.loc 1 23 0
 114 00c8 BEFFFF00 		movl	$65535, %esi
 114      00
 115 00cd BF010000 		movl	$1, %edi
 115      00
 116 00d2 E8AFFFFF 		call	_Z41__static_initialization_and_destruction_0ii
 116      FF
 117 00d7 5D       		popq	%rbp
 118              		.cfi_def_cfa 7, 8
 119 00d8 C3       		ret
 120              		.cfi_endproc
 121              	.LFE1027:
 122              		.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
 123              		.section	.init_array,"aw"
 124              		.align 8
 125 0000 00000000 		.quad	_GLOBAL__sub_I_main
 125      00000000 
 126              		.section	.rodata
 127 000e 0000     		.align 8
 128              	.LC0:
 129 0010 00000000 		.long	0
 130 0014 00000C40 		.long	1074528256
 131              		.align 8
 132              	.LC1:
 133 0018 00000000 		.long	0
 134 001c 00000040 		.long	1073741824
 135              		.text
 136              	.Letext0:
 137              		.file 3 "/usr/include/c++/5/cwchar"
 138              		.file 4 "/usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h"
 139              		.file 5 "/usr/include/c++/5/debug/debug.h"
 140              		.file 6 "/usr/include/c++/5/bits/char_traits.h"
 141              		.file 7 "/usr/include/c++/5/clocale"
 142              		.file 8 "/usr/include/c++/5/bits/ios_base.h"
 143              		.file 9 "/usr/include/c++/5/cwctype"
 144              		.file 10 "/usr/include/c++/5/iosfwd"
 145              		.file 11 "/usr/include/c++/5/bits/predefined_ops.h"
 146              		.file 12 "/usr/include/c++/5/ext/new_allocator.h"
 147              		.file 13 "/usr/include/c++/5/ext/numeric_traits.h"
 148              		.file 14 "/usr/include/stdio.h"
 149              		.file 15 "<built-in>"
 150              		.file 16 "/usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h"
 151              		.file 17 "/usr/include/wchar.h"
 152              		.file 18 "/usr/include/time.h"
 153              		.file 19 "/usr/include/locale.h"
 154              		.file 20 "/usr/include/x86_64-linux-gnu/bits/types.h"
 155              		.file 21 "/usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h"
 156              		.file 22 "/usr/include/wctype.h"
 157              		.section	.debug_info,"",@progbits
 158              	.Ldebug_info0:
 159 0000 FB150000 		.long	0x15fb
 160 0004 0400     		.value	0x4
GAS LISTING /tmp/ccZP9VrL.s 			page 6


 161 0006 00000000 		.long	.Ldebug_abbrev0
 162 000a 08       		.byte	0x8
 163 000b 01       		.uleb128 0x1
 164 000c 00000000 		.long	.LASF264
 165 0010 04       		.byte	0x4
 166 0011 00000000 		.long	.LASF265
 167 0015 00000000 		.long	.LASF266
 168 0019 00000000 		.quad	.Ltext0
 168      00000000 
 169 0021 D9000000 		.quad	.Letext0-.Ltext0
 169      00000000 
 170 0029 00000000 		.long	.Ldebug_line0
 171 002d 02       		.uleb128 0x2
 172 002e 00000000 		.long	.LASF267
 173 0032 10       		.byte	0x10
 174 0033 01       		.byte	0x1
 175 0034 01       		.byte	0x1
 176 0035 50000000 		.long	0x50
 177 0039 03       		.uleb128 0x3
 178 003a 7200     		.string	"r"
 179 003c 01       		.byte	0x1
 180 003d 04       		.byte	0x4
 181 003e 50000000 		.long	0x50
 182 0042 00       		.byte	0
 183 0043 01       		.byte	0x1
 184 0044 03       		.uleb128 0x3
 185 0045 6900     		.string	"i"
 186 0047 01       		.byte	0x1
 187 0048 04       		.byte	0x4
 188 0049 50000000 		.long	0x50
 189 004d 08       		.byte	0x8
 190 004e 01       		.byte	0x1
 191 004f 00       		.byte	0
 192 0050 04       		.uleb128 0x4
 193 0051 08       		.byte	0x8
 194 0052 04       		.byte	0x4
 195 0053 00000000 		.long	.LASF118
 196 0057 05       		.uleb128 0x5
 197 0058 73746400 		.string	"std"
 198 005c 0F       		.byte	0xf
 199 005d 00       		.byte	0
 200 005e 57080000 		.long	0x857
 201 0062 06       		.uleb128 0x6
 202 0063 00000000 		.long	.LASF0
 203 0067 04       		.byte	0x4
 204 0068 DA       		.byte	0xda
 205 0069 07       		.uleb128 0x7
 206 006a 04       		.byte	0x4
 207 006b DA       		.byte	0xda
 208 006c 62000000 		.long	0x62
 209 0070 08       		.uleb128 0x8
 210 0071 03       		.byte	0x3
 211 0072 40       		.byte	0x40
 212 0073 CB0A0000 		.long	0xacb
 213 0077 08       		.uleb128 0x8
 214 0078 03       		.byte	0x3
 215 0079 8B       		.byte	0x8b
GAS LISTING /tmp/ccZP9VrL.s 			page 7


 216 007a 520A0000 		.long	0xa52
 217 007e 08       		.uleb128 0x8
 218 007f 03       		.byte	0x3
 219 0080 8D       		.byte	0x8d
 220 0081 ED0A0000 		.long	0xaed
 221 0085 08       		.uleb128 0x8
 222 0086 03       		.byte	0x3
 223 0087 8E       		.byte	0x8e
 224 0088 030B0000 		.long	0xb03
 225 008c 08       		.uleb128 0x8
 226 008d 03       		.byte	0x3
 227 008e 8F       		.byte	0x8f
 228 008f 1F0B0000 		.long	0xb1f
 229 0093 08       		.uleb128 0x8
 230 0094 03       		.byte	0x3
 231 0095 90       		.byte	0x90
 232 0096 4C0B0000 		.long	0xb4c
 233 009a 08       		.uleb128 0x8
 234 009b 03       		.byte	0x3
 235 009c 91       		.byte	0x91
 236 009d 670B0000 		.long	0xb67
 237 00a1 08       		.uleb128 0x8
 238 00a2 03       		.byte	0x3
 239 00a3 92       		.byte	0x92
 240 00a4 8D0B0000 		.long	0xb8d
 241 00a8 08       		.uleb128 0x8
 242 00a9 03       		.byte	0x3
 243 00aa 93       		.byte	0x93
 244 00ab A80B0000 		.long	0xba8
 245 00af 08       		.uleb128 0x8
 246 00b0 03       		.byte	0x3
 247 00b1 94       		.byte	0x94
 248 00b2 C40B0000 		.long	0xbc4
 249 00b6 08       		.uleb128 0x8
 250 00b7 03       		.byte	0x3
 251 00b8 95       		.byte	0x95
 252 00b9 E00B0000 		.long	0xbe0
 253 00bd 08       		.uleb128 0x8
 254 00be 03       		.byte	0x3
 255 00bf 96       		.byte	0x96
 256 00c0 F60B0000 		.long	0xbf6
 257 00c4 08       		.uleb128 0x8
 258 00c5 03       		.byte	0x3
 259 00c6 97       		.byte	0x97
 260 00c7 020C0000 		.long	0xc02
 261 00cb 08       		.uleb128 0x8
 262 00cc 03       		.byte	0x3
 263 00cd 98       		.byte	0x98
 264 00ce 280C0000 		.long	0xc28
 265 00d2 08       		.uleb128 0x8
 266 00d3 03       		.byte	0x3
 267 00d4 99       		.byte	0x99
 268 00d5 4D0C0000 		.long	0xc4d
 269 00d9 08       		.uleb128 0x8
 270 00da 03       		.byte	0x3
 271 00db 9A       		.byte	0x9a
 272 00dc 6E0C0000 		.long	0xc6e
GAS LISTING /tmp/ccZP9VrL.s 			page 8


 273 00e0 08       		.uleb128 0x8
 274 00e1 03       		.byte	0x3
 275 00e2 9B       		.byte	0x9b
 276 00e3 990C0000 		.long	0xc99
 277 00e7 08       		.uleb128 0x8
 278 00e8 03       		.byte	0x3
 279 00e9 9C       		.byte	0x9c
 280 00ea B40C0000 		.long	0xcb4
 281 00ee 08       		.uleb128 0x8
 282 00ef 03       		.byte	0x3
 283 00f0 9E       		.byte	0x9e
 284 00f1 CA0C0000 		.long	0xcca
 285 00f5 08       		.uleb128 0x8
 286 00f6 03       		.byte	0x3
 287 00f7 A0       		.byte	0xa0
 288 00f8 EB0C0000 		.long	0xceb
 289 00fc 08       		.uleb128 0x8
 290 00fd 03       		.byte	0x3
 291 00fe A1       		.byte	0xa1
 292 00ff 070D0000 		.long	0xd07
 293 0103 08       		.uleb128 0x8
 294 0104 03       		.byte	0x3
 295 0105 A2       		.byte	0xa2
 296 0106 220D0000 		.long	0xd22
 297 010a 08       		.uleb128 0x8
 298 010b 03       		.byte	0x3
 299 010c A4       		.byte	0xa4
 300 010d 480D0000 		.long	0xd48
 301 0111 08       		.uleb128 0x8
 302 0112 03       		.byte	0x3
 303 0113 A7       		.byte	0xa7
 304 0114 680D0000 		.long	0xd68
 305 0118 08       		.uleb128 0x8
 306 0119 03       		.byte	0x3
 307 011a AA       		.byte	0xaa
 308 011b 8D0D0000 		.long	0xd8d
 309 011f 08       		.uleb128 0x8
 310 0120 03       		.byte	0x3
 311 0121 AC       		.byte	0xac
 312 0122 AD0D0000 		.long	0xdad
 313 0126 08       		.uleb128 0x8
 314 0127 03       		.byte	0x3
 315 0128 AE       		.byte	0xae
 316 0129 C80D0000 		.long	0xdc8
 317 012d 08       		.uleb128 0x8
 318 012e 03       		.byte	0x3
 319 012f B0       		.byte	0xb0
 320 0130 E30D0000 		.long	0xde3
 321 0134 08       		.uleb128 0x8
 322 0135 03       		.byte	0x3
 323 0136 B1       		.byte	0xb1
 324 0137 090E0000 		.long	0xe09
 325 013b 08       		.uleb128 0x8
 326 013c 03       		.byte	0x3
 327 013d B2       		.byte	0xb2
 328 013e 230E0000 		.long	0xe23
 329 0142 08       		.uleb128 0x8
GAS LISTING /tmp/ccZP9VrL.s 			page 9


 330 0143 03       		.byte	0x3
 331 0144 B3       		.byte	0xb3
 332 0145 3D0E0000 		.long	0xe3d
 333 0149 08       		.uleb128 0x8
 334 014a 03       		.byte	0x3
 335 014b B4       		.byte	0xb4
 336 014c 570E0000 		.long	0xe57
 337 0150 08       		.uleb128 0x8
 338 0151 03       		.byte	0x3
 339 0152 B5       		.byte	0xb5
 340 0153 710E0000 		.long	0xe71
 341 0157 08       		.uleb128 0x8
 342 0158 03       		.byte	0x3
 343 0159 B6       		.byte	0xb6
 344 015a 8B0E0000 		.long	0xe8b
 345 015e 08       		.uleb128 0x8
 346 015f 03       		.byte	0x3
 347 0160 B7       		.byte	0xb7
 348 0161 4B0F0000 		.long	0xf4b
 349 0165 08       		.uleb128 0x8
 350 0166 03       		.byte	0x3
 351 0167 B8       		.byte	0xb8
 352 0168 610F0000 		.long	0xf61
 353 016c 08       		.uleb128 0x8
 354 016d 03       		.byte	0x3
 355 016e B9       		.byte	0xb9
 356 016f 800F0000 		.long	0xf80
 357 0173 08       		.uleb128 0x8
 358 0174 03       		.byte	0x3
 359 0175 BA       		.byte	0xba
 360 0176 9F0F0000 		.long	0xf9f
 361 017a 08       		.uleb128 0x8
 362 017b 03       		.byte	0x3
 363 017c BB       		.byte	0xbb
 364 017d BE0F0000 		.long	0xfbe
 365 0181 08       		.uleb128 0x8
 366 0182 03       		.byte	0x3
 367 0183 BC       		.byte	0xbc
 368 0184 E90F0000 		.long	0xfe9
 369 0188 08       		.uleb128 0x8
 370 0189 03       		.byte	0x3
 371 018a BD       		.byte	0xbd
 372 018b 04100000 		.long	0x1004
 373 018f 08       		.uleb128 0x8
 374 0190 03       		.byte	0x3
 375 0191 BF       		.byte	0xbf
 376 0192 25100000 		.long	0x1025
 377 0196 08       		.uleb128 0x8
 378 0197 03       		.byte	0x3
 379 0198 C1       		.byte	0xc1
 380 0199 47100000 		.long	0x1047
 381 019d 08       		.uleb128 0x8
 382 019e 03       		.byte	0x3
 383 019f C2       		.byte	0xc2
 384 01a0 67100000 		.long	0x1067
 385 01a4 08       		.uleb128 0x8
 386 01a5 03       		.byte	0x3
GAS LISTING /tmp/ccZP9VrL.s 			page 10


 387 01a6 C3       		.byte	0xc3
 388 01a7 8E100000 		.long	0x108e
 389 01ab 08       		.uleb128 0x8
 390 01ac 03       		.byte	0x3
 391 01ad C4       		.byte	0xc4
 392 01ae AE100000 		.long	0x10ae
 393 01b2 08       		.uleb128 0x8
 394 01b3 03       		.byte	0x3
 395 01b4 C5       		.byte	0xc5
 396 01b5 CD100000 		.long	0x10cd
 397 01b9 08       		.uleb128 0x8
 398 01ba 03       		.byte	0x3
 399 01bb C6       		.byte	0xc6
 400 01bc E3100000 		.long	0x10e3
 401 01c0 08       		.uleb128 0x8
 402 01c1 03       		.byte	0x3
 403 01c2 C7       		.byte	0xc7
 404 01c3 03110000 		.long	0x1103
 405 01c7 08       		.uleb128 0x8
 406 01c8 03       		.byte	0x3
 407 01c9 C8       		.byte	0xc8
 408 01ca 23110000 		.long	0x1123
 409 01ce 08       		.uleb128 0x8
 410 01cf 03       		.byte	0x3
 411 01d0 C9       		.byte	0xc9
 412 01d1 43110000 		.long	0x1143
 413 01d5 08       		.uleb128 0x8
 414 01d6 03       		.byte	0x3
 415 01d7 CA       		.byte	0xca
 416 01d8 63110000 		.long	0x1163
 417 01dc 08       		.uleb128 0x8
 418 01dd 03       		.byte	0x3
 419 01de CB       		.byte	0xcb
 420 01df 7A110000 		.long	0x117a
 421 01e3 08       		.uleb128 0x8
 422 01e4 03       		.byte	0x3
 423 01e5 CC       		.byte	0xcc
 424 01e6 91110000 		.long	0x1191
 425 01ea 08       		.uleb128 0x8
 426 01eb 03       		.byte	0x3
 427 01ec CD       		.byte	0xcd
 428 01ed AF110000 		.long	0x11af
 429 01f1 08       		.uleb128 0x8
 430 01f2 03       		.byte	0x3
 431 01f3 CE       		.byte	0xce
 432 01f4 CE110000 		.long	0x11ce
 433 01f8 08       		.uleb128 0x8
 434 01f9 03       		.byte	0x3
 435 01fa CF       		.byte	0xcf
 436 01fb EC110000 		.long	0x11ec
 437 01ff 08       		.uleb128 0x8
 438 0200 03       		.byte	0x3
 439 0201 D0       		.byte	0xd0
 440 0202 0B120000 		.long	0x120b
 441 0206 09       		.uleb128 0x9
 442 0207 03       		.byte	0x3
 443 0208 0801     		.value	0x108
GAS LISTING /tmp/ccZP9VrL.s 			page 11


 444 020a 2F120000 		.long	0x122f
 445 020e 09       		.uleb128 0x9
 446 020f 03       		.byte	0x3
 447 0210 0901     		.value	0x109
 448 0212 51120000 		.long	0x1251
 449 0216 09       		.uleb128 0x9
 450 0217 03       		.byte	0x3
 451 0218 0A01     		.value	0x10a
 452 021a 78120000 		.long	0x1278
 453 021e 06       		.uleb128 0x6
 454 021f 00000000 		.long	.LASF1
 455 0223 05       		.byte	0x5
 456 0224 30       		.byte	0x30
 457 0225 0A       		.uleb128 0xa
 458 0226 00000000 		.long	.LASF106
 459 022a 01       		.byte	0x1
 460 022b 06       		.byte	0x6
 461 022c E9       		.byte	0xe9
 462 022d ED030000 		.long	0x3ed
 463 0231 0B       		.uleb128 0xb
 464 0232 00000000 		.long	.LASF2
 465 0236 06       		.byte	0x6
 466 0237 EB       		.byte	0xeb
 467 0238 B20A0000 		.long	0xab2
 468 023c 0B       		.uleb128 0xb
 469 023d 00000000 		.long	.LASF3
 470 0241 06       		.byte	0x6
 471 0242 EC       		.byte	0xec
 472 0243 B90A0000 		.long	0xab9
 473 0247 0C       		.uleb128 0xc
 474 0248 00000000 		.long	.LASF16
 475 024c 06       		.byte	0x6
 476 024d F2       		.byte	0xf2
 477 024e 00000000 		.long	.LASF268
 478 0252 61020000 		.long	0x261
 479 0256 0D       		.uleb128 0xd
 480 0257 C7120000 		.long	0x12c7
 481 025b 0D       		.uleb128 0xd
 482 025c CD120000 		.long	0x12cd
 483 0260 00       		.byte	0
 484 0261 0E       		.uleb128 0xe
 485 0262 31020000 		.long	0x231
 486 0266 0F       		.uleb128 0xf
 487 0267 657100   		.string	"eq"
 488 026a 06       		.byte	0x6
 489 026b F6       		.byte	0xf6
 490 026c 00000000 		.long	.LASF4
 491 0270 D3120000 		.long	0x12d3
 492 0274 83020000 		.long	0x283
 493 0278 0D       		.uleb128 0xd
 494 0279 CD120000 		.long	0x12cd
 495 027d 0D       		.uleb128 0xd
 496 027e CD120000 		.long	0x12cd
 497 0282 00       		.byte	0
 498 0283 0F       		.uleb128 0xf
 499 0284 6C7400   		.string	"lt"
 500 0287 06       		.byte	0x6
GAS LISTING /tmp/ccZP9VrL.s 			page 12


 501 0288 FA       		.byte	0xfa
 502 0289 00000000 		.long	.LASF5
 503 028d D3120000 		.long	0x12d3
 504 0291 A0020000 		.long	0x2a0
 505 0295 0D       		.uleb128 0xd
 506 0296 CD120000 		.long	0x12cd
 507 029a 0D       		.uleb128 0xd
 508 029b CD120000 		.long	0x12cd
 509 029f 00       		.byte	0
 510 02a0 10       		.uleb128 0x10
 511 02a1 00000000 		.long	.LASF6
 512 02a5 06       		.byte	0x6
 513 02a6 0201     		.value	0x102
 514 02a8 00000000 		.long	.LASF8
 515 02ac B90A0000 		.long	0xab9
 516 02b0 C4020000 		.long	0x2c4
 517 02b4 0D       		.uleb128 0xd
 518 02b5 DA120000 		.long	0x12da
 519 02b9 0D       		.uleb128 0xd
 520 02ba DA120000 		.long	0x12da
 521 02be 0D       		.uleb128 0xd
 522 02bf ED030000 		.long	0x3ed
 523 02c3 00       		.byte	0
 524 02c4 10       		.uleb128 0x10
 525 02c5 00000000 		.long	.LASF7
 526 02c9 06       		.byte	0x6
 527 02ca 0A01     		.value	0x10a
 528 02cc 00000000 		.long	.LASF9
 529 02d0 ED030000 		.long	0x3ed
 530 02d4 DE020000 		.long	0x2de
 531 02d8 0D       		.uleb128 0xd
 532 02d9 DA120000 		.long	0x12da
 533 02dd 00       		.byte	0
 534 02de 10       		.uleb128 0x10
 535 02df 00000000 		.long	.LASF10
 536 02e3 06       		.byte	0x6
 537 02e4 0E01     		.value	0x10e
 538 02e6 00000000 		.long	.LASF11
 539 02ea DA120000 		.long	0x12da
 540 02ee 02030000 		.long	0x302
 541 02f2 0D       		.uleb128 0xd
 542 02f3 DA120000 		.long	0x12da
 543 02f7 0D       		.uleb128 0xd
 544 02f8 ED030000 		.long	0x3ed
 545 02fc 0D       		.uleb128 0xd
 546 02fd CD120000 		.long	0x12cd
 547 0301 00       		.byte	0
 548 0302 10       		.uleb128 0x10
 549 0303 00000000 		.long	.LASF12
 550 0307 06       		.byte	0x6
 551 0308 1601     		.value	0x116
 552 030a 00000000 		.long	.LASF13
 553 030e E0120000 		.long	0x12e0
 554 0312 26030000 		.long	0x326
 555 0316 0D       		.uleb128 0xd
 556 0317 E0120000 		.long	0x12e0
 557 031b 0D       		.uleb128 0xd
GAS LISTING /tmp/ccZP9VrL.s 			page 13


 558 031c DA120000 		.long	0x12da
 559 0320 0D       		.uleb128 0xd
 560 0321 ED030000 		.long	0x3ed
 561 0325 00       		.byte	0
 562 0326 10       		.uleb128 0x10
 563 0327 00000000 		.long	.LASF14
 564 032b 06       		.byte	0x6
 565 032c 1E01     		.value	0x11e
 566 032e 00000000 		.long	.LASF15
 567 0332 E0120000 		.long	0x12e0
 568 0336 4A030000 		.long	0x34a
 569 033a 0D       		.uleb128 0xd
 570 033b E0120000 		.long	0x12e0
 571 033f 0D       		.uleb128 0xd
 572 0340 DA120000 		.long	0x12da
 573 0344 0D       		.uleb128 0xd
 574 0345 ED030000 		.long	0x3ed
 575 0349 00       		.byte	0
 576 034a 10       		.uleb128 0x10
 577 034b 00000000 		.long	.LASF16
 578 034f 06       		.byte	0x6
 579 0350 2601     		.value	0x126
 580 0352 00000000 		.long	.LASF17
 581 0356 E0120000 		.long	0x12e0
 582 035a 6E030000 		.long	0x36e
 583 035e 0D       		.uleb128 0xd
 584 035f E0120000 		.long	0x12e0
 585 0363 0D       		.uleb128 0xd
 586 0364 ED030000 		.long	0x3ed
 587 0368 0D       		.uleb128 0xd
 588 0369 31020000 		.long	0x231
 589 036d 00       		.byte	0
 590 036e 10       		.uleb128 0x10
 591 036f 00000000 		.long	.LASF18
 592 0373 06       		.byte	0x6
 593 0374 2E01     		.value	0x12e
 594 0376 00000000 		.long	.LASF19
 595 037a 31020000 		.long	0x231
 596 037e 88030000 		.long	0x388
 597 0382 0D       		.uleb128 0xd
 598 0383 E6120000 		.long	0x12e6
 599 0387 00       		.byte	0
 600 0388 0E       		.uleb128 0xe
 601 0389 3C020000 		.long	0x23c
 602 038d 10       		.uleb128 0x10
 603 038e 00000000 		.long	.LASF20
 604 0392 06       		.byte	0x6
 605 0393 3401     		.value	0x134
 606 0395 00000000 		.long	.LASF21
 607 0399 3C020000 		.long	0x23c
 608 039d A7030000 		.long	0x3a7
 609 03a1 0D       		.uleb128 0xd
 610 03a2 CD120000 		.long	0x12cd
 611 03a6 00       		.byte	0
 612 03a7 10       		.uleb128 0x10
 613 03a8 00000000 		.long	.LASF22
 614 03ac 06       		.byte	0x6
GAS LISTING /tmp/ccZP9VrL.s 			page 14


 615 03ad 3801     		.value	0x138
 616 03af 00000000 		.long	.LASF23
 617 03b3 D3120000 		.long	0x12d3
 618 03b7 C6030000 		.long	0x3c6
 619 03bb 0D       		.uleb128 0xd
 620 03bc E6120000 		.long	0x12e6
 621 03c0 0D       		.uleb128 0xd
 622 03c1 E6120000 		.long	0x12e6
 623 03c5 00       		.byte	0
 624 03c6 11       		.uleb128 0x11
 625 03c7 656F6600 		.string	"eof"
 626 03cb 06       		.byte	0x6
 627 03cc 3C01     		.value	0x13c
 628 03ce 00000000 		.long	.LASF269
 629 03d2 3C020000 		.long	0x23c
 630 03d6 12       		.uleb128 0x12
 631 03d7 00000000 		.long	.LASF24
 632 03db 06       		.byte	0x6
 633 03dc 4001     		.value	0x140
 634 03de 00000000 		.long	.LASF270
 635 03e2 3C020000 		.long	0x23c
 636 03e6 0D       		.uleb128 0xd
 637 03e7 E6120000 		.long	0x12e6
 638 03eb 00       		.byte	0
 639 03ec 00       		.byte	0
 640 03ed 0B       		.uleb128 0xb
 641 03ee 00000000 		.long	.LASF25
 642 03f2 04       		.byte	0x4
 643 03f3 C4       		.byte	0xc4
 644 03f4 4B0A0000 		.long	0xa4b
 645 03f8 08       		.uleb128 0x8
 646 03f9 07       		.byte	0x7
 647 03fa 35       		.byte	0x35
 648 03fb EC120000 		.long	0x12ec
 649 03ff 08       		.uleb128 0x8
 650 0400 07       		.byte	0x7
 651 0401 36       		.byte	0x36
 652 0402 19140000 		.long	0x1419
 653 0406 08       		.uleb128 0x8
 654 0407 07       		.byte	0x7
 655 0408 37       		.byte	0x37
 656 0409 33140000 		.long	0x1433
 657 040d 0B       		.uleb128 0xb
 658 040e 00000000 		.long	.LASF26
 659 0412 04       		.byte	0x4
 660 0413 C5       		.byte	0xc5
 661 0414 87100000 		.long	0x1087
 662 0418 13       		.uleb128 0x13
 663 0419 00000000 		.long	.LASF48
 664 041d 04       		.byte	0x4
 665 041e B90A0000 		.long	0xab9
 666 0422 08       		.byte	0x8
 667 0423 39       		.byte	0x39
 668 0424 B9040000 		.long	0x4b9
 669 0428 14       		.uleb128 0x14
 670 0429 00000000 		.long	.LASF27
 671 042d 01       		.byte	0x1
GAS LISTING /tmp/ccZP9VrL.s 			page 15


 672 042e 14       		.uleb128 0x14
 673 042f 00000000 		.long	.LASF28
 674 0433 02       		.byte	0x2
 675 0434 14       		.uleb128 0x14
 676 0435 00000000 		.long	.LASF29
 677 0439 04       		.byte	0x4
 678 043a 14       		.uleb128 0x14
 679 043b 00000000 		.long	.LASF30
 680 043f 08       		.byte	0x8
 681 0440 14       		.uleb128 0x14
 682 0441 00000000 		.long	.LASF31
 683 0445 10       		.byte	0x10
 684 0446 14       		.uleb128 0x14
 685 0447 00000000 		.long	.LASF32
 686 044b 20       		.byte	0x20
 687 044c 14       		.uleb128 0x14
 688 044d 00000000 		.long	.LASF33
 689 0451 40       		.byte	0x40
 690 0452 14       		.uleb128 0x14
 691 0453 00000000 		.long	.LASF34
 692 0457 80       		.byte	0x80
 693 0458 15       		.uleb128 0x15
 694 0459 00000000 		.long	.LASF35
 695 045d 0001     		.value	0x100
 696 045f 15       		.uleb128 0x15
 697 0460 00000000 		.long	.LASF36
 698 0464 0002     		.value	0x200
 699 0466 15       		.uleb128 0x15
 700 0467 00000000 		.long	.LASF37
 701 046b 0004     		.value	0x400
 702 046d 15       		.uleb128 0x15
 703 046e 00000000 		.long	.LASF38
 704 0472 0008     		.value	0x800
 705 0474 15       		.uleb128 0x15
 706 0475 00000000 		.long	.LASF39
 707 0479 0010     		.value	0x1000
 708 047b 15       		.uleb128 0x15
 709 047c 00000000 		.long	.LASF40
 710 0480 0020     		.value	0x2000
 711 0482 15       		.uleb128 0x15
 712 0483 00000000 		.long	.LASF41
 713 0487 0040     		.value	0x4000
 714 0489 14       		.uleb128 0x14
 715 048a 00000000 		.long	.LASF42
 716 048e B0       		.byte	0xb0
 717 048f 14       		.uleb128 0x14
 718 0490 00000000 		.long	.LASF43
 719 0494 4A       		.byte	0x4a
 720 0495 15       		.uleb128 0x15
 721 0496 00000000 		.long	.LASF44
 722 049a 0401     		.value	0x104
 723 049c 16       		.uleb128 0x16
 724 049d 00000000 		.long	.LASF45
 725 04a1 00000100 		.long	0x10000
 726 04a5 16       		.uleb128 0x16
 727 04a6 00000000 		.long	.LASF46
 728 04aa FFFFFF7F 		.long	0x7fffffff
GAS LISTING /tmp/ccZP9VrL.s 			page 16


 729 04ae 17       		.uleb128 0x17
 730 04af 00000000 		.long	.LASF47
 731 04b3 80808080 		.sleb128 -2147483648
 731      78
 732 04b8 00       		.byte	0
 733 04b9 13       		.uleb128 0x13
 734 04ba 00000000 		.long	.LASF49
 735 04be 04       		.byte	0x4
 736 04bf B90A0000 		.long	0xab9
 737 04c3 08       		.byte	0x8
 738 04c4 6F       		.byte	0x6f
 739 04c5 0A050000 		.long	0x50a
 740 04c9 14       		.uleb128 0x14
 741 04ca 00000000 		.long	.LASF50
 742 04ce 01       		.byte	0x1
 743 04cf 14       		.uleb128 0x14
 744 04d0 00000000 		.long	.LASF51
 745 04d4 02       		.byte	0x2
 746 04d5 14       		.uleb128 0x14
 747 04d6 00000000 		.long	.LASF52
 748 04da 04       		.byte	0x4
 749 04db 14       		.uleb128 0x14
 750 04dc 00000000 		.long	.LASF53
 751 04e0 08       		.byte	0x8
 752 04e1 14       		.uleb128 0x14
 753 04e2 00000000 		.long	.LASF54
 754 04e6 10       		.byte	0x10
 755 04e7 14       		.uleb128 0x14
 756 04e8 00000000 		.long	.LASF55
 757 04ec 20       		.byte	0x20
 758 04ed 16       		.uleb128 0x16
 759 04ee 00000000 		.long	.LASF56
 760 04f2 00000100 		.long	0x10000
 761 04f6 16       		.uleb128 0x16
 762 04f7 00000000 		.long	.LASF57
 763 04fb FFFFFF7F 		.long	0x7fffffff
 764 04ff 17       		.uleb128 0x17
 765 0500 00000000 		.long	.LASF58
 766 0504 80808080 		.sleb128 -2147483648
 766      78
 767 0509 00       		.byte	0
 768 050a 13       		.uleb128 0x13
 769 050b 00000000 		.long	.LASF59
 770 050f 04       		.byte	0x4
 771 0510 B90A0000 		.long	0xab9
 772 0514 08       		.byte	0x8
 773 0515 99       		.byte	0x99
 774 0516 4F050000 		.long	0x54f
 775 051a 14       		.uleb128 0x14
 776 051b 00000000 		.long	.LASF60
 777 051f 00       		.byte	0
 778 0520 14       		.uleb128 0x14
 779 0521 00000000 		.long	.LASF61
 780 0525 01       		.byte	0x1
 781 0526 14       		.uleb128 0x14
 782 0527 00000000 		.long	.LASF62
 783 052b 02       		.byte	0x2
GAS LISTING /tmp/ccZP9VrL.s 			page 17


 784 052c 14       		.uleb128 0x14
 785 052d 00000000 		.long	.LASF63
 786 0531 04       		.byte	0x4
 787 0532 16       		.uleb128 0x16
 788 0533 00000000 		.long	.LASF64
 789 0537 00000100 		.long	0x10000
 790 053b 16       		.uleb128 0x16
 791 053c 00000000 		.long	.LASF65
 792 0540 FFFFFF7F 		.long	0x7fffffff
 793 0544 17       		.uleb128 0x17
 794 0545 00000000 		.long	.LASF66
 795 0549 80808080 		.sleb128 -2147483648
 795      78
 796 054e 00       		.byte	0
 797 054f 13       		.uleb128 0x13
 798 0550 00000000 		.long	.LASF67
 799 0554 04       		.byte	0x4
 800 0555 370A0000 		.long	0xa37
 801 0559 08       		.byte	0x8
 802 055a C1       		.byte	0xc1
 803 055b 7B050000 		.long	0x57b
 804 055f 14       		.uleb128 0x14
 805 0560 00000000 		.long	.LASF68
 806 0564 00       		.byte	0
 807 0565 14       		.uleb128 0x14
 808 0566 00000000 		.long	.LASF69
 809 056a 01       		.byte	0x1
 810 056b 14       		.uleb128 0x14
 811 056c 00000000 		.long	.LASF70
 812 0570 02       		.byte	0x2
 813 0571 16       		.uleb128 0x16
 814 0572 00000000 		.long	.LASF71
 815 0576 00000100 		.long	0x10000
 816 057a 00       		.byte	0
 817 057b 18       		.uleb128 0x18
 818 057c 00000000 		.long	.LASF101
 819 0580 E4070000 		.long	0x7e4
 820 0584 19       		.uleb128 0x19
 821 0585 00000000 		.long	.LASF271
 822 0589 01       		.byte	0x1
 823 058a 08       		.byte	0x8
 824 058b 5902     		.value	0x259
 825 058d 01       		.byte	0x1
 826 058e E2050000 		.long	0x5e2
 827 0592 1A       		.uleb128 0x1a
 828 0593 00000000 		.long	.LASF72
 829 0597 08       		.byte	0x8
 830 0598 6102     		.value	0x261
 831 059a 4F140000 		.long	0x144f
 832 059e 1A       		.uleb128 0x1a
 833 059f 00000000 		.long	.LASF73
 834 05a3 08       		.byte	0x8
 835 05a4 6202     		.value	0x262
 836 05a6 D3120000 		.long	0x12d3
 837 05aa 1B       		.uleb128 0x1b
 838 05ab 00000000 		.long	.LASF271
 839 05af 08       		.byte	0x8
GAS LISTING /tmp/ccZP9VrL.s 			page 18


 840 05b0 5D02     		.value	0x25d
 841 05b2 00000000 		.long	.LASF272
 842 05b6 01       		.byte	0x1
 843 05b7 BF050000 		.long	0x5bf
 844 05bb C5050000 		.long	0x5c5
 845 05bf 1C       		.uleb128 0x1c
 846 05c0 64140000 		.long	0x1464
 847 05c4 00       		.byte	0
 848 05c5 1D       		.uleb128 0x1d
 849 05c6 00000000 		.long	.LASF74
 850 05ca 08       		.byte	0x8
 851 05cb 5E02     		.value	0x25e
 852 05cd 00000000 		.long	.LASF75
 853 05d1 01       		.byte	0x1
 854 05d2 D6050000 		.long	0x5d6
 855 05d6 1C       		.uleb128 0x1c
 856 05d7 64140000 		.long	0x1464
 857 05db 1C       		.uleb128 0x1c
 858 05dc B90A0000 		.long	0xab9
 859 05e0 00       		.byte	0
 860 05e1 00       		.byte	0
 861 05e2 1E       		.uleb128 0x1e
 862 05e3 00000000 		.long	.LASF91
 863 05e7 08       		.byte	0x8
 864 05e8 4301     		.value	0x143
 865 05ea 18040000 		.long	0x418
 866 05ee 01       		.byte	0x1
 867 05ef 1F       		.uleb128 0x1f
 868 05f0 00000000 		.long	.LASF76
 869 05f4 08       		.byte	0x8
 870 05f5 4601     		.value	0x146
 871 05f7 FD050000 		.long	0x5fd
 872 05fb 01       		.byte	0x1
 873 05fc 01       		.byte	0x1
 874 05fd 0E       		.uleb128 0xe
 875 05fe E2050000 		.long	0x5e2
 876 0602 20       		.uleb128 0x20
 877 0603 64656300 		.string	"dec"
 878 0607 08       		.byte	0x8
 879 0608 4901     		.value	0x149
 880 060a FD050000 		.long	0x5fd
 881 060e 01       		.byte	0x1
 882 060f 02       		.byte	0x2
 883 0610 1F       		.uleb128 0x1f
 884 0611 00000000 		.long	.LASF77
 885 0615 08       		.byte	0x8
 886 0616 4C01     		.value	0x14c
 887 0618 FD050000 		.long	0x5fd
 888 061c 01       		.byte	0x1
 889 061d 04       		.byte	0x4
 890 061e 20       		.uleb128 0x20
 891 061f 68657800 		.string	"hex"
 892 0623 08       		.byte	0x8
 893 0624 4F01     		.value	0x14f
 894 0626 FD050000 		.long	0x5fd
 895 062a 01       		.byte	0x1
 896 062b 08       		.byte	0x8
GAS LISTING /tmp/ccZP9VrL.s 			page 19


 897 062c 1F       		.uleb128 0x1f
 898 062d 00000000 		.long	.LASF78
 899 0631 08       		.byte	0x8
 900 0632 5401     		.value	0x154
 901 0634 FD050000 		.long	0x5fd
 902 0638 01       		.byte	0x1
 903 0639 10       		.byte	0x10
 904 063a 1F       		.uleb128 0x1f
 905 063b 00000000 		.long	.LASF79
 906 063f 08       		.byte	0x8
 907 0640 5801     		.value	0x158
 908 0642 FD050000 		.long	0x5fd
 909 0646 01       		.byte	0x1
 910 0647 20       		.byte	0x20
 911 0648 20       		.uleb128 0x20
 912 0649 6F637400 		.string	"oct"
 913 064d 08       		.byte	0x8
 914 064e 5B01     		.value	0x15b
 915 0650 FD050000 		.long	0x5fd
 916 0654 01       		.byte	0x1
 917 0655 40       		.byte	0x40
 918 0656 1F       		.uleb128 0x1f
 919 0657 00000000 		.long	.LASF80
 920 065b 08       		.byte	0x8
 921 065c 5F01     		.value	0x15f
 922 065e FD050000 		.long	0x5fd
 923 0662 01       		.byte	0x1
 924 0663 80       		.byte	0x80
 925 0664 21       		.uleb128 0x21
 926 0665 00000000 		.long	.LASF81
 927 0669 08       		.byte	0x8
 928 066a 6201     		.value	0x162
 929 066c FD050000 		.long	0x5fd
 930 0670 01       		.byte	0x1
 931 0671 0001     		.value	0x100
 932 0673 21       		.uleb128 0x21
 933 0674 00000000 		.long	.LASF82
 934 0678 08       		.byte	0x8
 935 0679 6601     		.value	0x166
 936 067b FD050000 		.long	0x5fd
 937 067f 01       		.byte	0x1
 938 0680 0002     		.value	0x200
 939 0682 21       		.uleb128 0x21
 940 0683 00000000 		.long	.LASF83
 941 0687 08       		.byte	0x8
 942 0688 6A01     		.value	0x16a
 943 068a FD050000 		.long	0x5fd
 944 068e 01       		.byte	0x1
 945 068f 0004     		.value	0x400
 946 0691 21       		.uleb128 0x21
 947 0692 00000000 		.long	.LASF84
 948 0696 08       		.byte	0x8
 949 0697 6D01     		.value	0x16d
 950 0699 FD050000 		.long	0x5fd
 951 069d 01       		.byte	0x1
 952 069e 0008     		.value	0x800
 953 06a0 21       		.uleb128 0x21
GAS LISTING /tmp/ccZP9VrL.s 			page 20


 954 06a1 00000000 		.long	.LASF85
 955 06a5 08       		.byte	0x8
 956 06a6 7001     		.value	0x170
 957 06a8 FD050000 		.long	0x5fd
 958 06ac 01       		.byte	0x1
 959 06ad 0010     		.value	0x1000
 960 06af 21       		.uleb128 0x21
 961 06b0 00000000 		.long	.LASF86
 962 06b4 08       		.byte	0x8
 963 06b5 7301     		.value	0x173
 964 06b7 FD050000 		.long	0x5fd
 965 06bb 01       		.byte	0x1
 966 06bc 0020     		.value	0x2000
 967 06be 21       		.uleb128 0x21
 968 06bf 00000000 		.long	.LASF87
 969 06c3 08       		.byte	0x8
 970 06c4 7701     		.value	0x177
 971 06c6 FD050000 		.long	0x5fd
 972 06ca 01       		.byte	0x1
 973 06cb 0040     		.value	0x4000
 974 06cd 1F       		.uleb128 0x1f
 975 06ce 00000000 		.long	.LASF88
 976 06d2 08       		.byte	0x8
 977 06d3 7A01     		.value	0x17a
 978 06d5 FD050000 		.long	0x5fd
 979 06d9 01       		.byte	0x1
 980 06da B0       		.byte	0xb0
 981 06db 1F       		.uleb128 0x1f
 982 06dc 00000000 		.long	.LASF89
 983 06e0 08       		.byte	0x8
 984 06e1 7D01     		.value	0x17d
 985 06e3 FD050000 		.long	0x5fd
 986 06e7 01       		.byte	0x1
 987 06e8 4A       		.byte	0x4a
 988 06e9 21       		.uleb128 0x21
 989 06ea 00000000 		.long	.LASF90
 990 06ee 08       		.byte	0x8
 991 06ef 8001     		.value	0x180
 992 06f1 FD050000 		.long	0x5fd
 993 06f5 01       		.byte	0x1
 994 06f6 0401     		.value	0x104
 995 06f8 1E       		.uleb128 0x1e
 996 06f9 00000000 		.long	.LASF92
 997 06fd 08       		.byte	0x8
 998 06fe 8E01     		.value	0x18e
 999 0700 0A050000 		.long	0x50a
 1000 0704 01       		.byte	0x1
 1001 0705 1F       		.uleb128 0x1f
 1002 0706 00000000 		.long	.LASF93
 1003 070a 08       		.byte	0x8
 1004 070b 9201     		.value	0x192
 1005 070d 13070000 		.long	0x713
 1006 0711 01       		.byte	0x1
 1007 0712 01       		.byte	0x1
 1008 0713 0E       		.uleb128 0xe
 1009 0714 F8060000 		.long	0x6f8
 1010 0718 1F       		.uleb128 0x1f
GAS LISTING /tmp/ccZP9VrL.s 			page 21


 1011 0719 00000000 		.long	.LASF94
 1012 071d 08       		.byte	0x8
 1013 071e 9501     		.value	0x195
 1014 0720 13070000 		.long	0x713
 1015 0724 01       		.byte	0x1
 1016 0725 02       		.byte	0x2
 1017 0726 1F       		.uleb128 0x1f
 1018 0727 00000000 		.long	.LASF95
 1019 072b 08       		.byte	0x8
 1020 072c 9A01     		.value	0x19a
 1021 072e 13070000 		.long	0x713
 1022 0732 01       		.byte	0x1
 1023 0733 04       		.byte	0x4
 1024 0734 1F       		.uleb128 0x1f
 1025 0735 00000000 		.long	.LASF96
 1026 0739 08       		.byte	0x8
 1027 073a 9D01     		.value	0x19d
 1028 073c 13070000 		.long	0x713
 1029 0740 01       		.byte	0x1
 1030 0741 00       		.byte	0
 1031 0742 1E       		.uleb128 0x1e
 1032 0743 00000000 		.long	.LASF97
 1033 0747 08       		.byte	0x8
 1034 0748 AD01     		.value	0x1ad
 1035 074a B9040000 		.long	0x4b9
 1036 074e 01       		.byte	0x1
 1037 074f 20       		.uleb128 0x20
 1038 0750 61707000 		.string	"app"
 1039 0754 08       		.byte	0x8
 1040 0755 B001     		.value	0x1b0
 1041 0757 5D070000 		.long	0x75d
 1042 075b 01       		.byte	0x1
 1043 075c 01       		.byte	0x1
 1044 075d 0E       		.uleb128 0xe
 1045 075e 42070000 		.long	0x742
 1046 0762 20       		.uleb128 0x20
 1047 0763 61746500 		.string	"ate"
 1048 0767 08       		.byte	0x8
 1049 0768 B301     		.value	0x1b3
 1050 076a 5D070000 		.long	0x75d
 1051 076e 01       		.byte	0x1
 1052 076f 02       		.byte	0x2
 1053 0770 1F       		.uleb128 0x1f
 1054 0771 00000000 		.long	.LASF98
 1055 0775 08       		.byte	0x8
 1056 0776 B801     		.value	0x1b8
 1057 0778 5D070000 		.long	0x75d
 1058 077c 01       		.byte	0x1
 1059 077d 04       		.byte	0x4
 1060 077e 20       		.uleb128 0x20
 1061 077f 696E00   		.string	"in"
 1062 0782 08       		.byte	0x8
 1063 0783 BB01     		.value	0x1bb
 1064 0785 5D070000 		.long	0x75d
 1065 0789 01       		.byte	0x1
 1066 078a 08       		.byte	0x8
 1067 078b 20       		.uleb128 0x20
GAS LISTING /tmp/ccZP9VrL.s 			page 22


 1068 078c 6F757400 		.string	"out"
 1069 0790 08       		.byte	0x8
 1070 0791 BE01     		.value	0x1be
 1071 0793 5D070000 		.long	0x75d
 1072 0797 01       		.byte	0x1
 1073 0798 10       		.byte	0x10
 1074 0799 1F       		.uleb128 0x1f
 1075 079a 00000000 		.long	.LASF99
 1076 079e 08       		.byte	0x8
 1077 079f C101     		.value	0x1c1
 1078 07a1 5D070000 		.long	0x75d
 1079 07a5 01       		.byte	0x1
 1080 07a6 20       		.byte	0x20
 1081 07a7 1E       		.uleb128 0x1e
 1082 07a8 00000000 		.long	.LASF100
 1083 07ac 08       		.byte	0x8
 1084 07ad CD01     		.value	0x1cd
 1085 07af 4F050000 		.long	0x54f
 1086 07b3 01       		.byte	0x1
 1087 07b4 20       		.uleb128 0x20
 1088 07b5 62656700 		.string	"beg"
 1089 07b9 08       		.byte	0x8
 1090 07ba D001     		.value	0x1d0
 1091 07bc C2070000 		.long	0x7c2
 1092 07c0 01       		.byte	0x1
 1093 07c1 00       		.byte	0
 1094 07c2 0E       		.uleb128 0xe
 1095 07c3 A7070000 		.long	0x7a7
 1096 07c7 20       		.uleb128 0x20
 1097 07c8 63757200 		.string	"cur"
 1098 07cc 08       		.byte	0x8
 1099 07cd D301     		.value	0x1d3
 1100 07cf C2070000 		.long	0x7c2
 1101 07d3 01       		.byte	0x1
 1102 07d4 01       		.byte	0x1
 1103 07d5 20       		.uleb128 0x20
 1104 07d6 656E6400 		.string	"end"
 1105 07da 08       		.byte	0x8
 1106 07db D601     		.value	0x1d6
 1107 07dd C2070000 		.long	0x7c2
 1108 07e1 01       		.byte	0x1
 1109 07e2 02       		.byte	0x2
 1110 07e3 00       		.byte	0
 1111 07e4 08       		.uleb128 0x8
 1112 07e5 09       		.byte	0x9
 1113 07e6 52       		.byte	0x52
 1114 07e7 75140000 		.long	0x1475
 1115 07eb 08       		.uleb128 0x8
 1116 07ec 09       		.byte	0x9
 1117 07ed 53       		.byte	0x53
 1118 07ee 6A140000 		.long	0x146a
 1119 07f2 08       		.uleb128 0x8
 1120 07f3 09       		.byte	0x9
 1121 07f4 54       		.byte	0x54
 1122 07f5 520A0000 		.long	0xa52
 1123 07f9 08       		.uleb128 0x8
 1124 07fa 09       		.byte	0x9
GAS LISTING /tmp/ccZP9VrL.s 			page 23


 1125 07fb 5C       		.byte	0x5c
 1126 07fc 8B140000 		.long	0x148b
 1127 0800 08       		.uleb128 0x8
 1128 0801 09       		.byte	0x9
 1129 0802 65       		.byte	0x65
 1130 0803 A5140000 		.long	0x14a5
 1131 0807 08       		.uleb128 0x8
 1132 0808 09       		.byte	0x9
 1133 0809 68       		.byte	0x68
 1134 080a BF140000 		.long	0x14bf
 1135 080e 08       		.uleb128 0x8
 1136 080f 09       		.byte	0x9
 1137 0810 69       		.byte	0x69
 1138 0811 D4140000 		.long	0x14d4
 1139 0815 18       		.uleb128 0x18
 1140 0816 00000000 		.long	.LASF102
 1141 081a 31080000 		.long	0x831
 1142 081e 22       		.uleb128 0x22
 1143 081f 00000000 		.long	.LASF112
 1144 0823 B20A0000 		.long	0xab2
 1145 0827 23       		.uleb128 0x23
 1146 0828 00000000 		.long	.LASF273
 1147 082c 25020000 		.long	0x225
 1148 0830 00       		.byte	0
 1149 0831 0B       		.uleb128 0xb
 1150 0832 00000000 		.long	.LASF103
 1151 0836 0A       		.byte	0xa
 1152 0837 8D       		.byte	0x8d
 1153 0838 15080000 		.long	0x815
 1154 083c 24       		.uleb128 0x24
 1155 083d 00000000 		.long	.LASF274
 1156 0841 02       		.byte	0x2
 1157 0842 3D       		.byte	0x3d
 1158 0843 00000000 		.long	.LASF275
 1159 0847 31080000 		.long	0x831
 1160 084b 25       		.uleb128 0x25
 1161 084c 00000000 		.long	.LASF252
 1162 0850 02       		.byte	0x2
 1163 0851 4A       		.byte	0x4a
 1164 0852 84050000 		.long	0x584
 1165 0856 00       		.byte	0
 1166 0857 26       		.uleb128 0x26
 1167 0858 00000000 		.long	.LASF104
 1168 085c 04       		.byte	0x4
 1169 085d DD       		.byte	0xdd
 1170 085e E3090000 		.long	0x9e3
 1171 0862 06       		.uleb128 0x6
 1172 0863 00000000 		.long	.LASF0
 1173 0867 04       		.byte	0x4
 1174 0868 DE       		.byte	0xde
 1175 0869 07       		.uleb128 0x7
 1176 086a 04       		.byte	0x4
 1177 086b DE       		.byte	0xde
 1178 086c 62080000 		.long	0x862
 1179 0870 08       		.uleb128 0x8
 1180 0871 03       		.byte	0x3
 1181 0872 F8       		.byte	0xf8
GAS LISTING /tmp/ccZP9VrL.s 			page 24


 1182 0873 2F120000 		.long	0x122f
 1183 0877 09       		.uleb128 0x9
 1184 0878 03       		.byte	0x3
 1185 0879 0101     		.value	0x101
 1186 087b 51120000 		.long	0x1251
 1187 087f 09       		.uleb128 0x9
 1188 0880 03       		.byte	0x3
 1189 0881 0201     		.value	0x102
 1190 0883 78120000 		.long	0x1278
 1191 0887 06       		.uleb128 0x6
 1192 0888 00000000 		.long	.LASF105
 1193 088c 0B       		.byte	0xb
 1194 088d 24       		.byte	0x24
 1195 088e 08       		.uleb128 0x8
 1196 088f 0C       		.byte	0xc
 1197 0890 2C       		.byte	0x2c
 1198 0891 ED030000 		.long	0x3ed
 1199 0895 08       		.uleb128 0x8
 1200 0896 0C       		.byte	0xc
 1201 0897 2D       		.byte	0x2d
 1202 0898 0D040000 		.long	0x40d
 1203 089c 0A       		.uleb128 0xa
 1204 089d 00000000 		.long	.LASF107
 1205 08a1 01       		.byte	0x1
 1206 08a2 0D       		.byte	0xd
 1207 08a3 37       		.byte	0x37
 1208 08a4 DE080000 		.long	0x8de
 1209 08a8 27       		.uleb128 0x27
 1210 08a9 00000000 		.long	.LASF108
 1211 08ad 0D       		.byte	0xd
 1212 08ae 3A       		.byte	0x3a
 1213 08af DD0A0000 		.long	0xadd
 1214 08b3 27       		.uleb128 0x27
 1215 08b4 00000000 		.long	.LASF109
 1216 08b8 0D       		.byte	0xd
 1217 08b9 3B       		.byte	0x3b
 1218 08ba DD0A0000 		.long	0xadd
 1219 08be 27       		.uleb128 0x27
 1220 08bf 00000000 		.long	.LASF110
 1221 08c3 0D       		.byte	0xd
 1222 08c4 3F       		.byte	0x3f
 1223 08c5 5A140000 		.long	0x145a
 1224 08c9 27       		.uleb128 0x27
 1225 08ca 00000000 		.long	.LASF111
 1226 08ce 0D       		.byte	0xd
 1227 08cf 40       		.byte	0x40
 1228 08d0 DD0A0000 		.long	0xadd
 1229 08d4 22       		.uleb128 0x22
 1230 08d5 00000000 		.long	.LASF113
 1231 08d9 B90A0000 		.long	0xab9
 1232 08dd 00       		.byte	0
 1233 08de 0A       		.uleb128 0xa
 1234 08df 00000000 		.long	.LASF114
 1235 08e3 01       		.byte	0x1
 1236 08e4 0D       		.byte	0xd
 1237 08e5 37       		.byte	0x37
 1238 08e6 20090000 		.long	0x920
GAS LISTING /tmp/ccZP9VrL.s 			page 25


 1239 08ea 27       		.uleb128 0x27
 1240 08eb 00000000 		.long	.LASF108
 1241 08ef 0D       		.byte	0xd
 1242 08f0 3A       		.byte	0x3a
 1243 08f1 5F140000 		.long	0x145f
 1244 08f5 27       		.uleb128 0x27
 1245 08f6 00000000 		.long	.LASF109
 1246 08fa 0D       		.byte	0xd
 1247 08fb 3B       		.byte	0x3b
 1248 08fc 5F140000 		.long	0x145f
 1249 0900 27       		.uleb128 0x27
 1250 0901 00000000 		.long	.LASF110
 1251 0905 0D       		.byte	0xd
 1252 0906 3F       		.byte	0x3f
 1253 0907 5A140000 		.long	0x145a
 1254 090b 27       		.uleb128 0x27
 1255 090c 00000000 		.long	.LASF111
 1256 0910 0D       		.byte	0xd
 1257 0911 40       		.byte	0x40
 1258 0912 DD0A0000 		.long	0xadd
 1259 0916 22       		.uleb128 0x22
 1260 0917 00000000 		.long	.LASF113
 1261 091b 4B0A0000 		.long	0xa4b
 1262 091f 00       		.byte	0
 1263 0920 0A       		.uleb128 0xa
 1264 0921 00000000 		.long	.LASF115
 1265 0925 01       		.byte	0x1
 1266 0926 0D       		.byte	0xd
 1267 0927 37       		.byte	0x37
 1268 0928 62090000 		.long	0x962
 1269 092c 27       		.uleb128 0x27
 1270 092d 00000000 		.long	.LASF108
 1271 0931 0D       		.byte	0xd
 1272 0932 3A       		.byte	0x3a
 1273 0933 E80A0000 		.long	0xae8
 1274 0937 27       		.uleb128 0x27
 1275 0938 00000000 		.long	.LASF109
 1276 093c 0D       		.byte	0xd
 1277 093d 3B       		.byte	0x3b
 1278 093e E80A0000 		.long	0xae8
 1279 0942 27       		.uleb128 0x27
 1280 0943 00000000 		.long	.LASF110
 1281 0947 0D       		.byte	0xd
 1282 0948 3F       		.byte	0x3f
 1283 0949 5A140000 		.long	0x145a
 1284 094d 27       		.uleb128 0x27
 1285 094e 00000000 		.long	.LASF111
 1286 0952 0D       		.byte	0xd
 1287 0953 40       		.byte	0x40
 1288 0954 DD0A0000 		.long	0xadd
 1289 0958 22       		.uleb128 0x22
 1290 0959 00000000 		.long	.LASF113
 1291 095d B20A0000 		.long	0xab2
 1292 0961 00       		.byte	0
 1293 0962 0A       		.uleb128 0xa
 1294 0963 00000000 		.long	.LASF116
 1295 0967 01       		.byte	0x1
GAS LISTING /tmp/ccZP9VrL.s 			page 26


 1296 0968 0D       		.byte	0xd
 1297 0969 37       		.byte	0x37
 1298 096a A4090000 		.long	0x9a4
 1299 096e 27       		.uleb128 0x27
 1300 096f 00000000 		.long	.LASF108
 1301 0973 0D       		.byte	0xd
 1302 0974 3A       		.byte	0x3a
 1303 0975 E9140000 		.long	0x14e9
 1304 0979 27       		.uleb128 0x27
 1305 097a 00000000 		.long	.LASF109
 1306 097e 0D       		.byte	0xd
 1307 097f 3B       		.byte	0x3b
 1308 0980 E9140000 		.long	0x14e9
 1309 0984 27       		.uleb128 0x27
 1310 0985 00000000 		.long	.LASF110
 1311 0989 0D       		.byte	0xd
 1312 098a 3F       		.byte	0x3f
 1313 098b 5A140000 		.long	0x145a
 1314 098f 27       		.uleb128 0x27
 1315 0990 00000000 		.long	.LASF111
 1316 0994 0D       		.byte	0xd
 1317 0995 40       		.byte	0x40
 1318 0996 DD0A0000 		.long	0xadd
 1319 099a 22       		.uleb128 0x22
 1320 099b 00000000 		.long	.LASF113
 1321 099f AD120000 		.long	0x12ad
 1322 09a3 00       		.byte	0
 1323 09a4 28       		.uleb128 0x28
 1324 09a5 00000000 		.long	.LASF276
 1325 09a9 01       		.byte	0x1
 1326 09aa 0D       		.byte	0xd
 1327 09ab 37       		.byte	0x37
 1328 09ac 27       		.uleb128 0x27
 1329 09ad 00000000 		.long	.LASF108
 1330 09b1 0D       		.byte	0xd
 1331 09b2 3A       		.byte	0x3a
 1332 09b3 EE140000 		.long	0x14ee
 1333 09b7 27       		.uleb128 0x27
 1334 09b8 00000000 		.long	.LASF109
 1335 09bc 0D       		.byte	0xd
 1336 09bd 3B       		.byte	0x3b
 1337 09be EE140000 		.long	0x14ee
 1338 09c2 27       		.uleb128 0x27
 1339 09c3 00000000 		.long	.LASF110
 1340 09c7 0D       		.byte	0xd
 1341 09c8 3F       		.byte	0x3f
 1342 09c9 5A140000 		.long	0x145a
 1343 09cd 27       		.uleb128 0x27
 1344 09ce 00000000 		.long	.LASF111
 1345 09d2 0D       		.byte	0xd
 1346 09d3 40       		.byte	0x40
 1347 09d4 DD0A0000 		.long	0xadd
 1348 09d8 22       		.uleb128 0x22
 1349 09d9 00000000 		.long	.LASF113
 1350 09dd 87100000 		.long	0x1087
 1351 09e1 00       		.byte	0
 1352 09e2 00       		.byte	0
GAS LISTING /tmp/ccZP9VrL.s 			page 27


 1353 09e3 29       		.uleb128 0x29
 1354 09e4 00000000 		.long	.LASF277
 1355 09e8 0B       		.uleb128 0xb
 1356 09e9 00000000 		.long	.LASF117
 1357 09ed 0E       		.byte	0xe
 1358 09ee 40       		.byte	0x40
 1359 09ef E3090000 		.long	0x9e3
 1360 09f3 04       		.uleb128 0x4
 1361 09f4 08       		.byte	0x8
 1362 09f5 07       		.byte	0x7
 1363 09f6 00000000 		.long	.LASF119
 1364 09fa 0A       		.uleb128 0xa
 1365 09fb 00000000 		.long	.LASF120
 1366 09ff 18       		.byte	0x18
 1367 0a00 0F       		.byte	0xf
 1368 0a01 00       		.byte	0
 1369 0a02 370A0000 		.long	0xa37
 1370 0a06 2A       		.uleb128 0x2a
 1371 0a07 00000000 		.long	.LASF121
 1372 0a0b 0F       		.byte	0xf
 1373 0a0c 00       		.byte	0
 1374 0a0d 370A0000 		.long	0xa37
 1375 0a11 00       		.byte	0
 1376 0a12 2A       		.uleb128 0x2a
 1377 0a13 00000000 		.long	.LASF122
 1378 0a17 0F       		.byte	0xf
 1379 0a18 00       		.byte	0
 1380 0a19 370A0000 		.long	0xa37
 1381 0a1d 04       		.byte	0x4
 1382 0a1e 2A       		.uleb128 0x2a
 1383 0a1f 00000000 		.long	.LASF123
 1384 0a23 0F       		.byte	0xf
 1385 0a24 00       		.byte	0
 1386 0a25 3E0A0000 		.long	0xa3e
 1387 0a29 08       		.byte	0x8
 1388 0a2a 2A       		.uleb128 0x2a
 1389 0a2b 00000000 		.long	.LASF124
 1390 0a2f 0F       		.byte	0xf
 1391 0a30 00       		.byte	0
 1392 0a31 3E0A0000 		.long	0xa3e
 1393 0a35 10       		.byte	0x10
 1394 0a36 00       		.byte	0
 1395 0a37 04       		.uleb128 0x4
 1396 0a38 04       		.byte	0x4
 1397 0a39 07       		.byte	0x7
 1398 0a3a 00000000 		.long	.LASF125
 1399 0a3e 2B       		.uleb128 0x2b
 1400 0a3f 08       		.byte	0x8
 1401 0a40 0B       		.uleb128 0xb
 1402 0a41 00000000 		.long	.LASF25
 1403 0a45 10       		.byte	0x10
 1404 0a46 D8       		.byte	0xd8
 1405 0a47 4B0A0000 		.long	0xa4b
 1406 0a4b 04       		.uleb128 0x4
 1407 0a4c 08       		.byte	0x8
 1408 0a4d 07       		.byte	0x7
 1409 0a4e 00000000 		.long	.LASF126
GAS LISTING /tmp/ccZP9VrL.s 			page 28


 1410 0a52 2C       		.uleb128 0x2c
 1411 0a53 00000000 		.long	.LASF127
 1412 0a57 10       		.byte	0x10
 1413 0a58 6501     		.value	0x165
 1414 0a5a 370A0000 		.long	0xa37
 1415 0a5e 2D       		.uleb128 0x2d
 1416 0a5f 08       		.byte	0x8
 1417 0a60 11       		.byte	0x11
 1418 0a61 53       		.byte	0x53
 1419 0a62 00000000 		.long	.LASF278
 1420 0a66 A20A0000 		.long	0xaa2
 1421 0a6a 2E       		.uleb128 0x2e
 1422 0a6b 04       		.byte	0x4
 1423 0a6c 11       		.byte	0x11
 1424 0a6d 56       		.byte	0x56
 1425 0a6e 890A0000 		.long	0xa89
 1426 0a72 2F       		.uleb128 0x2f
 1427 0a73 00000000 		.long	.LASF128
 1428 0a77 11       		.byte	0x11
 1429 0a78 58       		.byte	0x58
 1430 0a79 370A0000 		.long	0xa37
 1431 0a7d 2F       		.uleb128 0x2f
 1432 0a7e 00000000 		.long	.LASF129
 1433 0a82 11       		.byte	0x11
 1434 0a83 5C       		.byte	0x5c
 1435 0a84 A20A0000 		.long	0xaa2
 1436 0a88 00       		.byte	0
 1437 0a89 2A       		.uleb128 0x2a
 1438 0a8a 00000000 		.long	.LASF130
 1439 0a8e 11       		.byte	0x11
 1440 0a8f 54       		.byte	0x54
 1441 0a90 B90A0000 		.long	0xab9
 1442 0a94 00       		.byte	0
 1443 0a95 2A       		.uleb128 0x2a
 1444 0a96 00000000 		.long	.LASF131
 1445 0a9a 11       		.byte	0x11
 1446 0a9b 5D       		.byte	0x5d
 1447 0a9c 6A0A0000 		.long	0xa6a
 1448 0aa0 04       		.byte	0x4
 1449 0aa1 00       		.byte	0
 1450 0aa2 30       		.uleb128 0x30
 1451 0aa3 B20A0000 		.long	0xab2
 1452 0aa7 B20A0000 		.long	0xab2
 1453 0aab 31       		.uleb128 0x31
 1454 0aac F3090000 		.long	0x9f3
 1455 0ab0 03       		.byte	0x3
 1456 0ab1 00       		.byte	0
 1457 0ab2 04       		.uleb128 0x4
 1458 0ab3 01       		.byte	0x1
 1459 0ab4 06       		.byte	0x6
 1460 0ab5 00000000 		.long	.LASF132
 1461 0ab9 32       		.uleb128 0x32
 1462 0aba 04       		.byte	0x4
 1463 0abb 05       		.byte	0x5
 1464 0abc 696E7400 		.string	"int"
 1465 0ac0 0B       		.uleb128 0xb
 1466 0ac1 00000000 		.long	.LASF133
GAS LISTING /tmp/ccZP9VrL.s 			page 29


 1467 0ac5 11       		.byte	0x11
 1468 0ac6 5E       		.byte	0x5e
 1469 0ac7 5E0A0000 		.long	0xa5e
 1470 0acb 0B       		.uleb128 0xb
 1471 0acc 00000000 		.long	.LASF134
 1472 0ad0 11       		.byte	0x11
 1473 0ad1 6A       		.byte	0x6a
 1474 0ad2 C00A0000 		.long	0xac0
 1475 0ad6 04       		.uleb128 0x4
 1476 0ad7 02       		.byte	0x2
 1477 0ad8 07       		.byte	0x7
 1478 0ad9 00000000 		.long	.LASF135
 1479 0add 0E       		.uleb128 0xe
 1480 0ade B90A0000 		.long	0xab9
 1481 0ae2 33       		.uleb128 0x33
 1482 0ae3 08       		.byte	0x8
 1483 0ae4 E80A0000 		.long	0xae8
 1484 0ae8 0E       		.uleb128 0xe
 1485 0ae9 B20A0000 		.long	0xab2
 1486 0aed 34       		.uleb128 0x34
 1487 0aee 00000000 		.long	.LASF136
 1488 0af2 11       		.byte	0x11
 1489 0af3 6401     		.value	0x164
 1490 0af5 520A0000 		.long	0xa52
 1491 0af9 030B0000 		.long	0xb03
 1492 0afd 0D       		.uleb128 0xd
 1493 0afe B90A0000 		.long	0xab9
 1494 0b02 00       		.byte	0
 1495 0b03 34       		.uleb128 0x34
 1496 0b04 00000000 		.long	.LASF137
 1497 0b08 11       		.byte	0x11
 1498 0b09 EC02     		.value	0x2ec
 1499 0b0b 520A0000 		.long	0xa52
 1500 0b0f 190B0000 		.long	0xb19
 1501 0b13 0D       		.uleb128 0xd
 1502 0b14 190B0000 		.long	0xb19
 1503 0b18 00       		.byte	0
 1504 0b19 33       		.uleb128 0x33
 1505 0b1a 08       		.byte	0x8
 1506 0b1b E8090000 		.long	0x9e8
 1507 0b1f 34       		.uleb128 0x34
 1508 0b20 00000000 		.long	.LASF138
 1509 0b24 11       		.byte	0x11
 1510 0b25 0903     		.value	0x309
 1511 0b27 3F0B0000 		.long	0xb3f
 1512 0b2b 3F0B0000 		.long	0xb3f
 1513 0b2f 0D       		.uleb128 0xd
 1514 0b30 3F0B0000 		.long	0xb3f
 1515 0b34 0D       		.uleb128 0xd
 1516 0b35 B90A0000 		.long	0xab9
 1517 0b39 0D       		.uleb128 0xd
 1518 0b3a 190B0000 		.long	0xb19
 1519 0b3e 00       		.byte	0
 1520 0b3f 33       		.uleb128 0x33
 1521 0b40 08       		.byte	0x8
 1522 0b41 450B0000 		.long	0xb45
 1523 0b45 04       		.uleb128 0x4
GAS LISTING /tmp/ccZP9VrL.s 			page 30


 1524 0b46 04       		.byte	0x4
 1525 0b47 05       		.byte	0x5
 1526 0b48 00000000 		.long	.LASF139
 1527 0b4c 34       		.uleb128 0x34
 1528 0b4d 00000000 		.long	.LASF140
 1529 0b51 11       		.byte	0x11
 1530 0b52 FA02     		.value	0x2fa
 1531 0b54 520A0000 		.long	0xa52
 1532 0b58 670B0000 		.long	0xb67
 1533 0b5c 0D       		.uleb128 0xd
 1534 0b5d 450B0000 		.long	0xb45
 1535 0b61 0D       		.uleb128 0xd
 1536 0b62 190B0000 		.long	0xb19
 1537 0b66 00       		.byte	0
 1538 0b67 34       		.uleb128 0x34
 1539 0b68 00000000 		.long	.LASF141
 1540 0b6c 11       		.byte	0x11
 1541 0b6d 1003     		.value	0x310
 1542 0b6f B90A0000 		.long	0xab9
 1543 0b73 820B0000 		.long	0xb82
 1544 0b77 0D       		.uleb128 0xd
 1545 0b78 820B0000 		.long	0xb82
 1546 0b7c 0D       		.uleb128 0xd
 1547 0b7d 190B0000 		.long	0xb19
 1548 0b81 00       		.byte	0
 1549 0b82 33       		.uleb128 0x33
 1550 0b83 08       		.byte	0x8
 1551 0b84 880B0000 		.long	0xb88
 1552 0b88 0E       		.uleb128 0xe
 1553 0b89 450B0000 		.long	0xb45
 1554 0b8d 34       		.uleb128 0x34
 1555 0b8e 00000000 		.long	.LASF142
 1556 0b92 11       		.byte	0x11
 1557 0b93 4E02     		.value	0x24e
 1558 0b95 B90A0000 		.long	0xab9
 1559 0b99 A80B0000 		.long	0xba8
 1560 0b9d 0D       		.uleb128 0xd
 1561 0b9e 190B0000 		.long	0xb19
 1562 0ba2 0D       		.uleb128 0xd
 1563 0ba3 B90A0000 		.long	0xab9
 1564 0ba7 00       		.byte	0
 1565 0ba8 34       		.uleb128 0x34
 1566 0ba9 00000000 		.long	.LASF143
 1567 0bad 11       		.byte	0x11
 1568 0bae 5502     		.value	0x255
 1569 0bb0 B90A0000 		.long	0xab9
 1570 0bb4 C40B0000 		.long	0xbc4
 1571 0bb8 0D       		.uleb128 0xd
 1572 0bb9 190B0000 		.long	0xb19
 1573 0bbd 0D       		.uleb128 0xd
 1574 0bbe 820B0000 		.long	0xb82
 1575 0bc2 35       		.uleb128 0x35
 1576 0bc3 00       		.byte	0
 1577 0bc4 34       		.uleb128 0x34
 1578 0bc5 00000000 		.long	.LASF144
 1579 0bc9 11       		.byte	0x11
 1580 0bca 7E02     		.value	0x27e
GAS LISTING /tmp/ccZP9VrL.s 			page 31


 1581 0bcc B90A0000 		.long	0xab9
 1582 0bd0 E00B0000 		.long	0xbe0
 1583 0bd4 0D       		.uleb128 0xd
 1584 0bd5 190B0000 		.long	0xb19
 1585 0bd9 0D       		.uleb128 0xd
 1586 0bda 820B0000 		.long	0xb82
 1587 0bde 35       		.uleb128 0x35
 1588 0bdf 00       		.byte	0
 1589 0be0 34       		.uleb128 0x34
 1590 0be1 00000000 		.long	.LASF145
 1591 0be5 11       		.byte	0x11
 1592 0be6 ED02     		.value	0x2ed
 1593 0be8 520A0000 		.long	0xa52
 1594 0bec F60B0000 		.long	0xbf6
 1595 0bf0 0D       		.uleb128 0xd
 1596 0bf1 190B0000 		.long	0xb19
 1597 0bf5 00       		.byte	0
 1598 0bf6 36       		.uleb128 0x36
 1599 0bf7 00000000 		.long	.LASF242
 1600 0bfb 11       		.byte	0x11
 1601 0bfc F302     		.value	0x2f3
 1602 0bfe 520A0000 		.long	0xa52
 1603 0c02 34       		.uleb128 0x34
 1604 0c03 00000000 		.long	.LASF146
 1605 0c07 11       		.byte	0x11
 1606 0c08 7B01     		.value	0x17b
 1607 0c0a 400A0000 		.long	0xa40
 1608 0c0e 220C0000 		.long	0xc22
 1609 0c12 0D       		.uleb128 0xd
 1610 0c13 E20A0000 		.long	0xae2
 1611 0c17 0D       		.uleb128 0xd
 1612 0c18 400A0000 		.long	0xa40
 1613 0c1c 0D       		.uleb128 0xd
 1614 0c1d 220C0000 		.long	0xc22
 1615 0c21 00       		.byte	0
 1616 0c22 33       		.uleb128 0x33
 1617 0c23 08       		.byte	0x8
 1618 0c24 CB0A0000 		.long	0xacb
 1619 0c28 34       		.uleb128 0x34
 1620 0c29 00000000 		.long	.LASF147
 1621 0c2d 11       		.byte	0x11
 1622 0c2e 7001     		.value	0x170
 1623 0c30 400A0000 		.long	0xa40
 1624 0c34 4D0C0000 		.long	0xc4d
 1625 0c38 0D       		.uleb128 0xd
 1626 0c39 3F0B0000 		.long	0xb3f
 1627 0c3d 0D       		.uleb128 0xd
 1628 0c3e E20A0000 		.long	0xae2
 1629 0c42 0D       		.uleb128 0xd
 1630 0c43 400A0000 		.long	0xa40
 1631 0c47 0D       		.uleb128 0xd
 1632 0c48 220C0000 		.long	0xc22
 1633 0c4c 00       		.byte	0
 1634 0c4d 34       		.uleb128 0x34
 1635 0c4e 00000000 		.long	.LASF148
 1636 0c52 11       		.byte	0x11
 1637 0c53 6C01     		.value	0x16c
GAS LISTING /tmp/ccZP9VrL.s 			page 32


 1638 0c55 B90A0000 		.long	0xab9
 1639 0c59 630C0000 		.long	0xc63
 1640 0c5d 0D       		.uleb128 0xd
 1641 0c5e 630C0000 		.long	0xc63
 1642 0c62 00       		.byte	0
 1643 0c63 33       		.uleb128 0x33
 1644 0c64 08       		.byte	0x8
 1645 0c65 690C0000 		.long	0xc69
 1646 0c69 0E       		.uleb128 0xe
 1647 0c6a CB0A0000 		.long	0xacb
 1648 0c6e 34       		.uleb128 0x34
 1649 0c6f 00000000 		.long	.LASF149
 1650 0c73 11       		.byte	0x11
 1651 0c74 9B01     		.value	0x19b
 1652 0c76 400A0000 		.long	0xa40
 1653 0c7a 930C0000 		.long	0xc93
 1654 0c7e 0D       		.uleb128 0xd
 1655 0c7f 3F0B0000 		.long	0xb3f
 1656 0c83 0D       		.uleb128 0xd
 1657 0c84 930C0000 		.long	0xc93
 1658 0c88 0D       		.uleb128 0xd
 1659 0c89 400A0000 		.long	0xa40
 1660 0c8d 0D       		.uleb128 0xd
 1661 0c8e 220C0000 		.long	0xc22
 1662 0c92 00       		.byte	0
 1663 0c93 33       		.uleb128 0x33
 1664 0c94 08       		.byte	0x8
 1665 0c95 E20A0000 		.long	0xae2
 1666 0c99 34       		.uleb128 0x34
 1667 0c9a 00000000 		.long	.LASF150
 1668 0c9e 11       		.byte	0x11
 1669 0c9f FB02     		.value	0x2fb
 1670 0ca1 520A0000 		.long	0xa52
 1671 0ca5 B40C0000 		.long	0xcb4
 1672 0ca9 0D       		.uleb128 0xd
 1673 0caa 450B0000 		.long	0xb45
 1674 0cae 0D       		.uleb128 0xd
 1675 0caf 190B0000 		.long	0xb19
 1676 0cb3 00       		.byte	0
 1677 0cb4 34       		.uleb128 0x34
 1678 0cb5 00000000 		.long	.LASF151
 1679 0cb9 11       		.byte	0x11
 1680 0cba 0103     		.value	0x301
 1681 0cbc 520A0000 		.long	0xa52
 1682 0cc0 CA0C0000 		.long	0xcca
 1683 0cc4 0D       		.uleb128 0xd
 1684 0cc5 450B0000 		.long	0xb45
 1685 0cc9 00       		.byte	0
 1686 0cca 34       		.uleb128 0x34
 1687 0ccb 00000000 		.long	.LASF152
 1688 0ccf 11       		.byte	0x11
 1689 0cd0 5F02     		.value	0x25f
 1690 0cd2 B90A0000 		.long	0xab9
 1691 0cd6 EB0C0000 		.long	0xceb
 1692 0cda 0D       		.uleb128 0xd
 1693 0cdb 3F0B0000 		.long	0xb3f
 1694 0cdf 0D       		.uleb128 0xd
GAS LISTING /tmp/ccZP9VrL.s 			page 33


 1695 0ce0 400A0000 		.long	0xa40
 1696 0ce4 0D       		.uleb128 0xd
 1697 0ce5 820B0000 		.long	0xb82
 1698 0ce9 35       		.uleb128 0x35
 1699 0cea 00       		.byte	0
 1700 0ceb 34       		.uleb128 0x34
 1701 0cec 00000000 		.long	.LASF153
 1702 0cf0 11       		.byte	0x11
 1703 0cf1 8802     		.value	0x288
 1704 0cf3 B90A0000 		.long	0xab9
 1705 0cf7 070D0000 		.long	0xd07
 1706 0cfb 0D       		.uleb128 0xd
 1707 0cfc 820B0000 		.long	0xb82
 1708 0d00 0D       		.uleb128 0xd
 1709 0d01 820B0000 		.long	0xb82
 1710 0d05 35       		.uleb128 0x35
 1711 0d06 00       		.byte	0
 1712 0d07 34       		.uleb128 0x34
 1713 0d08 00000000 		.long	.LASF154
 1714 0d0c 11       		.byte	0x11
 1715 0d0d 1803     		.value	0x318
 1716 0d0f 520A0000 		.long	0xa52
 1717 0d13 220D0000 		.long	0xd22
 1718 0d17 0D       		.uleb128 0xd
 1719 0d18 520A0000 		.long	0xa52
 1720 0d1c 0D       		.uleb128 0xd
 1721 0d1d 190B0000 		.long	0xb19
 1722 0d21 00       		.byte	0
 1723 0d22 34       		.uleb128 0x34
 1724 0d23 00000000 		.long	.LASF155
 1725 0d27 11       		.byte	0x11
 1726 0d28 6702     		.value	0x267
 1727 0d2a B90A0000 		.long	0xab9
 1728 0d2e 420D0000 		.long	0xd42
 1729 0d32 0D       		.uleb128 0xd
 1730 0d33 190B0000 		.long	0xb19
 1731 0d37 0D       		.uleb128 0xd
 1732 0d38 820B0000 		.long	0xb82
 1733 0d3c 0D       		.uleb128 0xd
 1734 0d3d 420D0000 		.long	0xd42
 1735 0d41 00       		.byte	0
 1736 0d42 33       		.uleb128 0x33
 1737 0d43 08       		.byte	0x8
 1738 0d44 FA090000 		.long	0x9fa
 1739 0d48 34       		.uleb128 0x34
 1740 0d49 00000000 		.long	.LASF156
 1741 0d4d 11       		.byte	0x11
 1742 0d4e B402     		.value	0x2b4
 1743 0d50 B90A0000 		.long	0xab9
 1744 0d54 680D0000 		.long	0xd68
 1745 0d58 0D       		.uleb128 0xd
 1746 0d59 190B0000 		.long	0xb19
 1747 0d5d 0D       		.uleb128 0xd
 1748 0d5e 820B0000 		.long	0xb82
 1749 0d62 0D       		.uleb128 0xd
 1750 0d63 420D0000 		.long	0xd42
 1751 0d67 00       		.byte	0
GAS LISTING /tmp/ccZP9VrL.s 			page 34


 1752 0d68 34       		.uleb128 0x34
 1753 0d69 00000000 		.long	.LASF157
 1754 0d6d 11       		.byte	0x11
 1755 0d6e 7402     		.value	0x274
 1756 0d70 B90A0000 		.long	0xab9
 1757 0d74 8D0D0000 		.long	0xd8d
 1758 0d78 0D       		.uleb128 0xd
 1759 0d79 3F0B0000 		.long	0xb3f
 1760 0d7d 0D       		.uleb128 0xd
 1761 0d7e 400A0000 		.long	0xa40
 1762 0d82 0D       		.uleb128 0xd
 1763 0d83 820B0000 		.long	0xb82
 1764 0d87 0D       		.uleb128 0xd
 1765 0d88 420D0000 		.long	0xd42
 1766 0d8c 00       		.byte	0
 1767 0d8d 34       		.uleb128 0x34
 1768 0d8e 00000000 		.long	.LASF158
 1769 0d92 11       		.byte	0x11
 1770 0d93 C002     		.value	0x2c0
 1771 0d95 B90A0000 		.long	0xab9
 1772 0d99 AD0D0000 		.long	0xdad
 1773 0d9d 0D       		.uleb128 0xd
 1774 0d9e 820B0000 		.long	0xb82
 1775 0da2 0D       		.uleb128 0xd
 1776 0da3 820B0000 		.long	0xb82
 1777 0da7 0D       		.uleb128 0xd
 1778 0da8 420D0000 		.long	0xd42
 1779 0dac 00       		.byte	0
 1780 0dad 34       		.uleb128 0x34
 1781 0dae 00000000 		.long	.LASF159
 1782 0db2 11       		.byte	0x11
 1783 0db3 6F02     		.value	0x26f
 1784 0db5 B90A0000 		.long	0xab9
 1785 0db9 C80D0000 		.long	0xdc8
 1786 0dbd 0D       		.uleb128 0xd
 1787 0dbe 820B0000 		.long	0xb82
 1788 0dc2 0D       		.uleb128 0xd
 1789 0dc3 420D0000 		.long	0xd42
 1790 0dc7 00       		.byte	0
 1791 0dc8 34       		.uleb128 0x34
 1792 0dc9 00000000 		.long	.LASF160
 1793 0dcd 11       		.byte	0x11
 1794 0dce BC02     		.value	0x2bc
 1795 0dd0 B90A0000 		.long	0xab9
 1796 0dd4 E30D0000 		.long	0xde3
 1797 0dd8 0D       		.uleb128 0xd
 1798 0dd9 820B0000 		.long	0xb82
 1799 0ddd 0D       		.uleb128 0xd
 1800 0dde 420D0000 		.long	0xd42
 1801 0de2 00       		.byte	0
 1802 0de3 34       		.uleb128 0x34
 1803 0de4 00000000 		.long	.LASF161
 1804 0de8 11       		.byte	0x11
 1805 0de9 7501     		.value	0x175
 1806 0deb 400A0000 		.long	0xa40
 1807 0def 030E0000 		.long	0xe03
 1808 0df3 0D       		.uleb128 0xd
GAS LISTING /tmp/ccZP9VrL.s 			page 35


 1809 0df4 030E0000 		.long	0xe03
 1810 0df8 0D       		.uleb128 0xd
 1811 0df9 450B0000 		.long	0xb45
 1812 0dfd 0D       		.uleb128 0xd
 1813 0dfe 220C0000 		.long	0xc22
 1814 0e02 00       		.byte	0
 1815 0e03 33       		.uleb128 0x33
 1816 0e04 08       		.byte	0x8
 1817 0e05 B20A0000 		.long	0xab2
 1818 0e09 37       		.uleb128 0x37
 1819 0e0a 00000000 		.long	.LASF162
 1820 0e0e 11       		.byte	0x11
 1821 0e0f 9D       		.byte	0x9d
 1822 0e10 3F0B0000 		.long	0xb3f
 1823 0e14 230E0000 		.long	0xe23
 1824 0e18 0D       		.uleb128 0xd
 1825 0e19 3F0B0000 		.long	0xb3f
 1826 0e1d 0D       		.uleb128 0xd
 1827 0e1e 820B0000 		.long	0xb82
 1828 0e22 00       		.byte	0
 1829 0e23 37       		.uleb128 0x37
 1830 0e24 00000000 		.long	.LASF163
 1831 0e28 11       		.byte	0x11
 1832 0e29 A6       		.byte	0xa6
 1833 0e2a B90A0000 		.long	0xab9
 1834 0e2e 3D0E0000 		.long	0xe3d
 1835 0e32 0D       		.uleb128 0xd
 1836 0e33 820B0000 		.long	0xb82
 1837 0e37 0D       		.uleb128 0xd
 1838 0e38 820B0000 		.long	0xb82
 1839 0e3c 00       		.byte	0
 1840 0e3d 37       		.uleb128 0x37
 1841 0e3e 00000000 		.long	.LASF164
 1842 0e42 11       		.byte	0x11
 1843 0e43 C3       		.byte	0xc3
 1844 0e44 B90A0000 		.long	0xab9
 1845 0e48 570E0000 		.long	0xe57
 1846 0e4c 0D       		.uleb128 0xd
 1847 0e4d 820B0000 		.long	0xb82
 1848 0e51 0D       		.uleb128 0xd
 1849 0e52 820B0000 		.long	0xb82
 1850 0e56 00       		.byte	0
 1851 0e57 37       		.uleb128 0x37
 1852 0e58 00000000 		.long	.LASF165
 1853 0e5c 11       		.byte	0x11
 1854 0e5d 93       		.byte	0x93
 1855 0e5e 3F0B0000 		.long	0xb3f
 1856 0e62 710E0000 		.long	0xe71
 1857 0e66 0D       		.uleb128 0xd
 1858 0e67 3F0B0000 		.long	0xb3f
 1859 0e6b 0D       		.uleb128 0xd
 1860 0e6c 820B0000 		.long	0xb82
 1861 0e70 00       		.byte	0
 1862 0e71 37       		.uleb128 0x37
 1863 0e72 00000000 		.long	.LASF166
 1864 0e76 11       		.byte	0x11
 1865 0e77 FF       		.byte	0xff
GAS LISTING /tmp/ccZP9VrL.s 			page 36


 1866 0e78 400A0000 		.long	0xa40
 1867 0e7c 8B0E0000 		.long	0xe8b
 1868 0e80 0D       		.uleb128 0xd
 1869 0e81 820B0000 		.long	0xb82
 1870 0e85 0D       		.uleb128 0xd
 1871 0e86 820B0000 		.long	0xb82
 1872 0e8a 00       		.byte	0
 1873 0e8b 34       		.uleb128 0x34
 1874 0e8c 00000000 		.long	.LASF167
 1875 0e90 11       		.byte	0x11
 1876 0e91 5A03     		.value	0x35a
 1877 0e93 400A0000 		.long	0xa40
 1878 0e97 B00E0000 		.long	0xeb0
 1879 0e9b 0D       		.uleb128 0xd
 1880 0e9c 3F0B0000 		.long	0xb3f
 1881 0ea0 0D       		.uleb128 0xd
 1882 0ea1 400A0000 		.long	0xa40
 1883 0ea5 0D       		.uleb128 0xd
 1884 0ea6 820B0000 		.long	0xb82
 1885 0eaa 0D       		.uleb128 0xd
 1886 0eab B00E0000 		.long	0xeb0
 1887 0eaf 00       		.byte	0
 1888 0eb0 33       		.uleb128 0x33
 1889 0eb1 08       		.byte	0x8
 1890 0eb2 460F0000 		.long	0xf46
 1891 0eb6 38       		.uleb128 0x38
 1892 0eb7 746D00   		.string	"tm"
 1893 0eba 38       		.byte	0x38
 1894 0ebb 12       		.byte	0x12
 1895 0ebc 85       		.byte	0x85
 1896 0ebd 460F0000 		.long	0xf46
 1897 0ec1 2A       		.uleb128 0x2a
 1898 0ec2 00000000 		.long	.LASF168
 1899 0ec6 12       		.byte	0x12
 1900 0ec7 87       		.byte	0x87
 1901 0ec8 B90A0000 		.long	0xab9
 1902 0ecc 00       		.byte	0
 1903 0ecd 2A       		.uleb128 0x2a
 1904 0ece 00000000 		.long	.LASF169
 1905 0ed2 12       		.byte	0x12
 1906 0ed3 88       		.byte	0x88
 1907 0ed4 B90A0000 		.long	0xab9
 1908 0ed8 04       		.byte	0x4
 1909 0ed9 2A       		.uleb128 0x2a
 1910 0eda 00000000 		.long	.LASF170
 1911 0ede 12       		.byte	0x12
 1912 0edf 89       		.byte	0x89
 1913 0ee0 B90A0000 		.long	0xab9
 1914 0ee4 08       		.byte	0x8
 1915 0ee5 2A       		.uleb128 0x2a
 1916 0ee6 00000000 		.long	.LASF171
 1917 0eea 12       		.byte	0x12
 1918 0eeb 8A       		.byte	0x8a
 1919 0eec B90A0000 		.long	0xab9
 1920 0ef0 0C       		.byte	0xc
 1921 0ef1 2A       		.uleb128 0x2a
 1922 0ef2 00000000 		.long	.LASF172
GAS LISTING /tmp/ccZP9VrL.s 			page 37


 1923 0ef6 12       		.byte	0x12
 1924 0ef7 8B       		.byte	0x8b
 1925 0ef8 B90A0000 		.long	0xab9
 1926 0efc 10       		.byte	0x10
 1927 0efd 2A       		.uleb128 0x2a
 1928 0efe 00000000 		.long	.LASF173
 1929 0f02 12       		.byte	0x12
 1930 0f03 8C       		.byte	0x8c
 1931 0f04 B90A0000 		.long	0xab9
 1932 0f08 14       		.byte	0x14
 1933 0f09 2A       		.uleb128 0x2a
 1934 0f0a 00000000 		.long	.LASF174
 1935 0f0e 12       		.byte	0x12
 1936 0f0f 8D       		.byte	0x8d
 1937 0f10 B90A0000 		.long	0xab9
 1938 0f14 18       		.byte	0x18
 1939 0f15 2A       		.uleb128 0x2a
 1940 0f16 00000000 		.long	.LASF175
 1941 0f1a 12       		.byte	0x12
 1942 0f1b 8E       		.byte	0x8e
 1943 0f1c B90A0000 		.long	0xab9
 1944 0f20 1C       		.byte	0x1c
 1945 0f21 2A       		.uleb128 0x2a
 1946 0f22 00000000 		.long	.LASF176
 1947 0f26 12       		.byte	0x12
 1948 0f27 8F       		.byte	0x8f
 1949 0f28 B90A0000 		.long	0xab9
 1950 0f2c 20       		.byte	0x20
 1951 0f2d 2A       		.uleb128 0x2a
 1952 0f2e 00000000 		.long	.LASF177
 1953 0f32 12       		.byte	0x12
 1954 0f33 92       		.byte	0x92
 1955 0f34 87100000 		.long	0x1087
 1956 0f38 28       		.byte	0x28
 1957 0f39 2A       		.uleb128 0x2a
 1958 0f3a 00000000 		.long	.LASF178
 1959 0f3e 12       		.byte	0x12
 1960 0f3f 93       		.byte	0x93
 1961 0f40 E20A0000 		.long	0xae2
 1962 0f44 30       		.byte	0x30
 1963 0f45 00       		.byte	0
 1964 0f46 0E       		.uleb128 0xe
 1965 0f47 B60E0000 		.long	0xeb6
 1966 0f4b 34       		.uleb128 0x34
 1967 0f4c 00000000 		.long	.LASF179
 1968 0f50 11       		.byte	0x11
 1969 0f51 2201     		.value	0x122
 1970 0f53 400A0000 		.long	0xa40
 1971 0f57 610F0000 		.long	0xf61
 1972 0f5b 0D       		.uleb128 0xd
 1973 0f5c 820B0000 		.long	0xb82
 1974 0f60 00       		.byte	0
 1975 0f61 37       		.uleb128 0x37
 1976 0f62 00000000 		.long	.LASF180
 1977 0f66 11       		.byte	0x11
 1978 0f67 A1       		.byte	0xa1
 1979 0f68 3F0B0000 		.long	0xb3f
GAS LISTING /tmp/ccZP9VrL.s 			page 38


 1980 0f6c 800F0000 		.long	0xf80
 1981 0f70 0D       		.uleb128 0xd
 1982 0f71 3F0B0000 		.long	0xb3f
 1983 0f75 0D       		.uleb128 0xd
 1984 0f76 820B0000 		.long	0xb82
 1985 0f7a 0D       		.uleb128 0xd
 1986 0f7b 400A0000 		.long	0xa40
 1987 0f7f 00       		.byte	0
 1988 0f80 37       		.uleb128 0x37
 1989 0f81 00000000 		.long	.LASF181
 1990 0f85 11       		.byte	0x11
 1991 0f86 A9       		.byte	0xa9
 1992 0f87 B90A0000 		.long	0xab9
 1993 0f8b 9F0F0000 		.long	0xf9f
 1994 0f8f 0D       		.uleb128 0xd
 1995 0f90 820B0000 		.long	0xb82
 1996 0f94 0D       		.uleb128 0xd
 1997 0f95 820B0000 		.long	0xb82
 1998 0f99 0D       		.uleb128 0xd
 1999 0f9a 400A0000 		.long	0xa40
 2000 0f9e 00       		.byte	0
 2001 0f9f 37       		.uleb128 0x37
 2002 0fa0 00000000 		.long	.LASF182
 2003 0fa4 11       		.byte	0x11
 2004 0fa5 98       		.byte	0x98
 2005 0fa6 3F0B0000 		.long	0xb3f
 2006 0faa BE0F0000 		.long	0xfbe
 2007 0fae 0D       		.uleb128 0xd
 2008 0faf 3F0B0000 		.long	0xb3f
 2009 0fb3 0D       		.uleb128 0xd
 2010 0fb4 820B0000 		.long	0xb82
 2011 0fb8 0D       		.uleb128 0xd
 2012 0fb9 400A0000 		.long	0xa40
 2013 0fbd 00       		.byte	0
 2014 0fbe 34       		.uleb128 0x34
 2015 0fbf 00000000 		.long	.LASF183
 2016 0fc3 11       		.byte	0x11
 2017 0fc4 A101     		.value	0x1a1
 2018 0fc6 400A0000 		.long	0xa40
 2019 0fca E30F0000 		.long	0xfe3
 2020 0fce 0D       		.uleb128 0xd
 2021 0fcf 030E0000 		.long	0xe03
 2022 0fd3 0D       		.uleb128 0xd
 2023 0fd4 E30F0000 		.long	0xfe3
 2024 0fd8 0D       		.uleb128 0xd
 2025 0fd9 400A0000 		.long	0xa40
 2026 0fdd 0D       		.uleb128 0xd
 2027 0fde 220C0000 		.long	0xc22
 2028 0fe2 00       		.byte	0
 2029 0fe3 33       		.uleb128 0x33
 2030 0fe4 08       		.byte	0x8
 2031 0fe5 820B0000 		.long	0xb82
 2032 0fe9 34       		.uleb128 0x34
 2033 0fea 00000000 		.long	.LASF184
 2034 0fee 11       		.byte	0x11
 2035 0fef 0301     		.value	0x103
 2036 0ff1 400A0000 		.long	0xa40
GAS LISTING /tmp/ccZP9VrL.s 			page 39


 2037 0ff5 04100000 		.long	0x1004
 2038 0ff9 0D       		.uleb128 0xd
 2039 0ffa 820B0000 		.long	0xb82
 2040 0ffe 0D       		.uleb128 0xd
 2041 0fff 820B0000 		.long	0xb82
 2042 1003 00       		.byte	0
 2043 1004 34       		.uleb128 0x34
 2044 1005 00000000 		.long	.LASF185
 2045 1009 11       		.byte	0x11
 2046 100a C501     		.value	0x1c5
 2047 100c 50000000 		.long	0x50
 2048 1010 1F100000 		.long	0x101f
 2049 1014 0D       		.uleb128 0xd
 2050 1015 820B0000 		.long	0xb82
 2051 1019 0D       		.uleb128 0xd
 2052 101a 1F100000 		.long	0x101f
 2053 101e 00       		.byte	0
 2054 101f 33       		.uleb128 0x33
 2055 1020 08       		.byte	0x8
 2056 1021 3F0B0000 		.long	0xb3f
 2057 1025 34       		.uleb128 0x34
 2058 1026 00000000 		.long	.LASF186
 2059 102a 11       		.byte	0x11
 2060 102b CC01     		.value	0x1cc
 2061 102d 40100000 		.long	0x1040
 2062 1031 40100000 		.long	0x1040
 2063 1035 0D       		.uleb128 0xd
 2064 1036 820B0000 		.long	0xb82
 2065 103a 0D       		.uleb128 0xd
 2066 103b 1F100000 		.long	0x101f
 2067 103f 00       		.byte	0
 2068 1040 04       		.uleb128 0x4
 2069 1041 04       		.byte	0x4
 2070 1042 04       		.byte	0x4
 2071 1043 00000000 		.long	.LASF187
 2072 1047 34       		.uleb128 0x34
 2073 1048 00000000 		.long	.LASF188
 2074 104c 11       		.byte	0x11
 2075 104d 1D01     		.value	0x11d
 2076 104f 3F0B0000 		.long	0xb3f
 2077 1053 67100000 		.long	0x1067
 2078 1057 0D       		.uleb128 0xd
 2079 1058 3F0B0000 		.long	0xb3f
 2080 105c 0D       		.uleb128 0xd
 2081 105d 820B0000 		.long	0xb82
 2082 1061 0D       		.uleb128 0xd
 2083 1062 1F100000 		.long	0x101f
 2084 1066 00       		.byte	0
 2085 1067 34       		.uleb128 0x34
 2086 1068 00000000 		.long	.LASF189
 2087 106c 11       		.byte	0x11
 2088 106d D701     		.value	0x1d7
 2089 106f 87100000 		.long	0x1087
 2090 1073 87100000 		.long	0x1087
 2091 1077 0D       		.uleb128 0xd
 2092 1078 820B0000 		.long	0xb82
 2093 107c 0D       		.uleb128 0xd
GAS LISTING /tmp/ccZP9VrL.s 			page 40


 2094 107d 1F100000 		.long	0x101f
 2095 1081 0D       		.uleb128 0xd
 2096 1082 B90A0000 		.long	0xab9
 2097 1086 00       		.byte	0
 2098 1087 04       		.uleb128 0x4
 2099 1088 08       		.byte	0x8
 2100 1089 05       		.byte	0x5
 2101 108a 00000000 		.long	.LASF190
 2102 108e 34       		.uleb128 0x34
 2103 108f 00000000 		.long	.LASF191
 2104 1093 11       		.byte	0x11
 2105 1094 DC01     		.value	0x1dc
 2106 1096 4B0A0000 		.long	0xa4b
 2107 109a AE100000 		.long	0x10ae
 2108 109e 0D       		.uleb128 0xd
 2109 109f 820B0000 		.long	0xb82
 2110 10a3 0D       		.uleb128 0xd
 2111 10a4 1F100000 		.long	0x101f
 2112 10a8 0D       		.uleb128 0xd
 2113 10a9 B90A0000 		.long	0xab9
 2114 10ad 00       		.byte	0
 2115 10ae 37       		.uleb128 0x37
 2116 10af 00000000 		.long	.LASF192
 2117 10b3 11       		.byte	0x11
 2118 10b4 C7       		.byte	0xc7
 2119 10b5 400A0000 		.long	0xa40
 2120 10b9 CD100000 		.long	0x10cd
 2121 10bd 0D       		.uleb128 0xd
 2122 10be 3F0B0000 		.long	0xb3f
 2123 10c2 0D       		.uleb128 0xd
 2124 10c3 820B0000 		.long	0xb82
 2125 10c7 0D       		.uleb128 0xd
 2126 10c8 400A0000 		.long	0xa40
 2127 10cc 00       		.byte	0
 2128 10cd 34       		.uleb128 0x34
 2129 10ce 00000000 		.long	.LASF193
 2130 10d2 11       		.byte	0x11
 2131 10d3 6801     		.value	0x168
 2132 10d5 B90A0000 		.long	0xab9
 2133 10d9 E3100000 		.long	0x10e3
 2134 10dd 0D       		.uleb128 0xd
 2135 10de 520A0000 		.long	0xa52
 2136 10e2 00       		.byte	0
 2137 10e3 34       		.uleb128 0x34
 2138 10e4 00000000 		.long	.LASF194
 2139 10e8 11       		.byte	0x11
 2140 10e9 4801     		.value	0x148
 2141 10eb B90A0000 		.long	0xab9
 2142 10ef 03110000 		.long	0x1103
 2143 10f3 0D       		.uleb128 0xd
 2144 10f4 820B0000 		.long	0xb82
 2145 10f8 0D       		.uleb128 0xd
 2146 10f9 820B0000 		.long	0xb82
 2147 10fd 0D       		.uleb128 0xd
 2148 10fe 400A0000 		.long	0xa40
 2149 1102 00       		.byte	0
 2150 1103 34       		.uleb128 0x34
GAS LISTING /tmp/ccZP9VrL.s 			page 41


 2151 1104 00000000 		.long	.LASF195
 2152 1108 11       		.byte	0x11
 2153 1109 4C01     		.value	0x14c
 2154 110b 3F0B0000 		.long	0xb3f
 2155 110f 23110000 		.long	0x1123
 2156 1113 0D       		.uleb128 0xd
 2157 1114 3F0B0000 		.long	0xb3f
 2158 1118 0D       		.uleb128 0xd
 2159 1119 820B0000 		.long	0xb82
 2160 111d 0D       		.uleb128 0xd
 2161 111e 400A0000 		.long	0xa40
 2162 1122 00       		.byte	0
 2163 1123 34       		.uleb128 0x34
 2164 1124 00000000 		.long	.LASF196
 2165 1128 11       		.byte	0x11
 2166 1129 5101     		.value	0x151
 2167 112b 3F0B0000 		.long	0xb3f
 2168 112f 43110000 		.long	0x1143
 2169 1133 0D       		.uleb128 0xd
 2170 1134 3F0B0000 		.long	0xb3f
 2171 1138 0D       		.uleb128 0xd
 2172 1139 820B0000 		.long	0xb82
 2173 113d 0D       		.uleb128 0xd
 2174 113e 400A0000 		.long	0xa40
 2175 1142 00       		.byte	0
 2176 1143 34       		.uleb128 0x34
 2177 1144 00000000 		.long	.LASF197
 2178 1148 11       		.byte	0x11
 2179 1149 5501     		.value	0x155
 2180 114b 3F0B0000 		.long	0xb3f
 2181 114f 63110000 		.long	0x1163
 2182 1153 0D       		.uleb128 0xd
 2183 1154 3F0B0000 		.long	0xb3f
 2184 1158 0D       		.uleb128 0xd
 2185 1159 450B0000 		.long	0xb45
 2186 115d 0D       		.uleb128 0xd
 2187 115e 400A0000 		.long	0xa40
 2188 1162 00       		.byte	0
 2189 1163 34       		.uleb128 0x34
 2190 1164 00000000 		.long	.LASF198
 2191 1168 11       		.byte	0x11
 2192 1169 5C02     		.value	0x25c
 2193 116b B90A0000 		.long	0xab9
 2194 116f 7A110000 		.long	0x117a
 2195 1173 0D       		.uleb128 0xd
 2196 1174 820B0000 		.long	0xb82
 2197 1178 35       		.uleb128 0x35
 2198 1179 00       		.byte	0
 2199 117a 34       		.uleb128 0x34
 2200 117b 00000000 		.long	.LASF199
 2201 117f 11       		.byte	0x11
 2202 1180 8502     		.value	0x285
 2203 1182 B90A0000 		.long	0xab9
 2204 1186 91110000 		.long	0x1191
 2205 118a 0D       		.uleb128 0xd
 2206 118b 820B0000 		.long	0xb82
 2207 118f 35       		.uleb128 0x35
GAS LISTING /tmp/ccZP9VrL.s 			page 42


 2208 1190 00       		.byte	0
 2209 1191 39       		.uleb128 0x39
 2210 1192 00000000 		.long	.LASF200
 2211 1196 11       		.byte	0x11
 2212 1197 E3       		.byte	0xe3
 2213 1198 00000000 		.long	.LASF200
 2214 119c 820B0000 		.long	0xb82
 2215 11a0 AF110000 		.long	0x11af
 2216 11a4 0D       		.uleb128 0xd
 2217 11a5 820B0000 		.long	0xb82
 2218 11a9 0D       		.uleb128 0xd
 2219 11aa 450B0000 		.long	0xb45
 2220 11ae 00       		.byte	0
 2221 11af 10       		.uleb128 0x10
 2222 11b0 00000000 		.long	.LASF201
 2223 11b4 11       		.byte	0x11
 2224 11b5 0901     		.value	0x109
 2225 11b7 00000000 		.long	.LASF201
 2226 11bb 820B0000 		.long	0xb82
 2227 11bf CE110000 		.long	0x11ce
 2228 11c3 0D       		.uleb128 0xd
 2229 11c4 820B0000 		.long	0xb82
 2230 11c8 0D       		.uleb128 0xd
 2231 11c9 820B0000 		.long	0xb82
 2232 11cd 00       		.byte	0
 2233 11ce 39       		.uleb128 0x39
 2234 11cf 00000000 		.long	.LASF202
 2235 11d3 11       		.byte	0x11
 2236 11d4 ED       		.byte	0xed
 2237 11d5 00000000 		.long	.LASF202
 2238 11d9 820B0000 		.long	0xb82
 2239 11dd EC110000 		.long	0x11ec
 2240 11e1 0D       		.uleb128 0xd
 2241 11e2 820B0000 		.long	0xb82
 2242 11e6 0D       		.uleb128 0xd
 2243 11e7 450B0000 		.long	0xb45
 2244 11eb 00       		.byte	0
 2245 11ec 10       		.uleb128 0x10
 2246 11ed 00000000 		.long	.LASF203
 2247 11f1 11       		.byte	0x11
 2248 11f2 1401     		.value	0x114
 2249 11f4 00000000 		.long	.LASF203
 2250 11f8 820B0000 		.long	0xb82
 2251 11fc 0B120000 		.long	0x120b
 2252 1200 0D       		.uleb128 0xd
 2253 1201 820B0000 		.long	0xb82
 2254 1205 0D       		.uleb128 0xd
 2255 1206 820B0000 		.long	0xb82
 2256 120a 00       		.byte	0
 2257 120b 10       		.uleb128 0x10
 2258 120c 00000000 		.long	.LASF204
 2259 1210 11       		.byte	0x11
 2260 1211 3F01     		.value	0x13f
 2261 1213 00000000 		.long	.LASF204
 2262 1217 820B0000 		.long	0xb82
 2263 121b 2F120000 		.long	0x122f
 2264 121f 0D       		.uleb128 0xd
GAS LISTING /tmp/ccZP9VrL.s 			page 43


 2265 1220 820B0000 		.long	0xb82
 2266 1224 0D       		.uleb128 0xd
 2267 1225 450B0000 		.long	0xb45
 2268 1229 0D       		.uleb128 0xd
 2269 122a 400A0000 		.long	0xa40
 2270 122e 00       		.byte	0
 2271 122f 34       		.uleb128 0x34
 2272 1230 00000000 		.long	.LASF205
 2273 1234 11       		.byte	0x11
 2274 1235 CE01     		.value	0x1ce
 2275 1237 4A120000 		.long	0x124a
 2276 123b 4A120000 		.long	0x124a
 2277 123f 0D       		.uleb128 0xd
 2278 1240 820B0000 		.long	0xb82
 2279 1244 0D       		.uleb128 0xd
 2280 1245 1F100000 		.long	0x101f
 2281 1249 00       		.byte	0
 2282 124a 04       		.uleb128 0x4
 2283 124b 10       		.byte	0x10
 2284 124c 04       		.byte	0x4
 2285 124d 00000000 		.long	.LASF206
 2286 1251 34       		.uleb128 0x34
 2287 1252 00000000 		.long	.LASF207
 2288 1256 11       		.byte	0x11
 2289 1257 E601     		.value	0x1e6
 2290 1259 71120000 		.long	0x1271
 2291 125d 71120000 		.long	0x1271
 2292 1261 0D       		.uleb128 0xd
 2293 1262 820B0000 		.long	0xb82
 2294 1266 0D       		.uleb128 0xd
 2295 1267 1F100000 		.long	0x101f
 2296 126b 0D       		.uleb128 0xd
 2297 126c B90A0000 		.long	0xab9
 2298 1270 00       		.byte	0
 2299 1271 04       		.uleb128 0x4
 2300 1272 08       		.byte	0x8
 2301 1273 05       		.byte	0x5
 2302 1274 00000000 		.long	.LASF208
 2303 1278 34       		.uleb128 0x34
 2304 1279 00000000 		.long	.LASF209
 2305 127d 11       		.byte	0x11
 2306 127e ED01     		.value	0x1ed
 2307 1280 98120000 		.long	0x1298
 2308 1284 98120000 		.long	0x1298
 2309 1288 0D       		.uleb128 0xd
 2310 1289 820B0000 		.long	0xb82
 2311 128d 0D       		.uleb128 0xd
 2312 128e 1F100000 		.long	0x101f
 2313 1292 0D       		.uleb128 0xd
 2314 1293 B90A0000 		.long	0xab9
 2315 1297 00       		.byte	0
 2316 1298 04       		.uleb128 0x4
 2317 1299 08       		.byte	0x8
 2318 129a 07       		.byte	0x7
 2319 129b 00000000 		.long	.LASF210
 2320 129f 04       		.uleb128 0x4
 2321 12a0 01       		.byte	0x1
GAS LISTING /tmp/ccZP9VrL.s 			page 44


 2322 12a1 08       		.byte	0x8
 2323 12a2 00000000 		.long	.LASF211
 2324 12a6 04       		.uleb128 0x4
 2325 12a7 01       		.byte	0x1
 2326 12a8 06       		.byte	0x6
 2327 12a9 00000000 		.long	.LASF212
 2328 12ad 04       		.uleb128 0x4
 2329 12ae 02       		.byte	0x2
 2330 12af 05       		.byte	0x5
 2331 12b0 00000000 		.long	.LASF213
 2332 12b4 26       		.uleb128 0x26
 2333 12b5 00000000 		.long	.LASF214
 2334 12b9 05       		.byte	0x5
 2335 12ba 37       		.byte	0x37
 2336 12bb C7120000 		.long	0x12c7
 2337 12bf 07       		.uleb128 0x7
 2338 12c0 05       		.byte	0x5
 2339 12c1 38       		.byte	0x38
 2340 12c2 1E020000 		.long	0x21e
 2341 12c6 00       		.byte	0
 2342 12c7 3A       		.uleb128 0x3a
 2343 12c8 08       		.byte	0x8
 2344 12c9 31020000 		.long	0x231
 2345 12cd 3A       		.uleb128 0x3a
 2346 12ce 08       		.byte	0x8
 2347 12cf 61020000 		.long	0x261
 2348 12d3 04       		.uleb128 0x4
 2349 12d4 01       		.byte	0x1
 2350 12d5 02       		.byte	0x2
 2351 12d6 00000000 		.long	.LASF215
 2352 12da 33       		.uleb128 0x33
 2353 12db 08       		.byte	0x8
 2354 12dc 61020000 		.long	0x261
 2355 12e0 33       		.uleb128 0x33
 2356 12e1 08       		.byte	0x8
 2357 12e2 31020000 		.long	0x231
 2358 12e6 3A       		.uleb128 0x3a
 2359 12e7 08       		.byte	0x8
 2360 12e8 88030000 		.long	0x388
 2361 12ec 0A       		.uleb128 0xa
 2362 12ed 00000000 		.long	.LASF216
 2363 12f1 60       		.byte	0x60
 2364 12f2 13       		.byte	0x13
 2365 12f3 35       		.byte	0x35
 2366 12f4 19140000 		.long	0x1419
 2367 12f8 2A       		.uleb128 0x2a
 2368 12f9 00000000 		.long	.LASF217
 2369 12fd 13       		.byte	0x13
 2370 12fe 39       		.byte	0x39
 2371 12ff 030E0000 		.long	0xe03
 2372 1303 00       		.byte	0
 2373 1304 2A       		.uleb128 0x2a
 2374 1305 00000000 		.long	.LASF218
 2375 1309 13       		.byte	0x13
 2376 130a 3A       		.byte	0x3a
 2377 130b 030E0000 		.long	0xe03
 2378 130f 08       		.byte	0x8
GAS LISTING /tmp/ccZP9VrL.s 			page 45


 2379 1310 2A       		.uleb128 0x2a
 2380 1311 00000000 		.long	.LASF219
 2381 1315 13       		.byte	0x13
 2382 1316 40       		.byte	0x40
 2383 1317 030E0000 		.long	0xe03
 2384 131b 10       		.byte	0x10
 2385 131c 2A       		.uleb128 0x2a
 2386 131d 00000000 		.long	.LASF220
 2387 1321 13       		.byte	0x13
 2388 1322 46       		.byte	0x46
 2389 1323 030E0000 		.long	0xe03
 2390 1327 18       		.byte	0x18
 2391 1328 2A       		.uleb128 0x2a
 2392 1329 00000000 		.long	.LASF221
 2393 132d 13       		.byte	0x13
 2394 132e 47       		.byte	0x47
 2395 132f 030E0000 		.long	0xe03
 2396 1333 20       		.byte	0x20
 2397 1334 2A       		.uleb128 0x2a
 2398 1335 00000000 		.long	.LASF222
 2399 1339 13       		.byte	0x13
 2400 133a 48       		.byte	0x48
 2401 133b 030E0000 		.long	0xe03
 2402 133f 28       		.byte	0x28
 2403 1340 2A       		.uleb128 0x2a
 2404 1341 00000000 		.long	.LASF223
 2405 1345 13       		.byte	0x13
 2406 1346 49       		.byte	0x49
 2407 1347 030E0000 		.long	0xe03
 2408 134b 30       		.byte	0x30
 2409 134c 2A       		.uleb128 0x2a
 2410 134d 00000000 		.long	.LASF224
 2411 1351 13       		.byte	0x13
 2412 1352 4A       		.byte	0x4a
 2413 1353 030E0000 		.long	0xe03
 2414 1357 38       		.byte	0x38
 2415 1358 2A       		.uleb128 0x2a
 2416 1359 00000000 		.long	.LASF225
 2417 135d 13       		.byte	0x13
 2418 135e 4B       		.byte	0x4b
 2419 135f 030E0000 		.long	0xe03
 2420 1363 40       		.byte	0x40
 2421 1364 2A       		.uleb128 0x2a
 2422 1365 00000000 		.long	.LASF226
 2423 1369 13       		.byte	0x13
 2424 136a 4C       		.byte	0x4c
 2425 136b 030E0000 		.long	0xe03
 2426 136f 48       		.byte	0x48
 2427 1370 2A       		.uleb128 0x2a
 2428 1371 00000000 		.long	.LASF227
 2429 1375 13       		.byte	0x13
 2430 1376 4D       		.byte	0x4d
 2431 1377 B20A0000 		.long	0xab2
 2432 137b 50       		.byte	0x50
 2433 137c 2A       		.uleb128 0x2a
 2434 137d 00000000 		.long	.LASF228
 2435 1381 13       		.byte	0x13
GAS LISTING /tmp/ccZP9VrL.s 			page 46


 2436 1382 4E       		.byte	0x4e
 2437 1383 B20A0000 		.long	0xab2
 2438 1387 51       		.byte	0x51
 2439 1388 2A       		.uleb128 0x2a
 2440 1389 00000000 		.long	.LASF229
 2441 138d 13       		.byte	0x13
 2442 138e 50       		.byte	0x50
 2443 138f B20A0000 		.long	0xab2
 2444 1393 52       		.byte	0x52
 2445 1394 2A       		.uleb128 0x2a
 2446 1395 00000000 		.long	.LASF230
 2447 1399 13       		.byte	0x13
 2448 139a 52       		.byte	0x52
 2449 139b B20A0000 		.long	0xab2
 2450 139f 53       		.byte	0x53
 2451 13a0 2A       		.uleb128 0x2a
 2452 13a1 00000000 		.long	.LASF231
 2453 13a5 13       		.byte	0x13
 2454 13a6 54       		.byte	0x54
 2455 13a7 B20A0000 		.long	0xab2
 2456 13ab 54       		.byte	0x54
 2457 13ac 2A       		.uleb128 0x2a
 2458 13ad 00000000 		.long	.LASF232
 2459 13b1 13       		.byte	0x13
 2460 13b2 56       		.byte	0x56
 2461 13b3 B20A0000 		.long	0xab2
 2462 13b7 55       		.byte	0x55
 2463 13b8 2A       		.uleb128 0x2a
 2464 13b9 00000000 		.long	.LASF233
 2465 13bd 13       		.byte	0x13
 2466 13be 5D       		.byte	0x5d
 2467 13bf B20A0000 		.long	0xab2
 2468 13c3 56       		.byte	0x56
 2469 13c4 2A       		.uleb128 0x2a
 2470 13c5 00000000 		.long	.LASF234
 2471 13c9 13       		.byte	0x13
 2472 13ca 5E       		.byte	0x5e
 2473 13cb B20A0000 		.long	0xab2
 2474 13cf 57       		.byte	0x57
 2475 13d0 2A       		.uleb128 0x2a
 2476 13d1 00000000 		.long	.LASF235
 2477 13d5 13       		.byte	0x13
 2478 13d6 61       		.byte	0x61
 2479 13d7 B20A0000 		.long	0xab2
 2480 13db 58       		.byte	0x58
 2481 13dc 2A       		.uleb128 0x2a
 2482 13dd 00000000 		.long	.LASF236
 2483 13e1 13       		.byte	0x13
 2484 13e2 63       		.byte	0x63
 2485 13e3 B20A0000 		.long	0xab2
 2486 13e7 59       		.byte	0x59
 2487 13e8 2A       		.uleb128 0x2a
 2488 13e9 00000000 		.long	.LASF237
 2489 13ed 13       		.byte	0x13
 2490 13ee 65       		.byte	0x65
 2491 13ef B20A0000 		.long	0xab2
 2492 13f3 5A       		.byte	0x5a
GAS LISTING /tmp/ccZP9VrL.s 			page 47


 2493 13f4 2A       		.uleb128 0x2a
 2494 13f5 00000000 		.long	.LASF238
 2495 13f9 13       		.byte	0x13
 2496 13fa 67       		.byte	0x67
 2497 13fb B20A0000 		.long	0xab2
 2498 13ff 5B       		.byte	0x5b
 2499 1400 2A       		.uleb128 0x2a
 2500 1401 00000000 		.long	.LASF239
 2501 1405 13       		.byte	0x13
 2502 1406 6E       		.byte	0x6e
 2503 1407 B20A0000 		.long	0xab2
 2504 140b 5C       		.byte	0x5c
 2505 140c 2A       		.uleb128 0x2a
 2506 140d 00000000 		.long	.LASF240
 2507 1411 13       		.byte	0x13
 2508 1412 6F       		.byte	0x6f
 2509 1413 B20A0000 		.long	0xab2
 2510 1417 5D       		.byte	0x5d
 2511 1418 00       		.byte	0
 2512 1419 37       		.uleb128 0x37
 2513 141a 00000000 		.long	.LASF241
 2514 141e 13       		.byte	0x13
 2515 141f 7C       		.byte	0x7c
 2516 1420 030E0000 		.long	0xe03
 2517 1424 33140000 		.long	0x1433
 2518 1428 0D       		.uleb128 0xd
 2519 1429 B90A0000 		.long	0xab9
 2520 142d 0D       		.uleb128 0xd
 2521 142e E20A0000 		.long	0xae2
 2522 1432 00       		.byte	0
 2523 1433 3B       		.uleb128 0x3b
 2524 1434 00000000 		.long	.LASF243
 2525 1438 13       		.byte	0x13
 2526 1439 7F       		.byte	0x7f
 2527 143a 3E140000 		.long	0x143e
 2528 143e 33       		.uleb128 0x33
 2529 143f 08       		.byte	0x8
 2530 1440 EC120000 		.long	0x12ec
 2531 1444 0B       		.uleb128 0xb
 2532 1445 00000000 		.long	.LASF244
 2533 1449 14       		.byte	0x14
 2534 144a 28       		.byte	0x28
 2535 144b B90A0000 		.long	0xab9
 2536 144f 0B       		.uleb128 0xb
 2537 1450 00000000 		.long	.LASF245
 2538 1454 15       		.byte	0x15
 2539 1455 20       		.byte	0x20
 2540 1456 B90A0000 		.long	0xab9
 2541 145a 0E       		.uleb128 0xe
 2542 145b D3120000 		.long	0x12d3
 2543 145f 0E       		.uleb128 0xe
 2544 1460 4B0A0000 		.long	0xa4b
 2545 1464 33       		.uleb128 0x33
 2546 1465 08       		.byte	0x8
 2547 1466 84050000 		.long	0x584
 2548 146a 0B       		.uleb128 0xb
 2549 146b 00000000 		.long	.LASF246
GAS LISTING /tmp/ccZP9VrL.s 			page 48


 2550 146f 16       		.byte	0x16
 2551 1470 34       		.byte	0x34
 2552 1471 4B0A0000 		.long	0xa4b
 2553 1475 0B       		.uleb128 0xb
 2554 1476 00000000 		.long	.LASF247
 2555 147a 16       		.byte	0x16
 2556 147b BA       		.byte	0xba
 2557 147c 80140000 		.long	0x1480
 2558 1480 33       		.uleb128 0x33
 2559 1481 08       		.byte	0x8
 2560 1482 86140000 		.long	0x1486
 2561 1486 0E       		.uleb128 0xe
 2562 1487 44140000 		.long	0x1444
 2563 148b 37       		.uleb128 0x37
 2564 148c 00000000 		.long	.LASF248
 2565 1490 16       		.byte	0x16
 2566 1491 AF       		.byte	0xaf
 2567 1492 B90A0000 		.long	0xab9
 2568 1496 A5140000 		.long	0x14a5
 2569 149a 0D       		.uleb128 0xd
 2570 149b 520A0000 		.long	0xa52
 2571 149f 0D       		.uleb128 0xd
 2572 14a0 6A140000 		.long	0x146a
 2573 14a4 00       		.byte	0
 2574 14a5 37       		.uleb128 0x37
 2575 14a6 00000000 		.long	.LASF249
 2576 14aa 16       		.byte	0x16
 2577 14ab DD       		.byte	0xdd
 2578 14ac 520A0000 		.long	0xa52
 2579 14b0 BF140000 		.long	0x14bf
 2580 14b4 0D       		.uleb128 0xd
 2581 14b5 520A0000 		.long	0xa52
 2582 14b9 0D       		.uleb128 0xd
 2583 14ba 75140000 		.long	0x1475
 2584 14be 00       		.byte	0
 2585 14bf 37       		.uleb128 0x37
 2586 14c0 00000000 		.long	.LASF250
 2587 14c4 16       		.byte	0x16
 2588 14c5 DA       		.byte	0xda
 2589 14c6 75140000 		.long	0x1475
 2590 14ca D4140000 		.long	0x14d4
 2591 14ce 0D       		.uleb128 0xd
 2592 14cf E20A0000 		.long	0xae2
 2593 14d3 00       		.byte	0
 2594 14d4 37       		.uleb128 0x37
 2595 14d5 00000000 		.long	.LASF251
 2596 14d9 16       		.byte	0x16
 2597 14da AB       		.byte	0xab
 2598 14db 6A140000 		.long	0x146a
 2599 14df E9140000 		.long	0x14e9
 2600 14e3 0D       		.uleb128 0xd
 2601 14e4 E20A0000 		.long	0xae2
 2602 14e8 00       		.byte	0
 2603 14e9 0E       		.uleb128 0xe
 2604 14ea AD120000 		.long	0x12ad
 2605 14ee 0E       		.uleb128 0xe
 2606 14ef 87100000 		.long	0x1087
GAS LISTING /tmp/ccZP9VrL.s 			page 49


 2607 14f3 3C       		.uleb128 0x3c
 2608 14f4 00000000 		.long	.LASF279
 2609 14f8 01       		.byte	0x1
 2610 14f9 09       		.byte	0x9
 2611 14fa B90A0000 		.long	0xab9
 2612 14fe 00000000 		.quad	.LFB1021
 2612      00000000 
 2613 1506 86000000 		.quad	.LFE1021-.LFB1021
 2613      00000000 
 2614 150e 01       		.uleb128 0x1
 2615 150f 9C       		.byte	0x9c
 2616 1510 2D150000 		.long	0x152d
 2617 1514 3D       		.uleb128 0x3d
 2618 1515 7A00     		.string	"z"
 2619 1517 01       		.byte	0x1
 2620 1518 0E       		.byte	0xe
 2621 1519 2D000000 		.long	0x2d
 2622 151d 02       		.uleb128 0x2
 2623 151e 91       		.byte	0x91
 2624 151f 50       		.sleb128 -48
 2625 1520 3D       		.uleb128 0x3d
 2626 1521 6300     		.string	"c"
 2627 1523 01       		.byte	0x1
 2628 1524 0E       		.byte	0xe
 2629 1525 2D000000 		.long	0x2d
 2630 1529 02       		.uleb128 0x2
 2631 152a 91       		.byte	0x91
 2632 152b 50       		.sleb128 -48
 2633 152c 00       		.byte	0
 2634 152d 3E       		.uleb128 0x3e
 2635 152e 00000000 		.long	.LASF280
 2636 1532 00000000 		.quad	.LFB1026
 2636      00000000 
 2637 153a 3E000000 		.quad	.LFE1026-.LFB1026
 2637      00000000 
 2638 1542 01       		.uleb128 0x1
 2639 1543 9C       		.byte	0x9c
 2640 1544 65150000 		.long	0x1565
 2641 1548 3F       		.uleb128 0x3f
 2642 1549 00000000 		.long	.LASF253
 2643 154d 01       		.byte	0x1
 2644 154e 17       		.byte	0x17
 2645 154f B90A0000 		.long	0xab9
 2646 1553 02       		.uleb128 0x2
 2647 1554 91       		.byte	0x91
 2648 1555 6C       		.sleb128 -20
 2649 1556 3F       		.uleb128 0x3f
 2650 1557 00000000 		.long	.LASF254
 2651 155b 01       		.byte	0x1
 2652 155c 17       		.byte	0x17
 2653 155d B90A0000 		.long	0xab9
 2654 1561 02       		.uleb128 0x2
 2655 1562 91       		.byte	0x91
 2656 1563 68       		.sleb128 -24
 2657 1564 00       		.byte	0
 2658 1565 40       		.uleb128 0x40
 2659 1566 00000000 		.long	.LASF281
GAS LISTING /tmp/ccZP9VrL.s 			page 50


 2660 156a 00000000 		.quad	.LFB1027
 2660      00000000 
 2661 1572 15000000 		.quad	.LFE1027-.LFB1027
 2661      00000000 
 2662 157a 01       		.uleb128 0x1
 2663 157b 9C       		.byte	0x9c
 2664 157c 41       		.uleb128 0x41
 2665 157d 00000000 		.long	.LASF255
 2666 1581 3E0A0000 		.long	0xa3e
 2667 1585 42       		.uleb128 0x42
 2668 1586 4B080000 		.long	0x84b
 2669 158a 09       		.uleb128 0x9
 2670 158b 03       		.byte	0x3
 2671 158c 00000000 		.quad	_ZStL8__ioinit
 2671      00000000 
 2672 1594 43       		.uleb128 0x43
 2673 1595 A8080000 		.long	0x8a8
 2674 1599 00000000 		.long	.LASF256
 2675 159d 80808080 		.sleb128 -2147483648
 2675      78
 2676 15a2 44       		.uleb128 0x44
 2677 15a3 B3080000 		.long	0x8b3
 2678 15a7 00000000 		.long	.LASF257
 2679 15ab FFFFFF7F 		.long	0x7fffffff
 2680 15af 45       		.uleb128 0x45
 2681 15b0 0B090000 		.long	0x90b
 2682 15b4 00000000 		.long	.LASF258
 2683 15b8 40       		.byte	0x40
 2684 15b9 45       		.uleb128 0x45
 2685 15ba 37090000 		.long	0x937
 2686 15be 00000000 		.long	.LASF259
 2687 15c2 7F       		.byte	0x7f
 2688 15c3 43       		.uleb128 0x43
 2689 15c4 6E090000 		.long	0x96e
 2690 15c8 00000000 		.long	.LASF260
 2691 15cc 80807E   		.sleb128 -32768
 2692 15cf 46       		.uleb128 0x46
 2693 15d0 79090000 		.long	0x979
 2694 15d4 00000000 		.long	.LASF261
 2695 15d8 FF7F     		.value	0x7fff
 2696 15da 43       		.uleb128 0x43
 2697 15db AC090000 		.long	0x9ac
 2698 15df 00000000 		.long	.LASF262
 2699 15e3 80808080 		.sleb128 -9223372036854775808
 2699      80808080 
 2699      807F
 2700 15ed 47       		.uleb128 0x47
 2701 15ee B7090000 		.long	0x9b7
 2702 15f2 00000000 		.long	.LASF263
 2703 15f6 FFFFFFFF 		.quad	0x7fffffffffffffff
 2703      FFFFFF7F 
 2704 15fe 00       		.byte	0
 2705              		.section	.debug_abbrev,"",@progbits
 2706              	.Ldebug_abbrev0:
 2707 0000 01       		.uleb128 0x1
 2708 0001 11       		.uleb128 0x11
 2709 0002 01       		.byte	0x1
GAS LISTING /tmp/ccZP9VrL.s 			page 51


 2710 0003 25       		.uleb128 0x25
 2711 0004 0E       		.uleb128 0xe
 2712 0005 13       		.uleb128 0x13
 2713 0006 0B       		.uleb128 0xb
 2714 0007 03       		.uleb128 0x3
 2715 0008 0E       		.uleb128 0xe
 2716 0009 1B       		.uleb128 0x1b
 2717 000a 0E       		.uleb128 0xe
 2718 000b 11       		.uleb128 0x11
 2719 000c 01       		.uleb128 0x1
 2720 000d 12       		.uleb128 0x12
 2721 000e 07       		.uleb128 0x7
 2722 000f 10       		.uleb128 0x10
 2723 0010 17       		.uleb128 0x17
 2724 0011 00       		.byte	0
 2725 0012 00       		.byte	0
 2726 0013 02       		.uleb128 0x2
 2727 0014 02       		.uleb128 0x2
 2728 0015 01       		.byte	0x1
 2729 0016 03       		.uleb128 0x3
 2730 0017 0E       		.uleb128 0xe
 2731 0018 0B       		.uleb128 0xb
 2732 0019 0B       		.uleb128 0xb
 2733 001a 3A       		.uleb128 0x3a
 2734 001b 0B       		.uleb128 0xb
 2735 001c 3B       		.uleb128 0x3b
 2736 001d 0B       		.uleb128 0xb
 2737 001e 01       		.uleb128 0x1
 2738 001f 13       		.uleb128 0x13
 2739 0020 00       		.byte	0
 2740 0021 00       		.byte	0
 2741 0022 03       		.uleb128 0x3
 2742 0023 0D       		.uleb128 0xd
 2743 0024 00       		.byte	0
 2744 0025 03       		.uleb128 0x3
 2745 0026 08       		.uleb128 0x8
 2746 0027 3A       		.uleb128 0x3a
 2747 0028 0B       		.uleb128 0xb
 2748 0029 3B       		.uleb128 0x3b
 2749 002a 0B       		.uleb128 0xb
 2750 002b 49       		.uleb128 0x49
 2751 002c 13       		.uleb128 0x13
 2752 002d 38       		.uleb128 0x38
 2753 002e 0B       		.uleb128 0xb
 2754 002f 32       		.uleb128 0x32
 2755 0030 0B       		.uleb128 0xb
 2756 0031 00       		.byte	0
 2757 0032 00       		.byte	0
 2758 0033 04       		.uleb128 0x4
 2759 0034 24       		.uleb128 0x24
 2760 0035 00       		.byte	0
 2761 0036 0B       		.uleb128 0xb
 2762 0037 0B       		.uleb128 0xb
 2763 0038 3E       		.uleb128 0x3e
 2764 0039 0B       		.uleb128 0xb
 2765 003a 03       		.uleb128 0x3
 2766 003b 0E       		.uleb128 0xe
GAS LISTING /tmp/ccZP9VrL.s 			page 52


 2767 003c 00       		.byte	0
 2768 003d 00       		.byte	0
 2769 003e 05       		.uleb128 0x5
 2770 003f 39       		.uleb128 0x39
 2771 0040 01       		.byte	0x1
 2772 0041 03       		.uleb128 0x3
 2773 0042 08       		.uleb128 0x8
 2774 0043 3A       		.uleb128 0x3a
 2775 0044 0B       		.uleb128 0xb
 2776 0045 3B       		.uleb128 0x3b
 2777 0046 0B       		.uleb128 0xb
 2778 0047 01       		.uleb128 0x1
 2779 0048 13       		.uleb128 0x13
 2780 0049 00       		.byte	0
 2781 004a 00       		.byte	0
 2782 004b 06       		.uleb128 0x6
 2783 004c 39       		.uleb128 0x39
 2784 004d 00       		.byte	0
 2785 004e 03       		.uleb128 0x3
 2786 004f 0E       		.uleb128 0xe
 2787 0050 3A       		.uleb128 0x3a
 2788 0051 0B       		.uleb128 0xb
 2789 0052 3B       		.uleb128 0x3b
 2790 0053 0B       		.uleb128 0xb
 2791 0054 00       		.byte	0
 2792 0055 00       		.byte	0
 2793 0056 07       		.uleb128 0x7
 2794 0057 3A       		.uleb128 0x3a
 2795 0058 00       		.byte	0
 2796 0059 3A       		.uleb128 0x3a
 2797 005a 0B       		.uleb128 0xb
 2798 005b 3B       		.uleb128 0x3b
 2799 005c 0B       		.uleb128 0xb
 2800 005d 18       		.uleb128 0x18
 2801 005e 13       		.uleb128 0x13
 2802 005f 00       		.byte	0
 2803 0060 00       		.byte	0
 2804 0061 08       		.uleb128 0x8
 2805 0062 08       		.uleb128 0x8
 2806 0063 00       		.byte	0
 2807 0064 3A       		.uleb128 0x3a
 2808 0065 0B       		.uleb128 0xb
 2809 0066 3B       		.uleb128 0x3b
 2810 0067 0B       		.uleb128 0xb
 2811 0068 18       		.uleb128 0x18
 2812 0069 13       		.uleb128 0x13
 2813 006a 00       		.byte	0
 2814 006b 00       		.byte	0
 2815 006c 09       		.uleb128 0x9
 2816 006d 08       		.uleb128 0x8
 2817 006e 00       		.byte	0
 2818 006f 3A       		.uleb128 0x3a
 2819 0070 0B       		.uleb128 0xb
 2820 0071 3B       		.uleb128 0x3b
 2821 0072 05       		.uleb128 0x5
 2822 0073 18       		.uleb128 0x18
 2823 0074 13       		.uleb128 0x13
GAS LISTING /tmp/ccZP9VrL.s 			page 53


 2824 0075 00       		.byte	0
 2825 0076 00       		.byte	0
 2826 0077 0A       		.uleb128 0xa
 2827 0078 13       		.uleb128 0x13
 2828 0079 01       		.byte	0x1
 2829 007a 03       		.uleb128 0x3
 2830 007b 0E       		.uleb128 0xe
 2831 007c 0B       		.uleb128 0xb
 2832 007d 0B       		.uleb128 0xb
 2833 007e 3A       		.uleb128 0x3a
 2834 007f 0B       		.uleb128 0xb
 2835 0080 3B       		.uleb128 0x3b
 2836 0081 0B       		.uleb128 0xb
 2837 0082 01       		.uleb128 0x1
 2838 0083 13       		.uleb128 0x13
 2839 0084 00       		.byte	0
 2840 0085 00       		.byte	0
 2841 0086 0B       		.uleb128 0xb
 2842 0087 16       		.uleb128 0x16
 2843 0088 00       		.byte	0
 2844 0089 03       		.uleb128 0x3
 2845 008a 0E       		.uleb128 0xe
 2846 008b 3A       		.uleb128 0x3a
 2847 008c 0B       		.uleb128 0xb
 2848 008d 3B       		.uleb128 0x3b
 2849 008e 0B       		.uleb128 0xb
 2850 008f 49       		.uleb128 0x49
 2851 0090 13       		.uleb128 0x13
 2852 0091 00       		.byte	0
 2853 0092 00       		.byte	0
 2854 0093 0C       		.uleb128 0xc
 2855 0094 2E       		.uleb128 0x2e
 2856 0095 01       		.byte	0x1
 2857 0096 3F       		.uleb128 0x3f
 2858 0097 19       		.uleb128 0x19
 2859 0098 03       		.uleb128 0x3
 2860 0099 0E       		.uleb128 0xe
 2861 009a 3A       		.uleb128 0x3a
 2862 009b 0B       		.uleb128 0xb
 2863 009c 3B       		.uleb128 0x3b
 2864 009d 0B       		.uleb128 0xb
 2865 009e 6E       		.uleb128 0x6e
 2866 009f 0E       		.uleb128 0xe
 2867 00a0 3C       		.uleb128 0x3c
 2868 00a1 19       		.uleb128 0x19
 2869 00a2 01       		.uleb128 0x1
 2870 00a3 13       		.uleb128 0x13
 2871 00a4 00       		.byte	0
 2872 00a5 00       		.byte	0
 2873 00a6 0D       		.uleb128 0xd
 2874 00a7 05       		.uleb128 0x5
 2875 00a8 00       		.byte	0
 2876 00a9 49       		.uleb128 0x49
 2877 00aa 13       		.uleb128 0x13
 2878 00ab 00       		.byte	0
 2879 00ac 00       		.byte	0
 2880 00ad 0E       		.uleb128 0xe
GAS LISTING /tmp/ccZP9VrL.s 			page 54


 2881 00ae 26       		.uleb128 0x26
 2882 00af 00       		.byte	0
 2883 00b0 49       		.uleb128 0x49
 2884 00b1 13       		.uleb128 0x13
 2885 00b2 00       		.byte	0
 2886 00b3 00       		.byte	0
 2887 00b4 0F       		.uleb128 0xf
 2888 00b5 2E       		.uleb128 0x2e
 2889 00b6 01       		.byte	0x1
 2890 00b7 3F       		.uleb128 0x3f
 2891 00b8 19       		.uleb128 0x19
 2892 00b9 03       		.uleb128 0x3
 2893 00ba 08       		.uleb128 0x8
 2894 00bb 3A       		.uleb128 0x3a
 2895 00bc 0B       		.uleb128 0xb
 2896 00bd 3B       		.uleb128 0x3b
 2897 00be 0B       		.uleb128 0xb
 2898 00bf 6E       		.uleb128 0x6e
 2899 00c0 0E       		.uleb128 0xe
 2900 00c1 49       		.uleb128 0x49
 2901 00c2 13       		.uleb128 0x13
 2902 00c3 3C       		.uleb128 0x3c
 2903 00c4 19       		.uleb128 0x19
 2904 00c5 01       		.uleb128 0x1
 2905 00c6 13       		.uleb128 0x13
 2906 00c7 00       		.byte	0
 2907 00c8 00       		.byte	0
 2908 00c9 10       		.uleb128 0x10
 2909 00ca 2E       		.uleb128 0x2e
 2910 00cb 01       		.byte	0x1
 2911 00cc 3F       		.uleb128 0x3f
 2912 00cd 19       		.uleb128 0x19
 2913 00ce 03       		.uleb128 0x3
 2914 00cf 0E       		.uleb128 0xe
 2915 00d0 3A       		.uleb128 0x3a
 2916 00d1 0B       		.uleb128 0xb
 2917 00d2 3B       		.uleb128 0x3b
 2918 00d3 05       		.uleb128 0x5
 2919 00d4 6E       		.uleb128 0x6e
 2920 00d5 0E       		.uleb128 0xe
 2921 00d6 49       		.uleb128 0x49
 2922 00d7 13       		.uleb128 0x13
 2923 00d8 3C       		.uleb128 0x3c
 2924 00d9 19       		.uleb128 0x19
 2925 00da 01       		.uleb128 0x1
 2926 00db 13       		.uleb128 0x13
 2927 00dc 00       		.byte	0
 2928 00dd 00       		.byte	0
 2929 00de 11       		.uleb128 0x11
 2930 00df 2E       		.uleb128 0x2e
 2931 00e0 00       		.byte	0
 2932 00e1 3F       		.uleb128 0x3f
 2933 00e2 19       		.uleb128 0x19
 2934 00e3 03       		.uleb128 0x3
 2935 00e4 08       		.uleb128 0x8
 2936 00e5 3A       		.uleb128 0x3a
 2937 00e6 0B       		.uleb128 0xb
GAS LISTING /tmp/ccZP9VrL.s 			page 55


 2938 00e7 3B       		.uleb128 0x3b
 2939 00e8 05       		.uleb128 0x5
 2940 00e9 6E       		.uleb128 0x6e
 2941 00ea 0E       		.uleb128 0xe
 2942 00eb 49       		.uleb128 0x49
 2943 00ec 13       		.uleb128 0x13
 2944 00ed 3C       		.uleb128 0x3c
 2945 00ee 19       		.uleb128 0x19
 2946 00ef 00       		.byte	0
 2947 00f0 00       		.byte	0
 2948 00f1 12       		.uleb128 0x12
 2949 00f2 2E       		.uleb128 0x2e
 2950 00f3 01       		.byte	0x1
 2951 00f4 3F       		.uleb128 0x3f
 2952 00f5 19       		.uleb128 0x19
 2953 00f6 03       		.uleb128 0x3
 2954 00f7 0E       		.uleb128 0xe
 2955 00f8 3A       		.uleb128 0x3a
 2956 00f9 0B       		.uleb128 0xb
 2957 00fa 3B       		.uleb128 0x3b
 2958 00fb 05       		.uleb128 0x5
 2959 00fc 6E       		.uleb128 0x6e
 2960 00fd 0E       		.uleb128 0xe
 2961 00fe 49       		.uleb128 0x49
 2962 00ff 13       		.uleb128 0x13
 2963 0100 3C       		.uleb128 0x3c
 2964 0101 19       		.uleb128 0x19
 2965 0102 00       		.byte	0
 2966 0103 00       		.byte	0
 2967 0104 13       		.uleb128 0x13
 2968 0105 04       		.uleb128 0x4
 2969 0106 01       		.byte	0x1
 2970 0107 03       		.uleb128 0x3
 2971 0108 0E       		.uleb128 0xe
 2972 0109 0B       		.uleb128 0xb
 2973 010a 0B       		.uleb128 0xb
 2974 010b 49       		.uleb128 0x49
 2975 010c 13       		.uleb128 0x13
 2976 010d 3A       		.uleb128 0x3a
 2977 010e 0B       		.uleb128 0xb
 2978 010f 3B       		.uleb128 0x3b
 2979 0110 0B       		.uleb128 0xb
 2980 0111 01       		.uleb128 0x1
 2981 0112 13       		.uleb128 0x13
 2982 0113 00       		.byte	0
 2983 0114 00       		.byte	0
 2984 0115 14       		.uleb128 0x14
 2985 0116 28       		.uleb128 0x28
 2986 0117 00       		.byte	0
 2987 0118 03       		.uleb128 0x3
 2988 0119 0E       		.uleb128 0xe
 2989 011a 1C       		.uleb128 0x1c
 2990 011b 0B       		.uleb128 0xb
 2991 011c 00       		.byte	0
 2992 011d 00       		.byte	0
 2993 011e 15       		.uleb128 0x15
 2994 011f 28       		.uleb128 0x28
GAS LISTING /tmp/ccZP9VrL.s 			page 56


 2995 0120 00       		.byte	0
 2996 0121 03       		.uleb128 0x3
 2997 0122 0E       		.uleb128 0xe
 2998 0123 1C       		.uleb128 0x1c
 2999 0124 05       		.uleb128 0x5
 3000 0125 00       		.byte	0
 3001 0126 00       		.byte	0
 3002 0127 16       		.uleb128 0x16
 3003 0128 28       		.uleb128 0x28
 3004 0129 00       		.byte	0
 3005 012a 03       		.uleb128 0x3
 3006 012b 0E       		.uleb128 0xe
 3007 012c 1C       		.uleb128 0x1c
 3008 012d 06       		.uleb128 0x6
 3009 012e 00       		.byte	0
 3010 012f 00       		.byte	0
 3011 0130 17       		.uleb128 0x17
 3012 0131 28       		.uleb128 0x28
 3013 0132 00       		.byte	0
 3014 0133 03       		.uleb128 0x3
 3015 0134 0E       		.uleb128 0xe
 3016 0135 1C       		.uleb128 0x1c
 3017 0136 0D       		.uleb128 0xd
 3018 0137 00       		.byte	0
 3019 0138 00       		.byte	0
 3020 0139 18       		.uleb128 0x18
 3021 013a 02       		.uleb128 0x2
 3022 013b 01       		.byte	0x1
 3023 013c 03       		.uleb128 0x3
 3024 013d 0E       		.uleb128 0xe
 3025 013e 3C       		.uleb128 0x3c
 3026 013f 19       		.uleb128 0x19
 3027 0140 01       		.uleb128 0x1
 3028 0141 13       		.uleb128 0x13
 3029 0142 00       		.byte	0
 3030 0143 00       		.byte	0
 3031 0144 19       		.uleb128 0x19
 3032 0145 02       		.uleb128 0x2
 3033 0146 01       		.byte	0x1
 3034 0147 03       		.uleb128 0x3
 3035 0148 0E       		.uleb128 0xe
 3036 0149 0B       		.uleb128 0xb
 3037 014a 0B       		.uleb128 0xb
 3038 014b 3A       		.uleb128 0x3a
 3039 014c 0B       		.uleb128 0xb
 3040 014d 3B       		.uleb128 0x3b
 3041 014e 05       		.uleb128 0x5
 3042 014f 32       		.uleb128 0x32
 3043 0150 0B       		.uleb128 0xb
 3044 0151 01       		.uleb128 0x1
 3045 0152 13       		.uleb128 0x13
 3046 0153 00       		.byte	0
 3047 0154 00       		.byte	0
 3048 0155 1A       		.uleb128 0x1a
 3049 0156 0D       		.uleb128 0xd
 3050 0157 00       		.byte	0
 3051 0158 03       		.uleb128 0x3
GAS LISTING /tmp/ccZP9VrL.s 			page 57


 3052 0159 0E       		.uleb128 0xe
 3053 015a 3A       		.uleb128 0x3a
 3054 015b 0B       		.uleb128 0xb
 3055 015c 3B       		.uleb128 0x3b
 3056 015d 05       		.uleb128 0x5
 3057 015e 49       		.uleb128 0x49
 3058 015f 13       		.uleb128 0x13
 3059 0160 3F       		.uleb128 0x3f
 3060 0161 19       		.uleb128 0x19
 3061 0162 3C       		.uleb128 0x3c
 3062 0163 19       		.uleb128 0x19
 3063 0164 00       		.byte	0
 3064 0165 00       		.byte	0
 3065 0166 1B       		.uleb128 0x1b
 3066 0167 2E       		.uleb128 0x2e
 3067 0168 01       		.byte	0x1
 3068 0169 3F       		.uleb128 0x3f
 3069 016a 19       		.uleb128 0x19
 3070 016b 03       		.uleb128 0x3
 3071 016c 0E       		.uleb128 0xe
 3072 016d 3A       		.uleb128 0x3a
 3073 016e 0B       		.uleb128 0xb
 3074 016f 3B       		.uleb128 0x3b
 3075 0170 05       		.uleb128 0x5
 3076 0171 6E       		.uleb128 0x6e
 3077 0172 0E       		.uleb128 0xe
 3078 0173 32       		.uleb128 0x32
 3079 0174 0B       		.uleb128 0xb
 3080 0175 3C       		.uleb128 0x3c
 3081 0176 19       		.uleb128 0x19
 3082 0177 64       		.uleb128 0x64
 3083 0178 13       		.uleb128 0x13
 3084 0179 01       		.uleb128 0x1
 3085 017a 13       		.uleb128 0x13
 3086 017b 00       		.byte	0
 3087 017c 00       		.byte	0
 3088 017d 1C       		.uleb128 0x1c
 3089 017e 05       		.uleb128 0x5
 3090 017f 00       		.byte	0
 3091 0180 49       		.uleb128 0x49
 3092 0181 13       		.uleb128 0x13
 3093 0182 34       		.uleb128 0x34
 3094 0183 19       		.uleb128 0x19
 3095 0184 00       		.byte	0
 3096 0185 00       		.byte	0
 3097 0186 1D       		.uleb128 0x1d
 3098 0187 2E       		.uleb128 0x2e
 3099 0188 01       		.byte	0x1
 3100 0189 3F       		.uleb128 0x3f
 3101 018a 19       		.uleb128 0x19
 3102 018b 03       		.uleb128 0x3
 3103 018c 0E       		.uleb128 0xe
 3104 018d 3A       		.uleb128 0x3a
 3105 018e 0B       		.uleb128 0xb
 3106 018f 3B       		.uleb128 0x3b
 3107 0190 05       		.uleb128 0x5
 3108 0191 6E       		.uleb128 0x6e
GAS LISTING /tmp/ccZP9VrL.s 			page 58


 3109 0192 0E       		.uleb128 0xe
 3110 0193 32       		.uleb128 0x32
 3111 0194 0B       		.uleb128 0xb
 3112 0195 3C       		.uleb128 0x3c
 3113 0196 19       		.uleb128 0x19
 3114 0197 64       		.uleb128 0x64
 3115 0198 13       		.uleb128 0x13
 3116 0199 00       		.byte	0
 3117 019a 00       		.byte	0
 3118 019b 1E       		.uleb128 0x1e
 3119 019c 16       		.uleb128 0x16
 3120 019d 00       		.byte	0
 3121 019e 03       		.uleb128 0x3
 3122 019f 0E       		.uleb128 0xe
 3123 01a0 3A       		.uleb128 0x3a
 3124 01a1 0B       		.uleb128 0xb
 3125 01a2 3B       		.uleb128 0x3b
 3126 01a3 05       		.uleb128 0x5
 3127 01a4 49       		.uleb128 0x49
 3128 01a5 13       		.uleb128 0x13
 3129 01a6 32       		.uleb128 0x32
 3130 01a7 0B       		.uleb128 0xb
 3131 01a8 00       		.byte	0
 3132 01a9 00       		.byte	0
 3133 01aa 1F       		.uleb128 0x1f
 3134 01ab 0D       		.uleb128 0xd
 3135 01ac 00       		.byte	0
 3136 01ad 03       		.uleb128 0x3
 3137 01ae 0E       		.uleb128 0xe
 3138 01af 3A       		.uleb128 0x3a
 3139 01b0 0B       		.uleb128 0xb
 3140 01b1 3B       		.uleb128 0x3b
 3141 01b2 05       		.uleb128 0x5
 3142 01b3 49       		.uleb128 0x49
 3143 01b4 13       		.uleb128 0x13
 3144 01b5 3F       		.uleb128 0x3f
 3145 01b6 19       		.uleb128 0x19
 3146 01b7 32       		.uleb128 0x32
 3147 01b8 0B       		.uleb128 0xb
 3148 01b9 3C       		.uleb128 0x3c
 3149 01ba 19       		.uleb128 0x19
 3150 01bb 1C       		.uleb128 0x1c
 3151 01bc 0B       		.uleb128 0xb
 3152 01bd 00       		.byte	0
 3153 01be 00       		.byte	0
 3154 01bf 20       		.uleb128 0x20
 3155 01c0 0D       		.uleb128 0xd
 3156 01c1 00       		.byte	0
 3157 01c2 03       		.uleb128 0x3
 3158 01c3 08       		.uleb128 0x8
 3159 01c4 3A       		.uleb128 0x3a
 3160 01c5 0B       		.uleb128 0xb
 3161 01c6 3B       		.uleb128 0x3b
 3162 01c7 05       		.uleb128 0x5
 3163 01c8 49       		.uleb128 0x49
 3164 01c9 13       		.uleb128 0x13
 3165 01ca 3F       		.uleb128 0x3f
GAS LISTING /tmp/ccZP9VrL.s 			page 59


 3166 01cb 19       		.uleb128 0x19
 3167 01cc 32       		.uleb128 0x32
 3168 01cd 0B       		.uleb128 0xb
 3169 01ce 3C       		.uleb128 0x3c
 3170 01cf 19       		.uleb128 0x19
 3171 01d0 1C       		.uleb128 0x1c
 3172 01d1 0B       		.uleb128 0xb
 3173 01d2 00       		.byte	0
 3174 01d3 00       		.byte	0
 3175 01d4 21       		.uleb128 0x21
 3176 01d5 0D       		.uleb128 0xd
 3177 01d6 00       		.byte	0
 3178 01d7 03       		.uleb128 0x3
 3179 01d8 0E       		.uleb128 0xe
 3180 01d9 3A       		.uleb128 0x3a
 3181 01da 0B       		.uleb128 0xb
 3182 01db 3B       		.uleb128 0x3b
 3183 01dc 05       		.uleb128 0x5
 3184 01dd 49       		.uleb128 0x49
 3185 01de 13       		.uleb128 0x13
 3186 01df 3F       		.uleb128 0x3f
 3187 01e0 19       		.uleb128 0x19
 3188 01e1 32       		.uleb128 0x32
 3189 01e2 0B       		.uleb128 0xb
 3190 01e3 3C       		.uleb128 0x3c
 3191 01e4 19       		.uleb128 0x19
 3192 01e5 1C       		.uleb128 0x1c
 3193 01e6 05       		.uleb128 0x5
 3194 01e7 00       		.byte	0
 3195 01e8 00       		.byte	0
 3196 01e9 22       		.uleb128 0x22
 3197 01ea 2F       		.uleb128 0x2f
 3198 01eb 00       		.byte	0
 3199 01ec 03       		.uleb128 0x3
 3200 01ed 0E       		.uleb128 0xe
 3201 01ee 49       		.uleb128 0x49
 3202 01ef 13       		.uleb128 0x13
 3203 01f0 00       		.byte	0
 3204 01f1 00       		.byte	0
 3205 01f2 23       		.uleb128 0x23
 3206 01f3 2F       		.uleb128 0x2f
 3207 01f4 00       		.byte	0
 3208 01f5 03       		.uleb128 0x3
 3209 01f6 0E       		.uleb128 0xe
 3210 01f7 49       		.uleb128 0x49
 3211 01f8 13       		.uleb128 0x13
 3212 01f9 1E       		.uleb128 0x1e
 3213 01fa 19       		.uleb128 0x19
 3214 01fb 00       		.byte	0
 3215 01fc 00       		.byte	0
 3216 01fd 24       		.uleb128 0x24
 3217 01fe 34       		.uleb128 0x34
 3218 01ff 00       		.byte	0
 3219 0200 03       		.uleb128 0x3
 3220 0201 0E       		.uleb128 0xe
 3221 0202 3A       		.uleb128 0x3a
 3222 0203 0B       		.uleb128 0xb
GAS LISTING /tmp/ccZP9VrL.s 			page 60


 3223 0204 3B       		.uleb128 0x3b
 3224 0205 0B       		.uleb128 0xb
 3225 0206 6E       		.uleb128 0x6e
 3226 0207 0E       		.uleb128 0xe
 3227 0208 49       		.uleb128 0x49
 3228 0209 13       		.uleb128 0x13
 3229 020a 3F       		.uleb128 0x3f
 3230 020b 19       		.uleb128 0x19
 3231 020c 3C       		.uleb128 0x3c
 3232 020d 19       		.uleb128 0x19
 3233 020e 00       		.byte	0
 3234 020f 00       		.byte	0
 3235 0210 25       		.uleb128 0x25
 3236 0211 34       		.uleb128 0x34
 3237 0212 00       		.byte	0
 3238 0213 03       		.uleb128 0x3
 3239 0214 0E       		.uleb128 0xe
 3240 0215 3A       		.uleb128 0x3a
 3241 0216 0B       		.uleb128 0xb
 3242 0217 3B       		.uleb128 0x3b
 3243 0218 0B       		.uleb128 0xb
 3244 0219 49       		.uleb128 0x49
 3245 021a 13       		.uleb128 0x13
 3246 021b 3C       		.uleb128 0x3c
 3247 021c 19       		.uleb128 0x19
 3248 021d 00       		.byte	0
 3249 021e 00       		.byte	0
 3250 021f 26       		.uleb128 0x26
 3251 0220 39       		.uleb128 0x39
 3252 0221 01       		.byte	0x1
 3253 0222 03       		.uleb128 0x3
 3254 0223 0E       		.uleb128 0xe
 3255 0224 3A       		.uleb128 0x3a
 3256 0225 0B       		.uleb128 0xb
 3257 0226 3B       		.uleb128 0x3b
 3258 0227 0B       		.uleb128 0xb
 3259 0228 01       		.uleb128 0x1
 3260 0229 13       		.uleb128 0x13
 3261 022a 00       		.byte	0
 3262 022b 00       		.byte	0
 3263 022c 27       		.uleb128 0x27
 3264 022d 0D       		.uleb128 0xd
 3265 022e 00       		.byte	0
 3266 022f 03       		.uleb128 0x3
 3267 0230 0E       		.uleb128 0xe
 3268 0231 3A       		.uleb128 0x3a
 3269 0232 0B       		.uleb128 0xb
 3270 0233 3B       		.uleb128 0x3b
 3271 0234 0B       		.uleb128 0xb
 3272 0235 49       		.uleb128 0x49
 3273 0236 13       		.uleb128 0x13
 3274 0237 3F       		.uleb128 0x3f
 3275 0238 19       		.uleb128 0x19
 3276 0239 3C       		.uleb128 0x3c
 3277 023a 19       		.uleb128 0x19
 3278 023b 00       		.byte	0
 3279 023c 00       		.byte	0
GAS LISTING /tmp/ccZP9VrL.s 			page 61


 3280 023d 28       		.uleb128 0x28
 3281 023e 13       		.uleb128 0x13
 3282 023f 01       		.byte	0x1
 3283 0240 03       		.uleb128 0x3
 3284 0241 0E       		.uleb128 0xe
 3285 0242 0B       		.uleb128 0xb
 3286 0243 0B       		.uleb128 0xb
 3287 0244 3A       		.uleb128 0x3a
 3288 0245 0B       		.uleb128 0xb
 3289 0246 3B       		.uleb128 0x3b
 3290 0247 0B       		.uleb128 0xb
 3291 0248 00       		.byte	0
 3292 0249 00       		.byte	0
 3293 024a 29       		.uleb128 0x29
 3294 024b 13       		.uleb128 0x13
 3295 024c 00       		.byte	0
 3296 024d 03       		.uleb128 0x3
 3297 024e 0E       		.uleb128 0xe
 3298 024f 3C       		.uleb128 0x3c
 3299 0250 19       		.uleb128 0x19
 3300 0251 00       		.byte	0
 3301 0252 00       		.byte	0
 3302 0253 2A       		.uleb128 0x2a
 3303 0254 0D       		.uleb128 0xd
 3304 0255 00       		.byte	0
 3305 0256 03       		.uleb128 0x3
 3306 0257 0E       		.uleb128 0xe
 3307 0258 3A       		.uleb128 0x3a
 3308 0259 0B       		.uleb128 0xb
 3309 025a 3B       		.uleb128 0x3b
 3310 025b 0B       		.uleb128 0xb
 3311 025c 49       		.uleb128 0x49
 3312 025d 13       		.uleb128 0x13
 3313 025e 38       		.uleb128 0x38
 3314 025f 0B       		.uleb128 0xb
 3315 0260 00       		.byte	0
 3316 0261 00       		.byte	0
 3317 0262 2B       		.uleb128 0x2b
 3318 0263 0F       		.uleb128 0xf
 3319 0264 00       		.byte	0
 3320 0265 0B       		.uleb128 0xb
 3321 0266 0B       		.uleb128 0xb
 3322 0267 00       		.byte	0
 3323 0268 00       		.byte	0
 3324 0269 2C       		.uleb128 0x2c
 3325 026a 16       		.uleb128 0x16
 3326 026b 00       		.byte	0
 3327 026c 03       		.uleb128 0x3
 3328 026d 0E       		.uleb128 0xe
 3329 026e 3A       		.uleb128 0x3a
 3330 026f 0B       		.uleb128 0xb
 3331 0270 3B       		.uleb128 0x3b
 3332 0271 05       		.uleb128 0x5
 3333 0272 49       		.uleb128 0x49
 3334 0273 13       		.uleb128 0x13
 3335 0274 00       		.byte	0
 3336 0275 00       		.byte	0
GAS LISTING /tmp/ccZP9VrL.s 			page 62


 3337 0276 2D       		.uleb128 0x2d
 3338 0277 13       		.uleb128 0x13
 3339 0278 01       		.byte	0x1
 3340 0279 0B       		.uleb128 0xb
 3341 027a 0B       		.uleb128 0xb
 3342 027b 3A       		.uleb128 0x3a
 3343 027c 0B       		.uleb128 0xb
 3344 027d 3B       		.uleb128 0x3b
 3345 027e 0B       		.uleb128 0xb
 3346 027f 6E       		.uleb128 0x6e
 3347 0280 0E       		.uleb128 0xe
 3348 0281 01       		.uleb128 0x1
 3349 0282 13       		.uleb128 0x13
 3350 0283 00       		.byte	0
 3351 0284 00       		.byte	0
 3352 0285 2E       		.uleb128 0x2e
 3353 0286 17       		.uleb128 0x17
 3354 0287 01       		.byte	0x1
 3355 0288 0B       		.uleb128 0xb
 3356 0289 0B       		.uleb128 0xb
 3357 028a 3A       		.uleb128 0x3a
 3358 028b 0B       		.uleb128 0xb
 3359 028c 3B       		.uleb128 0x3b
 3360 028d 0B       		.uleb128 0xb
 3361 028e 01       		.uleb128 0x1
 3362 028f 13       		.uleb128 0x13
 3363 0290 00       		.byte	0
 3364 0291 00       		.byte	0
 3365 0292 2F       		.uleb128 0x2f
 3366 0293 0D       		.uleb128 0xd
 3367 0294 00       		.byte	0
 3368 0295 03       		.uleb128 0x3
 3369 0296 0E       		.uleb128 0xe
 3370 0297 3A       		.uleb128 0x3a
 3371 0298 0B       		.uleb128 0xb
 3372 0299 3B       		.uleb128 0x3b
 3373 029a 0B       		.uleb128 0xb
 3374 029b 49       		.uleb128 0x49
 3375 029c 13       		.uleb128 0x13
 3376 029d 00       		.byte	0
 3377 029e 00       		.byte	0
 3378 029f 30       		.uleb128 0x30
 3379 02a0 01       		.uleb128 0x1
 3380 02a1 01       		.byte	0x1
 3381 02a2 49       		.uleb128 0x49
 3382 02a3 13       		.uleb128 0x13
 3383 02a4 01       		.uleb128 0x1
 3384 02a5 13       		.uleb128 0x13
 3385 02a6 00       		.byte	0
 3386 02a7 00       		.byte	0
 3387 02a8 31       		.uleb128 0x31
 3388 02a9 21       		.uleb128 0x21
 3389 02aa 00       		.byte	0
 3390 02ab 49       		.uleb128 0x49
 3391 02ac 13       		.uleb128 0x13
 3392 02ad 2F       		.uleb128 0x2f
 3393 02ae 0B       		.uleb128 0xb
GAS LISTING /tmp/ccZP9VrL.s 			page 63


 3394 02af 00       		.byte	0
 3395 02b0 00       		.byte	0
 3396 02b1 32       		.uleb128 0x32
 3397 02b2 24       		.uleb128 0x24
 3398 02b3 00       		.byte	0
 3399 02b4 0B       		.uleb128 0xb
 3400 02b5 0B       		.uleb128 0xb
 3401 02b6 3E       		.uleb128 0x3e
 3402 02b7 0B       		.uleb128 0xb
 3403 02b8 03       		.uleb128 0x3
 3404 02b9 08       		.uleb128 0x8
 3405 02ba 00       		.byte	0
 3406 02bb 00       		.byte	0
 3407 02bc 33       		.uleb128 0x33
 3408 02bd 0F       		.uleb128 0xf
 3409 02be 00       		.byte	0
 3410 02bf 0B       		.uleb128 0xb
 3411 02c0 0B       		.uleb128 0xb
 3412 02c1 49       		.uleb128 0x49
 3413 02c2 13       		.uleb128 0x13
 3414 02c3 00       		.byte	0
 3415 02c4 00       		.byte	0
 3416 02c5 34       		.uleb128 0x34
 3417 02c6 2E       		.uleb128 0x2e
 3418 02c7 01       		.byte	0x1
 3419 02c8 3F       		.uleb128 0x3f
 3420 02c9 19       		.uleb128 0x19
 3421 02ca 03       		.uleb128 0x3
 3422 02cb 0E       		.uleb128 0xe
 3423 02cc 3A       		.uleb128 0x3a
 3424 02cd 0B       		.uleb128 0xb
 3425 02ce 3B       		.uleb128 0x3b
 3426 02cf 05       		.uleb128 0x5
 3427 02d0 49       		.uleb128 0x49
 3428 02d1 13       		.uleb128 0x13
 3429 02d2 3C       		.uleb128 0x3c
 3430 02d3 19       		.uleb128 0x19
 3431 02d4 01       		.uleb128 0x1
 3432 02d5 13       		.uleb128 0x13
 3433 02d6 00       		.byte	0
 3434 02d7 00       		.byte	0
 3435 02d8 35       		.uleb128 0x35
 3436 02d9 18       		.uleb128 0x18
 3437 02da 00       		.byte	0
 3438 02db 00       		.byte	0
 3439 02dc 00       		.byte	0
 3440 02dd 36       		.uleb128 0x36
 3441 02de 2E       		.uleb128 0x2e
 3442 02df 00       		.byte	0
 3443 02e0 3F       		.uleb128 0x3f
 3444 02e1 19       		.uleb128 0x19
 3445 02e2 03       		.uleb128 0x3
 3446 02e3 0E       		.uleb128 0xe
 3447 02e4 3A       		.uleb128 0x3a
 3448 02e5 0B       		.uleb128 0xb
 3449 02e6 3B       		.uleb128 0x3b
 3450 02e7 05       		.uleb128 0x5
GAS LISTING /tmp/ccZP9VrL.s 			page 64


 3451 02e8 49       		.uleb128 0x49
 3452 02e9 13       		.uleb128 0x13
 3453 02ea 3C       		.uleb128 0x3c
 3454 02eb 19       		.uleb128 0x19
 3455 02ec 00       		.byte	0
 3456 02ed 00       		.byte	0
 3457 02ee 37       		.uleb128 0x37
 3458 02ef 2E       		.uleb128 0x2e
 3459 02f0 01       		.byte	0x1
 3460 02f1 3F       		.uleb128 0x3f
 3461 02f2 19       		.uleb128 0x19
 3462 02f3 03       		.uleb128 0x3
 3463 02f4 0E       		.uleb128 0xe
 3464 02f5 3A       		.uleb128 0x3a
 3465 02f6 0B       		.uleb128 0xb
 3466 02f7 3B       		.uleb128 0x3b
 3467 02f8 0B       		.uleb128 0xb
 3468 02f9 49       		.uleb128 0x49
 3469 02fa 13       		.uleb128 0x13
 3470 02fb 3C       		.uleb128 0x3c
 3471 02fc 19       		.uleb128 0x19
 3472 02fd 01       		.uleb128 0x1
 3473 02fe 13       		.uleb128 0x13
 3474 02ff 00       		.byte	0
 3475 0300 00       		.byte	0
 3476 0301 38       		.uleb128 0x38
 3477 0302 13       		.uleb128 0x13
 3478 0303 01       		.byte	0x1
 3479 0304 03       		.uleb128 0x3
 3480 0305 08       		.uleb128 0x8
 3481 0306 0B       		.uleb128 0xb
 3482 0307 0B       		.uleb128 0xb
 3483 0308 3A       		.uleb128 0x3a
 3484 0309 0B       		.uleb128 0xb
 3485 030a 3B       		.uleb128 0x3b
 3486 030b 0B       		.uleb128 0xb
 3487 030c 01       		.uleb128 0x1
 3488 030d 13       		.uleb128 0x13
 3489 030e 00       		.byte	0
 3490 030f 00       		.byte	0
 3491 0310 39       		.uleb128 0x39
 3492 0311 2E       		.uleb128 0x2e
 3493 0312 01       		.byte	0x1
 3494 0313 3F       		.uleb128 0x3f
 3495 0314 19       		.uleb128 0x19
 3496 0315 03       		.uleb128 0x3
 3497 0316 0E       		.uleb128 0xe
 3498 0317 3A       		.uleb128 0x3a
 3499 0318 0B       		.uleb128 0xb
 3500 0319 3B       		.uleb128 0x3b
 3501 031a 0B       		.uleb128 0xb
 3502 031b 6E       		.uleb128 0x6e
 3503 031c 0E       		.uleb128 0xe
 3504 031d 49       		.uleb128 0x49
 3505 031e 13       		.uleb128 0x13
 3506 031f 3C       		.uleb128 0x3c
 3507 0320 19       		.uleb128 0x19
GAS LISTING /tmp/ccZP9VrL.s 			page 65


 3508 0321 01       		.uleb128 0x1
 3509 0322 13       		.uleb128 0x13
 3510 0323 00       		.byte	0
 3511 0324 00       		.byte	0
 3512 0325 3A       		.uleb128 0x3a
 3513 0326 10       		.uleb128 0x10
 3514 0327 00       		.byte	0
 3515 0328 0B       		.uleb128 0xb
 3516 0329 0B       		.uleb128 0xb
 3517 032a 49       		.uleb128 0x49
 3518 032b 13       		.uleb128 0x13
 3519 032c 00       		.byte	0
 3520 032d 00       		.byte	0
 3521 032e 3B       		.uleb128 0x3b
 3522 032f 2E       		.uleb128 0x2e
 3523 0330 00       		.byte	0
 3524 0331 3F       		.uleb128 0x3f
 3525 0332 19       		.uleb128 0x19
 3526 0333 03       		.uleb128 0x3
 3527 0334 0E       		.uleb128 0xe
 3528 0335 3A       		.uleb128 0x3a
 3529 0336 0B       		.uleb128 0xb
 3530 0337 3B       		.uleb128 0x3b
 3531 0338 0B       		.uleb128 0xb
 3532 0339 49       		.uleb128 0x49
 3533 033a 13       		.uleb128 0x13
 3534 033b 3C       		.uleb128 0x3c
 3535 033c 19       		.uleb128 0x19
 3536 033d 00       		.byte	0
 3537 033e 00       		.byte	0
 3538 033f 3C       		.uleb128 0x3c
 3539 0340 2E       		.uleb128 0x2e
 3540 0341 01       		.byte	0x1
 3541 0342 3F       		.uleb128 0x3f
 3542 0343 19       		.uleb128 0x19
 3543 0344 03       		.uleb128 0x3
 3544 0345 0E       		.uleb128 0xe
 3545 0346 3A       		.uleb128 0x3a
 3546 0347 0B       		.uleb128 0xb
 3547 0348 3B       		.uleb128 0x3b
 3548 0349 0B       		.uleb128 0xb
 3549 034a 49       		.uleb128 0x49
 3550 034b 13       		.uleb128 0x13
 3551 034c 11       		.uleb128 0x11
 3552 034d 01       		.uleb128 0x1
 3553 034e 12       		.uleb128 0x12
 3554 034f 07       		.uleb128 0x7
 3555 0350 40       		.uleb128 0x40
 3556 0351 18       		.uleb128 0x18
 3557 0352 9642     		.uleb128 0x2116
 3558 0354 19       		.uleb128 0x19
 3559 0355 01       		.uleb128 0x1
 3560 0356 13       		.uleb128 0x13
 3561 0357 00       		.byte	0
 3562 0358 00       		.byte	0
 3563 0359 3D       		.uleb128 0x3d
 3564 035a 34       		.uleb128 0x34
GAS LISTING /tmp/ccZP9VrL.s 			page 66


 3565 035b 00       		.byte	0
 3566 035c 03       		.uleb128 0x3
 3567 035d 08       		.uleb128 0x8
 3568 035e 3A       		.uleb128 0x3a
 3569 035f 0B       		.uleb128 0xb
 3570 0360 3B       		.uleb128 0x3b
 3571 0361 0B       		.uleb128 0xb
 3572 0362 49       		.uleb128 0x49
 3573 0363 13       		.uleb128 0x13
 3574 0364 02       		.uleb128 0x2
 3575 0365 18       		.uleb128 0x18
 3576 0366 00       		.byte	0
 3577 0367 00       		.byte	0
 3578 0368 3E       		.uleb128 0x3e
 3579 0369 2E       		.uleb128 0x2e
 3580 036a 01       		.byte	0x1
 3581 036b 03       		.uleb128 0x3
 3582 036c 0E       		.uleb128 0xe
 3583 036d 34       		.uleb128 0x34
 3584 036e 19       		.uleb128 0x19
 3585 036f 11       		.uleb128 0x11
 3586 0370 01       		.uleb128 0x1
 3587 0371 12       		.uleb128 0x12
 3588 0372 07       		.uleb128 0x7
 3589 0373 40       		.uleb128 0x40
 3590 0374 18       		.uleb128 0x18
 3591 0375 9642     		.uleb128 0x2116
 3592 0377 19       		.uleb128 0x19
 3593 0378 01       		.uleb128 0x1
 3594 0379 13       		.uleb128 0x13
 3595 037a 00       		.byte	0
 3596 037b 00       		.byte	0
 3597 037c 3F       		.uleb128 0x3f
 3598 037d 05       		.uleb128 0x5
 3599 037e 00       		.byte	0
 3600 037f 03       		.uleb128 0x3
 3601 0380 0E       		.uleb128 0xe
 3602 0381 3A       		.uleb128 0x3a
 3603 0382 0B       		.uleb128 0xb
 3604 0383 3B       		.uleb128 0x3b
 3605 0384 0B       		.uleb128 0xb
 3606 0385 49       		.uleb128 0x49
 3607 0386 13       		.uleb128 0x13
 3608 0387 02       		.uleb128 0x2
 3609 0388 18       		.uleb128 0x18
 3610 0389 00       		.byte	0
 3611 038a 00       		.byte	0
 3612 038b 40       		.uleb128 0x40
 3613 038c 2E       		.uleb128 0x2e
 3614 038d 00       		.byte	0
 3615 038e 03       		.uleb128 0x3
 3616 038f 0E       		.uleb128 0xe
 3617 0390 34       		.uleb128 0x34
 3618 0391 19       		.uleb128 0x19
 3619 0392 11       		.uleb128 0x11
 3620 0393 01       		.uleb128 0x1
 3621 0394 12       		.uleb128 0x12
GAS LISTING /tmp/ccZP9VrL.s 			page 67


 3622 0395 07       		.uleb128 0x7
 3623 0396 40       		.uleb128 0x40
 3624 0397 18       		.uleb128 0x18
 3625 0398 9642     		.uleb128 0x2116
 3626 039a 19       		.uleb128 0x19
 3627 039b 00       		.byte	0
 3628 039c 00       		.byte	0
 3629 039d 41       		.uleb128 0x41
 3630 039e 34       		.uleb128 0x34
 3631 039f 00       		.byte	0
 3632 03a0 03       		.uleb128 0x3
 3633 03a1 0E       		.uleb128 0xe
 3634 03a2 49       		.uleb128 0x49
 3635 03a3 13       		.uleb128 0x13
 3636 03a4 3F       		.uleb128 0x3f
 3637 03a5 19       		.uleb128 0x19
 3638 03a6 34       		.uleb128 0x34
 3639 03a7 19       		.uleb128 0x19
 3640 03a8 3C       		.uleb128 0x3c
 3641 03a9 19       		.uleb128 0x19
 3642 03aa 00       		.byte	0
 3643 03ab 00       		.byte	0
 3644 03ac 42       		.uleb128 0x42
 3645 03ad 34       		.uleb128 0x34
 3646 03ae 00       		.byte	0
 3647 03af 47       		.uleb128 0x47
 3648 03b0 13       		.uleb128 0x13
 3649 03b1 02       		.uleb128 0x2
 3650 03b2 18       		.uleb128 0x18
 3651 03b3 00       		.byte	0
 3652 03b4 00       		.byte	0
 3653 03b5 43       		.uleb128 0x43
 3654 03b6 34       		.uleb128 0x34
 3655 03b7 00       		.byte	0
 3656 03b8 47       		.uleb128 0x47
 3657 03b9 13       		.uleb128 0x13
 3658 03ba 6E       		.uleb128 0x6e
 3659 03bb 0E       		.uleb128 0xe
 3660 03bc 1C       		.uleb128 0x1c
 3661 03bd 0D       		.uleb128 0xd
 3662 03be 00       		.byte	0
 3663 03bf 00       		.byte	0
 3664 03c0 44       		.uleb128 0x44
 3665 03c1 34       		.uleb128 0x34
 3666 03c2 00       		.byte	0
 3667 03c3 47       		.uleb128 0x47
 3668 03c4 13       		.uleb128 0x13
 3669 03c5 6E       		.uleb128 0x6e
 3670 03c6 0E       		.uleb128 0xe
 3671 03c7 1C       		.uleb128 0x1c
 3672 03c8 06       		.uleb128 0x6
 3673 03c9 00       		.byte	0
 3674 03ca 00       		.byte	0
 3675 03cb 45       		.uleb128 0x45
 3676 03cc 34       		.uleb128 0x34
 3677 03cd 00       		.byte	0
 3678 03ce 47       		.uleb128 0x47
GAS LISTING /tmp/ccZP9VrL.s 			page 68


 3679 03cf 13       		.uleb128 0x13
 3680 03d0 6E       		.uleb128 0x6e
 3681 03d1 0E       		.uleb128 0xe
 3682 03d2 1C       		.uleb128 0x1c
 3683 03d3 0B       		.uleb128 0xb
 3684 03d4 00       		.byte	0
 3685 03d5 00       		.byte	0
 3686 03d6 46       		.uleb128 0x46
 3687 03d7 34       		.uleb128 0x34
 3688 03d8 00       		.byte	0
 3689 03d9 47       		.uleb128 0x47
 3690 03da 13       		.uleb128 0x13
 3691 03db 6E       		.uleb128 0x6e
 3692 03dc 0E       		.uleb128 0xe
 3693 03dd 1C       		.uleb128 0x1c
 3694 03de 05       		.uleb128 0x5
 3695 03df 00       		.byte	0
 3696 03e0 00       		.byte	0
 3697 03e1 47       		.uleb128 0x47
 3698 03e2 34       		.uleb128 0x34
 3699 03e3 00       		.byte	0
 3700 03e4 47       		.uleb128 0x47
 3701 03e5 13       		.uleb128 0x13
 3702 03e6 6E       		.uleb128 0x6e
 3703 03e7 0E       		.uleb128 0xe
 3704 03e8 1C       		.uleb128 0x1c
 3705 03e9 07       		.uleb128 0x7
 3706 03ea 00       		.byte	0
 3707 03eb 00       		.byte	0
 3708 03ec 00       		.byte	0
 3709              		.section	.debug_aranges,"",@progbits
 3710 0000 2C000000 		.long	0x2c
 3711 0004 0200     		.value	0x2
 3712 0006 00000000 		.long	.Ldebug_info0
 3713 000a 08       		.byte	0x8
 3714 000b 00       		.byte	0
 3715 000c 0000     		.value	0
 3716 000e 0000     		.value	0
 3717 0010 00000000 		.quad	.Ltext0
 3717      00000000 
 3718 0018 D9000000 		.quad	.Letext0-.Ltext0
 3718      00000000 
 3719 0020 00000000 		.quad	0
 3719      00000000 
 3720 0028 00000000 		.quad	0
 3720      00000000 
 3721              		.section	.debug_line,"",@progbits
 3722              	.Ldebug_line0:
 3723 0000 4A020000 		.section	.debug_str,"MS",@progbits,1
 3723      02001402 
 3723      00000101 
 3723      FB0E0D00 
 3723      01010101 
 3724              	.LASF70:
 3725 0000 5F535F65 		.string	"_S_end"
 3725      6E6400
 3726              	.LASF25:
GAS LISTING /tmp/ccZP9VrL.s 			page 69


 3727 0007 73697A65 		.string	"size_t"
 3727      5F7400
 3728              	.LASF119:
 3729 000e 73697A65 		.string	"sizetype"
 3729      74797065 
 3729      00
 3730              	.LASF170:
 3731 0017 746D5F68 		.string	"tm_hour"
 3731      6F757200 
 3732              	.LASF110:
 3733 001f 5F5F6973 		.string	"__is_signed"
 3733      5F736967 
 3733      6E656400 
 3734              	.LASF58:
 3735 002b 5F535F69 		.string	"_S_ios_openmode_min"
 3735      6F735F6F 
 3735      70656E6D 
 3735      6F64655F 
 3735      6D696E00 
 3736              	.LASF107:
 3737 003f 5F5F6E75 		.string	"__numeric_traits_integer<int>"
 3737      6D657269 
 3737      635F7472 
 3737      61697473 
 3737      5F696E74 
 3738              	.LASF257:
 3739 005d 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIiE5__maxE"
 3739      5F5F676E 
 3739      755F6378 
 3739      7832345F 
 3739      5F6E756D 
 3740              	.LASF76:
 3741 008f 626F6F6C 		.string	"boolalpha"
 3741      616C7068 
 3741      6100
 3742              	.LASF81:
 3743 0099 73636965 		.string	"scientific"
 3743      6E746966 
 3743      696300
 3744              	.LASF109:
 3745 00a4 5F5F6D61 		.string	"__max"
 3745      7800
 3746              	.LASF166:
 3747 00aa 77637363 		.string	"wcscspn"
 3747      73706E00 
 3748              	.LASF243:
 3749 00b2 6C6F6361 		.string	"localeconv"
 3749      6C65636F 
 3749      6E7600
 3750              	.LASF47:
 3751 00bd 5F535F69 		.string	"_S_ios_fmtflags_min"
 3751      6F735F66 
 3751      6D74666C 
 3751      6167735F 
 3751      6D696E00 
 3752              	.LASF228:
 3753 00d1 66726163 		.string	"frac_digits"
GAS LISTING /tmp/ccZP9VrL.s 			page 70


 3753      5F646967 
 3753      69747300 
 3754              	.LASF65:
 3755 00dd 5F535F69 		.string	"_S_ios_iostate_max"
 3755      6F735F69 
 3755      6F737461 
 3755      74655F6D 
 3755      617800
 3756              	.LASF220:
 3757 00f0 696E745F 		.string	"int_curr_symbol"
 3757      63757272 
 3757      5F73796D 
 3757      626F6C00 
 3758              	.LASF96:
 3759 0100 676F6F64 		.string	"goodbit"
 3759      62697400 
 3760              	.LASF200:
 3761 0108 77637363 		.string	"wcschr"
 3761      687200
 3762              	.LASF27:
 3763 010f 5F535F62 		.string	"_S_boolalpha"
 3763      6F6F6C61 
 3763      6C706861 
 3763      00
 3764              	.LASF61:
 3765 011c 5F535F62 		.string	"_S_badbit"
 3765      61646269 
 3765      7400
 3766              	.LASF95:
 3767 0126 6661696C 		.string	"failbit"
 3767      62697400 
 3768              	.LASF231:
 3769 012e 6E5F6373 		.string	"n_cs_precedes"
 3769      5F707265 
 3769      63656465 
 3769      7300
 3770              	.LASF147:
 3771 013c 6D627274 		.string	"mbrtowc"
 3771      6F776300 
 3772              	.LASF192:
 3773 0144 77637378 		.string	"wcsxfrm"
 3773      66726D00 
 3774              	.LASF227:
 3775 014c 696E745F 		.string	"int_frac_digits"
 3775      66726163 
 3775      5F646967 
 3775      69747300 
 3776              	.LASF68:
 3777 015c 5F535F62 		.string	"_S_beg"
 3777      656700
 3778              	.LASF164:
 3779 0163 77637363 		.string	"wcscoll"
 3779      6F6C6C00 
 3780              	.LASF85:
 3781 016b 736B6970 		.string	"skipws"
 3781      777300
 3782              	.LASF128:
GAS LISTING /tmp/ccZP9VrL.s 			page 71


 3783 0172 5F5F7763 		.string	"__wch"
 3783      6800
 3784              	.LASF13:
 3785 0178 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4moveEPcPKcm"
 3785      74313163 
 3785      6861725F 
 3785      74726169 
 3785      74734963 
 3786              	.LASF87:
 3787 019a 75707065 		.string	"uppercase"
 3787      72636173 
 3787      6500
 3788              	.LASF43:
 3789 01a4 5F535F62 		.string	"_S_basefield"
 3789      61736566 
 3789      69656C64 
 3789      00
 3790              	.LASF267:
 3791 01b1 636F6D70 		.string	"complex"
 3791      6C657800 
 3792              	.LASF21:
 3793 01b9 5F5A4E53 		.string	"_ZNSt11char_traitsIcE11to_int_typeERKc"
 3793      74313163 
 3793      6861725F 
 3793      74726169 
 3793      74734963 
 3794              	.LASF222:
 3795 01e0 6D6F6E5F 		.string	"mon_decimal_point"
 3795      64656369 
 3795      6D616C5F 
 3795      706F696E 
 3795      7400
 3796              	.LASF190:
 3797 01f2 6C6F6E67 		.string	"long int"
 3797      20696E74 
 3797      00
 3798              	.LASF115:
 3799 01fb 5F5F6E75 		.string	"__numeric_traits_integer<char>"
 3799      6D657269 
 3799      635F7472 
 3799      61697473 
 3799      5F696E74 
 3800              	.LASF159:
 3801 021a 76777072 		.string	"vwprintf"
 3801      696E7466 
 3801      00
 3802              	.LASF49:
 3803 0223 5F496F73 		.string	"_Ios_Openmode"
 3803      5F4F7065 
 3803      6E6D6F64 
 3803      6500
 3804              	.LASF3:
 3805 0231 696E745F 		.string	"int_type"
 3805      74797065 
 3805      00
 3806              	.LASF279:
 3807 023a 6D61696E 		.string	"main"
GAS LISTING /tmp/ccZP9VrL.s 			page 72


 3807      00
 3808              	.LASF237:
 3809 023f 696E745F 		.string	"int_n_cs_precedes"
 3809      6E5F6373 
 3809      5F707265 
 3809      63656465 
 3809      7300
 3810              	.LASF249:
 3811 0251 746F7763 		.string	"towctrans"
 3811      7472616E 
 3811      7300
 3812              	.LASF14:
 3813 025b 636F7079 		.string	"copy"
 3813      00
 3814              	.LASF5:
 3815 0260 5F5A4E53 		.string	"_ZNSt11char_traitsIcE2ltERKcS2_"
 3815      74313163 
 3815      6861725F 
 3815      74726169 
 3815      74734963 
 3816              	.LASF252:
 3817 0280 5F5F696F 		.string	"__ioinit"
 3817      696E6974 
 3817      00
 3818              	.LASF73:
 3819 0289 5F535F73 		.string	"_S_synced_with_stdio"
 3819      796E6365 
 3819      645F7769 
 3819      74685F73 
 3819      7464696F 
 3820              	.LASF113:
 3821 029e 5F56616C 		.string	"_Value"
 3821      756500
 3822              	.LASF62:
 3823 02a5 5F535F65 		.string	"_S_eofbit"
 3823      6F666269 
 3823      7400
 3824              	.LASF175:
 3825 02af 746D5F79 		.string	"tm_yday"
 3825      64617900 
 3826              	.LASF212:
 3827 02b7 7369676E 		.string	"signed char"
 3827      65642063 
 3827      68617200 
 3828              	.LASF277:
 3829 02c3 5F494F5F 		.string	"_IO_FILE"
 3829      46494C45 
 3829      00
 3830              	.LASF102:
 3831 02cc 62617369 		.string	"basic_ostream<char, std::char_traits<char> >"
 3831      635F6F73 
 3831      74726561 
 3831      6D3C6368 
 3831      61722C20 
 3832              	.LASF131:
 3833 02f9 5F5F7661 		.string	"__value"
 3833      6C756500 
GAS LISTING /tmp/ccZP9VrL.s 			page 73


 3834              	.LASF246:
 3835 0301 77637479 		.string	"wctype_t"
 3835      70655F74 
 3835      00
 3836              	.LASF137:
 3837 030a 66676574 		.string	"fgetwc"
 3837      776300
 3838              	.LASF242:
 3839 0311 67657477 		.string	"getwchar"
 3839      63686172 
 3839      00
 3840              	.LASF138:
 3841 031a 66676574 		.string	"fgetws"
 3841      777300
 3842              	.LASF34:
 3843 0321 5F535F72 		.string	"_S_right"
 3843      69676874 
 3843      00
 3844              	.LASF2:
 3845 032a 63686172 		.string	"char_type"
 3845      5F747970 
 3845      6500
 3846              	.LASF211:
 3847 0334 756E7369 		.string	"unsigned char"
 3847      676E6564 
 3847      20636861 
 3847      7200
 3848              	.LASF232:
 3849 0342 6E5F7365 		.string	"n_sep_by_space"
 3849      705F6279 
 3849      5F737061 
 3849      636500
 3850              	.LASF204:
 3851 0351 776D656D 		.string	"wmemchr"
 3851      63687200 
 3852              	.LASF60:
 3853 0359 5F535F67 		.string	"_S_goodbit"
 3853      6F6F6462 
 3853      697400
 3854              	.LASF266:
 3855 0364 2F686F6D 		.string	"/home/vinu/cs4230/par-lang/cpp/discover_modern_c++/classes"
 3855      652F7669 
 3855      6E752F63 
 3855      73343233 
 3855      302F7061 
 3856              	.LASF52:
 3857 039f 5F535F62 		.string	"_S_bin"
 3857      696E00
 3858              	.LASF163:
 3859 03a6 77637363 		.string	"wcscmp"
 3859      6D7000
 3860              	.LASF24:
 3861 03ad 6E6F745F 		.string	"not_eof"
 3861      656F6600 
 3862              	.LASF152:
 3863 03b5 73777072 		.string	"swprintf"
 3863      696E7466 
GAS LISTING /tmp/ccZP9VrL.s 			page 74


 3863      00
 3864              	.LASF201:
 3865 03be 77637370 		.string	"wcspbrk"
 3865      62726B00 
 3866              	.LASF54:
 3867 03c6 5F535F6F 		.string	"_S_out"
 3867      757400
 3868              	.LASF132:
 3869 03cd 63686172 		.string	"char"
 3869      00
 3870              	.LASF50:
 3871 03d2 5F535F61 		.string	"_S_app"
 3871      707000
 3872              	.LASF134:
 3873 03d9 6D627374 		.string	"mbstate_t"
 3873      6174655F 
 3873      7400
 3874              	.LASF251:
 3875 03e3 77637479 		.string	"wctype"
 3875      706500
 3876              	.LASF97:
 3877 03ea 6F70656E 		.string	"openmode"
 3877      6D6F6465 
 3877      00
 3878              	.LASF181:
 3879 03f3 7763736E 		.string	"wcsncmp"
 3879      636D7000 
 3880              	.LASF240:
 3881 03fb 696E745F 		.string	"int_n_sign_posn"
 3881      6E5F7369 
 3881      676E5F70 
 3881      6F736E00 
 3882              	.LASF234:
 3883 040b 6E5F7369 		.string	"n_sign_posn"
 3883      676E5F70 
 3883      6F736E00 
 3884              	.LASF75:
 3885 0417 5F5A4E53 		.string	"_ZNSt8ios_base4InitD4Ev"
 3885      7438696F 
 3885      735F6261 
 3885      73653449 
 3885      6E697444 
 3886              	.LASF196:
 3887 042f 776D656D 		.string	"wmemmove"
 3887      6D6F7665 
 3887      00
 3888              	.LASF108:
 3889 0438 5F5F6D69 		.string	"__min"
 3889      6E00
 3890              	.LASF136:
 3891 043e 62746F77 		.string	"btowc"
 3891      6300
 3892              	.LASF199:
 3893 0444 77736361 		.string	"wscanf"
 3893      6E6600
 3894              	.LASF223:
 3895 044b 6D6F6E5F 		.string	"mon_thousands_sep"
GAS LISTING /tmp/ccZP9VrL.s 			page 75


 3895      74686F75 
 3895      73616E64 
 3895      735F7365 
 3895      7000
 3896              	.LASF154:
 3897 045d 756E6765 		.string	"ungetwc"
 3897      74776300 
 3898              	.LASF122:
 3899 0465 66705F6F 		.string	"fp_offset"
 3899      66667365 
 3899      7400
 3900              	.LASF26:
 3901 046f 70747264 		.string	"ptrdiff_t"
 3901      6966665F 
 3901      7400
 3902              	.LASF256:
 3903 0479 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIiE5__minE"
 3903      5F5F676E 
 3903      755F6378 
 3903      7832345F 
 3903      5F6E756D 
 3904              	.LASF247:
 3905 04ab 77637472 		.string	"wctrans_t"
 3905      616E735F 
 3905      7400
 3906              	.LASF146:
 3907 04b5 6D62726C 		.string	"mbrlen"
 3907      656E00
 3908              	.LASF226:
 3909 04bc 6E656761 		.string	"negative_sign"
 3909      74697665 
 3909      5F736967 
 3909      6E00
 3910              	.LASF30:
 3911 04ca 5F535F68 		.string	"_S_hex"
 3911      657800
 3912              	.LASF235:
 3913 04d1 696E745F 		.string	"int_p_cs_precedes"
 3913      705F6373 
 3913      5F707265 
 3913      63656465 
 3913      7300
 3914              	.LASF143:
 3915 04e3 66777072 		.string	"fwprintf"
 3915      696E7466 
 3915      00
 3916              	.LASF66:
 3917 04ec 5F535F69 		.string	"_S_ios_iostate_min"
 3917      6F735F69 
 3917      6F737461 
 3917      74655F6D 
 3917      696E00
 3918              	.LASF274:
 3919 04ff 636F7574 		.string	"cout"
 3919      00
 3920              	.LASF209:
 3921 0504 77637374 		.string	"wcstoull"
GAS LISTING /tmp/ccZP9VrL.s 			page 76


 3921      6F756C6C 
 3921      00
 3922              	.LASF31:
 3923 050d 5F535F69 		.string	"_S_internal"
 3923      6E746572 
 3923      6E616C00 
 3924              	.LASF6:
 3925 0519 636F6D70 		.string	"compare"
 3925      61726500 
 3926              	.LASF171:
 3927 0521 746D5F6D 		.string	"tm_mday"
 3927      64617900 
 3928              	.LASF89:
 3929 0529 62617365 		.string	"basefield"
 3929      6669656C 
 3929      6400
 3930              	.LASF165:
 3931 0533 77637363 		.string	"wcscpy"
 3931      707900
 3932              	.LASF112:
 3933 053a 5F436861 		.string	"_CharT"
 3933      725400
 3934              	.LASF77:
 3935 0541 66697865 		.string	"fixed"
 3935      6400
 3936              	.LASF157:
 3937 0547 76737770 		.string	"vswprintf"
 3937      72696E74 
 3937      6600
 3938              	.LASF197:
 3939 0551 776D656D 		.string	"wmemset"
 3939      73657400 
 3940              	.LASF100:
 3941 0559 7365656B 		.string	"seekdir"
 3941      64697200 
 3942              	.LASF142:
 3943 0561 66776964 		.string	"fwide"
 3943      6500
 3944              	.LASF79:
 3945 0567 6C656674 		.string	"left"
 3945      00
 3946              	.LASF168:
 3947 056c 746D5F73 		.string	"tm_sec"
 3947      656300
 3948              	.LASF176:
 3949 0573 746D5F69 		.string	"tm_isdst"
 3949      73647374 
 3949      00
 3950              	.LASF182:
 3951 057c 7763736E 		.string	"wcsncpy"
 3951      63707900 
 3952              	.LASF151:
 3953 0584 70757477 		.string	"putwchar"
 3953      63686172 
 3953      00
 3954              	.LASF194:
 3955 058d 776D656D 		.string	"wmemcmp"
GAS LISTING /tmp/ccZP9VrL.s 			page 77


 3955      636D7000 
 3956              	.LASF51:
 3957 0595 5F535F61 		.string	"_S_ate"
 3957      746500
 3958              	.LASF15:
 3959 059c 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4copyEPcPKcm"
 3959      74313163 
 3959      6861725F 
 3959      74726169 
 3959      74734963 
 3960              	.LASF10:
 3961 05be 66696E64 		.string	"find"
 3961      00
 3962              	.LASF29:
 3963 05c3 5F535F66 		.string	"_S_fixed"
 3963      69786564 
 3963      00
 3964              	.LASF265:
 3965 05cc 636F6D70 		.string	"complex.c"
 3965      6C65782E 
 3965      6300
 3966              	.LASF238:
 3967 05d6 696E745F 		.string	"int_n_sep_by_space"
 3967      6E5F7365 
 3967      705F6279 
 3967      5F737061 
 3967      636500
 3968              	.LASF254:
 3969 05e9 5F5F7072 		.string	"__priority"
 3969      696F7269 
 3969      747900
 3970              	.LASF12:
 3971 05f4 6D6F7665 		.string	"move"
 3971      00
 3972              	.LASF36:
 3973 05f9 5F535F73 		.string	"_S_showbase"
 3973      686F7762 
 3973      61736500 
 3974              	.LASF53:
 3975 0605 5F535F69 		.string	"_S_in"
 3975      6E00
 3976              	.LASF214:
 3977 060b 5F5F676E 		.string	"__gnu_debug"
 3977      755F6465 
 3977      62756700 
 3978              	.LASF155:
 3979 0617 76667770 		.string	"vfwprintf"
 3979      72696E74 
 3979      6600
 3980              	.LASF158:
 3981 0621 76737773 		.string	"vswscanf"
 3981      63616E66 
 3981      00
 3982              	.LASF230:
 3983 062a 705F7365 		.string	"p_sep_by_space"
 3983      705F6279 
 3983      5F737061 
GAS LISTING /tmp/ccZP9VrL.s 			page 78


 3983      636500
 3984              	.LASF22:
 3985 0639 65715F69 		.string	"eq_int_type"
 3985      6E745F74 
 3985      79706500 
 3986              	.LASF72:
 3987 0645 5F535F72 		.string	"_S_refcount"
 3987      6566636F 
 3987      756E7400 
 3988              	.LASF19:
 3989 0651 5F5A4E53 		.string	"_ZNSt11char_traitsIcE12to_char_typeERKi"
 3989      74313163 
 3989      6861725F 
 3989      74726169 
 3989      74734963 
 3990              	.LASF55:
 3991 0679 5F535F74 		.string	"_S_trunc"
 3991      72756E63 
 3991      00
 3992              	.LASF253:
 3993 0682 5F5F696E 		.string	"__initialize_p"
 3993      69746961 
 3993      6C697A65 
 3993      5F7000
 3994              	.LASF80:
 3995 0691 72696768 		.string	"right"
 3995      7400
 3996              	.LASF38:
 3997 0697 5F535F73 		.string	"_S_showpos"
 3997      686F7770 
 3997      6F7300
 3998              	.LASF281:
 3999 06a2 5F474C4F 		.string	"_GLOBAL__sub_I_main"
 3999      42414C5F 
 3999      5F737562 
 3999      5F495F6D 
 3999      61696E00 
 4000              	.LASF133:
 4001 06b6 5F5F6D62 		.string	"__mbstate_t"
 4001      73746174 
 4001      655F7400 
 4002              	.LASF195:
 4003 06c2 776D656D 		.string	"wmemcpy"
 4003      63707900 
 4004              	.LASF172:
 4005 06ca 746D5F6D 		.string	"tm_mon"
 4005      6F6E00
 4006              	.LASF28:
 4007 06d1 5F535F64 		.string	"_S_dec"
 4007      656300
 4008              	.LASF48:
 4009 06d8 5F496F73 		.string	"_Ios_Fmtflags"
 4009      5F466D74 
 4009      666C6167 
 4009      7300
 4010              	.LASF189:
 4011 06e6 77637374 		.string	"wcstol"
GAS LISTING /tmp/ccZP9VrL.s 			page 79


 4011      6F6C00
 4012              	.LASF118:
 4013 06ed 646F7562 		.string	"double"
 4013      6C6500
 4014              	.LASF11:
 4015 06f4 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4findEPKcmRS1_"
 4015      74313163 
 4015      6861725F 
 4015      74726169 
 4015      74734963 
 4016              	.LASF260:
 4017 0718 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIsE5__minE"
 4017      5F5F676E 
 4017      755F6378 
 4017      7832345F 
 4017      5F6E756D 
 4018              	.LASF193:
 4019 074a 7763746F 		.string	"wctob"
 4019      6200
 4020              	.LASF37:
 4021 0750 5F535F73 		.string	"_S_showpoint"
 4021      686F7770 
 4021      6F696E74 
 4021      00
 4022              	.LASF39:
 4023 075d 5F535F73 		.string	"_S_skipws"
 4023      6B697077 
 4023      7300
 4024              	.LASF121:
 4025 0767 67705F6F 		.string	"gp_offset"
 4025      66667365 
 4025      7400
 4026              	.LASF42:
 4027 0771 5F535F61 		.string	"_S_adjustfield"
 4027      646A7573 
 4027      74666965 
 4027      6C6400
 4028              	.LASF258:
 4029 0780 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerImE8__digitsE"
 4029      5F5F676E 
 4029      755F6378 
 4029      7832345F 
 4029      5F6E756D 
 4030              	.LASF187:
 4031 07b5 666C6F61 		.string	"float"
 4031      7400
 4032              	.LASF78:
 4033 07bb 696E7465 		.string	"internal"
 4033      726E616C 
 4033      00
 4034              	.LASF275:
 4035 07c4 5F5A5374 		.string	"_ZSt4cout"
 4035      34636F75 
 4035      7400
 4036              	.LASF169:
 4037 07ce 746D5F6D 		.string	"tm_min"
 4037      696E00
GAS LISTING /tmp/ccZP9VrL.s 			page 80


 4038              	.LASF32:
 4039 07d5 5F535F6C 		.string	"_S_left"
 4039      65667400 
 4040              	.LASF125:
 4041 07dd 756E7369 		.string	"unsigned int"
 4041      676E6564 
 4041      20696E74 
 4041      00
 4042              	.LASF262:
 4043 07ea 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIlE5__minE"
 4043      5F5F676E 
 4043      755F6378 
 4043      7832345F 
 4043      5F6E756D 
 4044              	.LASF106:
 4045 081c 63686172 		.string	"char_traits<char>"
 4045      5F747261 
 4045      6974733C 
 4045      63686172 
 4045      3E00
 4046              	.LASF225:
 4047 082e 706F7369 		.string	"positive_sign"
 4047      74697665 
 4047      5F736967 
 4047      6E00
 4048              	.LASF56:
 4049 083c 5F535F69 		.string	"_S_ios_openmode_end"
 4049      6F735F6F 
 4049      70656E6D 
 4049      6F64655F 
 4049      656E6400 
 4050              	.LASF184:
 4051 0850 77637373 		.string	"wcsspn"
 4051      706E00
 4052              	.LASF233:
 4053 0857 705F7369 		.string	"p_sign_posn"
 4053      676E5F70 
 4053      6F736E00 
 4054              	.LASF23:
 4055 0863 5F5A4E53 		.string	"_ZNSt11char_traitsIcE11eq_int_typeERKiS2_"
 4055      74313163 
 4055      6861725F 
 4055      74726169 
 4055      74734963 
 4056              	.LASF41:
 4057 088d 5F535F75 		.string	"_S_uppercase"
 4057      70706572 
 4057      63617365 
 4057      00
 4058              	.LASF245:
 4059 089a 5F41746F 		.string	"_Atomic_word"
 4059      6D69635F 
 4059      776F7264 
 4059      00
 4060              	.LASF82:
 4061 08a7 73686F77 		.string	"showbase"
 4061      62617365 
GAS LISTING /tmp/ccZP9VrL.s 			page 81


 4061      00
 4062              	.LASF123:
 4063 08b0 6F766572 		.string	"overflow_arg_area"
 4063      666C6F77 
 4063      5F617267 
 4063      5F617265 
 4063      6100
 4064              	.LASF45:
 4065 08c2 5F535F69 		.string	"_S_ios_fmtflags_end"
 4065      6F735F66 
 4065      6D74666C 
 4065      6167735F 
 4065      656E6400 
 4066              	.LASF271:
 4067 08d6 496E6974 		.string	"Init"
 4067      00
 4068              	.LASF103:
 4069 08db 6F737472 		.string	"ostream"
 4069      65616D00 
 4070              	.LASF217:
 4071 08e3 64656369 		.string	"decimal_point"
 4071      6D616C5F 
 4071      706F696E 
 4071      7400
 4072              	.LASF130:
 4073 08f1 5F5F636F 		.string	"__count"
 4073      756E7400 
 4074              	.LASF104:
 4075 08f9 5F5F676E 		.string	"__gnu_cxx"
 4075      755F6378 
 4075      7800
 4076              	.LASF215:
 4077 0903 626F6F6C 		.string	"bool"
 4077      00
 4078              	.LASF264:
 4079 0908 474E5520 		.string	"GNU C++ 5.4.1 20160904 -mtune=generic -march=x86-64 -g -O0 -fstack-protector-strong"
 4079      432B2B20 
 4079      352E342E 
 4079      31203230 
 4079      31363039 
 4080              	.LASF17:
 4081 095c 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6assignEPcmc"
 4081      74313163 
 4081      6861725F 
 4081      74726169 
 4081      74734963 
 4082              	.LASF206:
 4083 097e 6C6F6E67 		.string	"long double"
 4083      20646F75 
 4083      626C6500 
 4084              	.LASF150:
 4085 098a 70757477 		.string	"putwc"
 4085      6300
 4086              	.LASF276:
 4087 0990 5F5F6E75 		.string	"__numeric_traits_integer<long int>"
 4087      6D657269 
 4087      635F7472 
GAS LISTING /tmp/ccZP9VrL.s 			page 82


 4087      61697473 
 4087      5F696E74 
 4088              	.LASF84:
 4089 09b3 73686F77 		.string	"showpos"
 4089      706F7300 
 4090              	.LASF44:
 4091 09bb 5F535F66 		.string	"_S_floatfield"
 4091      6C6F6174 
 4091      6669656C 
 4091      6400
 4092              	.LASF33:
 4093 09c9 5F535F6F 		.string	"_S_oct"
 4093      637400
 4094              	.LASF129:
 4095 09d0 5F5F7763 		.string	"__wchb"
 4095      686200
 4096              	.LASF98:
 4097 09d7 62696E61 		.string	"binary"
 4097      727900
 4098              	.LASF280:
 4099 09de 5F5F7374 		.string	"__static_initialization_and_destruction_0"
 4099      61746963 
 4099      5F696E69 
 4099      7469616C 
 4099      697A6174 
 4100              	.LASF8:
 4101 0a08 5F5A4E53 		.string	"_ZNSt11char_traitsIcE7compareEPKcS2_m"
 4101      74313163 
 4101      6861725F 
 4101      74726169 
 4101      74734963 
 4102              	.LASF210:
 4103 0a2e 6C6F6E67 		.string	"long long unsigned int"
 4103      206C6F6E 
 4103      6720756E 
 4103      7369676E 
 4103      65642069 
 4104              	.LASF124:
 4105 0a45 7265675F 		.string	"reg_save_area"
 4105      73617665 
 4105      5F617265 
 4105      6100
 4106              	.LASF205:
 4107 0a53 77637374 		.string	"wcstold"
 4107      6F6C6400 
 4108              	.LASF236:
 4109 0a5b 696E745F 		.string	"int_p_sep_by_space"
 4109      705F7365 
 4109      705F6279 
 4109      5F737061 
 4109      636500
 4110              	.LASF71:
 4111 0a6e 5F535F69 		.string	"_S_ios_seekdir_end"
 4111      6F735F73 
 4111      65656B64 
 4111      69725F65 
 4111      6E6400
GAS LISTING /tmp/ccZP9VrL.s 			page 83


 4112              	.LASF7:
 4113 0a81 6C656E67 		.string	"length"
 4113      746800
 4114              	.LASF207:
 4115 0a88 77637374 		.string	"wcstoll"
 4115      6F6C6C00 
 4116              	.LASF203:
 4117 0a90 77637373 		.string	"wcsstr"
 4117      747200
 4118              	.LASF59:
 4119 0a97 5F496F73 		.string	"_Ios_Iostate"
 4119      5F496F73 
 4119      74617465 
 4119      00
 4120              	.LASF126:
 4121 0aa4 6C6F6E67 		.string	"long unsigned int"
 4121      20756E73 
 4121      69676E65 
 4121      6420696E 
 4121      7400
 4122              	.LASF259:
 4123 0ab6 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIcE5__maxE"
 4123      5F5F676E 
 4123      755F6378 
 4123      7832345F 
 4123      5F6E756D 
 4124              	.LASF183:
 4125 0ae8 77637372 		.string	"wcsrtombs"
 4125      746F6D62 
 4125      7300
 4126              	.LASF88:
 4127 0af2 61646A75 		.string	"adjustfield"
 4127      73746669 
 4127      656C6400 
 4128              	.LASF174:
 4129 0afe 746D5F77 		.string	"tm_wday"
 4129      64617900 
 4130              	.LASF40:
 4131 0b06 5F535F75 		.string	"_S_unitbuf"
 4131      6E697462 
 4131      756600
 4132              	.LASF4:
 4133 0b11 5F5A4E53 		.string	"_ZNSt11char_traitsIcE2eqERKcS2_"
 4133      74313163 
 4133      6861725F 
 4133      74726169 
 4133      74734963 
 4134              	.LASF261:
 4135 0b31 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIsE5__maxE"
 4135      5F5F676E 
 4135      755F6378 
 4135      7832345F 
 4135      5F6E756D 
 4136              	.LASF90:
 4137 0b63 666C6F61 		.string	"floatfield"
 4137      74666965 
 4137      6C6400
GAS LISTING /tmp/ccZP9VrL.s 			page 84


 4138              	.LASF153:
 4139 0b6e 73777363 		.string	"swscanf"
 4139      616E6600 
 4140              	.LASF111:
 4141 0b76 5F5F6469 		.string	"__digits"
 4141      67697473 
 4141      00
 4142              	.LASF185:
 4143 0b7f 77637374 		.string	"wcstod"
 4143      6F6400
 4144              	.LASF186:
 4145 0b86 77637374 		.string	"wcstof"
 4145      6F6600
 4146              	.LASF188:
 4147 0b8d 77637374 		.string	"wcstok"
 4147      6F6B00
 4148              	.LASF0:
 4149 0b94 5F5F6378 		.string	"__cxx11"
 4149      78313100 
 4150              	.LASF99:
 4151 0b9c 7472756E 		.string	"trunc"
 4151      6300
 4152              	.LASF117:
 4153 0ba2 5F5F4649 		.string	"__FILE"
 4153      4C4500
 4154              	.LASF83:
 4155 0ba9 73686F77 		.string	"showpoint"
 4155      706F696E 
 4155      7400
 4156              	.LASF241:
 4157 0bb3 7365746C 		.string	"setlocale"
 4157      6F63616C 
 4157      6500
 4158              	.LASF202:
 4159 0bbd 77637372 		.string	"wcsrchr"
 4159      63687200 
 4160              	.LASF144:
 4161 0bc5 66777363 		.string	"fwscanf"
 4161      616E6600 
 4162              	.LASF127:
 4163 0bcd 77696E74 		.string	"wint_t"
 4163      5F7400
 4164              	.LASF57:
 4165 0bd4 5F535F69 		.string	"_S_ios_openmode_max"
 4165      6F735F6F 
 4165      70656E6D 
 4165      6F64655F 
 4165      6D617800 
 4166              	.LASF101:
 4167 0be8 696F735F 		.string	"ios_base"
 4167      62617365 
 4167      00
 4168              	.LASF93:
 4169 0bf1 62616462 		.string	"badbit"
 4169      697400
 4170              	.LASF250:
 4171 0bf8 77637472 		.string	"wctrans"
GAS LISTING /tmp/ccZP9VrL.s 			page 85


 4171      616E7300 
 4172              	.LASF218:
 4173 0c00 74686F75 		.string	"thousands_sep"
 4173      73616E64 
 4173      735F7365 
 4173      7000
 4174              	.LASF272:
 4175 0c0e 5F5A4E53 		.string	"_ZNSt8ios_base4InitC4Ev"
 4175      7438696F 
 4175      735F6261 
 4175      73653449 
 4175      6E697443 
 4176              	.LASF94:
 4177 0c26 656F6662 		.string	"eofbit"
 4177      697400
 4178              	.LASF179:
 4179 0c2d 7763736C 		.string	"wcslen"
 4179      656E00
 4180              	.LASF92:
 4181 0c34 696F7374 		.string	"iostate"
 4181      61746500 
 4182              	.LASF46:
 4183 0c3c 5F535F69 		.string	"_S_ios_fmtflags_max"
 4183      6F735F66 
 4183      6D74666C 
 4183      6167735F 
 4183      6D617800 
 4184              	.LASF20:
 4185 0c50 746F5F69 		.string	"to_int_type"
 4185      6E745F74 
 4185      79706500 
 4186              	.LASF18:
 4187 0c5c 746F5F63 		.string	"to_char_type"
 4187      6861725F 
 4187      74797065 
 4187      00
 4188              	.LASF1:
 4189 0c69 5F5F6465 		.string	"__debug"
 4189      62756700 
 4190              	.LASF177:
 4191 0c71 746D5F67 		.string	"tm_gmtoff"
 4191      6D746F66 
 4191      6600
 4192              	.LASF221:
 4193 0c7b 63757272 		.string	"currency_symbol"
 4193      656E6379 
 4193      5F73796D 
 4193      626F6C00 
 4194              	.LASF213:
 4195 0c8b 73686F72 		.string	"short int"
 4195      7420696E 
 4195      7400
 4196              	.LASF9:
 4197 0c95 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6lengthEPKc"
 4197      74313163 
 4197      6861725F 
 4197      74726169 
GAS LISTING /tmp/ccZP9VrL.s 			page 86


 4197      74734963 
 4198              	.LASF167:
 4199 0cb6 77637366 		.string	"wcsftime"
 4199      74696D65 
 4199      00
 4200              	.LASF224:
 4201 0cbf 6D6F6E5F 		.string	"mon_grouping"
 4201      67726F75 
 4201      70696E67 
 4201      00
 4202              	.LASF69:
 4203 0ccc 5F535F63 		.string	"_S_cur"
 4203      757200
 4204              	.LASF268:
 4205 0cd3 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6assignERcRKc"
 4205      74313163 
 4205      6861725F 
 4205      74726169 
 4205      74734963 
 4206              	.LASF162:
 4207 0cf6 77637363 		.string	"wcscat"
 4207      617400
 4208              	.LASF278:
 4209 0cfd 31315F5F 		.string	"11__mbstate_t"
 4209      6D627374 
 4209      6174655F 
 4209      7400
 4210              	.LASF239:
 4211 0d0b 696E745F 		.string	"int_p_sign_posn"
 4211      705F7369 
 4211      676E5F70 
 4211      6F736E00 
 4212              	.LASF178:
 4213 0d1b 746D5F7A 		.string	"tm_zone"
 4213      6F6E6500 
 4214              	.LASF160:
 4215 0d23 76777363 		.string	"vwscanf"
 4215      616E6600 
 4216              	.LASF64:
 4217 0d2b 5F535F69 		.string	"_S_ios_iostate_end"
 4217      6F735F69 
 4217      6F737461 
 4217      74655F65 
 4217      6E6400
 4218              	.LASF161:
 4219 0d3e 77637274 		.string	"wcrtomb"
 4219      6F6D6200 
 4220              	.LASF216:
 4221 0d46 6C636F6E 		.string	"lconv"
 4221      7600
 4222              	.LASF86:
 4223 0d4c 756E6974 		.string	"unitbuf"
 4223      62756600 
 4224              	.LASF269:
 4225 0d54 5F5A4E53 		.string	"_ZNSt11char_traitsIcE3eofEv"
 4225      74313163 
 4225      6861725F 
GAS LISTING /tmp/ccZP9VrL.s 			page 87


 4225      74726169 
 4225      74734963 
 4226              	.LASF180:
 4227 0d70 7763736E 		.string	"wcsncat"
 4227      63617400 
 4228              	.LASF116:
 4229 0d78 5F5F6E75 		.string	"__numeric_traits_integer<short int>"
 4229      6D657269 
 4229      635F7472 
 4229      61697473 
 4229      5F696E74 
 4230              	.LASF255:
 4231 0d9c 5F5F6473 		.string	"__dso_handle"
 4231      6F5F6861 
 4231      6E646C65 
 4231      00
 4232              	.LASF208:
 4233 0da9 6C6F6E67 		.string	"long long int"
 4233      206C6F6E 
 4233      6720696E 
 4233      7400
 4234              	.LASF140:
 4235 0db7 66707574 		.string	"fputwc"
 4235      776300
 4236              	.LASF141:
 4237 0dbe 66707574 		.string	"fputws"
 4237      777300
 4238              	.LASF74:
 4239 0dc5 7E496E69 		.string	"~Init"
 4239      7400
 4240              	.LASF149:
 4241 0dcb 6D627372 		.string	"mbsrtowcs"
 4241      746F7763 
 4241      7300
 4242              	.LASF63:
 4243 0dd5 5F535F66 		.string	"_S_failbit"
 4243      61696C62 
 4243      697400
 4244              	.LASF229:
 4245 0de0 705F6373 		.string	"p_cs_precedes"
 4245      5F707265 
 4245      63656465 
 4245      7300
 4246              	.LASF114:
 4247 0dee 5F5F6E75 		.string	"__numeric_traits_integer<long unsigned int>"
 4247      6D657269 
 4247      635F7472 
 4247      61697473 
 4247      5F696E74 
 4248              	.LASF173:
 4249 0e1a 746D5F79 		.string	"tm_year"
 4249      65617200 
 4250              	.LASF135:
 4251 0e22 73686F72 		.string	"short unsigned int"
 4251      7420756E 
 4251      7369676E 
 4251      65642069 
GAS LISTING /tmp/ccZP9VrL.s 			page 88


 4251      6E7400
 4252              	.LASF273:
 4253 0e35 5F547261 		.string	"_Traits"
 4253      69747300 
 4254              	.LASF105:
 4255 0e3d 5F5F6F70 		.string	"__ops"
 4255      7300
 4256              	.LASF156:
 4257 0e43 76667773 		.string	"vfwscanf"
 4257      63616E66 
 4257      00
 4258              	.LASF67:
 4259 0e4c 5F496F73 		.string	"_Ios_Seekdir"
 4259      5F536565 
 4259      6B646972 
 4259      00
 4260              	.LASF91:
 4261 0e59 666D7466 		.string	"fmtflags"
 4261      6C616773 
 4261      00
 4262              	.LASF244:
 4263 0e62 5F5F696E 		.string	"__int32_t"
 4263      7433325F 
 4263      7400
 4264              	.LASF145:
 4265 0e6c 67657477 		.string	"getwc"
 4265      6300
 4266              	.LASF148:
 4267 0e72 6D627369 		.string	"mbsinit"
 4267      6E697400 
 4268              	.LASF248:
 4269 0e7a 69737763 		.string	"iswctype"
 4269      74797065 
 4269      00
 4270              	.LASF16:
 4271 0e83 61737369 		.string	"assign"
 4271      676E00
 4272              	.LASF219:
 4273 0e8a 67726F75 		.string	"grouping"
 4273      70696E67 
 4273      00
 4274              	.LASF198:
 4275 0e93 77707269 		.string	"wprintf"
 4275      6E746600 
 4276              	.LASF263:
 4277 0e9b 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIlE5__maxE"
 4277      5F5F676E 
 4277      755F6378 
 4277      7832345F 
 4277      5F6E756D 
 4278              	.LASF270:
 4279 0ecd 5F5A4E53 		.string	"_ZNSt11char_traitsIcE7not_eofERKi"
 4279      74313163 
 4279      6861725F 
 4279      74726169 
 4279      74734963 
 4280              	.LASF35:
GAS LISTING /tmp/ccZP9VrL.s 			page 89


 4281 0eef 5F535F73 		.string	"_S_scientific"
 4281      6369656E 
 4281      74696669 
 4281      6300
 4282              	.LASF139:
 4283 0efd 77636861 		.string	"wchar_t"
 4283      725F7400 
 4284              	.LASF120:
 4285 0f05 74797065 		.string	"typedef __va_list_tag __va_list_tag"
 4285      64656620 
 4285      5F5F7661 
 4285      5F6C6973 
 4285      745F7461 
 4286              	.LASF191:
 4287 0f29 77637374 		.string	"wcstoul"
 4287      6F756C00 
 4288              		.hidden	__dso_handle
 4289              		.ident	"GCC: (Ubuntu 5.4.1-2ubuntu1~16.04) 5.4.1 20160904"
 4290              		.section	.note.GNU-stack,"",@progbits
GAS LISTING /tmp/ccZP9VrL.s 			page 90


DEFINED SYMBOLS
                            *ABS*:0000000000000000 complex.c
                             .bss:0000000000000000 _ZStL8__ioinit
     /tmp/ccZP9VrL.s:16     .text:0000000000000000 main
     /tmp/ccZP9VrL.s:68     .text:0000000000000086 _Z41__static_initialization_and_destruction_0ii
     /tmp/ccZP9VrL.s:104    .text:00000000000000c4 _GLOBAL__sub_I_main

UNDEFINED SYMBOLS
_ZSt4cout
_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
_ZNSolsEd
_ZNSt8ios_base4InitC1Ev
__dso_handle
_ZNSt8ios_base4InitD1Ev
__cxa_atexit
