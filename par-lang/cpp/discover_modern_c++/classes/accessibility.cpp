// specifier declares a property of a single item
// modifier characterizes multiple items
//  all methods and data members preceding the next modifiers

class rational
{
  public:
    ...
    rational operator+(...) {...}
    rational operator-(...) {...}
  private:
    int p;
    int q;
};

