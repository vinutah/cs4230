/*
 * steams are not limited to screens, keyboards and files
 *
 * every class can be used as a stream when it is derived from
 * istream, ostream, or iostream
 *
 * boost.asio      stream for tcp/ip
 *
 * boost.iostream  alternatives to std io
 *
 * the standard library contains a stringstream --> sstream
 * stringstream can be used to create a string from
 * any kind of printable type
 *
 * stringstream's method str() can be use to create a string
 * from any kind of printable type.
 *
 * stringstream's method str() returns the stream's internal string.
 *
 */

#include <iostream>
#include <fstream>
#include <sstream>

void write_something(std::ostream& os)
{
  os << "Hi stream, did you know that 3 * 3 = " << 3 * 3 << std::endl;
}

int main(){


  write_something(std::cout);

  std::ofstream myfile("example.txt");
  write_something(myfile);

  std::stringstream mystream;
  write_something(mystream);
  std::cout << "mystream is: " << mystream.str(); // new line contained


}
