GAS LISTING /tmp/cca1HRfE.s 			page 1


   1              		.file	"generic_stream.cpp"
   2              		.text
   3              	.Ltext0:
   4              		.section	.text._ZStorSt13_Ios_OpenmodeS_,"axG",@progbits,_ZStorSt13_Ios_OpenmodeS_,comdat
   5              		.weak	_ZStorSt13_Ios_OpenmodeS_
   6              		.type	_ZStorSt13_Ios_OpenmodeS_, @function
   7              	_ZStorSt13_Ios_OpenmodeS_:
   8              	.LFB644:
   9              		.file 1 "/usr/include/c++/5/bits/ios_base.h"
   1:/usr/include/c++/5/bits/ios_base.h **** // Iostreams base classes -*- C++ -*-
   2:/usr/include/c++/5/bits/ios_base.h **** 
   3:/usr/include/c++/5/bits/ios_base.h **** // Copyright (C) 1997-2015 Free Software Foundation, Inc.
   4:/usr/include/c++/5/bits/ios_base.h **** //
   5:/usr/include/c++/5/bits/ios_base.h **** // This file is part of the GNU ISO C++ Library.  This library is free
   6:/usr/include/c++/5/bits/ios_base.h **** // software; you can redistribute it and/or modify it under the
   7:/usr/include/c++/5/bits/ios_base.h **** // terms of the GNU General Public License as published by the
   8:/usr/include/c++/5/bits/ios_base.h **** // Free Software Foundation; either version 3, or (at your option)
   9:/usr/include/c++/5/bits/ios_base.h **** // any later version.
  10:/usr/include/c++/5/bits/ios_base.h **** 
  11:/usr/include/c++/5/bits/ios_base.h **** // This library is distributed in the hope that it will be useful,
  12:/usr/include/c++/5/bits/ios_base.h **** // but WITHOUT ANY WARRANTY; without even the implied warranty of
  13:/usr/include/c++/5/bits/ios_base.h **** // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  14:/usr/include/c++/5/bits/ios_base.h **** // GNU General Public License for more details.
  15:/usr/include/c++/5/bits/ios_base.h **** 
  16:/usr/include/c++/5/bits/ios_base.h **** // Under Section 7 of GPL version 3, you are granted additional
  17:/usr/include/c++/5/bits/ios_base.h **** // permissions described in the GCC Runtime Library Exception, version
  18:/usr/include/c++/5/bits/ios_base.h **** // 3.1, as published by the Free Software Foundation.
  19:/usr/include/c++/5/bits/ios_base.h **** 
  20:/usr/include/c++/5/bits/ios_base.h **** // You should have received a copy of the GNU General Public License and
  21:/usr/include/c++/5/bits/ios_base.h **** // a copy of the GCC Runtime Library Exception along with this program;
  22:/usr/include/c++/5/bits/ios_base.h **** // see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
  23:/usr/include/c++/5/bits/ios_base.h **** // <http://www.gnu.org/licenses/>.
  24:/usr/include/c++/5/bits/ios_base.h **** 
  25:/usr/include/c++/5/bits/ios_base.h **** /** @file bits/ios_base.h
  26:/usr/include/c++/5/bits/ios_base.h ****  *  This is an internal header file, included by other library headers.
  27:/usr/include/c++/5/bits/ios_base.h ****  *  Do not attempt to use it directly. @headername{ios}
  28:/usr/include/c++/5/bits/ios_base.h ****  */
  29:/usr/include/c++/5/bits/ios_base.h **** 
  30:/usr/include/c++/5/bits/ios_base.h **** //
  31:/usr/include/c++/5/bits/ios_base.h **** // ISO C++ 14882: 27.4  Iostreams base classes
  32:/usr/include/c++/5/bits/ios_base.h **** //
  33:/usr/include/c++/5/bits/ios_base.h **** 
  34:/usr/include/c++/5/bits/ios_base.h **** #ifndef _IOS_BASE_H
  35:/usr/include/c++/5/bits/ios_base.h **** #define _IOS_BASE_H 1
  36:/usr/include/c++/5/bits/ios_base.h **** 
  37:/usr/include/c++/5/bits/ios_base.h **** #pragma GCC system_header
  38:/usr/include/c++/5/bits/ios_base.h **** 
  39:/usr/include/c++/5/bits/ios_base.h **** #include <ext/atomicity.h>
  40:/usr/include/c++/5/bits/ios_base.h **** #include <bits/localefwd.h>
  41:/usr/include/c++/5/bits/ios_base.h **** #include <bits/locale_classes.h>
  42:/usr/include/c++/5/bits/ios_base.h **** 
  43:/usr/include/c++/5/bits/ios_base.h **** #if __cplusplus < 201103L
  44:/usr/include/c++/5/bits/ios_base.h **** # include <stdexcept>
  45:/usr/include/c++/5/bits/ios_base.h **** #else
  46:/usr/include/c++/5/bits/ios_base.h **** # include <system_error>
  47:/usr/include/c++/5/bits/ios_base.h **** #endif
  48:/usr/include/c++/5/bits/ios_base.h **** 
GAS LISTING /tmp/cca1HRfE.s 			page 2


  49:/usr/include/c++/5/bits/ios_base.h **** namespace std _GLIBCXX_VISIBILITY(default)
  50:/usr/include/c++/5/bits/ios_base.h **** {
  51:/usr/include/c++/5/bits/ios_base.h **** _GLIBCXX_BEGIN_NAMESPACE_VERSION
  52:/usr/include/c++/5/bits/ios_base.h **** 
  53:/usr/include/c++/5/bits/ios_base.h ****   // The following definitions of bitmask types are enums, not ints,
  54:/usr/include/c++/5/bits/ios_base.h ****   // as permitted (but not required) in the standard, in order to provide
  55:/usr/include/c++/5/bits/ios_base.h ****   // better type safety in iostream calls.  A side effect is that
  56:/usr/include/c++/5/bits/ios_base.h ****   // expressions involving them are no longer compile-time constants.
  57:/usr/include/c++/5/bits/ios_base.h ****   enum _Ios_Fmtflags 
  58:/usr/include/c++/5/bits/ios_base.h ****     { 
  59:/usr/include/c++/5/bits/ios_base.h ****       _S_boolalpha 	= 1L << 0,
  60:/usr/include/c++/5/bits/ios_base.h ****       _S_dec 		= 1L << 1,
  61:/usr/include/c++/5/bits/ios_base.h ****       _S_fixed 		= 1L << 2,
  62:/usr/include/c++/5/bits/ios_base.h ****       _S_hex 		= 1L << 3,
  63:/usr/include/c++/5/bits/ios_base.h ****       _S_internal 	= 1L << 4,
  64:/usr/include/c++/5/bits/ios_base.h ****       _S_left 		= 1L << 5,
  65:/usr/include/c++/5/bits/ios_base.h ****       _S_oct 		= 1L << 6,
  66:/usr/include/c++/5/bits/ios_base.h ****       _S_right 		= 1L << 7,
  67:/usr/include/c++/5/bits/ios_base.h ****       _S_scientific 	= 1L << 8,
  68:/usr/include/c++/5/bits/ios_base.h ****       _S_showbase 	= 1L << 9,
  69:/usr/include/c++/5/bits/ios_base.h ****       _S_showpoint 	= 1L << 10,
  70:/usr/include/c++/5/bits/ios_base.h ****       _S_showpos 	= 1L << 11,
  71:/usr/include/c++/5/bits/ios_base.h ****       _S_skipws 	= 1L << 12,
  72:/usr/include/c++/5/bits/ios_base.h ****       _S_unitbuf 	= 1L << 13,
  73:/usr/include/c++/5/bits/ios_base.h ****       _S_uppercase 	= 1L << 14,
  74:/usr/include/c++/5/bits/ios_base.h ****       _S_adjustfield 	= _S_left | _S_right | _S_internal,
  75:/usr/include/c++/5/bits/ios_base.h ****       _S_basefield 	= _S_dec | _S_oct | _S_hex,
  76:/usr/include/c++/5/bits/ios_base.h ****       _S_floatfield 	= _S_scientific | _S_fixed,
  77:/usr/include/c++/5/bits/ios_base.h ****       _S_ios_fmtflags_end = 1L << 16,
  78:/usr/include/c++/5/bits/ios_base.h ****       _S_ios_fmtflags_max = __INT_MAX__,
  79:/usr/include/c++/5/bits/ios_base.h ****       _S_ios_fmtflags_min = ~__INT_MAX__
  80:/usr/include/c++/5/bits/ios_base.h ****     };
  81:/usr/include/c++/5/bits/ios_base.h **** 
  82:/usr/include/c++/5/bits/ios_base.h ****   inline _GLIBCXX_CONSTEXPR _Ios_Fmtflags
  83:/usr/include/c++/5/bits/ios_base.h ****   operator&(_Ios_Fmtflags __a, _Ios_Fmtflags __b)
  84:/usr/include/c++/5/bits/ios_base.h ****   { return _Ios_Fmtflags(static_cast<int>(__a) & static_cast<int>(__b)); }
  85:/usr/include/c++/5/bits/ios_base.h **** 
  86:/usr/include/c++/5/bits/ios_base.h ****   inline _GLIBCXX_CONSTEXPR _Ios_Fmtflags
  87:/usr/include/c++/5/bits/ios_base.h ****   operator|(_Ios_Fmtflags __a, _Ios_Fmtflags __b)
  88:/usr/include/c++/5/bits/ios_base.h ****   { return _Ios_Fmtflags(static_cast<int>(__a) | static_cast<int>(__b)); }
  89:/usr/include/c++/5/bits/ios_base.h **** 
  90:/usr/include/c++/5/bits/ios_base.h ****   inline _GLIBCXX_CONSTEXPR _Ios_Fmtflags
  91:/usr/include/c++/5/bits/ios_base.h ****   operator^(_Ios_Fmtflags __a, _Ios_Fmtflags __b)
  92:/usr/include/c++/5/bits/ios_base.h ****   { return _Ios_Fmtflags(static_cast<int>(__a) ^ static_cast<int>(__b)); }
  93:/usr/include/c++/5/bits/ios_base.h **** 
  94:/usr/include/c++/5/bits/ios_base.h ****   inline _GLIBCXX_CONSTEXPR _Ios_Fmtflags
  95:/usr/include/c++/5/bits/ios_base.h ****   operator~(_Ios_Fmtflags __a)
  96:/usr/include/c++/5/bits/ios_base.h ****   { return _Ios_Fmtflags(~static_cast<int>(__a)); }
  97:/usr/include/c++/5/bits/ios_base.h **** 
  98:/usr/include/c++/5/bits/ios_base.h ****   inline const _Ios_Fmtflags&
  99:/usr/include/c++/5/bits/ios_base.h ****   operator|=(_Ios_Fmtflags& __a, _Ios_Fmtflags __b)
 100:/usr/include/c++/5/bits/ios_base.h ****   { return __a = __a | __b; }
 101:/usr/include/c++/5/bits/ios_base.h **** 
 102:/usr/include/c++/5/bits/ios_base.h ****   inline const _Ios_Fmtflags&
 103:/usr/include/c++/5/bits/ios_base.h ****   operator&=(_Ios_Fmtflags& __a, _Ios_Fmtflags __b)
 104:/usr/include/c++/5/bits/ios_base.h ****   { return __a = __a & __b; }
 105:/usr/include/c++/5/bits/ios_base.h **** 
GAS LISTING /tmp/cca1HRfE.s 			page 3


 106:/usr/include/c++/5/bits/ios_base.h ****   inline const _Ios_Fmtflags&
 107:/usr/include/c++/5/bits/ios_base.h ****   operator^=(_Ios_Fmtflags& __a, _Ios_Fmtflags __b)
 108:/usr/include/c++/5/bits/ios_base.h ****   { return __a = __a ^ __b; }
 109:/usr/include/c++/5/bits/ios_base.h **** 
 110:/usr/include/c++/5/bits/ios_base.h **** 
 111:/usr/include/c++/5/bits/ios_base.h ****   enum _Ios_Openmode 
 112:/usr/include/c++/5/bits/ios_base.h ****     { 
 113:/usr/include/c++/5/bits/ios_base.h ****       _S_app 		= 1L << 0,
 114:/usr/include/c++/5/bits/ios_base.h ****       _S_ate 		= 1L << 1,
 115:/usr/include/c++/5/bits/ios_base.h ****       _S_bin 		= 1L << 2,
 116:/usr/include/c++/5/bits/ios_base.h ****       _S_in 		= 1L << 3,
 117:/usr/include/c++/5/bits/ios_base.h ****       _S_out 		= 1L << 4,
 118:/usr/include/c++/5/bits/ios_base.h ****       _S_trunc 		= 1L << 5,
 119:/usr/include/c++/5/bits/ios_base.h ****       _S_ios_openmode_end = 1L << 16,
 120:/usr/include/c++/5/bits/ios_base.h ****       _S_ios_openmode_max = __INT_MAX__,
 121:/usr/include/c++/5/bits/ios_base.h ****       _S_ios_openmode_min = ~__INT_MAX__
 122:/usr/include/c++/5/bits/ios_base.h ****     };
 123:/usr/include/c++/5/bits/ios_base.h **** 
 124:/usr/include/c++/5/bits/ios_base.h ****   inline _GLIBCXX_CONSTEXPR _Ios_Openmode
 125:/usr/include/c++/5/bits/ios_base.h ****   operator&(_Ios_Openmode __a, _Ios_Openmode __b)
 126:/usr/include/c++/5/bits/ios_base.h ****   { return _Ios_Openmode(static_cast<int>(__a) & static_cast<int>(__b)); }
 127:/usr/include/c++/5/bits/ios_base.h **** 
 128:/usr/include/c++/5/bits/ios_base.h ****   inline _GLIBCXX_CONSTEXPR _Ios_Openmode
 129:/usr/include/c++/5/bits/ios_base.h ****   operator|(_Ios_Openmode __a, _Ios_Openmode __b)
 130:/usr/include/c++/5/bits/ios_base.h ****   { return _Ios_Openmode(static_cast<int>(__a) | static_cast<int>(__b)); }
  10              		.loc 1 130 0
  11              		.cfi_startproc
  12 0000 55       		pushq	%rbp
  13              		.cfi_def_cfa_offset 16
  14              		.cfi_offset 6, -16
  15 0001 4889E5   		movq	%rsp, %rbp
  16              		.cfi_def_cfa_register 6
  17 0004 897DFC   		movl	%edi, -4(%rbp)
  18 0007 8975F8   		movl	%esi, -8(%rbp)
  19              		.loc 1 130 0
  20 000a 8B45FC   		movl	-4(%rbp), %eax
  21 000d 0B45F8   		orl	-8(%rbp), %eax
  22 0010 5D       		popq	%rbp
  23              		.cfi_def_cfa 7, 8
  24 0011 C3       		ret
  25              		.cfi_endproc
  26              	.LFE644:
  27              		.size	_ZStorSt13_Ios_OpenmodeS_, .-_ZStorSt13_Ios_OpenmodeS_
  28              		.local	_ZStL8__ioinit
  29              		.comm	_ZStL8__ioinit,1,1
  30              		.section	.rodata
  31              		.align 8
  32              	.LC0:
  33 0000 48692073 		.string	"Hi stream, did you know that 3 * 3 = "
  33      74726561 
  33      6D2C2064 
  33      69642079 
  33      6F75206B 
  34              		.text
  35              		.globl	_Z15write_somethingRSo
  36              		.type	_Z15write_somethingRSo, @function
  37              	_Z15write_somethingRSo:
GAS LISTING /tmp/cca1HRfE.s 			page 4


  38              	.LFB1118:
  39              		.file 2 "generic_stream.cpp"
   1:generic_stream.cpp **** /*
   2:generic_stream.cpp ****  * steams are not limited to screens, keyboards and files
   3:generic_stream.cpp ****  *
   4:generic_stream.cpp ****  * every class can be used as a stream when it is derived from
   5:generic_stream.cpp ****  * istream, ostream, or iostream
   6:generic_stream.cpp ****  *
   7:generic_stream.cpp ****  * boost.asio      stream for tcp/ip
   8:generic_stream.cpp ****  *
   9:generic_stream.cpp ****  * boost.iostream  alternatives to std io
  10:generic_stream.cpp ****  *
  11:generic_stream.cpp ****  * the standard library contains a stringstream --> sstream
  12:generic_stream.cpp ****  * stringstream can be used to create a string from
  13:generic_stream.cpp ****  * any kind of printable type
  14:generic_stream.cpp ****  *
  15:generic_stream.cpp ****  * stringstream's method str() can be use to create a string
  16:generic_stream.cpp ****  * from any kind of printable type.
  17:generic_stream.cpp ****  *
  18:generic_stream.cpp ****  * stringstream's method str() returns the stream's internal string.
  19:generic_stream.cpp ****  *
  20:generic_stream.cpp ****  */
  21:generic_stream.cpp **** 
  22:generic_stream.cpp **** #include <iostream>
  23:generic_stream.cpp **** #include <fstream>
  24:generic_stream.cpp **** #include <sstream>
  25:generic_stream.cpp **** 
  26:generic_stream.cpp **** void write_something(std::ostream& os)
  27:generic_stream.cpp **** {
  40              		.loc 2 27 0
  41              		.cfi_startproc
  42 0000 55       		pushq	%rbp
  43              		.cfi_def_cfa_offset 16
  44              		.cfi_offset 6, -16
  45 0001 4889E5   		movq	%rsp, %rbp
  46              		.cfi_def_cfa_register 6
  47 0004 4883EC10 		subq	$16, %rsp
  48 0008 48897DF8 		movq	%rdi, -8(%rbp)
  28:generic_stream.cpp ****   os << "Hi stream, did you know that 3 * 3 = " << 3 * 3 << std::endl;
  49              		.loc 2 28 0
  50 000c 488B45F8 		movq	-8(%rbp), %rax
  51 0010 BE000000 		movl	$.LC0, %esi
  51      00
  52 0015 4889C7   		movq	%rax, %rdi
  53 0018 E8000000 		call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
  53      00
  54 001d BE090000 		movl	$9, %esi
  54      00
  55 0022 4889C7   		movq	%rax, %rdi
  56 0025 E8000000 		call	_ZNSolsEi
  56      00
  57 002a BE000000 		movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
  57      00
  58 002f 4889C7   		movq	%rax, %rdi
  59 0032 E8000000 		call	_ZNSolsEPFRSoS_E
  59      00
  29:generic_stream.cpp **** }
GAS LISTING /tmp/cca1HRfE.s 			page 5


  60              		.loc 2 29 0
  61 0037 90       		nop
  62 0038 C9       		leave
  63              		.cfi_def_cfa 7, 8
  64 0039 C3       		ret
  65              		.cfi_endproc
  66              	.LFE1118:
  67              		.size	_Z15write_somethingRSo, .-_Z15write_somethingRSo
  68              		.section	.rodata
  69              	.LC1:
  70 0026 6578616D 		.string	"example.txt"
  70      706C652E 
  70      74787400 
  71              	.LC2:
  72 0032 6D797374 		.string	"mystream is: "
  72      7265616D 
  72      2069733A 
  72      2000
  73              		.text
  74              		.globl	main
  75              		.type	main, @function
  76              	main:
  77              	.LFB1119:
  30:generic_stream.cpp **** 
  31:generic_stream.cpp **** int main(){
  78              		.loc 2 31 0
  79              		.cfi_startproc
  80              		.cfi_personality 0x3,__gxx_personality_v0
  81              		.cfi_lsda 0x3,.LLSDA1119
  82 003a 55       		pushq	%rbp
  83              		.cfi_def_cfa_offset 16
  84              		.cfi_offset 6, -16
  85 003b 4889E5   		movq	%rsp, %rbp
  86              		.cfi_def_cfa_register 6
  87 003e 53       		pushq	%rbx
  88 003f 4881ECC8 		subq	$968, %rsp
  88      030000
  89              		.cfi_offset 3, -24
  90              		.loc 2 31 0
  91 0046 64488B04 		movq	%fs:40, %rax
  91      25280000 
  91      00
  92 004f 488945E8 		movq	%rax, -24(%rbp)
  93 0053 31C0     		xorl	%eax, %eax
  32:generic_stream.cpp **** 
  33:generic_stream.cpp **** 
  34:generic_stream.cpp ****   write_something(std::cout);
  94              		.loc 2 34 0
  95 0055 BF000000 		movl	$_ZSt4cout, %edi
  95      00
  96              	.LEHB0:
  97 005a E8000000 		call	_Z15write_somethingRSo
  97      00
  35:generic_stream.cpp **** 
  36:generic_stream.cpp ****   std::ofstream myfile("example.txt");
  98              		.loc 2 36 0
  99 005f BE200000 		movl	$32, %esi
GAS LISTING /tmp/cca1HRfE.s 			page 6


  99      00
 100 0064 BF100000 		movl	$16, %edi
 100      00
 101 0069 E8000000 		call	_ZStorSt13_Ios_OpenmodeS_
 101      00
 102 006e 89C2     		movl	%eax, %edx
 103 0070 488D85E0 		leaq	-544(%rbp), %rax
 103      FDFFFF
 104 0077 BE000000 		movl	$.LC1, %esi
 104      00
 105 007c 4889C7   		movq	%rax, %rdi
 106 007f E8000000 		call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1EPKcSt13_Ios_Openmode
 106      00
 107              	.LEHE0:
  37:generic_stream.cpp ****   write_something(myfile);
 108              		.loc 2 37 0
 109 0084 488D85E0 		leaq	-544(%rbp), %rax
 109      FDFFFF
 110 008b 4889C7   		movq	%rax, %rdi
 111              	.LEHB1:
 112 008e E8000000 		call	_Z15write_somethingRSo
 112      00
  38:generic_stream.cpp **** 
  39:generic_stream.cpp ****   std::stringstream mystream;
 113              		.loc 2 39 0
 114 0093 BE080000 		movl	$8, %esi
 114      00
 115 0098 BF100000 		movl	$16, %edi
 115      00
 116 009d E8000000 		call	_ZStorSt13_Ios_OpenmodeS_
 116      00
 117 00a2 89C2     		movl	%eax, %edx
 118 00a4 488D8550 		leaq	-944(%rbp), %rax
 118      FCFFFF
 119 00ab 89D6     		movl	%edx, %esi
 120 00ad 4889C7   		movq	%rax, %rdi
 121 00b0 E8000000 		call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1ESt13_Ios_Openmode
 121      00
 122              	.LEHE1:
  40:generic_stream.cpp ****   write_something(mystream);
 123              		.loc 2 40 0
 124 00b5 488D8550 		leaq	-944(%rbp), %rax
 124      FCFFFF
 125 00bc 4883C010 		addq	$16, %rax
 126 00c0 4889C7   		movq	%rax, %rdi
 127              	.LEHB2:
 128 00c3 E8000000 		call	_Z15write_somethingRSo
 128      00
  41:generic_stream.cpp ****   std::cout << "mystream is: " << mystream.str(); // new line contained
 129              		.loc 2 41 0
 130 00c8 488D8530 		leaq	-976(%rbp), %rax
 130      FCFFFF
 131 00cf 488D9550 		leaq	-944(%rbp), %rdx
 131      FCFFFF
 132 00d6 4889D6   		movq	%rdx, %rsi
 133 00d9 4889C7   		movq	%rax, %rdi
 134 00dc E8000000 		call	_ZNKSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEE3strEv
GAS LISTING /tmp/cca1HRfE.s 			page 7


 134      00
 135              	.LEHE2:
 136              		.loc 2 41 0 is_stmt 0 discriminator 1
 137 00e1 BE000000 		movl	$.LC2, %esi
 137      00
 138 00e6 BF000000 		movl	$_ZSt4cout, %edi
 138      00
 139              	.LEHB3:
 140 00eb E8000000 		call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
 140      00
 141 00f0 4889C2   		movq	%rax, %rdx
 142 00f3 488D8530 		leaq	-976(%rbp), %rax
 142      FCFFFF
 143 00fa 4889C6   		movq	%rax, %rsi
 144 00fd 4889D7   		movq	%rdx, %rdi
 145 0100 E8000000 		call	_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5
 145      00
 146              	.LEHE3:
 147              		.loc 2 41 0 discriminator 5
 148 0105 488D8530 		leaq	-976(%rbp), %rax
 148      FCFFFF
 149 010c 4889C7   		movq	%rax, %rdi
 150              	.LEHB4:
 151 010f E8000000 		call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev
 151      00
 152              	.LEHE4:
  39:generic_stream.cpp ****   write_something(mystream);
 153              		.loc 2 39 0 is_stmt 1 discriminator 6
 154 0114 488D8550 		leaq	-944(%rbp), %rax
 154      FCFFFF
 155 011b 4889C7   		movq	%rax, %rdi
 156              	.LEHB5:
 157 011e E8000000 		call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev
 157      00
 158              	.LEHE5:
  36:generic_stream.cpp ****   write_something(myfile);
 159              		.loc 2 36 0
 160 0123 488D85E0 		leaq	-544(%rbp), %rax
 160      FDFFFF
 161 012a 4889C7   		movq	%rax, %rdi
 162              	.LEHB6:
 163 012d E8000000 		call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
 163      00
 164              	.LEHE6:
  42:generic_stream.cpp **** 
  43:generic_stream.cpp **** 
  44:generic_stream.cpp **** }
 165              		.loc 2 44 0
 166 0132 B8000000 		movl	$0, %eax
 166      00
 167 0137 488B4DE8 		movq	-24(%rbp), %rcx
 168 013b 6448330C 		xorq	%fs:40, %rcx
 168      25280000 
 168      00
 169 0144 744C     		je	.L9
 170 0146 EB45     		jmp	.L13
 171              	.L12:
GAS LISTING /tmp/cca1HRfE.s 			page 8


 172 0148 4889C3   		movq	%rax, %rbx
  41:generic_stream.cpp **** 
 173              		.loc 2 41 0
 174 014b 488D8530 		leaq	-976(%rbp), %rax
 174      FCFFFF
 175 0152 4889C7   		movq	%rax, %rdi
 176 0155 E8000000 		call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev
 176      00
 177 015a EB03     		jmp	.L7
 178              	.L11:
 179 015c 4889C3   		movq	%rax, %rbx
 180              	.L7:
  39:generic_stream.cpp ****   write_something(mystream);
 181              		.loc 2 39 0
 182 015f 488D8550 		leaq	-944(%rbp), %rax
 182      FCFFFF
 183 0166 4889C7   		movq	%rax, %rdi
 184 0169 E8000000 		call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev
 184      00
 185 016e EB03     		jmp	.L8
 186              	.L10:
 187 0170 4889C3   		movq	%rax, %rbx
 188              	.L8:
  36:generic_stream.cpp ****   write_something(myfile);
 189              		.loc 2 36 0
 190 0173 488D85E0 		leaq	-544(%rbp), %rax
 190      FDFFFF
 191 017a 4889C7   		movq	%rax, %rdi
 192 017d E8000000 		call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
 192      00
 193 0182 4889D8   		movq	%rbx, %rax
 194 0185 4889C7   		movq	%rax, %rdi
 195              	.LEHB7:
 196 0188 E8000000 		call	_Unwind_Resume
 196      00
 197              	.LEHE7:
 198              	.L13:
 199              		.loc 2 44 0
 200 018d E8000000 		call	__stack_chk_fail
 200      00
 201              	.L9:
 202 0192 4881C4C8 		addq	$968, %rsp
 202      030000
 203 0199 5B       		popq	%rbx
 204 019a 5D       		popq	%rbp
 205              		.cfi_def_cfa 7, 8
 206 019b C3       		ret
 207              		.cfi_endproc
 208              	.LFE1119:
 209              		.globl	__gxx_personality_v0
 210              		.section	.gcc_except_table,"a",@progbits
 211              	.LLSDA1119:
 212 0000 FF       		.byte	0xff
 213 0001 FF       		.byte	0xff
 214 0002 01       		.byte	0x1
 215 0003 2B       		.uleb128 .LLSDACSE1119-.LLSDACSB1119
 216              	.LLSDACSB1119:
GAS LISTING /tmp/cca1HRfE.s 			page 9


 217 0004 20       		.uleb128 .LEHB0-.LFB1119
 218 0005 2A       		.uleb128 .LEHE0-.LEHB0
 219 0006 00       		.uleb128 0
 220 0007 00       		.uleb128 0
 221 0008 54       		.uleb128 .LEHB1-.LFB1119
 222 0009 27       		.uleb128 .LEHE1-.LEHB1
 223 000a B602     		.uleb128 .L10-.LFB1119
 224 000c 00       		.uleb128 0
 225 000d 8901     		.uleb128 .LEHB2-.LFB1119
 226 000f 1E       		.uleb128 .LEHE2-.LEHB2
 227 0010 A202     		.uleb128 .L11-.LFB1119
 228 0012 00       		.uleb128 0
 229 0013 B101     		.uleb128 .LEHB3-.LFB1119
 230 0015 1A       		.uleb128 .LEHE3-.LEHB3
 231 0016 8E02     		.uleb128 .L12-.LFB1119
 232 0018 00       		.uleb128 0
 233 0019 D501     		.uleb128 .LEHB4-.LFB1119
 234 001b 05       		.uleb128 .LEHE4-.LEHB4
 235 001c A202     		.uleb128 .L11-.LFB1119
 236 001e 00       		.uleb128 0
 237 001f E401     		.uleb128 .LEHB5-.LFB1119
 238 0021 05       		.uleb128 .LEHE5-.LEHB5
 239 0022 B602     		.uleb128 .L10-.LFB1119
 240 0024 00       		.uleb128 0
 241 0025 F301     		.uleb128 .LEHB6-.LFB1119
 242 0027 05       		.uleb128 .LEHE6-.LEHB6
 243 0028 00       		.uleb128 0
 244 0029 00       		.uleb128 0
 245 002a CE02     		.uleb128 .LEHB7-.LFB1119
 246 002c 05       		.uleb128 .LEHE7-.LEHB7
 247 002d 00       		.uleb128 0
 248 002e 00       		.uleb128 0
 249              	.LLSDACSE1119:
 250              		.text
 251              		.size	main, .-main
 252              		.type	_Z41__static_initialization_and_destruction_0ii, @function
 253              	_Z41__static_initialization_and_destruction_0ii:
 254              	.LFB1260:
 255              		.loc 2 44 0
 256              		.cfi_startproc
 257 019c 55       		pushq	%rbp
 258              		.cfi_def_cfa_offset 16
 259              		.cfi_offset 6, -16
 260 019d 4889E5   		movq	%rsp, %rbp
 261              		.cfi_def_cfa_register 6
 262 01a0 4883EC10 		subq	$16, %rsp
 263 01a4 897DFC   		movl	%edi, -4(%rbp)
 264 01a7 8975F8   		movl	%esi, -8(%rbp)
 265              		.loc 2 44 0
 266 01aa 837DFC01 		cmpl	$1, -4(%rbp)
 267 01ae 7527     		jne	.L16
 268              		.loc 2 44 0 is_stmt 0 discriminator 1
 269 01b0 817DF8FF 		cmpl	$65535, -8(%rbp)
 269      FF0000
 270 01b7 751E     		jne	.L16
 271              		.file 3 "/usr/include/c++/5/iostream"
   1:/usr/include/c++/5/iostream **** // Standard iostream objects -*- C++ -*-
GAS LISTING /tmp/cca1HRfE.s 			page 10


   2:/usr/include/c++/5/iostream **** 
   3:/usr/include/c++/5/iostream **** // Copyright (C) 1997-2015 Free Software Foundation, Inc.
   4:/usr/include/c++/5/iostream **** //
   5:/usr/include/c++/5/iostream **** // This file is part of the GNU ISO C++ Library.  This library is free
   6:/usr/include/c++/5/iostream **** // software; you can redistribute it and/or modify it under the
   7:/usr/include/c++/5/iostream **** // terms of the GNU General Public License as published by the
   8:/usr/include/c++/5/iostream **** // Free Software Foundation; either version 3, or (at your option)
   9:/usr/include/c++/5/iostream **** // any later version.
  10:/usr/include/c++/5/iostream **** 
  11:/usr/include/c++/5/iostream **** // This library is distributed in the hope that it will be useful,
  12:/usr/include/c++/5/iostream **** // but WITHOUT ANY WARRANTY; without even the implied warranty of
  13:/usr/include/c++/5/iostream **** // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  14:/usr/include/c++/5/iostream **** // GNU General Public License for more details.
  15:/usr/include/c++/5/iostream **** 
  16:/usr/include/c++/5/iostream **** // Under Section 7 of GPL version 3, you are granted additional
  17:/usr/include/c++/5/iostream **** // permissions described in the GCC Runtime Library Exception, version
  18:/usr/include/c++/5/iostream **** // 3.1, as published by the Free Software Foundation.
  19:/usr/include/c++/5/iostream **** 
  20:/usr/include/c++/5/iostream **** // You should have received a copy of the GNU General Public License and
  21:/usr/include/c++/5/iostream **** // a copy of the GCC Runtime Library Exception along with this program;
  22:/usr/include/c++/5/iostream **** // see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
  23:/usr/include/c++/5/iostream **** // <http://www.gnu.org/licenses/>.
  24:/usr/include/c++/5/iostream **** 
  25:/usr/include/c++/5/iostream **** /** @file include/iostream
  26:/usr/include/c++/5/iostream ****  *  This is a Standard C++ Library header.
  27:/usr/include/c++/5/iostream ****  */
  28:/usr/include/c++/5/iostream **** 
  29:/usr/include/c++/5/iostream **** //
  30:/usr/include/c++/5/iostream **** // ISO C++ 14882: 27.3  Standard iostream objects
  31:/usr/include/c++/5/iostream **** //
  32:/usr/include/c++/5/iostream **** 
  33:/usr/include/c++/5/iostream **** #ifndef _GLIBCXX_IOSTREAM
  34:/usr/include/c++/5/iostream **** #define _GLIBCXX_IOSTREAM 1
  35:/usr/include/c++/5/iostream **** 
  36:/usr/include/c++/5/iostream **** #pragma GCC system_header
  37:/usr/include/c++/5/iostream **** 
  38:/usr/include/c++/5/iostream **** #include <bits/c++config.h>
  39:/usr/include/c++/5/iostream **** #include <ostream>
  40:/usr/include/c++/5/iostream **** #include <istream>
  41:/usr/include/c++/5/iostream **** 
  42:/usr/include/c++/5/iostream **** namespace std _GLIBCXX_VISIBILITY(default)
  43:/usr/include/c++/5/iostream **** {
  44:/usr/include/c++/5/iostream **** _GLIBCXX_BEGIN_NAMESPACE_VERSION
  45:/usr/include/c++/5/iostream **** 
  46:/usr/include/c++/5/iostream ****   /**
  47:/usr/include/c++/5/iostream ****    *  @name Standard Stream Objects
  48:/usr/include/c++/5/iostream ****    *
  49:/usr/include/c++/5/iostream ****    *  The &lt;iostream&gt; header declares the eight <em>standard stream
  50:/usr/include/c++/5/iostream ****    *  objects</em>.  For other declarations, see
  51:/usr/include/c++/5/iostream ****    *  http://gcc.gnu.org/onlinedocs/libstdc++/manual/io.html
  52:/usr/include/c++/5/iostream ****    *  and the @link iosfwd I/O forward declarations @endlink
  53:/usr/include/c++/5/iostream ****    *
  54:/usr/include/c++/5/iostream ****    *  They are required by default to cooperate with the global C
  55:/usr/include/c++/5/iostream ****    *  library's @c FILE streams, and to be available during program
  56:/usr/include/c++/5/iostream ****    *  startup and termination. For more information, see the section of the
  57:/usr/include/c++/5/iostream ****    *  manual linked to above.
  58:/usr/include/c++/5/iostream ****   */
GAS LISTING /tmp/cca1HRfE.s 			page 11


  59:/usr/include/c++/5/iostream ****   //@{
  60:/usr/include/c++/5/iostream ****   extern istream cin;		/// Linked to standard input
  61:/usr/include/c++/5/iostream ****   extern ostream cout;		/// Linked to standard output
  62:/usr/include/c++/5/iostream ****   extern ostream cerr;		/// Linked to standard error (unbuffered)
  63:/usr/include/c++/5/iostream ****   extern ostream clog;		/// Linked to standard error (buffered)
  64:/usr/include/c++/5/iostream **** 
  65:/usr/include/c++/5/iostream **** #ifdef _GLIBCXX_USE_WCHAR_T
  66:/usr/include/c++/5/iostream ****   extern wistream wcin;		/// Linked to standard input
  67:/usr/include/c++/5/iostream ****   extern wostream wcout;	/// Linked to standard output
  68:/usr/include/c++/5/iostream ****   extern wostream wcerr;	/// Linked to standard error (unbuffered)
  69:/usr/include/c++/5/iostream ****   extern wostream wclog;	/// Linked to standard error (buffered)
  70:/usr/include/c++/5/iostream **** #endif
  71:/usr/include/c++/5/iostream ****   //@}
  72:/usr/include/c++/5/iostream **** 
  73:/usr/include/c++/5/iostream ****   // For construction of filebuffers for cout, cin, cerr, clog et. al.
  74:/usr/include/c++/5/iostream ****   static ios_base::Init __ioinit;
 272              		.loc 3 74 0 is_stmt 1
 273 01b9 BF000000 		movl	$_ZStL8__ioinit, %edi
 273      00
 274 01be E8000000 		call	_ZNSt8ios_base4InitC1Ev
 274      00
 275 01c3 BA000000 		movl	$__dso_handle, %edx
 275      00
 276 01c8 BE000000 		movl	$_ZStL8__ioinit, %esi
 276      00
 277 01cd BF000000 		movl	$_ZNSt8ios_base4InitD1Ev, %edi
 277      00
 278 01d2 E8000000 		call	__cxa_atexit
 278      00
 279              	.L16:
 280              		.loc 2 44 0
 281 01d7 90       		nop
 282 01d8 C9       		leave
 283              		.cfi_def_cfa 7, 8
 284 01d9 C3       		ret
 285              		.cfi_endproc
 286              	.LFE1260:
 287              		.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destructi
 288              		.type	_GLOBAL__sub_I__Z15write_somethingRSo, @function
 289              	_GLOBAL__sub_I__Z15write_somethingRSo:
 290              	.LFB1261:
 291              		.loc 2 44 0
 292              		.cfi_startproc
 293 01da 55       		pushq	%rbp
 294              		.cfi_def_cfa_offset 16
 295              		.cfi_offset 6, -16
 296 01db 4889E5   		movq	%rsp, %rbp
 297              		.cfi_def_cfa_register 6
 298              		.loc 2 44 0
 299 01de BEFFFF00 		movl	$65535, %esi
 299      00
 300 01e3 BF010000 		movl	$1, %edi
 300      00
 301 01e8 E8AFFFFF 		call	_Z41__static_initialization_and_destruction_0ii
 301      FF
 302 01ed 5D       		popq	%rbp
 303              		.cfi_def_cfa 7, 8
GAS LISTING /tmp/cca1HRfE.s 			page 12


 304 01ee C3       		ret
 305              		.cfi_endproc
 306              	.LFE1261:
 307              		.size	_GLOBAL__sub_I__Z15write_somethingRSo, .-_GLOBAL__sub_I__Z15write_somethingRSo
 308              		.section	.init_array,"aw"
 309              		.align 8
 310 0000 00000000 		.quad	_GLOBAL__sub_I__Z15write_somethingRSo
 310      00000000 
 311              		.text
 312              	.Letext0:
 313              		.file 4 "/usr/include/c++/5/cwchar"
 314              		.file 5 "/usr/include/c++/5/bits/char_traits.h"
 315              		.file 6 "/usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h"
 316              		.file 7 "/usr/include/c++/5/clocale"
 317              		.file 8 "/usr/include/c++/5/bits/allocator.h"
 318              		.file 9 "/usr/include/c++/5/cwctype"
 319              		.file 10 "/usr/include/c++/5/cstdio"
 320              		.file 11 "/usr/include/c++/5/iosfwd"
 321              		.file 12 "/usr/include/c++/5/debug/debug.h"
 322              		.file 13 "/usr/include/c++/5/bits/predefined_ops.h"
 323              		.file 14 "/usr/include/c++/5/ext/new_allocator.h"
 324              		.file 15 "/usr/include/c++/5/ext/numeric_traits.h"
 325              		.file 16 "/usr/include/stdio.h"
 326              		.file 17 "/usr/include/libio.h"
 327              		.file 18 "<built-in>"
 328              		.file 19 "/usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h"
 329              		.file 20 "/usr/include/wchar.h"
 330              		.file 21 "/usr/include/time.h"
 331              		.file 22 "/usr/include/locale.h"
 332              		.file 23 "/usr/include/x86_64-linux-gnu/bits/types.h"
 333              		.file 24 "/usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h"
 334              		.file 25 "/usr/include/wctype.h"
 335              		.file 26 "/usr/include/_G_config.h"
 336              		.section	.debug_info,"",@progbits
 337              	.Ldebug_info0:
 338 0000 871E0000 		.long	0x1e87
 339 0004 0400     		.value	0x4
 340 0006 00000000 		.long	.Ldebug_abbrev0
 341 000a 08       		.byte	0x8
 342 000b 01       		.uleb128 0x1
 343 000c 00000000 		.long	.LASF376
 344 0010 04       		.byte	0x4
 345 0011 00000000 		.long	.LASF377
 346 0015 00000000 		.long	.LASF378
 347 0019 00000000 		.long	.Ldebug_ranges0+0
 348 001d 00000000 		.quad	0
 348      00000000 
 349 0025 00000000 		.long	.Ldebug_line0
 350 0029 02       		.uleb128 0x2
 351 002a 73746400 		.string	"std"
 352 002e 12       		.byte	0x12
 353 002f 00       		.byte	0
 354 0030 D0090000 		.long	0x9d0
 355 0034 03       		.uleb128 0x3
 356 0035 00000000 		.long	.LASF0
 357 0039 06       		.byte	0x6
 358 003a DA       		.byte	0xda
GAS LISTING /tmp/cca1HRfE.s 			page 13


 359 003b 61000000 		.long	0x61
 360 003f 04       		.uleb128 0x4
 361 0040 00000000 		.long	.LASF379
 362 0044 05       		.uleb128 0x5
 363 0045 00000000 		.long	.LASF109
 364 0049 500F0000 		.long	0xf50
 365 004d 06       		.uleb128 0x6
 366 004e 00000000 		.long	.LASF1
 367 0052 1D020000 		.long	0x21d
 368 0056 06       		.uleb128 0x6
 369 0057 00000000 		.long	.LASF2
 370 005b 10040000 		.long	0x410
 371 005f 00       		.byte	0
 372 0060 00       		.byte	0
 373 0061 07       		.uleb128 0x7
 374 0062 06       		.byte	0x6
 375 0063 DA       		.byte	0xda
 376 0064 34000000 		.long	0x34
 377 0068 08       		.uleb128 0x8
 378 0069 04       		.byte	0x4
 379 006a 40       		.byte	0x40
 380 006b 690F0000 		.long	0xf69
 381 006f 08       		.uleb128 0x8
 382 0070 04       		.byte	0x4
 383 0071 8B       		.byte	0x8b
 384 0072 F00E0000 		.long	0xef0
 385 0076 08       		.uleb128 0x8
 386 0077 04       		.byte	0x4
 387 0078 8D       		.byte	0x8d
 388 0079 8B0F0000 		.long	0xf8b
 389 007d 08       		.uleb128 0x8
 390 007e 04       		.byte	0x4
 391 007f 8E       		.byte	0x8e
 392 0080 A10F0000 		.long	0xfa1
 393 0084 08       		.uleb128 0x8
 394 0085 04       		.byte	0x4
 395 0086 8F       		.byte	0x8f
 396 0087 BD0F0000 		.long	0xfbd
 397 008b 08       		.uleb128 0x8
 398 008c 04       		.byte	0x4
 399 008d 90       		.byte	0x90
 400 008e EA0F0000 		.long	0xfea
 401 0092 08       		.uleb128 0x8
 402 0093 04       		.byte	0x4
 403 0094 91       		.byte	0x91
 404 0095 05100000 		.long	0x1005
 405 0099 08       		.uleb128 0x8
 406 009a 04       		.byte	0x4
 407 009b 92       		.byte	0x92
 408 009c 2B100000 		.long	0x102b
 409 00a0 08       		.uleb128 0x8
 410 00a1 04       		.byte	0x4
 411 00a2 93       		.byte	0x93
 412 00a3 46100000 		.long	0x1046
 413 00a7 08       		.uleb128 0x8
 414 00a8 04       		.byte	0x4
 415 00a9 94       		.byte	0x94
GAS LISTING /tmp/cca1HRfE.s 			page 14


 416 00aa 62100000 		.long	0x1062
 417 00ae 08       		.uleb128 0x8
 418 00af 04       		.byte	0x4
 419 00b0 95       		.byte	0x95
 420 00b1 7E100000 		.long	0x107e
 421 00b5 08       		.uleb128 0x8
 422 00b6 04       		.byte	0x4
 423 00b7 96       		.byte	0x96
 424 00b8 94100000 		.long	0x1094
 425 00bc 08       		.uleb128 0x8
 426 00bd 04       		.byte	0x4
 427 00be 97       		.byte	0x97
 428 00bf A0100000 		.long	0x10a0
 429 00c3 08       		.uleb128 0x8
 430 00c4 04       		.byte	0x4
 431 00c5 98       		.byte	0x98
 432 00c6 C6100000 		.long	0x10c6
 433 00ca 08       		.uleb128 0x8
 434 00cb 04       		.byte	0x4
 435 00cc 99       		.byte	0x99
 436 00cd EB100000 		.long	0x10eb
 437 00d1 08       		.uleb128 0x8
 438 00d2 04       		.byte	0x4
 439 00d3 9A       		.byte	0x9a
 440 00d4 0C110000 		.long	0x110c
 441 00d8 08       		.uleb128 0x8
 442 00d9 04       		.byte	0x4
 443 00da 9B       		.byte	0x9b
 444 00db 37110000 		.long	0x1137
 445 00df 08       		.uleb128 0x8
 446 00e0 04       		.byte	0x4
 447 00e1 9C       		.byte	0x9c
 448 00e2 52110000 		.long	0x1152
 449 00e6 08       		.uleb128 0x8
 450 00e7 04       		.byte	0x4
 451 00e8 9E       		.byte	0x9e
 452 00e9 68110000 		.long	0x1168
 453 00ed 08       		.uleb128 0x8
 454 00ee 04       		.byte	0x4
 455 00ef A0       		.byte	0xa0
 456 00f0 89110000 		.long	0x1189
 457 00f4 08       		.uleb128 0x8
 458 00f5 04       		.byte	0x4
 459 00f6 A1       		.byte	0xa1
 460 00f7 A5110000 		.long	0x11a5
 461 00fb 08       		.uleb128 0x8
 462 00fc 04       		.byte	0x4
 463 00fd A2       		.byte	0xa2
 464 00fe C0110000 		.long	0x11c0
 465 0102 08       		.uleb128 0x8
 466 0103 04       		.byte	0x4
 467 0104 A4       		.byte	0xa4
 468 0105 E6110000 		.long	0x11e6
 469 0109 08       		.uleb128 0x8
 470 010a 04       		.byte	0x4
 471 010b A7       		.byte	0xa7
 472 010c 06120000 		.long	0x1206
GAS LISTING /tmp/cca1HRfE.s 			page 15


 473 0110 08       		.uleb128 0x8
 474 0111 04       		.byte	0x4
 475 0112 AA       		.byte	0xaa
 476 0113 2B120000 		.long	0x122b
 477 0117 08       		.uleb128 0x8
 478 0118 04       		.byte	0x4
 479 0119 AC       		.byte	0xac
 480 011a 4B120000 		.long	0x124b
 481 011e 08       		.uleb128 0x8
 482 011f 04       		.byte	0x4
 483 0120 AE       		.byte	0xae
 484 0121 66120000 		.long	0x1266
 485 0125 08       		.uleb128 0x8
 486 0126 04       		.byte	0x4
 487 0127 B0       		.byte	0xb0
 488 0128 81120000 		.long	0x1281
 489 012c 08       		.uleb128 0x8
 490 012d 04       		.byte	0x4
 491 012e B1       		.byte	0xb1
 492 012f A7120000 		.long	0x12a7
 493 0133 08       		.uleb128 0x8
 494 0134 04       		.byte	0x4
 495 0135 B2       		.byte	0xb2
 496 0136 C1120000 		.long	0x12c1
 497 013a 08       		.uleb128 0x8
 498 013b 04       		.byte	0x4
 499 013c B3       		.byte	0xb3
 500 013d DB120000 		.long	0x12db
 501 0141 08       		.uleb128 0x8
 502 0142 04       		.byte	0x4
 503 0143 B4       		.byte	0xb4
 504 0144 F5120000 		.long	0x12f5
 505 0148 08       		.uleb128 0x8
 506 0149 04       		.byte	0x4
 507 014a B5       		.byte	0xb5
 508 014b 0F130000 		.long	0x130f
 509 014f 08       		.uleb128 0x8
 510 0150 04       		.byte	0x4
 511 0151 B6       		.byte	0xb6
 512 0152 29130000 		.long	0x1329
 513 0156 08       		.uleb128 0x8
 514 0157 04       		.byte	0x4
 515 0158 B7       		.byte	0xb7
 516 0159 E9130000 		.long	0x13e9
 517 015d 08       		.uleb128 0x8
 518 015e 04       		.byte	0x4
 519 015f B8       		.byte	0xb8
 520 0160 FF130000 		.long	0x13ff
 521 0164 08       		.uleb128 0x8
 522 0165 04       		.byte	0x4
 523 0166 B9       		.byte	0xb9
 524 0167 1E140000 		.long	0x141e
 525 016b 08       		.uleb128 0x8
 526 016c 04       		.byte	0x4
 527 016d BA       		.byte	0xba
 528 016e 3D140000 		.long	0x143d
 529 0172 08       		.uleb128 0x8
GAS LISTING /tmp/cca1HRfE.s 			page 16


 530 0173 04       		.byte	0x4
 531 0174 BB       		.byte	0xbb
 532 0175 5C140000 		.long	0x145c
 533 0179 08       		.uleb128 0x8
 534 017a 04       		.byte	0x4
 535 017b BC       		.byte	0xbc
 536 017c 87140000 		.long	0x1487
 537 0180 08       		.uleb128 0x8
 538 0181 04       		.byte	0x4
 539 0182 BD       		.byte	0xbd
 540 0183 A2140000 		.long	0x14a2
 541 0187 08       		.uleb128 0x8
 542 0188 04       		.byte	0x4
 543 0189 BF       		.byte	0xbf
 544 018a CA140000 		.long	0x14ca
 545 018e 08       		.uleb128 0x8
 546 018f 04       		.byte	0x4
 547 0190 C1       		.byte	0xc1
 548 0191 EC140000 		.long	0x14ec
 549 0195 08       		.uleb128 0x8
 550 0196 04       		.byte	0x4
 551 0197 C2       		.byte	0xc2
 552 0198 0C150000 		.long	0x150c
 553 019c 08       		.uleb128 0x8
 554 019d 04       		.byte	0x4
 555 019e C3       		.byte	0xc3
 556 019f 33150000 		.long	0x1533
 557 01a3 08       		.uleb128 0x8
 558 01a4 04       		.byte	0x4
 559 01a5 C4       		.byte	0xc4
 560 01a6 53150000 		.long	0x1553
 561 01aa 08       		.uleb128 0x8
 562 01ab 04       		.byte	0x4
 563 01ac C5       		.byte	0xc5
 564 01ad 72150000 		.long	0x1572
 565 01b1 08       		.uleb128 0x8
 566 01b2 04       		.byte	0x4
 567 01b3 C6       		.byte	0xc6
 568 01b4 88150000 		.long	0x1588
 569 01b8 08       		.uleb128 0x8
 570 01b9 04       		.byte	0x4
 571 01ba C7       		.byte	0xc7
 572 01bb A8150000 		.long	0x15a8
 573 01bf 08       		.uleb128 0x8
 574 01c0 04       		.byte	0x4
 575 01c1 C8       		.byte	0xc8
 576 01c2 C8150000 		.long	0x15c8
 577 01c6 08       		.uleb128 0x8
 578 01c7 04       		.byte	0x4
 579 01c8 C9       		.byte	0xc9
 580 01c9 E8150000 		.long	0x15e8
 581 01cd 08       		.uleb128 0x8
 582 01ce 04       		.byte	0x4
 583 01cf CA       		.byte	0xca
 584 01d0 08160000 		.long	0x1608
 585 01d4 08       		.uleb128 0x8
 586 01d5 04       		.byte	0x4
GAS LISTING /tmp/cca1HRfE.s 			page 17


 587 01d6 CB       		.byte	0xcb
 588 01d7 1F160000 		.long	0x161f
 589 01db 08       		.uleb128 0x8
 590 01dc 04       		.byte	0x4
 591 01dd CC       		.byte	0xcc
 592 01de 36160000 		.long	0x1636
 593 01e2 08       		.uleb128 0x8
 594 01e3 04       		.byte	0x4
 595 01e4 CD       		.byte	0xcd
 596 01e5 54160000 		.long	0x1654
 597 01e9 08       		.uleb128 0x8
 598 01ea 04       		.byte	0x4
 599 01eb CE       		.byte	0xce
 600 01ec 73160000 		.long	0x1673
 601 01f0 08       		.uleb128 0x8
 602 01f1 04       		.byte	0x4
 603 01f2 CF       		.byte	0xcf
 604 01f3 91160000 		.long	0x1691
 605 01f7 08       		.uleb128 0x8
 606 01f8 04       		.byte	0x4
 607 01f9 D0       		.byte	0xd0
 608 01fa B0160000 		.long	0x16b0
 609 01fe 09       		.uleb128 0x9
 610 01ff 04       		.byte	0x4
 611 0200 0801     		.value	0x108
 612 0202 D4160000 		.long	0x16d4
 613 0206 09       		.uleb128 0x9
 614 0207 04       		.byte	0x4
 615 0208 0901     		.value	0x109
 616 020a F6160000 		.long	0x16f6
 617 020e 09       		.uleb128 0x9
 618 020f 04       		.byte	0x4
 619 0210 0A01     		.value	0x10a
 620 0212 1D170000 		.long	0x171d
 621 0216 0A       		.uleb128 0xa
 622 0217 00000000 		.long	.LASF118
 623 021b 0C       		.byte	0xc
 624 021c 30       		.byte	0x30
 625 021d 0B       		.uleb128 0xb
 626 021e 00000000 		.long	.LASF145
 627 0222 01       		.byte	0x1
 628 0223 05       		.byte	0x5
 629 0224 E9       		.byte	0xe9
 630 0225 E5030000 		.long	0x3e5
 631 0229 0C       		.uleb128 0xc
 632 022a 00000000 		.long	.LASF3
 633 022e 05       		.byte	0x5
 634 022f EB       		.byte	0xeb
 635 0230 500F0000 		.long	0xf50
 636 0234 0C       		.uleb128 0xc
 637 0235 00000000 		.long	.LASF4
 638 0239 05       		.byte	0x5
 639 023a EC       		.byte	0xec
 640 023b 570F0000 		.long	0xf57
 641 023f 0D       		.uleb128 0xd
 642 0240 00000000 		.long	.LASF17
 643 0244 05       		.byte	0x5
GAS LISTING /tmp/cca1HRfE.s 			page 18


 644 0245 F2       		.byte	0xf2
 645 0246 00000000 		.long	.LASF380
 646 024a 59020000 		.long	0x259
 647 024e 0E       		.uleb128 0xe
 648 024f 6C170000 		.long	0x176c
 649 0253 0E       		.uleb128 0xe
 650 0254 72170000 		.long	0x1772
 651 0258 00       		.byte	0
 652 0259 0F       		.uleb128 0xf
 653 025a 29020000 		.long	0x229
 654 025e 10       		.uleb128 0x10
 655 025f 657100   		.string	"eq"
 656 0262 05       		.byte	0x5
 657 0263 F6       		.byte	0xf6
 658 0264 00000000 		.long	.LASF5
 659 0268 78170000 		.long	0x1778
 660 026c 7B020000 		.long	0x27b
 661 0270 0E       		.uleb128 0xe
 662 0271 72170000 		.long	0x1772
 663 0275 0E       		.uleb128 0xe
 664 0276 72170000 		.long	0x1772
 665 027a 00       		.byte	0
 666 027b 10       		.uleb128 0x10
 667 027c 6C7400   		.string	"lt"
 668 027f 05       		.byte	0x5
 669 0280 FA       		.byte	0xfa
 670 0281 00000000 		.long	.LASF6
 671 0285 78170000 		.long	0x1778
 672 0289 98020000 		.long	0x298
 673 028d 0E       		.uleb128 0xe
 674 028e 72170000 		.long	0x1772
 675 0292 0E       		.uleb128 0xe
 676 0293 72170000 		.long	0x1772
 677 0297 00       		.byte	0
 678 0298 11       		.uleb128 0x11
 679 0299 00000000 		.long	.LASF7
 680 029d 05       		.byte	0x5
 681 029e 0201     		.value	0x102
 682 02a0 00000000 		.long	.LASF9
 683 02a4 570F0000 		.long	0xf57
 684 02a8 BC020000 		.long	0x2bc
 685 02ac 0E       		.uleb128 0xe
 686 02ad 7F170000 		.long	0x177f
 687 02b1 0E       		.uleb128 0xe
 688 02b2 7F170000 		.long	0x177f
 689 02b6 0E       		.uleb128 0xe
 690 02b7 E5030000 		.long	0x3e5
 691 02bb 00       		.byte	0
 692 02bc 11       		.uleb128 0x11
 693 02bd 00000000 		.long	.LASF8
 694 02c1 05       		.byte	0x5
 695 02c2 0A01     		.value	0x10a
 696 02c4 00000000 		.long	.LASF10
 697 02c8 E5030000 		.long	0x3e5
 698 02cc D6020000 		.long	0x2d6
 699 02d0 0E       		.uleb128 0xe
 700 02d1 7F170000 		.long	0x177f
GAS LISTING /tmp/cca1HRfE.s 			page 19


 701 02d5 00       		.byte	0
 702 02d6 11       		.uleb128 0x11
 703 02d7 00000000 		.long	.LASF11
 704 02db 05       		.byte	0x5
 705 02dc 0E01     		.value	0x10e
 706 02de 00000000 		.long	.LASF12
 707 02e2 7F170000 		.long	0x177f
 708 02e6 FA020000 		.long	0x2fa
 709 02ea 0E       		.uleb128 0xe
 710 02eb 7F170000 		.long	0x177f
 711 02ef 0E       		.uleb128 0xe
 712 02f0 E5030000 		.long	0x3e5
 713 02f4 0E       		.uleb128 0xe
 714 02f5 72170000 		.long	0x1772
 715 02f9 00       		.byte	0
 716 02fa 11       		.uleb128 0x11
 717 02fb 00000000 		.long	.LASF13
 718 02ff 05       		.byte	0x5
 719 0300 1601     		.value	0x116
 720 0302 00000000 		.long	.LASF14
 721 0306 85170000 		.long	0x1785
 722 030a 1E030000 		.long	0x31e
 723 030e 0E       		.uleb128 0xe
 724 030f 85170000 		.long	0x1785
 725 0313 0E       		.uleb128 0xe
 726 0314 7F170000 		.long	0x177f
 727 0318 0E       		.uleb128 0xe
 728 0319 E5030000 		.long	0x3e5
 729 031d 00       		.byte	0
 730 031e 11       		.uleb128 0x11
 731 031f 00000000 		.long	.LASF15
 732 0323 05       		.byte	0x5
 733 0324 1E01     		.value	0x11e
 734 0326 00000000 		.long	.LASF16
 735 032a 85170000 		.long	0x1785
 736 032e 42030000 		.long	0x342
 737 0332 0E       		.uleb128 0xe
 738 0333 85170000 		.long	0x1785
 739 0337 0E       		.uleb128 0xe
 740 0338 7F170000 		.long	0x177f
 741 033c 0E       		.uleb128 0xe
 742 033d E5030000 		.long	0x3e5
 743 0341 00       		.byte	0
 744 0342 11       		.uleb128 0x11
 745 0343 00000000 		.long	.LASF17
 746 0347 05       		.byte	0x5
 747 0348 2601     		.value	0x126
 748 034a 00000000 		.long	.LASF18
 749 034e 85170000 		.long	0x1785
 750 0352 66030000 		.long	0x366
 751 0356 0E       		.uleb128 0xe
 752 0357 85170000 		.long	0x1785
 753 035b 0E       		.uleb128 0xe
 754 035c E5030000 		.long	0x3e5
 755 0360 0E       		.uleb128 0xe
 756 0361 29020000 		.long	0x229
 757 0365 00       		.byte	0
GAS LISTING /tmp/cca1HRfE.s 			page 20


 758 0366 11       		.uleb128 0x11
 759 0367 00000000 		.long	.LASF19
 760 036b 05       		.byte	0x5
 761 036c 2E01     		.value	0x12e
 762 036e 00000000 		.long	.LASF20
 763 0372 29020000 		.long	0x229
 764 0376 80030000 		.long	0x380
 765 037a 0E       		.uleb128 0xe
 766 037b 8B170000 		.long	0x178b
 767 037f 00       		.byte	0
 768 0380 0F       		.uleb128 0xf
 769 0381 34020000 		.long	0x234
 770 0385 11       		.uleb128 0x11
 771 0386 00000000 		.long	.LASF21
 772 038a 05       		.byte	0x5
 773 038b 3401     		.value	0x134
 774 038d 00000000 		.long	.LASF22
 775 0391 34020000 		.long	0x234
 776 0395 9F030000 		.long	0x39f
 777 0399 0E       		.uleb128 0xe
 778 039a 72170000 		.long	0x1772
 779 039e 00       		.byte	0
 780 039f 11       		.uleb128 0x11
 781 03a0 00000000 		.long	.LASF23
 782 03a4 05       		.byte	0x5
 783 03a5 3801     		.value	0x138
 784 03a7 00000000 		.long	.LASF24
 785 03ab 78170000 		.long	0x1778
 786 03af BE030000 		.long	0x3be
 787 03b3 0E       		.uleb128 0xe
 788 03b4 8B170000 		.long	0x178b
 789 03b8 0E       		.uleb128 0xe
 790 03b9 8B170000 		.long	0x178b
 791 03bd 00       		.byte	0
 792 03be 12       		.uleb128 0x12
 793 03bf 656F6600 		.string	"eof"
 794 03c3 05       		.byte	0x5
 795 03c4 3C01     		.value	0x13c
 796 03c6 00000000 		.long	.LASF381
 797 03ca 34020000 		.long	0x234
 798 03ce 13       		.uleb128 0x13
 799 03cf 00000000 		.long	.LASF25
 800 03d3 05       		.byte	0x5
 801 03d4 4001     		.value	0x140
 802 03d6 00000000 		.long	.LASF382
 803 03da 34020000 		.long	0x234
 804 03de 0E       		.uleb128 0xe
 805 03df 8B170000 		.long	0x178b
 806 03e3 00       		.byte	0
 807 03e4 00       		.byte	0
 808 03e5 0C       		.uleb128 0xc
 809 03e6 00000000 		.long	.LASF26
 810 03ea 06       		.byte	0x6
 811 03eb C4       		.byte	0xc4
 812 03ec E90E0000 		.long	0xee9
 813 03f0 08       		.uleb128 0x8
 814 03f1 07       		.byte	0x7
GAS LISTING /tmp/cca1HRfE.s 			page 21


 815 03f2 35       		.byte	0x35
 816 03f3 91170000 		.long	0x1791
 817 03f7 08       		.uleb128 0x8
 818 03f8 07       		.byte	0x7
 819 03f9 36       		.byte	0x36
 820 03fa BE180000 		.long	0x18be
 821 03fe 08       		.uleb128 0x8
 822 03ff 07       		.byte	0x7
 823 0400 37       		.byte	0x37
 824 0401 D8180000 		.long	0x18d8
 825 0405 0C       		.uleb128 0xc
 826 0406 00000000 		.long	.LASF27
 827 040a 06       		.byte	0x6
 828 040b C5       		.byte	0xc5
 829 040c 2C150000 		.long	0x152c
 830 0410 14       		.uleb128 0x14
 831 0411 00000000 		.long	.LASF120
 832 0415 01       		.byte	0x1
 833 0416 08       		.byte	0x8
 834 0417 5C       		.byte	0x5c
 835 0418 78040000 		.long	0x478
 836 041c 15       		.uleb128 0x15
 837 041d 150A0000 		.long	0xa15
 838 0421 00       		.byte	0
 839 0422 01       		.byte	0x1
 840 0423 16       		.uleb128 0x16
 841 0424 00000000 		.long	.LASF28
 842 0428 08       		.byte	0x8
 843 0429 71       		.byte	0x71
 844 042a 00000000 		.long	.LASF29
 845 042e 01       		.byte	0x1
 846 042f 37040000 		.long	0x437
 847 0433 3D040000 		.long	0x43d
 848 0437 17       		.uleb128 0x17
 849 0438 3A190000 		.long	0x193a
 850 043c 00       		.byte	0
 851 043d 16       		.uleb128 0x16
 852 043e 00000000 		.long	.LASF28
 853 0442 08       		.byte	0x8
 854 0443 73       		.byte	0x73
 855 0444 00000000 		.long	.LASF30
 856 0448 01       		.byte	0x1
 857 0449 51040000 		.long	0x451
 858 044d 5C040000 		.long	0x45c
 859 0451 17       		.uleb128 0x17
 860 0452 3A190000 		.long	0x193a
 861 0456 0E       		.uleb128 0xe
 862 0457 40190000 		.long	0x1940
 863 045b 00       		.byte	0
 864 045c 18       		.uleb128 0x18
 865 045d 00000000 		.long	.LASF31
 866 0461 08       		.byte	0x8
 867 0462 79       		.byte	0x79
 868 0463 00000000 		.long	.LASF111
 869 0467 01       		.byte	0x1
 870 0468 6C040000 		.long	0x46c
 871 046c 17       		.uleb128 0x17
GAS LISTING /tmp/cca1HRfE.s 			page 22


 872 046d 3A190000 		.long	0x193a
 873 0471 17       		.uleb128 0x17
 874 0472 570F0000 		.long	0xf57
 875 0476 00       		.byte	0
 876 0477 00       		.byte	0
 877 0478 0F       		.uleb128 0xf
 878 0479 10040000 		.long	0x410
 879 047d 19       		.uleb128 0x19
 880 047e 00000000 		.long	.LASF53
 881 0482 04       		.byte	0x4
 882 0483 570F0000 		.long	0xf57
 883 0487 01       		.byte	0x1
 884 0488 39       		.byte	0x39
 885 0489 1E050000 		.long	0x51e
 886 048d 1A       		.uleb128 0x1a
 887 048e 00000000 		.long	.LASF32
 888 0492 01       		.byte	0x1
 889 0493 1A       		.uleb128 0x1a
 890 0494 00000000 		.long	.LASF33
 891 0498 02       		.byte	0x2
 892 0499 1A       		.uleb128 0x1a
 893 049a 00000000 		.long	.LASF34
 894 049e 04       		.byte	0x4
 895 049f 1A       		.uleb128 0x1a
 896 04a0 00000000 		.long	.LASF35
 897 04a4 08       		.byte	0x8
 898 04a5 1A       		.uleb128 0x1a
 899 04a6 00000000 		.long	.LASF36
 900 04aa 10       		.byte	0x10
 901 04ab 1A       		.uleb128 0x1a
 902 04ac 00000000 		.long	.LASF37
 903 04b0 20       		.byte	0x20
 904 04b1 1A       		.uleb128 0x1a
 905 04b2 00000000 		.long	.LASF38
 906 04b6 40       		.byte	0x40
 907 04b7 1A       		.uleb128 0x1a
 908 04b8 00000000 		.long	.LASF39
 909 04bc 80       		.byte	0x80
 910 04bd 1B       		.uleb128 0x1b
 911 04be 00000000 		.long	.LASF40
 912 04c2 0001     		.value	0x100
 913 04c4 1B       		.uleb128 0x1b
 914 04c5 00000000 		.long	.LASF41
 915 04c9 0002     		.value	0x200
 916 04cb 1B       		.uleb128 0x1b
 917 04cc 00000000 		.long	.LASF42
 918 04d0 0004     		.value	0x400
 919 04d2 1B       		.uleb128 0x1b
 920 04d3 00000000 		.long	.LASF43
 921 04d7 0008     		.value	0x800
 922 04d9 1B       		.uleb128 0x1b
 923 04da 00000000 		.long	.LASF44
 924 04de 0010     		.value	0x1000
 925 04e0 1B       		.uleb128 0x1b
 926 04e1 00000000 		.long	.LASF45
 927 04e5 0020     		.value	0x2000
 928 04e7 1B       		.uleb128 0x1b
GAS LISTING /tmp/cca1HRfE.s 			page 23


 929 04e8 00000000 		.long	.LASF46
 930 04ec 0040     		.value	0x4000
 931 04ee 1A       		.uleb128 0x1a
 932 04ef 00000000 		.long	.LASF47
 933 04f3 B0       		.byte	0xb0
 934 04f4 1A       		.uleb128 0x1a
 935 04f5 00000000 		.long	.LASF48
 936 04f9 4A       		.byte	0x4a
 937 04fa 1B       		.uleb128 0x1b
 938 04fb 00000000 		.long	.LASF49
 939 04ff 0401     		.value	0x104
 940 0501 1C       		.uleb128 0x1c
 941 0502 00000000 		.long	.LASF50
 942 0506 00000100 		.long	0x10000
 943 050a 1C       		.uleb128 0x1c
 944 050b 00000000 		.long	.LASF51
 945 050f FFFFFF7F 		.long	0x7fffffff
 946 0513 1D       		.uleb128 0x1d
 947 0514 00000000 		.long	.LASF52
 948 0518 80808080 		.sleb128 -2147483648
 948      78
 949 051d 00       		.byte	0
 950 051e 19       		.uleb128 0x19
 951 051f 00000000 		.long	.LASF54
 952 0523 04       		.byte	0x4
 953 0524 570F0000 		.long	0xf57
 954 0528 01       		.byte	0x1
 955 0529 6F       		.byte	0x6f
 956 052a 6F050000 		.long	0x56f
 957 052e 1A       		.uleb128 0x1a
 958 052f 00000000 		.long	.LASF55
 959 0533 01       		.byte	0x1
 960 0534 1A       		.uleb128 0x1a
 961 0535 00000000 		.long	.LASF56
 962 0539 02       		.byte	0x2
 963 053a 1A       		.uleb128 0x1a
 964 053b 00000000 		.long	.LASF57
 965 053f 04       		.byte	0x4
 966 0540 1A       		.uleb128 0x1a
 967 0541 00000000 		.long	.LASF58
 968 0545 08       		.byte	0x8
 969 0546 1A       		.uleb128 0x1a
 970 0547 00000000 		.long	.LASF59
 971 054b 10       		.byte	0x10
 972 054c 1A       		.uleb128 0x1a
 973 054d 00000000 		.long	.LASF60
 974 0551 20       		.byte	0x20
 975 0552 1C       		.uleb128 0x1c
 976 0553 00000000 		.long	.LASF61
 977 0557 00000100 		.long	0x10000
 978 055b 1C       		.uleb128 0x1c
 979 055c 00000000 		.long	.LASF62
 980 0560 FFFFFF7F 		.long	0x7fffffff
 981 0564 1D       		.uleb128 0x1d
 982 0565 00000000 		.long	.LASF63
 983 0569 80808080 		.sleb128 -2147483648
 983      78
GAS LISTING /tmp/cca1HRfE.s 			page 24


 984 056e 00       		.byte	0
 985 056f 19       		.uleb128 0x19
 986 0570 00000000 		.long	.LASF64
 987 0574 04       		.byte	0x4
 988 0575 570F0000 		.long	0xf57
 989 0579 01       		.byte	0x1
 990 057a 99       		.byte	0x99
 991 057b B4050000 		.long	0x5b4
 992 057f 1A       		.uleb128 0x1a
 993 0580 00000000 		.long	.LASF65
 994 0584 00       		.byte	0
 995 0585 1A       		.uleb128 0x1a
 996 0586 00000000 		.long	.LASF66
 997 058a 01       		.byte	0x1
 998 058b 1A       		.uleb128 0x1a
 999 058c 00000000 		.long	.LASF67
 1000 0590 02       		.byte	0x2
 1001 0591 1A       		.uleb128 0x1a
 1002 0592 00000000 		.long	.LASF68
 1003 0596 04       		.byte	0x4
 1004 0597 1C       		.uleb128 0x1c
 1005 0598 00000000 		.long	.LASF69
 1006 059c 00000100 		.long	0x10000
 1007 05a0 1C       		.uleb128 0x1c
 1008 05a1 00000000 		.long	.LASF70
 1009 05a5 FFFFFF7F 		.long	0x7fffffff
 1010 05a9 1D       		.uleb128 0x1d
 1011 05aa 00000000 		.long	.LASF71
 1012 05ae 80808080 		.sleb128 -2147483648
 1012      78
 1013 05b3 00       		.byte	0
 1014 05b4 19       		.uleb128 0x19
 1015 05b5 00000000 		.long	.LASF72
 1016 05b9 04       		.byte	0x4
 1017 05ba D50E0000 		.long	0xed5
 1018 05be 01       		.byte	0x1
 1019 05bf C1       		.byte	0xc1
 1020 05c0 E0050000 		.long	0x5e0
 1021 05c4 1A       		.uleb128 0x1a
 1022 05c5 00000000 		.long	.LASF73
 1023 05c9 00       		.byte	0
 1024 05ca 1A       		.uleb128 0x1a
 1025 05cb 00000000 		.long	.LASF74
 1026 05cf 01       		.byte	0x1
 1027 05d0 1A       		.uleb128 0x1a
 1028 05d1 00000000 		.long	.LASF75
 1029 05d5 02       		.byte	0x2
 1030 05d6 1C       		.uleb128 0x1c
 1031 05d7 00000000 		.long	.LASF76
 1032 05db 00000100 		.long	0x10000
 1033 05df 00       		.byte	0
 1034 05e0 1E       		.uleb128 0x1e
 1035 05e1 00000000 		.long	.LASF107
 1036 05e5 49080000 		.long	0x849
 1037 05e9 1F       		.uleb128 0x1f
 1038 05ea 00000000 		.long	.LASF79
 1039 05ee 01       		.byte	0x1
GAS LISTING /tmp/cca1HRfE.s 			page 25


 1040 05ef 01       		.byte	0x1
 1041 05f0 5902     		.value	0x259
 1042 05f2 01       		.byte	0x1
 1043 05f3 47060000 		.long	0x647
 1044 05f7 20       		.uleb128 0x20
 1045 05f8 00000000 		.long	.LASF77
 1046 05fc 01       		.byte	0x1
 1047 05fd 6102     		.value	0x261
 1048 05ff 0A190000 		.long	0x190a
 1049 0603 20       		.uleb128 0x20
 1050 0604 00000000 		.long	.LASF78
 1051 0608 01       		.byte	0x1
 1052 0609 6202     		.value	0x262
 1053 060b 78170000 		.long	0x1778
 1054 060f 21       		.uleb128 0x21
 1055 0610 00000000 		.long	.LASF79
 1056 0614 01       		.byte	0x1
 1057 0615 5D02     		.value	0x25d
 1058 0617 00000000 		.long	.LASF383
 1059 061b 01       		.byte	0x1
 1060 061c 24060000 		.long	0x624
 1061 0620 2A060000 		.long	0x62a
 1062 0624 17       		.uleb128 0x17
 1063 0625 50190000 		.long	0x1950
 1064 0629 00       		.byte	0
 1065 062a 22       		.uleb128 0x22
 1066 062b 00000000 		.long	.LASF80
 1067 062f 01       		.byte	0x1
 1068 0630 5E02     		.value	0x25e
 1069 0632 00000000 		.long	.LASF81
 1070 0636 01       		.byte	0x1
 1071 0637 3B060000 		.long	0x63b
 1072 063b 17       		.uleb128 0x17
 1073 063c 50190000 		.long	0x1950
 1074 0640 17       		.uleb128 0x17
 1075 0641 570F0000 		.long	0xf57
 1076 0645 00       		.byte	0
 1077 0646 00       		.byte	0
 1078 0647 23       		.uleb128 0x23
 1079 0648 00000000 		.long	.LASF82
 1080 064c 01       		.byte	0x1
 1081 064d AD01     		.value	0x1ad
 1082 064f 1E050000 		.long	0x51e
 1083 0653 01       		.byte	0x1
 1084 0654 23       		.uleb128 0x23
 1085 0655 00000000 		.long	.LASF83
 1086 0659 01       		.byte	0x1
 1087 065a CD01     		.value	0x1cd
 1088 065c B4050000 		.long	0x5b4
 1089 0660 01       		.byte	0x1
 1090 0661 23       		.uleb128 0x23
 1091 0662 00000000 		.long	.LASF84
 1092 0666 01       		.byte	0x1
 1093 0667 4301     		.value	0x143
 1094 0669 7D040000 		.long	0x47d
 1095 066d 01       		.byte	0x1
 1096 066e 24       		.uleb128 0x24
GAS LISTING /tmp/cca1HRfE.s 			page 26


 1097 066f 00000000 		.long	.LASF85
 1098 0673 01       		.byte	0x1
 1099 0674 4601     		.value	0x146
 1100 0676 7C060000 		.long	0x67c
 1101 067a 01       		.byte	0x1
 1102 067b 01       		.byte	0x1
 1103 067c 0F       		.uleb128 0xf
 1104 067d 61060000 		.long	0x661
 1105 0681 25       		.uleb128 0x25
 1106 0682 64656300 		.string	"dec"
 1107 0686 01       		.byte	0x1
 1108 0687 4901     		.value	0x149
 1109 0689 7C060000 		.long	0x67c
 1110 068d 01       		.byte	0x1
 1111 068e 02       		.byte	0x2
 1112 068f 24       		.uleb128 0x24
 1113 0690 00000000 		.long	.LASF86
 1114 0694 01       		.byte	0x1
 1115 0695 4C01     		.value	0x14c
 1116 0697 7C060000 		.long	0x67c
 1117 069b 01       		.byte	0x1
 1118 069c 04       		.byte	0x4
 1119 069d 25       		.uleb128 0x25
 1120 069e 68657800 		.string	"hex"
 1121 06a2 01       		.byte	0x1
 1122 06a3 4F01     		.value	0x14f
 1123 06a5 7C060000 		.long	0x67c
 1124 06a9 01       		.byte	0x1
 1125 06aa 08       		.byte	0x8
 1126 06ab 24       		.uleb128 0x24
 1127 06ac 00000000 		.long	.LASF87
 1128 06b0 01       		.byte	0x1
 1129 06b1 5401     		.value	0x154
 1130 06b3 7C060000 		.long	0x67c
 1131 06b7 01       		.byte	0x1
 1132 06b8 10       		.byte	0x10
 1133 06b9 24       		.uleb128 0x24
 1134 06ba 00000000 		.long	.LASF88
 1135 06be 01       		.byte	0x1
 1136 06bf 5801     		.value	0x158
 1137 06c1 7C060000 		.long	0x67c
 1138 06c5 01       		.byte	0x1
 1139 06c6 20       		.byte	0x20
 1140 06c7 25       		.uleb128 0x25
 1141 06c8 6F637400 		.string	"oct"
 1142 06cc 01       		.byte	0x1
 1143 06cd 5B01     		.value	0x15b
 1144 06cf 7C060000 		.long	0x67c
 1145 06d3 01       		.byte	0x1
 1146 06d4 40       		.byte	0x40
 1147 06d5 24       		.uleb128 0x24
 1148 06d6 00000000 		.long	.LASF89
 1149 06da 01       		.byte	0x1
 1150 06db 5F01     		.value	0x15f
 1151 06dd 7C060000 		.long	0x67c
 1152 06e1 01       		.byte	0x1
 1153 06e2 80       		.byte	0x80
GAS LISTING /tmp/cca1HRfE.s 			page 27


 1154 06e3 26       		.uleb128 0x26
 1155 06e4 00000000 		.long	.LASF90
 1156 06e8 01       		.byte	0x1
 1157 06e9 6201     		.value	0x162
 1158 06eb 7C060000 		.long	0x67c
 1159 06ef 01       		.byte	0x1
 1160 06f0 0001     		.value	0x100
 1161 06f2 26       		.uleb128 0x26
 1162 06f3 00000000 		.long	.LASF91
 1163 06f7 01       		.byte	0x1
 1164 06f8 6601     		.value	0x166
 1165 06fa 7C060000 		.long	0x67c
 1166 06fe 01       		.byte	0x1
 1167 06ff 0002     		.value	0x200
 1168 0701 26       		.uleb128 0x26
 1169 0702 00000000 		.long	.LASF92
 1170 0706 01       		.byte	0x1
 1171 0707 6A01     		.value	0x16a
 1172 0709 7C060000 		.long	0x67c
 1173 070d 01       		.byte	0x1
 1174 070e 0004     		.value	0x400
 1175 0710 26       		.uleb128 0x26
 1176 0711 00000000 		.long	.LASF93
 1177 0715 01       		.byte	0x1
 1178 0716 6D01     		.value	0x16d
 1179 0718 7C060000 		.long	0x67c
 1180 071c 01       		.byte	0x1
 1181 071d 0008     		.value	0x800
 1182 071f 26       		.uleb128 0x26
 1183 0720 00000000 		.long	.LASF94
 1184 0724 01       		.byte	0x1
 1185 0725 7001     		.value	0x170
 1186 0727 7C060000 		.long	0x67c
 1187 072b 01       		.byte	0x1
 1188 072c 0010     		.value	0x1000
 1189 072e 26       		.uleb128 0x26
 1190 072f 00000000 		.long	.LASF95
 1191 0733 01       		.byte	0x1
 1192 0734 7301     		.value	0x173
 1193 0736 7C060000 		.long	0x67c
 1194 073a 01       		.byte	0x1
 1195 073b 0020     		.value	0x2000
 1196 073d 26       		.uleb128 0x26
 1197 073e 00000000 		.long	.LASF96
 1198 0742 01       		.byte	0x1
 1199 0743 7701     		.value	0x177
 1200 0745 7C060000 		.long	0x67c
 1201 0749 01       		.byte	0x1
 1202 074a 0040     		.value	0x4000
 1203 074c 24       		.uleb128 0x24
 1204 074d 00000000 		.long	.LASF97
 1205 0751 01       		.byte	0x1
 1206 0752 7A01     		.value	0x17a
 1207 0754 7C060000 		.long	0x67c
 1208 0758 01       		.byte	0x1
 1209 0759 B0       		.byte	0xb0
 1210 075a 24       		.uleb128 0x24
GAS LISTING /tmp/cca1HRfE.s 			page 28


 1211 075b 00000000 		.long	.LASF98
 1212 075f 01       		.byte	0x1
 1213 0760 7D01     		.value	0x17d
 1214 0762 7C060000 		.long	0x67c
 1215 0766 01       		.byte	0x1
 1216 0767 4A       		.byte	0x4a
 1217 0768 26       		.uleb128 0x26
 1218 0769 00000000 		.long	.LASF99
 1219 076d 01       		.byte	0x1
 1220 076e 8001     		.value	0x180
 1221 0770 7C060000 		.long	0x67c
 1222 0774 01       		.byte	0x1
 1223 0775 0401     		.value	0x104
 1224 0777 23       		.uleb128 0x23
 1225 0778 00000000 		.long	.LASF100
 1226 077c 01       		.byte	0x1
 1227 077d 8E01     		.value	0x18e
 1228 077f 6F050000 		.long	0x56f
 1229 0783 01       		.byte	0x1
 1230 0784 24       		.uleb128 0x24
 1231 0785 00000000 		.long	.LASF101
 1232 0789 01       		.byte	0x1
 1233 078a 9201     		.value	0x192
 1234 078c 92070000 		.long	0x792
 1235 0790 01       		.byte	0x1
 1236 0791 01       		.byte	0x1
 1237 0792 0F       		.uleb128 0xf
 1238 0793 77070000 		.long	0x777
 1239 0797 24       		.uleb128 0x24
 1240 0798 00000000 		.long	.LASF102
 1241 079c 01       		.byte	0x1
 1242 079d 9501     		.value	0x195
 1243 079f 92070000 		.long	0x792
 1244 07a3 01       		.byte	0x1
 1245 07a4 02       		.byte	0x2
 1246 07a5 24       		.uleb128 0x24
 1247 07a6 00000000 		.long	.LASF103
 1248 07aa 01       		.byte	0x1
 1249 07ab 9A01     		.value	0x19a
 1250 07ad 92070000 		.long	0x792
 1251 07b1 01       		.byte	0x1
 1252 07b2 04       		.byte	0x4
 1253 07b3 24       		.uleb128 0x24
 1254 07b4 00000000 		.long	.LASF104
 1255 07b8 01       		.byte	0x1
 1256 07b9 9D01     		.value	0x19d
 1257 07bb 92070000 		.long	0x792
 1258 07bf 01       		.byte	0x1
 1259 07c0 00       		.byte	0
 1260 07c1 25       		.uleb128 0x25
 1261 07c2 61707000 		.string	"app"
 1262 07c6 01       		.byte	0x1
 1263 07c7 B001     		.value	0x1b0
 1264 07c9 CF070000 		.long	0x7cf
 1265 07cd 01       		.byte	0x1
 1266 07ce 01       		.byte	0x1
 1267 07cf 0F       		.uleb128 0xf
GAS LISTING /tmp/cca1HRfE.s 			page 29


 1268 07d0 47060000 		.long	0x647
 1269 07d4 25       		.uleb128 0x25
 1270 07d5 61746500 		.string	"ate"
 1271 07d9 01       		.byte	0x1
 1272 07da B301     		.value	0x1b3
 1273 07dc CF070000 		.long	0x7cf
 1274 07e0 01       		.byte	0x1
 1275 07e1 02       		.byte	0x2
 1276 07e2 24       		.uleb128 0x24
 1277 07e3 00000000 		.long	.LASF105
 1278 07e7 01       		.byte	0x1
 1279 07e8 B801     		.value	0x1b8
 1280 07ea CF070000 		.long	0x7cf
 1281 07ee 01       		.byte	0x1
 1282 07ef 04       		.byte	0x4
 1283 07f0 25       		.uleb128 0x25
 1284 07f1 696E00   		.string	"in"
 1285 07f4 01       		.byte	0x1
 1286 07f5 BB01     		.value	0x1bb
 1287 07f7 CF070000 		.long	0x7cf
 1288 07fb 01       		.byte	0x1
 1289 07fc 08       		.byte	0x8
 1290 07fd 25       		.uleb128 0x25
 1291 07fe 6F757400 		.string	"out"
 1292 0802 01       		.byte	0x1
 1293 0803 BE01     		.value	0x1be
 1294 0805 CF070000 		.long	0x7cf
 1295 0809 01       		.byte	0x1
 1296 080a 10       		.byte	0x10
 1297 080b 24       		.uleb128 0x24
 1298 080c 00000000 		.long	.LASF106
 1299 0810 01       		.byte	0x1
 1300 0811 C101     		.value	0x1c1
 1301 0813 CF070000 		.long	0x7cf
 1302 0817 01       		.byte	0x1
 1303 0818 20       		.byte	0x20
 1304 0819 25       		.uleb128 0x25
 1305 081a 62656700 		.string	"beg"
 1306 081e 01       		.byte	0x1
 1307 081f D001     		.value	0x1d0
 1308 0821 27080000 		.long	0x827
 1309 0825 01       		.byte	0x1
 1310 0826 00       		.byte	0
 1311 0827 0F       		.uleb128 0xf
 1312 0828 54060000 		.long	0x654
 1313 082c 25       		.uleb128 0x25
 1314 082d 63757200 		.string	"cur"
 1315 0831 01       		.byte	0x1
 1316 0832 D301     		.value	0x1d3
 1317 0834 27080000 		.long	0x827
 1318 0838 01       		.byte	0x1
 1319 0839 01       		.byte	0x1
 1320 083a 25       		.uleb128 0x25
 1321 083b 656E6400 		.string	"end"
 1322 083f 01       		.byte	0x1
 1323 0840 D601     		.value	0x1d6
 1324 0842 27080000 		.long	0x827
GAS LISTING /tmp/cca1HRfE.s 			page 30


 1325 0846 01       		.byte	0x1
 1326 0847 02       		.byte	0x2
 1327 0848 00       		.byte	0
 1328 0849 08       		.uleb128 0x8
 1329 084a 09       		.byte	0x9
 1330 084b 52       		.byte	0x52
 1331 084c 61190000 		.long	0x1961
 1332 0850 08       		.uleb128 0x8
 1333 0851 09       		.byte	0x9
 1334 0852 53       		.byte	0x53
 1335 0853 56190000 		.long	0x1956
 1336 0857 08       		.uleb128 0x8
 1337 0858 09       		.byte	0x9
 1338 0859 54       		.byte	0x54
 1339 085a F00E0000 		.long	0xef0
 1340 085e 08       		.uleb128 0x8
 1341 085f 09       		.byte	0x9
 1342 0860 5C       		.byte	0x5c
 1343 0861 77190000 		.long	0x1977
 1344 0865 08       		.uleb128 0x8
 1345 0866 09       		.byte	0x9
 1346 0867 65       		.byte	0x65
 1347 0868 91190000 		.long	0x1991
 1348 086c 08       		.uleb128 0x8
 1349 086d 09       		.byte	0x9
 1350 086e 68       		.byte	0x68
 1351 086f AB190000 		.long	0x19ab
 1352 0873 08       		.uleb128 0x8
 1353 0874 09       		.byte	0x9
 1354 0875 69       		.byte	0x69
 1355 0876 C0190000 		.long	0x19c0
 1356 087a 1E       		.uleb128 0x1e
 1357 087b 00000000 		.long	.LASF108
 1358 087f 96080000 		.long	0x896
 1359 0883 05       		.uleb128 0x5
 1360 0884 00000000 		.long	.LASF109
 1361 0888 500F0000 		.long	0xf50
 1362 088c 06       		.uleb128 0x6
 1363 088d 00000000 		.long	.LASF1
 1364 0891 1D020000 		.long	0x21d
 1365 0895 00       		.byte	0
 1366 0896 08       		.uleb128 0x8
 1367 0897 0A       		.byte	0xa
 1368 0898 62       		.byte	0x62
 1369 0899 FE0C0000 		.long	0xcfe
 1370 089d 08       		.uleb128 0x8
 1371 089e 0A       		.byte	0xa
 1372 089f 63       		.byte	0x63
 1373 08a0 791A0000 		.long	0x1a79
 1374 08a4 08       		.uleb128 0x8
 1375 08a5 0A       		.byte	0xa
 1376 08a6 65       		.byte	0x65
 1377 08a7 841A0000 		.long	0x1a84
 1378 08ab 08       		.uleb128 0x8
 1379 08ac 0A       		.byte	0xa
 1380 08ad 66       		.byte	0x66
 1381 08ae 9C1A0000 		.long	0x1a9c
GAS LISTING /tmp/cca1HRfE.s 			page 31


 1382 08b2 08       		.uleb128 0x8
 1383 08b3 0A       		.byte	0xa
 1384 08b4 67       		.byte	0x67
 1385 08b5 B11A0000 		.long	0x1ab1
 1386 08b9 08       		.uleb128 0x8
 1387 08ba 0A       		.byte	0xa
 1388 08bb 68       		.byte	0x68
 1389 08bc C71A0000 		.long	0x1ac7
 1390 08c0 08       		.uleb128 0x8
 1391 08c1 0A       		.byte	0xa
 1392 08c2 69       		.byte	0x69
 1393 08c3 DD1A0000 		.long	0x1add
 1394 08c7 08       		.uleb128 0x8
 1395 08c8 0A       		.byte	0xa
 1396 08c9 6A       		.byte	0x6a
 1397 08ca F21A0000 		.long	0x1af2
 1398 08ce 08       		.uleb128 0x8
 1399 08cf 0A       		.byte	0xa
 1400 08d0 6B       		.byte	0x6b
 1401 08d1 081B0000 		.long	0x1b08
 1402 08d5 08       		.uleb128 0x8
 1403 08d6 0A       		.byte	0xa
 1404 08d7 6C       		.byte	0x6c
 1405 08d8 291B0000 		.long	0x1b29
 1406 08dc 08       		.uleb128 0x8
 1407 08dd 0A       		.byte	0xa
 1408 08de 6D       		.byte	0x6d
 1409 08df 491B0000 		.long	0x1b49
 1410 08e3 08       		.uleb128 0x8
 1411 08e4 0A       		.byte	0xa
 1412 08e5 71       		.byte	0x71
 1413 08e6 641B0000 		.long	0x1b64
 1414 08ea 08       		.uleb128 0x8
 1415 08eb 0A       		.byte	0xa
 1416 08ec 72       		.byte	0x72
 1417 08ed 891B0000 		.long	0x1b89
 1418 08f1 08       		.uleb128 0x8
 1419 08f2 0A       		.byte	0xa
 1420 08f3 74       		.byte	0x74
 1421 08f4 A91B0000 		.long	0x1ba9
 1422 08f8 08       		.uleb128 0x8
 1423 08f9 0A       		.byte	0xa
 1424 08fa 75       		.byte	0x75
 1425 08fb C91B0000 		.long	0x1bc9
 1426 08ff 08       		.uleb128 0x8
 1427 0900 0A       		.byte	0xa
 1428 0901 76       		.byte	0x76
 1429 0902 EF1B0000 		.long	0x1bef
 1430 0906 08       		.uleb128 0x8
 1431 0907 0A       		.byte	0xa
 1432 0908 78       		.byte	0x78
 1433 0909 051C0000 		.long	0x1c05
 1434 090d 08       		.uleb128 0x8
 1435 090e 0A       		.byte	0xa
 1436 090f 79       		.byte	0x79
 1437 0910 1B1C0000 		.long	0x1c1b
 1438 0914 08       		.uleb128 0x8
GAS LISTING /tmp/cca1HRfE.s 			page 32


 1439 0915 0A       		.byte	0xa
 1440 0916 7C       		.byte	0x7c
 1441 0917 271C0000 		.long	0x1c27
 1442 091b 08       		.uleb128 0x8
 1443 091c 0A       		.byte	0xa
 1444 091d 7E       		.byte	0x7e
 1445 091e 3D1C0000 		.long	0x1c3d
 1446 0922 08       		.uleb128 0x8
 1447 0923 0A       		.byte	0xa
 1448 0924 83       		.byte	0x83
 1449 0925 4F1C0000 		.long	0x1c4f
 1450 0929 08       		.uleb128 0x8
 1451 092a 0A       		.byte	0xa
 1452 092b 84       		.byte	0x84
 1453 092c 641C0000 		.long	0x1c64
 1454 0930 08       		.uleb128 0x8
 1455 0931 0A       		.byte	0xa
 1456 0932 85       		.byte	0x85
 1457 0933 7E1C0000 		.long	0x1c7e
 1458 0937 08       		.uleb128 0x8
 1459 0938 0A       		.byte	0xa
 1460 0939 87       		.byte	0x87
 1461 093a 901C0000 		.long	0x1c90
 1462 093e 08       		.uleb128 0x8
 1463 093f 0A       		.byte	0xa
 1464 0940 88       		.byte	0x88
 1465 0941 A71C0000 		.long	0x1ca7
 1466 0945 08       		.uleb128 0x8
 1467 0946 0A       		.byte	0xa
 1468 0947 8B       		.byte	0x8b
 1469 0948 CC1C0000 		.long	0x1ccc
 1470 094c 08       		.uleb128 0x8
 1471 094d 0A       		.byte	0xa
 1472 094e 8D       		.byte	0x8d
 1473 094f D71C0000 		.long	0x1cd7
 1474 0953 08       		.uleb128 0x8
 1475 0954 0A       		.byte	0xa
 1476 0955 8F       		.byte	0x8f
 1477 0956 EC1C0000 		.long	0x1cec
 1478 095a 27       		.uleb128 0x27
 1479 095b 00000000 		.long	.LASF110
 1480 095f 01       		.byte	0x1
 1481 0960 81       		.byte	0x81
 1482 0961 00000000 		.long	.LASF112
 1483 0965 1E050000 		.long	0x51e
 1484 0969 78090000 		.long	0x978
 1485 096d 0E       		.uleb128 0xe
 1486 096e 1E050000 		.long	0x51e
 1487 0972 0E       		.uleb128 0xe
 1488 0973 1E050000 		.long	0x51e
 1489 0977 00       		.byte	0
 1490 0978 0C       		.uleb128 0xc
 1491 0979 00000000 		.long	.LASF113
 1492 097d 0B       		.byte	0xb
 1493 097e 8D       		.byte	0x8d
 1494 097f 7A080000 		.long	0x87a
 1495 0983 0C       		.uleb128 0xc
GAS LISTING /tmp/cca1HRfE.s 			page 33


 1496 0984 00000000 		.long	.LASF114
 1497 0988 0B       		.byte	0xb
 1498 0989 A5       		.byte	0xa5
 1499 098a 8E090000 		.long	0x98e
 1500 098e 1E       		.uleb128 0x1e
 1501 098f 00000000 		.long	.LASF115
 1502 0993 AA090000 		.long	0x9aa
 1503 0997 05       		.uleb128 0x5
 1504 0998 00000000 		.long	.LASF109
 1505 099c 500F0000 		.long	0xf50
 1506 09a0 06       		.uleb128 0x6
 1507 09a1 00000000 		.long	.LASF1
 1508 09a5 1D020000 		.long	0x21d
 1509 09a9 00       		.byte	0
 1510 09aa 0C       		.uleb128 0xc
 1511 09ab 00000000 		.long	.LASF116
 1512 09af 0B       		.byte	0xb
 1513 09b0 9C       		.byte	0x9c
 1514 09b1 3F000000 		.long	0x3f
 1515 09b5 28       		.uleb128 0x28
 1516 09b6 00000000 		.long	.LASF384
 1517 09ba 03       		.byte	0x3
 1518 09bb 3D       		.byte	0x3d
 1519 09bc 00000000 		.long	.LASF385
 1520 09c0 78090000 		.long	0x978
 1521 09c4 29       		.uleb128 0x29
 1522 09c5 00000000 		.long	.LASF362
 1523 09c9 03       		.byte	0x3
 1524 09ca 4A       		.byte	0x4a
 1525 09cb E9050000 		.long	0x5e9
 1526 09cf 00       		.byte	0
 1527 09d0 03       		.uleb128 0x3
 1528 09d1 00000000 		.long	.LASF117
 1529 09d5 06       		.byte	0x6
 1530 09d6 DD       		.byte	0xdd
 1531 09d7 FE0C0000 		.long	0xcfe
 1532 09db 0A       		.uleb128 0xa
 1533 09dc 00000000 		.long	.LASF0
 1534 09e0 06       		.byte	0x6
 1535 09e1 DE       		.byte	0xde
 1536 09e2 07       		.uleb128 0x7
 1537 09e3 06       		.byte	0x6
 1538 09e4 DE       		.byte	0xde
 1539 09e5 DB090000 		.long	0x9db
 1540 09e9 08       		.uleb128 0x8
 1541 09ea 04       		.byte	0x4
 1542 09eb F8       		.byte	0xf8
 1543 09ec D4160000 		.long	0x16d4
 1544 09f0 09       		.uleb128 0x9
 1545 09f1 04       		.byte	0x4
 1546 09f2 0101     		.value	0x101
 1547 09f4 F6160000 		.long	0x16f6
 1548 09f8 09       		.uleb128 0x9
 1549 09f9 04       		.byte	0x4
 1550 09fa 0201     		.value	0x102
 1551 09fc 1D170000 		.long	0x171d
 1552 0a00 0A       		.uleb128 0xa
GAS LISTING /tmp/cca1HRfE.s 			page 34


 1553 0a01 00000000 		.long	.LASF119
 1554 0a05 0D       		.byte	0xd
 1555 0a06 24       		.byte	0x24
 1556 0a07 08       		.uleb128 0x8
 1557 0a08 0E       		.byte	0xe
 1558 0a09 2C       		.byte	0x2c
 1559 0a0a E5030000 		.long	0x3e5
 1560 0a0e 08       		.uleb128 0x8
 1561 0a0f 0E       		.byte	0xe
 1562 0a10 2D       		.byte	0x2d
 1563 0a11 05040000 		.long	0x405
 1564 0a15 14       		.uleb128 0x14
 1565 0a16 00000000 		.long	.LASF121
 1566 0a1a 01       		.byte	0x1
 1567 0a1b 0E       		.byte	0xe
 1568 0a1c 3A       		.byte	0x3a
 1569 0a1d B20B0000 		.long	0xbb2
 1570 0a21 2A       		.uleb128 0x2a
 1571 0a22 00000000 		.long	.LASF122
 1572 0a26 0E       		.byte	0xe
 1573 0a27 3D       		.byte	0x3d
 1574 0a28 E5030000 		.long	0x3e5
 1575 0a2c 01       		.byte	0x1
 1576 0a2d 2A       		.uleb128 0x2a
 1577 0a2e 00000000 		.long	.LASF123
 1578 0a32 0E       		.byte	0xe
 1579 0a33 3F       		.byte	0x3f
 1580 0a34 A1120000 		.long	0x12a1
 1581 0a38 01       		.byte	0x1
 1582 0a39 2A       		.uleb128 0x2a
 1583 0a3a 00000000 		.long	.LASF124
 1584 0a3e 0E       		.byte	0xe
 1585 0a3f 40       		.byte	0x40
 1586 0a40 800F0000 		.long	0xf80
 1587 0a44 01       		.byte	0x1
 1588 0a45 2A       		.uleb128 0x2a
 1589 0a46 00000000 		.long	.LASF125
 1590 0a4a 0E       		.byte	0xe
 1591 0a4b 41       		.byte	0x41
 1592 0a4c 1C190000 		.long	0x191c
 1593 0a50 01       		.byte	0x1
 1594 0a51 2A       		.uleb128 0x2a
 1595 0a52 00000000 		.long	.LASF126
 1596 0a56 0E       		.byte	0xe
 1597 0a57 42       		.byte	0x42
 1598 0a58 22190000 		.long	0x1922
 1599 0a5c 01       		.byte	0x1
 1600 0a5d 16       		.uleb128 0x16
 1601 0a5e 00000000 		.long	.LASF127
 1602 0a62 0E       		.byte	0xe
 1603 0a63 4F       		.byte	0x4f
 1604 0a64 00000000 		.long	.LASF128
 1605 0a68 01       		.byte	0x1
 1606 0a69 710A0000 		.long	0xa71
 1607 0a6d 770A0000 		.long	0xa77
 1608 0a71 17       		.uleb128 0x17
 1609 0a72 28190000 		.long	0x1928
GAS LISTING /tmp/cca1HRfE.s 			page 35


 1610 0a76 00       		.byte	0
 1611 0a77 16       		.uleb128 0x16
 1612 0a78 00000000 		.long	.LASF127
 1613 0a7c 0E       		.byte	0xe
 1614 0a7d 51       		.byte	0x51
 1615 0a7e 00000000 		.long	.LASF129
 1616 0a82 01       		.byte	0x1
 1617 0a83 8B0A0000 		.long	0xa8b
 1618 0a87 960A0000 		.long	0xa96
 1619 0a8b 17       		.uleb128 0x17
 1620 0a8c 28190000 		.long	0x1928
 1621 0a90 0E       		.uleb128 0xe
 1622 0a91 2E190000 		.long	0x192e
 1623 0a95 00       		.byte	0
 1624 0a96 16       		.uleb128 0x16
 1625 0a97 00000000 		.long	.LASF130
 1626 0a9b 0E       		.byte	0xe
 1627 0a9c 56       		.byte	0x56
 1628 0a9d 00000000 		.long	.LASF131
 1629 0aa1 01       		.byte	0x1
 1630 0aa2 AA0A0000 		.long	0xaaa
 1631 0aa6 B50A0000 		.long	0xab5
 1632 0aaa 17       		.uleb128 0x17
 1633 0aab 28190000 		.long	0x1928
 1634 0aaf 17       		.uleb128 0x17
 1635 0ab0 570F0000 		.long	0xf57
 1636 0ab4 00       		.byte	0
 1637 0ab5 2B       		.uleb128 0x2b
 1638 0ab6 00000000 		.long	.LASF132
 1639 0aba 0E       		.byte	0xe
 1640 0abb 59       		.byte	0x59
 1641 0abc 00000000 		.long	.LASF133
 1642 0ac0 2D0A0000 		.long	0xa2d
 1643 0ac4 01       		.byte	0x1
 1644 0ac5 CD0A0000 		.long	0xacd
 1645 0ac9 D80A0000 		.long	0xad8
 1646 0acd 17       		.uleb128 0x17
 1647 0ace 34190000 		.long	0x1934
 1648 0ad2 0E       		.uleb128 0xe
 1649 0ad3 450A0000 		.long	0xa45
 1650 0ad7 00       		.byte	0
 1651 0ad8 2B       		.uleb128 0x2b
 1652 0ad9 00000000 		.long	.LASF132
 1653 0add 0E       		.byte	0xe
 1654 0ade 5D       		.byte	0x5d
 1655 0adf 00000000 		.long	.LASF134
 1656 0ae3 390A0000 		.long	0xa39
 1657 0ae7 01       		.byte	0x1
 1658 0ae8 F00A0000 		.long	0xaf0
 1659 0aec FB0A0000 		.long	0xafb
 1660 0af0 17       		.uleb128 0x17
 1661 0af1 34190000 		.long	0x1934
 1662 0af5 0E       		.uleb128 0xe
 1663 0af6 510A0000 		.long	0xa51
 1664 0afa 00       		.byte	0
 1665 0afb 2B       		.uleb128 0x2b
 1666 0afc 00000000 		.long	.LASF135
GAS LISTING /tmp/cca1HRfE.s 			page 36


 1667 0b00 0E       		.byte	0xe
 1668 0b01 63       		.byte	0x63
 1669 0b02 00000000 		.long	.LASF136
 1670 0b06 2D0A0000 		.long	0xa2d
 1671 0b0a 01       		.byte	0x1
 1672 0b0b 130B0000 		.long	0xb13
 1673 0b0f 230B0000 		.long	0xb23
 1674 0b13 17       		.uleb128 0x17
 1675 0b14 28190000 		.long	0x1928
 1676 0b18 0E       		.uleb128 0xe
 1677 0b19 210A0000 		.long	0xa21
 1678 0b1d 0E       		.uleb128 0xe
 1679 0b1e 15190000 		.long	0x1915
 1680 0b22 00       		.byte	0
 1681 0b23 16       		.uleb128 0x16
 1682 0b24 00000000 		.long	.LASF137
 1683 0b28 0E       		.byte	0xe
 1684 0b29 6D       		.byte	0x6d
 1685 0b2a 00000000 		.long	.LASF138
 1686 0b2e 01       		.byte	0x1
 1687 0b2f 370B0000 		.long	0xb37
 1688 0b33 470B0000 		.long	0xb47
 1689 0b37 17       		.uleb128 0x17
 1690 0b38 28190000 		.long	0x1928
 1691 0b3c 0E       		.uleb128 0xe
 1692 0b3d 2D0A0000 		.long	0xa2d
 1693 0b41 0E       		.uleb128 0xe
 1694 0b42 210A0000 		.long	0xa21
 1695 0b46 00       		.byte	0
 1696 0b47 2B       		.uleb128 0x2b
 1697 0b48 00000000 		.long	.LASF139
 1698 0b4c 0E       		.byte	0xe
 1699 0b4d 71       		.byte	0x71
 1700 0b4e 00000000 		.long	.LASF140
 1701 0b52 210A0000 		.long	0xa21
 1702 0b56 01       		.byte	0x1
 1703 0b57 5F0B0000 		.long	0xb5f
 1704 0b5b 650B0000 		.long	0xb65
 1705 0b5f 17       		.uleb128 0x17
 1706 0b60 34190000 		.long	0x1934
 1707 0b64 00       		.byte	0
 1708 0b65 16       		.uleb128 0x16
 1709 0b66 00000000 		.long	.LASF141
 1710 0b6a 0E       		.byte	0xe
 1711 0b6b 81       		.byte	0x81
 1712 0b6c 00000000 		.long	.LASF142
 1713 0b70 01       		.byte	0x1
 1714 0b71 790B0000 		.long	0xb79
 1715 0b75 890B0000 		.long	0xb89
 1716 0b79 17       		.uleb128 0x17
 1717 0b7a 28190000 		.long	0x1928
 1718 0b7e 0E       		.uleb128 0xe
 1719 0b7f 2D0A0000 		.long	0xa2d
 1720 0b83 0E       		.uleb128 0xe
 1721 0b84 22190000 		.long	0x1922
 1722 0b88 00       		.byte	0
 1723 0b89 16       		.uleb128 0x16
GAS LISTING /tmp/cca1HRfE.s 			page 37


 1724 0b8a 00000000 		.long	.LASF143
 1725 0b8e 0E       		.byte	0xe
 1726 0b8f 85       		.byte	0x85
 1727 0b90 00000000 		.long	.LASF144
 1728 0b94 01       		.byte	0x1
 1729 0b95 9D0B0000 		.long	0xb9d
 1730 0b99 A80B0000 		.long	0xba8
 1731 0b9d 17       		.uleb128 0x17
 1732 0b9e 28190000 		.long	0x1928
 1733 0ba2 0E       		.uleb128 0xe
 1734 0ba3 2D0A0000 		.long	0xa2d
 1735 0ba7 00       		.byte	0
 1736 0ba8 2C       		.uleb128 0x2c
 1737 0ba9 5F547000 		.string	"_Tp"
 1738 0bad 500F0000 		.long	0xf50
 1739 0bb1 00       		.byte	0
 1740 0bb2 0F       		.uleb128 0xf
 1741 0bb3 150A0000 		.long	0xa15
 1742 0bb7 0B       		.uleb128 0xb
 1743 0bb8 00000000 		.long	.LASF146
 1744 0bbc 01       		.byte	0x1
 1745 0bbd 0F       		.byte	0xf
 1746 0bbe 37       		.byte	0x37
 1747 0bbf F90B0000 		.long	0xbf9
 1748 0bc3 2D       		.uleb128 0x2d
 1749 0bc4 00000000 		.long	.LASF147
 1750 0bc8 0F       		.byte	0xf
 1751 0bc9 3A       		.byte	0x3a
 1752 0bca 7B0F0000 		.long	0xf7b
 1753 0bce 2D       		.uleb128 0x2d
 1754 0bcf 00000000 		.long	.LASF148
 1755 0bd3 0F       		.byte	0xf
 1756 0bd4 3B       		.byte	0x3b
 1757 0bd5 7B0F0000 		.long	0xf7b
 1758 0bd9 2D       		.uleb128 0x2d
 1759 0bda 00000000 		.long	.LASF149
 1760 0bde 0F       		.byte	0xf
 1761 0bdf 3F       		.byte	0x3f
 1762 0be0 46190000 		.long	0x1946
 1763 0be4 2D       		.uleb128 0x2d
 1764 0be5 00000000 		.long	.LASF150
 1765 0be9 0F       		.byte	0xf
 1766 0bea 40       		.byte	0x40
 1767 0beb 7B0F0000 		.long	0xf7b
 1768 0bef 05       		.uleb128 0x5
 1769 0bf0 00000000 		.long	.LASF151
 1770 0bf4 570F0000 		.long	0xf57
 1771 0bf8 00       		.byte	0
 1772 0bf9 0B       		.uleb128 0xb
 1773 0bfa 00000000 		.long	.LASF152
 1774 0bfe 01       		.byte	0x1
 1775 0bff 0F       		.byte	0xf
 1776 0c00 37       		.byte	0x37
 1777 0c01 3B0C0000 		.long	0xc3b
 1778 0c05 2D       		.uleb128 0x2d
 1779 0c06 00000000 		.long	.LASF147
 1780 0c0a 0F       		.byte	0xf
GAS LISTING /tmp/cca1HRfE.s 			page 38


 1781 0c0b 3A       		.byte	0x3a
 1782 0c0c 4B190000 		.long	0x194b
 1783 0c10 2D       		.uleb128 0x2d
 1784 0c11 00000000 		.long	.LASF148
 1785 0c15 0F       		.byte	0xf
 1786 0c16 3B       		.byte	0x3b
 1787 0c17 4B190000 		.long	0x194b
 1788 0c1b 2D       		.uleb128 0x2d
 1789 0c1c 00000000 		.long	.LASF149
 1790 0c20 0F       		.byte	0xf
 1791 0c21 3F       		.byte	0x3f
 1792 0c22 46190000 		.long	0x1946
 1793 0c26 2D       		.uleb128 0x2d
 1794 0c27 00000000 		.long	.LASF150
 1795 0c2b 0F       		.byte	0xf
 1796 0c2c 40       		.byte	0x40
 1797 0c2d 7B0F0000 		.long	0xf7b
 1798 0c31 05       		.uleb128 0x5
 1799 0c32 00000000 		.long	.LASF151
 1800 0c36 E90E0000 		.long	0xee9
 1801 0c3a 00       		.byte	0
 1802 0c3b 0B       		.uleb128 0xb
 1803 0c3c 00000000 		.long	.LASF153
 1804 0c40 01       		.byte	0x1
 1805 0c41 0F       		.byte	0xf
 1806 0c42 37       		.byte	0x37
 1807 0c43 7D0C0000 		.long	0xc7d
 1808 0c47 2D       		.uleb128 0x2d
 1809 0c48 00000000 		.long	.LASF147
 1810 0c4c 0F       		.byte	0xf
 1811 0c4d 3A       		.byte	0x3a
 1812 0c4e 860F0000 		.long	0xf86
 1813 0c52 2D       		.uleb128 0x2d
 1814 0c53 00000000 		.long	.LASF148
 1815 0c57 0F       		.byte	0xf
 1816 0c58 3B       		.byte	0x3b
 1817 0c59 860F0000 		.long	0xf86
 1818 0c5d 2D       		.uleb128 0x2d
 1819 0c5e 00000000 		.long	.LASF149
 1820 0c62 0F       		.byte	0xf
 1821 0c63 3F       		.byte	0x3f
 1822 0c64 46190000 		.long	0x1946
 1823 0c68 2D       		.uleb128 0x2d
 1824 0c69 00000000 		.long	.LASF150
 1825 0c6d 0F       		.byte	0xf
 1826 0c6e 40       		.byte	0x40
 1827 0c6f 7B0F0000 		.long	0xf7b
 1828 0c73 05       		.uleb128 0x5
 1829 0c74 00000000 		.long	.LASF151
 1830 0c78 500F0000 		.long	0xf50
 1831 0c7c 00       		.byte	0
 1832 0c7d 0B       		.uleb128 0xb
 1833 0c7e 00000000 		.long	.LASF154
 1834 0c82 01       		.byte	0x1
 1835 0c83 0F       		.byte	0xf
 1836 0c84 37       		.byte	0x37
 1837 0c85 BF0C0000 		.long	0xcbf
GAS LISTING /tmp/cca1HRfE.s 			page 39


 1838 0c89 2D       		.uleb128 0x2d
 1839 0c8a 00000000 		.long	.LASF147
 1840 0c8e 0F       		.byte	0xf
 1841 0c8f 3A       		.byte	0x3a
 1842 0c90 D5190000 		.long	0x19d5
 1843 0c94 2D       		.uleb128 0x2d
 1844 0c95 00000000 		.long	.LASF148
 1845 0c99 0F       		.byte	0xf
 1846 0c9a 3B       		.byte	0x3b
 1847 0c9b D5190000 		.long	0x19d5
 1848 0c9f 2D       		.uleb128 0x2d
 1849 0ca0 00000000 		.long	.LASF149
 1850 0ca4 0F       		.byte	0xf
 1851 0ca5 3F       		.byte	0x3f
 1852 0ca6 46190000 		.long	0x1946
 1853 0caa 2D       		.uleb128 0x2d
 1854 0cab 00000000 		.long	.LASF150
 1855 0caf 0F       		.byte	0xf
 1856 0cb0 40       		.byte	0x40
 1857 0cb1 7B0F0000 		.long	0xf7b
 1858 0cb5 05       		.uleb128 0x5
 1859 0cb6 00000000 		.long	.LASF151
 1860 0cba 52170000 		.long	0x1752
 1861 0cbe 00       		.byte	0
 1862 0cbf 2E       		.uleb128 0x2e
 1863 0cc0 00000000 		.long	.LASF386
 1864 0cc4 01       		.byte	0x1
 1865 0cc5 0F       		.byte	0xf
 1866 0cc6 37       		.byte	0x37
 1867 0cc7 2D       		.uleb128 0x2d
 1868 0cc8 00000000 		.long	.LASF147
 1869 0ccc 0F       		.byte	0xf
 1870 0ccd 3A       		.byte	0x3a
 1871 0cce DA190000 		.long	0x19da
 1872 0cd2 2D       		.uleb128 0x2d
 1873 0cd3 00000000 		.long	.LASF148
 1874 0cd7 0F       		.byte	0xf
 1875 0cd8 3B       		.byte	0x3b
 1876 0cd9 DA190000 		.long	0x19da
 1877 0cdd 2D       		.uleb128 0x2d
 1878 0cde 00000000 		.long	.LASF149
 1879 0ce2 0F       		.byte	0xf
 1880 0ce3 3F       		.byte	0x3f
 1881 0ce4 46190000 		.long	0x1946
 1882 0ce8 2D       		.uleb128 0x2d
 1883 0ce9 00000000 		.long	.LASF150
 1884 0ced 0F       		.byte	0xf
 1885 0cee 40       		.byte	0x40
 1886 0cef 7B0F0000 		.long	0xf7b
 1887 0cf3 05       		.uleb128 0x5
 1888 0cf4 00000000 		.long	.LASF151
 1889 0cf8 2C150000 		.long	0x152c
 1890 0cfc 00       		.byte	0
 1891 0cfd 00       		.byte	0
 1892 0cfe 0C       		.uleb128 0xc
 1893 0cff 00000000 		.long	.LASF155
 1894 0d03 10       		.byte	0x10
GAS LISTING /tmp/cca1HRfE.s 			page 40


 1895 0d04 30       		.byte	0x30
 1896 0d05 090D0000 		.long	0xd09
 1897 0d09 0B       		.uleb128 0xb
 1898 0d0a 00000000 		.long	.LASF156
 1899 0d0e D8       		.byte	0xd8
 1900 0d0f 11       		.byte	0x11
 1901 0d10 F1       		.byte	0xf1
 1902 0d11 860E0000 		.long	0xe86
 1903 0d15 2F       		.uleb128 0x2f
 1904 0d16 00000000 		.long	.LASF157
 1905 0d1a 11       		.byte	0x11
 1906 0d1b F2       		.byte	0xf2
 1907 0d1c 570F0000 		.long	0xf57
 1908 0d20 00       		.byte	0
 1909 0d21 2F       		.uleb128 0x2f
 1910 0d22 00000000 		.long	.LASF158
 1911 0d26 11       		.byte	0x11
 1912 0d27 F7       		.byte	0xf7
 1913 0d28 A1120000 		.long	0x12a1
 1914 0d2c 08       		.byte	0x8
 1915 0d2d 2F       		.uleb128 0x2f
 1916 0d2e 00000000 		.long	.LASF159
 1917 0d32 11       		.byte	0x11
 1918 0d33 F8       		.byte	0xf8
 1919 0d34 A1120000 		.long	0x12a1
 1920 0d38 10       		.byte	0x10
 1921 0d39 2F       		.uleb128 0x2f
 1922 0d3a 00000000 		.long	.LASF160
 1923 0d3e 11       		.byte	0x11
 1924 0d3f F9       		.byte	0xf9
 1925 0d40 A1120000 		.long	0x12a1
 1926 0d44 18       		.byte	0x18
 1927 0d45 2F       		.uleb128 0x2f
 1928 0d46 00000000 		.long	.LASF161
 1929 0d4a 11       		.byte	0x11
 1930 0d4b FA       		.byte	0xfa
 1931 0d4c A1120000 		.long	0x12a1
 1932 0d50 20       		.byte	0x20
 1933 0d51 2F       		.uleb128 0x2f
 1934 0d52 00000000 		.long	.LASF162
 1935 0d56 11       		.byte	0x11
 1936 0d57 FB       		.byte	0xfb
 1937 0d58 A1120000 		.long	0x12a1
 1938 0d5c 28       		.byte	0x28
 1939 0d5d 2F       		.uleb128 0x2f
 1940 0d5e 00000000 		.long	.LASF163
 1941 0d62 11       		.byte	0x11
 1942 0d63 FC       		.byte	0xfc
 1943 0d64 A1120000 		.long	0x12a1
 1944 0d68 30       		.byte	0x30
 1945 0d69 2F       		.uleb128 0x2f
 1946 0d6a 00000000 		.long	.LASF164
 1947 0d6e 11       		.byte	0x11
 1948 0d6f FD       		.byte	0xfd
 1949 0d70 A1120000 		.long	0x12a1
 1950 0d74 38       		.byte	0x38
 1951 0d75 2F       		.uleb128 0x2f
GAS LISTING /tmp/cca1HRfE.s 			page 41


 1952 0d76 00000000 		.long	.LASF165
 1953 0d7a 11       		.byte	0x11
 1954 0d7b FE       		.byte	0xfe
 1955 0d7c A1120000 		.long	0x12a1
 1956 0d80 40       		.byte	0x40
 1957 0d81 30       		.uleb128 0x30
 1958 0d82 00000000 		.long	.LASF166
 1959 0d86 11       		.byte	0x11
 1960 0d87 0001     		.value	0x100
 1961 0d89 A1120000 		.long	0x12a1
 1962 0d8d 48       		.byte	0x48
 1963 0d8e 30       		.uleb128 0x30
 1964 0d8f 00000000 		.long	.LASF167
 1965 0d93 11       		.byte	0x11
 1966 0d94 0101     		.value	0x101
 1967 0d96 A1120000 		.long	0x12a1
 1968 0d9a 50       		.byte	0x50
 1969 0d9b 30       		.uleb128 0x30
 1970 0d9c 00000000 		.long	.LASF168
 1971 0da0 11       		.byte	0x11
 1972 0da1 0201     		.value	0x102
 1973 0da3 A1120000 		.long	0x12a1
 1974 0da7 58       		.byte	0x58
 1975 0da8 30       		.uleb128 0x30
 1976 0da9 00000000 		.long	.LASF169
 1977 0dad 11       		.byte	0x11
 1978 0dae 0401     		.value	0x104
 1979 0db0 471A0000 		.long	0x1a47
 1980 0db4 60       		.byte	0x60
 1981 0db5 30       		.uleb128 0x30
 1982 0db6 00000000 		.long	.LASF170
 1983 0dba 11       		.byte	0x11
 1984 0dbb 0601     		.value	0x106
 1985 0dbd 4D1A0000 		.long	0x1a4d
 1986 0dc1 68       		.byte	0x68
 1987 0dc2 30       		.uleb128 0x30
 1988 0dc3 00000000 		.long	.LASF171
 1989 0dc7 11       		.byte	0x11
 1990 0dc8 0801     		.value	0x108
 1991 0dca 570F0000 		.long	0xf57
 1992 0dce 70       		.byte	0x70
 1993 0dcf 30       		.uleb128 0x30
 1994 0dd0 00000000 		.long	.LASF172
 1995 0dd4 11       		.byte	0x11
 1996 0dd5 0C01     		.value	0x10c
 1997 0dd7 570F0000 		.long	0xf57
 1998 0ddb 74       		.byte	0x74
 1999 0ddc 30       		.uleb128 0x30
 2000 0ddd 00000000 		.long	.LASF173
 2001 0de1 11       		.byte	0x11
 2002 0de2 0E01     		.value	0x10e
 2003 0de4 F4180000 		.long	0x18f4
 2004 0de8 78       		.byte	0x78
 2005 0de9 30       		.uleb128 0x30
 2006 0dea 00000000 		.long	.LASF174
 2007 0dee 11       		.byte	0x11
 2008 0def 1201     		.value	0x112
GAS LISTING /tmp/cca1HRfE.s 			page 42


 2009 0df1 740F0000 		.long	0xf74
 2010 0df5 80       		.byte	0x80
 2011 0df6 30       		.uleb128 0x30
 2012 0df7 00000000 		.long	.LASF175
 2013 0dfb 11       		.byte	0x11
 2014 0dfc 1301     		.value	0x113
 2015 0dfe 4B170000 		.long	0x174b
 2016 0e02 82       		.byte	0x82
 2017 0e03 30       		.uleb128 0x30
 2018 0e04 00000000 		.long	.LASF176
 2019 0e08 11       		.byte	0x11
 2020 0e09 1401     		.value	0x114
 2021 0e0b 531A0000 		.long	0x1a53
 2022 0e0f 83       		.byte	0x83
 2023 0e10 30       		.uleb128 0x30
 2024 0e11 00000000 		.long	.LASF177
 2025 0e15 11       		.byte	0x11
 2026 0e16 1801     		.value	0x118
 2027 0e18 631A0000 		.long	0x1a63
 2028 0e1c 88       		.byte	0x88
 2029 0e1d 30       		.uleb128 0x30
 2030 0e1e 00000000 		.long	.LASF178
 2031 0e22 11       		.byte	0x11
 2032 0e23 2101     		.value	0x121
 2033 0e25 FF180000 		.long	0x18ff
 2034 0e29 90       		.byte	0x90
 2035 0e2a 30       		.uleb128 0x30
 2036 0e2b 00000000 		.long	.LASF179
 2037 0e2f 11       		.byte	0x11
 2038 0e30 2901     		.value	0x129
 2039 0e32 DC0E0000 		.long	0xedc
 2040 0e36 98       		.byte	0x98
 2041 0e37 30       		.uleb128 0x30
 2042 0e38 00000000 		.long	.LASF180
 2043 0e3c 11       		.byte	0x11
 2044 0e3d 2A01     		.value	0x12a
 2045 0e3f DC0E0000 		.long	0xedc
 2046 0e43 A0       		.byte	0xa0
 2047 0e44 30       		.uleb128 0x30
 2048 0e45 00000000 		.long	.LASF181
 2049 0e49 11       		.byte	0x11
 2050 0e4a 2B01     		.value	0x12b
 2051 0e4c DC0E0000 		.long	0xedc
 2052 0e50 A8       		.byte	0xa8
 2053 0e51 30       		.uleb128 0x30
 2054 0e52 00000000 		.long	.LASF182
 2055 0e56 11       		.byte	0x11
 2056 0e57 2C01     		.value	0x12c
 2057 0e59 DC0E0000 		.long	0xedc
 2058 0e5d B0       		.byte	0xb0
 2059 0e5e 30       		.uleb128 0x30
 2060 0e5f 00000000 		.long	.LASF183
 2061 0e63 11       		.byte	0x11
 2062 0e64 2E01     		.value	0x12e
 2063 0e66 DE0E0000 		.long	0xede
 2064 0e6a B8       		.byte	0xb8
 2065 0e6b 30       		.uleb128 0x30
GAS LISTING /tmp/cca1HRfE.s 			page 43


 2066 0e6c 00000000 		.long	.LASF184
 2067 0e70 11       		.byte	0x11
 2068 0e71 2F01     		.value	0x12f
 2069 0e73 570F0000 		.long	0xf57
 2070 0e77 C0       		.byte	0xc0
 2071 0e78 30       		.uleb128 0x30
 2072 0e79 00000000 		.long	.LASF185
 2073 0e7d 11       		.byte	0x11
 2074 0e7e 3101     		.value	0x131
 2075 0e80 691A0000 		.long	0x1a69
 2076 0e84 C4       		.byte	0xc4
 2077 0e85 00       		.byte	0
 2078 0e86 0C       		.uleb128 0xc
 2079 0e87 00000000 		.long	.LASF186
 2080 0e8b 10       		.byte	0x10
 2081 0e8c 40       		.byte	0x40
 2082 0e8d 090D0000 		.long	0xd09
 2083 0e91 31       		.uleb128 0x31
 2084 0e92 08       		.byte	0x8
 2085 0e93 07       		.byte	0x7
 2086 0e94 00000000 		.long	.LASF192
 2087 0e98 0B       		.uleb128 0xb
 2088 0e99 00000000 		.long	.LASF187
 2089 0e9d 18       		.byte	0x18
 2090 0e9e 12       		.byte	0x12
 2091 0e9f 00       		.byte	0
 2092 0ea0 D50E0000 		.long	0xed5
 2093 0ea4 2F       		.uleb128 0x2f
 2094 0ea5 00000000 		.long	.LASF188
 2095 0ea9 12       		.byte	0x12
 2096 0eaa 00       		.byte	0
 2097 0eab D50E0000 		.long	0xed5
 2098 0eaf 00       		.byte	0
 2099 0eb0 2F       		.uleb128 0x2f
 2100 0eb1 00000000 		.long	.LASF189
 2101 0eb5 12       		.byte	0x12
 2102 0eb6 00       		.byte	0
 2103 0eb7 D50E0000 		.long	0xed5
 2104 0ebb 04       		.byte	0x4
 2105 0ebc 2F       		.uleb128 0x2f
 2106 0ebd 00000000 		.long	.LASF190
 2107 0ec1 12       		.byte	0x12
 2108 0ec2 00       		.byte	0
 2109 0ec3 DC0E0000 		.long	0xedc
 2110 0ec7 08       		.byte	0x8
 2111 0ec8 2F       		.uleb128 0x2f
 2112 0ec9 00000000 		.long	.LASF191
 2113 0ecd 12       		.byte	0x12
 2114 0ece 00       		.byte	0
 2115 0ecf DC0E0000 		.long	0xedc
 2116 0ed3 10       		.byte	0x10
 2117 0ed4 00       		.byte	0
 2118 0ed5 31       		.uleb128 0x31
 2119 0ed6 04       		.byte	0x4
 2120 0ed7 07       		.byte	0x7
 2121 0ed8 00000000 		.long	.LASF193
 2122 0edc 32       		.uleb128 0x32
GAS LISTING /tmp/cca1HRfE.s 			page 44


 2123 0edd 08       		.byte	0x8
 2124 0ede 0C       		.uleb128 0xc
 2125 0edf 00000000 		.long	.LASF26
 2126 0ee3 13       		.byte	0x13
 2127 0ee4 D8       		.byte	0xd8
 2128 0ee5 E90E0000 		.long	0xee9
 2129 0ee9 31       		.uleb128 0x31
 2130 0eea 08       		.byte	0x8
 2131 0eeb 07       		.byte	0x7
 2132 0eec 00000000 		.long	.LASF194
 2133 0ef0 33       		.uleb128 0x33
 2134 0ef1 00000000 		.long	.LASF195
 2135 0ef5 13       		.byte	0x13
 2136 0ef6 6501     		.value	0x165
 2137 0ef8 D50E0000 		.long	0xed5
 2138 0efc 34       		.uleb128 0x34
 2139 0efd 08       		.byte	0x8
 2140 0efe 14       		.byte	0x14
 2141 0eff 53       		.byte	0x53
 2142 0f00 00000000 		.long	.LASF323
 2143 0f04 400F0000 		.long	0xf40
 2144 0f08 35       		.uleb128 0x35
 2145 0f09 04       		.byte	0x4
 2146 0f0a 14       		.byte	0x14
 2147 0f0b 56       		.byte	0x56
 2148 0f0c 270F0000 		.long	0xf27
 2149 0f10 36       		.uleb128 0x36
 2150 0f11 00000000 		.long	.LASF196
 2151 0f15 14       		.byte	0x14
 2152 0f16 58       		.byte	0x58
 2153 0f17 D50E0000 		.long	0xed5
 2154 0f1b 36       		.uleb128 0x36
 2155 0f1c 00000000 		.long	.LASF197
 2156 0f20 14       		.byte	0x14
 2157 0f21 5C       		.byte	0x5c
 2158 0f22 400F0000 		.long	0xf40
 2159 0f26 00       		.byte	0
 2160 0f27 2F       		.uleb128 0x2f
 2161 0f28 00000000 		.long	.LASF198
 2162 0f2c 14       		.byte	0x14
 2163 0f2d 54       		.byte	0x54
 2164 0f2e 570F0000 		.long	0xf57
 2165 0f32 00       		.byte	0
 2166 0f33 2F       		.uleb128 0x2f
 2167 0f34 00000000 		.long	.LASF199
 2168 0f38 14       		.byte	0x14
 2169 0f39 5D       		.byte	0x5d
 2170 0f3a 080F0000 		.long	0xf08
 2171 0f3e 04       		.byte	0x4
 2172 0f3f 00       		.byte	0
 2173 0f40 37       		.uleb128 0x37
 2174 0f41 500F0000 		.long	0xf50
 2175 0f45 500F0000 		.long	0xf50
 2176 0f49 38       		.uleb128 0x38
 2177 0f4a 910E0000 		.long	0xe91
 2178 0f4e 03       		.byte	0x3
 2179 0f4f 00       		.byte	0
GAS LISTING /tmp/cca1HRfE.s 			page 45


 2180 0f50 31       		.uleb128 0x31
 2181 0f51 01       		.byte	0x1
 2182 0f52 06       		.byte	0x6
 2183 0f53 00000000 		.long	.LASF200
 2184 0f57 39       		.uleb128 0x39
 2185 0f58 04       		.byte	0x4
 2186 0f59 05       		.byte	0x5
 2187 0f5a 696E7400 		.string	"int"
 2188 0f5e 0C       		.uleb128 0xc
 2189 0f5f 00000000 		.long	.LASF201
 2190 0f63 14       		.byte	0x14
 2191 0f64 5E       		.byte	0x5e
 2192 0f65 FC0E0000 		.long	0xefc
 2193 0f69 0C       		.uleb128 0xc
 2194 0f6a 00000000 		.long	.LASF202
 2195 0f6e 14       		.byte	0x14
 2196 0f6f 6A       		.byte	0x6a
 2197 0f70 5E0F0000 		.long	0xf5e
 2198 0f74 31       		.uleb128 0x31
 2199 0f75 02       		.byte	0x2
 2200 0f76 07       		.byte	0x7
 2201 0f77 00000000 		.long	.LASF203
 2202 0f7b 0F       		.uleb128 0xf
 2203 0f7c 570F0000 		.long	0xf57
 2204 0f80 3A       		.uleb128 0x3a
 2205 0f81 08       		.byte	0x8
 2206 0f82 860F0000 		.long	0xf86
 2207 0f86 0F       		.uleb128 0xf
 2208 0f87 500F0000 		.long	0xf50
 2209 0f8b 3B       		.uleb128 0x3b
 2210 0f8c 00000000 		.long	.LASF204
 2211 0f90 14       		.byte	0x14
 2212 0f91 6401     		.value	0x164
 2213 0f93 F00E0000 		.long	0xef0
 2214 0f97 A10F0000 		.long	0xfa1
 2215 0f9b 0E       		.uleb128 0xe
 2216 0f9c 570F0000 		.long	0xf57
 2217 0fa0 00       		.byte	0
 2218 0fa1 3B       		.uleb128 0x3b
 2219 0fa2 00000000 		.long	.LASF205
 2220 0fa6 14       		.byte	0x14
 2221 0fa7 EC02     		.value	0x2ec
 2222 0fa9 F00E0000 		.long	0xef0
 2223 0fad B70F0000 		.long	0xfb7
 2224 0fb1 0E       		.uleb128 0xe
 2225 0fb2 B70F0000 		.long	0xfb7
 2226 0fb6 00       		.byte	0
 2227 0fb7 3A       		.uleb128 0x3a
 2228 0fb8 08       		.byte	0x8
 2229 0fb9 860E0000 		.long	0xe86
 2230 0fbd 3B       		.uleb128 0x3b
 2231 0fbe 00000000 		.long	.LASF206
 2232 0fc2 14       		.byte	0x14
 2233 0fc3 0903     		.value	0x309
 2234 0fc5 DD0F0000 		.long	0xfdd
 2235 0fc9 DD0F0000 		.long	0xfdd
 2236 0fcd 0E       		.uleb128 0xe
GAS LISTING /tmp/cca1HRfE.s 			page 46


 2237 0fce DD0F0000 		.long	0xfdd
 2238 0fd2 0E       		.uleb128 0xe
 2239 0fd3 570F0000 		.long	0xf57
 2240 0fd7 0E       		.uleb128 0xe
 2241 0fd8 B70F0000 		.long	0xfb7
 2242 0fdc 00       		.byte	0
 2243 0fdd 3A       		.uleb128 0x3a
 2244 0fde 08       		.byte	0x8
 2245 0fdf E30F0000 		.long	0xfe3
 2246 0fe3 31       		.uleb128 0x31
 2247 0fe4 04       		.byte	0x4
 2248 0fe5 05       		.byte	0x5
 2249 0fe6 00000000 		.long	.LASF207
 2250 0fea 3B       		.uleb128 0x3b
 2251 0feb 00000000 		.long	.LASF208
 2252 0fef 14       		.byte	0x14
 2253 0ff0 FA02     		.value	0x2fa
 2254 0ff2 F00E0000 		.long	0xef0
 2255 0ff6 05100000 		.long	0x1005
 2256 0ffa 0E       		.uleb128 0xe
 2257 0ffb E30F0000 		.long	0xfe3
 2258 0fff 0E       		.uleb128 0xe
 2259 1000 B70F0000 		.long	0xfb7
 2260 1004 00       		.byte	0
 2261 1005 3B       		.uleb128 0x3b
 2262 1006 00000000 		.long	.LASF209
 2263 100a 14       		.byte	0x14
 2264 100b 1003     		.value	0x310
 2265 100d 570F0000 		.long	0xf57
 2266 1011 20100000 		.long	0x1020
 2267 1015 0E       		.uleb128 0xe
 2268 1016 20100000 		.long	0x1020
 2269 101a 0E       		.uleb128 0xe
 2270 101b B70F0000 		.long	0xfb7
 2271 101f 00       		.byte	0
 2272 1020 3A       		.uleb128 0x3a
 2273 1021 08       		.byte	0x8
 2274 1022 26100000 		.long	0x1026
 2275 1026 0F       		.uleb128 0xf
 2276 1027 E30F0000 		.long	0xfe3
 2277 102b 3B       		.uleb128 0x3b
 2278 102c 00000000 		.long	.LASF210
 2279 1030 14       		.byte	0x14
 2280 1031 4E02     		.value	0x24e
 2281 1033 570F0000 		.long	0xf57
 2282 1037 46100000 		.long	0x1046
 2283 103b 0E       		.uleb128 0xe
 2284 103c B70F0000 		.long	0xfb7
 2285 1040 0E       		.uleb128 0xe
 2286 1041 570F0000 		.long	0xf57
 2287 1045 00       		.byte	0
 2288 1046 3B       		.uleb128 0x3b
 2289 1047 00000000 		.long	.LASF211
 2290 104b 14       		.byte	0x14
 2291 104c 5502     		.value	0x255
 2292 104e 570F0000 		.long	0xf57
 2293 1052 62100000 		.long	0x1062
GAS LISTING /tmp/cca1HRfE.s 			page 47


 2294 1056 0E       		.uleb128 0xe
 2295 1057 B70F0000 		.long	0xfb7
 2296 105b 0E       		.uleb128 0xe
 2297 105c 20100000 		.long	0x1020
 2298 1060 3C       		.uleb128 0x3c
 2299 1061 00       		.byte	0
 2300 1062 3B       		.uleb128 0x3b
 2301 1063 00000000 		.long	.LASF212
 2302 1067 14       		.byte	0x14
 2303 1068 7E02     		.value	0x27e
 2304 106a 570F0000 		.long	0xf57
 2305 106e 7E100000 		.long	0x107e
 2306 1072 0E       		.uleb128 0xe
 2307 1073 B70F0000 		.long	0xfb7
 2308 1077 0E       		.uleb128 0xe
 2309 1078 20100000 		.long	0x1020
 2310 107c 3C       		.uleb128 0x3c
 2311 107d 00       		.byte	0
 2312 107e 3B       		.uleb128 0x3b
 2313 107f 00000000 		.long	.LASF213
 2314 1083 14       		.byte	0x14
 2315 1084 ED02     		.value	0x2ed
 2316 1086 F00E0000 		.long	0xef0
 2317 108a 94100000 		.long	0x1094
 2318 108e 0E       		.uleb128 0xe
 2319 108f B70F0000 		.long	0xfb7
 2320 1093 00       		.byte	0
 2321 1094 3D       		.uleb128 0x3d
 2322 1095 00000000 		.long	.LASF311
 2323 1099 14       		.byte	0x14
 2324 109a F302     		.value	0x2f3
 2325 109c F00E0000 		.long	0xef0
 2326 10a0 3B       		.uleb128 0x3b
 2327 10a1 00000000 		.long	.LASF214
 2328 10a5 14       		.byte	0x14
 2329 10a6 7B01     		.value	0x17b
 2330 10a8 DE0E0000 		.long	0xede
 2331 10ac C0100000 		.long	0x10c0
 2332 10b0 0E       		.uleb128 0xe
 2333 10b1 800F0000 		.long	0xf80
 2334 10b5 0E       		.uleb128 0xe
 2335 10b6 DE0E0000 		.long	0xede
 2336 10ba 0E       		.uleb128 0xe
 2337 10bb C0100000 		.long	0x10c0
 2338 10bf 00       		.byte	0
 2339 10c0 3A       		.uleb128 0x3a
 2340 10c1 08       		.byte	0x8
 2341 10c2 690F0000 		.long	0xf69
 2342 10c6 3B       		.uleb128 0x3b
 2343 10c7 00000000 		.long	.LASF215
 2344 10cb 14       		.byte	0x14
 2345 10cc 7001     		.value	0x170
 2346 10ce DE0E0000 		.long	0xede
 2347 10d2 EB100000 		.long	0x10eb
 2348 10d6 0E       		.uleb128 0xe
 2349 10d7 DD0F0000 		.long	0xfdd
 2350 10db 0E       		.uleb128 0xe
GAS LISTING /tmp/cca1HRfE.s 			page 48


 2351 10dc 800F0000 		.long	0xf80
 2352 10e0 0E       		.uleb128 0xe
 2353 10e1 DE0E0000 		.long	0xede
 2354 10e5 0E       		.uleb128 0xe
 2355 10e6 C0100000 		.long	0x10c0
 2356 10ea 00       		.byte	0
 2357 10eb 3B       		.uleb128 0x3b
 2358 10ec 00000000 		.long	.LASF216
 2359 10f0 14       		.byte	0x14
 2360 10f1 6C01     		.value	0x16c
 2361 10f3 570F0000 		.long	0xf57
 2362 10f7 01110000 		.long	0x1101
 2363 10fb 0E       		.uleb128 0xe
 2364 10fc 01110000 		.long	0x1101
 2365 1100 00       		.byte	0
 2366 1101 3A       		.uleb128 0x3a
 2367 1102 08       		.byte	0x8
 2368 1103 07110000 		.long	0x1107
 2369 1107 0F       		.uleb128 0xf
 2370 1108 690F0000 		.long	0xf69
 2371 110c 3B       		.uleb128 0x3b
 2372 110d 00000000 		.long	.LASF217
 2373 1111 14       		.byte	0x14
 2374 1112 9B01     		.value	0x19b
 2375 1114 DE0E0000 		.long	0xede
 2376 1118 31110000 		.long	0x1131
 2377 111c 0E       		.uleb128 0xe
 2378 111d DD0F0000 		.long	0xfdd
 2379 1121 0E       		.uleb128 0xe
 2380 1122 31110000 		.long	0x1131
 2381 1126 0E       		.uleb128 0xe
 2382 1127 DE0E0000 		.long	0xede
 2383 112b 0E       		.uleb128 0xe
 2384 112c C0100000 		.long	0x10c0
 2385 1130 00       		.byte	0
 2386 1131 3A       		.uleb128 0x3a
 2387 1132 08       		.byte	0x8
 2388 1133 800F0000 		.long	0xf80
 2389 1137 3B       		.uleb128 0x3b
 2390 1138 00000000 		.long	.LASF218
 2391 113c 14       		.byte	0x14
 2392 113d FB02     		.value	0x2fb
 2393 113f F00E0000 		.long	0xef0
 2394 1143 52110000 		.long	0x1152
 2395 1147 0E       		.uleb128 0xe
 2396 1148 E30F0000 		.long	0xfe3
 2397 114c 0E       		.uleb128 0xe
 2398 114d B70F0000 		.long	0xfb7
 2399 1151 00       		.byte	0
 2400 1152 3B       		.uleb128 0x3b
 2401 1153 00000000 		.long	.LASF219
 2402 1157 14       		.byte	0x14
 2403 1158 0103     		.value	0x301
 2404 115a F00E0000 		.long	0xef0
 2405 115e 68110000 		.long	0x1168
 2406 1162 0E       		.uleb128 0xe
 2407 1163 E30F0000 		.long	0xfe3
GAS LISTING /tmp/cca1HRfE.s 			page 49


 2408 1167 00       		.byte	0
 2409 1168 3B       		.uleb128 0x3b
 2410 1169 00000000 		.long	.LASF220
 2411 116d 14       		.byte	0x14
 2412 116e 5F02     		.value	0x25f
 2413 1170 570F0000 		.long	0xf57
 2414 1174 89110000 		.long	0x1189
 2415 1178 0E       		.uleb128 0xe
 2416 1179 DD0F0000 		.long	0xfdd
 2417 117d 0E       		.uleb128 0xe
 2418 117e DE0E0000 		.long	0xede
 2419 1182 0E       		.uleb128 0xe
 2420 1183 20100000 		.long	0x1020
 2421 1187 3C       		.uleb128 0x3c
 2422 1188 00       		.byte	0
 2423 1189 3B       		.uleb128 0x3b
 2424 118a 00000000 		.long	.LASF221
 2425 118e 14       		.byte	0x14
 2426 118f 8802     		.value	0x288
 2427 1191 570F0000 		.long	0xf57
 2428 1195 A5110000 		.long	0x11a5
 2429 1199 0E       		.uleb128 0xe
 2430 119a 20100000 		.long	0x1020
 2431 119e 0E       		.uleb128 0xe
 2432 119f 20100000 		.long	0x1020
 2433 11a3 3C       		.uleb128 0x3c
 2434 11a4 00       		.byte	0
 2435 11a5 3B       		.uleb128 0x3b
 2436 11a6 00000000 		.long	.LASF222
 2437 11aa 14       		.byte	0x14
 2438 11ab 1803     		.value	0x318
 2439 11ad F00E0000 		.long	0xef0
 2440 11b1 C0110000 		.long	0x11c0
 2441 11b5 0E       		.uleb128 0xe
 2442 11b6 F00E0000 		.long	0xef0
 2443 11ba 0E       		.uleb128 0xe
 2444 11bb B70F0000 		.long	0xfb7
 2445 11bf 00       		.byte	0
 2446 11c0 3B       		.uleb128 0x3b
 2447 11c1 00000000 		.long	.LASF223
 2448 11c5 14       		.byte	0x14
 2449 11c6 6702     		.value	0x267
 2450 11c8 570F0000 		.long	0xf57
 2451 11cc E0110000 		.long	0x11e0
 2452 11d0 0E       		.uleb128 0xe
 2453 11d1 B70F0000 		.long	0xfb7
 2454 11d5 0E       		.uleb128 0xe
 2455 11d6 20100000 		.long	0x1020
 2456 11da 0E       		.uleb128 0xe
 2457 11db E0110000 		.long	0x11e0
 2458 11df 00       		.byte	0
 2459 11e0 3A       		.uleb128 0x3a
 2460 11e1 08       		.byte	0x8
 2461 11e2 980E0000 		.long	0xe98
 2462 11e6 3B       		.uleb128 0x3b
 2463 11e7 00000000 		.long	.LASF224
 2464 11eb 14       		.byte	0x14
GAS LISTING /tmp/cca1HRfE.s 			page 50


 2465 11ec B402     		.value	0x2b4
 2466 11ee 570F0000 		.long	0xf57
 2467 11f2 06120000 		.long	0x1206
 2468 11f6 0E       		.uleb128 0xe
 2469 11f7 B70F0000 		.long	0xfb7
 2470 11fb 0E       		.uleb128 0xe
 2471 11fc 20100000 		.long	0x1020
 2472 1200 0E       		.uleb128 0xe
 2473 1201 E0110000 		.long	0x11e0
 2474 1205 00       		.byte	0
 2475 1206 3B       		.uleb128 0x3b
 2476 1207 00000000 		.long	.LASF225
 2477 120b 14       		.byte	0x14
 2478 120c 7402     		.value	0x274
 2479 120e 570F0000 		.long	0xf57
 2480 1212 2B120000 		.long	0x122b
 2481 1216 0E       		.uleb128 0xe
 2482 1217 DD0F0000 		.long	0xfdd
 2483 121b 0E       		.uleb128 0xe
 2484 121c DE0E0000 		.long	0xede
 2485 1220 0E       		.uleb128 0xe
 2486 1221 20100000 		.long	0x1020
 2487 1225 0E       		.uleb128 0xe
 2488 1226 E0110000 		.long	0x11e0
 2489 122a 00       		.byte	0
 2490 122b 3B       		.uleb128 0x3b
 2491 122c 00000000 		.long	.LASF226
 2492 1230 14       		.byte	0x14
 2493 1231 C002     		.value	0x2c0
 2494 1233 570F0000 		.long	0xf57
 2495 1237 4B120000 		.long	0x124b
 2496 123b 0E       		.uleb128 0xe
 2497 123c 20100000 		.long	0x1020
 2498 1240 0E       		.uleb128 0xe
 2499 1241 20100000 		.long	0x1020
 2500 1245 0E       		.uleb128 0xe
 2501 1246 E0110000 		.long	0x11e0
 2502 124a 00       		.byte	0
 2503 124b 3B       		.uleb128 0x3b
 2504 124c 00000000 		.long	.LASF227
 2505 1250 14       		.byte	0x14
 2506 1251 6F02     		.value	0x26f
 2507 1253 570F0000 		.long	0xf57
 2508 1257 66120000 		.long	0x1266
 2509 125b 0E       		.uleb128 0xe
 2510 125c 20100000 		.long	0x1020
 2511 1260 0E       		.uleb128 0xe
 2512 1261 E0110000 		.long	0x11e0
 2513 1265 00       		.byte	0
 2514 1266 3B       		.uleb128 0x3b
 2515 1267 00000000 		.long	.LASF228
 2516 126b 14       		.byte	0x14
 2517 126c BC02     		.value	0x2bc
 2518 126e 570F0000 		.long	0xf57
 2519 1272 81120000 		.long	0x1281
 2520 1276 0E       		.uleb128 0xe
 2521 1277 20100000 		.long	0x1020
GAS LISTING /tmp/cca1HRfE.s 			page 51


 2522 127b 0E       		.uleb128 0xe
 2523 127c E0110000 		.long	0x11e0
 2524 1280 00       		.byte	0
 2525 1281 3B       		.uleb128 0x3b
 2526 1282 00000000 		.long	.LASF229
 2527 1286 14       		.byte	0x14
 2528 1287 7501     		.value	0x175
 2529 1289 DE0E0000 		.long	0xede
 2530 128d A1120000 		.long	0x12a1
 2531 1291 0E       		.uleb128 0xe
 2532 1292 A1120000 		.long	0x12a1
 2533 1296 0E       		.uleb128 0xe
 2534 1297 E30F0000 		.long	0xfe3
 2535 129b 0E       		.uleb128 0xe
 2536 129c C0100000 		.long	0x10c0
 2537 12a0 00       		.byte	0
 2538 12a1 3A       		.uleb128 0x3a
 2539 12a2 08       		.byte	0x8
 2540 12a3 500F0000 		.long	0xf50
 2541 12a7 3E       		.uleb128 0x3e
 2542 12a8 00000000 		.long	.LASF230
 2543 12ac 14       		.byte	0x14
 2544 12ad 9D       		.byte	0x9d
 2545 12ae DD0F0000 		.long	0xfdd
 2546 12b2 C1120000 		.long	0x12c1
 2547 12b6 0E       		.uleb128 0xe
 2548 12b7 DD0F0000 		.long	0xfdd
 2549 12bb 0E       		.uleb128 0xe
 2550 12bc 20100000 		.long	0x1020
 2551 12c0 00       		.byte	0
 2552 12c1 3E       		.uleb128 0x3e
 2553 12c2 00000000 		.long	.LASF231
 2554 12c6 14       		.byte	0x14
 2555 12c7 A6       		.byte	0xa6
 2556 12c8 570F0000 		.long	0xf57
 2557 12cc DB120000 		.long	0x12db
 2558 12d0 0E       		.uleb128 0xe
 2559 12d1 20100000 		.long	0x1020
 2560 12d5 0E       		.uleb128 0xe
 2561 12d6 20100000 		.long	0x1020
 2562 12da 00       		.byte	0
 2563 12db 3E       		.uleb128 0x3e
 2564 12dc 00000000 		.long	.LASF232
 2565 12e0 14       		.byte	0x14
 2566 12e1 C3       		.byte	0xc3
 2567 12e2 570F0000 		.long	0xf57
 2568 12e6 F5120000 		.long	0x12f5
 2569 12ea 0E       		.uleb128 0xe
 2570 12eb 20100000 		.long	0x1020
 2571 12ef 0E       		.uleb128 0xe
 2572 12f0 20100000 		.long	0x1020
 2573 12f4 00       		.byte	0
 2574 12f5 3E       		.uleb128 0x3e
 2575 12f6 00000000 		.long	.LASF233
 2576 12fa 14       		.byte	0x14
 2577 12fb 93       		.byte	0x93
 2578 12fc DD0F0000 		.long	0xfdd
GAS LISTING /tmp/cca1HRfE.s 			page 52


 2579 1300 0F130000 		.long	0x130f
 2580 1304 0E       		.uleb128 0xe
 2581 1305 DD0F0000 		.long	0xfdd
 2582 1309 0E       		.uleb128 0xe
 2583 130a 20100000 		.long	0x1020
 2584 130e 00       		.byte	0
 2585 130f 3E       		.uleb128 0x3e
 2586 1310 00000000 		.long	.LASF234
 2587 1314 14       		.byte	0x14
 2588 1315 FF       		.byte	0xff
 2589 1316 DE0E0000 		.long	0xede
 2590 131a 29130000 		.long	0x1329
 2591 131e 0E       		.uleb128 0xe
 2592 131f 20100000 		.long	0x1020
 2593 1323 0E       		.uleb128 0xe
 2594 1324 20100000 		.long	0x1020
 2595 1328 00       		.byte	0
 2596 1329 3B       		.uleb128 0x3b
 2597 132a 00000000 		.long	.LASF235
 2598 132e 14       		.byte	0x14
 2599 132f 5A03     		.value	0x35a
 2600 1331 DE0E0000 		.long	0xede
 2601 1335 4E130000 		.long	0x134e
 2602 1339 0E       		.uleb128 0xe
 2603 133a DD0F0000 		.long	0xfdd
 2604 133e 0E       		.uleb128 0xe
 2605 133f DE0E0000 		.long	0xede
 2606 1343 0E       		.uleb128 0xe
 2607 1344 20100000 		.long	0x1020
 2608 1348 0E       		.uleb128 0xe
 2609 1349 4E130000 		.long	0x134e
 2610 134d 00       		.byte	0
 2611 134e 3A       		.uleb128 0x3a
 2612 134f 08       		.byte	0x8
 2613 1350 E4130000 		.long	0x13e4
 2614 1354 3F       		.uleb128 0x3f
 2615 1355 746D00   		.string	"tm"
 2616 1358 38       		.byte	0x38
 2617 1359 15       		.byte	0x15
 2618 135a 85       		.byte	0x85
 2619 135b E4130000 		.long	0x13e4
 2620 135f 2F       		.uleb128 0x2f
 2621 1360 00000000 		.long	.LASF236
 2622 1364 15       		.byte	0x15
 2623 1365 87       		.byte	0x87
 2624 1366 570F0000 		.long	0xf57
 2625 136a 00       		.byte	0
 2626 136b 2F       		.uleb128 0x2f
 2627 136c 00000000 		.long	.LASF237
 2628 1370 15       		.byte	0x15
 2629 1371 88       		.byte	0x88
 2630 1372 570F0000 		.long	0xf57
 2631 1376 04       		.byte	0x4
 2632 1377 2F       		.uleb128 0x2f
 2633 1378 00000000 		.long	.LASF238
 2634 137c 15       		.byte	0x15
 2635 137d 89       		.byte	0x89
GAS LISTING /tmp/cca1HRfE.s 			page 53


 2636 137e 570F0000 		.long	0xf57
 2637 1382 08       		.byte	0x8
 2638 1383 2F       		.uleb128 0x2f
 2639 1384 00000000 		.long	.LASF239
 2640 1388 15       		.byte	0x15
 2641 1389 8A       		.byte	0x8a
 2642 138a 570F0000 		.long	0xf57
 2643 138e 0C       		.byte	0xc
 2644 138f 2F       		.uleb128 0x2f
 2645 1390 00000000 		.long	.LASF240
 2646 1394 15       		.byte	0x15
 2647 1395 8B       		.byte	0x8b
 2648 1396 570F0000 		.long	0xf57
 2649 139a 10       		.byte	0x10
 2650 139b 2F       		.uleb128 0x2f
 2651 139c 00000000 		.long	.LASF241
 2652 13a0 15       		.byte	0x15
 2653 13a1 8C       		.byte	0x8c
 2654 13a2 570F0000 		.long	0xf57
 2655 13a6 14       		.byte	0x14
 2656 13a7 2F       		.uleb128 0x2f
 2657 13a8 00000000 		.long	.LASF242
 2658 13ac 15       		.byte	0x15
 2659 13ad 8D       		.byte	0x8d
 2660 13ae 570F0000 		.long	0xf57
 2661 13b2 18       		.byte	0x18
 2662 13b3 2F       		.uleb128 0x2f
 2663 13b4 00000000 		.long	.LASF243
 2664 13b8 15       		.byte	0x15
 2665 13b9 8E       		.byte	0x8e
 2666 13ba 570F0000 		.long	0xf57
 2667 13be 1C       		.byte	0x1c
 2668 13bf 2F       		.uleb128 0x2f
 2669 13c0 00000000 		.long	.LASF244
 2670 13c4 15       		.byte	0x15
 2671 13c5 8F       		.byte	0x8f
 2672 13c6 570F0000 		.long	0xf57
 2673 13ca 20       		.byte	0x20
 2674 13cb 2F       		.uleb128 0x2f
 2675 13cc 00000000 		.long	.LASF245
 2676 13d0 15       		.byte	0x15
 2677 13d1 92       		.byte	0x92
 2678 13d2 2C150000 		.long	0x152c
 2679 13d6 28       		.byte	0x28
 2680 13d7 2F       		.uleb128 0x2f
 2681 13d8 00000000 		.long	.LASF246
 2682 13dc 15       		.byte	0x15
 2683 13dd 93       		.byte	0x93
 2684 13de 800F0000 		.long	0xf80
 2685 13e2 30       		.byte	0x30
 2686 13e3 00       		.byte	0
 2687 13e4 0F       		.uleb128 0xf
 2688 13e5 54130000 		.long	0x1354
 2689 13e9 3B       		.uleb128 0x3b
 2690 13ea 00000000 		.long	.LASF247
 2691 13ee 14       		.byte	0x14
 2692 13ef 2201     		.value	0x122
GAS LISTING /tmp/cca1HRfE.s 			page 54


 2693 13f1 DE0E0000 		.long	0xede
 2694 13f5 FF130000 		.long	0x13ff
 2695 13f9 0E       		.uleb128 0xe
 2696 13fa 20100000 		.long	0x1020
 2697 13fe 00       		.byte	0
 2698 13ff 3E       		.uleb128 0x3e
 2699 1400 00000000 		.long	.LASF248
 2700 1404 14       		.byte	0x14
 2701 1405 A1       		.byte	0xa1
 2702 1406 DD0F0000 		.long	0xfdd
 2703 140a 1E140000 		.long	0x141e
 2704 140e 0E       		.uleb128 0xe
 2705 140f DD0F0000 		.long	0xfdd
 2706 1413 0E       		.uleb128 0xe
 2707 1414 20100000 		.long	0x1020
 2708 1418 0E       		.uleb128 0xe
 2709 1419 DE0E0000 		.long	0xede
 2710 141d 00       		.byte	0
 2711 141e 3E       		.uleb128 0x3e
 2712 141f 00000000 		.long	.LASF249
 2713 1423 14       		.byte	0x14
 2714 1424 A9       		.byte	0xa9
 2715 1425 570F0000 		.long	0xf57
 2716 1429 3D140000 		.long	0x143d
 2717 142d 0E       		.uleb128 0xe
 2718 142e 20100000 		.long	0x1020
 2719 1432 0E       		.uleb128 0xe
 2720 1433 20100000 		.long	0x1020
 2721 1437 0E       		.uleb128 0xe
 2722 1438 DE0E0000 		.long	0xede
 2723 143c 00       		.byte	0
 2724 143d 3E       		.uleb128 0x3e
 2725 143e 00000000 		.long	.LASF250
 2726 1442 14       		.byte	0x14
 2727 1443 98       		.byte	0x98
 2728 1444 DD0F0000 		.long	0xfdd
 2729 1448 5C140000 		.long	0x145c
 2730 144c 0E       		.uleb128 0xe
 2731 144d DD0F0000 		.long	0xfdd
 2732 1451 0E       		.uleb128 0xe
 2733 1452 20100000 		.long	0x1020
 2734 1456 0E       		.uleb128 0xe
 2735 1457 DE0E0000 		.long	0xede
 2736 145b 00       		.byte	0
 2737 145c 3B       		.uleb128 0x3b
 2738 145d 00000000 		.long	.LASF251
 2739 1461 14       		.byte	0x14
 2740 1462 A101     		.value	0x1a1
 2741 1464 DE0E0000 		.long	0xede
 2742 1468 81140000 		.long	0x1481
 2743 146c 0E       		.uleb128 0xe
 2744 146d A1120000 		.long	0x12a1
 2745 1471 0E       		.uleb128 0xe
 2746 1472 81140000 		.long	0x1481
 2747 1476 0E       		.uleb128 0xe
 2748 1477 DE0E0000 		.long	0xede
 2749 147b 0E       		.uleb128 0xe
GAS LISTING /tmp/cca1HRfE.s 			page 55


 2750 147c C0100000 		.long	0x10c0
 2751 1480 00       		.byte	0
 2752 1481 3A       		.uleb128 0x3a
 2753 1482 08       		.byte	0x8
 2754 1483 20100000 		.long	0x1020
 2755 1487 3B       		.uleb128 0x3b
 2756 1488 00000000 		.long	.LASF252
 2757 148c 14       		.byte	0x14
 2758 148d 0301     		.value	0x103
 2759 148f DE0E0000 		.long	0xede
 2760 1493 A2140000 		.long	0x14a2
 2761 1497 0E       		.uleb128 0xe
 2762 1498 20100000 		.long	0x1020
 2763 149c 0E       		.uleb128 0xe
 2764 149d 20100000 		.long	0x1020
 2765 14a1 00       		.byte	0
 2766 14a2 3B       		.uleb128 0x3b
 2767 14a3 00000000 		.long	.LASF253
 2768 14a7 14       		.byte	0x14
 2769 14a8 C501     		.value	0x1c5
 2770 14aa BD140000 		.long	0x14bd
 2771 14ae BD140000 		.long	0x14bd
 2772 14b2 0E       		.uleb128 0xe
 2773 14b3 20100000 		.long	0x1020
 2774 14b7 0E       		.uleb128 0xe
 2775 14b8 C4140000 		.long	0x14c4
 2776 14bc 00       		.byte	0
 2777 14bd 31       		.uleb128 0x31
 2778 14be 08       		.byte	0x8
 2779 14bf 04       		.byte	0x4
 2780 14c0 00000000 		.long	.LASF254
 2781 14c4 3A       		.uleb128 0x3a
 2782 14c5 08       		.byte	0x8
 2783 14c6 DD0F0000 		.long	0xfdd
 2784 14ca 3B       		.uleb128 0x3b
 2785 14cb 00000000 		.long	.LASF255
 2786 14cf 14       		.byte	0x14
 2787 14d0 CC01     		.value	0x1cc
 2788 14d2 E5140000 		.long	0x14e5
 2789 14d6 E5140000 		.long	0x14e5
 2790 14da 0E       		.uleb128 0xe
 2791 14db 20100000 		.long	0x1020
 2792 14df 0E       		.uleb128 0xe
 2793 14e0 C4140000 		.long	0x14c4
 2794 14e4 00       		.byte	0
 2795 14e5 31       		.uleb128 0x31
 2796 14e6 04       		.byte	0x4
 2797 14e7 04       		.byte	0x4
 2798 14e8 00000000 		.long	.LASF256
 2799 14ec 3B       		.uleb128 0x3b
 2800 14ed 00000000 		.long	.LASF257
 2801 14f1 14       		.byte	0x14
 2802 14f2 1D01     		.value	0x11d
 2803 14f4 DD0F0000 		.long	0xfdd
 2804 14f8 0C150000 		.long	0x150c
 2805 14fc 0E       		.uleb128 0xe
 2806 14fd DD0F0000 		.long	0xfdd
GAS LISTING /tmp/cca1HRfE.s 			page 56


 2807 1501 0E       		.uleb128 0xe
 2808 1502 20100000 		.long	0x1020
 2809 1506 0E       		.uleb128 0xe
 2810 1507 C4140000 		.long	0x14c4
 2811 150b 00       		.byte	0
 2812 150c 3B       		.uleb128 0x3b
 2813 150d 00000000 		.long	.LASF258
 2814 1511 14       		.byte	0x14
 2815 1512 D701     		.value	0x1d7
 2816 1514 2C150000 		.long	0x152c
 2817 1518 2C150000 		.long	0x152c
 2818 151c 0E       		.uleb128 0xe
 2819 151d 20100000 		.long	0x1020
 2820 1521 0E       		.uleb128 0xe
 2821 1522 C4140000 		.long	0x14c4
 2822 1526 0E       		.uleb128 0xe
 2823 1527 570F0000 		.long	0xf57
 2824 152b 00       		.byte	0
 2825 152c 31       		.uleb128 0x31
 2826 152d 08       		.byte	0x8
 2827 152e 05       		.byte	0x5
 2828 152f 00000000 		.long	.LASF259
 2829 1533 3B       		.uleb128 0x3b
 2830 1534 00000000 		.long	.LASF260
 2831 1538 14       		.byte	0x14
 2832 1539 DC01     		.value	0x1dc
 2833 153b E90E0000 		.long	0xee9
 2834 153f 53150000 		.long	0x1553
 2835 1543 0E       		.uleb128 0xe
 2836 1544 20100000 		.long	0x1020
 2837 1548 0E       		.uleb128 0xe
 2838 1549 C4140000 		.long	0x14c4
 2839 154d 0E       		.uleb128 0xe
 2840 154e 570F0000 		.long	0xf57
 2841 1552 00       		.byte	0
 2842 1553 3E       		.uleb128 0x3e
 2843 1554 00000000 		.long	.LASF261
 2844 1558 14       		.byte	0x14
 2845 1559 C7       		.byte	0xc7
 2846 155a DE0E0000 		.long	0xede
 2847 155e 72150000 		.long	0x1572
 2848 1562 0E       		.uleb128 0xe
 2849 1563 DD0F0000 		.long	0xfdd
 2850 1567 0E       		.uleb128 0xe
 2851 1568 20100000 		.long	0x1020
 2852 156c 0E       		.uleb128 0xe
 2853 156d DE0E0000 		.long	0xede
 2854 1571 00       		.byte	0
 2855 1572 3B       		.uleb128 0x3b
 2856 1573 00000000 		.long	.LASF262
 2857 1577 14       		.byte	0x14
 2858 1578 6801     		.value	0x168
 2859 157a 570F0000 		.long	0xf57
 2860 157e 88150000 		.long	0x1588
 2861 1582 0E       		.uleb128 0xe
 2862 1583 F00E0000 		.long	0xef0
 2863 1587 00       		.byte	0
GAS LISTING /tmp/cca1HRfE.s 			page 57


 2864 1588 3B       		.uleb128 0x3b
 2865 1589 00000000 		.long	.LASF263
 2866 158d 14       		.byte	0x14
 2867 158e 4801     		.value	0x148
 2868 1590 570F0000 		.long	0xf57
 2869 1594 A8150000 		.long	0x15a8
 2870 1598 0E       		.uleb128 0xe
 2871 1599 20100000 		.long	0x1020
 2872 159d 0E       		.uleb128 0xe
 2873 159e 20100000 		.long	0x1020
 2874 15a2 0E       		.uleb128 0xe
 2875 15a3 DE0E0000 		.long	0xede
 2876 15a7 00       		.byte	0
 2877 15a8 3B       		.uleb128 0x3b
 2878 15a9 00000000 		.long	.LASF264
 2879 15ad 14       		.byte	0x14
 2880 15ae 4C01     		.value	0x14c
 2881 15b0 DD0F0000 		.long	0xfdd
 2882 15b4 C8150000 		.long	0x15c8
 2883 15b8 0E       		.uleb128 0xe
 2884 15b9 DD0F0000 		.long	0xfdd
 2885 15bd 0E       		.uleb128 0xe
 2886 15be 20100000 		.long	0x1020
 2887 15c2 0E       		.uleb128 0xe
 2888 15c3 DE0E0000 		.long	0xede
 2889 15c7 00       		.byte	0
 2890 15c8 3B       		.uleb128 0x3b
 2891 15c9 00000000 		.long	.LASF265
 2892 15cd 14       		.byte	0x14
 2893 15ce 5101     		.value	0x151
 2894 15d0 DD0F0000 		.long	0xfdd
 2895 15d4 E8150000 		.long	0x15e8
 2896 15d8 0E       		.uleb128 0xe
 2897 15d9 DD0F0000 		.long	0xfdd
 2898 15dd 0E       		.uleb128 0xe
 2899 15de 20100000 		.long	0x1020
 2900 15e2 0E       		.uleb128 0xe
 2901 15e3 DE0E0000 		.long	0xede
 2902 15e7 00       		.byte	0
 2903 15e8 3B       		.uleb128 0x3b
 2904 15e9 00000000 		.long	.LASF266
 2905 15ed 14       		.byte	0x14
 2906 15ee 5501     		.value	0x155
 2907 15f0 DD0F0000 		.long	0xfdd
 2908 15f4 08160000 		.long	0x1608
 2909 15f8 0E       		.uleb128 0xe
 2910 15f9 DD0F0000 		.long	0xfdd
 2911 15fd 0E       		.uleb128 0xe
 2912 15fe E30F0000 		.long	0xfe3
 2913 1602 0E       		.uleb128 0xe
 2914 1603 DE0E0000 		.long	0xede
 2915 1607 00       		.byte	0
 2916 1608 3B       		.uleb128 0x3b
 2917 1609 00000000 		.long	.LASF267
 2918 160d 14       		.byte	0x14
 2919 160e 5C02     		.value	0x25c
 2920 1610 570F0000 		.long	0xf57
GAS LISTING /tmp/cca1HRfE.s 			page 58


 2921 1614 1F160000 		.long	0x161f
 2922 1618 0E       		.uleb128 0xe
 2923 1619 20100000 		.long	0x1020
 2924 161d 3C       		.uleb128 0x3c
 2925 161e 00       		.byte	0
 2926 161f 3B       		.uleb128 0x3b
 2927 1620 00000000 		.long	.LASF268
 2928 1624 14       		.byte	0x14
 2929 1625 8502     		.value	0x285
 2930 1627 570F0000 		.long	0xf57
 2931 162b 36160000 		.long	0x1636
 2932 162f 0E       		.uleb128 0xe
 2933 1630 20100000 		.long	0x1020
 2934 1634 3C       		.uleb128 0x3c
 2935 1635 00       		.byte	0
 2936 1636 27       		.uleb128 0x27
 2937 1637 00000000 		.long	.LASF269
 2938 163b 14       		.byte	0x14
 2939 163c E3       		.byte	0xe3
 2940 163d 00000000 		.long	.LASF269
 2941 1641 20100000 		.long	0x1020
 2942 1645 54160000 		.long	0x1654
 2943 1649 0E       		.uleb128 0xe
 2944 164a 20100000 		.long	0x1020
 2945 164e 0E       		.uleb128 0xe
 2946 164f E30F0000 		.long	0xfe3
 2947 1653 00       		.byte	0
 2948 1654 11       		.uleb128 0x11
 2949 1655 00000000 		.long	.LASF270
 2950 1659 14       		.byte	0x14
 2951 165a 0901     		.value	0x109
 2952 165c 00000000 		.long	.LASF270
 2953 1660 20100000 		.long	0x1020
 2954 1664 73160000 		.long	0x1673
 2955 1668 0E       		.uleb128 0xe
 2956 1669 20100000 		.long	0x1020
 2957 166d 0E       		.uleb128 0xe
 2958 166e 20100000 		.long	0x1020
 2959 1672 00       		.byte	0
 2960 1673 27       		.uleb128 0x27
 2961 1674 00000000 		.long	.LASF271
 2962 1678 14       		.byte	0x14
 2963 1679 ED       		.byte	0xed
 2964 167a 00000000 		.long	.LASF271
 2965 167e 20100000 		.long	0x1020
 2966 1682 91160000 		.long	0x1691
 2967 1686 0E       		.uleb128 0xe
 2968 1687 20100000 		.long	0x1020
 2969 168b 0E       		.uleb128 0xe
 2970 168c E30F0000 		.long	0xfe3
 2971 1690 00       		.byte	0
 2972 1691 11       		.uleb128 0x11
 2973 1692 00000000 		.long	.LASF272
 2974 1696 14       		.byte	0x14
 2975 1697 1401     		.value	0x114
 2976 1699 00000000 		.long	.LASF272
 2977 169d 20100000 		.long	0x1020
GAS LISTING /tmp/cca1HRfE.s 			page 59


 2978 16a1 B0160000 		.long	0x16b0
 2979 16a5 0E       		.uleb128 0xe
 2980 16a6 20100000 		.long	0x1020
 2981 16aa 0E       		.uleb128 0xe
 2982 16ab 20100000 		.long	0x1020
 2983 16af 00       		.byte	0
 2984 16b0 11       		.uleb128 0x11
 2985 16b1 00000000 		.long	.LASF273
 2986 16b5 14       		.byte	0x14
 2987 16b6 3F01     		.value	0x13f
 2988 16b8 00000000 		.long	.LASF273
 2989 16bc 20100000 		.long	0x1020
 2990 16c0 D4160000 		.long	0x16d4
 2991 16c4 0E       		.uleb128 0xe
 2992 16c5 20100000 		.long	0x1020
 2993 16c9 0E       		.uleb128 0xe
 2994 16ca E30F0000 		.long	0xfe3
 2995 16ce 0E       		.uleb128 0xe
 2996 16cf DE0E0000 		.long	0xede
 2997 16d3 00       		.byte	0
 2998 16d4 3B       		.uleb128 0x3b
 2999 16d5 00000000 		.long	.LASF274
 3000 16d9 14       		.byte	0x14
 3001 16da CE01     		.value	0x1ce
 3002 16dc EF160000 		.long	0x16ef
 3003 16e0 EF160000 		.long	0x16ef
 3004 16e4 0E       		.uleb128 0xe
 3005 16e5 20100000 		.long	0x1020
 3006 16e9 0E       		.uleb128 0xe
 3007 16ea C4140000 		.long	0x14c4
 3008 16ee 00       		.byte	0
 3009 16ef 31       		.uleb128 0x31
 3010 16f0 10       		.byte	0x10
 3011 16f1 04       		.byte	0x4
 3012 16f2 00000000 		.long	.LASF275
 3013 16f6 3B       		.uleb128 0x3b
 3014 16f7 00000000 		.long	.LASF276
 3015 16fb 14       		.byte	0x14
 3016 16fc E601     		.value	0x1e6
 3017 16fe 16170000 		.long	0x1716
 3018 1702 16170000 		.long	0x1716
 3019 1706 0E       		.uleb128 0xe
 3020 1707 20100000 		.long	0x1020
 3021 170b 0E       		.uleb128 0xe
 3022 170c C4140000 		.long	0x14c4
 3023 1710 0E       		.uleb128 0xe
 3024 1711 570F0000 		.long	0xf57
 3025 1715 00       		.byte	0
 3026 1716 31       		.uleb128 0x31
 3027 1717 08       		.byte	0x8
 3028 1718 05       		.byte	0x5
 3029 1719 00000000 		.long	.LASF277
 3030 171d 3B       		.uleb128 0x3b
 3031 171e 00000000 		.long	.LASF278
 3032 1722 14       		.byte	0x14
 3033 1723 ED01     		.value	0x1ed
 3034 1725 3D170000 		.long	0x173d
GAS LISTING /tmp/cca1HRfE.s 			page 60


 3035 1729 3D170000 		.long	0x173d
 3036 172d 0E       		.uleb128 0xe
 3037 172e 20100000 		.long	0x1020
 3038 1732 0E       		.uleb128 0xe
 3039 1733 C4140000 		.long	0x14c4
 3040 1737 0E       		.uleb128 0xe
 3041 1738 570F0000 		.long	0xf57
 3042 173c 00       		.byte	0
 3043 173d 31       		.uleb128 0x31
 3044 173e 08       		.byte	0x8
 3045 173f 07       		.byte	0x7
 3046 1740 00000000 		.long	.LASF279
 3047 1744 31       		.uleb128 0x31
 3048 1745 01       		.byte	0x1
 3049 1746 08       		.byte	0x8
 3050 1747 00000000 		.long	.LASF280
 3051 174b 31       		.uleb128 0x31
 3052 174c 01       		.byte	0x1
 3053 174d 06       		.byte	0x6
 3054 174e 00000000 		.long	.LASF281
 3055 1752 31       		.uleb128 0x31
 3056 1753 02       		.byte	0x2
 3057 1754 05       		.byte	0x5
 3058 1755 00000000 		.long	.LASF282
 3059 1759 03       		.uleb128 0x3
 3060 175a 00000000 		.long	.LASF283
 3061 175e 0C       		.byte	0xc
 3062 175f 37       		.byte	0x37
 3063 1760 6C170000 		.long	0x176c
 3064 1764 07       		.uleb128 0x7
 3065 1765 0C       		.byte	0xc
 3066 1766 38       		.byte	0x38
 3067 1767 16020000 		.long	0x216
 3068 176b 00       		.byte	0
 3069 176c 40       		.uleb128 0x40
 3070 176d 08       		.byte	0x8
 3071 176e 29020000 		.long	0x229
 3072 1772 40       		.uleb128 0x40
 3073 1773 08       		.byte	0x8
 3074 1774 59020000 		.long	0x259
 3075 1778 31       		.uleb128 0x31
 3076 1779 01       		.byte	0x1
 3077 177a 02       		.byte	0x2
 3078 177b 00000000 		.long	.LASF284
 3079 177f 3A       		.uleb128 0x3a
 3080 1780 08       		.byte	0x8
 3081 1781 59020000 		.long	0x259
 3082 1785 3A       		.uleb128 0x3a
 3083 1786 08       		.byte	0x8
 3084 1787 29020000 		.long	0x229
 3085 178b 40       		.uleb128 0x40
 3086 178c 08       		.byte	0x8
 3087 178d 80030000 		.long	0x380
 3088 1791 0B       		.uleb128 0xb
 3089 1792 00000000 		.long	.LASF285
 3090 1796 60       		.byte	0x60
 3091 1797 16       		.byte	0x16
GAS LISTING /tmp/cca1HRfE.s 			page 61


 3092 1798 35       		.byte	0x35
 3093 1799 BE180000 		.long	0x18be
 3094 179d 2F       		.uleb128 0x2f
 3095 179e 00000000 		.long	.LASF286
 3096 17a2 16       		.byte	0x16
 3097 17a3 39       		.byte	0x39
 3098 17a4 A1120000 		.long	0x12a1
 3099 17a8 00       		.byte	0
 3100 17a9 2F       		.uleb128 0x2f
 3101 17aa 00000000 		.long	.LASF287
 3102 17ae 16       		.byte	0x16
 3103 17af 3A       		.byte	0x3a
 3104 17b0 A1120000 		.long	0x12a1
 3105 17b4 08       		.byte	0x8
 3106 17b5 2F       		.uleb128 0x2f
 3107 17b6 00000000 		.long	.LASF288
 3108 17ba 16       		.byte	0x16
 3109 17bb 40       		.byte	0x40
 3110 17bc A1120000 		.long	0x12a1
 3111 17c0 10       		.byte	0x10
 3112 17c1 2F       		.uleb128 0x2f
 3113 17c2 00000000 		.long	.LASF289
 3114 17c6 16       		.byte	0x16
 3115 17c7 46       		.byte	0x46
 3116 17c8 A1120000 		.long	0x12a1
 3117 17cc 18       		.byte	0x18
 3118 17cd 2F       		.uleb128 0x2f
 3119 17ce 00000000 		.long	.LASF290
 3120 17d2 16       		.byte	0x16
 3121 17d3 47       		.byte	0x47
 3122 17d4 A1120000 		.long	0x12a1
 3123 17d8 20       		.byte	0x20
 3124 17d9 2F       		.uleb128 0x2f
 3125 17da 00000000 		.long	.LASF291
 3126 17de 16       		.byte	0x16
 3127 17df 48       		.byte	0x48
 3128 17e0 A1120000 		.long	0x12a1
 3129 17e4 28       		.byte	0x28
 3130 17e5 2F       		.uleb128 0x2f
 3131 17e6 00000000 		.long	.LASF292
 3132 17ea 16       		.byte	0x16
 3133 17eb 49       		.byte	0x49
 3134 17ec A1120000 		.long	0x12a1
 3135 17f0 30       		.byte	0x30
 3136 17f1 2F       		.uleb128 0x2f
 3137 17f2 00000000 		.long	.LASF293
 3138 17f6 16       		.byte	0x16
 3139 17f7 4A       		.byte	0x4a
 3140 17f8 A1120000 		.long	0x12a1
 3141 17fc 38       		.byte	0x38
 3142 17fd 2F       		.uleb128 0x2f
 3143 17fe 00000000 		.long	.LASF294
 3144 1802 16       		.byte	0x16
 3145 1803 4B       		.byte	0x4b
 3146 1804 A1120000 		.long	0x12a1
 3147 1808 40       		.byte	0x40
 3148 1809 2F       		.uleb128 0x2f
GAS LISTING /tmp/cca1HRfE.s 			page 62


 3149 180a 00000000 		.long	.LASF295
 3150 180e 16       		.byte	0x16
 3151 180f 4C       		.byte	0x4c
 3152 1810 A1120000 		.long	0x12a1
 3153 1814 48       		.byte	0x48
 3154 1815 2F       		.uleb128 0x2f
 3155 1816 00000000 		.long	.LASF296
 3156 181a 16       		.byte	0x16
 3157 181b 4D       		.byte	0x4d
 3158 181c 500F0000 		.long	0xf50
 3159 1820 50       		.byte	0x50
 3160 1821 2F       		.uleb128 0x2f
 3161 1822 00000000 		.long	.LASF297
 3162 1826 16       		.byte	0x16
 3163 1827 4E       		.byte	0x4e
 3164 1828 500F0000 		.long	0xf50
 3165 182c 51       		.byte	0x51
 3166 182d 2F       		.uleb128 0x2f
 3167 182e 00000000 		.long	.LASF298
 3168 1832 16       		.byte	0x16
 3169 1833 50       		.byte	0x50
 3170 1834 500F0000 		.long	0xf50
 3171 1838 52       		.byte	0x52
 3172 1839 2F       		.uleb128 0x2f
 3173 183a 00000000 		.long	.LASF299
 3174 183e 16       		.byte	0x16
 3175 183f 52       		.byte	0x52
 3176 1840 500F0000 		.long	0xf50
 3177 1844 53       		.byte	0x53
 3178 1845 2F       		.uleb128 0x2f
 3179 1846 00000000 		.long	.LASF300
 3180 184a 16       		.byte	0x16
 3181 184b 54       		.byte	0x54
 3182 184c 500F0000 		.long	0xf50
 3183 1850 54       		.byte	0x54
 3184 1851 2F       		.uleb128 0x2f
 3185 1852 00000000 		.long	.LASF301
 3186 1856 16       		.byte	0x16
 3187 1857 56       		.byte	0x56
 3188 1858 500F0000 		.long	0xf50
 3189 185c 55       		.byte	0x55
 3190 185d 2F       		.uleb128 0x2f
 3191 185e 00000000 		.long	.LASF302
 3192 1862 16       		.byte	0x16
 3193 1863 5D       		.byte	0x5d
 3194 1864 500F0000 		.long	0xf50
 3195 1868 56       		.byte	0x56
 3196 1869 2F       		.uleb128 0x2f
 3197 186a 00000000 		.long	.LASF303
 3198 186e 16       		.byte	0x16
 3199 186f 5E       		.byte	0x5e
 3200 1870 500F0000 		.long	0xf50
 3201 1874 57       		.byte	0x57
 3202 1875 2F       		.uleb128 0x2f
 3203 1876 00000000 		.long	.LASF304
 3204 187a 16       		.byte	0x16
 3205 187b 61       		.byte	0x61
GAS LISTING /tmp/cca1HRfE.s 			page 63


 3206 187c 500F0000 		.long	0xf50
 3207 1880 58       		.byte	0x58
 3208 1881 2F       		.uleb128 0x2f
 3209 1882 00000000 		.long	.LASF305
 3210 1886 16       		.byte	0x16
 3211 1887 63       		.byte	0x63
 3212 1888 500F0000 		.long	0xf50
 3213 188c 59       		.byte	0x59
 3214 188d 2F       		.uleb128 0x2f
 3215 188e 00000000 		.long	.LASF306
 3216 1892 16       		.byte	0x16
 3217 1893 65       		.byte	0x65
 3218 1894 500F0000 		.long	0xf50
 3219 1898 5A       		.byte	0x5a
 3220 1899 2F       		.uleb128 0x2f
 3221 189a 00000000 		.long	.LASF307
 3222 189e 16       		.byte	0x16
 3223 189f 67       		.byte	0x67
 3224 18a0 500F0000 		.long	0xf50
 3225 18a4 5B       		.byte	0x5b
 3226 18a5 2F       		.uleb128 0x2f
 3227 18a6 00000000 		.long	.LASF308
 3228 18aa 16       		.byte	0x16
 3229 18ab 6E       		.byte	0x6e
 3230 18ac 500F0000 		.long	0xf50
 3231 18b0 5C       		.byte	0x5c
 3232 18b1 2F       		.uleb128 0x2f
 3233 18b2 00000000 		.long	.LASF309
 3234 18b6 16       		.byte	0x16
 3235 18b7 6F       		.byte	0x6f
 3236 18b8 500F0000 		.long	0xf50
 3237 18bc 5D       		.byte	0x5d
 3238 18bd 00       		.byte	0
 3239 18be 3E       		.uleb128 0x3e
 3240 18bf 00000000 		.long	.LASF310
 3241 18c3 16       		.byte	0x16
 3242 18c4 7C       		.byte	0x7c
 3243 18c5 A1120000 		.long	0x12a1
 3244 18c9 D8180000 		.long	0x18d8
 3245 18cd 0E       		.uleb128 0xe
 3246 18ce 570F0000 		.long	0xf57
 3247 18d2 0E       		.uleb128 0xe
 3248 18d3 800F0000 		.long	0xf80
 3249 18d7 00       		.byte	0
 3250 18d8 41       		.uleb128 0x41
 3251 18d9 00000000 		.long	.LASF312
 3252 18dd 16       		.byte	0x16
 3253 18de 7F       		.byte	0x7f
 3254 18df E3180000 		.long	0x18e3
 3255 18e3 3A       		.uleb128 0x3a
 3256 18e4 08       		.byte	0x8
 3257 18e5 91170000 		.long	0x1791
 3258 18e9 0C       		.uleb128 0xc
 3259 18ea 00000000 		.long	.LASF313
 3260 18ee 17       		.byte	0x17
 3261 18ef 28       		.byte	0x28
 3262 18f0 570F0000 		.long	0xf57
GAS LISTING /tmp/cca1HRfE.s 			page 64


 3263 18f4 0C       		.uleb128 0xc
 3264 18f5 00000000 		.long	.LASF314
 3265 18f9 17       		.byte	0x17
 3266 18fa 83       		.byte	0x83
 3267 18fb 2C150000 		.long	0x152c
 3268 18ff 0C       		.uleb128 0xc
 3269 1900 00000000 		.long	.LASF315
 3270 1904 17       		.byte	0x17
 3271 1905 84       		.byte	0x84
 3272 1906 2C150000 		.long	0x152c
 3273 190a 0C       		.uleb128 0xc
 3274 190b 00000000 		.long	.LASF316
 3275 190f 18       		.byte	0x18
 3276 1910 20       		.byte	0x20
 3277 1911 570F0000 		.long	0xf57
 3278 1915 3A       		.uleb128 0x3a
 3279 1916 08       		.byte	0x8
 3280 1917 1B190000 		.long	0x191b
 3281 191b 42       		.uleb128 0x42
 3282 191c 40       		.uleb128 0x40
 3283 191d 08       		.byte	0x8
 3284 191e 500F0000 		.long	0xf50
 3285 1922 40       		.uleb128 0x40
 3286 1923 08       		.byte	0x8
 3287 1924 860F0000 		.long	0xf86
 3288 1928 3A       		.uleb128 0x3a
 3289 1929 08       		.byte	0x8
 3290 192a 150A0000 		.long	0xa15
 3291 192e 40       		.uleb128 0x40
 3292 192f 08       		.byte	0x8
 3293 1930 B20B0000 		.long	0xbb2
 3294 1934 3A       		.uleb128 0x3a
 3295 1935 08       		.byte	0x8
 3296 1936 B20B0000 		.long	0xbb2
 3297 193a 3A       		.uleb128 0x3a
 3298 193b 08       		.byte	0x8
 3299 193c 10040000 		.long	0x410
 3300 1940 40       		.uleb128 0x40
 3301 1941 08       		.byte	0x8
 3302 1942 78040000 		.long	0x478
 3303 1946 0F       		.uleb128 0xf
 3304 1947 78170000 		.long	0x1778
 3305 194b 0F       		.uleb128 0xf
 3306 194c E90E0000 		.long	0xee9
 3307 1950 3A       		.uleb128 0x3a
 3308 1951 08       		.byte	0x8
 3309 1952 E9050000 		.long	0x5e9
 3310 1956 0C       		.uleb128 0xc
 3311 1957 00000000 		.long	.LASF317
 3312 195b 19       		.byte	0x19
 3313 195c 34       		.byte	0x34
 3314 195d E90E0000 		.long	0xee9
 3315 1961 0C       		.uleb128 0xc
 3316 1962 00000000 		.long	.LASF318
 3317 1966 19       		.byte	0x19
 3318 1967 BA       		.byte	0xba
 3319 1968 6C190000 		.long	0x196c
GAS LISTING /tmp/cca1HRfE.s 			page 65


 3320 196c 3A       		.uleb128 0x3a
 3321 196d 08       		.byte	0x8
 3322 196e 72190000 		.long	0x1972
 3323 1972 0F       		.uleb128 0xf
 3324 1973 E9180000 		.long	0x18e9
 3325 1977 3E       		.uleb128 0x3e
 3326 1978 00000000 		.long	.LASF319
 3327 197c 19       		.byte	0x19
 3328 197d AF       		.byte	0xaf
 3329 197e 570F0000 		.long	0xf57
 3330 1982 91190000 		.long	0x1991
 3331 1986 0E       		.uleb128 0xe
 3332 1987 F00E0000 		.long	0xef0
 3333 198b 0E       		.uleb128 0xe
 3334 198c 56190000 		.long	0x1956
 3335 1990 00       		.byte	0
 3336 1991 3E       		.uleb128 0x3e
 3337 1992 00000000 		.long	.LASF320
 3338 1996 19       		.byte	0x19
 3339 1997 DD       		.byte	0xdd
 3340 1998 F00E0000 		.long	0xef0
 3341 199c AB190000 		.long	0x19ab
 3342 19a0 0E       		.uleb128 0xe
 3343 19a1 F00E0000 		.long	0xef0
 3344 19a5 0E       		.uleb128 0xe
 3345 19a6 61190000 		.long	0x1961
 3346 19aa 00       		.byte	0
 3347 19ab 3E       		.uleb128 0x3e
 3348 19ac 00000000 		.long	.LASF321
 3349 19b0 19       		.byte	0x19
 3350 19b1 DA       		.byte	0xda
 3351 19b2 61190000 		.long	0x1961
 3352 19b6 C0190000 		.long	0x19c0
 3353 19ba 0E       		.uleb128 0xe
 3354 19bb 800F0000 		.long	0xf80
 3355 19bf 00       		.byte	0
 3356 19c0 3E       		.uleb128 0x3e
 3357 19c1 00000000 		.long	.LASF322
 3358 19c5 19       		.byte	0x19
 3359 19c6 AB       		.byte	0xab
 3360 19c7 56190000 		.long	0x1956
 3361 19cb D5190000 		.long	0x19d5
 3362 19cf 0E       		.uleb128 0xe
 3363 19d0 800F0000 		.long	0xf80
 3364 19d4 00       		.byte	0
 3365 19d5 0F       		.uleb128 0xf
 3366 19d6 52170000 		.long	0x1752
 3367 19da 0F       		.uleb128 0xf
 3368 19db 2C150000 		.long	0x152c
 3369 19df 34       		.uleb128 0x34
 3370 19e0 10       		.byte	0x10
 3371 19e1 1A       		.byte	0x1a
 3372 19e2 16       		.byte	0x16
 3373 19e3 00000000 		.long	.LASF324
 3374 19e7 041A0000 		.long	0x1a04
 3375 19eb 2F       		.uleb128 0x2f
 3376 19ec 00000000 		.long	.LASF325
GAS LISTING /tmp/cca1HRfE.s 			page 66


 3377 19f0 1A       		.byte	0x1a
 3378 19f1 17       		.byte	0x17
 3379 19f2 F4180000 		.long	0x18f4
 3380 19f6 00       		.byte	0
 3381 19f7 2F       		.uleb128 0x2f
 3382 19f8 00000000 		.long	.LASF326
 3383 19fc 1A       		.byte	0x1a
 3384 19fd 18       		.byte	0x18
 3385 19fe 5E0F0000 		.long	0xf5e
 3386 1a02 08       		.byte	0x8
 3387 1a03 00       		.byte	0
 3388 1a04 0C       		.uleb128 0xc
 3389 1a05 00000000 		.long	.LASF327
 3390 1a09 1A       		.byte	0x1a
 3391 1a0a 19       		.byte	0x19
 3392 1a0b DF190000 		.long	0x19df
 3393 1a0f 43       		.uleb128 0x43
 3394 1a10 00000000 		.long	.LASF387
 3395 1a14 11       		.byte	0x11
 3396 1a15 96       		.byte	0x96
 3397 1a16 0B       		.uleb128 0xb
 3398 1a17 00000000 		.long	.LASF328
 3399 1a1b 18       		.byte	0x18
 3400 1a1c 11       		.byte	0x11
 3401 1a1d 9C       		.byte	0x9c
 3402 1a1e 471A0000 		.long	0x1a47
 3403 1a22 2F       		.uleb128 0x2f
 3404 1a23 00000000 		.long	.LASF329
 3405 1a27 11       		.byte	0x11
 3406 1a28 9D       		.byte	0x9d
 3407 1a29 471A0000 		.long	0x1a47
 3408 1a2d 00       		.byte	0
 3409 1a2e 2F       		.uleb128 0x2f
 3410 1a2f 00000000 		.long	.LASF330
 3411 1a33 11       		.byte	0x11
 3412 1a34 9E       		.byte	0x9e
 3413 1a35 4D1A0000 		.long	0x1a4d
 3414 1a39 08       		.byte	0x8
 3415 1a3a 2F       		.uleb128 0x2f
 3416 1a3b 00000000 		.long	.LASF331
 3417 1a3f 11       		.byte	0x11
 3418 1a40 A2       		.byte	0xa2
 3419 1a41 570F0000 		.long	0xf57
 3420 1a45 10       		.byte	0x10
 3421 1a46 00       		.byte	0
 3422 1a47 3A       		.uleb128 0x3a
 3423 1a48 08       		.byte	0x8
 3424 1a49 161A0000 		.long	0x1a16
 3425 1a4d 3A       		.uleb128 0x3a
 3426 1a4e 08       		.byte	0x8
 3427 1a4f 090D0000 		.long	0xd09
 3428 1a53 37       		.uleb128 0x37
 3429 1a54 500F0000 		.long	0xf50
 3430 1a58 631A0000 		.long	0x1a63
 3431 1a5c 38       		.uleb128 0x38
 3432 1a5d 910E0000 		.long	0xe91
 3433 1a61 00       		.byte	0
GAS LISTING /tmp/cca1HRfE.s 			page 67


 3434 1a62 00       		.byte	0
 3435 1a63 3A       		.uleb128 0x3a
 3436 1a64 08       		.byte	0x8
 3437 1a65 0F1A0000 		.long	0x1a0f
 3438 1a69 37       		.uleb128 0x37
 3439 1a6a 500F0000 		.long	0xf50
 3440 1a6e 791A0000 		.long	0x1a79
 3441 1a72 38       		.uleb128 0x38
 3442 1a73 910E0000 		.long	0xe91
 3443 1a77 13       		.byte	0x13
 3444 1a78 00       		.byte	0
 3445 1a79 0C       		.uleb128 0xc
 3446 1a7a 00000000 		.long	.LASF332
 3447 1a7e 10       		.byte	0x10
 3448 1a7f 6E       		.byte	0x6e
 3449 1a80 041A0000 		.long	0x1a04
 3450 1a84 44       		.uleb128 0x44
 3451 1a85 00000000 		.long	.LASF349
 3452 1a89 10       		.byte	0x10
 3453 1a8a 3A03     		.value	0x33a
 3454 1a8c 961A0000 		.long	0x1a96
 3455 1a90 0E       		.uleb128 0xe
 3456 1a91 961A0000 		.long	0x1a96
 3457 1a95 00       		.byte	0
 3458 1a96 3A       		.uleb128 0x3a
 3459 1a97 08       		.byte	0x8
 3460 1a98 FE0C0000 		.long	0xcfe
 3461 1a9c 3E       		.uleb128 0x3e
 3462 1a9d 00000000 		.long	.LASF333
 3463 1aa1 10       		.byte	0x10
 3464 1aa2 ED       		.byte	0xed
 3465 1aa3 570F0000 		.long	0xf57
 3466 1aa7 B11A0000 		.long	0x1ab1
 3467 1aab 0E       		.uleb128 0xe
 3468 1aac 961A0000 		.long	0x1a96
 3469 1ab0 00       		.byte	0
 3470 1ab1 3B       		.uleb128 0x3b
 3471 1ab2 00000000 		.long	.LASF334
 3472 1ab6 10       		.byte	0x10
 3473 1ab7 3C03     		.value	0x33c
 3474 1ab9 570F0000 		.long	0xf57
 3475 1abd C71A0000 		.long	0x1ac7
 3476 1ac1 0E       		.uleb128 0xe
 3477 1ac2 961A0000 		.long	0x1a96
 3478 1ac6 00       		.byte	0
 3479 1ac7 3B       		.uleb128 0x3b
 3480 1ac8 00000000 		.long	.LASF335
 3481 1acc 10       		.byte	0x10
 3482 1acd 3E03     		.value	0x33e
 3483 1acf 570F0000 		.long	0xf57
 3484 1ad3 DD1A0000 		.long	0x1add
 3485 1ad7 0E       		.uleb128 0xe
 3486 1ad8 961A0000 		.long	0x1a96
 3487 1adc 00       		.byte	0
 3488 1add 3E       		.uleb128 0x3e
 3489 1ade 00000000 		.long	.LASF336
 3490 1ae2 10       		.byte	0x10
GAS LISTING /tmp/cca1HRfE.s 			page 68


 3491 1ae3 F2       		.byte	0xf2
 3492 1ae4 570F0000 		.long	0xf57
 3493 1ae8 F21A0000 		.long	0x1af2
 3494 1aec 0E       		.uleb128 0xe
 3495 1aed 961A0000 		.long	0x1a96
 3496 1af1 00       		.byte	0
 3497 1af2 3B       		.uleb128 0x3b
 3498 1af3 00000000 		.long	.LASF337
 3499 1af7 10       		.byte	0x10
 3500 1af8 1302     		.value	0x213
 3501 1afa 570F0000 		.long	0xf57
 3502 1afe 081B0000 		.long	0x1b08
 3503 1b02 0E       		.uleb128 0xe
 3504 1b03 961A0000 		.long	0x1a96
 3505 1b07 00       		.byte	0
 3506 1b08 3B       		.uleb128 0x3b
 3507 1b09 00000000 		.long	.LASF338
 3508 1b0d 10       		.byte	0x10
 3509 1b0e 1E03     		.value	0x31e
 3510 1b10 570F0000 		.long	0xf57
 3511 1b14 231B0000 		.long	0x1b23
 3512 1b18 0E       		.uleb128 0xe
 3513 1b19 961A0000 		.long	0x1a96
 3514 1b1d 0E       		.uleb128 0xe
 3515 1b1e 231B0000 		.long	0x1b23
 3516 1b22 00       		.byte	0
 3517 1b23 3A       		.uleb128 0x3a
 3518 1b24 08       		.byte	0x8
 3519 1b25 791A0000 		.long	0x1a79
 3520 1b29 3B       		.uleb128 0x3b
 3521 1b2a 00000000 		.long	.LASF339
 3522 1b2e 10       		.byte	0x10
 3523 1b2f 6E02     		.value	0x26e
 3524 1b31 A1120000 		.long	0x12a1
 3525 1b35 491B0000 		.long	0x1b49
 3526 1b39 0E       		.uleb128 0xe
 3527 1b3a A1120000 		.long	0x12a1
 3528 1b3e 0E       		.uleb128 0xe
 3529 1b3f 570F0000 		.long	0xf57
 3530 1b43 0E       		.uleb128 0xe
 3531 1b44 961A0000 		.long	0x1a96
 3532 1b48 00       		.byte	0
 3533 1b49 3B       		.uleb128 0x3b
 3534 1b4a 00000000 		.long	.LASF340
 3535 1b4e 10       		.byte	0x10
 3536 1b4f 1001     		.value	0x110
 3537 1b51 961A0000 		.long	0x1a96
 3538 1b55 641B0000 		.long	0x1b64
 3539 1b59 0E       		.uleb128 0xe
 3540 1b5a 800F0000 		.long	0xf80
 3541 1b5e 0E       		.uleb128 0xe
 3542 1b5f 800F0000 		.long	0xf80
 3543 1b63 00       		.byte	0
 3544 1b64 3B       		.uleb128 0x3b
 3545 1b65 00000000 		.long	.LASF341
 3546 1b69 10       		.byte	0x10
 3547 1b6a C502     		.value	0x2c5
GAS LISTING /tmp/cca1HRfE.s 			page 69


 3548 1b6c DE0E0000 		.long	0xede
 3549 1b70 891B0000 		.long	0x1b89
 3550 1b74 0E       		.uleb128 0xe
 3551 1b75 DC0E0000 		.long	0xedc
 3552 1b79 0E       		.uleb128 0xe
 3553 1b7a DE0E0000 		.long	0xede
 3554 1b7e 0E       		.uleb128 0xe
 3555 1b7f DE0E0000 		.long	0xede
 3556 1b83 0E       		.uleb128 0xe
 3557 1b84 961A0000 		.long	0x1a96
 3558 1b88 00       		.byte	0
 3559 1b89 3B       		.uleb128 0x3b
 3560 1b8a 00000000 		.long	.LASF342
 3561 1b8e 10       		.byte	0x10
 3562 1b8f 1601     		.value	0x116
 3563 1b91 961A0000 		.long	0x1a96
 3564 1b95 A91B0000 		.long	0x1ba9
 3565 1b99 0E       		.uleb128 0xe
 3566 1b9a 800F0000 		.long	0xf80
 3567 1b9e 0E       		.uleb128 0xe
 3568 1b9f 800F0000 		.long	0xf80
 3569 1ba3 0E       		.uleb128 0xe
 3570 1ba4 961A0000 		.long	0x1a96
 3571 1ba8 00       		.byte	0
 3572 1ba9 3B       		.uleb128 0x3b
 3573 1baa 00000000 		.long	.LASF343
 3574 1bae 10       		.byte	0x10
 3575 1baf ED02     		.value	0x2ed
 3576 1bb1 570F0000 		.long	0xf57
 3577 1bb5 C91B0000 		.long	0x1bc9
 3578 1bb9 0E       		.uleb128 0xe
 3579 1bba 961A0000 		.long	0x1a96
 3580 1bbe 0E       		.uleb128 0xe
 3581 1bbf 2C150000 		.long	0x152c
 3582 1bc3 0E       		.uleb128 0xe
 3583 1bc4 570F0000 		.long	0xf57
 3584 1bc8 00       		.byte	0
 3585 1bc9 3B       		.uleb128 0x3b
 3586 1bca 00000000 		.long	.LASF344
 3587 1bce 10       		.byte	0x10
 3588 1bcf 2303     		.value	0x323
 3589 1bd1 570F0000 		.long	0xf57
 3590 1bd5 E41B0000 		.long	0x1be4
 3591 1bd9 0E       		.uleb128 0xe
 3592 1bda 961A0000 		.long	0x1a96
 3593 1bde 0E       		.uleb128 0xe
 3594 1bdf E41B0000 		.long	0x1be4
 3595 1be3 00       		.byte	0
 3596 1be4 3A       		.uleb128 0x3a
 3597 1be5 08       		.byte	0x8
 3598 1be6 EA1B0000 		.long	0x1bea
 3599 1bea 0F       		.uleb128 0xf
 3600 1beb 791A0000 		.long	0x1a79
 3601 1bef 3B       		.uleb128 0x3b
 3602 1bf0 00000000 		.long	.LASF345
 3603 1bf4 10       		.byte	0x10
 3604 1bf5 F202     		.value	0x2f2
GAS LISTING /tmp/cca1HRfE.s 			page 70


 3605 1bf7 2C150000 		.long	0x152c
 3606 1bfb 051C0000 		.long	0x1c05
 3607 1bff 0E       		.uleb128 0xe
 3608 1c00 961A0000 		.long	0x1a96
 3609 1c04 00       		.byte	0
 3610 1c05 3B       		.uleb128 0x3b
 3611 1c06 00000000 		.long	.LASF346
 3612 1c0a 10       		.byte	0x10
 3613 1c0b 1402     		.value	0x214
 3614 1c0d 570F0000 		.long	0xf57
 3615 1c11 1B1C0000 		.long	0x1c1b
 3616 1c15 0E       		.uleb128 0xe
 3617 1c16 961A0000 		.long	0x1a96
 3618 1c1a 00       		.byte	0
 3619 1c1b 3D       		.uleb128 0x3d
 3620 1c1c 00000000 		.long	.LASF347
 3621 1c20 10       		.byte	0x10
 3622 1c21 1A02     		.value	0x21a
 3623 1c23 570F0000 		.long	0xf57
 3624 1c27 3B       		.uleb128 0x3b
 3625 1c28 00000000 		.long	.LASF348
 3626 1c2c 10       		.byte	0x10
 3627 1c2d 7E02     		.value	0x27e
 3628 1c2f A1120000 		.long	0x12a1
 3629 1c33 3D1C0000 		.long	0x1c3d
 3630 1c37 0E       		.uleb128 0xe
 3631 1c38 A1120000 		.long	0x12a1
 3632 1c3c 00       		.byte	0
 3633 1c3d 44       		.uleb128 0x44
 3634 1c3e 00000000 		.long	.LASF350
 3635 1c42 10       		.byte	0x10
 3636 1c43 4E03     		.value	0x34e
 3637 1c45 4F1C0000 		.long	0x1c4f
 3638 1c49 0E       		.uleb128 0xe
 3639 1c4a 800F0000 		.long	0xf80
 3640 1c4e 00       		.byte	0
 3641 1c4f 3E       		.uleb128 0x3e
 3642 1c50 00000000 		.long	.LASF351
 3643 1c54 10       		.byte	0x10
 3644 1c55 B2       		.byte	0xb2
 3645 1c56 570F0000 		.long	0xf57
 3646 1c5a 641C0000 		.long	0x1c64
 3647 1c5e 0E       		.uleb128 0xe
 3648 1c5f 800F0000 		.long	0xf80
 3649 1c63 00       		.byte	0
 3650 1c64 3E       		.uleb128 0x3e
 3651 1c65 00000000 		.long	.LASF352
 3652 1c69 10       		.byte	0x10
 3653 1c6a B4       		.byte	0xb4
 3654 1c6b 570F0000 		.long	0xf57
 3655 1c6f 7E1C0000 		.long	0x1c7e
 3656 1c73 0E       		.uleb128 0xe
 3657 1c74 800F0000 		.long	0xf80
 3658 1c78 0E       		.uleb128 0xe
 3659 1c79 800F0000 		.long	0xf80
 3660 1c7d 00       		.byte	0
 3661 1c7e 44       		.uleb128 0x44
GAS LISTING /tmp/cca1HRfE.s 			page 71


 3662 1c7f 00000000 		.long	.LASF353
 3663 1c83 10       		.byte	0x10
 3664 1c84 F702     		.value	0x2f7
 3665 1c86 901C0000 		.long	0x1c90
 3666 1c8a 0E       		.uleb128 0xe
 3667 1c8b 961A0000 		.long	0x1a96
 3668 1c8f 00       		.byte	0
 3669 1c90 44       		.uleb128 0x44
 3670 1c91 00000000 		.long	.LASF354
 3671 1c95 10       		.byte	0x10
 3672 1c96 4C01     		.value	0x14c
 3673 1c98 A71C0000 		.long	0x1ca7
 3674 1c9c 0E       		.uleb128 0xe
 3675 1c9d 961A0000 		.long	0x1a96
 3676 1ca1 0E       		.uleb128 0xe
 3677 1ca2 A1120000 		.long	0x12a1
 3678 1ca6 00       		.byte	0
 3679 1ca7 3B       		.uleb128 0x3b
 3680 1ca8 00000000 		.long	.LASF355
 3681 1cac 10       		.byte	0x10
 3682 1cad 5001     		.value	0x150
 3683 1caf 570F0000 		.long	0xf57
 3684 1cb3 CC1C0000 		.long	0x1ccc
 3685 1cb7 0E       		.uleb128 0xe
 3686 1cb8 961A0000 		.long	0x1a96
 3687 1cbc 0E       		.uleb128 0xe
 3688 1cbd A1120000 		.long	0x12a1
 3689 1cc1 0E       		.uleb128 0xe
 3690 1cc2 570F0000 		.long	0xf57
 3691 1cc6 0E       		.uleb128 0xe
 3692 1cc7 DE0E0000 		.long	0xede
 3693 1ccb 00       		.byte	0
 3694 1ccc 41       		.uleb128 0x41
 3695 1ccd 00000000 		.long	.LASF356
 3696 1cd1 10       		.byte	0x10
 3697 1cd2 C3       		.byte	0xc3
 3698 1cd3 961A0000 		.long	0x1a96
 3699 1cd7 3E       		.uleb128 0x3e
 3700 1cd8 00000000 		.long	.LASF357
 3701 1cdc 10       		.byte	0x10
 3702 1cdd D1       		.byte	0xd1
 3703 1cde A1120000 		.long	0x12a1
 3704 1ce2 EC1C0000 		.long	0x1cec
 3705 1ce6 0E       		.uleb128 0xe
 3706 1ce7 A1120000 		.long	0x12a1
 3707 1ceb 00       		.byte	0
 3708 1cec 3B       		.uleb128 0x3b
 3709 1ced 00000000 		.long	.LASF358
 3710 1cf1 10       		.byte	0x10
 3711 1cf2 BE02     		.value	0x2be
 3712 1cf4 570F0000 		.long	0xf57
 3713 1cf8 071D0000 		.long	0x1d07
 3714 1cfc 0E       		.uleb128 0xe
 3715 1cfd 570F0000 		.long	0xf57
 3716 1d01 0E       		.uleb128 0xe
 3717 1d02 961A0000 		.long	0x1a96
 3718 1d06 00       		.byte	0
GAS LISTING /tmp/cca1HRfE.s 			page 72


 3719 1d07 45       		.uleb128 0x45
 3720 1d08 5A090000 		.long	0x95a
 3721 1d0c 00000000 		.quad	.LFB644
 3721      00000000 
 3722 1d14 12000000 		.quad	.LFE644-.LFB644
 3722      00000000 
 3723 1d1c 01       		.uleb128 0x1
 3724 1d1d 9C       		.byte	0x9c
 3725 1d1e 3F1D0000 		.long	0x1d3f
 3726 1d22 46       		.uleb128 0x46
 3727 1d23 5F5F6100 		.string	"__a"
 3728 1d27 01       		.byte	0x1
 3729 1d28 81       		.byte	0x81
 3730 1d29 1E050000 		.long	0x51e
 3731 1d2d 02       		.uleb128 0x2
 3732 1d2e 91       		.byte	0x91
 3733 1d2f 6C       		.sleb128 -20
 3734 1d30 46       		.uleb128 0x46
 3735 1d31 5F5F6200 		.string	"__b"
 3736 1d35 01       		.byte	0x1
 3737 1d36 81       		.byte	0x81
 3738 1d37 1E050000 		.long	0x51e
 3739 1d3b 02       		.uleb128 0x2
 3740 1d3c 91       		.byte	0x91
 3741 1d3d 68       		.sleb128 -24
 3742 1d3e 00       		.byte	0
 3743 1d3f 47       		.uleb128 0x47
 3744 1d40 00000000 		.long	.LASF359
 3745 1d44 02       		.byte	0x2
 3746 1d45 1A       		.byte	0x1a
 3747 1d46 00000000 		.long	.LASF360
 3748 1d4a 00000000 		.quad	.LFB1118
 3748      00000000 
 3749 1d52 3A000000 		.quad	.LFE1118-.LFB1118
 3749      00000000 
 3750 1d5a 01       		.uleb128 0x1
 3751 1d5b 9C       		.byte	0x9c
 3752 1d5c 6E1D0000 		.long	0x1d6e
 3753 1d60 46       		.uleb128 0x46
 3754 1d61 6F7300   		.string	"os"
 3755 1d64 02       		.byte	0x2
 3756 1d65 1A       		.byte	0x1a
 3757 1d66 741D0000 		.long	0x1d74
 3758 1d6a 02       		.uleb128 0x2
 3759 1d6b 91       		.byte	0x91
 3760 1d6c 68       		.sleb128 -24
 3761 1d6d 00       		.byte	0
 3762 1d6e 40       		.uleb128 0x40
 3763 1d6f 08       		.byte	0x8
 3764 1d70 78090000 		.long	0x978
 3765 1d74 0F       		.uleb128 0xf
 3766 1d75 6E1D0000 		.long	0x1d6e
 3767 1d79 48       		.uleb128 0x48
 3768 1d7a 00000000 		.long	.LASF361
 3769 1d7e 02       		.byte	0x2
 3770 1d7f 1F       		.byte	0x1f
 3771 1d80 570F0000 		.long	0xf57
GAS LISTING /tmp/cca1HRfE.s 			page 73


 3772 1d84 00000000 		.quad	.LFB1119
 3772      00000000 
 3773 1d8c 62010000 		.quad	.LFE1119-.LFB1119
 3773      00000000 
 3774 1d94 01       		.uleb128 0x1
 3775 1d95 9C       		.byte	0x9c
 3776 1d96 B91D0000 		.long	0x1db9
 3777 1d9a 49       		.uleb128 0x49
 3778 1d9b 00000000 		.long	.LASF363
 3779 1d9f 02       		.byte	0x2
 3780 1da0 24       		.byte	0x24
 3781 1da1 83090000 		.long	0x983
 3782 1da5 03       		.uleb128 0x3
 3783 1da6 91       		.byte	0x91
 3784 1da7 D07B     		.sleb128 -560
 3785 1da9 49       		.uleb128 0x49
 3786 1daa 00000000 		.long	.LASF364
 3787 1dae 02       		.byte	0x2
 3788 1daf 27       		.byte	0x27
 3789 1db0 AA090000 		.long	0x9aa
 3790 1db4 03       		.uleb128 0x3
 3791 1db5 91       		.byte	0x91
 3792 1db6 C078     		.sleb128 -960
 3793 1db8 00       		.byte	0
 3794 1db9 4A       		.uleb128 0x4a
 3795 1dba 00000000 		.long	.LASF388
 3796 1dbe 00000000 		.quad	.LFB1260
 3796      00000000 
 3797 1dc6 3E000000 		.quad	.LFE1260-.LFB1260
 3797      00000000 
 3798 1dce 01       		.uleb128 0x1
 3799 1dcf 9C       		.byte	0x9c
 3800 1dd0 F11D0000 		.long	0x1df1
 3801 1dd4 4B       		.uleb128 0x4b
 3802 1dd5 00000000 		.long	.LASF365
 3803 1dd9 02       		.byte	0x2
 3804 1dda 2C       		.byte	0x2c
 3805 1ddb 570F0000 		.long	0xf57
 3806 1ddf 02       		.uleb128 0x2
 3807 1de0 91       		.byte	0x91
 3808 1de1 6C       		.sleb128 -20
 3809 1de2 4B       		.uleb128 0x4b
 3810 1de3 00000000 		.long	.LASF366
 3811 1de7 02       		.byte	0x2
 3812 1de8 2C       		.byte	0x2c
 3813 1de9 570F0000 		.long	0xf57
 3814 1ded 02       		.uleb128 0x2
 3815 1dee 91       		.byte	0x91
 3816 1def 68       		.sleb128 -24
 3817 1df0 00       		.byte	0
 3818 1df1 4C       		.uleb128 0x4c
 3819 1df2 00000000 		.long	.LASF389
 3820 1df6 00000000 		.quad	.LFB1261
 3820      00000000 
 3821 1dfe 15000000 		.quad	.LFE1261-.LFB1261
 3821      00000000 
 3822 1e06 01       		.uleb128 0x1
GAS LISTING /tmp/cca1HRfE.s 			page 74


 3823 1e07 9C       		.byte	0x9c
 3824 1e08 4D       		.uleb128 0x4d
 3825 1e09 00000000 		.long	.LASF367
 3826 1e0d DC0E0000 		.long	0xedc
 3827 1e11 4E       		.uleb128 0x4e
 3828 1e12 C4090000 		.long	0x9c4
 3829 1e16 09       		.uleb128 0x9
 3830 1e17 03       		.byte	0x3
 3831 1e18 00000000 		.quad	_ZStL8__ioinit
 3831      00000000 
 3832 1e20 4F       		.uleb128 0x4f
 3833 1e21 C30B0000 		.long	0xbc3
 3834 1e25 00000000 		.long	.LASF368
 3835 1e29 80808080 		.sleb128 -2147483648
 3835      78
 3836 1e2e 50       		.uleb128 0x50
 3837 1e2f CE0B0000 		.long	0xbce
 3838 1e33 00000000 		.long	.LASF369
 3839 1e37 FFFFFF7F 		.long	0x7fffffff
 3840 1e3b 51       		.uleb128 0x51
 3841 1e3c 260C0000 		.long	0xc26
 3842 1e40 00000000 		.long	.LASF370
 3843 1e44 40       		.byte	0x40
 3844 1e45 51       		.uleb128 0x51
 3845 1e46 520C0000 		.long	0xc52
 3846 1e4a 00000000 		.long	.LASF371
 3847 1e4e 7F       		.byte	0x7f
 3848 1e4f 4F       		.uleb128 0x4f
 3849 1e50 890C0000 		.long	0xc89
 3850 1e54 00000000 		.long	.LASF372
 3851 1e58 80807E   		.sleb128 -32768
 3852 1e5b 52       		.uleb128 0x52
 3853 1e5c 940C0000 		.long	0xc94
 3854 1e60 00000000 		.long	.LASF373
 3855 1e64 FF7F     		.value	0x7fff
 3856 1e66 4F       		.uleb128 0x4f
 3857 1e67 C70C0000 		.long	0xcc7
 3858 1e6b 00000000 		.long	.LASF374
 3859 1e6f 80808080 		.sleb128 -9223372036854775808
 3859      80808080 
 3859      807F
 3860 1e79 53       		.uleb128 0x53
 3861 1e7a D20C0000 		.long	0xcd2
 3862 1e7e 00000000 		.long	.LASF375
 3863 1e82 FFFFFFFF 		.quad	0x7fffffffffffffff
 3863      FFFFFF7F 
 3864 1e8a 00       		.byte	0
 3865              		.section	.debug_abbrev,"",@progbits
 3866              	.Ldebug_abbrev0:
 3867 0000 01       		.uleb128 0x1
 3868 0001 11       		.uleb128 0x11
 3869 0002 01       		.byte	0x1
 3870 0003 25       		.uleb128 0x25
 3871 0004 0E       		.uleb128 0xe
 3872 0005 13       		.uleb128 0x13
 3873 0006 0B       		.uleb128 0xb
 3874 0007 03       		.uleb128 0x3
GAS LISTING /tmp/cca1HRfE.s 			page 75


 3875 0008 0E       		.uleb128 0xe
 3876 0009 1B       		.uleb128 0x1b
 3877 000a 0E       		.uleb128 0xe
 3878 000b 55       		.uleb128 0x55
 3879 000c 17       		.uleb128 0x17
 3880 000d 11       		.uleb128 0x11
 3881 000e 01       		.uleb128 0x1
 3882 000f 10       		.uleb128 0x10
 3883 0010 17       		.uleb128 0x17
 3884 0011 00       		.byte	0
 3885 0012 00       		.byte	0
 3886 0013 02       		.uleb128 0x2
 3887 0014 39       		.uleb128 0x39
 3888 0015 01       		.byte	0x1
 3889 0016 03       		.uleb128 0x3
 3890 0017 08       		.uleb128 0x8
 3891 0018 3A       		.uleb128 0x3a
 3892 0019 0B       		.uleb128 0xb
 3893 001a 3B       		.uleb128 0x3b
 3894 001b 0B       		.uleb128 0xb
 3895 001c 01       		.uleb128 0x1
 3896 001d 13       		.uleb128 0x13
 3897 001e 00       		.byte	0
 3898 001f 00       		.byte	0
 3899 0020 03       		.uleb128 0x3
 3900 0021 39       		.uleb128 0x39
 3901 0022 01       		.byte	0x1
 3902 0023 03       		.uleb128 0x3
 3903 0024 0E       		.uleb128 0xe
 3904 0025 3A       		.uleb128 0x3a
 3905 0026 0B       		.uleb128 0xb
 3906 0027 3B       		.uleb128 0x3b
 3907 0028 0B       		.uleb128 0xb
 3908 0029 01       		.uleb128 0x1
 3909 002a 13       		.uleb128 0x13
 3910 002b 00       		.byte	0
 3911 002c 00       		.byte	0
 3912 002d 04       		.uleb128 0x4
 3913 002e 02       		.uleb128 0x2
 3914 002f 01       		.byte	0x1
 3915 0030 03       		.uleb128 0x3
 3916 0031 0E       		.uleb128 0xe
 3917 0032 3C       		.uleb128 0x3c
 3918 0033 19       		.uleb128 0x19
 3919 0034 00       		.byte	0
 3920 0035 00       		.byte	0
 3921 0036 05       		.uleb128 0x5
 3922 0037 2F       		.uleb128 0x2f
 3923 0038 00       		.byte	0
 3924 0039 03       		.uleb128 0x3
 3925 003a 0E       		.uleb128 0xe
 3926 003b 49       		.uleb128 0x49
 3927 003c 13       		.uleb128 0x13
 3928 003d 00       		.byte	0
 3929 003e 00       		.byte	0
 3930 003f 06       		.uleb128 0x6
 3931 0040 2F       		.uleb128 0x2f
GAS LISTING /tmp/cca1HRfE.s 			page 76


 3932 0041 00       		.byte	0
 3933 0042 03       		.uleb128 0x3
 3934 0043 0E       		.uleb128 0xe
 3935 0044 49       		.uleb128 0x49
 3936 0045 13       		.uleb128 0x13
 3937 0046 1E       		.uleb128 0x1e
 3938 0047 19       		.uleb128 0x19
 3939 0048 00       		.byte	0
 3940 0049 00       		.byte	0
 3941 004a 07       		.uleb128 0x7
 3942 004b 3A       		.uleb128 0x3a
 3943 004c 00       		.byte	0
 3944 004d 3A       		.uleb128 0x3a
 3945 004e 0B       		.uleb128 0xb
 3946 004f 3B       		.uleb128 0x3b
 3947 0050 0B       		.uleb128 0xb
 3948 0051 18       		.uleb128 0x18
 3949 0052 13       		.uleb128 0x13
 3950 0053 00       		.byte	0
 3951 0054 00       		.byte	0
 3952 0055 08       		.uleb128 0x8
 3953 0056 08       		.uleb128 0x8
 3954 0057 00       		.byte	0
 3955 0058 3A       		.uleb128 0x3a
 3956 0059 0B       		.uleb128 0xb
 3957 005a 3B       		.uleb128 0x3b
 3958 005b 0B       		.uleb128 0xb
 3959 005c 18       		.uleb128 0x18
 3960 005d 13       		.uleb128 0x13
 3961 005e 00       		.byte	0
 3962 005f 00       		.byte	0
 3963 0060 09       		.uleb128 0x9
 3964 0061 08       		.uleb128 0x8
 3965 0062 00       		.byte	0
 3966 0063 3A       		.uleb128 0x3a
 3967 0064 0B       		.uleb128 0xb
 3968 0065 3B       		.uleb128 0x3b
 3969 0066 05       		.uleb128 0x5
 3970 0067 18       		.uleb128 0x18
 3971 0068 13       		.uleb128 0x13
 3972 0069 00       		.byte	0
 3973 006a 00       		.byte	0
 3974 006b 0A       		.uleb128 0xa
 3975 006c 39       		.uleb128 0x39
 3976 006d 00       		.byte	0
 3977 006e 03       		.uleb128 0x3
 3978 006f 0E       		.uleb128 0xe
 3979 0070 3A       		.uleb128 0x3a
 3980 0071 0B       		.uleb128 0xb
 3981 0072 3B       		.uleb128 0x3b
 3982 0073 0B       		.uleb128 0xb
 3983 0074 00       		.byte	0
 3984 0075 00       		.byte	0
 3985 0076 0B       		.uleb128 0xb
 3986 0077 13       		.uleb128 0x13
 3987 0078 01       		.byte	0x1
 3988 0079 03       		.uleb128 0x3
GAS LISTING /tmp/cca1HRfE.s 			page 77


 3989 007a 0E       		.uleb128 0xe
 3990 007b 0B       		.uleb128 0xb
 3991 007c 0B       		.uleb128 0xb
 3992 007d 3A       		.uleb128 0x3a
 3993 007e 0B       		.uleb128 0xb
 3994 007f 3B       		.uleb128 0x3b
 3995 0080 0B       		.uleb128 0xb
 3996 0081 01       		.uleb128 0x1
 3997 0082 13       		.uleb128 0x13
 3998 0083 00       		.byte	0
 3999 0084 00       		.byte	0
 4000 0085 0C       		.uleb128 0xc
 4001 0086 16       		.uleb128 0x16
 4002 0087 00       		.byte	0
 4003 0088 03       		.uleb128 0x3
 4004 0089 0E       		.uleb128 0xe
 4005 008a 3A       		.uleb128 0x3a
 4006 008b 0B       		.uleb128 0xb
 4007 008c 3B       		.uleb128 0x3b
 4008 008d 0B       		.uleb128 0xb
 4009 008e 49       		.uleb128 0x49
 4010 008f 13       		.uleb128 0x13
 4011 0090 00       		.byte	0
 4012 0091 00       		.byte	0
 4013 0092 0D       		.uleb128 0xd
 4014 0093 2E       		.uleb128 0x2e
 4015 0094 01       		.byte	0x1
 4016 0095 3F       		.uleb128 0x3f
 4017 0096 19       		.uleb128 0x19
 4018 0097 03       		.uleb128 0x3
 4019 0098 0E       		.uleb128 0xe
 4020 0099 3A       		.uleb128 0x3a
 4021 009a 0B       		.uleb128 0xb
 4022 009b 3B       		.uleb128 0x3b
 4023 009c 0B       		.uleb128 0xb
 4024 009d 6E       		.uleb128 0x6e
 4025 009e 0E       		.uleb128 0xe
 4026 009f 3C       		.uleb128 0x3c
 4027 00a0 19       		.uleb128 0x19
 4028 00a1 01       		.uleb128 0x1
 4029 00a2 13       		.uleb128 0x13
 4030 00a3 00       		.byte	0
 4031 00a4 00       		.byte	0
 4032 00a5 0E       		.uleb128 0xe
 4033 00a6 05       		.uleb128 0x5
 4034 00a7 00       		.byte	0
 4035 00a8 49       		.uleb128 0x49
 4036 00a9 13       		.uleb128 0x13
 4037 00aa 00       		.byte	0
 4038 00ab 00       		.byte	0
 4039 00ac 0F       		.uleb128 0xf
 4040 00ad 26       		.uleb128 0x26
 4041 00ae 00       		.byte	0
 4042 00af 49       		.uleb128 0x49
 4043 00b0 13       		.uleb128 0x13
 4044 00b1 00       		.byte	0
 4045 00b2 00       		.byte	0
GAS LISTING /tmp/cca1HRfE.s 			page 78


 4046 00b3 10       		.uleb128 0x10
 4047 00b4 2E       		.uleb128 0x2e
 4048 00b5 01       		.byte	0x1
 4049 00b6 3F       		.uleb128 0x3f
 4050 00b7 19       		.uleb128 0x19
 4051 00b8 03       		.uleb128 0x3
 4052 00b9 08       		.uleb128 0x8
 4053 00ba 3A       		.uleb128 0x3a
 4054 00bb 0B       		.uleb128 0xb
 4055 00bc 3B       		.uleb128 0x3b
 4056 00bd 0B       		.uleb128 0xb
 4057 00be 6E       		.uleb128 0x6e
 4058 00bf 0E       		.uleb128 0xe
 4059 00c0 49       		.uleb128 0x49
 4060 00c1 13       		.uleb128 0x13
 4061 00c2 3C       		.uleb128 0x3c
 4062 00c3 19       		.uleb128 0x19
 4063 00c4 01       		.uleb128 0x1
 4064 00c5 13       		.uleb128 0x13
 4065 00c6 00       		.byte	0
 4066 00c7 00       		.byte	0
 4067 00c8 11       		.uleb128 0x11
 4068 00c9 2E       		.uleb128 0x2e
 4069 00ca 01       		.byte	0x1
 4070 00cb 3F       		.uleb128 0x3f
 4071 00cc 19       		.uleb128 0x19
 4072 00cd 03       		.uleb128 0x3
 4073 00ce 0E       		.uleb128 0xe
 4074 00cf 3A       		.uleb128 0x3a
 4075 00d0 0B       		.uleb128 0xb
 4076 00d1 3B       		.uleb128 0x3b
 4077 00d2 05       		.uleb128 0x5
 4078 00d3 6E       		.uleb128 0x6e
 4079 00d4 0E       		.uleb128 0xe
 4080 00d5 49       		.uleb128 0x49
 4081 00d6 13       		.uleb128 0x13
 4082 00d7 3C       		.uleb128 0x3c
 4083 00d8 19       		.uleb128 0x19
 4084 00d9 01       		.uleb128 0x1
 4085 00da 13       		.uleb128 0x13
 4086 00db 00       		.byte	0
 4087 00dc 00       		.byte	0
 4088 00dd 12       		.uleb128 0x12
 4089 00de 2E       		.uleb128 0x2e
 4090 00df 00       		.byte	0
 4091 00e0 3F       		.uleb128 0x3f
 4092 00e1 19       		.uleb128 0x19
 4093 00e2 03       		.uleb128 0x3
 4094 00e3 08       		.uleb128 0x8
 4095 00e4 3A       		.uleb128 0x3a
 4096 00e5 0B       		.uleb128 0xb
 4097 00e6 3B       		.uleb128 0x3b
 4098 00e7 05       		.uleb128 0x5
 4099 00e8 6E       		.uleb128 0x6e
 4100 00e9 0E       		.uleb128 0xe
 4101 00ea 49       		.uleb128 0x49
 4102 00eb 13       		.uleb128 0x13
GAS LISTING /tmp/cca1HRfE.s 			page 79


 4103 00ec 3C       		.uleb128 0x3c
 4104 00ed 19       		.uleb128 0x19
 4105 00ee 00       		.byte	0
 4106 00ef 00       		.byte	0
 4107 00f0 13       		.uleb128 0x13
 4108 00f1 2E       		.uleb128 0x2e
 4109 00f2 01       		.byte	0x1
 4110 00f3 3F       		.uleb128 0x3f
 4111 00f4 19       		.uleb128 0x19
 4112 00f5 03       		.uleb128 0x3
 4113 00f6 0E       		.uleb128 0xe
 4114 00f7 3A       		.uleb128 0x3a
 4115 00f8 0B       		.uleb128 0xb
 4116 00f9 3B       		.uleb128 0x3b
 4117 00fa 05       		.uleb128 0x5
 4118 00fb 6E       		.uleb128 0x6e
 4119 00fc 0E       		.uleb128 0xe
 4120 00fd 49       		.uleb128 0x49
 4121 00fe 13       		.uleb128 0x13
 4122 00ff 3C       		.uleb128 0x3c
 4123 0100 19       		.uleb128 0x19
 4124 0101 00       		.byte	0
 4125 0102 00       		.byte	0
 4126 0103 14       		.uleb128 0x14
 4127 0104 02       		.uleb128 0x2
 4128 0105 01       		.byte	0x1
 4129 0106 03       		.uleb128 0x3
 4130 0107 0E       		.uleb128 0xe
 4131 0108 0B       		.uleb128 0xb
 4132 0109 0B       		.uleb128 0xb
 4133 010a 3A       		.uleb128 0x3a
 4134 010b 0B       		.uleb128 0xb
 4135 010c 3B       		.uleb128 0x3b
 4136 010d 0B       		.uleb128 0xb
 4137 010e 01       		.uleb128 0x1
 4138 010f 13       		.uleb128 0x13
 4139 0110 00       		.byte	0
 4140 0111 00       		.byte	0
 4141 0112 15       		.uleb128 0x15
 4142 0113 1C       		.uleb128 0x1c
 4143 0114 00       		.byte	0
 4144 0115 49       		.uleb128 0x49
 4145 0116 13       		.uleb128 0x13
 4146 0117 38       		.uleb128 0x38
 4147 0118 0B       		.uleb128 0xb
 4148 0119 32       		.uleb128 0x32
 4149 011a 0B       		.uleb128 0xb
 4150 011b 00       		.byte	0
 4151 011c 00       		.byte	0
 4152 011d 16       		.uleb128 0x16
 4153 011e 2E       		.uleb128 0x2e
 4154 011f 01       		.byte	0x1
 4155 0120 3F       		.uleb128 0x3f
 4156 0121 19       		.uleb128 0x19
 4157 0122 03       		.uleb128 0x3
 4158 0123 0E       		.uleb128 0xe
 4159 0124 3A       		.uleb128 0x3a
GAS LISTING /tmp/cca1HRfE.s 			page 80


 4160 0125 0B       		.uleb128 0xb
 4161 0126 3B       		.uleb128 0x3b
 4162 0127 0B       		.uleb128 0xb
 4163 0128 6E       		.uleb128 0x6e
 4164 0129 0E       		.uleb128 0xe
 4165 012a 32       		.uleb128 0x32
 4166 012b 0B       		.uleb128 0xb
 4167 012c 3C       		.uleb128 0x3c
 4168 012d 19       		.uleb128 0x19
 4169 012e 64       		.uleb128 0x64
 4170 012f 13       		.uleb128 0x13
 4171 0130 01       		.uleb128 0x1
 4172 0131 13       		.uleb128 0x13
 4173 0132 00       		.byte	0
 4174 0133 00       		.byte	0
 4175 0134 17       		.uleb128 0x17
 4176 0135 05       		.uleb128 0x5
 4177 0136 00       		.byte	0
 4178 0137 49       		.uleb128 0x49
 4179 0138 13       		.uleb128 0x13
 4180 0139 34       		.uleb128 0x34
 4181 013a 19       		.uleb128 0x19
 4182 013b 00       		.byte	0
 4183 013c 00       		.byte	0
 4184 013d 18       		.uleb128 0x18
 4185 013e 2E       		.uleb128 0x2e
 4186 013f 01       		.byte	0x1
 4187 0140 3F       		.uleb128 0x3f
 4188 0141 19       		.uleb128 0x19
 4189 0142 03       		.uleb128 0x3
 4190 0143 0E       		.uleb128 0xe
 4191 0144 3A       		.uleb128 0x3a
 4192 0145 0B       		.uleb128 0xb
 4193 0146 3B       		.uleb128 0x3b
 4194 0147 0B       		.uleb128 0xb
 4195 0148 6E       		.uleb128 0x6e
 4196 0149 0E       		.uleb128 0xe
 4197 014a 32       		.uleb128 0x32
 4198 014b 0B       		.uleb128 0xb
 4199 014c 3C       		.uleb128 0x3c
 4200 014d 19       		.uleb128 0x19
 4201 014e 64       		.uleb128 0x64
 4202 014f 13       		.uleb128 0x13
 4203 0150 00       		.byte	0
 4204 0151 00       		.byte	0
 4205 0152 19       		.uleb128 0x19
 4206 0153 04       		.uleb128 0x4
 4207 0154 01       		.byte	0x1
 4208 0155 03       		.uleb128 0x3
 4209 0156 0E       		.uleb128 0xe
 4210 0157 0B       		.uleb128 0xb
 4211 0158 0B       		.uleb128 0xb
 4212 0159 49       		.uleb128 0x49
 4213 015a 13       		.uleb128 0x13
 4214 015b 3A       		.uleb128 0x3a
 4215 015c 0B       		.uleb128 0xb
 4216 015d 3B       		.uleb128 0x3b
GAS LISTING /tmp/cca1HRfE.s 			page 81


 4217 015e 0B       		.uleb128 0xb
 4218 015f 01       		.uleb128 0x1
 4219 0160 13       		.uleb128 0x13
 4220 0161 00       		.byte	0
 4221 0162 00       		.byte	0
 4222 0163 1A       		.uleb128 0x1a
 4223 0164 28       		.uleb128 0x28
 4224 0165 00       		.byte	0
 4225 0166 03       		.uleb128 0x3
 4226 0167 0E       		.uleb128 0xe
 4227 0168 1C       		.uleb128 0x1c
 4228 0169 0B       		.uleb128 0xb
 4229 016a 00       		.byte	0
 4230 016b 00       		.byte	0
 4231 016c 1B       		.uleb128 0x1b
 4232 016d 28       		.uleb128 0x28
 4233 016e 00       		.byte	0
 4234 016f 03       		.uleb128 0x3
 4235 0170 0E       		.uleb128 0xe
 4236 0171 1C       		.uleb128 0x1c
 4237 0172 05       		.uleb128 0x5
 4238 0173 00       		.byte	0
 4239 0174 00       		.byte	0
 4240 0175 1C       		.uleb128 0x1c
 4241 0176 28       		.uleb128 0x28
 4242 0177 00       		.byte	0
 4243 0178 03       		.uleb128 0x3
 4244 0179 0E       		.uleb128 0xe
 4245 017a 1C       		.uleb128 0x1c
 4246 017b 06       		.uleb128 0x6
 4247 017c 00       		.byte	0
 4248 017d 00       		.byte	0
 4249 017e 1D       		.uleb128 0x1d
 4250 017f 28       		.uleb128 0x28
 4251 0180 00       		.byte	0
 4252 0181 03       		.uleb128 0x3
 4253 0182 0E       		.uleb128 0xe
 4254 0183 1C       		.uleb128 0x1c
 4255 0184 0D       		.uleb128 0xd
 4256 0185 00       		.byte	0
 4257 0186 00       		.byte	0
 4258 0187 1E       		.uleb128 0x1e
 4259 0188 02       		.uleb128 0x2
 4260 0189 01       		.byte	0x1
 4261 018a 03       		.uleb128 0x3
 4262 018b 0E       		.uleb128 0xe
 4263 018c 3C       		.uleb128 0x3c
 4264 018d 19       		.uleb128 0x19
 4265 018e 01       		.uleb128 0x1
 4266 018f 13       		.uleb128 0x13
 4267 0190 00       		.byte	0
 4268 0191 00       		.byte	0
 4269 0192 1F       		.uleb128 0x1f
 4270 0193 02       		.uleb128 0x2
 4271 0194 01       		.byte	0x1
 4272 0195 03       		.uleb128 0x3
 4273 0196 0E       		.uleb128 0xe
GAS LISTING /tmp/cca1HRfE.s 			page 82


 4274 0197 0B       		.uleb128 0xb
 4275 0198 0B       		.uleb128 0xb
 4276 0199 3A       		.uleb128 0x3a
 4277 019a 0B       		.uleb128 0xb
 4278 019b 3B       		.uleb128 0x3b
 4279 019c 05       		.uleb128 0x5
 4280 019d 32       		.uleb128 0x32
 4281 019e 0B       		.uleb128 0xb
 4282 019f 01       		.uleb128 0x1
 4283 01a0 13       		.uleb128 0x13
 4284 01a1 00       		.byte	0
 4285 01a2 00       		.byte	0
 4286 01a3 20       		.uleb128 0x20
 4287 01a4 0D       		.uleb128 0xd
 4288 01a5 00       		.byte	0
 4289 01a6 03       		.uleb128 0x3
 4290 01a7 0E       		.uleb128 0xe
 4291 01a8 3A       		.uleb128 0x3a
 4292 01a9 0B       		.uleb128 0xb
 4293 01aa 3B       		.uleb128 0x3b
 4294 01ab 05       		.uleb128 0x5
 4295 01ac 49       		.uleb128 0x49
 4296 01ad 13       		.uleb128 0x13
 4297 01ae 3F       		.uleb128 0x3f
 4298 01af 19       		.uleb128 0x19
 4299 01b0 3C       		.uleb128 0x3c
 4300 01b1 19       		.uleb128 0x19
 4301 01b2 00       		.byte	0
 4302 01b3 00       		.byte	0
 4303 01b4 21       		.uleb128 0x21
 4304 01b5 2E       		.uleb128 0x2e
 4305 01b6 01       		.byte	0x1
 4306 01b7 3F       		.uleb128 0x3f
 4307 01b8 19       		.uleb128 0x19
 4308 01b9 03       		.uleb128 0x3
 4309 01ba 0E       		.uleb128 0xe
 4310 01bb 3A       		.uleb128 0x3a
 4311 01bc 0B       		.uleb128 0xb
 4312 01bd 3B       		.uleb128 0x3b
 4313 01be 05       		.uleb128 0x5
 4314 01bf 6E       		.uleb128 0x6e
 4315 01c0 0E       		.uleb128 0xe
 4316 01c1 32       		.uleb128 0x32
 4317 01c2 0B       		.uleb128 0xb
 4318 01c3 3C       		.uleb128 0x3c
 4319 01c4 19       		.uleb128 0x19
 4320 01c5 64       		.uleb128 0x64
 4321 01c6 13       		.uleb128 0x13
 4322 01c7 01       		.uleb128 0x1
 4323 01c8 13       		.uleb128 0x13
 4324 01c9 00       		.byte	0
 4325 01ca 00       		.byte	0
 4326 01cb 22       		.uleb128 0x22
 4327 01cc 2E       		.uleb128 0x2e
 4328 01cd 01       		.byte	0x1
 4329 01ce 3F       		.uleb128 0x3f
 4330 01cf 19       		.uleb128 0x19
GAS LISTING /tmp/cca1HRfE.s 			page 83


 4331 01d0 03       		.uleb128 0x3
 4332 01d1 0E       		.uleb128 0xe
 4333 01d2 3A       		.uleb128 0x3a
 4334 01d3 0B       		.uleb128 0xb
 4335 01d4 3B       		.uleb128 0x3b
 4336 01d5 05       		.uleb128 0x5
 4337 01d6 6E       		.uleb128 0x6e
 4338 01d7 0E       		.uleb128 0xe
 4339 01d8 32       		.uleb128 0x32
 4340 01d9 0B       		.uleb128 0xb
 4341 01da 3C       		.uleb128 0x3c
 4342 01db 19       		.uleb128 0x19
 4343 01dc 64       		.uleb128 0x64
 4344 01dd 13       		.uleb128 0x13
 4345 01de 00       		.byte	0
 4346 01df 00       		.byte	0
 4347 01e0 23       		.uleb128 0x23
 4348 01e1 16       		.uleb128 0x16
 4349 01e2 00       		.byte	0
 4350 01e3 03       		.uleb128 0x3
 4351 01e4 0E       		.uleb128 0xe
 4352 01e5 3A       		.uleb128 0x3a
 4353 01e6 0B       		.uleb128 0xb
 4354 01e7 3B       		.uleb128 0x3b
 4355 01e8 05       		.uleb128 0x5
 4356 01e9 49       		.uleb128 0x49
 4357 01ea 13       		.uleb128 0x13
 4358 01eb 32       		.uleb128 0x32
 4359 01ec 0B       		.uleb128 0xb
 4360 01ed 00       		.byte	0
 4361 01ee 00       		.byte	0
 4362 01ef 24       		.uleb128 0x24
 4363 01f0 0D       		.uleb128 0xd
 4364 01f1 00       		.byte	0
 4365 01f2 03       		.uleb128 0x3
 4366 01f3 0E       		.uleb128 0xe
 4367 01f4 3A       		.uleb128 0x3a
 4368 01f5 0B       		.uleb128 0xb
 4369 01f6 3B       		.uleb128 0x3b
 4370 01f7 05       		.uleb128 0x5
 4371 01f8 49       		.uleb128 0x49
 4372 01f9 13       		.uleb128 0x13
 4373 01fa 3F       		.uleb128 0x3f
 4374 01fb 19       		.uleb128 0x19
 4375 01fc 32       		.uleb128 0x32
 4376 01fd 0B       		.uleb128 0xb
 4377 01fe 3C       		.uleb128 0x3c
 4378 01ff 19       		.uleb128 0x19
 4379 0200 1C       		.uleb128 0x1c
 4380 0201 0B       		.uleb128 0xb
 4381 0202 00       		.byte	0
 4382 0203 00       		.byte	0
 4383 0204 25       		.uleb128 0x25
 4384 0205 0D       		.uleb128 0xd
 4385 0206 00       		.byte	0
 4386 0207 03       		.uleb128 0x3
 4387 0208 08       		.uleb128 0x8
GAS LISTING /tmp/cca1HRfE.s 			page 84


 4388 0209 3A       		.uleb128 0x3a
 4389 020a 0B       		.uleb128 0xb
 4390 020b 3B       		.uleb128 0x3b
 4391 020c 05       		.uleb128 0x5
 4392 020d 49       		.uleb128 0x49
 4393 020e 13       		.uleb128 0x13
 4394 020f 3F       		.uleb128 0x3f
 4395 0210 19       		.uleb128 0x19
 4396 0211 32       		.uleb128 0x32
 4397 0212 0B       		.uleb128 0xb
 4398 0213 3C       		.uleb128 0x3c
 4399 0214 19       		.uleb128 0x19
 4400 0215 1C       		.uleb128 0x1c
 4401 0216 0B       		.uleb128 0xb
 4402 0217 00       		.byte	0
 4403 0218 00       		.byte	0
 4404 0219 26       		.uleb128 0x26
 4405 021a 0D       		.uleb128 0xd
 4406 021b 00       		.byte	0
 4407 021c 03       		.uleb128 0x3
 4408 021d 0E       		.uleb128 0xe
 4409 021e 3A       		.uleb128 0x3a
 4410 021f 0B       		.uleb128 0xb
 4411 0220 3B       		.uleb128 0x3b
 4412 0221 05       		.uleb128 0x5
 4413 0222 49       		.uleb128 0x49
 4414 0223 13       		.uleb128 0x13
 4415 0224 3F       		.uleb128 0x3f
 4416 0225 19       		.uleb128 0x19
 4417 0226 32       		.uleb128 0x32
 4418 0227 0B       		.uleb128 0xb
 4419 0228 3C       		.uleb128 0x3c
 4420 0229 19       		.uleb128 0x19
 4421 022a 1C       		.uleb128 0x1c
 4422 022b 05       		.uleb128 0x5
 4423 022c 00       		.byte	0
 4424 022d 00       		.byte	0
 4425 022e 27       		.uleb128 0x27
 4426 022f 2E       		.uleb128 0x2e
 4427 0230 01       		.byte	0x1
 4428 0231 3F       		.uleb128 0x3f
 4429 0232 19       		.uleb128 0x19
 4430 0233 03       		.uleb128 0x3
 4431 0234 0E       		.uleb128 0xe
 4432 0235 3A       		.uleb128 0x3a
 4433 0236 0B       		.uleb128 0xb
 4434 0237 3B       		.uleb128 0x3b
 4435 0238 0B       		.uleb128 0xb
 4436 0239 6E       		.uleb128 0x6e
 4437 023a 0E       		.uleb128 0xe
 4438 023b 49       		.uleb128 0x49
 4439 023c 13       		.uleb128 0x13
 4440 023d 3C       		.uleb128 0x3c
 4441 023e 19       		.uleb128 0x19
 4442 023f 01       		.uleb128 0x1
 4443 0240 13       		.uleb128 0x13
 4444 0241 00       		.byte	0
GAS LISTING /tmp/cca1HRfE.s 			page 85


 4445 0242 00       		.byte	0
 4446 0243 28       		.uleb128 0x28
 4447 0244 34       		.uleb128 0x34
 4448 0245 00       		.byte	0
 4449 0246 03       		.uleb128 0x3
 4450 0247 0E       		.uleb128 0xe
 4451 0248 3A       		.uleb128 0x3a
 4452 0249 0B       		.uleb128 0xb
 4453 024a 3B       		.uleb128 0x3b
 4454 024b 0B       		.uleb128 0xb
 4455 024c 6E       		.uleb128 0x6e
 4456 024d 0E       		.uleb128 0xe
 4457 024e 49       		.uleb128 0x49
 4458 024f 13       		.uleb128 0x13
 4459 0250 3F       		.uleb128 0x3f
 4460 0251 19       		.uleb128 0x19
 4461 0252 3C       		.uleb128 0x3c
 4462 0253 19       		.uleb128 0x19
 4463 0254 00       		.byte	0
 4464 0255 00       		.byte	0
 4465 0256 29       		.uleb128 0x29
 4466 0257 34       		.uleb128 0x34
 4467 0258 00       		.byte	0
 4468 0259 03       		.uleb128 0x3
 4469 025a 0E       		.uleb128 0xe
 4470 025b 3A       		.uleb128 0x3a
 4471 025c 0B       		.uleb128 0xb
 4472 025d 3B       		.uleb128 0x3b
 4473 025e 0B       		.uleb128 0xb
 4474 025f 49       		.uleb128 0x49
 4475 0260 13       		.uleb128 0x13
 4476 0261 3C       		.uleb128 0x3c
 4477 0262 19       		.uleb128 0x19
 4478 0263 00       		.byte	0
 4479 0264 00       		.byte	0
 4480 0265 2A       		.uleb128 0x2a
 4481 0266 16       		.uleb128 0x16
 4482 0267 00       		.byte	0
 4483 0268 03       		.uleb128 0x3
 4484 0269 0E       		.uleb128 0xe
 4485 026a 3A       		.uleb128 0x3a
 4486 026b 0B       		.uleb128 0xb
 4487 026c 3B       		.uleb128 0x3b
 4488 026d 0B       		.uleb128 0xb
 4489 026e 49       		.uleb128 0x49
 4490 026f 13       		.uleb128 0x13
 4491 0270 32       		.uleb128 0x32
 4492 0271 0B       		.uleb128 0xb
 4493 0272 00       		.byte	0
 4494 0273 00       		.byte	0
 4495 0274 2B       		.uleb128 0x2b
 4496 0275 2E       		.uleb128 0x2e
 4497 0276 01       		.byte	0x1
 4498 0277 3F       		.uleb128 0x3f
 4499 0278 19       		.uleb128 0x19
 4500 0279 03       		.uleb128 0x3
 4501 027a 0E       		.uleb128 0xe
GAS LISTING /tmp/cca1HRfE.s 			page 86


 4502 027b 3A       		.uleb128 0x3a
 4503 027c 0B       		.uleb128 0xb
 4504 027d 3B       		.uleb128 0x3b
 4505 027e 0B       		.uleb128 0xb
 4506 027f 6E       		.uleb128 0x6e
 4507 0280 0E       		.uleb128 0xe
 4508 0281 49       		.uleb128 0x49
 4509 0282 13       		.uleb128 0x13
 4510 0283 32       		.uleb128 0x32
 4511 0284 0B       		.uleb128 0xb
 4512 0285 3C       		.uleb128 0x3c
 4513 0286 19       		.uleb128 0x19
 4514 0287 64       		.uleb128 0x64
 4515 0288 13       		.uleb128 0x13
 4516 0289 01       		.uleb128 0x1
 4517 028a 13       		.uleb128 0x13
 4518 028b 00       		.byte	0
 4519 028c 00       		.byte	0
 4520 028d 2C       		.uleb128 0x2c
 4521 028e 2F       		.uleb128 0x2f
 4522 028f 00       		.byte	0
 4523 0290 03       		.uleb128 0x3
 4524 0291 08       		.uleb128 0x8
 4525 0292 49       		.uleb128 0x49
 4526 0293 13       		.uleb128 0x13
 4527 0294 00       		.byte	0
 4528 0295 00       		.byte	0
 4529 0296 2D       		.uleb128 0x2d
 4530 0297 0D       		.uleb128 0xd
 4531 0298 00       		.byte	0
 4532 0299 03       		.uleb128 0x3
 4533 029a 0E       		.uleb128 0xe
 4534 029b 3A       		.uleb128 0x3a
 4535 029c 0B       		.uleb128 0xb
 4536 029d 3B       		.uleb128 0x3b
 4537 029e 0B       		.uleb128 0xb
 4538 029f 49       		.uleb128 0x49
 4539 02a0 13       		.uleb128 0x13
 4540 02a1 3F       		.uleb128 0x3f
 4541 02a2 19       		.uleb128 0x19
 4542 02a3 3C       		.uleb128 0x3c
 4543 02a4 19       		.uleb128 0x19
 4544 02a5 00       		.byte	0
 4545 02a6 00       		.byte	0
 4546 02a7 2E       		.uleb128 0x2e
 4547 02a8 13       		.uleb128 0x13
 4548 02a9 01       		.byte	0x1
 4549 02aa 03       		.uleb128 0x3
 4550 02ab 0E       		.uleb128 0xe
 4551 02ac 0B       		.uleb128 0xb
 4552 02ad 0B       		.uleb128 0xb
 4553 02ae 3A       		.uleb128 0x3a
 4554 02af 0B       		.uleb128 0xb
 4555 02b0 3B       		.uleb128 0x3b
 4556 02b1 0B       		.uleb128 0xb
 4557 02b2 00       		.byte	0
 4558 02b3 00       		.byte	0
GAS LISTING /tmp/cca1HRfE.s 			page 87


 4559 02b4 2F       		.uleb128 0x2f
 4560 02b5 0D       		.uleb128 0xd
 4561 02b6 00       		.byte	0
 4562 02b7 03       		.uleb128 0x3
 4563 02b8 0E       		.uleb128 0xe
 4564 02b9 3A       		.uleb128 0x3a
 4565 02ba 0B       		.uleb128 0xb
 4566 02bb 3B       		.uleb128 0x3b
 4567 02bc 0B       		.uleb128 0xb
 4568 02bd 49       		.uleb128 0x49
 4569 02be 13       		.uleb128 0x13
 4570 02bf 38       		.uleb128 0x38
 4571 02c0 0B       		.uleb128 0xb
 4572 02c1 00       		.byte	0
 4573 02c2 00       		.byte	0
 4574 02c3 30       		.uleb128 0x30
 4575 02c4 0D       		.uleb128 0xd
 4576 02c5 00       		.byte	0
 4577 02c6 03       		.uleb128 0x3
 4578 02c7 0E       		.uleb128 0xe
 4579 02c8 3A       		.uleb128 0x3a
 4580 02c9 0B       		.uleb128 0xb
 4581 02ca 3B       		.uleb128 0x3b
 4582 02cb 05       		.uleb128 0x5
 4583 02cc 49       		.uleb128 0x49
 4584 02cd 13       		.uleb128 0x13
 4585 02ce 38       		.uleb128 0x38
 4586 02cf 0B       		.uleb128 0xb
 4587 02d0 00       		.byte	0
 4588 02d1 00       		.byte	0
 4589 02d2 31       		.uleb128 0x31
 4590 02d3 24       		.uleb128 0x24
 4591 02d4 00       		.byte	0
 4592 02d5 0B       		.uleb128 0xb
 4593 02d6 0B       		.uleb128 0xb
 4594 02d7 3E       		.uleb128 0x3e
 4595 02d8 0B       		.uleb128 0xb
 4596 02d9 03       		.uleb128 0x3
 4597 02da 0E       		.uleb128 0xe
 4598 02db 00       		.byte	0
 4599 02dc 00       		.byte	0
 4600 02dd 32       		.uleb128 0x32
 4601 02de 0F       		.uleb128 0xf
 4602 02df 00       		.byte	0
 4603 02e0 0B       		.uleb128 0xb
 4604 02e1 0B       		.uleb128 0xb
 4605 02e2 00       		.byte	0
 4606 02e3 00       		.byte	0
 4607 02e4 33       		.uleb128 0x33
 4608 02e5 16       		.uleb128 0x16
 4609 02e6 00       		.byte	0
 4610 02e7 03       		.uleb128 0x3
 4611 02e8 0E       		.uleb128 0xe
 4612 02e9 3A       		.uleb128 0x3a
 4613 02ea 0B       		.uleb128 0xb
 4614 02eb 3B       		.uleb128 0x3b
 4615 02ec 05       		.uleb128 0x5
GAS LISTING /tmp/cca1HRfE.s 			page 88


 4616 02ed 49       		.uleb128 0x49
 4617 02ee 13       		.uleb128 0x13
 4618 02ef 00       		.byte	0
 4619 02f0 00       		.byte	0
 4620 02f1 34       		.uleb128 0x34
 4621 02f2 13       		.uleb128 0x13
 4622 02f3 01       		.byte	0x1
 4623 02f4 0B       		.uleb128 0xb
 4624 02f5 0B       		.uleb128 0xb
 4625 02f6 3A       		.uleb128 0x3a
 4626 02f7 0B       		.uleb128 0xb
 4627 02f8 3B       		.uleb128 0x3b
 4628 02f9 0B       		.uleb128 0xb
 4629 02fa 6E       		.uleb128 0x6e
 4630 02fb 0E       		.uleb128 0xe
 4631 02fc 01       		.uleb128 0x1
 4632 02fd 13       		.uleb128 0x13
 4633 02fe 00       		.byte	0
 4634 02ff 00       		.byte	0
 4635 0300 35       		.uleb128 0x35
 4636 0301 17       		.uleb128 0x17
 4637 0302 01       		.byte	0x1
 4638 0303 0B       		.uleb128 0xb
 4639 0304 0B       		.uleb128 0xb
 4640 0305 3A       		.uleb128 0x3a
 4641 0306 0B       		.uleb128 0xb
 4642 0307 3B       		.uleb128 0x3b
 4643 0308 0B       		.uleb128 0xb
 4644 0309 01       		.uleb128 0x1
 4645 030a 13       		.uleb128 0x13
 4646 030b 00       		.byte	0
 4647 030c 00       		.byte	0
 4648 030d 36       		.uleb128 0x36
 4649 030e 0D       		.uleb128 0xd
 4650 030f 00       		.byte	0
 4651 0310 03       		.uleb128 0x3
 4652 0311 0E       		.uleb128 0xe
 4653 0312 3A       		.uleb128 0x3a
 4654 0313 0B       		.uleb128 0xb
 4655 0314 3B       		.uleb128 0x3b
 4656 0315 0B       		.uleb128 0xb
 4657 0316 49       		.uleb128 0x49
 4658 0317 13       		.uleb128 0x13
 4659 0318 00       		.byte	0
 4660 0319 00       		.byte	0
 4661 031a 37       		.uleb128 0x37
 4662 031b 01       		.uleb128 0x1
 4663 031c 01       		.byte	0x1
 4664 031d 49       		.uleb128 0x49
 4665 031e 13       		.uleb128 0x13
 4666 031f 01       		.uleb128 0x1
 4667 0320 13       		.uleb128 0x13
 4668 0321 00       		.byte	0
 4669 0322 00       		.byte	0
 4670 0323 38       		.uleb128 0x38
 4671 0324 21       		.uleb128 0x21
 4672 0325 00       		.byte	0
GAS LISTING /tmp/cca1HRfE.s 			page 89


 4673 0326 49       		.uleb128 0x49
 4674 0327 13       		.uleb128 0x13
 4675 0328 2F       		.uleb128 0x2f
 4676 0329 0B       		.uleb128 0xb
 4677 032a 00       		.byte	0
 4678 032b 00       		.byte	0
 4679 032c 39       		.uleb128 0x39
 4680 032d 24       		.uleb128 0x24
 4681 032e 00       		.byte	0
 4682 032f 0B       		.uleb128 0xb
 4683 0330 0B       		.uleb128 0xb
 4684 0331 3E       		.uleb128 0x3e
 4685 0332 0B       		.uleb128 0xb
 4686 0333 03       		.uleb128 0x3
 4687 0334 08       		.uleb128 0x8
 4688 0335 00       		.byte	0
 4689 0336 00       		.byte	0
 4690 0337 3A       		.uleb128 0x3a
 4691 0338 0F       		.uleb128 0xf
 4692 0339 00       		.byte	0
 4693 033a 0B       		.uleb128 0xb
 4694 033b 0B       		.uleb128 0xb
 4695 033c 49       		.uleb128 0x49
 4696 033d 13       		.uleb128 0x13
 4697 033e 00       		.byte	0
 4698 033f 00       		.byte	0
 4699 0340 3B       		.uleb128 0x3b
 4700 0341 2E       		.uleb128 0x2e
 4701 0342 01       		.byte	0x1
 4702 0343 3F       		.uleb128 0x3f
 4703 0344 19       		.uleb128 0x19
 4704 0345 03       		.uleb128 0x3
 4705 0346 0E       		.uleb128 0xe
 4706 0347 3A       		.uleb128 0x3a
 4707 0348 0B       		.uleb128 0xb
 4708 0349 3B       		.uleb128 0x3b
 4709 034a 05       		.uleb128 0x5
 4710 034b 49       		.uleb128 0x49
 4711 034c 13       		.uleb128 0x13
 4712 034d 3C       		.uleb128 0x3c
 4713 034e 19       		.uleb128 0x19
 4714 034f 01       		.uleb128 0x1
 4715 0350 13       		.uleb128 0x13
 4716 0351 00       		.byte	0
 4717 0352 00       		.byte	0
 4718 0353 3C       		.uleb128 0x3c
 4719 0354 18       		.uleb128 0x18
 4720 0355 00       		.byte	0
 4721 0356 00       		.byte	0
 4722 0357 00       		.byte	0
 4723 0358 3D       		.uleb128 0x3d
 4724 0359 2E       		.uleb128 0x2e
 4725 035a 00       		.byte	0
 4726 035b 3F       		.uleb128 0x3f
 4727 035c 19       		.uleb128 0x19
 4728 035d 03       		.uleb128 0x3
 4729 035e 0E       		.uleb128 0xe
GAS LISTING /tmp/cca1HRfE.s 			page 90


 4730 035f 3A       		.uleb128 0x3a
 4731 0360 0B       		.uleb128 0xb
 4732 0361 3B       		.uleb128 0x3b
 4733 0362 05       		.uleb128 0x5
 4734 0363 49       		.uleb128 0x49
 4735 0364 13       		.uleb128 0x13
 4736 0365 3C       		.uleb128 0x3c
 4737 0366 19       		.uleb128 0x19
 4738 0367 00       		.byte	0
 4739 0368 00       		.byte	0
 4740 0369 3E       		.uleb128 0x3e
 4741 036a 2E       		.uleb128 0x2e
 4742 036b 01       		.byte	0x1
 4743 036c 3F       		.uleb128 0x3f
 4744 036d 19       		.uleb128 0x19
 4745 036e 03       		.uleb128 0x3
 4746 036f 0E       		.uleb128 0xe
 4747 0370 3A       		.uleb128 0x3a
 4748 0371 0B       		.uleb128 0xb
 4749 0372 3B       		.uleb128 0x3b
 4750 0373 0B       		.uleb128 0xb
 4751 0374 49       		.uleb128 0x49
 4752 0375 13       		.uleb128 0x13
 4753 0376 3C       		.uleb128 0x3c
 4754 0377 19       		.uleb128 0x19
 4755 0378 01       		.uleb128 0x1
 4756 0379 13       		.uleb128 0x13
 4757 037a 00       		.byte	0
 4758 037b 00       		.byte	0
 4759 037c 3F       		.uleb128 0x3f
 4760 037d 13       		.uleb128 0x13
 4761 037e 01       		.byte	0x1
 4762 037f 03       		.uleb128 0x3
 4763 0380 08       		.uleb128 0x8
 4764 0381 0B       		.uleb128 0xb
 4765 0382 0B       		.uleb128 0xb
 4766 0383 3A       		.uleb128 0x3a
 4767 0384 0B       		.uleb128 0xb
 4768 0385 3B       		.uleb128 0x3b
 4769 0386 0B       		.uleb128 0xb
 4770 0387 01       		.uleb128 0x1
 4771 0388 13       		.uleb128 0x13
 4772 0389 00       		.byte	0
 4773 038a 00       		.byte	0
 4774 038b 40       		.uleb128 0x40
 4775 038c 10       		.uleb128 0x10
 4776 038d 00       		.byte	0
 4777 038e 0B       		.uleb128 0xb
 4778 038f 0B       		.uleb128 0xb
 4779 0390 49       		.uleb128 0x49
 4780 0391 13       		.uleb128 0x13
 4781 0392 00       		.byte	0
 4782 0393 00       		.byte	0
 4783 0394 41       		.uleb128 0x41
 4784 0395 2E       		.uleb128 0x2e
 4785 0396 00       		.byte	0
 4786 0397 3F       		.uleb128 0x3f
GAS LISTING /tmp/cca1HRfE.s 			page 91


 4787 0398 19       		.uleb128 0x19
 4788 0399 03       		.uleb128 0x3
 4789 039a 0E       		.uleb128 0xe
 4790 039b 3A       		.uleb128 0x3a
 4791 039c 0B       		.uleb128 0xb
 4792 039d 3B       		.uleb128 0x3b
 4793 039e 0B       		.uleb128 0xb
 4794 039f 49       		.uleb128 0x49
 4795 03a0 13       		.uleb128 0x13
 4796 03a1 3C       		.uleb128 0x3c
 4797 03a2 19       		.uleb128 0x19
 4798 03a3 00       		.byte	0
 4799 03a4 00       		.byte	0
 4800 03a5 42       		.uleb128 0x42
 4801 03a6 26       		.uleb128 0x26
 4802 03a7 00       		.byte	0
 4803 03a8 00       		.byte	0
 4804 03a9 00       		.byte	0
 4805 03aa 43       		.uleb128 0x43
 4806 03ab 16       		.uleb128 0x16
 4807 03ac 00       		.byte	0
 4808 03ad 03       		.uleb128 0x3
 4809 03ae 0E       		.uleb128 0xe
 4810 03af 3A       		.uleb128 0x3a
 4811 03b0 0B       		.uleb128 0xb
 4812 03b1 3B       		.uleb128 0x3b
 4813 03b2 0B       		.uleb128 0xb
 4814 03b3 00       		.byte	0
 4815 03b4 00       		.byte	0
 4816 03b5 44       		.uleb128 0x44
 4817 03b6 2E       		.uleb128 0x2e
 4818 03b7 01       		.byte	0x1
 4819 03b8 3F       		.uleb128 0x3f
 4820 03b9 19       		.uleb128 0x19
 4821 03ba 03       		.uleb128 0x3
 4822 03bb 0E       		.uleb128 0xe
 4823 03bc 3A       		.uleb128 0x3a
 4824 03bd 0B       		.uleb128 0xb
 4825 03be 3B       		.uleb128 0x3b
 4826 03bf 05       		.uleb128 0x5
 4827 03c0 3C       		.uleb128 0x3c
 4828 03c1 19       		.uleb128 0x19
 4829 03c2 01       		.uleb128 0x1
 4830 03c3 13       		.uleb128 0x13
 4831 03c4 00       		.byte	0
 4832 03c5 00       		.byte	0
 4833 03c6 45       		.uleb128 0x45
 4834 03c7 2E       		.uleb128 0x2e
 4835 03c8 01       		.byte	0x1
 4836 03c9 47       		.uleb128 0x47
 4837 03ca 13       		.uleb128 0x13
 4838 03cb 11       		.uleb128 0x11
 4839 03cc 01       		.uleb128 0x1
 4840 03cd 12       		.uleb128 0x12
 4841 03ce 07       		.uleb128 0x7
 4842 03cf 40       		.uleb128 0x40
 4843 03d0 18       		.uleb128 0x18
GAS LISTING /tmp/cca1HRfE.s 			page 92


 4844 03d1 9742     		.uleb128 0x2117
 4845 03d3 19       		.uleb128 0x19
 4846 03d4 01       		.uleb128 0x1
 4847 03d5 13       		.uleb128 0x13
 4848 03d6 00       		.byte	0
 4849 03d7 00       		.byte	0
 4850 03d8 46       		.uleb128 0x46
 4851 03d9 05       		.uleb128 0x5
 4852 03da 00       		.byte	0
 4853 03db 03       		.uleb128 0x3
 4854 03dc 08       		.uleb128 0x8
 4855 03dd 3A       		.uleb128 0x3a
 4856 03de 0B       		.uleb128 0xb
 4857 03df 3B       		.uleb128 0x3b
 4858 03e0 0B       		.uleb128 0xb
 4859 03e1 49       		.uleb128 0x49
 4860 03e2 13       		.uleb128 0x13
 4861 03e3 02       		.uleb128 0x2
 4862 03e4 18       		.uleb128 0x18
 4863 03e5 00       		.byte	0
 4864 03e6 00       		.byte	0
 4865 03e7 47       		.uleb128 0x47
 4866 03e8 2E       		.uleb128 0x2e
 4867 03e9 01       		.byte	0x1
 4868 03ea 3F       		.uleb128 0x3f
 4869 03eb 19       		.uleb128 0x19
 4870 03ec 03       		.uleb128 0x3
 4871 03ed 0E       		.uleb128 0xe
 4872 03ee 3A       		.uleb128 0x3a
 4873 03ef 0B       		.uleb128 0xb
 4874 03f0 3B       		.uleb128 0x3b
 4875 03f1 0B       		.uleb128 0xb
 4876 03f2 6E       		.uleb128 0x6e
 4877 03f3 0E       		.uleb128 0xe
 4878 03f4 11       		.uleb128 0x11
 4879 03f5 01       		.uleb128 0x1
 4880 03f6 12       		.uleb128 0x12
 4881 03f7 07       		.uleb128 0x7
 4882 03f8 40       		.uleb128 0x40
 4883 03f9 18       		.uleb128 0x18
 4884 03fa 9642     		.uleb128 0x2116
 4885 03fc 19       		.uleb128 0x19
 4886 03fd 01       		.uleb128 0x1
 4887 03fe 13       		.uleb128 0x13
 4888 03ff 00       		.byte	0
 4889 0400 00       		.byte	0
 4890 0401 48       		.uleb128 0x48
 4891 0402 2E       		.uleb128 0x2e
 4892 0403 01       		.byte	0x1
 4893 0404 3F       		.uleb128 0x3f
 4894 0405 19       		.uleb128 0x19
 4895 0406 03       		.uleb128 0x3
 4896 0407 0E       		.uleb128 0xe
 4897 0408 3A       		.uleb128 0x3a
 4898 0409 0B       		.uleb128 0xb
 4899 040a 3B       		.uleb128 0x3b
 4900 040b 0B       		.uleb128 0xb
GAS LISTING /tmp/cca1HRfE.s 			page 93


 4901 040c 49       		.uleb128 0x49
 4902 040d 13       		.uleb128 0x13
 4903 040e 11       		.uleb128 0x11
 4904 040f 01       		.uleb128 0x1
 4905 0410 12       		.uleb128 0x12
 4906 0411 07       		.uleb128 0x7
 4907 0412 40       		.uleb128 0x40
 4908 0413 18       		.uleb128 0x18
 4909 0414 9642     		.uleb128 0x2116
 4910 0416 19       		.uleb128 0x19
 4911 0417 01       		.uleb128 0x1
 4912 0418 13       		.uleb128 0x13
 4913 0419 00       		.byte	0
 4914 041a 00       		.byte	0
 4915 041b 49       		.uleb128 0x49
 4916 041c 34       		.uleb128 0x34
 4917 041d 00       		.byte	0
 4918 041e 03       		.uleb128 0x3
 4919 041f 0E       		.uleb128 0xe
 4920 0420 3A       		.uleb128 0x3a
 4921 0421 0B       		.uleb128 0xb
 4922 0422 3B       		.uleb128 0x3b
 4923 0423 0B       		.uleb128 0xb
 4924 0424 49       		.uleb128 0x49
 4925 0425 13       		.uleb128 0x13
 4926 0426 02       		.uleb128 0x2
 4927 0427 18       		.uleb128 0x18
 4928 0428 00       		.byte	0
 4929 0429 00       		.byte	0
 4930 042a 4A       		.uleb128 0x4a
 4931 042b 2E       		.uleb128 0x2e
 4932 042c 01       		.byte	0x1
 4933 042d 03       		.uleb128 0x3
 4934 042e 0E       		.uleb128 0xe
 4935 042f 34       		.uleb128 0x34
 4936 0430 19       		.uleb128 0x19
 4937 0431 11       		.uleb128 0x11
 4938 0432 01       		.uleb128 0x1
 4939 0433 12       		.uleb128 0x12
 4940 0434 07       		.uleb128 0x7
 4941 0435 40       		.uleb128 0x40
 4942 0436 18       		.uleb128 0x18
 4943 0437 9642     		.uleb128 0x2116
 4944 0439 19       		.uleb128 0x19
 4945 043a 01       		.uleb128 0x1
 4946 043b 13       		.uleb128 0x13
 4947 043c 00       		.byte	0
 4948 043d 00       		.byte	0
 4949 043e 4B       		.uleb128 0x4b
 4950 043f 05       		.uleb128 0x5
 4951 0440 00       		.byte	0
 4952 0441 03       		.uleb128 0x3
 4953 0442 0E       		.uleb128 0xe
 4954 0443 3A       		.uleb128 0x3a
 4955 0444 0B       		.uleb128 0xb
 4956 0445 3B       		.uleb128 0x3b
 4957 0446 0B       		.uleb128 0xb
GAS LISTING /tmp/cca1HRfE.s 			page 94


 4958 0447 49       		.uleb128 0x49
 4959 0448 13       		.uleb128 0x13
 4960 0449 02       		.uleb128 0x2
 4961 044a 18       		.uleb128 0x18
 4962 044b 00       		.byte	0
 4963 044c 00       		.byte	0
 4964 044d 4C       		.uleb128 0x4c
 4965 044e 2E       		.uleb128 0x2e
 4966 044f 00       		.byte	0
 4967 0450 03       		.uleb128 0x3
 4968 0451 0E       		.uleb128 0xe
 4969 0452 34       		.uleb128 0x34
 4970 0453 19       		.uleb128 0x19
 4971 0454 11       		.uleb128 0x11
 4972 0455 01       		.uleb128 0x1
 4973 0456 12       		.uleb128 0x12
 4974 0457 07       		.uleb128 0x7
 4975 0458 40       		.uleb128 0x40
 4976 0459 18       		.uleb128 0x18
 4977 045a 9642     		.uleb128 0x2116
 4978 045c 19       		.uleb128 0x19
 4979 045d 00       		.byte	0
 4980 045e 00       		.byte	0
 4981 045f 4D       		.uleb128 0x4d
 4982 0460 34       		.uleb128 0x34
 4983 0461 00       		.byte	0
 4984 0462 03       		.uleb128 0x3
 4985 0463 0E       		.uleb128 0xe
 4986 0464 49       		.uleb128 0x49
 4987 0465 13       		.uleb128 0x13
 4988 0466 3F       		.uleb128 0x3f
 4989 0467 19       		.uleb128 0x19
 4990 0468 34       		.uleb128 0x34
 4991 0469 19       		.uleb128 0x19
 4992 046a 3C       		.uleb128 0x3c
 4993 046b 19       		.uleb128 0x19
 4994 046c 00       		.byte	0
 4995 046d 00       		.byte	0
 4996 046e 4E       		.uleb128 0x4e
 4997 046f 34       		.uleb128 0x34
 4998 0470 00       		.byte	0
 4999 0471 47       		.uleb128 0x47
 5000 0472 13       		.uleb128 0x13
 5001 0473 02       		.uleb128 0x2
 5002 0474 18       		.uleb128 0x18
 5003 0475 00       		.byte	0
 5004 0476 00       		.byte	0
 5005 0477 4F       		.uleb128 0x4f
 5006 0478 34       		.uleb128 0x34
 5007 0479 00       		.byte	0
 5008 047a 47       		.uleb128 0x47
 5009 047b 13       		.uleb128 0x13
 5010 047c 6E       		.uleb128 0x6e
 5011 047d 0E       		.uleb128 0xe
 5012 047e 1C       		.uleb128 0x1c
 5013 047f 0D       		.uleb128 0xd
 5014 0480 00       		.byte	0
GAS LISTING /tmp/cca1HRfE.s 			page 95


 5015 0481 00       		.byte	0
 5016 0482 50       		.uleb128 0x50
 5017 0483 34       		.uleb128 0x34
 5018 0484 00       		.byte	0
 5019 0485 47       		.uleb128 0x47
 5020 0486 13       		.uleb128 0x13
 5021 0487 6E       		.uleb128 0x6e
 5022 0488 0E       		.uleb128 0xe
 5023 0489 1C       		.uleb128 0x1c
 5024 048a 06       		.uleb128 0x6
 5025 048b 00       		.byte	0
 5026 048c 00       		.byte	0
 5027 048d 51       		.uleb128 0x51
 5028 048e 34       		.uleb128 0x34
 5029 048f 00       		.byte	0
 5030 0490 47       		.uleb128 0x47
 5031 0491 13       		.uleb128 0x13
 5032 0492 6E       		.uleb128 0x6e
 5033 0493 0E       		.uleb128 0xe
 5034 0494 1C       		.uleb128 0x1c
 5035 0495 0B       		.uleb128 0xb
 5036 0496 00       		.byte	0
 5037 0497 00       		.byte	0
 5038 0498 52       		.uleb128 0x52
 5039 0499 34       		.uleb128 0x34
 5040 049a 00       		.byte	0
 5041 049b 47       		.uleb128 0x47
 5042 049c 13       		.uleb128 0x13
 5043 049d 6E       		.uleb128 0x6e
 5044 049e 0E       		.uleb128 0xe
 5045 049f 1C       		.uleb128 0x1c
 5046 04a0 05       		.uleb128 0x5
 5047 04a1 00       		.byte	0
 5048 04a2 00       		.byte	0
 5049 04a3 53       		.uleb128 0x53
 5050 04a4 34       		.uleb128 0x34
 5051 04a5 00       		.byte	0
 5052 04a6 47       		.uleb128 0x47
 5053 04a7 13       		.uleb128 0x13
 5054 04a8 6E       		.uleb128 0x6e
 5055 04a9 0E       		.uleb128 0xe
 5056 04aa 1C       		.uleb128 0x1c
 5057 04ab 07       		.uleb128 0x7
 5058 04ac 00       		.byte	0
 5059 04ad 00       		.byte	0
 5060 04ae 00       		.byte	0
 5061              		.section	.debug_aranges,"",@progbits
 5062 0000 3C000000 		.long	0x3c
 5063 0004 0200     		.value	0x2
 5064 0006 00000000 		.long	.Ldebug_info0
 5065 000a 08       		.byte	0x8
 5066 000b 00       		.byte	0
 5067 000c 0000     		.value	0
 5068 000e 0000     		.value	0
 5069 0010 00000000 		.quad	.Ltext0
 5069      00000000 
 5070 0018 EF010000 		.quad	.Letext0-.Ltext0
GAS LISTING /tmp/cca1HRfE.s 			page 96


 5070      00000000 
 5071 0020 00000000 		.quad	.LFB644
 5071      00000000 
 5072 0028 12000000 		.quad	.LFE644-.LFB644
 5072      00000000 
 5073 0030 00000000 		.quad	0
 5073      00000000 
 5074 0038 00000000 		.quad	0
 5074      00000000 
 5075              		.section	.debug_ranges,"",@progbits
 5076              	.Ldebug_ranges0:
 5077 0000 00000000 		.quad	.Ltext0
 5077      00000000 
 5078 0008 00000000 		.quad	.Letext0
 5078      00000000 
 5079 0010 00000000 		.quad	.LFB644
 5079      00000000 
 5080 0018 00000000 		.quad	.LFE644
 5080      00000000 
 5081 0020 00000000 		.quad	0
 5081      00000000 
 5082 0028 00000000 		.quad	0
 5082      00000000 
 5083              		.section	.debug_line,"",@progbits
 5084              	.Ldebug_line0:
 5085 0000 C4020000 		.section	.debug_str,"MS",@progbits,1
 5085      02005002 
 5085      00000101 
 5085      FB0E0D00 
 5085      01010101 
 5086              	.LASF337:
 5087 0000 66676574 		.string	"fgetc"
 5087      6300
 5088              	.LASF75:
 5089 0006 5F535F65 		.string	"_S_end"
 5089      6E6400
 5090              	.LASF26:
 5091 000d 73697A65 		.string	"size_t"
 5091      5F7400
 5092              	.LASF192:
 5093 0014 73697A65 		.string	"sizetype"
 5093      74797065 
 5093      00
 5094              	.LASF238:
 5095 001d 746D5F68 		.string	"tm_hour"
 5095      6F757200 
 5096              	.LASF149:
 5097 0025 5F5F6973 		.string	"__is_signed"
 5097      5F736967 
 5097      6E656400 
 5098              	.LASF63:
 5099 0031 5F535F69 		.string	"_S_ios_openmode_min"
 5099      6F735F6F 
 5099      70656E6D 
 5099      6F64655F 
 5099      6D696E00 
 5100              	.LASF146:
GAS LISTING /tmp/cca1HRfE.s 			page 97


 5101 0045 5F5F6E75 		.string	"__numeric_traits_integer<int>"
 5101      6D657269 
 5101      635F7472 
 5101      61697473 
 5101      5F696E74 
 5102              	.LASF332:
 5103 0063 66706F73 		.string	"fpos_t"
 5103      5F7400
 5104              	.LASF369:
 5105 006a 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIiE5__maxE"
 5105      5F5F676E 
 5105      755F6378 
 5105      7832345F 
 5105      5F6E756D 
 5106              	.LASF85:
 5107 009c 626F6F6C 		.string	"boolalpha"
 5107      616C7068 
 5107      6100
 5108              	.LASF168:
 5109 00a6 5F494F5F 		.string	"_IO_save_end"
 5109      73617665 
 5109      5F656E64 
 5109      00
 5110              	.LASF90:
 5111 00b3 73636965 		.string	"scientific"
 5111      6E746966 
 5111      696300
 5112              	.LASF148:
 5113 00be 5F5F6D61 		.string	"__max"
 5113      7800
 5114              	.LASF234:
 5115 00c4 77637363 		.string	"wcscspn"
 5115      73706E00 
 5116              	.LASF312:
 5117 00cc 6C6F6361 		.string	"localeconv"
 5117      6C65636F 
 5117      6E7600
 5118              	.LASF349:
 5119 00d7 636C6561 		.string	"clearerr"
 5119      72657272 
 5119      00
 5120              	.LASF324:
 5121 00e0 395F475F 		.string	"9_G_fpos_t"
 5121      66706F73 
 5121      5F7400
 5122              	.LASF52:
 5123 00eb 5F535F69 		.string	"_S_ios_fmtflags_min"
 5123      6F735F66 
 5123      6D74666C 
 5123      6167735F 
 5123      6D696E00 
 5124              	.LASF297:
 5125 00ff 66726163 		.string	"frac_digits"
 5125      5F646967 
 5125      69747300 
 5126              	.LASF161:
 5127 010b 5F494F5F 		.string	"_IO_write_base"
GAS LISTING /tmp/cca1HRfE.s 			page 98


 5127      77726974 
 5127      655F6261 
 5127      736500
 5128              	.LASF142:
 5129 011a 5F5A4E39 		.string	"_ZN9__gnu_cxx13new_allocatorIcE9constructEPcRKc"
 5129      5F5F676E 
 5129      755F6378 
 5129      7831336E 
 5129      65775F61 
 5130              	.LASF70:
 5131 014a 5F535F69 		.string	"_S_ios_iostate_max"
 5131      6F735F69 
 5131      6F737461 
 5131      74655F6D 
 5131      617800
 5132              	.LASF177:
 5133 015d 5F6C6F63 		.string	"_lock"
 5133      6B00
 5134              	.LASF289:
 5135 0163 696E745F 		.string	"int_curr_symbol"
 5135      63757272 
 5135      5F73796D 
 5135      626F6C00 
 5136              	.LASF127:
 5137 0173 6E65775F 		.string	"new_allocator"
 5137      616C6C6F 
 5137      6361746F 
 5137      7200
 5138              	.LASF104:
 5139 0181 676F6F64 		.string	"goodbit"
 5139      62697400 
 5140              	.LASF269:
 5141 0189 77637363 		.string	"wcschr"
 5141      687200
 5142              	.LASF32:
 5143 0190 5F535F62 		.string	"_S_boolalpha"
 5143      6F6F6C61 
 5143      6C706861 
 5143      00
 5144              	.LASF66:
 5145 019d 5F535F62 		.string	"_S_badbit"
 5145      61646269 
 5145      7400
 5146              	.LASF103:
 5147 01a7 6661696C 		.string	"failbit"
 5147      62697400 
 5148              	.LASF379:
 5149 01af 62617369 		.string	"basic_stringstream<char, std::char_traits<char>, std::allocator<char> >"
 5149      635F7374 
 5149      72696E67 
 5149      73747265 
 5149      616D3C63 
 5150              	.LASF159:
 5151 01f7 5F494F5F 		.string	"_IO_read_end"
 5151      72656164 
 5151      5F656E64 
 5151      00
GAS LISTING /tmp/cca1HRfE.s 			page 99


 5152              	.LASF166:
 5153 0204 5F494F5F 		.string	"_IO_save_base"
 5153      73617665 
 5153      5F626173 
 5153      6500
 5154              	.LASF215:
 5155 0212 6D627274 		.string	"mbrtowc"
 5155      6F776300 
 5156              	.LASF368:
 5157 021a 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIiE5__minE"
 5157      5F5F676E 
 5157      755F6378 
 5157      7832345F 
 5157      5F6E756D 
 5158              	.LASF141:
 5159 024c 636F6E73 		.string	"construct"
 5159      74727563 
 5159      7400
 5160              	.LASF261:
 5161 0256 77637378 		.string	"wcsxfrm"
 5161      66726D00 
 5162              	.LASF296:
 5163 025e 696E745F 		.string	"int_frac_digits"
 5163      66726163 
 5163      5F646967 
 5163      69747300 
 5164              	.LASF338:
 5165 026e 66676574 		.string	"fgetpos"
 5165      706F7300 
 5166              	.LASF325:
 5167 0276 5F5F706F 		.string	"__pos"
 5167      7300
 5168              	.LASF73:
 5169 027c 5F535F62 		.string	"_S_beg"
 5169      656700
 5170              	.LASF232:
 5171 0283 77637363 		.string	"wcscoll"
 5171      6F6C6C00 
 5172              	.LASF31:
 5173 028b 7E616C6C 		.string	"~allocator"
 5173      6F636174 
 5173      6F7200
 5174              	.LASF174:
 5175 0296 5F637572 		.string	"_cur_column"
 5175      5F636F6C 
 5175      756D6E00 
 5176              	.LASF94:
 5177 02a2 736B6970 		.string	"skipws"
 5177      777300
 5178              	.LASF196:
 5179 02a9 5F5F7763 		.string	"__wch"
 5179      6800
 5180              	.LASF14:
 5181 02af 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4moveEPcPKcm"
 5181      74313163 
 5181      6861725F 
 5181      74726169 
GAS LISTING /tmp/cca1HRfE.s 			page 100


 5181      74734963 
 5182              	.LASF96:
 5183 02d1 75707065 		.string	"uppercase"
 5183      72636173 
 5183      6500
 5184              	.LASF48:
 5185 02db 5F535F62 		.string	"_S_basefield"
 5185      61736566 
 5185      69656C64 
 5185      00
 5186              	.LASF22:
 5187 02e8 5F5A4E53 		.string	"_ZNSt11char_traitsIcE11to_int_typeERKc"
 5187      74313163 
 5187      6861725F 
 5187      74726169 
 5187      74734963 
 5188              	.LASF291:
 5189 030f 6D6F6E5F 		.string	"mon_decimal_point"
 5189      64656369 
 5189      6D616C5F 
 5189      706F696E 
 5189      7400
 5190              	.LASF130:
 5191 0321 7E6E6577 		.string	"~new_allocator"
 5191      5F616C6C 
 5191      6F636174 
 5191      6F7200
 5192              	.LASF155:
 5193 0330 46494C45 		.string	"FILE"
 5193      00
 5194              	.LASF259:
 5195 0335 6C6F6E67 		.string	"long int"
 5195      20696E74 
 5195      00
 5196              	.LASF244:
 5197 033e 746D5F69 		.string	"tm_isdst"
 5197      73647374 
 5197      00
 5198              	.LASF131:
 5199 0347 5F5A4E39 		.string	"_ZN9__gnu_cxx13new_allocatorIcED4Ev"
 5199      5F5F676E 
 5199      755F6378 
 5199      7831336E 
 5199      65775F61 
 5200              	.LASF153:
 5201 036b 5F5F6E75 		.string	"__numeric_traits_integer<char>"
 5201      6D657269 
 5201      635F7472 
 5201      61697473 
 5201      5F696E74 
 5202              	.LASF227:
 5203 038a 76777072 		.string	"vwprintf"
 5203      696E7466 
 5203      00
 5204              	.LASF105:
 5205 0393 62696E61 		.string	"binary"
 5205      727900
GAS LISTING /tmp/cca1HRfE.s 			page 101


 5206              	.LASF54:
 5207 039a 5F496F73 		.string	"_Ios_Openmode"
 5207      5F4F7065 
 5207      6E6D6F64 
 5207      6500
 5208              	.LASF4:
 5209 03a8 696E745F 		.string	"int_type"
 5209      74797065 
 5209      00
 5210              	.LASF328:
 5211 03b1 5F494F5F 		.string	"_IO_marker"
 5211      6D61726B 
 5211      657200
 5212              	.LASF361:
 5213 03bc 6D61696E 		.string	"main"
 5213      00
 5214              	.LASF306:
 5215 03c1 696E745F 		.string	"int_n_cs_precedes"
 5215      6E5F6373 
 5215      5F707265 
 5215      63656465 
 5215      7300
 5216              	.LASF320:
 5217 03d3 746F7763 		.string	"towctrans"
 5217      7472616E 
 5217      7300
 5218              	.LASF15:
 5219 03dd 636F7079 		.string	"copy"
 5219      00
 5220              	.LASF29:
 5221 03e2 5F5A4E53 		.string	"_ZNSaIcEC4Ev"
 5221      61496345 
 5221      43344576 
 5221      00
 5222              	.LASF364:
 5223 03ef 6D797374 		.string	"mystream"
 5223      7265616D 
 5223      00
 5224              	.LASF6:
 5225 03f8 5F5A4E53 		.string	"_ZNSt11char_traitsIcE2ltERKcS2_"
 5225      74313163 
 5225      6861725F 
 5225      74726169 
 5225      74734963 
 5226              	.LASF114:
 5227 0418 6F667374 		.string	"ofstream"
 5227      7265616D 
 5227      00
 5228              	.LASF362:
 5229 0421 5F5F696F 		.string	"__ioinit"
 5229      696E6974 
 5229      00
 5230              	.LASF78:
 5231 042a 5F535F73 		.string	"_S_synced_with_stdio"
 5231      796E6365 
 5231      645F7769 
 5231      74685F73 
GAS LISTING /tmp/cca1HRfE.s 			page 102


 5231      7464696F 
 5232              	.LASF151:
 5233 043f 5F56616C 		.string	"_Value"
 5233      756500
 5234              	.LASF67:
 5235 0446 5F535F65 		.string	"_S_eofbit"
 5235      6F666269 
 5235      7400
 5236              	.LASF243:
 5237 0450 746D5F79 		.string	"tm_yday"
 5237      64617900 
 5238              	.LASF281:
 5239 0458 7369676E 		.string	"signed char"
 5239      65642063 
 5239      68617200 
 5240              	.LASF156:
 5241 0464 5F494F5F 		.string	"_IO_FILE"
 5241      46494C45 
 5241      00
 5242              	.LASF351:
 5243 046d 72656D6F 		.string	"remove"
 5243      766500
 5244              	.LASF108:
 5245 0474 62617369 		.string	"basic_ostream<char, std::char_traits<char> >"
 5245      635F6F73 
 5245      74726561 
 5245      6D3C6368 
 5245      61722C20 
 5246              	.LASF199:
 5247 04a1 5F5F7661 		.string	"__value"
 5247      6C756500 
 5248              	.LASF317:
 5249 04a9 77637479 		.string	"wctype_t"
 5249      70655F74 
 5249      00
 5250              	.LASF205:
 5251 04b2 66676574 		.string	"fgetwc"
 5251      776300
 5252              	.LASF311:
 5253 04b9 67657477 		.string	"getwchar"
 5253      63686172 
 5253      00
 5254              	.LASF327:
 5255 04c2 5F475F66 		.string	"_G_fpos_t"
 5255      706F735F 
 5255      7400
 5256              	.LASF206:
 5257 04cc 66676574 		.string	"fgetws"
 5257      777300
 5258              	.LASF39:
 5259 04d3 5F535F72 		.string	"_S_right"
 5259      69676874 
 5259      00
 5260              	.LASF3:
 5261 04dc 63686172 		.string	"char_type"
 5261      5F747970 
 5261      6500
GAS LISTING /tmp/cca1HRfE.s 			page 103


 5262              	.LASF280:
 5263 04e6 756E7369 		.string	"unsigned char"
 5263      676E6564 
 5263      20636861 
 5263      7200
 5264              	.LASF301:
 5265 04f4 6E5F7365 		.string	"n_sep_by_space"
 5265      705F6279 
 5265      5F737061 
 5265      636500
 5266              	.LASF333:
 5267 0503 66636C6F 		.string	"fclose"
 5267      736500
 5268              	.LASF273:
 5269 050a 776D656D 		.string	"wmemchr"
 5269      63687200 
 5270              	.LASF65:
 5271 0512 5F535F67 		.string	"_S_goodbit"
 5271      6F6F6462 
 5271      697400
 5272              	.LASF134:
 5273 051d 5F5A4E4B 		.string	"_ZNK9__gnu_cxx13new_allocatorIcE7addressERKc"
 5273      395F5F67 
 5273      6E755F63 
 5273      78783133 
 5273      6E65775F 
 5274              	.LASF373:
 5275 054a 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIsE5__maxE"
 5275      5F5F676E 
 5275      755F6378 
 5275      7832345F 
 5275      5F6E756D 
 5276              	.LASF57:
 5277 057c 5F535F62 		.string	"_S_bin"
 5277      696E00
 5278              	.LASF110:
 5279 0583 6F706572 		.string	"operator|"
 5279      61746F72 
 5279      7C00
 5280              	.LASF25:
 5281 058d 6E6F745F 		.string	"not_eof"
 5281      656F6600 
 5282              	.LASF220:
 5283 0595 73777072 		.string	"swprintf"
 5283      696E7466 
 5283      00
 5284              	.LASF270:
 5285 059e 77637370 		.string	"wcspbrk"
 5285      62726B00 
 5286              	.LASF59:
 5287 05a6 5F535F6F 		.string	"_S_out"
 5287      757400
 5288              	.LASF200:
 5289 05ad 63686172 		.string	"char"
 5289      00
 5290              	.LASF121:
 5291 05b2 6E65775F 		.string	"new_allocator<char>"
GAS LISTING /tmp/cca1HRfE.s 			page 104


 5291      616C6C6F 
 5291      6361746F 
 5291      723C6368 
 5291      61723E00 
 5292              	.LASF55:
 5293 05c6 5F535F61 		.string	"_S_app"
 5293      707000
 5294              	.LASF202:
 5295 05cd 6D627374 		.string	"mbstate_t"
 5295      6174655F 
 5295      7400
 5296              	.LASF322:
 5297 05d7 77637479 		.string	"wctype"
 5297      706500
 5298              	.LASF82:
 5299 05de 6F70656E 		.string	"openmode"
 5299      6D6F6465 
 5299      00
 5300              	.LASF249:
 5301 05e7 7763736E 		.string	"wcsncmp"
 5301      636D7000 
 5302              	.LASF387:
 5303 05ef 5F494F5F 		.string	"_IO_lock_t"
 5303      6C6F636B 
 5303      5F7400
 5304              	.LASF309:
 5305 05fa 696E745F 		.string	"int_n_sign_posn"
 5305      6E5F7369 
 5305      676E5F70 
 5305      6F736E00 
 5306              	.LASF303:
 5307 060a 6E5F7369 		.string	"n_sign_posn"
 5307      676E5F70 
 5307      6F736E00 
 5308              	.LASF81:
 5309 0616 5F5A4E53 		.string	"_ZNSt8ios_base4InitD4Ev"
 5309      7438696F 
 5309      735F6261 
 5309      73653449 
 5309      6E697444 
 5310              	.LASF265:
 5311 062e 776D656D 		.string	"wmemmove"
 5311      6D6F7665 
 5311      00
 5312              	.LASF346:
 5313 0637 67657463 		.string	"getc"
 5313      00
 5314              	.LASF135:
 5315 063c 616C6C6F 		.string	"allocate"
 5315      63617465 
 5315      00
 5316              	.LASF147:
 5317 0645 5F5F6D69 		.string	"__min"
 5317      6E00
 5318              	.LASF204:
 5319 064b 62746F77 		.string	"btowc"
 5319      6300
GAS LISTING /tmp/cca1HRfE.s 			page 105


 5320              	.LASF348:
 5321 0651 67657473 		.string	"gets"
 5321      00
 5322              	.LASF158:
 5323 0656 5F494F5F 		.string	"_IO_read_ptr"
 5323      72656164 
 5323      5F707472 
 5323      00
 5324              	.LASF268:
 5325 0663 77736361 		.string	"wscanf"
 5325      6E6600
 5326              	.LASF292:
 5327 066a 6D6F6E5F 		.string	"mon_thousands_sep"
 5327      74686F75 
 5327      73616E64 
 5327      735F7365 
 5327      7000
 5328              	.LASF222:
 5329 067c 756E6765 		.string	"ungetwc"
 5329      74776300 
 5330              	.LASF189:
 5331 0684 66705F6F 		.string	"fp_offset"
 5331      66667365 
 5331      7400
 5332              	.LASF345:
 5333 068e 6674656C 		.string	"ftell"
 5333      6C00
 5334              	.LASF27:
 5335 0694 70747264 		.string	"ptrdiff_t"
 5335      6966665F 
 5335      7400
 5336              	.LASF137:
 5337 069e 6465616C 		.string	"deallocate"
 5337      6C6F6361 
 5337      746500
 5338              	.LASF318:
 5339 06a9 77637472 		.string	"wctrans_t"
 5339      616E735F 
 5339      7400
 5340              	.LASF214:
 5341 06b3 6D62726C 		.string	"mbrlen"
 5341      656E00
 5342              	.LASF331:
 5343 06ba 5F706F73 		.string	"_pos"
 5343      00
 5344              	.LASF295:
 5345 06bf 6E656761 		.string	"negative_sign"
 5345      74697665 
 5345      5F736967 
 5345      6E00
 5346              	.LASF35:
 5347 06cd 5F535F68 		.string	"_S_hex"
 5347      657800
 5348              	.LASF304:
 5349 06d4 696E745F 		.string	"int_p_cs_precedes"
 5349      705F6373 
 5349      5F707265 
GAS LISTING /tmp/cca1HRfE.s 			page 106


 5349      63656465 
 5349      7300
 5350              	.LASF123:
 5351 06e6 706F696E 		.string	"pointer"
 5351      74657200 
 5352              	.LASF71:
 5353 06ee 5F535F69 		.string	"_S_ios_iostate_min"
 5353      6F735F69 
 5353      6F737461 
 5353      74655F6D 
 5353      696E00
 5354              	.LASF384:
 5355 0701 636F7574 		.string	"cout"
 5355      00
 5356              	.LASF169:
 5357 0706 5F6D6172 		.string	"_markers"
 5357      6B657273 
 5357      00
 5358              	.LASF278:
 5359 070f 77637374 		.string	"wcstoull"
 5359      6F756C6C 
 5359      00
 5360              	.LASF36:
 5361 0718 5F535F69 		.string	"_S_internal"
 5361      6E746572 
 5361      6E616C00 
 5362              	.LASF7:
 5363 0724 636F6D70 		.string	"compare"
 5363      61726500 
 5364              	.LASF239:
 5365 072c 746D5F6D 		.string	"tm_mday"
 5365      64617900 
 5366              	.LASF98:
 5367 0734 62617365 		.string	"basefield"
 5367      6669656C 
 5367      6400
 5368              	.LASF233:
 5369 073e 77637363 		.string	"wcscpy"
 5369      707900
 5370              	.LASF109:
 5371 0745 5F436861 		.string	"_CharT"
 5371      725400
 5372              	.LASF86:
 5373 074c 66697865 		.string	"fixed"
 5373      6400
 5374              	.LASF225:
 5375 0752 76737770 		.string	"vswprintf"
 5375      72696E74 
 5375      6600
 5376              	.LASF266:
 5377 075c 776D656D 		.string	"wmemset"
 5377      73657400 
 5378              	.LASF83:
 5379 0764 7365656B 		.string	"seekdir"
 5379      64697200 
 5380              	.LASF210:
 5381 076c 66776964 		.string	"fwide"
GAS LISTING /tmp/cca1HRfE.s 			page 107


 5381      6500
 5382              	.LASF211:
 5383 0772 66777072 		.string	"fwprintf"
 5383      696E7466 
 5383      00
 5384              	.LASF144:
 5385 077b 5F5A4E39 		.string	"_ZN9__gnu_cxx13new_allocatorIcE7destroyEPc"
 5385      5F5F676E 
 5385      755F6378 
 5385      7831336E 
 5385      65775F61 
 5386              	.LASF88:
 5387 07a6 6C656674 		.string	"left"
 5387      00
 5388              	.LASF236:
 5389 07ab 746D5F73 		.string	"tm_sec"
 5389      656300
 5390              	.LASF178:
 5391 07b2 5F6F6666 		.string	"_offset"
 5391      73657400 
 5392              	.LASF250:
 5393 07ba 7763736E 		.string	"wcsncpy"
 5393      63707900 
 5394              	.LASF112:
 5395 07c2 5F5A5374 		.string	"_ZStorSt13_Ios_OpenmodeS_"
 5395      6F725374 
 5395      31335F49 
 5395      6F735F4F 
 5395      70656E6D 
 5396              	.LASF219:
 5397 07dc 70757477 		.string	"putwchar"
 5397      63686172 
 5397      00
 5398              	.LASF263:
 5399 07e5 776D656D 		.string	"wmemcmp"
 5399      636D7000 
 5400              	.LASF56:
 5401 07ed 5F535F61 		.string	"_S_ate"
 5401      746500
 5402              	.LASF16:
 5403 07f4 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4copyEPcPKcm"
 5403      74313163 
 5403      6861725F 
 5403      74726169 
 5403      74734963 
 5404              	.LASF126:
 5405 0816 636F6E73 		.string	"const_reference"
 5405      745F7265 
 5405      66657265 
 5405      6E636500 
 5406              	.LASF11:
 5407 0826 66696E64 		.string	"find"
 5407      00
 5408              	.LASF341:
 5409 082b 66726561 		.string	"fread"
 5409      6400
 5410              	.LASF34:
GAS LISTING /tmp/cca1HRfE.s 			page 108


 5411 0831 5F535F66 		.string	"_S_fixed"
 5411      69786564 
 5411      00
 5412              	.LASF307:
 5413 083a 696E745F 		.string	"int_n_sep_by_space"
 5413      6E5F7365 
 5413      705F6279 
 5413      5F737061 
 5413      636500
 5414              	.LASF366:
 5415 084d 5F5F7072 		.string	"__priority"
 5415      696F7269 
 5415      747900
 5416              	.LASF13:
 5417 0858 6D6F7665 		.string	"move"
 5417      00
 5418              	.LASF41:
 5419 085d 5F535F73 		.string	"_S_showbase"
 5419      686F7762 
 5419      61736500 
 5420              	.LASF125:
 5421 0869 72656665 		.string	"reference"
 5421      72656E63 
 5421      6500
 5422              	.LASF58:
 5423 0873 5F535F69 		.string	"_S_in"
 5423      6E00
 5424              	.LASF172:
 5425 0879 5F666C61 		.string	"_flags2"
 5425      67733200 
 5426              	.LASF132:
 5427 0881 61646472 		.string	"address"
 5427      65737300 
 5428              	.LASF283:
 5429 0889 5F5F676E 		.string	"__gnu_debug"
 5429      755F6465 
 5429      62756700 
 5430              	.LASF160:
 5431 0895 5F494F5F 		.string	"_IO_read_base"
 5431      72656164 
 5431      5F626173 
 5431      6500
 5432              	.LASF335:
 5433 08a3 66657272 		.string	"ferror"
 5433      6F7200
 5434              	.LASF389:
 5435 08aa 5F474C4F 		.string	"_GLOBAL__sub_I__Z15write_somethingRSo"
 5435      42414C5F 
 5435      5F737562 
 5435      5F495F5F 
 5435      5A313577 
 5436              	.LASF378:
 5437 08d0 2F686F6D 		.string	"/home/vinu/cs4230/par-lang/c++/discover_modern_c++/basics"
 5437      652F7669 
 5437      6E752F63 
 5437      73343233 
 5437      302F7061 
GAS LISTING /tmp/cca1HRfE.s 			page 109


 5438              	.LASF223:
 5439 090a 76667770 		.string	"vfwprintf"
 5439      72696E74 
 5439      6600
 5440              	.LASF185:
 5441 0914 5F756E75 		.string	"_unused2"
 5441      73656432 
 5441      00
 5442              	.LASF272:
 5443 091d 77637373 		.string	"wcsstr"
 5443      747200
 5444              	.LASF139:
 5445 0924 6D61785F 		.string	"max_size"
 5445      73697A65 
 5445      00
 5446              	.LASF226:
 5447 092d 76737773 		.string	"vswscanf"
 5447      63616E66 
 5447      00
 5448              	.LASF350:
 5449 0936 70657272 		.string	"perror"
 5449      6F7200
 5450              	.LASF299:
 5451 093d 705F7365 		.string	"p_sep_by_space"
 5451      705F6279 
 5451      5F737061 
 5451      636500
 5452              	.LASF23:
 5453 094c 65715F69 		.string	"eq_int_type"
 5453      6E745F74 
 5453      79706500 
 5454              	.LASF77:
 5455 0958 5F535F72 		.string	"_S_refcount"
 5455      6566636F 
 5455      756E7400 
 5456              	.LASF20:
 5457 0964 5F5A4E53 		.string	"_ZNSt11char_traitsIcE12to_char_typeERKi"
 5457      74313163 
 5457      6861725F 
 5457      74726169 
 5457      74734963 
 5458              	.LASF344:
 5459 098c 66736574 		.string	"fsetpos"
 5459      706F7300 
 5460              	.LASF60:
 5461 0994 5F535F74 		.string	"_S_trunc"
 5461      72756E63 
 5461      00
 5462              	.LASF365:
 5463 099d 5F5F696E 		.string	"__initialize_p"
 5463      69746961 
 5463      6C697A65 
 5463      5F7000
 5464              	.LASF138:
 5465 09ac 5F5A4E39 		.string	"_ZN9__gnu_cxx13new_allocatorIcE10deallocateEPcm"
 5465      5F5F676E 
 5465      755F6378 
GAS LISTING /tmp/cca1HRfE.s 			page 110


 5465      7831336E 
 5465      65775F61 
 5466              	.LASF89:
 5467 09dc 72696768 		.string	"right"
 5467      7400
 5468              	.LASF43:
 5469 09e2 5F535F73 		.string	"_S_showpos"
 5469      686F7770 
 5469      6F7300
 5470              	.LASF231:
 5471 09ed 77637363 		.string	"wcscmp"
 5471      6D7000
 5472              	.LASF201:
 5473 09f4 5F5F6D62 		.string	"__mbstate_t"
 5473      73746174 
 5473      655F7400 
 5474              	.LASF264:
 5475 0a00 776D656D 		.string	"wmemcpy"
 5475      63707900 
 5476              	.LASF240:
 5477 0a08 746D5F6D 		.string	"tm_mon"
 5477      6F6E00
 5478              	.LASF33:
 5479 0a0f 5F535F64 		.string	"_S_dec"
 5479      656300
 5480              	.LASF53:
 5481 0a16 5F496F73 		.string	"_Ios_Fmtflags"
 5481      5F466D74 
 5481      666C6167 
 5481      7300
 5482              	.LASF258:
 5483 0a24 77637374 		.string	"wcstol"
 5483      6F6C00
 5484              	.LASF254:
 5485 0a2b 646F7562 		.string	"double"
 5485      6C6500
 5486              	.LASF12:
 5487 0a32 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4findEPKcmRS1_"
 5487      74313163 
 5487      6861725F 
 5487      74726169 
 5487      74734963 
 5488              	.LASF163:
 5489 0a56 5F494F5F 		.string	"_IO_write_end"
 5489      77726974 
 5489      655F656E 
 5489      6400
 5490              	.LASF372:
 5491 0a64 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIsE5__minE"
 5491      5F5F676E 
 5491      755F6378 
 5491      7832345F 
 5491      5F6E756D 
 5492              	.LASF262:
 5493 0a96 7763746F 		.string	"wctob"
 5493      6200
 5494              	.LASF42:
GAS LISTING /tmp/cca1HRfE.s 			page 111


 5495 0a9c 5F535F73 		.string	"_S_showpoint"
 5495      686F7770 
 5495      6F696E74 
 5495      00
 5496              	.LASF44:
 5497 0aa9 5F535F73 		.string	"_S_skipws"
 5497      6B697077 
 5497      7300
 5498              	.LASF188:
 5499 0ab3 67705F6F 		.string	"gp_offset"
 5499      66667365 
 5499      7400
 5500              	.LASF47:
 5501 0abd 5F535F61 		.string	"_S_adjustfield"
 5501      646A7573 
 5501      74666965 
 5501      6C6400
 5502              	.LASF370:
 5503 0acc 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerImE8__digitsE"
 5503      5F5F676E 
 5503      755F6378 
 5503      7832345F 
 5503      5F6E756D 
 5504              	.LASF256:
 5505 0b01 666C6F61 		.string	"float"
 5505      7400
 5506              	.LASF87:
 5507 0b07 696E7465 		.string	"internal"
 5507      726E616C 
 5507      00
 5508              	.LASF385:
 5509 0b10 5F5A5374 		.string	"_ZSt4cout"
 5509      34636F75 
 5509      7400
 5510              	.LASF237:
 5511 0b1a 746D5F6D 		.string	"tm_min"
 5511      696E00
 5512              	.LASF136:
 5513 0b21 5F5A4E39 		.string	"_ZN9__gnu_cxx13new_allocatorIcE8allocateEmPKv"
 5513      5F5F676E 
 5513      755F6378 
 5513      7831336E 
 5513      65775F61 
 5514              	.LASF164:
 5515 0b4f 5F494F5F 		.string	"_IO_buf_base"
 5515      6275665F 
 5515      62617365 
 5515      00
 5516              	.LASF37:
 5517 0b5c 5F535F6C 		.string	"_S_left"
 5517      65667400 
 5518              	.LASF193:
 5519 0b64 756E7369 		.string	"unsigned int"
 5519      676E6564 
 5519      20696E74 
 5519      00
 5520              	.LASF374:
GAS LISTING /tmp/cca1HRfE.s 			page 112


 5521 0b71 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIlE5__minE"
 5521      5F5F676E 
 5521      755F6378 
 5521      7832345F 
 5521      5F6E756D 
 5522              	.LASF145:
 5523 0ba3 63686172 		.string	"char_traits<char>"
 5523      5F747261 
 5523      6974733C 
 5523      63686172 
 5523      3E00
 5524              	.LASF294:
 5525 0bb5 706F7369 		.string	"positive_sign"
 5525      74697665 
 5525      5F736967 
 5525      6E00
 5526              	.LASF61:
 5527 0bc3 5F535F69 		.string	"_S_ios_openmode_end"
 5527      6F735F6F 
 5527      70656E6D 
 5527      6F64655F 
 5527      656E6400 
 5528              	.LASF252:
 5529 0bd7 77637373 		.string	"wcsspn"
 5529      706E00
 5530              	.LASF302:
 5531 0bde 705F7369 		.string	"p_sign_posn"
 5531      676E5F70 
 5531      6F736E00 
 5532              	.LASF24:
 5533 0bea 5F5A4E53 		.string	"_ZNSt11char_traitsIcE11eq_int_typeERKiS2_"
 5533      74313163 
 5533      6861725F 
 5533      74726169 
 5533      74734963 
 5534              	.LASF363:
 5535 0c14 6D796669 		.string	"myfile"
 5535      6C6500
 5536              	.LASF173:
 5537 0c1b 5F6F6C64 		.string	"_old_offset"
 5537      5F6F6666 
 5537      73657400 
 5538              	.LASF179:
 5539 0c27 5F5F7061 		.string	"__pad1"
 5539      643100
 5540              	.LASF180:
 5541 0c2e 5F5F7061 		.string	"__pad2"
 5541      643200
 5542              	.LASF181:
 5543 0c35 5F5F7061 		.string	"__pad3"
 5543      643300
 5544              	.LASF182:
 5545 0c3c 5F5F7061 		.string	"__pad4"
 5545      643400
 5546              	.LASF183:
 5547 0c43 5F5F7061 		.string	"__pad5"
 5547      643500
GAS LISTING /tmp/cca1HRfE.s 			page 113


 5548              	.LASF355:
 5549 0c4a 73657476 		.string	"setvbuf"
 5549      62756600 
 5550              	.LASF330:
 5551 0c52 5F736275 		.string	"_sbuf"
 5551      6600
 5552              	.LASF360:
 5553 0c58 5F5A3135 		.string	"_Z15write_somethingRSo"
 5553      77726974 
 5553      655F736F 
 5553      6D657468 
 5553      696E6752 
 5554              	.LASF46:
 5555 0c6f 5F535F75 		.string	"_S_uppercase"
 5555      70706572 
 5555      63617365 
 5555      00
 5556              	.LASF316:
 5557 0c7c 5F41746F 		.string	"_Atomic_word"
 5557      6D69635F 
 5557      776F7264 
 5557      00
 5558              	.LASF91:
 5559 0c89 73686F77 		.string	"showbase"
 5559      62617365 
 5559      00
 5560              	.LASF190:
 5561 0c92 6F766572 		.string	"overflow_arg_area"
 5561      666C6F77 
 5561      5F617267 
 5561      5F617265 
 5561      6100
 5562              	.LASF352:
 5563 0ca4 72656E61 		.string	"rename"
 5563      6D6500
 5564              	.LASF157:
 5565 0cab 5F666C61 		.string	"_flags"
 5565      677300
 5566              	.LASF50:
 5567 0cb2 5F535F69 		.string	"_S_ios_fmtflags_end"
 5567      6F735F66 
 5567      6D74666C 
 5567      6167735F 
 5567      656E6400 
 5568              	.LASF79:
 5569 0cc6 496E6974 		.string	"Init"
 5569      00
 5570              	.LASF184:
 5571 0ccb 5F6D6F64 		.string	"_mode"
 5571      6500
 5572              	.LASF113:
 5573 0cd1 6F737472 		.string	"ostream"
 5573      65616D00 
 5574              	.LASF286:
 5575 0cd9 64656369 		.string	"decimal_point"
 5575      6D616C5F 
 5575      706F696E 
GAS LISTING /tmp/cca1HRfE.s 			page 114


 5575      7400
 5576              	.LASF347:
 5577 0ce7 67657463 		.string	"getchar"
 5577      68617200 
 5578              	.LASF198:
 5579 0cef 5F5F636F 		.string	"__count"
 5579      756E7400 
 5580              	.LASF117:
 5581 0cf7 5F5F676E 		.string	"__gnu_cxx"
 5581      755F6378 
 5581      7800
 5582              	.LASF284:
 5583 0d01 626F6F6C 		.string	"bool"
 5583      00
 5584              	.LASF376:
 5585 0d06 474E5520 		.string	"GNU C++ 5.4.1 20160904 -mtune=generic -march=x86-64 -g -O0 -fstack-protector-strong"
 5585      432B2B20 
 5585      352E342E 
 5585      31203230 
 5585      31363039 
 5586              	.LASF334:
 5587 0d5a 66656F66 		.string	"feof"
 5587      00
 5588              	.LASF18:
 5589 0d5f 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6assignEPcmc"
 5589      74313163 
 5589      6861725F 
 5589      74726169 
 5589      74734963 
 5590              	.LASF275:
 5591 0d81 6C6F6E67 		.string	"long double"
 5591      20646F75 
 5591      626C6500 
 5592              	.LASF128:
 5593 0d8d 5F5A4E39 		.string	"_ZN9__gnu_cxx13new_allocatorIcEC4Ev"
 5593      5F5F676E 
 5593      755F6378 
 5593      7831336E 
 5593      65775F61 
 5594              	.LASF218:
 5595 0db1 70757477 		.string	"putwc"
 5595      6300
 5596              	.LASF386:
 5597 0db7 5F5F6E75 		.string	"__numeric_traits_integer<long int>"
 5597      6D657269 
 5597      635F7472 
 5597      61697473 
 5597      5F696E74 
 5598              	.LASF93:
 5599 0dda 73686F77 		.string	"showpos"
 5599      706F7300 
 5600              	.LASF300:
 5601 0de2 6E5F6373 		.string	"n_cs_precedes"
 5601      5F707265 
 5601      63656465 
 5601      7300
 5602              	.LASF49:
GAS LISTING /tmp/cca1HRfE.s 			page 115


 5603 0df0 5F535F66 		.string	"_S_floatfield"
 5603      6C6F6174 
 5603      6669656C 
 5603      6400
 5604              	.LASF38:
 5605 0dfe 5F535F6F 		.string	"_S_oct"
 5605      637400
 5606              	.LASF197:
 5607 0e05 5F5F7763 		.string	"__wchb"
 5607      686200
 5608              	.LASF122:
 5609 0e0c 73697A65 		.string	"size_type"
 5609      5F747970 
 5609      6500
 5610              	.LASF359:
 5611 0e16 77726974 		.string	"write_something"
 5611      655F736F 
 5611      6D657468 
 5611      696E6700 
 5612              	.LASF111:
 5613 0e26 5F5A4E53 		.string	"_ZNSaIcED4Ev"
 5613      61496345 
 5613      44344576 
 5613      00
 5614              	.LASF388:
 5615 0e33 5F5F7374 		.string	"__static_initialization_and_destruction_0"
 5615      61746963 
 5615      5F696E69 
 5615      7469616C 
 5615      697A6174 
 5616              	.LASF9:
 5617 0e5d 5F5A4E53 		.string	"_ZNSt11char_traitsIcE7compareEPKcS2_m"
 5617      74313163 
 5617      6861725F 
 5617      74726169 
 5617      74734963 
 5618              	.LASF279:
 5619 0e83 6C6F6E67 		.string	"long long unsigned int"
 5619      206C6F6E 
 5619      6720756E 
 5619      7369676E 
 5619      65642069 
 5620              	.LASF143:
 5621 0e9a 64657374 		.string	"destroy"
 5621      726F7900 
 5622              	.LASF191:
 5623 0ea2 7265675F 		.string	"reg_save_area"
 5623      73617665 
 5623      5F617265 
 5623      6100
 5624              	.LASF274:
 5625 0eb0 77637374 		.string	"wcstold"
 5625      6F6C6400 
 5626              	.LASF305:
 5627 0eb8 696E745F 		.string	"int_p_sep_by_space"
 5627      705F7365 
 5627      705F6279 
GAS LISTING /tmp/cca1HRfE.s 			page 116


 5627      5F737061 
 5627      636500
 5628              	.LASF76:
 5629 0ecb 5F535F69 		.string	"_S_ios_seekdir_end"
 5629      6F735F73 
 5629      65656B64 
 5629      69725F65 
 5629      6E6400
 5630              	.LASF8:
 5631 0ede 6C656E67 		.string	"length"
 5631      746800
 5632              	.LASF276:
 5633 0ee5 77637374 		.string	"wcstoll"
 5633      6F6C6C00 
 5634              	.LASF314:
 5635 0eed 5F5F6F66 		.string	"__off_t"
 5635      665F7400 
 5636              	.LASF170:
 5637 0ef5 5F636861 		.string	"_chain"
 5637      696E00
 5638              	.LASF64:
 5639 0efc 5F496F73 		.string	"_Ios_Iostate"
 5639      5F496F73 
 5639      74617465 
 5639      00
 5640              	.LASF194:
 5641 0f09 6C6F6E67 		.string	"long unsigned int"
 5641      20756E73 
 5641      69676E65 
 5641      6420696E 
 5641      7400
 5642              	.LASF133:
 5643 0f1b 5F5A4E4B 		.string	"_ZNK9__gnu_cxx13new_allocatorIcE7addressERc"
 5643      395F5F67 
 5643      6E755F63 
 5643      78783133 
 5643      6E65775F 
 5644              	.LASF371:
 5645 0f47 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIcE5__maxE"
 5645      5F5F676E 
 5645      755F6378 
 5645      7832345F 
 5645      5F6E756D 
 5646              	.LASF354:
 5647 0f79 73657462 		.string	"setbuf"
 5647      756600
 5648              	.LASF251:
 5649 0f80 77637372 		.string	"wcsrtombs"
 5649      746F6D62 
 5649      7300
 5650              	.LASF97:
 5651 0f8a 61646A75 		.string	"adjustfield"
 5651      73746669 
 5651      656C6400 
 5652              	.LASF242:
 5653 0f96 746D5F77 		.string	"tm_wday"
 5653      64617900 
GAS LISTING /tmp/cca1HRfE.s 			page 117


 5654              	.LASF45:
 5655 0f9e 5F535F75 		.string	"_S_unitbuf"
 5655      6E697462 
 5655      756600
 5656              	.LASF30:
 5657 0fa9 5F5A4E53 		.string	"_ZNSaIcEC4ERKS_"
 5657      61496345 
 5657      43344552 
 5657      4B535F00 
 5658              	.LASF5:
 5659 0fb9 5F5A4E53 		.string	"_ZNSt11char_traitsIcE2eqERKcS2_"
 5659      74313163 
 5659      6861725F 
 5659      74726169 
 5659      74734963 
 5660              	.LASF129:
 5661 0fd9 5F5A4E39 		.string	"_ZN9__gnu_cxx13new_allocatorIcEC4ERKS1_"
 5661      5F5F676E 
 5661      755F6378 
 5661      7831336E 
 5661      65775F61 
 5662              	.LASF99:
 5663 1001 666C6F61 		.string	"floatfield"
 5663      74666965 
 5663      6C6400
 5664              	.LASF221:
 5665 100c 73777363 		.string	"swscanf"
 5665      616E6600 
 5666              	.LASF150:
 5667 1014 5F5F6469 		.string	"__digits"
 5667      67697473 
 5667      00
 5668              	.LASF253:
 5669 101d 77637374 		.string	"wcstod"
 5669      6F6400
 5670              	.LASF255:
 5671 1024 77637374 		.string	"wcstof"
 5671      6F6600
 5672              	.LASF257:
 5673 102b 77637374 		.string	"wcstok"
 5673      6F6B00
 5674              	.LASF0:
 5675 1032 5F5F6378 		.string	"__cxx11"
 5675      78313100 
 5676              	.LASF106:
 5677 103a 7472756E 		.string	"trunc"
 5677      6300
 5678              	.LASF186:
 5679 1040 5F5F4649 		.string	"__FILE"
 5679      4C4500
 5680              	.LASF92:
 5681 1047 73686F77 		.string	"showpoint"
 5681      706F696E 
 5681      7400
 5682              	.LASF167:
 5683 1051 5F494F5F 		.string	"_IO_backup_base"
 5683      6261636B 
GAS LISTING /tmp/cca1HRfE.s 			page 118


 5683      75705F62 
 5683      61736500 
 5684              	.LASF310:
 5685 1061 7365746C 		.string	"setlocale"
 5685      6F63616C 
 5685      6500
 5686              	.LASF176:
 5687 106b 5F73686F 		.string	"_shortbuf"
 5687      72746275 
 5687      6600
 5688              	.LASF271:
 5689 1075 77637372 		.string	"wcsrchr"
 5689      63687200 
 5690              	.LASF212:
 5691 107d 66777363 		.string	"fwscanf"
 5691      616E6600 
 5692              	.LASF195:
 5693 1085 77696E74 		.string	"wint_t"
 5693      5F7400
 5694              	.LASF62:
 5695 108c 5F535F69 		.string	"_S_ios_openmode_max"
 5695      6F735F6F 
 5695      70656E6D 
 5695      6F64655F 
 5695      6D617800 
 5696              	.LASF329:
 5697 10a0 5F6E6578 		.string	"_next"
 5697      7400
 5698              	.LASF107:
 5699 10a6 696F735F 		.string	"ios_base"
 5699      62617365 
 5699      00
 5700              	.LASF315:
 5701 10af 5F5F6F66 		.string	"__off64_t"
 5701      6636345F 
 5701      7400
 5702              	.LASF116:
 5703 10b9 73747269 		.string	"stringstream"
 5703      6E677374 
 5703      7265616D 
 5703      00
 5704              	.LASF101:
 5705 10c6 62616462 		.string	"badbit"
 5705      697400
 5706              	.LASF340:
 5707 10cd 666F7065 		.string	"fopen"
 5707      6E00
 5708              	.LASF28:
 5709 10d3 616C6C6F 		.string	"allocator"
 5709      6361746F 
 5709      7200
 5710              	.LASF321:
 5711 10dd 77637472 		.string	"wctrans"
 5711      616E7300 
 5712              	.LASF287:
 5713 10e5 74686F75 		.string	"thousands_sep"
 5713      73616E64 
GAS LISTING /tmp/cca1HRfE.s 			page 119


 5713      735F7365 
 5713      7000
 5714              	.LASF383:
 5715 10f3 5F5A4E53 		.string	"_ZNSt8ios_base4InitC4Ev"
 5715      7438696F 
 5715      735F6261 
 5715      73653449 
 5715      6E697443 
 5716              	.LASF102:
 5717 110b 656F6662 		.string	"eofbit"
 5717      697400
 5718              	.LASF353:
 5719 1112 72657769 		.string	"rewind"
 5719      6E6400
 5720              	.LASF165:
 5721 1119 5F494F5F 		.string	"_IO_buf_end"
 5721      6275665F 
 5721      656E6400 
 5722              	.LASF247:
 5723 1125 7763736C 		.string	"wcslen"
 5723      656E00
 5724              	.LASF100:
 5725 112c 696F7374 		.string	"iostate"
 5725      61746500 
 5726              	.LASF51:
 5727 1134 5F535F69 		.string	"_S_ios_fmtflags_max"
 5727      6F735F66 
 5727      6D74666C 
 5727      6167735F 
 5727      6D617800 
 5728              	.LASF120:
 5729 1148 616C6C6F 		.string	"allocator<char>"
 5729      6361746F 
 5729      723C6368 
 5729      61723E00 
 5730              	.LASF21:
 5731 1158 746F5F69 		.string	"to_int_type"
 5731      6E745F74 
 5731      79706500 
 5732              	.LASF2:
 5733 1164 5F416C6C 		.string	"_Alloc"
 5733      6F6300
 5734              	.LASF19:
 5735 116b 746F5F63 		.string	"to_char_type"
 5735      6861725F 
 5735      74797065 
 5735      00
 5736              	.LASF118:
 5737 1178 5F5F6465 		.string	"__debug"
 5737      62756700 
 5738              	.LASF245:
 5739 1180 746D5F67 		.string	"tm_gmtoff"
 5739      6D746F66 
 5739      6600
 5740              	.LASF290:
 5741 118a 63757272 		.string	"currency_symbol"
 5741      656E6379 
GAS LISTING /tmp/cca1HRfE.s 			page 120


 5741      5F73796D 
 5741      626F6C00 
 5742              	.LASF282:
 5743 119a 73686F72 		.string	"short int"
 5743      7420696E 
 5743      7400
 5744              	.LASF10:
 5745 11a4 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6lengthEPKc"
 5745      74313163 
 5745      6861725F 
 5745      74726169 
 5745      74734963 
 5746              	.LASF235:
 5747 11c5 77637366 		.string	"wcsftime"
 5747      74696D65 
 5747      00
 5748              	.LASF326:
 5749 11ce 5F5F7374 		.string	"__state"
 5749      61746500 
 5750              	.LASF343:
 5751 11d6 66736565 		.string	"fseek"
 5751      6B00
 5752              	.LASF357:
 5753 11dc 746D706E 		.string	"tmpnam"
 5753      616D00
 5754              	.LASF175:
 5755 11e3 5F767461 		.string	"_vtable_offset"
 5755      626C655F 
 5755      6F666673 
 5755      657400
 5756              	.LASF293:
 5757 11f2 6D6F6E5F 		.string	"mon_grouping"
 5757      67726F75 
 5757      70696E67 
 5757      00
 5758              	.LASF74:
 5759 11ff 5F535F63 		.string	"_S_cur"
 5759      757200
 5760              	.LASF380:
 5761 1206 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6assignERcRKc"
 5761      74313163 
 5761      6861725F 
 5761      74726169 
 5761      74734963 
 5762              	.LASF230:
 5763 1229 77637363 		.string	"wcscat"
 5763      617400
 5764              	.LASF356:
 5765 1230 746D7066 		.string	"tmpfile"
 5765      696C6500 
 5766              	.LASF323:
 5767 1238 31315F5F 		.string	"11__mbstate_t"
 5767      6D627374 
 5767      6174655F 
 5767      7400
 5768              	.LASF308:
 5769 1246 696E745F 		.string	"int_p_sign_posn"
GAS LISTING /tmp/cca1HRfE.s 			page 121


 5769      705F7369 
 5769      676E5F70 
 5769      6F736E00 
 5770              	.LASF246:
 5771 1256 746D5F7A 		.string	"tm_zone"
 5771      6F6E6500 
 5772              	.LASF358:
 5773 125e 756E6765 		.string	"ungetc"
 5773      746300
 5774              	.LASF228:
 5775 1265 76777363 		.string	"vwscanf"
 5775      616E6600 
 5776              	.LASF69:
 5777 126d 5F535F69 		.string	"_S_ios_iostate_end"
 5777      6F735F69 
 5777      6F737461 
 5777      74655F65 
 5777      6E6400
 5778              	.LASF229:
 5779 1280 77637274 		.string	"wcrtomb"
 5779      6F6D6200 
 5780              	.LASF285:
 5781 1288 6C636F6E 		.string	"lconv"
 5781      7600
 5782              	.LASF95:
 5783 128e 756E6974 		.string	"unitbuf"
 5783      62756600 
 5784              	.LASF381:
 5785 1296 5F5A4E53 		.string	"_ZNSt11char_traitsIcE3eofEv"
 5785      74313163 
 5785      6861725F 
 5785      74726169 
 5785      74734963 
 5786              	.LASF248:
 5787 12b2 7763736E 		.string	"wcsncat"
 5787      63617400 
 5788              	.LASF154:
 5789 12ba 5F5F6E75 		.string	"__numeric_traits_integer<short int>"
 5789      6D657269 
 5789      635F7472 
 5789      61697473 
 5789      5F696E74 
 5790              	.LASF367:
 5791 12de 5F5F6473 		.string	"__dso_handle"
 5791      6F5F6861 
 5791      6E646C65 
 5791      00
 5792              	.LASF277:
 5793 12eb 6C6F6E67 		.string	"long long int"
 5793      206C6F6E 
 5793      6720696E 
 5793      7400
 5794              	.LASF208:
 5795 12f9 66707574 		.string	"fputwc"
 5795      776300
 5796              	.LASF171:
 5797 1300 5F66696C 		.string	"_fileno"
GAS LISTING /tmp/cca1HRfE.s 			page 122


 5797      656E6F00 
 5798              	.LASF209:
 5799 1308 66707574 		.string	"fputws"
 5799      777300
 5800              	.LASF80:
 5801 130f 7E496E69 		.string	"~Init"
 5801      7400
 5802              	.LASF217:
 5803 1315 6D627372 		.string	"mbsrtowcs"
 5803      746F7763 
 5803      7300
 5804              	.LASF68:
 5805 131f 5F535F66 		.string	"_S_failbit"
 5805      61696C62 
 5805      697400
 5806              	.LASF298:
 5807 132a 705F6373 		.string	"p_cs_precedes"
 5807      5F707265 
 5807      63656465 
 5807      7300
 5808              	.LASF152:
 5809 1338 5F5F6E75 		.string	"__numeric_traits_integer<long unsigned int>"
 5809      6D657269 
 5809      635F7472 
 5809      61697473 
 5809      5F696E74 
 5810              	.LASF241:
 5811 1364 746D5F79 		.string	"tm_year"
 5811      65617200 
 5812              	.LASF203:
 5813 136c 73686F72 		.string	"short unsigned int"
 5813      7420756E 
 5813      7369676E 
 5813      65642069 
 5813      6E7400
 5814              	.LASF124:
 5815 137f 636F6E73 		.string	"const_pointer"
 5815      745F706F 
 5815      696E7465 
 5815      7200
 5816              	.LASF1:
 5817 138d 5F547261 		.string	"_Traits"
 5817      69747300 
 5818              	.LASF119:
 5819 1395 5F5F6F70 		.string	"__ops"
 5819      7300
 5820              	.LASF224:
 5821 139b 76667773 		.string	"vfwscanf"
 5821      63616E66 
 5821      00
 5822              	.LASF162:
 5823 13a4 5F494F5F 		.string	"_IO_write_ptr"
 5823      77726974 
 5823      655F7074 
 5823      7200
 5824              	.LASF72:
 5825 13b2 5F496F73 		.string	"_Ios_Seekdir"
GAS LISTING /tmp/cca1HRfE.s 			page 123


 5825      5F536565 
 5825      6B646972 
 5825      00
 5826              	.LASF84:
 5827 13bf 666D7466 		.string	"fmtflags"
 5827      6C616773 
 5827      00
 5828              	.LASF313:
 5829 13c8 5F5F696E 		.string	"__int32_t"
 5829      7433325F 
 5829      7400
 5830              	.LASF213:
 5831 13d2 67657477 		.string	"getwc"
 5831      6300
 5832              	.LASF339:
 5833 13d8 66676574 		.string	"fgets"
 5833      7300
 5834              	.LASF216:
 5835 13de 6D627369 		.string	"mbsinit"
 5835      6E697400 
 5836              	.LASF319:
 5837 13e6 69737763 		.string	"iswctype"
 5837      74797065 
 5837      00
 5838              	.LASF17:
 5839 13ef 61737369 		.string	"assign"
 5839      676E00
 5840              	.LASF288:
 5841 13f6 67726F75 		.string	"grouping"
 5841      70696E67 
 5841      00
 5842              	.LASF267:
 5843 13ff 77707269 		.string	"wprintf"
 5843      6E746600 
 5844              	.LASF375:
 5845 1407 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIlE5__maxE"
 5845      5F5F676E 
 5845      755F6378 
 5845      7832345F 
 5845      5F6E756D 
 5846              	.LASF382:
 5847 1439 5F5A4E53 		.string	"_ZNSt11char_traitsIcE7not_eofERKi"
 5847      74313163 
 5847      6861725F 
 5847      74726169 
 5847      74734963 
 5848              	.LASF336:
 5849 145b 66666C75 		.string	"fflush"
 5849      736800
 5850              	.LASF40:
 5851 1462 5F535F73 		.string	"_S_scientific"
 5851      6369656E 
 5851      74696669 
 5851      6300
 5852              	.LASF207:
 5853 1470 77636861 		.string	"wchar_t"
 5853      725F7400 
GAS LISTING /tmp/cca1HRfE.s 			page 124


 5854              	.LASF187:
 5855 1478 74797065 		.string	"typedef __va_list_tag __va_list_tag"
 5855      64656620 
 5855      5F5F7661 
 5855      5F6C6973 
 5855      745F7461 
 5856              	.LASF140:
 5857 149c 5F5A4E4B 		.string	"_ZNK9__gnu_cxx13new_allocatorIcE8max_sizeEv"
 5857      395F5F67 
 5857      6E755F63 
 5857      78783133 
 5857      6E65775F 
 5858              	.LASF260:
 5859 14c8 77637374 		.string	"wcstoul"
 5859      6F756C00 
 5860              	.LASF115:
 5861 14d0 62617369 		.string	"basic_ofstream<char, std::char_traits<char> >"
 5861      635F6F66 
 5861      73747265 
 5861      616D3C63 
 5861      6861722C 
 5862              	.LASF342:
 5863 14fe 6672656F 		.string	"freopen"
 5863      70656E00 
 5864              	.LASF377:
 5865 1506 67656E65 		.string	"generic_stream.cpp"
 5865      7269635F 
 5865      73747265 
 5865      616D2E63 
 5865      707000
 5866              		.hidden	__dso_handle
 5867              		.ident	"GCC: (Ubuntu 5.4.1-2ubuntu1~16.04) 5.4.1 20160904"
 5868              		.section	.note.GNU-stack,"",@progbits
GAS LISTING /tmp/cca1HRfE.s 			page 125


DEFINED SYMBOLS
                            *ABS*:0000000000000000 generic_stream.cpp
     /tmp/cca1HRfE.s:7      .text._ZStorSt13_Ios_OpenmodeS_:0000000000000000 _ZStorSt13_Ios_OpenmodeS_
                             .bss:0000000000000000 _ZStL8__ioinit
     /tmp/cca1HRfE.s:37     .text:0000000000000000 _Z15write_somethingRSo
     /tmp/cca1HRfE.s:76     .text:000000000000003a main
     /tmp/cca1HRfE.s:253    .text:000000000000019c _Z41__static_initialization_and_destruction_0ii
     /tmp/cca1HRfE.s:289    .text:00000000000001da _GLOBAL__sub_I__Z15write_somethingRSo

UNDEFINED SYMBOLS
_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
_ZNSolsEi
_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
_ZNSolsEPFRSoS_E
__gxx_personality_v0
_ZSt4cout
_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1EPKcSt13_Ios_Openmode
_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEC1ESt13_Ios_Openmode
_ZNKSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEE3strEv
_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev
_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev
_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
_Unwind_Resume
__stack_chk_fail
_ZNSt8ios_base4InitC1Ev
__dso_handle
_ZNSt8ios_base4InitD1Ev
__cxa_atexit
