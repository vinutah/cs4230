GAS LISTING /tmp/cce2OND8.s 			page 1


   1              		.file	"io.cpp"
   2              		.text
   3              	.Ltext0:
   4              		.section	.text._ZStorSt13_Ios_OpenmodeS_,"axG",@progbits,_ZStorSt13_Ios_OpenmodeS_,comdat
   5              		.weak	_ZStorSt13_Ios_OpenmodeS_
   6              		.type	_ZStorSt13_Ios_OpenmodeS_, @function
   7              	_ZStorSt13_Ios_OpenmodeS_:
   8              	.LFB644:
   9              		.file 1 "/usr/include/c++/5/bits/ios_base.h"
   1:/usr/include/c++/5/bits/ios_base.h **** // Iostreams base classes -*- C++ -*-
   2:/usr/include/c++/5/bits/ios_base.h **** 
   3:/usr/include/c++/5/bits/ios_base.h **** // Copyright (C) 1997-2015 Free Software Foundation, Inc.
   4:/usr/include/c++/5/bits/ios_base.h **** //
   5:/usr/include/c++/5/bits/ios_base.h **** // This file is part of the GNU ISO C++ Library.  This library is free
   6:/usr/include/c++/5/bits/ios_base.h **** // software; you can redistribute it and/or modify it under the
   7:/usr/include/c++/5/bits/ios_base.h **** // terms of the GNU General Public License as published by the
   8:/usr/include/c++/5/bits/ios_base.h **** // Free Software Foundation; either version 3, or (at your option)
   9:/usr/include/c++/5/bits/ios_base.h **** // any later version.
  10:/usr/include/c++/5/bits/ios_base.h **** 
  11:/usr/include/c++/5/bits/ios_base.h **** // This library is distributed in the hope that it will be useful,
  12:/usr/include/c++/5/bits/ios_base.h **** // but WITHOUT ANY WARRANTY; without even the implied warranty of
  13:/usr/include/c++/5/bits/ios_base.h **** // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  14:/usr/include/c++/5/bits/ios_base.h **** // GNU General Public License for more details.
  15:/usr/include/c++/5/bits/ios_base.h **** 
  16:/usr/include/c++/5/bits/ios_base.h **** // Under Section 7 of GPL version 3, you are granted additional
  17:/usr/include/c++/5/bits/ios_base.h **** // permissions described in the GCC Runtime Library Exception, version
  18:/usr/include/c++/5/bits/ios_base.h **** // 3.1, as published by the Free Software Foundation.
  19:/usr/include/c++/5/bits/ios_base.h **** 
  20:/usr/include/c++/5/bits/ios_base.h **** // You should have received a copy of the GNU General Public License and
  21:/usr/include/c++/5/bits/ios_base.h **** // a copy of the GCC Runtime Library Exception along with this program;
  22:/usr/include/c++/5/bits/ios_base.h **** // see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
  23:/usr/include/c++/5/bits/ios_base.h **** // <http://www.gnu.org/licenses/>.
  24:/usr/include/c++/5/bits/ios_base.h **** 
  25:/usr/include/c++/5/bits/ios_base.h **** /** @file bits/ios_base.h
  26:/usr/include/c++/5/bits/ios_base.h ****  *  This is an internal header file, included by other library headers.
  27:/usr/include/c++/5/bits/ios_base.h ****  *  Do not attempt to use it directly. @headername{ios}
  28:/usr/include/c++/5/bits/ios_base.h ****  */
  29:/usr/include/c++/5/bits/ios_base.h **** 
  30:/usr/include/c++/5/bits/ios_base.h **** //
  31:/usr/include/c++/5/bits/ios_base.h **** // ISO C++ 14882: 27.4  Iostreams base classes
  32:/usr/include/c++/5/bits/ios_base.h **** //
  33:/usr/include/c++/5/bits/ios_base.h **** 
  34:/usr/include/c++/5/bits/ios_base.h **** #ifndef _IOS_BASE_H
  35:/usr/include/c++/5/bits/ios_base.h **** #define _IOS_BASE_H 1
  36:/usr/include/c++/5/bits/ios_base.h **** 
  37:/usr/include/c++/5/bits/ios_base.h **** #pragma GCC system_header
  38:/usr/include/c++/5/bits/ios_base.h **** 
  39:/usr/include/c++/5/bits/ios_base.h **** #include <ext/atomicity.h>
  40:/usr/include/c++/5/bits/ios_base.h **** #include <bits/localefwd.h>
  41:/usr/include/c++/5/bits/ios_base.h **** #include <bits/locale_classes.h>
  42:/usr/include/c++/5/bits/ios_base.h **** 
  43:/usr/include/c++/5/bits/ios_base.h **** #if __cplusplus < 201103L
  44:/usr/include/c++/5/bits/ios_base.h **** # include <stdexcept>
  45:/usr/include/c++/5/bits/ios_base.h **** #else
  46:/usr/include/c++/5/bits/ios_base.h **** # include <system_error>
  47:/usr/include/c++/5/bits/ios_base.h **** #endif
  48:/usr/include/c++/5/bits/ios_base.h **** 
GAS LISTING /tmp/cce2OND8.s 			page 2


  49:/usr/include/c++/5/bits/ios_base.h **** namespace std _GLIBCXX_VISIBILITY(default)
  50:/usr/include/c++/5/bits/ios_base.h **** {
  51:/usr/include/c++/5/bits/ios_base.h **** _GLIBCXX_BEGIN_NAMESPACE_VERSION
  52:/usr/include/c++/5/bits/ios_base.h **** 
  53:/usr/include/c++/5/bits/ios_base.h ****   // The following definitions of bitmask types are enums, not ints,
  54:/usr/include/c++/5/bits/ios_base.h ****   // as permitted (but not required) in the standard, in order to provide
  55:/usr/include/c++/5/bits/ios_base.h ****   // better type safety in iostream calls.  A side effect is that
  56:/usr/include/c++/5/bits/ios_base.h ****   // expressions involving them are no longer compile-time constants.
  57:/usr/include/c++/5/bits/ios_base.h ****   enum _Ios_Fmtflags 
  58:/usr/include/c++/5/bits/ios_base.h ****     { 
  59:/usr/include/c++/5/bits/ios_base.h ****       _S_boolalpha 	= 1L << 0,
  60:/usr/include/c++/5/bits/ios_base.h ****       _S_dec 		= 1L << 1,
  61:/usr/include/c++/5/bits/ios_base.h ****       _S_fixed 		= 1L << 2,
  62:/usr/include/c++/5/bits/ios_base.h ****       _S_hex 		= 1L << 3,
  63:/usr/include/c++/5/bits/ios_base.h ****       _S_internal 	= 1L << 4,
  64:/usr/include/c++/5/bits/ios_base.h ****       _S_left 		= 1L << 5,
  65:/usr/include/c++/5/bits/ios_base.h ****       _S_oct 		= 1L << 6,
  66:/usr/include/c++/5/bits/ios_base.h ****       _S_right 		= 1L << 7,
  67:/usr/include/c++/5/bits/ios_base.h ****       _S_scientific 	= 1L << 8,
  68:/usr/include/c++/5/bits/ios_base.h ****       _S_showbase 	= 1L << 9,
  69:/usr/include/c++/5/bits/ios_base.h ****       _S_showpoint 	= 1L << 10,
  70:/usr/include/c++/5/bits/ios_base.h ****       _S_showpos 	= 1L << 11,
  71:/usr/include/c++/5/bits/ios_base.h ****       _S_skipws 	= 1L << 12,
  72:/usr/include/c++/5/bits/ios_base.h ****       _S_unitbuf 	= 1L << 13,
  73:/usr/include/c++/5/bits/ios_base.h ****       _S_uppercase 	= 1L << 14,
  74:/usr/include/c++/5/bits/ios_base.h ****       _S_adjustfield 	= _S_left | _S_right | _S_internal,
  75:/usr/include/c++/5/bits/ios_base.h ****       _S_basefield 	= _S_dec | _S_oct | _S_hex,
  76:/usr/include/c++/5/bits/ios_base.h ****       _S_floatfield 	= _S_scientific | _S_fixed,
  77:/usr/include/c++/5/bits/ios_base.h ****       _S_ios_fmtflags_end = 1L << 16,
  78:/usr/include/c++/5/bits/ios_base.h ****       _S_ios_fmtflags_max = __INT_MAX__,
  79:/usr/include/c++/5/bits/ios_base.h ****       _S_ios_fmtflags_min = ~__INT_MAX__
  80:/usr/include/c++/5/bits/ios_base.h ****     };
  81:/usr/include/c++/5/bits/ios_base.h **** 
  82:/usr/include/c++/5/bits/ios_base.h ****   inline _GLIBCXX_CONSTEXPR _Ios_Fmtflags
  83:/usr/include/c++/5/bits/ios_base.h ****   operator&(_Ios_Fmtflags __a, _Ios_Fmtflags __b)
  84:/usr/include/c++/5/bits/ios_base.h ****   { return _Ios_Fmtflags(static_cast<int>(__a) & static_cast<int>(__b)); }
  85:/usr/include/c++/5/bits/ios_base.h **** 
  86:/usr/include/c++/5/bits/ios_base.h ****   inline _GLIBCXX_CONSTEXPR _Ios_Fmtflags
  87:/usr/include/c++/5/bits/ios_base.h ****   operator|(_Ios_Fmtflags __a, _Ios_Fmtflags __b)
  88:/usr/include/c++/5/bits/ios_base.h ****   { return _Ios_Fmtflags(static_cast<int>(__a) | static_cast<int>(__b)); }
  89:/usr/include/c++/5/bits/ios_base.h **** 
  90:/usr/include/c++/5/bits/ios_base.h ****   inline _GLIBCXX_CONSTEXPR _Ios_Fmtflags
  91:/usr/include/c++/5/bits/ios_base.h ****   operator^(_Ios_Fmtflags __a, _Ios_Fmtflags __b)
  92:/usr/include/c++/5/bits/ios_base.h ****   { return _Ios_Fmtflags(static_cast<int>(__a) ^ static_cast<int>(__b)); }
  93:/usr/include/c++/5/bits/ios_base.h **** 
  94:/usr/include/c++/5/bits/ios_base.h ****   inline _GLIBCXX_CONSTEXPR _Ios_Fmtflags
  95:/usr/include/c++/5/bits/ios_base.h ****   operator~(_Ios_Fmtflags __a)
  96:/usr/include/c++/5/bits/ios_base.h ****   { return _Ios_Fmtflags(~static_cast<int>(__a)); }
  97:/usr/include/c++/5/bits/ios_base.h **** 
  98:/usr/include/c++/5/bits/ios_base.h ****   inline const _Ios_Fmtflags&
  99:/usr/include/c++/5/bits/ios_base.h ****   operator|=(_Ios_Fmtflags& __a, _Ios_Fmtflags __b)
 100:/usr/include/c++/5/bits/ios_base.h ****   { return __a = __a | __b; }
 101:/usr/include/c++/5/bits/ios_base.h **** 
 102:/usr/include/c++/5/bits/ios_base.h ****   inline const _Ios_Fmtflags&
 103:/usr/include/c++/5/bits/ios_base.h ****   operator&=(_Ios_Fmtflags& __a, _Ios_Fmtflags __b)
 104:/usr/include/c++/5/bits/ios_base.h ****   { return __a = __a & __b; }
 105:/usr/include/c++/5/bits/ios_base.h **** 
GAS LISTING /tmp/cce2OND8.s 			page 3


 106:/usr/include/c++/5/bits/ios_base.h ****   inline const _Ios_Fmtflags&
 107:/usr/include/c++/5/bits/ios_base.h ****   operator^=(_Ios_Fmtflags& __a, _Ios_Fmtflags __b)
 108:/usr/include/c++/5/bits/ios_base.h ****   { return __a = __a ^ __b; }
 109:/usr/include/c++/5/bits/ios_base.h **** 
 110:/usr/include/c++/5/bits/ios_base.h **** 
 111:/usr/include/c++/5/bits/ios_base.h ****   enum _Ios_Openmode 
 112:/usr/include/c++/5/bits/ios_base.h ****     { 
 113:/usr/include/c++/5/bits/ios_base.h ****       _S_app 		= 1L << 0,
 114:/usr/include/c++/5/bits/ios_base.h ****       _S_ate 		= 1L << 1,
 115:/usr/include/c++/5/bits/ios_base.h ****       _S_bin 		= 1L << 2,
 116:/usr/include/c++/5/bits/ios_base.h ****       _S_in 		= 1L << 3,
 117:/usr/include/c++/5/bits/ios_base.h ****       _S_out 		= 1L << 4,
 118:/usr/include/c++/5/bits/ios_base.h ****       _S_trunc 		= 1L << 5,
 119:/usr/include/c++/5/bits/ios_base.h ****       _S_ios_openmode_end = 1L << 16,
 120:/usr/include/c++/5/bits/ios_base.h ****       _S_ios_openmode_max = __INT_MAX__,
 121:/usr/include/c++/5/bits/ios_base.h ****       _S_ios_openmode_min = ~__INT_MAX__
 122:/usr/include/c++/5/bits/ios_base.h ****     };
 123:/usr/include/c++/5/bits/ios_base.h **** 
 124:/usr/include/c++/5/bits/ios_base.h ****   inline _GLIBCXX_CONSTEXPR _Ios_Openmode
 125:/usr/include/c++/5/bits/ios_base.h ****   operator&(_Ios_Openmode __a, _Ios_Openmode __b)
 126:/usr/include/c++/5/bits/ios_base.h ****   { return _Ios_Openmode(static_cast<int>(__a) & static_cast<int>(__b)); }
 127:/usr/include/c++/5/bits/ios_base.h **** 
 128:/usr/include/c++/5/bits/ios_base.h ****   inline _GLIBCXX_CONSTEXPR _Ios_Openmode
 129:/usr/include/c++/5/bits/ios_base.h ****   operator|(_Ios_Openmode __a, _Ios_Openmode __b)
 130:/usr/include/c++/5/bits/ios_base.h ****   { return _Ios_Openmode(static_cast<int>(__a) | static_cast<int>(__b)); }
  10              		.loc 1 130 0
  11              		.cfi_startproc
  12 0000 55       		pushq	%rbp
  13              		.cfi_def_cfa_offset 16
  14              		.cfi_offset 6, -16
  15 0001 4889E5   		movq	%rsp, %rbp
  16              		.cfi_def_cfa_register 6
  17 0004 897DFC   		movl	%edi, -4(%rbp)
  18 0007 8975F8   		movl	%esi, -8(%rbp)
  19              		.loc 1 130 0
  20 000a 8B45FC   		movl	-4(%rbp), %eax
  21 000d 0B45F8   		orl	-8(%rbp), %eax
  22 0010 5D       		popq	%rbp
  23              		.cfi_def_cfa 7, 8
  24 0011 C3       		ret
  25              		.cfi_endproc
  26              	.LFE644:
  27              		.size	_ZStorSt13_Ios_OpenmodeS_, .-_ZStorSt13_Ios_OpenmodeS_
  28              		.section	.rodata
  29              	.LC0:
  30 0000 73717561 		.string	"squares1.txt"
  30      72657331 
  30      2E747874 
  30      00
  31              	.LC1:
  32 000d 5E32203D 		.string	"^2 = "
  32      2000
  33              	.LC2:
  34 0013 73717561 		.string	"squares2.txt"
  34      72657332 
  34      2E747874 
  34      00
GAS LISTING /tmp/cce2OND8.s 			page 4


  35              		.text
  36              		.globl	main
  37              		.type	main, @function
  38              	main:
  39              	.LFB1085:
  40              		.file 2 "io.cpp"
   1:io.cpp        **** /*
   2:io.cpp        ****  * c++ uses a convienient abstraction called stream to perform I/O
   3:io.cpp        ****  * operations in a sequential media such as screen or keyboard
   4:io.cpp        ****  *
   5:io.cpp        ****  * stream is an object where a program can insert characters or extract them
   6:io.cpp        ****  *
   7:io.cpp        ****  * standard input and output stream objects are declared in <iostream>
   8:io.cpp        ****  *
   9:io.cpp        ****  * 1 Standard Output
  10:io.cpp        ****  *    ostream
  11:io.cpp        ****  *    Insertion operator <<
  12:io.cpp        ****  *    difference beteween \n and std::endl
  13:io.cpp        ****  *
  14:io.cpp        ****  * 2 Standard Input
  15:io.cpp        ****  *    istream
  16:io.cpp        ****  *    Extraction operator >>
  17:io.cpp        ****  *    cin stream, interpretes them as int
  18:io.cpp        ****  *
  19:io.cpp        ****  *    istream
  20:io.cpp        ****  *    ostream
  21:io.cpp        ****  *    iostream
  22:io.cpp        ****  *
  23:io.cpp        ****  * 3 Input Output with files
  24:io.cpp        ****  *    c++ gives classes for io with files
  25:io.cpp        ****  *
  26:io.cpp        ****  *    ofstream
  27:io.cpp        ****  *    ifstream
  28:io.cpp        ****  *    fstream
  29:io.cpp        ****  *
  30:io.cpp        ****  * 4
  31:io.cpp        ****  * 5
  32:io.cpp        ****  * 6
  33:io.cpp        ****  *
  34:io.cpp        ****  *
  35:io.cpp        ****  *
  36:io.cpp        ****  */
  37:io.cpp        **** 
  38:io.cpp        **** #include <fstream>
  39:io.cpp        **** 
  40:io.cpp        **** int main()
  41:io.cpp        **** {
  41              		.loc 2 41 0
  42              		.cfi_startproc
  43              		.cfi_personality 0x3,__gxx_personality_v0
  44              		.cfi_lsda 0x3,.LLSDA1085
  45 0000 55       		pushq	%rbp
  46              		.cfi_def_cfa_offset 16
  47              		.cfi_offset 6, -16
  48 0001 4889E5   		movq	%rsp, %rbp
  49              		.cfi_def_cfa_register 6
  50 0004 53       		pushq	%rbx
GAS LISTING /tmp/cce2OND8.s 			page 5


  51 0005 4881EC28 		subq	$1064, %rsp
  51      040000
  52              		.cfi_offset 3, -24
  53              		.loc 2 41 0
  54 000c 64488B04 		movq	%fs:40, %rax
  54      25280000 
  54      00
  55 0015 488945E8 		movq	%rax, -24(%rbp)
  56 0019 31C0     		xorl	%eax, %eax
  42:io.cpp        ****   std::ofstream square_file;
  57              		.loc 2 42 0
  58 001b 488D85E0 		leaq	-1056(%rbp), %rax
  58      FBFFFF
  59 0022 4889C7   		movq	%rax, %rdi
  60              	.LEHB0:
  61 0025 E8000000 		call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev
  61      00
  62              	.LEHE0:
  43:io.cpp        **** 
  44:io.cpp        ****   square_file.open("squares1.txt");
  63              		.loc 2 44 0
  64 002a BE200000 		movl	$32, %esi
  64      00
  65 002f BF100000 		movl	$16, %edi
  65      00
  66 0034 E8000000 		call	_ZStorSt13_Ios_OpenmodeS_
  66      00
  67 0039 89C2     		movl	%eax, %edx
  68 003b 488D85E0 		leaq	-1056(%rbp), %rax
  68      FBFFFF
  69 0042 BE000000 		movl	$.LC0, %esi
  69      00
  70 0047 4889C7   		movq	%rax, %rdi
  71              	.LEHB1:
  72 004a E8000000 		call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode
  72      00
  73              	.LBB2:
  45:io.cpp        **** 
  46:io.cpp        ****   for (int i = 0; i < 10; ++i)
  74              		.loc 2 46 0
  75 004f C785D8FB 		movl	$0, -1064(%rbp)
  75      FFFF0000 
  75      0000
  76              	.L5:
  77              		.loc 2 46 0 is_stmt 0 discriminator 1
  78 0059 83BDD8FB 		cmpl	$9, -1064(%rbp)
  78      FFFF09
  79 0060 7F53     		jg	.L4
  47:io.cpp        ****       square_file << i << "^2 = " << i*i << std::endl ;
  80              		.loc 2 47 0 is_stmt 1
  81 0062 8B85D8FB 		movl	-1064(%rbp), %eax
  81      FFFF
  82 0068 0FAF85D8 		imull	-1064(%rbp), %eax
  82      FBFFFF
  83 006f 89C3     		movl	%eax, %ebx
  84 0071 8B95D8FB 		movl	-1064(%rbp), %edx
  84      FFFF
GAS LISTING /tmp/cce2OND8.s 			page 6


  85 0077 488D85E0 		leaq	-1056(%rbp), %rax
  85      FBFFFF
  86 007e 89D6     		movl	%edx, %esi
  87 0080 4889C7   		movq	%rax, %rdi
  88 0083 E8000000 		call	_ZNSolsEi
  88      00
  89 0088 BE000000 		movl	$.LC1, %esi
  89      00
  90 008d 4889C7   		movq	%rax, %rdi
  91 0090 E8000000 		call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
  91      00
  92              		.loc 2 47 0 is_stmt 0 discriminator 1
  93 0095 89DE     		movl	%ebx, %esi
  94 0097 4889C7   		movq	%rax, %rdi
  95 009a E8000000 		call	_ZNSolsEi
  95      00
  96              		.loc 2 47 0 discriminator 2
  97 009f BE000000 		movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
  97      00
  98 00a4 4889C7   		movq	%rax, %rdi
  99 00a7 E8000000 		call	_ZNSolsEPFRSoS_E
  99      00
  46:io.cpp        ****       square_file << i << "^2 = " << i*i << std::endl ;
 100              		.loc 2 46 0 is_stmt 1 discriminator 2
 101 00ac 8385D8FB 		addl	$1, -1064(%rbp)
 101      FFFF01
 102 00b3 EBA4     		jmp	.L5
 103              	.L4:
 104              	.LBE2:
  48:io.cpp        **** 
  49:io.cpp        ****   square_file.close();
 105              		.loc 2 49 0
 106 00b5 488D85E0 		leaq	-1056(%rbp), %rax
 106      FBFFFF
 107 00bc 4889C7   		movq	%rax, %rdi
 108 00bf E8000000 		call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEE5closeEv
 108      00
  50:io.cpp        **** 
  51:io.cpp        ****   std::ofstream square_file2("squares2.txt");
 109              		.loc 2 51 0
 110 00c4 BE200000 		movl	$32, %esi
 110      00
 111 00c9 BF100000 		movl	$16, %edi
 111      00
 112 00ce E8000000 		call	_ZStorSt13_Ios_OpenmodeS_
 112      00
 113 00d3 89C2     		movl	%eax, %edx
 114 00d5 488D85E0 		leaq	-544(%rbp), %rax
 114      FDFFFF
 115 00dc BE000000 		movl	$.LC2, %esi
 115      00
 116 00e1 4889C7   		movq	%rax, %rdi
 117 00e4 E8000000 		call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1EPKcSt13_Ios_Openmode
 117      00
 118              	.LEHE1:
 119              	.LBB3:
  52:io.cpp        **** 
GAS LISTING /tmp/cce2OND8.s 			page 7


  53:io.cpp        ****   for (int i= 0; i < 10; ++i)
 120              		.loc 2 53 0
 121 00e9 C785DCFB 		movl	$0, -1060(%rbp)
 121      FFFF0000 
 121      0000
 122              	.L7:
 123              		.loc 2 53 0 is_stmt 0 discriminator 1
 124 00f3 83BDDCFB 		cmpl	$9, -1060(%rbp)
 124      FFFF09
 125 00fa 7F53     		jg	.L6
  54:io.cpp        ****     square_file2 << i << "^2 = " << i*i << std::endl;
 126              		.loc 2 54 0 is_stmt 1
 127 00fc 8B85DCFB 		movl	-1060(%rbp), %eax
 127      FFFF
 128 0102 0FAF85DC 		imull	-1060(%rbp), %eax
 128      FBFFFF
 129 0109 89C3     		movl	%eax, %ebx
 130 010b 8B95DCFB 		movl	-1060(%rbp), %edx
 130      FFFF
 131 0111 488D85E0 		leaq	-544(%rbp), %rax
 131      FDFFFF
 132 0118 89D6     		movl	%edx, %esi
 133 011a 4889C7   		movq	%rax, %rdi
 134              	.LEHB2:
 135 011d E8000000 		call	_ZNSolsEi
 135      00
 136 0122 BE000000 		movl	$.LC1, %esi
 136      00
 137 0127 4889C7   		movq	%rax, %rdi
 138 012a E8000000 		call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
 138      00
 139              		.loc 2 54 0 is_stmt 0 discriminator 1
 140 012f 89DE     		movl	%ebx, %esi
 141 0131 4889C7   		movq	%rax, %rdi
 142 0134 E8000000 		call	_ZNSolsEi
 142      00
 143              		.loc 2 54 0 discriminator 2
 144 0139 BE000000 		movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
 144      00
 145 013e 4889C7   		movq	%rax, %rdi
 146 0141 E8000000 		call	_ZNSolsEPFRSoS_E
 146      00
 147              	.LEHE2:
  53:io.cpp        ****     square_file2 << i << "^2 = " << i*i << std::endl;
 148              		.loc 2 53 0 is_stmt 1 discriminator 2
 149 0146 8385DCFB 		addl	$1, -1060(%rbp)
 149      FFFF01
 150 014d EBA4     		jmp	.L7
 151              	.L6:
 152              	.LBE3:
  55:io.cpp        **** 
  56:io.cpp        ****   return 0;
 153              		.loc 2 56 0
 154 014f BB000000 		movl	$0, %ebx
 154      00
  51:io.cpp        **** 
 155              		.loc 2 51 0
GAS LISTING /tmp/cce2OND8.s 			page 8


 156 0154 488D85E0 		leaq	-544(%rbp), %rax
 156      FDFFFF
 157 015b 4889C7   		movq	%rax, %rdi
 158              	.LEHB3:
 159 015e E8000000 		call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
 159      00
 160              	.LEHE3:
  42:io.cpp        **** 
 161              		.loc 2 42 0
 162 0163 488D85E0 		leaq	-1056(%rbp), %rax
 162      FBFFFF
 163 016a 4889C7   		movq	%rax, %rdi
 164              	.LEHB4:
 165 016d E8000000 		call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
 165      00
 166              	.LEHE4:
 167 0172 89D8     		movl	%ebx, %eax
  57:io.cpp        **** }
 168              		.loc 2 57 0
 169 0174 488B4DE8 		movq	-24(%rbp), %rcx
 170 0178 6448330C 		xorq	%fs:40, %rcx
 170      25280000 
 170      00
 171 0181 7438     		je	.L11
 172 0183 EB31     		jmp	.L14
 173              	.L13:
 174 0185 4889C3   		movq	%rax, %rbx
  51:io.cpp        **** 
 175              		.loc 2 51 0
 176 0188 488D85E0 		leaq	-544(%rbp), %rax
 176      FDFFFF
 177 018f 4889C7   		movq	%rax, %rdi
 178 0192 E8000000 		call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
 178      00
 179 0197 EB03     		jmp	.L10
 180              	.L12:
 181 0199 4889C3   		movq	%rax, %rbx
 182              	.L10:
  42:io.cpp        **** 
 183              		.loc 2 42 0
 184 019c 488D85E0 		leaq	-1056(%rbp), %rax
 184      FBFFFF
 185 01a3 4889C7   		movq	%rax, %rdi
 186 01a6 E8000000 		call	_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
 186      00
 187 01ab 4889D8   		movq	%rbx, %rax
 188 01ae 4889C7   		movq	%rax, %rdi
 189              	.LEHB5:
 190 01b1 E8000000 		call	_Unwind_Resume
 190      00
 191              	.LEHE5:
 192              	.L14:
 193              		.loc 2 57 0
 194 01b6 E8000000 		call	__stack_chk_fail
 194      00
 195              	.L11:
 196 01bb 4881C428 		addq	$1064, %rsp
GAS LISTING /tmp/cce2OND8.s 			page 9


 196      040000
 197 01c2 5B       		popq	%rbx
 198 01c3 5D       		popq	%rbp
 199              		.cfi_def_cfa 7, 8
 200 01c4 C3       		ret
 201              		.cfi_endproc
 202              	.LFE1085:
 203              		.globl	__gxx_personality_v0
 204              		.section	.gcc_except_table,"a",@progbits
 205              	.LLSDA1085:
 206 0000 FF       		.byte	0xff
 207 0001 FF       		.byte	0xff
 208 0002 01       		.byte	0x1
 209 0003 20       		.uleb128 .LLSDACSE1085-.LLSDACSB1085
 210              	.LLSDACSB1085:
 211 0004 25       		.uleb128 .LEHB0-.LFB1085
 212 0005 05       		.uleb128 .LEHE0-.LEHB0
 213 0006 00       		.uleb128 0
 214 0007 00       		.uleb128 0
 215 0008 4A       		.uleb128 .LEHB1-.LFB1085
 216 0009 9F01     		.uleb128 .LEHE1-.LEHB1
 217 000b 9903     		.uleb128 .L12-.LFB1085
 218 000d 00       		.uleb128 0
 219 000e 9D02     		.uleb128 .LEHB2-.LFB1085
 220 0010 29       		.uleb128 .LEHE2-.LEHB2
 221 0011 8503     		.uleb128 .L13-.LFB1085
 222 0013 00       		.uleb128 0
 223 0014 DE02     		.uleb128 .LEHB3-.LFB1085
 224 0016 05       		.uleb128 .LEHE3-.LEHB3
 225 0017 9903     		.uleb128 .L12-.LFB1085
 226 0019 00       		.uleb128 0
 227 001a ED02     		.uleb128 .LEHB4-.LFB1085
 228 001c 05       		.uleb128 .LEHE4-.LEHB4
 229 001d 00       		.uleb128 0
 230 001e 00       		.uleb128 0
 231 001f B103     		.uleb128 .LEHB5-.LFB1085
 232 0021 05       		.uleb128 .LEHE5-.LEHB5
 233 0022 00       		.uleb128 0
 234 0023 00       		.uleb128 0
 235              	.LLSDACSE1085:
 236              		.text
 237              		.size	main, .-main
 238              	.Letext0:
 239              		.file 3 "/usr/include/c++/5/cwchar"
 240              		.file 4 "/usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h"
 241              		.file 5 "/usr/include/c++/5/debug/debug.h"
 242              		.file 6 "/usr/include/c++/5/bits/char_traits.h"
 243              		.file 7 "/usr/include/c++/5/clocale"
 244              		.file 8 "/usr/include/c++/5/cwctype"
 245              		.file 9 "/usr/include/c++/5/cstdio"
 246              		.file 10 "/usr/include/c++/5/iosfwd"
 247              		.file 11 "/usr/include/c++/5/bits/predefined_ops.h"
 248              		.file 12 "/usr/include/c++/5/ext/new_allocator.h"
 249              		.file 13 "/usr/include/c++/5/ext/numeric_traits.h"
 250              		.file 14 "/usr/include/stdio.h"
 251              		.file 15 "/usr/include/libio.h"
 252              		.file 16 "<built-in>"
GAS LISTING /tmp/cce2OND8.s 			page 10


 253              		.file 17 "/usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h"
 254              		.file 18 "/usr/include/wchar.h"
 255              		.file 19 "/usr/include/time.h"
 256              		.file 20 "/usr/include/locale.h"
 257              		.file 21 "/usr/include/x86_64-linux-gnu/bits/types.h"
 258              		.file 22 "/usr/include/wctype.h"
 259              		.file 23 "/usr/include/_G_config.h"
 260              		.section	.debug_info,"",@progbits
 261              	.Ldebug_info0:
 262 0000 E6170000 		.long	0x17e6
 263 0004 0400     		.value	0x4
 264 0006 00000000 		.long	.Ldebug_abbrev0
 265 000a 08       		.byte	0x8
 266 000b 01       		.uleb128 0x1
 267 000c 00000000 		.long	.LASF265
 268 0010 04       		.byte	0x4
 269 0011 00000000 		.long	.LASF266
 270 0015 00000000 		.long	.LASF267
 271 0019 00000000 		.long	.Ldebug_ranges0+0
 272 001d 00000000 		.quad	0
 272      00000000 
 273 0025 00000000 		.long	.Ldebug_line0
 274 0029 02       		.uleb128 0x2
 275 002a 73746400 		.string	"std"
 276 002e 10       		.byte	0x10
 277 002f 00       		.byte	0
 278 0030 72050000 		.long	0x572
 279 0034 03       		.uleb128 0x3
 280 0035 00000000 		.long	.LASF0
 281 0039 04       		.byte	0x4
 282 003a DA       		.byte	0xda
 283 003b 04       		.uleb128 0x4
 284 003c 04       		.byte	0x4
 285 003d DA       		.byte	0xda
 286 003e 34000000 		.long	0x34
 287 0042 05       		.uleb128 0x5
 288 0043 03       		.byte	0x3
 289 0044 40       		.byte	0x40
 290 0045 69090000 		.long	0x969
 291 0049 05       		.uleb128 0x5
 292 004a 03       		.byte	0x3
 293 004b 8B       		.byte	0x8b
 294 004c F0080000 		.long	0x8f0
 295 0050 05       		.uleb128 0x5
 296 0051 03       		.byte	0x3
 297 0052 8D       		.byte	0x8d
 298 0053 8B090000 		.long	0x98b
 299 0057 05       		.uleb128 0x5
 300 0058 03       		.byte	0x3
 301 0059 8E       		.byte	0x8e
 302 005a A1090000 		.long	0x9a1
 303 005e 05       		.uleb128 0x5
 304 005f 03       		.byte	0x3
 305 0060 8F       		.byte	0x8f
 306 0061 BD090000 		.long	0x9bd
 307 0065 05       		.uleb128 0x5
 308 0066 03       		.byte	0x3
GAS LISTING /tmp/cce2OND8.s 			page 11


 309 0067 90       		.byte	0x90
 310 0068 EA090000 		.long	0x9ea
 311 006c 05       		.uleb128 0x5
 312 006d 03       		.byte	0x3
 313 006e 91       		.byte	0x91
 314 006f 050A0000 		.long	0xa05
 315 0073 05       		.uleb128 0x5
 316 0074 03       		.byte	0x3
 317 0075 92       		.byte	0x92
 318 0076 2B0A0000 		.long	0xa2b
 319 007a 05       		.uleb128 0x5
 320 007b 03       		.byte	0x3
 321 007c 93       		.byte	0x93
 322 007d 460A0000 		.long	0xa46
 323 0081 05       		.uleb128 0x5
 324 0082 03       		.byte	0x3
 325 0083 94       		.byte	0x94
 326 0084 620A0000 		.long	0xa62
 327 0088 05       		.uleb128 0x5
 328 0089 03       		.byte	0x3
 329 008a 95       		.byte	0x95
 330 008b 7E0A0000 		.long	0xa7e
 331 008f 05       		.uleb128 0x5
 332 0090 03       		.byte	0x3
 333 0091 96       		.byte	0x96
 334 0092 940A0000 		.long	0xa94
 335 0096 05       		.uleb128 0x5
 336 0097 03       		.byte	0x3
 337 0098 97       		.byte	0x97
 338 0099 A00A0000 		.long	0xaa0
 339 009d 05       		.uleb128 0x5
 340 009e 03       		.byte	0x3
 341 009f 98       		.byte	0x98
 342 00a0 C60A0000 		.long	0xac6
 343 00a4 05       		.uleb128 0x5
 344 00a5 03       		.byte	0x3
 345 00a6 99       		.byte	0x99
 346 00a7 EB0A0000 		.long	0xaeb
 347 00ab 05       		.uleb128 0x5
 348 00ac 03       		.byte	0x3
 349 00ad 9A       		.byte	0x9a
 350 00ae 0C0B0000 		.long	0xb0c
 351 00b2 05       		.uleb128 0x5
 352 00b3 03       		.byte	0x3
 353 00b4 9B       		.byte	0x9b
 354 00b5 370B0000 		.long	0xb37
 355 00b9 05       		.uleb128 0x5
 356 00ba 03       		.byte	0x3
 357 00bb 9C       		.byte	0x9c
 358 00bc 520B0000 		.long	0xb52
 359 00c0 05       		.uleb128 0x5
 360 00c1 03       		.byte	0x3
 361 00c2 9E       		.byte	0x9e
 362 00c3 680B0000 		.long	0xb68
 363 00c7 05       		.uleb128 0x5
 364 00c8 03       		.byte	0x3
 365 00c9 A0       		.byte	0xa0
GAS LISTING /tmp/cce2OND8.s 			page 12


 366 00ca 890B0000 		.long	0xb89
 367 00ce 05       		.uleb128 0x5
 368 00cf 03       		.byte	0x3
 369 00d0 A1       		.byte	0xa1
 370 00d1 A50B0000 		.long	0xba5
 371 00d5 05       		.uleb128 0x5
 372 00d6 03       		.byte	0x3
 373 00d7 A2       		.byte	0xa2
 374 00d8 C00B0000 		.long	0xbc0
 375 00dc 05       		.uleb128 0x5
 376 00dd 03       		.byte	0x3
 377 00de A4       		.byte	0xa4
 378 00df E60B0000 		.long	0xbe6
 379 00e3 05       		.uleb128 0x5
 380 00e4 03       		.byte	0x3
 381 00e5 A7       		.byte	0xa7
 382 00e6 060C0000 		.long	0xc06
 383 00ea 05       		.uleb128 0x5
 384 00eb 03       		.byte	0x3
 385 00ec AA       		.byte	0xaa
 386 00ed 2B0C0000 		.long	0xc2b
 387 00f1 05       		.uleb128 0x5
 388 00f2 03       		.byte	0x3
 389 00f3 AC       		.byte	0xac
 390 00f4 4B0C0000 		.long	0xc4b
 391 00f8 05       		.uleb128 0x5
 392 00f9 03       		.byte	0x3
 393 00fa AE       		.byte	0xae
 394 00fb 660C0000 		.long	0xc66
 395 00ff 05       		.uleb128 0x5
 396 0100 03       		.byte	0x3
 397 0101 B0       		.byte	0xb0
 398 0102 810C0000 		.long	0xc81
 399 0106 05       		.uleb128 0x5
 400 0107 03       		.byte	0x3
 401 0108 B1       		.byte	0xb1
 402 0109 A70C0000 		.long	0xca7
 403 010d 05       		.uleb128 0x5
 404 010e 03       		.byte	0x3
 405 010f B2       		.byte	0xb2
 406 0110 C10C0000 		.long	0xcc1
 407 0114 05       		.uleb128 0x5
 408 0115 03       		.byte	0x3
 409 0116 B3       		.byte	0xb3
 410 0117 DB0C0000 		.long	0xcdb
 411 011b 05       		.uleb128 0x5
 412 011c 03       		.byte	0x3
 413 011d B4       		.byte	0xb4
 414 011e F50C0000 		.long	0xcf5
 415 0122 05       		.uleb128 0x5
 416 0123 03       		.byte	0x3
 417 0124 B5       		.byte	0xb5
 418 0125 0F0D0000 		.long	0xd0f
 419 0129 05       		.uleb128 0x5
 420 012a 03       		.byte	0x3
 421 012b B6       		.byte	0xb6
 422 012c 290D0000 		.long	0xd29
GAS LISTING /tmp/cce2OND8.s 			page 13


 423 0130 05       		.uleb128 0x5
 424 0131 03       		.byte	0x3
 425 0132 B7       		.byte	0xb7
 426 0133 E90D0000 		.long	0xde9
 427 0137 05       		.uleb128 0x5
 428 0138 03       		.byte	0x3
 429 0139 B8       		.byte	0xb8
 430 013a FF0D0000 		.long	0xdff
 431 013e 05       		.uleb128 0x5
 432 013f 03       		.byte	0x3
 433 0140 B9       		.byte	0xb9
 434 0141 1E0E0000 		.long	0xe1e
 435 0145 05       		.uleb128 0x5
 436 0146 03       		.byte	0x3
 437 0147 BA       		.byte	0xba
 438 0148 3D0E0000 		.long	0xe3d
 439 014c 05       		.uleb128 0x5
 440 014d 03       		.byte	0x3
 441 014e BB       		.byte	0xbb
 442 014f 5C0E0000 		.long	0xe5c
 443 0153 05       		.uleb128 0x5
 444 0154 03       		.byte	0x3
 445 0155 BC       		.byte	0xbc
 446 0156 870E0000 		.long	0xe87
 447 015a 05       		.uleb128 0x5
 448 015b 03       		.byte	0x3
 449 015c BD       		.byte	0xbd
 450 015d A20E0000 		.long	0xea2
 451 0161 05       		.uleb128 0x5
 452 0162 03       		.byte	0x3
 453 0163 BF       		.byte	0xbf
 454 0164 CA0E0000 		.long	0xeca
 455 0168 05       		.uleb128 0x5
 456 0169 03       		.byte	0x3
 457 016a C1       		.byte	0xc1
 458 016b EC0E0000 		.long	0xeec
 459 016f 05       		.uleb128 0x5
 460 0170 03       		.byte	0x3
 461 0171 C2       		.byte	0xc2
 462 0172 0C0F0000 		.long	0xf0c
 463 0176 05       		.uleb128 0x5
 464 0177 03       		.byte	0x3
 465 0178 C3       		.byte	0xc3
 466 0179 330F0000 		.long	0xf33
 467 017d 05       		.uleb128 0x5
 468 017e 03       		.byte	0x3
 469 017f C4       		.byte	0xc4
 470 0180 530F0000 		.long	0xf53
 471 0184 05       		.uleb128 0x5
 472 0185 03       		.byte	0x3
 473 0186 C5       		.byte	0xc5
 474 0187 720F0000 		.long	0xf72
 475 018b 05       		.uleb128 0x5
 476 018c 03       		.byte	0x3
 477 018d C6       		.byte	0xc6
 478 018e 880F0000 		.long	0xf88
 479 0192 05       		.uleb128 0x5
GAS LISTING /tmp/cce2OND8.s 			page 14


 480 0193 03       		.byte	0x3
 481 0194 C7       		.byte	0xc7
 482 0195 A80F0000 		.long	0xfa8
 483 0199 05       		.uleb128 0x5
 484 019a 03       		.byte	0x3
 485 019b C8       		.byte	0xc8
 486 019c C80F0000 		.long	0xfc8
 487 01a0 05       		.uleb128 0x5
 488 01a1 03       		.byte	0x3
 489 01a2 C9       		.byte	0xc9
 490 01a3 E80F0000 		.long	0xfe8
 491 01a7 05       		.uleb128 0x5
 492 01a8 03       		.byte	0x3
 493 01a9 CA       		.byte	0xca
 494 01aa 08100000 		.long	0x1008
 495 01ae 05       		.uleb128 0x5
 496 01af 03       		.byte	0x3
 497 01b0 CB       		.byte	0xcb
 498 01b1 1F100000 		.long	0x101f
 499 01b5 05       		.uleb128 0x5
 500 01b6 03       		.byte	0x3
 501 01b7 CC       		.byte	0xcc
 502 01b8 36100000 		.long	0x1036
 503 01bc 05       		.uleb128 0x5
 504 01bd 03       		.byte	0x3
 505 01be CD       		.byte	0xcd
 506 01bf 54100000 		.long	0x1054
 507 01c3 05       		.uleb128 0x5
 508 01c4 03       		.byte	0x3
 509 01c5 CE       		.byte	0xce
 510 01c6 73100000 		.long	0x1073
 511 01ca 05       		.uleb128 0x5
 512 01cb 03       		.byte	0x3
 513 01cc CF       		.byte	0xcf
 514 01cd 91100000 		.long	0x1091
 515 01d1 05       		.uleb128 0x5
 516 01d2 03       		.byte	0x3
 517 01d3 D0       		.byte	0xd0
 518 01d4 B0100000 		.long	0x10b0
 519 01d8 06       		.uleb128 0x6
 520 01d9 03       		.byte	0x3
 521 01da 0801     		.value	0x108
 522 01dc D4100000 		.long	0x10d4
 523 01e0 06       		.uleb128 0x6
 524 01e1 03       		.byte	0x3
 525 01e2 0901     		.value	0x109
 526 01e4 F6100000 		.long	0x10f6
 527 01e8 06       		.uleb128 0x6
 528 01e9 03       		.byte	0x3
 529 01ea 0A01     		.value	0x10a
 530 01ec 1D110000 		.long	0x111d
 531 01f0 03       		.uleb128 0x3
 532 01f1 00000000 		.long	.LASF1
 533 01f5 05       		.byte	0x5
 534 01f6 30       		.byte	0x30
 535 01f7 07       		.uleb128 0x7
 536 01f8 00000000 		.long	.LASF40
GAS LISTING /tmp/cce2OND8.s 			page 15


 537 01fc 01       		.byte	0x1
 538 01fd 06       		.byte	0x6
 539 01fe E9       		.byte	0xe9
 540 01ff BF030000 		.long	0x3bf
 541 0203 08       		.uleb128 0x8
 542 0204 00000000 		.long	.LASF2
 543 0208 06       		.byte	0x6
 544 0209 EB       		.byte	0xeb
 545 020a 50090000 		.long	0x950
 546 020e 08       		.uleb128 0x8
 547 020f 00000000 		.long	.LASF3
 548 0213 06       		.byte	0x6
 549 0214 EC       		.byte	0xec
 550 0215 57090000 		.long	0x957
 551 0219 09       		.uleb128 0x9
 552 021a 00000000 		.long	.LASF16
 553 021e 06       		.byte	0x6
 554 021f F2       		.byte	0xf2
 555 0220 00000000 		.long	.LASF268
 556 0224 33020000 		.long	0x233
 557 0228 0A       		.uleb128 0xa
 558 0229 6C110000 		.long	0x116c
 559 022d 0A       		.uleb128 0xa
 560 022e 72110000 		.long	0x1172
 561 0232 00       		.byte	0
 562 0233 0B       		.uleb128 0xb
 563 0234 03020000 		.long	0x203
 564 0238 0C       		.uleb128 0xc
 565 0239 657100   		.string	"eq"
 566 023c 06       		.byte	0x6
 567 023d F6       		.byte	0xf6
 568 023e 00000000 		.long	.LASF4
 569 0242 78110000 		.long	0x1178
 570 0246 55020000 		.long	0x255
 571 024a 0A       		.uleb128 0xa
 572 024b 72110000 		.long	0x1172
 573 024f 0A       		.uleb128 0xa
 574 0250 72110000 		.long	0x1172
 575 0254 00       		.byte	0
 576 0255 0C       		.uleb128 0xc
 577 0256 6C7400   		.string	"lt"
 578 0259 06       		.byte	0x6
 579 025a FA       		.byte	0xfa
 580 025b 00000000 		.long	.LASF5
 581 025f 78110000 		.long	0x1178
 582 0263 72020000 		.long	0x272
 583 0267 0A       		.uleb128 0xa
 584 0268 72110000 		.long	0x1172
 585 026c 0A       		.uleb128 0xa
 586 026d 72110000 		.long	0x1172
 587 0271 00       		.byte	0
 588 0272 0D       		.uleb128 0xd
 589 0273 00000000 		.long	.LASF6
 590 0277 06       		.byte	0x6
 591 0278 0201     		.value	0x102
 592 027a 00000000 		.long	.LASF8
 593 027e 57090000 		.long	0x957
GAS LISTING /tmp/cce2OND8.s 			page 16


 594 0282 96020000 		.long	0x296
 595 0286 0A       		.uleb128 0xa
 596 0287 7F110000 		.long	0x117f
 597 028b 0A       		.uleb128 0xa
 598 028c 7F110000 		.long	0x117f
 599 0290 0A       		.uleb128 0xa
 600 0291 BF030000 		.long	0x3bf
 601 0295 00       		.byte	0
 602 0296 0D       		.uleb128 0xd
 603 0297 00000000 		.long	.LASF7
 604 029b 06       		.byte	0x6
 605 029c 0A01     		.value	0x10a
 606 029e 00000000 		.long	.LASF9
 607 02a2 BF030000 		.long	0x3bf
 608 02a6 B0020000 		.long	0x2b0
 609 02aa 0A       		.uleb128 0xa
 610 02ab 7F110000 		.long	0x117f
 611 02af 00       		.byte	0
 612 02b0 0D       		.uleb128 0xd
 613 02b1 00000000 		.long	.LASF10
 614 02b5 06       		.byte	0x6
 615 02b6 0E01     		.value	0x10e
 616 02b8 00000000 		.long	.LASF11
 617 02bc 7F110000 		.long	0x117f
 618 02c0 D4020000 		.long	0x2d4
 619 02c4 0A       		.uleb128 0xa
 620 02c5 7F110000 		.long	0x117f
 621 02c9 0A       		.uleb128 0xa
 622 02ca BF030000 		.long	0x3bf
 623 02ce 0A       		.uleb128 0xa
 624 02cf 72110000 		.long	0x1172
 625 02d3 00       		.byte	0
 626 02d4 0D       		.uleb128 0xd
 627 02d5 00000000 		.long	.LASF12
 628 02d9 06       		.byte	0x6
 629 02da 1601     		.value	0x116
 630 02dc 00000000 		.long	.LASF13
 631 02e0 85110000 		.long	0x1185
 632 02e4 F8020000 		.long	0x2f8
 633 02e8 0A       		.uleb128 0xa
 634 02e9 85110000 		.long	0x1185
 635 02ed 0A       		.uleb128 0xa
 636 02ee 7F110000 		.long	0x117f
 637 02f2 0A       		.uleb128 0xa
 638 02f3 BF030000 		.long	0x3bf
 639 02f7 00       		.byte	0
 640 02f8 0D       		.uleb128 0xd
 641 02f9 00000000 		.long	.LASF14
 642 02fd 06       		.byte	0x6
 643 02fe 1E01     		.value	0x11e
 644 0300 00000000 		.long	.LASF15
 645 0304 85110000 		.long	0x1185
 646 0308 1C030000 		.long	0x31c
 647 030c 0A       		.uleb128 0xa
 648 030d 85110000 		.long	0x1185
 649 0311 0A       		.uleb128 0xa
 650 0312 7F110000 		.long	0x117f
GAS LISTING /tmp/cce2OND8.s 			page 17


 651 0316 0A       		.uleb128 0xa
 652 0317 BF030000 		.long	0x3bf
 653 031b 00       		.byte	0
 654 031c 0D       		.uleb128 0xd
 655 031d 00000000 		.long	.LASF16
 656 0321 06       		.byte	0x6
 657 0322 2601     		.value	0x126
 658 0324 00000000 		.long	.LASF17
 659 0328 85110000 		.long	0x1185
 660 032c 40030000 		.long	0x340
 661 0330 0A       		.uleb128 0xa
 662 0331 85110000 		.long	0x1185
 663 0335 0A       		.uleb128 0xa
 664 0336 BF030000 		.long	0x3bf
 665 033a 0A       		.uleb128 0xa
 666 033b 03020000 		.long	0x203
 667 033f 00       		.byte	0
 668 0340 0D       		.uleb128 0xd
 669 0341 00000000 		.long	.LASF18
 670 0345 06       		.byte	0x6
 671 0346 2E01     		.value	0x12e
 672 0348 00000000 		.long	.LASF19
 673 034c 03020000 		.long	0x203
 674 0350 5A030000 		.long	0x35a
 675 0354 0A       		.uleb128 0xa
 676 0355 8B110000 		.long	0x118b
 677 0359 00       		.byte	0
 678 035a 0B       		.uleb128 0xb
 679 035b 0E020000 		.long	0x20e
 680 035f 0D       		.uleb128 0xd
 681 0360 00000000 		.long	.LASF20
 682 0364 06       		.byte	0x6
 683 0365 3401     		.value	0x134
 684 0367 00000000 		.long	.LASF21
 685 036b 0E020000 		.long	0x20e
 686 036f 79030000 		.long	0x379
 687 0373 0A       		.uleb128 0xa
 688 0374 72110000 		.long	0x1172
 689 0378 00       		.byte	0
 690 0379 0D       		.uleb128 0xd
 691 037a 00000000 		.long	.LASF22
 692 037e 06       		.byte	0x6
 693 037f 3801     		.value	0x138
 694 0381 00000000 		.long	.LASF23
 695 0385 78110000 		.long	0x1178
 696 0389 98030000 		.long	0x398
 697 038d 0A       		.uleb128 0xa
 698 038e 8B110000 		.long	0x118b
 699 0392 0A       		.uleb128 0xa
 700 0393 8B110000 		.long	0x118b
 701 0397 00       		.byte	0
 702 0398 0E       		.uleb128 0xe
 703 0399 656F6600 		.string	"eof"
 704 039d 06       		.byte	0x6
 705 039e 3C01     		.value	0x13c
 706 03a0 00000000 		.long	.LASF269
 707 03a4 0E020000 		.long	0x20e
GAS LISTING /tmp/cce2OND8.s 			page 18


 708 03a8 0F       		.uleb128 0xf
 709 03a9 00000000 		.long	.LASF24
 710 03ad 06       		.byte	0x6
 711 03ae 4001     		.value	0x140
 712 03b0 00000000 		.long	.LASF270
 713 03b4 0E020000 		.long	0x20e
 714 03b8 0A       		.uleb128 0xa
 715 03b9 8B110000 		.long	0x118b
 716 03bd 00       		.byte	0
 717 03be 00       		.byte	0
 718 03bf 08       		.uleb128 0x8
 719 03c0 00000000 		.long	.LASF25
 720 03c4 04       		.byte	0x4
 721 03c5 C4       		.byte	0xc4
 722 03c6 E9080000 		.long	0x8e9
 723 03ca 05       		.uleb128 0x5
 724 03cb 07       		.byte	0x7
 725 03cc 35       		.byte	0x35
 726 03cd 91110000 		.long	0x1191
 727 03d1 05       		.uleb128 0x5
 728 03d2 07       		.byte	0x7
 729 03d3 36       		.byte	0x36
 730 03d4 BE120000 		.long	0x12be
 731 03d8 05       		.uleb128 0x5
 732 03d9 07       		.byte	0x7
 733 03da 37       		.byte	0x37
 734 03db D8120000 		.long	0x12d8
 735 03df 08       		.uleb128 0x8
 736 03e0 00000000 		.long	.LASF26
 737 03e4 04       		.byte	0x4
 738 03e5 C5       		.byte	0xc5
 739 03e6 2C0F0000 		.long	0xf2c
 740 03ea 10       		.uleb128 0x10
 741 03eb 00000000 		.long	.LASF271
 742 03ef 04       		.byte	0x4
 743 03f0 57090000 		.long	0x957
 744 03f4 01       		.byte	0x1
 745 03f5 6F       		.byte	0x6f
 746 03f6 3B040000 		.long	0x43b
 747 03fa 11       		.uleb128 0x11
 748 03fb 00000000 		.long	.LASF27
 749 03ff 01       		.byte	0x1
 750 0400 11       		.uleb128 0x11
 751 0401 00000000 		.long	.LASF28
 752 0405 02       		.byte	0x2
 753 0406 11       		.uleb128 0x11
 754 0407 00000000 		.long	.LASF29
 755 040b 04       		.byte	0x4
 756 040c 11       		.uleb128 0x11
 757 040d 00000000 		.long	.LASF30
 758 0411 08       		.byte	0x8
 759 0412 11       		.uleb128 0x11
 760 0413 00000000 		.long	.LASF31
 761 0417 10       		.byte	0x10
 762 0418 11       		.uleb128 0x11
 763 0419 00000000 		.long	.LASF32
 764 041d 20       		.byte	0x20
GAS LISTING /tmp/cce2OND8.s 			page 19


 765 041e 12       		.uleb128 0x12
 766 041f 00000000 		.long	.LASF33
 767 0423 00000100 		.long	0x10000
 768 0427 12       		.uleb128 0x12
 769 0428 00000000 		.long	.LASF34
 770 042c FFFFFF7F 		.long	0x7fffffff
 771 0430 13       		.uleb128 0x13
 772 0431 00000000 		.long	.LASF35
 773 0435 80808080 		.sleb128 -2147483648
 773      78
 774 043a 00       		.byte	0
 775 043b 05       		.uleb128 0x5
 776 043c 08       		.byte	0x8
 777 043d 52       		.byte	0x52
 778 043e 1F130000 		.long	0x131f
 779 0442 05       		.uleb128 0x5
 780 0443 08       		.byte	0x8
 781 0444 53       		.byte	0x53
 782 0445 14130000 		.long	0x1314
 783 0449 05       		.uleb128 0x5
 784 044a 08       		.byte	0x8
 785 044b 54       		.byte	0x54
 786 044c F0080000 		.long	0x8f0
 787 0450 05       		.uleb128 0x5
 788 0451 08       		.byte	0x8
 789 0452 5C       		.byte	0x5c
 790 0453 35130000 		.long	0x1335
 791 0457 05       		.uleb128 0x5
 792 0458 08       		.byte	0x8
 793 0459 65       		.byte	0x65
 794 045a 4F130000 		.long	0x134f
 795 045e 05       		.uleb128 0x5
 796 045f 08       		.byte	0x8
 797 0460 68       		.byte	0x68
 798 0461 69130000 		.long	0x1369
 799 0465 05       		.uleb128 0x5
 800 0466 08       		.byte	0x8
 801 0467 69       		.byte	0x69
 802 0468 7E130000 		.long	0x137e
 803 046c 05       		.uleb128 0x5
 804 046d 09       		.byte	0x9
 805 046e 62       		.byte	0x62
 806 046f FE060000 		.long	0x6fe
 807 0473 05       		.uleb128 0x5
 808 0474 09       		.byte	0x9
 809 0475 63       		.byte	0x63
 810 0476 37140000 		.long	0x1437
 811 047a 05       		.uleb128 0x5
 812 047b 09       		.byte	0x9
 813 047c 65       		.byte	0x65
 814 047d 42140000 		.long	0x1442
 815 0481 05       		.uleb128 0x5
 816 0482 09       		.byte	0x9
 817 0483 66       		.byte	0x66
 818 0484 5A140000 		.long	0x145a
 819 0488 05       		.uleb128 0x5
 820 0489 09       		.byte	0x9
GAS LISTING /tmp/cce2OND8.s 			page 20


 821 048a 67       		.byte	0x67
 822 048b 6F140000 		.long	0x146f
 823 048f 05       		.uleb128 0x5
 824 0490 09       		.byte	0x9
 825 0491 68       		.byte	0x68
 826 0492 85140000 		.long	0x1485
 827 0496 05       		.uleb128 0x5
 828 0497 09       		.byte	0x9
 829 0498 69       		.byte	0x69
 830 0499 9B140000 		.long	0x149b
 831 049d 05       		.uleb128 0x5
 832 049e 09       		.byte	0x9
 833 049f 6A       		.byte	0x6a
 834 04a0 B0140000 		.long	0x14b0
 835 04a4 05       		.uleb128 0x5
 836 04a5 09       		.byte	0x9
 837 04a6 6B       		.byte	0x6b
 838 04a7 C6140000 		.long	0x14c6
 839 04ab 05       		.uleb128 0x5
 840 04ac 09       		.byte	0x9
 841 04ad 6C       		.byte	0x6c
 842 04ae E7140000 		.long	0x14e7
 843 04b2 05       		.uleb128 0x5
 844 04b3 09       		.byte	0x9
 845 04b4 6D       		.byte	0x6d
 846 04b5 07150000 		.long	0x1507
 847 04b9 05       		.uleb128 0x5
 848 04ba 09       		.byte	0x9
 849 04bb 71       		.byte	0x71
 850 04bc 22150000 		.long	0x1522
 851 04c0 05       		.uleb128 0x5
 852 04c1 09       		.byte	0x9
 853 04c2 72       		.byte	0x72
 854 04c3 47150000 		.long	0x1547
 855 04c7 05       		.uleb128 0x5
 856 04c8 09       		.byte	0x9
 857 04c9 74       		.byte	0x74
 858 04ca 67150000 		.long	0x1567
 859 04ce 05       		.uleb128 0x5
 860 04cf 09       		.byte	0x9
 861 04d0 75       		.byte	0x75
 862 04d1 87150000 		.long	0x1587
 863 04d5 05       		.uleb128 0x5
 864 04d6 09       		.byte	0x9
 865 04d7 76       		.byte	0x76
 866 04d8 AD150000 		.long	0x15ad
 867 04dc 05       		.uleb128 0x5
 868 04dd 09       		.byte	0x9
 869 04de 78       		.byte	0x78
 870 04df C3150000 		.long	0x15c3
 871 04e3 05       		.uleb128 0x5
 872 04e4 09       		.byte	0x9
 873 04e5 79       		.byte	0x79
 874 04e6 D9150000 		.long	0x15d9
 875 04ea 05       		.uleb128 0x5
 876 04eb 09       		.byte	0x9
 877 04ec 7C       		.byte	0x7c
GAS LISTING /tmp/cce2OND8.s 			page 21


 878 04ed E5150000 		.long	0x15e5
 879 04f1 05       		.uleb128 0x5
 880 04f2 09       		.byte	0x9
 881 04f3 7E       		.byte	0x7e
 882 04f4 FB150000 		.long	0x15fb
 883 04f8 05       		.uleb128 0x5
 884 04f9 09       		.byte	0x9
 885 04fa 83       		.byte	0x83
 886 04fb 0D160000 		.long	0x160d
 887 04ff 05       		.uleb128 0x5
 888 0500 09       		.byte	0x9
 889 0501 84       		.byte	0x84
 890 0502 22160000 		.long	0x1622
 891 0506 05       		.uleb128 0x5
 892 0507 09       		.byte	0x9
 893 0508 85       		.byte	0x85
 894 0509 3C160000 		.long	0x163c
 895 050d 05       		.uleb128 0x5
 896 050e 09       		.byte	0x9
 897 050f 87       		.byte	0x87
 898 0510 4E160000 		.long	0x164e
 899 0514 05       		.uleb128 0x5
 900 0515 09       		.byte	0x9
 901 0516 88       		.byte	0x88
 902 0517 65160000 		.long	0x1665
 903 051b 05       		.uleb128 0x5
 904 051c 09       		.byte	0x9
 905 051d 8B       		.byte	0x8b
 906 051e 8A160000 		.long	0x168a
 907 0522 05       		.uleb128 0x5
 908 0523 09       		.byte	0x9
 909 0524 8D       		.byte	0x8d
 910 0525 95160000 		.long	0x1695
 911 0529 05       		.uleb128 0x5
 912 052a 09       		.byte	0x9
 913 052b 8F       		.byte	0x8f
 914 052c AA160000 		.long	0x16aa
 915 0530 14       		.uleb128 0x14
 916 0531 00000000 		.long	.LASF36
 917 0535 01       		.byte	0x1
 918 0536 81       		.byte	0x81
 919 0537 00000000 		.long	.LASF166
 920 053b EA030000 		.long	0x3ea
 921 053f 4E050000 		.long	0x54e
 922 0543 0A       		.uleb128 0xa
 923 0544 EA030000 		.long	0x3ea
 924 0548 0A       		.uleb128 0xa
 925 0549 EA030000 		.long	0x3ea
 926 054d 00       		.byte	0
 927 054e 08       		.uleb128 0x8
 928 054f 00000000 		.long	.LASF37
 929 0553 0A       		.byte	0xa
 930 0554 A5       		.byte	0xa5
 931 0555 59050000 		.long	0x559
 932 0559 15       		.uleb128 0x15
 933 055a 00000000 		.long	.LASF272
 934 055e 16       		.uleb128 0x16
GAS LISTING /tmp/cce2OND8.s 			page 22


 935 055f 00000000 		.long	.LASF46
 936 0563 50090000 		.long	0x950
 937 0567 17       		.uleb128 0x17
 938 0568 00000000 		.long	.LASF273
 939 056c F7010000 		.long	0x1f7
 940 0570 00       		.byte	0
 941 0571 00       		.byte	0
 942 0572 18       		.uleb128 0x18
 943 0573 00000000 		.long	.LASF38
 944 0577 04       		.byte	0x4
 945 0578 DD       		.byte	0xdd
 946 0579 FE060000 		.long	0x6fe
 947 057d 03       		.uleb128 0x3
 948 057e 00000000 		.long	.LASF0
 949 0582 04       		.byte	0x4
 950 0583 DE       		.byte	0xde
 951 0584 04       		.uleb128 0x4
 952 0585 04       		.byte	0x4
 953 0586 DE       		.byte	0xde
 954 0587 7D050000 		.long	0x57d
 955 058b 05       		.uleb128 0x5
 956 058c 03       		.byte	0x3
 957 058d F8       		.byte	0xf8
 958 058e D4100000 		.long	0x10d4
 959 0592 06       		.uleb128 0x6
 960 0593 03       		.byte	0x3
 961 0594 0101     		.value	0x101
 962 0596 F6100000 		.long	0x10f6
 963 059a 06       		.uleb128 0x6
 964 059b 03       		.byte	0x3
 965 059c 0201     		.value	0x102
 966 059e 1D110000 		.long	0x111d
 967 05a2 03       		.uleb128 0x3
 968 05a3 00000000 		.long	.LASF39
 969 05a7 0B       		.byte	0xb
 970 05a8 24       		.byte	0x24
 971 05a9 05       		.uleb128 0x5
 972 05aa 0C       		.byte	0xc
 973 05ab 2C       		.byte	0x2c
 974 05ac BF030000 		.long	0x3bf
 975 05b0 05       		.uleb128 0x5
 976 05b1 0C       		.byte	0xc
 977 05b2 2D       		.byte	0x2d
 978 05b3 DF030000 		.long	0x3df
 979 05b7 07       		.uleb128 0x7
 980 05b8 00000000 		.long	.LASF41
 981 05bc 01       		.byte	0x1
 982 05bd 0D       		.byte	0xd
 983 05be 37       		.byte	0x37
 984 05bf F9050000 		.long	0x5f9
 985 05c3 19       		.uleb128 0x19
 986 05c4 00000000 		.long	.LASF42
 987 05c8 0D       		.byte	0xd
 988 05c9 3A       		.byte	0x3a
 989 05ca 7B090000 		.long	0x97b
 990 05ce 19       		.uleb128 0x19
 991 05cf 00000000 		.long	.LASF43
GAS LISTING /tmp/cce2OND8.s 			page 23


 992 05d3 0D       		.byte	0xd
 993 05d4 3B       		.byte	0x3b
 994 05d5 7B090000 		.long	0x97b
 995 05d9 19       		.uleb128 0x19
 996 05da 00000000 		.long	.LASF44
 997 05de 0D       		.byte	0xd
 998 05df 3F       		.byte	0x3f
 999 05e0 0A130000 		.long	0x130a
 1000 05e4 19       		.uleb128 0x19
 1001 05e5 00000000 		.long	.LASF45
 1002 05e9 0D       		.byte	0xd
 1003 05ea 40       		.byte	0x40
 1004 05eb 7B090000 		.long	0x97b
 1005 05ef 16       		.uleb128 0x16
 1006 05f0 00000000 		.long	.LASF47
 1007 05f4 57090000 		.long	0x957
 1008 05f8 00       		.byte	0
 1009 05f9 07       		.uleb128 0x7
 1010 05fa 00000000 		.long	.LASF48
 1011 05fe 01       		.byte	0x1
 1012 05ff 0D       		.byte	0xd
 1013 0600 37       		.byte	0x37
 1014 0601 3B060000 		.long	0x63b
 1015 0605 19       		.uleb128 0x19
 1016 0606 00000000 		.long	.LASF42
 1017 060a 0D       		.byte	0xd
 1018 060b 3A       		.byte	0x3a
 1019 060c 0F130000 		.long	0x130f
 1020 0610 19       		.uleb128 0x19
 1021 0611 00000000 		.long	.LASF43
 1022 0615 0D       		.byte	0xd
 1023 0616 3B       		.byte	0x3b
 1024 0617 0F130000 		.long	0x130f
 1025 061b 19       		.uleb128 0x19
 1026 061c 00000000 		.long	.LASF44
 1027 0620 0D       		.byte	0xd
 1028 0621 3F       		.byte	0x3f
 1029 0622 0A130000 		.long	0x130a
 1030 0626 19       		.uleb128 0x19
 1031 0627 00000000 		.long	.LASF45
 1032 062b 0D       		.byte	0xd
 1033 062c 40       		.byte	0x40
 1034 062d 7B090000 		.long	0x97b
 1035 0631 16       		.uleb128 0x16
 1036 0632 00000000 		.long	.LASF47
 1037 0636 E9080000 		.long	0x8e9
 1038 063a 00       		.byte	0
 1039 063b 07       		.uleb128 0x7
 1040 063c 00000000 		.long	.LASF49
 1041 0640 01       		.byte	0x1
 1042 0641 0D       		.byte	0xd
 1043 0642 37       		.byte	0x37
 1044 0643 7D060000 		.long	0x67d
 1045 0647 19       		.uleb128 0x19
 1046 0648 00000000 		.long	.LASF42
 1047 064c 0D       		.byte	0xd
 1048 064d 3A       		.byte	0x3a
GAS LISTING /tmp/cce2OND8.s 			page 24


 1049 064e 86090000 		.long	0x986
 1050 0652 19       		.uleb128 0x19
 1051 0653 00000000 		.long	.LASF43
 1052 0657 0D       		.byte	0xd
 1053 0658 3B       		.byte	0x3b
 1054 0659 86090000 		.long	0x986
 1055 065d 19       		.uleb128 0x19
 1056 065e 00000000 		.long	.LASF44
 1057 0662 0D       		.byte	0xd
 1058 0663 3F       		.byte	0x3f
 1059 0664 0A130000 		.long	0x130a
 1060 0668 19       		.uleb128 0x19
 1061 0669 00000000 		.long	.LASF45
 1062 066d 0D       		.byte	0xd
 1063 066e 40       		.byte	0x40
 1064 066f 7B090000 		.long	0x97b
 1065 0673 16       		.uleb128 0x16
 1066 0674 00000000 		.long	.LASF47
 1067 0678 50090000 		.long	0x950
 1068 067c 00       		.byte	0
 1069 067d 07       		.uleb128 0x7
 1070 067e 00000000 		.long	.LASF50
 1071 0682 01       		.byte	0x1
 1072 0683 0D       		.byte	0xd
 1073 0684 37       		.byte	0x37
 1074 0685 BF060000 		.long	0x6bf
 1075 0689 19       		.uleb128 0x19
 1076 068a 00000000 		.long	.LASF42
 1077 068e 0D       		.byte	0xd
 1078 068f 3A       		.byte	0x3a
 1079 0690 93130000 		.long	0x1393
 1080 0694 19       		.uleb128 0x19
 1081 0695 00000000 		.long	.LASF43
 1082 0699 0D       		.byte	0xd
 1083 069a 3B       		.byte	0x3b
 1084 069b 93130000 		.long	0x1393
 1085 069f 19       		.uleb128 0x19
 1086 06a0 00000000 		.long	.LASF44
 1087 06a4 0D       		.byte	0xd
 1088 06a5 3F       		.byte	0x3f
 1089 06a6 0A130000 		.long	0x130a
 1090 06aa 19       		.uleb128 0x19
 1091 06ab 00000000 		.long	.LASF45
 1092 06af 0D       		.byte	0xd
 1093 06b0 40       		.byte	0x40
 1094 06b1 7B090000 		.long	0x97b
 1095 06b5 16       		.uleb128 0x16
 1096 06b6 00000000 		.long	.LASF47
 1097 06ba 52110000 		.long	0x1152
 1098 06be 00       		.byte	0
 1099 06bf 1A       		.uleb128 0x1a
 1100 06c0 00000000 		.long	.LASF274
 1101 06c4 01       		.byte	0x1
 1102 06c5 0D       		.byte	0xd
 1103 06c6 37       		.byte	0x37
 1104 06c7 19       		.uleb128 0x19
 1105 06c8 00000000 		.long	.LASF42
GAS LISTING /tmp/cce2OND8.s 			page 25


 1106 06cc 0D       		.byte	0xd
 1107 06cd 3A       		.byte	0x3a
 1108 06ce 98130000 		.long	0x1398
 1109 06d2 19       		.uleb128 0x19
 1110 06d3 00000000 		.long	.LASF43
 1111 06d7 0D       		.byte	0xd
 1112 06d8 3B       		.byte	0x3b
 1113 06d9 98130000 		.long	0x1398
 1114 06dd 19       		.uleb128 0x19
 1115 06de 00000000 		.long	.LASF44
 1116 06e2 0D       		.byte	0xd
 1117 06e3 3F       		.byte	0x3f
 1118 06e4 0A130000 		.long	0x130a
 1119 06e8 19       		.uleb128 0x19
 1120 06e9 00000000 		.long	.LASF45
 1121 06ed 0D       		.byte	0xd
 1122 06ee 40       		.byte	0x40
 1123 06ef 7B090000 		.long	0x97b
 1124 06f3 16       		.uleb128 0x16
 1125 06f4 00000000 		.long	.LASF47
 1126 06f8 2C0F0000 		.long	0xf2c
 1127 06fc 00       		.byte	0
 1128 06fd 00       		.byte	0
 1129 06fe 08       		.uleb128 0x8
 1130 06ff 00000000 		.long	.LASF51
 1131 0703 0E       		.byte	0xe
 1132 0704 30       		.byte	0x30
 1133 0705 09070000 		.long	0x709
 1134 0709 07       		.uleb128 0x7
 1135 070a 00000000 		.long	.LASF52
 1136 070e D8       		.byte	0xd8
 1137 070f 0F       		.byte	0xf
 1138 0710 F1       		.byte	0xf1
 1139 0711 86080000 		.long	0x886
 1140 0715 1B       		.uleb128 0x1b
 1141 0716 00000000 		.long	.LASF53
 1142 071a 0F       		.byte	0xf
 1143 071b F2       		.byte	0xf2
 1144 071c 57090000 		.long	0x957
 1145 0720 00       		.byte	0
 1146 0721 1B       		.uleb128 0x1b
 1147 0722 00000000 		.long	.LASF54
 1148 0726 0F       		.byte	0xf
 1149 0727 F7       		.byte	0xf7
 1150 0728 A10C0000 		.long	0xca1
 1151 072c 08       		.byte	0x8
 1152 072d 1B       		.uleb128 0x1b
 1153 072e 00000000 		.long	.LASF55
 1154 0732 0F       		.byte	0xf
 1155 0733 F8       		.byte	0xf8
 1156 0734 A10C0000 		.long	0xca1
 1157 0738 10       		.byte	0x10
 1158 0739 1B       		.uleb128 0x1b
 1159 073a 00000000 		.long	.LASF56
 1160 073e 0F       		.byte	0xf
 1161 073f F9       		.byte	0xf9
 1162 0740 A10C0000 		.long	0xca1
GAS LISTING /tmp/cce2OND8.s 			page 26


 1163 0744 18       		.byte	0x18
 1164 0745 1B       		.uleb128 0x1b
 1165 0746 00000000 		.long	.LASF57
 1166 074a 0F       		.byte	0xf
 1167 074b FA       		.byte	0xfa
 1168 074c A10C0000 		.long	0xca1
 1169 0750 20       		.byte	0x20
 1170 0751 1B       		.uleb128 0x1b
 1171 0752 00000000 		.long	.LASF58
 1172 0756 0F       		.byte	0xf
 1173 0757 FB       		.byte	0xfb
 1174 0758 A10C0000 		.long	0xca1
 1175 075c 28       		.byte	0x28
 1176 075d 1B       		.uleb128 0x1b
 1177 075e 00000000 		.long	.LASF59
 1178 0762 0F       		.byte	0xf
 1179 0763 FC       		.byte	0xfc
 1180 0764 A10C0000 		.long	0xca1
 1181 0768 30       		.byte	0x30
 1182 0769 1B       		.uleb128 0x1b
 1183 076a 00000000 		.long	.LASF60
 1184 076e 0F       		.byte	0xf
 1185 076f FD       		.byte	0xfd
 1186 0770 A10C0000 		.long	0xca1
 1187 0774 38       		.byte	0x38
 1188 0775 1B       		.uleb128 0x1b
 1189 0776 00000000 		.long	.LASF61
 1190 077a 0F       		.byte	0xf
 1191 077b FE       		.byte	0xfe
 1192 077c A10C0000 		.long	0xca1
 1193 0780 40       		.byte	0x40
 1194 0781 1C       		.uleb128 0x1c
 1195 0782 00000000 		.long	.LASF62
 1196 0786 0F       		.byte	0xf
 1197 0787 0001     		.value	0x100
 1198 0789 A10C0000 		.long	0xca1
 1199 078d 48       		.byte	0x48
 1200 078e 1C       		.uleb128 0x1c
 1201 078f 00000000 		.long	.LASF63
 1202 0793 0F       		.byte	0xf
 1203 0794 0101     		.value	0x101
 1204 0796 A10C0000 		.long	0xca1
 1205 079a 50       		.byte	0x50
 1206 079b 1C       		.uleb128 0x1c
 1207 079c 00000000 		.long	.LASF64
 1208 07a0 0F       		.byte	0xf
 1209 07a1 0201     		.value	0x102
 1210 07a3 A10C0000 		.long	0xca1
 1211 07a7 58       		.byte	0x58
 1212 07a8 1C       		.uleb128 0x1c
 1213 07a9 00000000 		.long	.LASF65
 1214 07ad 0F       		.byte	0xf
 1215 07ae 0401     		.value	0x104
 1216 07b0 05140000 		.long	0x1405
 1217 07b4 60       		.byte	0x60
 1218 07b5 1C       		.uleb128 0x1c
 1219 07b6 00000000 		.long	.LASF66
GAS LISTING /tmp/cce2OND8.s 			page 27


 1220 07ba 0F       		.byte	0xf
 1221 07bb 0601     		.value	0x106
 1222 07bd 0B140000 		.long	0x140b
 1223 07c1 68       		.byte	0x68
 1224 07c2 1C       		.uleb128 0x1c
 1225 07c3 00000000 		.long	.LASF67
 1226 07c7 0F       		.byte	0xf
 1227 07c8 0801     		.value	0x108
 1228 07ca 57090000 		.long	0x957
 1229 07ce 70       		.byte	0x70
 1230 07cf 1C       		.uleb128 0x1c
 1231 07d0 00000000 		.long	.LASF68
 1232 07d4 0F       		.byte	0xf
 1233 07d5 0C01     		.value	0x10c
 1234 07d7 57090000 		.long	0x957
 1235 07db 74       		.byte	0x74
 1236 07dc 1C       		.uleb128 0x1c
 1237 07dd 00000000 		.long	.LASF69
 1238 07e1 0F       		.byte	0xf
 1239 07e2 0E01     		.value	0x10e
 1240 07e4 F4120000 		.long	0x12f4
 1241 07e8 78       		.byte	0x78
 1242 07e9 1C       		.uleb128 0x1c
 1243 07ea 00000000 		.long	.LASF70
 1244 07ee 0F       		.byte	0xf
 1245 07ef 1201     		.value	0x112
 1246 07f1 74090000 		.long	0x974
 1247 07f5 80       		.byte	0x80
 1248 07f6 1C       		.uleb128 0x1c
 1249 07f7 00000000 		.long	.LASF71
 1250 07fb 0F       		.byte	0xf
 1251 07fc 1301     		.value	0x113
 1252 07fe 4B110000 		.long	0x114b
 1253 0802 82       		.byte	0x82
 1254 0803 1C       		.uleb128 0x1c
 1255 0804 00000000 		.long	.LASF72
 1256 0808 0F       		.byte	0xf
 1257 0809 1401     		.value	0x114
 1258 080b 11140000 		.long	0x1411
 1259 080f 83       		.byte	0x83
 1260 0810 1C       		.uleb128 0x1c
 1261 0811 00000000 		.long	.LASF73
 1262 0815 0F       		.byte	0xf
 1263 0816 1801     		.value	0x118
 1264 0818 21140000 		.long	0x1421
 1265 081c 88       		.byte	0x88
 1266 081d 1C       		.uleb128 0x1c
 1267 081e 00000000 		.long	.LASF74
 1268 0822 0F       		.byte	0xf
 1269 0823 2101     		.value	0x121
 1270 0825 FF120000 		.long	0x12ff
 1271 0829 90       		.byte	0x90
 1272 082a 1C       		.uleb128 0x1c
 1273 082b 00000000 		.long	.LASF75
 1274 082f 0F       		.byte	0xf
 1275 0830 2901     		.value	0x129
 1276 0832 DC080000 		.long	0x8dc
GAS LISTING /tmp/cce2OND8.s 			page 28


 1277 0836 98       		.byte	0x98
 1278 0837 1C       		.uleb128 0x1c
 1279 0838 00000000 		.long	.LASF76
 1280 083c 0F       		.byte	0xf
 1281 083d 2A01     		.value	0x12a
 1282 083f DC080000 		.long	0x8dc
 1283 0843 A0       		.byte	0xa0
 1284 0844 1C       		.uleb128 0x1c
 1285 0845 00000000 		.long	.LASF77
 1286 0849 0F       		.byte	0xf
 1287 084a 2B01     		.value	0x12b
 1288 084c DC080000 		.long	0x8dc
 1289 0850 A8       		.byte	0xa8
 1290 0851 1C       		.uleb128 0x1c
 1291 0852 00000000 		.long	.LASF78
 1292 0856 0F       		.byte	0xf
 1293 0857 2C01     		.value	0x12c
 1294 0859 DC080000 		.long	0x8dc
 1295 085d B0       		.byte	0xb0
 1296 085e 1C       		.uleb128 0x1c
 1297 085f 00000000 		.long	.LASF79
 1298 0863 0F       		.byte	0xf
 1299 0864 2E01     		.value	0x12e
 1300 0866 DE080000 		.long	0x8de
 1301 086a B8       		.byte	0xb8
 1302 086b 1C       		.uleb128 0x1c
 1303 086c 00000000 		.long	.LASF80
 1304 0870 0F       		.byte	0xf
 1305 0871 2F01     		.value	0x12f
 1306 0873 57090000 		.long	0x957
 1307 0877 C0       		.byte	0xc0
 1308 0878 1C       		.uleb128 0x1c
 1309 0879 00000000 		.long	.LASF81
 1310 087d 0F       		.byte	0xf
 1311 087e 3101     		.value	0x131
 1312 0880 27140000 		.long	0x1427
 1313 0884 C4       		.byte	0xc4
 1314 0885 00       		.byte	0
 1315 0886 08       		.uleb128 0x8
 1316 0887 00000000 		.long	.LASF82
 1317 088b 0E       		.byte	0xe
 1318 088c 40       		.byte	0x40
 1319 088d 09070000 		.long	0x709
 1320 0891 1D       		.uleb128 0x1d
 1321 0892 08       		.byte	0x8
 1322 0893 07       		.byte	0x7
 1323 0894 00000000 		.long	.LASF88
 1324 0898 07       		.uleb128 0x7
 1325 0899 00000000 		.long	.LASF83
 1326 089d 18       		.byte	0x18
 1327 089e 10       		.byte	0x10
 1328 089f 00       		.byte	0
 1329 08a0 D5080000 		.long	0x8d5
 1330 08a4 1B       		.uleb128 0x1b
 1331 08a5 00000000 		.long	.LASF84
 1332 08a9 10       		.byte	0x10
 1333 08aa 00       		.byte	0
GAS LISTING /tmp/cce2OND8.s 			page 29


 1334 08ab D5080000 		.long	0x8d5
 1335 08af 00       		.byte	0
 1336 08b0 1B       		.uleb128 0x1b
 1337 08b1 00000000 		.long	.LASF85
 1338 08b5 10       		.byte	0x10
 1339 08b6 00       		.byte	0
 1340 08b7 D5080000 		.long	0x8d5
 1341 08bb 04       		.byte	0x4
 1342 08bc 1B       		.uleb128 0x1b
 1343 08bd 00000000 		.long	.LASF86
 1344 08c1 10       		.byte	0x10
 1345 08c2 00       		.byte	0
 1346 08c3 DC080000 		.long	0x8dc
 1347 08c7 08       		.byte	0x8
 1348 08c8 1B       		.uleb128 0x1b
 1349 08c9 00000000 		.long	.LASF87
 1350 08cd 10       		.byte	0x10
 1351 08ce 00       		.byte	0
 1352 08cf DC080000 		.long	0x8dc
 1353 08d3 10       		.byte	0x10
 1354 08d4 00       		.byte	0
 1355 08d5 1D       		.uleb128 0x1d
 1356 08d6 04       		.byte	0x4
 1357 08d7 07       		.byte	0x7
 1358 08d8 00000000 		.long	.LASF89
 1359 08dc 1E       		.uleb128 0x1e
 1360 08dd 08       		.byte	0x8
 1361 08de 08       		.uleb128 0x8
 1362 08df 00000000 		.long	.LASF25
 1363 08e3 11       		.byte	0x11
 1364 08e4 D8       		.byte	0xd8
 1365 08e5 E9080000 		.long	0x8e9
 1366 08e9 1D       		.uleb128 0x1d
 1367 08ea 08       		.byte	0x8
 1368 08eb 07       		.byte	0x7
 1369 08ec 00000000 		.long	.LASF90
 1370 08f0 1F       		.uleb128 0x1f
 1371 08f1 00000000 		.long	.LASF91
 1372 08f5 11       		.byte	0x11
 1373 08f6 6501     		.value	0x165
 1374 08f8 D5080000 		.long	0x8d5
 1375 08fc 20       		.uleb128 0x20
 1376 08fd 08       		.byte	0x8
 1377 08fe 12       		.byte	0x12
 1378 08ff 53       		.byte	0x53
 1379 0900 00000000 		.long	.LASF219
 1380 0904 40090000 		.long	0x940
 1381 0908 21       		.uleb128 0x21
 1382 0909 04       		.byte	0x4
 1383 090a 12       		.byte	0x12
 1384 090b 56       		.byte	0x56
 1385 090c 27090000 		.long	0x927
 1386 0910 22       		.uleb128 0x22
 1387 0911 00000000 		.long	.LASF92
 1388 0915 12       		.byte	0x12
 1389 0916 58       		.byte	0x58
 1390 0917 D5080000 		.long	0x8d5
GAS LISTING /tmp/cce2OND8.s 			page 30


 1391 091b 22       		.uleb128 0x22
 1392 091c 00000000 		.long	.LASF93
 1393 0920 12       		.byte	0x12
 1394 0921 5C       		.byte	0x5c
 1395 0922 40090000 		.long	0x940
 1396 0926 00       		.byte	0
 1397 0927 1B       		.uleb128 0x1b
 1398 0928 00000000 		.long	.LASF94
 1399 092c 12       		.byte	0x12
 1400 092d 54       		.byte	0x54
 1401 092e 57090000 		.long	0x957
 1402 0932 00       		.byte	0
 1403 0933 1B       		.uleb128 0x1b
 1404 0934 00000000 		.long	.LASF95
 1405 0938 12       		.byte	0x12
 1406 0939 5D       		.byte	0x5d
 1407 093a 08090000 		.long	0x908
 1408 093e 04       		.byte	0x4
 1409 093f 00       		.byte	0
 1410 0940 23       		.uleb128 0x23
 1411 0941 50090000 		.long	0x950
 1412 0945 50090000 		.long	0x950
 1413 0949 24       		.uleb128 0x24
 1414 094a 91080000 		.long	0x891
 1415 094e 03       		.byte	0x3
 1416 094f 00       		.byte	0
 1417 0950 1D       		.uleb128 0x1d
 1418 0951 01       		.byte	0x1
 1419 0952 06       		.byte	0x6
 1420 0953 00000000 		.long	.LASF96
 1421 0957 25       		.uleb128 0x25
 1422 0958 04       		.byte	0x4
 1423 0959 05       		.byte	0x5
 1424 095a 696E7400 		.string	"int"
 1425 095e 08       		.uleb128 0x8
 1426 095f 00000000 		.long	.LASF97
 1427 0963 12       		.byte	0x12
 1428 0964 5E       		.byte	0x5e
 1429 0965 FC080000 		.long	0x8fc
 1430 0969 08       		.uleb128 0x8
 1431 096a 00000000 		.long	.LASF98
 1432 096e 12       		.byte	0x12
 1433 096f 6A       		.byte	0x6a
 1434 0970 5E090000 		.long	0x95e
 1435 0974 1D       		.uleb128 0x1d
 1436 0975 02       		.byte	0x2
 1437 0976 07       		.byte	0x7
 1438 0977 00000000 		.long	.LASF99
 1439 097b 0B       		.uleb128 0xb
 1440 097c 57090000 		.long	0x957
 1441 0980 26       		.uleb128 0x26
 1442 0981 08       		.byte	0x8
 1443 0982 86090000 		.long	0x986
 1444 0986 0B       		.uleb128 0xb
 1445 0987 50090000 		.long	0x950
 1446 098b 27       		.uleb128 0x27
 1447 098c 00000000 		.long	.LASF100
GAS LISTING /tmp/cce2OND8.s 			page 31


 1448 0990 12       		.byte	0x12
 1449 0991 6401     		.value	0x164
 1450 0993 F0080000 		.long	0x8f0
 1451 0997 A1090000 		.long	0x9a1
 1452 099b 0A       		.uleb128 0xa
 1453 099c 57090000 		.long	0x957
 1454 09a0 00       		.byte	0
 1455 09a1 27       		.uleb128 0x27
 1456 09a2 00000000 		.long	.LASF101
 1457 09a6 12       		.byte	0x12
 1458 09a7 EC02     		.value	0x2ec
 1459 09a9 F0080000 		.long	0x8f0
 1460 09ad B7090000 		.long	0x9b7
 1461 09b1 0A       		.uleb128 0xa
 1462 09b2 B7090000 		.long	0x9b7
 1463 09b6 00       		.byte	0
 1464 09b7 26       		.uleb128 0x26
 1465 09b8 08       		.byte	0x8
 1466 09b9 86080000 		.long	0x886
 1467 09bd 27       		.uleb128 0x27
 1468 09be 00000000 		.long	.LASF102
 1469 09c2 12       		.byte	0x12
 1470 09c3 0903     		.value	0x309
 1471 09c5 DD090000 		.long	0x9dd
 1472 09c9 DD090000 		.long	0x9dd
 1473 09cd 0A       		.uleb128 0xa
 1474 09ce DD090000 		.long	0x9dd
 1475 09d2 0A       		.uleb128 0xa
 1476 09d3 57090000 		.long	0x957
 1477 09d7 0A       		.uleb128 0xa
 1478 09d8 B7090000 		.long	0x9b7
 1479 09dc 00       		.byte	0
 1480 09dd 26       		.uleb128 0x26
 1481 09de 08       		.byte	0x8
 1482 09df E3090000 		.long	0x9e3
 1483 09e3 1D       		.uleb128 0x1d
 1484 09e4 04       		.byte	0x4
 1485 09e5 05       		.byte	0x5
 1486 09e6 00000000 		.long	.LASF103
 1487 09ea 27       		.uleb128 0x27
 1488 09eb 00000000 		.long	.LASF104
 1489 09ef 12       		.byte	0x12
 1490 09f0 FA02     		.value	0x2fa
 1491 09f2 F0080000 		.long	0x8f0
 1492 09f6 050A0000 		.long	0xa05
 1493 09fa 0A       		.uleb128 0xa
 1494 09fb E3090000 		.long	0x9e3
 1495 09ff 0A       		.uleb128 0xa
 1496 0a00 B7090000 		.long	0x9b7
 1497 0a04 00       		.byte	0
 1498 0a05 27       		.uleb128 0x27
 1499 0a06 00000000 		.long	.LASF105
 1500 0a0a 12       		.byte	0x12
 1501 0a0b 1003     		.value	0x310
 1502 0a0d 57090000 		.long	0x957
 1503 0a11 200A0000 		.long	0xa20
 1504 0a15 0A       		.uleb128 0xa
GAS LISTING /tmp/cce2OND8.s 			page 32


 1505 0a16 200A0000 		.long	0xa20
 1506 0a1a 0A       		.uleb128 0xa
 1507 0a1b B7090000 		.long	0x9b7
 1508 0a1f 00       		.byte	0
 1509 0a20 26       		.uleb128 0x26
 1510 0a21 08       		.byte	0x8
 1511 0a22 260A0000 		.long	0xa26
 1512 0a26 0B       		.uleb128 0xb
 1513 0a27 E3090000 		.long	0x9e3
 1514 0a2b 27       		.uleb128 0x27
 1515 0a2c 00000000 		.long	.LASF106
 1516 0a30 12       		.byte	0x12
 1517 0a31 4E02     		.value	0x24e
 1518 0a33 57090000 		.long	0x957
 1519 0a37 460A0000 		.long	0xa46
 1520 0a3b 0A       		.uleb128 0xa
 1521 0a3c B7090000 		.long	0x9b7
 1522 0a40 0A       		.uleb128 0xa
 1523 0a41 57090000 		.long	0x957
 1524 0a45 00       		.byte	0
 1525 0a46 27       		.uleb128 0x27
 1526 0a47 00000000 		.long	.LASF107
 1527 0a4b 12       		.byte	0x12
 1528 0a4c 5502     		.value	0x255
 1529 0a4e 57090000 		.long	0x957
 1530 0a52 620A0000 		.long	0xa62
 1531 0a56 0A       		.uleb128 0xa
 1532 0a57 B7090000 		.long	0x9b7
 1533 0a5b 0A       		.uleb128 0xa
 1534 0a5c 200A0000 		.long	0xa20
 1535 0a60 28       		.uleb128 0x28
 1536 0a61 00       		.byte	0
 1537 0a62 27       		.uleb128 0x27
 1538 0a63 00000000 		.long	.LASF108
 1539 0a67 12       		.byte	0x12
 1540 0a68 7E02     		.value	0x27e
 1541 0a6a 57090000 		.long	0x957
 1542 0a6e 7E0A0000 		.long	0xa7e
 1543 0a72 0A       		.uleb128 0xa
 1544 0a73 B7090000 		.long	0x9b7
 1545 0a77 0A       		.uleb128 0xa
 1546 0a78 200A0000 		.long	0xa20
 1547 0a7c 28       		.uleb128 0x28
 1548 0a7d 00       		.byte	0
 1549 0a7e 27       		.uleb128 0x27
 1550 0a7f 00000000 		.long	.LASF109
 1551 0a83 12       		.byte	0x12
 1552 0a84 ED02     		.value	0x2ed
 1553 0a86 F0080000 		.long	0x8f0
 1554 0a8a 940A0000 		.long	0xa94
 1555 0a8e 0A       		.uleb128 0xa
 1556 0a8f B7090000 		.long	0x9b7
 1557 0a93 00       		.byte	0
 1558 0a94 29       		.uleb128 0x29
 1559 0a95 00000000 		.long	.LASF208
 1560 0a99 12       		.byte	0x12
 1561 0a9a F302     		.value	0x2f3
GAS LISTING /tmp/cce2OND8.s 			page 33


 1562 0a9c F0080000 		.long	0x8f0
 1563 0aa0 27       		.uleb128 0x27
 1564 0aa1 00000000 		.long	.LASF110
 1565 0aa5 12       		.byte	0x12
 1566 0aa6 7B01     		.value	0x17b
 1567 0aa8 DE080000 		.long	0x8de
 1568 0aac C00A0000 		.long	0xac0
 1569 0ab0 0A       		.uleb128 0xa
 1570 0ab1 80090000 		.long	0x980
 1571 0ab5 0A       		.uleb128 0xa
 1572 0ab6 DE080000 		.long	0x8de
 1573 0aba 0A       		.uleb128 0xa
 1574 0abb C00A0000 		.long	0xac0
 1575 0abf 00       		.byte	0
 1576 0ac0 26       		.uleb128 0x26
 1577 0ac1 08       		.byte	0x8
 1578 0ac2 69090000 		.long	0x969
 1579 0ac6 27       		.uleb128 0x27
 1580 0ac7 00000000 		.long	.LASF111
 1581 0acb 12       		.byte	0x12
 1582 0acc 7001     		.value	0x170
 1583 0ace DE080000 		.long	0x8de
 1584 0ad2 EB0A0000 		.long	0xaeb
 1585 0ad6 0A       		.uleb128 0xa
 1586 0ad7 DD090000 		.long	0x9dd
 1587 0adb 0A       		.uleb128 0xa
 1588 0adc 80090000 		.long	0x980
 1589 0ae0 0A       		.uleb128 0xa
 1590 0ae1 DE080000 		.long	0x8de
 1591 0ae5 0A       		.uleb128 0xa
 1592 0ae6 C00A0000 		.long	0xac0
 1593 0aea 00       		.byte	0
 1594 0aeb 27       		.uleb128 0x27
 1595 0aec 00000000 		.long	.LASF112
 1596 0af0 12       		.byte	0x12
 1597 0af1 6C01     		.value	0x16c
 1598 0af3 57090000 		.long	0x957
 1599 0af7 010B0000 		.long	0xb01
 1600 0afb 0A       		.uleb128 0xa
 1601 0afc 010B0000 		.long	0xb01
 1602 0b00 00       		.byte	0
 1603 0b01 26       		.uleb128 0x26
 1604 0b02 08       		.byte	0x8
 1605 0b03 070B0000 		.long	0xb07
 1606 0b07 0B       		.uleb128 0xb
 1607 0b08 69090000 		.long	0x969
 1608 0b0c 27       		.uleb128 0x27
 1609 0b0d 00000000 		.long	.LASF113
 1610 0b11 12       		.byte	0x12
 1611 0b12 9B01     		.value	0x19b
 1612 0b14 DE080000 		.long	0x8de
 1613 0b18 310B0000 		.long	0xb31
 1614 0b1c 0A       		.uleb128 0xa
 1615 0b1d DD090000 		.long	0x9dd
 1616 0b21 0A       		.uleb128 0xa
 1617 0b22 310B0000 		.long	0xb31
 1618 0b26 0A       		.uleb128 0xa
GAS LISTING /tmp/cce2OND8.s 			page 34


 1619 0b27 DE080000 		.long	0x8de
 1620 0b2b 0A       		.uleb128 0xa
 1621 0b2c C00A0000 		.long	0xac0
 1622 0b30 00       		.byte	0
 1623 0b31 26       		.uleb128 0x26
 1624 0b32 08       		.byte	0x8
 1625 0b33 80090000 		.long	0x980
 1626 0b37 27       		.uleb128 0x27
 1627 0b38 00000000 		.long	.LASF114
 1628 0b3c 12       		.byte	0x12
 1629 0b3d FB02     		.value	0x2fb
 1630 0b3f F0080000 		.long	0x8f0
 1631 0b43 520B0000 		.long	0xb52
 1632 0b47 0A       		.uleb128 0xa
 1633 0b48 E3090000 		.long	0x9e3
 1634 0b4c 0A       		.uleb128 0xa
 1635 0b4d B7090000 		.long	0x9b7
 1636 0b51 00       		.byte	0
 1637 0b52 27       		.uleb128 0x27
 1638 0b53 00000000 		.long	.LASF115
 1639 0b57 12       		.byte	0x12
 1640 0b58 0103     		.value	0x301
 1641 0b5a F0080000 		.long	0x8f0
 1642 0b5e 680B0000 		.long	0xb68
 1643 0b62 0A       		.uleb128 0xa
 1644 0b63 E3090000 		.long	0x9e3
 1645 0b67 00       		.byte	0
 1646 0b68 27       		.uleb128 0x27
 1647 0b69 00000000 		.long	.LASF116
 1648 0b6d 12       		.byte	0x12
 1649 0b6e 5F02     		.value	0x25f
 1650 0b70 57090000 		.long	0x957
 1651 0b74 890B0000 		.long	0xb89
 1652 0b78 0A       		.uleb128 0xa
 1653 0b79 DD090000 		.long	0x9dd
 1654 0b7d 0A       		.uleb128 0xa
 1655 0b7e DE080000 		.long	0x8de
 1656 0b82 0A       		.uleb128 0xa
 1657 0b83 200A0000 		.long	0xa20
 1658 0b87 28       		.uleb128 0x28
 1659 0b88 00       		.byte	0
 1660 0b89 27       		.uleb128 0x27
 1661 0b8a 00000000 		.long	.LASF117
 1662 0b8e 12       		.byte	0x12
 1663 0b8f 8802     		.value	0x288
 1664 0b91 57090000 		.long	0x957
 1665 0b95 A50B0000 		.long	0xba5
 1666 0b99 0A       		.uleb128 0xa
 1667 0b9a 200A0000 		.long	0xa20
 1668 0b9e 0A       		.uleb128 0xa
 1669 0b9f 200A0000 		.long	0xa20
 1670 0ba3 28       		.uleb128 0x28
 1671 0ba4 00       		.byte	0
 1672 0ba5 27       		.uleb128 0x27
 1673 0ba6 00000000 		.long	.LASF118
 1674 0baa 12       		.byte	0x12
 1675 0bab 1803     		.value	0x318
GAS LISTING /tmp/cce2OND8.s 			page 35


 1676 0bad F0080000 		.long	0x8f0
 1677 0bb1 C00B0000 		.long	0xbc0
 1678 0bb5 0A       		.uleb128 0xa
 1679 0bb6 F0080000 		.long	0x8f0
 1680 0bba 0A       		.uleb128 0xa
 1681 0bbb B7090000 		.long	0x9b7
 1682 0bbf 00       		.byte	0
 1683 0bc0 27       		.uleb128 0x27
 1684 0bc1 00000000 		.long	.LASF119
 1685 0bc5 12       		.byte	0x12
 1686 0bc6 6702     		.value	0x267
 1687 0bc8 57090000 		.long	0x957
 1688 0bcc E00B0000 		.long	0xbe0
 1689 0bd0 0A       		.uleb128 0xa
 1690 0bd1 B7090000 		.long	0x9b7
 1691 0bd5 0A       		.uleb128 0xa
 1692 0bd6 200A0000 		.long	0xa20
 1693 0bda 0A       		.uleb128 0xa
 1694 0bdb E00B0000 		.long	0xbe0
 1695 0bdf 00       		.byte	0
 1696 0be0 26       		.uleb128 0x26
 1697 0be1 08       		.byte	0x8
 1698 0be2 98080000 		.long	0x898
 1699 0be6 27       		.uleb128 0x27
 1700 0be7 00000000 		.long	.LASF120
 1701 0beb 12       		.byte	0x12
 1702 0bec B402     		.value	0x2b4
 1703 0bee 57090000 		.long	0x957
 1704 0bf2 060C0000 		.long	0xc06
 1705 0bf6 0A       		.uleb128 0xa
 1706 0bf7 B7090000 		.long	0x9b7
 1707 0bfb 0A       		.uleb128 0xa
 1708 0bfc 200A0000 		.long	0xa20
 1709 0c00 0A       		.uleb128 0xa
 1710 0c01 E00B0000 		.long	0xbe0
 1711 0c05 00       		.byte	0
 1712 0c06 27       		.uleb128 0x27
 1713 0c07 00000000 		.long	.LASF121
 1714 0c0b 12       		.byte	0x12
 1715 0c0c 7402     		.value	0x274
 1716 0c0e 57090000 		.long	0x957
 1717 0c12 2B0C0000 		.long	0xc2b
 1718 0c16 0A       		.uleb128 0xa
 1719 0c17 DD090000 		.long	0x9dd
 1720 0c1b 0A       		.uleb128 0xa
 1721 0c1c DE080000 		.long	0x8de
 1722 0c20 0A       		.uleb128 0xa
 1723 0c21 200A0000 		.long	0xa20
 1724 0c25 0A       		.uleb128 0xa
 1725 0c26 E00B0000 		.long	0xbe0
 1726 0c2a 00       		.byte	0
 1727 0c2b 27       		.uleb128 0x27
 1728 0c2c 00000000 		.long	.LASF122
 1729 0c30 12       		.byte	0x12
 1730 0c31 C002     		.value	0x2c0
 1731 0c33 57090000 		.long	0x957
 1732 0c37 4B0C0000 		.long	0xc4b
GAS LISTING /tmp/cce2OND8.s 			page 36


 1733 0c3b 0A       		.uleb128 0xa
 1734 0c3c 200A0000 		.long	0xa20
 1735 0c40 0A       		.uleb128 0xa
 1736 0c41 200A0000 		.long	0xa20
 1737 0c45 0A       		.uleb128 0xa
 1738 0c46 E00B0000 		.long	0xbe0
 1739 0c4a 00       		.byte	0
 1740 0c4b 27       		.uleb128 0x27
 1741 0c4c 00000000 		.long	.LASF123
 1742 0c50 12       		.byte	0x12
 1743 0c51 6F02     		.value	0x26f
 1744 0c53 57090000 		.long	0x957
 1745 0c57 660C0000 		.long	0xc66
 1746 0c5b 0A       		.uleb128 0xa
 1747 0c5c 200A0000 		.long	0xa20
 1748 0c60 0A       		.uleb128 0xa
 1749 0c61 E00B0000 		.long	0xbe0
 1750 0c65 00       		.byte	0
 1751 0c66 27       		.uleb128 0x27
 1752 0c67 00000000 		.long	.LASF124
 1753 0c6b 12       		.byte	0x12
 1754 0c6c BC02     		.value	0x2bc
 1755 0c6e 57090000 		.long	0x957
 1756 0c72 810C0000 		.long	0xc81
 1757 0c76 0A       		.uleb128 0xa
 1758 0c77 200A0000 		.long	0xa20
 1759 0c7b 0A       		.uleb128 0xa
 1760 0c7c E00B0000 		.long	0xbe0
 1761 0c80 00       		.byte	0
 1762 0c81 27       		.uleb128 0x27
 1763 0c82 00000000 		.long	.LASF125
 1764 0c86 12       		.byte	0x12
 1765 0c87 7501     		.value	0x175
 1766 0c89 DE080000 		.long	0x8de
 1767 0c8d A10C0000 		.long	0xca1
 1768 0c91 0A       		.uleb128 0xa
 1769 0c92 A10C0000 		.long	0xca1
 1770 0c96 0A       		.uleb128 0xa
 1771 0c97 E3090000 		.long	0x9e3
 1772 0c9b 0A       		.uleb128 0xa
 1773 0c9c C00A0000 		.long	0xac0
 1774 0ca0 00       		.byte	0
 1775 0ca1 26       		.uleb128 0x26
 1776 0ca2 08       		.byte	0x8
 1777 0ca3 50090000 		.long	0x950
 1778 0ca7 2A       		.uleb128 0x2a
 1779 0ca8 00000000 		.long	.LASF126
 1780 0cac 12       		.byte	0x12
 1781 0cad 9D       		.byte	0x9d
 1782 0cae DD090000 		.long	0x9dd
 1783 0cb2 C10C0000 		.long	0xcc1
 1784 0cb6 0A       		.uleb128 0xa
 1785 0cb7 DD090000 		.long	0x9dd
 1786 0cbb 0A       		.uleb128 0xa
 1787 0cbc 200A0000 		.long	0xa20
 1788 0cc0 00       		.byte	0
 1789 0cc1 2A       		.uleb128 0x2a
GAS LISTING /tmp/cce2OND8.s 			page 37


 1790 0cc2 00000000 		.long	.LASF127
 1791 0cc6 12       		.byte	0x12
 1792 0cc7 A6       		.byte	0xa6
 1793 0cc8 57090000 		.long	0x957
 1794 0ccc DB0C0000 		.long	0xcdb
 1795 0cd0 0A       		.uleb128 0xa
 1796 0cd1 200A0000 		.long	0xa20
 1797 0cd5 0A       		.uleb128 0xa
 1798 0cd6 200A0000 		.long	0xa20
 1799 0cda 00       		.byte	0
 1800 0cdb 2A       		.uleb128 0x2a
 1801 0cdc 00000000 		.long	.LASF128
 1802 0ce0 12       		.byte	0x12
 1803 0ce1 C3       		.byte	0xc3
 1804 0ce2 57090000 		.long	0x957
 1805 0ce6 F50C0000 		.long	0xcf5
 1806 0cea 0A       		.uleb128 0xa
 1807 0ceb 200A0000 		.long	0xa20
 1808 0cef 0A       		.uleb128 0xa
 1809 0cf0 200A0000 		.long	0xa20
 1810 0cf4 00       		.byte	0
 1811 0cf5 2A       		.uleb128 0x2a
 1812 0cf6 00000000 		.long	.LASF129
 1813 0cfa 12       		.byte	0x12
 1814 0cfb 93       		.byte	0x93
 1815 0cfc DD090000 		.long	0x9dd
 1816 0d00 0F0D0000 		.long	0xd0f
 1817 0d04 0A       		.uleb128 0xa
 1818 0d05 DD090000 		.long	0x9dd
 1819 0d09 0A       		.uleb128 0xa
 1820 0d0a 200A0000 		.long	0xa20
 1821 0d0e 00       		.byte	0
 1822 0d0f 2A       		.uleb128 0x2a
 1823 0d10 00000000 		.long	.LASF130
 1824 0d14 12       		.byte	0x12
 1825 0d15 FF       		.byte	0xff
 1826 0d16 DE080000 		.long	0x8de
 1827 0d1a 290D0000 		.long	0xd29
 1828 0d1e 0A       		.uleb128 0xa
 1829 0d1f 200A0000 		.long	0xa20
 1830 0d23 0A       		.uleb128 0xa
 1831 0d24 200A0000 		.long	0xa20
 1832 0d28 00       		.byte	0
 1833 0d29 27       		.uleb128 0x27
 1834 0d2a 00000000 		.long	.LASF131
 1835 0d2e 12       		.byte	0x12
 1836 0d2f 5A03     		.value	0x35a
 1837 0d31 DE080000 		.long	0x8de
 1838 0d35 4E0D0000 		.long	0xd4e
 1839 0d39 0A       		.uleb128 0xa
 1840 0d3a DD090000 		.long	0x9dd
 1841 0d3e 0A       		.uleb128 0xa
 1842 0d3f DE080000 		.long	0x8de
 1843 0d43 0A       		.uleb128 0xa
 1844 0d44 200A0000 		.long	0xa20
 1845 0d48 0A       		.uleb128 0xa
 1846 0d49 4E0D0000 		.long	0xd4e
GAS LISTING /tmp/cce2OND8.s 			page 38


 1847 0d4d 00       		.byte	0
 1848 0d4e 26       		.uleb128 0x26
 1849 0d4f 08       		.byte	0x8
 1850 0d50 E40D0000 		.long	0xde4
 1851 0d54 2B       		.uleb128 0x2b
 1852 0d55 746D00   		.string	"tm"
 1853 0d58 38       		.byte	0x38
 1854 0d59 13       		.byte	0x13
 1855 0d5a 85       		.byte	0x85
 1856 0d5b E40D0000 		.long	0xde4
 1857 0d5f 1B       		.uleb128 0x1b
 1858 0d60 00000000 		.long	.LASF132
 1859 0d64 13       		.byte	0x13
 1860 0d65 87       		.byte	0x87
 1861 0d66 57090000 		.long	0x957
 1862 0d6a 00       		.byte	0
 1863 0d6b 1B       		.uleb128 0x1b
 1864 0d6c 00000000 		.long	.LASF133
 1865 0d70 13       		.byte	0x13
 1866 0d71 88       		.byte	0x88
 1867 0d72 57090000 		.long	0x957
 1868 0d76 04       		.byte	0x4
 1869 0d77 1B       		.uleb128 0x1b
 1870 0d78 00000000 		.long	.LASF134
 1871 0d7c 13       		.byte	0x13
 1872 0d7d 89       		.byte	0x89
 1873 0d7e 57090000 		.long	0x957
 1874 0d82 08       		.byte	0x8
 1875 0d83 1B       		.uleb128 0x1b
 1876 0d84 00000000 		.long	.LASF135
 1877 0d88 13       		.byte	0x13
 1878 0d89 8A       		.byte	0x8a
 1879 0d8a 57090000 		.long	0x957
 1880 0d8e 0C       		.byte	0xc
 1881 0d8f 1B       		.uleb128 0x1b
 1882 0d90 00000000 		.long	.LASF136
 1883 0d94 13       		.byte	0x13
 1884 0d95 8B       		.byte	0x8b
 1885 0d96 57090000 		.long	0x957
 1886 0d9a 10       		.byte	0x10
 1887 0d9b 1B       		.uleb128 0x1b
 1888 0d9c 00000000 		.long	.LASF137
 1889 0da0 13       		.byte	0x13
 1890 0da1 8C       		.byte	0x8c
 1891 0da2 57090000 		.long	0x957
 1892 0da6 14       		.byte	0x14
 1893 0da7 1B       		.uleb128 0x1b
 1894 0da8 00000000 		.long	.LASF138
 1895 0dac 13       		.byte	0x13
 1896 0dad 8D       		.byte	0x8d
 1897 0dae 57090000 		.long	0x957
 1898 0db2 18       		.byte	0x18
 1899 0db3 1B       		.uleb128 0x1b
 1900 0db4 00000000 		.long	.LASF139
 1901 0db8 13       		.byte	0x13
 1902 0db9 8E       		.byte	0x8e
 1903 0dba 57090000 		.long	0x957
GAS LISTING /tmp/cce2OND8.s 			page 39


 1904 0dbe 1C       		.byte	0x1c
 1905 0dbf 1B       		.uleb128 0x1b
 1906 0dc0 00000000 		.long	.LASF140
 1907 0dc4 13       		.byte	0x13
 1908 0dc5 8F       		.byte	0x8f
 1909 0dc6 57090000 		.long	0x957
 1910 0dca 20       		.byte	0x20
 1911 0dcb 1B       		.uleb128 0x1b
 1912 0dcc 00000000 		.long	.LASF141
 1913 0dd0 13       		.byte	0x13
 1914 0dd1 92       		.byte	0x92
 1915 0dd2 2C0F0000 		.long	0xf2c
 1916 0dd6 28       		.byte	0x28
 1917 0dd7 1B       		.uleb128 0x1b
 1918 0dd8 00000000 		.long	.LASF142
 1919 0ddc 13       		.byte	0x13
 1920 0ddd 93       		.byte	0x93
 1921 0dde 80090000 		.long	0x980
 1922 0de2 30       		.byte	0x30
 1923 0de3 00       		.byte	0
 1924 0de4 0B       		.uleb128 0xb
 1925 0de5 540D0000 		.long	0xd54
 1926 0de9 27       		.uleb128 0x27
 1927 0dea 00000000 		.long	.LASF143
 1928 0dee 12       		.byte	0x12
 1929 0def 2201     		.value	0x122
 1930 0df1 DE080000 		.long	0x8de
 1931 0df5 FF0D0000 		.long	0xdff
 1932 0df9 0A       		.uleb128 0xa
 1933 0dfa 200A0000 		.long	0xa20
 1934 0dfe 00       		.byte	0
 1935 0dff 2A       		.uleb128 0x2a
 1936 0e00 00000000 		.long	.LASF144
 1937 0e04 12       		.byte	0x12
 1938 0e05 A1       		.byte	0xa1
 1939 0e06 DD090000 		.long	0x9dd
 1940 0e0a 1E0E0000 		.long	0xe1e
 1941 0e0e 0A       		.uleb128 0xa
 1942 0e0f DD090000 		.long	0x9dd
 1943 0e13 0A       		.uleb128 0xa
 1944 0e14 200A0000 		.long	0xa20
 1945 0e18 0A       		.uleb128 0xa
 1946 0e19 DE080000 		.long	0x8de
 1947 0e1d 00       		.byte	0
 1948 0e1e 2A       		.uleb128 0x2a
 1949 0e1f 00000000 		.long	.LASF145
 1950 0e23 12       		.byte	0x12
 1951 0e24 A9       		.byte	0xa9
 1952 0e25 57090000 		.long	0x957
 1953 0e29 3D0E0000 		.long	0xe3d
 1954 0e2d 0A       		.uleb128 0xa
 1955 0e2e 200A0000 		.long	0xa20
 1956 0e32 0A       		.uleb128 0xa
 1957 0e33 200A0000 		.long	0xa20
 1958 0e37 0A       		.uleb128 0xa
 1959 0e38 DE080000 		.long	0x8de
 1960 0e3c 00       		.byte	0
GAS LISTING /tmp/cce2OND8.s 			page 40


 1961 0e3d 2A       		.uleb128 0x2a
 1962 0e3e 00000000 		.long	.LASF146
 1963 0e42 12       		.byte	0x12
 1964 0e43 98       		.byte	0x98
 1965 0e44 DD090000 		.long	0x9dd
 1966 0e48 5C0E0000 		.long	0xe5c
 1967 0e4c 0A       		.uleb128 0xa
 1968 0e4d DD090000 		.long	0x9dd
 1969 0e51 0A       		.uleb128 0xa
 1970 0e52 200A0000 		.long	0xa20
 1971 0e56 0A       		.uleb128 0xa
 1972 0e57 DE080000 		.long	0x8de
 1973 0e5b 00       		.byte	0
 1974 0e5c 27       		.uleb128 0x27
 1975 0e5d 00000000 		.long	.LASF147
 1976 0e61 12       		.byte	0x12
 1977 0e62 A101     		.value	0x1a1
 1978 0e64 DE080000 		.long	0x8de
 1979 0e68 810E0000 		.long	0xe81
 1980 0e6c 0A       		.uleb128 0xa
 1981 0e6d A10C0000 		.long	0xca1
 1982 0e71 0A       		.uleb128 0xa
 1983 0e72 810E0000 		.long	0xe81
 1984 0e76 0A       		.uleb128 0xa
 1985 0e77 DE080000 		.long	0x8de
 1986 0e7b 0A       		.uleb128 0xa
 1987 0e7c C00A0000 		.long	0xac0
 1988 0e80 00       		.byte	0
 1989 0e81 26       		.uleb128 0x26
 1990 0e82 08       		.byte	0x8
 1991 0e83 200A0000 		.long	0xa20
 1992 0e87 27       		.uleb128 0x27
 1993 0e88 00000000 		.long	.LASF148
 1994 0e8c 12       		.byte	0x12
 1995 0e8d 0301     		.value	0x103
 1996 0e8f DE080000 		.long	0x8de
 1997 0e93 A20E0000 		.long	0xea2
 1998 0e97 0A       		.uleb128 0xa
 1999 0e98 200A0000 		.long	0xa20
 2000 0e9c 0A       		.uleb128 0xa
 2001 0e9d 200A0000 		.long	0xa20
 2002 0ea1 00       		.byte	0
 2003 0ea2 27       		.uleb128 0x27
 2004 0ea3 00000000 		.long	.LASF149
 2005 0ea7 12       		.byte	0x12
 2006 0ea8 C501     		.value	0x1c5
 2007 0eaa BD0E0000 		.long	0xebd
 2008 0eae BD0E0000 		.long	0xebd
 2009 0eb2 0A       		.uleb128 0xa
 2010 0eb3 200A0000 		.long	0xa20
 2011 0eb7 0A       		.uleb128 0xa
 2012 0eb8 C40E0000 		.long	0xec4
 2013 0ebc 00       		.byte	0
 2014 0ebd 1D       		.uleb128 0x1d
 2015 0ebe 08       		.byte	0x8
 2016 0ebf 04       		.byte	0x4
 2017 0ec0 00000000 		.long	.LASF150
GAS LISTING /tmp/cce2OND8.s 			page 41


 2018 0ec4 26       		.uleb128 0x26
 2019 0ec5 08       		.byte	0x8
 2020 0ec6 DD090000 		.long	0x9dd
 2021 0eca 27       		.uleb128 0x27
 2022 0ecb 00000000 		.long	.LASF151
 2023 0ecf 12       		.byte	0x12
 2024 0ed0 CC01     		.value	0x1cc
 2025 0ed2 E50E0000 		.long	0xee5
 2026 0ed6 E50E0000 		.long	0xee5
 2027 0eda 0A       		.uleb128 0xa
 2028 0edb 200A0000 		.long	0xa20
 2029 0edf 0A       		.uleb128 0xa
 2030 0ee0 C40E0000 		.long	0xec4
 2031 0ee4 00       		.byte	0
 2032 0ee5 1D       		.uleb128 0x1d
 2033 0ee6 04       		.byte	0x4
 2034 0ee7 04       		.byte	0x4
 2035 0ee8 00000000 		.long	.LASF152
 2036 0eec 27       		.uleb128 0x27
 2037 0eed 00000000 		.long	.LASF153
 2038 0ef1 12       		.byte	0x12
 2039 0ef2 1D01     		.value	0x11d
 2040 0ef4 DD090000 		.long	0x9dd
 2041 0ef8 0C0F0000 		.long	0xf0c
 2042 0efc 0A       		.uleb128 0xa
 2043 0efd DD090000 		.long	0x9dd
 2044 0f01 0A       		.uleb128 0xa
 2045 0f02 200A0000 		.long	0xa20
 2046 0f06 0A       		.uleb128 0xa
 2047 0f07 C40E0000 		.long	0xec4
 2048 0f0b 00       		.byte	0
 2049 0f0c 27       		.uleb128 0x27
 2050 0f0d 00000000 		.long	.LASF154
 2051 0f11 12       		.byte	0x12
 2052 0f12 D701     		.value	0x1d7
 2053 0f14 2C0F0000 		.long	0xf2c
 2054 0f18 2C0F0000 		.long	0xf2c
 2055 0f1c 0A       		.uleb128 0xa
 2056 0f1d 200A0000 		.long	0xa20
 2057 0f21 0A       		.uleb128 0xa
 2058 0f22 C40E0000 		.long	0xec4
 2059 0f26 0A       		.uleb128 0xa
 2060 0f27 57090000 		.long	0x957
 2061 0f2b 00       		.byte	0
 2062 0f2c 1D       		.uleb128 0x1d
 2063 0f2d 08       		.byte	0x8
 2064 0f2e 05       		.byte	0x5
 2065 0f2f 00000000 		.long	.LASF155
 2066 0f33 27       		.uleb128 0x27
 2067 0f34 00000000 		.long	.LASF156
 2068 0f38 12       		.byte	0x12
 2069 0f39 DC01     		.value	0x1dc
 2070 0f3b E9080000 		.long	0x8e9
 2071 0f3f 530F0000 		.long	0xf53
 2072 0f43 0A       		.uleb128 0xa
 2073 0f44 200A0000 		.long	0xa20
 2074 0f48 0A       		.uleb128 0xa
GAS LISTING /tmp/cce2OND8.s 			page 42


 2075 0f49 C40E0000 		.long	0xec4
 2076 0f4d 0A       		.uleb128 0xa
 2077 0f4e 57090000 		.long	0x957
 2078 0f52 00       		.byte	0
 2079 0f53 2A       		.uleb128 0x2a
 2080 0f54 00000000 		.long	.LASF157
 2081 0f58 12       		.byte	0x12
 2082 0f59 C7       		.byte	0xc7
 2083 0f5a DE080000 		.long	0x8de
 2084 0f5e 720F0000 		.long	0xf72
 2085 0f62 0A       		.uleb128 0xa
 2086 0f63 DD090000 		.long	0x9dd
 2087 0f67 0A       		.uleb128 0xa
 2088 0f68 200A0000 		.long	0xa20
 2089 0f6c 0A       		.uleb128 0xa
 2090 0f6d DE080000 		.long	0x8de
 2091 0f71 00       		.byte	0
 2092 0f72 27       		.uleb128 0x27
 2093 0f73 00000000 		.long	.LASF158
 2094 0f77 12       		.byte	0x12
 2095 0f78 6801     		.value	0x168
 2096 0f7a 57090000 		.long	0x957
 2097 0f7e 880F0000 		.long	0xf88
 2098 0f82 0A       		.uleb128 0xa
 2099 0f83 F0080000 		.long	0x8f0
 2100 0f87 00       		.byte	0
 2101 0f88 27       		.uleb128 0x27
 2102 0f89 00000000 		.long	.LASF159
 2103 0f8d 12       		.byte	0x12
 2104 0f8e 4801     		.value	0x148
 2105 0f90 57090000 		.long	0x957
 2106 0f94 A80F0000 		.long	0xfa8
 2107 0f98 0A       		.uleb128 0xa
 2108 0f99 200A0000 		.long	0xa20
 2109 0f9d 0A       		.uleb128 0xa
 2110 0f9e 200A0000 		.long	0xa20
 2111 0fa2 0A       		.uleb128 0xa
 2112 0fa3 DE080000 		.long	0x8de
 2113 0fa7 00       		.byte	0
 2114 0fa8 27       		.uleb128 0x27
 2115 0fa9 00000000 		.long	.LASF160
 2116 0fad 12       		.byte	0x12
 2117 0fae 4C01     		.value	0x14c
 2118 0fb0 DD090000 		.long	0x9dd
 2119 0fb4 C80F0000 		.long	0xfc8
 2120 0fb8 0A       		.uleb128 0xa
 2121 0fb9 DD090000 		.long	0x9dd
 2122 0fbd 0A       		.uleb128 0xa
 2123 0fbe 200A0000 		.long	0xa20
 2124 0fc2 0A       		.uleb128 0xa
 2125 0fc3 DE080000 		.long	0x8de
 2126 0fc7 00       		.byte	0
 2127 0fc8 27       		.uleb128 0x27
 2128 0fc9 00000000 		.long	.LASF161
 2129 0fcd 12       		.byte	0x12
 2130 0fce 5101     		.value	0x151
 2131 0fd0 DD090000 		.long	0x9dd
GAS LISTING /tmp/cce2OND8.s 			page 43


 2132 0fd4 E80F0000 		.long	0xfe8
 2133 0fd8 0A       		.uleb128 0xa
 2134 0fd9 DD090000 		.long	0x9dd
 2135 0fdd 0A       		.uleb128 0xa
 2136 0fde 200A0000 		.long	0xa20
 2137 0fe2 0A       		.uleb128 0xa
 2138 0fe3 DE080000 		.long	0x8de
 2139 0fe7 00       		.byte	0
 2140 0fe8 27       		.uleb128 0x27
 2141 0fe9 00000000 		.long	.LASF162
 2142 0fed 12       		.byte	0x12
 2143 0fee 5501     		.value	0x155
 2144 0ff0 DD090000 		.long	0x9dd
 2145 0ff4 08100000 		.long	0x1008
 2146 0ff8 0A       		.uleb128 0xa
 2147 0ff9 DD090000 		.long	0x9dd
 2148 0ffd 0A       		.uleb128 0xa
 2149 0ffe E3090000 		.long	0x9e3
 2150 1002 0A       		.uleb128 0xa
 2151 1003 DE080000 		.long	0x8de
 2152 1007 00       		.byte	0
 2153 1008 27       		.uleb128 0x27
 2154 1009 00000000 		.long	.LASF163
 2155 100d 12       		.byte	0x12
 2156 100e 5C02     		.value	0x25c
 2157 1010 57090000 		.long	0x957
 2158 1014 1F100000 		.long	0x101f
 2159 1018 0A       		.uleb128 0xa
 2160 1019 200A0000 		.long	0xa20
 2161 101d 28       		.uleb128 0x28
 2162 101e 00       		.byte	0
 2163 101f 27       		.uleb128 0x27
 2164 1020 00000000 		.long	.LASF164
 2165 1024 12       		.byte	0x12
 2166 1025 8502     		.value	0x285
 2167 1027 57090000 		.long	0x957
 2168 102b 36100000 		.long	0x1036
 2169 102f 0A       		.uleb128 0xa
 2170 1030 200A0000 		.long	0xa20
 2171 1034 28       		.uleb128 0x28
 2172 1035 00       		.byte	0
 2173 1036 14       		.uleb128 0x14
 2174 1037 00000000 		.long	.LASF165
 2175 103b 12       		.byte	0x12
 2176 103c E3       		.byte	0xe3
 2177 103d 00000000 		.long	.LASF165
 2178 1041 200A0000 		.long	0xa20
 2179 1045 54100000 		.long	0x1054
 2180 1049 0A       		.uleb128 0xa
 2181 104a 200A0000 		.long	0xa20
 2182 104e 0A       		.uleb128 0xa
 2183 104f E3090000 		.long	0x9e3
 2184 1053 00       		.byte	0
 2185 1054 0D       		.uleb128 0xd
 2186 1055 00000000 		.long	.LASF167
 2187 1059 12       		.byte	0x12
 2188 105a 0901     		.value	0x109
GAS LISTING /tmp/cce2OND8.s 			page 44


 2189 105c 00000000 		.long	.LASF167
 2190 1060 200A0000 		.long	0xa20
 2191 1064 73100000 		.long	0x1073
 2192 1068 0A       		.uleb128 0xa
 2193 1069 200A0000 		.long	0xa20
 2194 106d 0A       		.uleb128 0xa
 2195 106e 200A0000 		.long	0xa20
 2196 1072 00       		.byte	0
 2197 1073 14       		.uleb128 0x14
 2198 1074 00000000 		.long	.LASF168
 2199 1078 12       		.byte	0x12
 2200 1079 ED       		.byte	0xed
 2201 107a 00000000 		.long	.LASF168
 2202 107e 200A0000 		.long	0xa20
 2203 1082 91100000 		.long	0x1091
 2204 1086 0A       		.uleb128 0xa
 2205 1087 200A0000 		.long	0xa20
 2206 108b 0A       		.uleb128 0xa
 2207 108c E3090000 		.long	0x9e3
 2208 1090 00       		.byte	0
 2209 1091 0D       		.uleb128 0xd
 2210 1092 00000000 		.long	.LASF169
 2211 1096 12       		.byte	0x12
 2212 1097 1401     		.value	0x114
 2213 1099 00000000 		.long	.LASF169
 2214 109d 200A0000 		.long	0xa20
 2215 10a1 B0100000 		.long	0x10b0
 2216 10a5 0A       		.uleb128 0xa
 2217 10a6 200A0000 		.long	0xa20
 2218 10aa 0A       		.uleb128 0xa
 2219 10ab 200A0000 		.long	0xa20
 2220 10af 00       		.byte	0
 2221 10b0 0D       		.uleb128 0xd
 2222 10b1 00000000 		.long	.LASF170
 2223 10b5 12       		.byte	0x12
 2224 10b6 3F01     		.value	0x13f
 2225 10b8 00000000 		.long	.LASF170
 2226 10bc 200A0000 		.long	0xa20
 2227 10c0 D4100000 		.long	0x10d4
 2228 10c4 0A       		.uleb128 0xa
 2229 10c5 200A0000 		.long	0xa20
 2230 10c9 0A       		.uleb128 0xa
 2231 10ca E3090000 		.long	0x9e3
 2232 10ce 0A       		.uleb128 0xa
 2233 10cf DE080000 		.long	0x8de
 2234 10d3 00       		.byte	0
 2235 10d4 27       		.uleb128 0x27
 2236 10d5 00000000 		.long	.LASF171
 2237 10d9 12       		.byte	0x12
 2238 10da CE01     		.value	0x1ce
 2239 10dc EF100000 		.long	0x10ef
 2240 10e0 EF100000 		.long	0x10ef
 2241 10e4 0A       		.uleb128 0xa
 2242 10e5 200A0000 		.long	0xa20
 2243 10e9 0A       		.uleb128 0xa
 2244 10ea C40E0000 		.long	0xec4
 2245 10ee 00       		.byte	0
GAS LISTING /tmp/cce2OND8.s 			page 45


 2246 10ef 1D       		.uleb128 0x1d
 2247 10f0 10       		.byte	0x10
 2248 10f1 04       		.byte	0x4
 2249 10f2 00000000 		.long	.LASF172
 2250 10f6 27       		.uleb128 0x27
 2251 10f7 00000000 		.long	.LASF173
 2252 10fb 12       		.byte	0x12
 2253 10fc E601     		.value	0x1e6
 2254 10fe 16110000 		.long	0x1116
 2255 1102 16110000 		.long	0x1116
 2256 1106 0A       		.uleb128 0xa
 2257 1107 200A0000 		.long	0xa20
 2258 110b 0A       		.uleb128 0xa
 2259 110c C40E0000 		.long	0xec4
 2260 1110 0A       		.uleb128 0xa
 2261 1111 57090000 		.long	0x957
 2262 1115 00       		.byte	0
 2263 1116 1D       		.uleb128 0x1d
 2264 1117 08       		.byte	0x8
 2265 1118 05       		.byte	0x5
 2266 1119 00000000 		.long	.LASF174
 2267 111d 27       		.uleb128 0x27
 2268 111e 00000000 		.long	.LASF175
 2269 1122 12       		.byte	0x12
 2270 1123 ED01     		.value	0x1ed
 2271 1125 3D110000 		.long	0x113d
 2272 1129 3D110000 		.long	0x113d
 2273 112d 0A       		.uleb128 0xa
 2274 112e 200A0000 		.long	0xa20
 2275 1132 0A       		.uleb128 0xa
 2276 1133 C40E0000 		.long	0xec4
 2277 1137 0A       		.uleb128 0xa
 2278 1138 57090000 		.long	0x957
 2279 113c 00       		.byte	0
 2280 113d 1D       		.uleb128 0x1d
 2281 113e 08       		.byte	0x8
 2282 113f 07       		.byte	0x7
 2283 1140 00000000 		.long	.LASF176
 2284 1144 1D       		.uleb128 0x1d
 2285 1145 01       		.byte	0x1
 2286 1146 08       		.byte	0x8
 2287 1147 00000000 		.long	.LASF177
 2288 114b 1D       		.uleb128 0x1d
 2289 114c 01       		.byte	0x1
 2290 114d 06       		.byte	0x6
 2291 114e 00000000 		.long	.LASF178
 2292 1152 1D       		.uleb128 0x1d
 2293 1153 02       		.byte	0x2
 2294 1154 05       		.byte	0x5
 2295 1155 00000000 		.long	.LASF179
 2296 1159 18       		.uleb128 0x18
 2297 115a 00000000 		.long	.LASF180
 2298 115e 05       		.byte	0x5
 2299 115f 37       		.byte	0x37
 2300 1160 6C110000 		.long	0x116c
 2301 1164 04       		.uleb128 0x4
 2302 1165 05       		.byte	0x5
GAS LISTING /tmp/cce2OND8.s 			page 46


 2303 1166 38       		.byte	0x38
 2304 1167 F0010000 		.long	0x1f0
 2305 116b 00       		.byte	0
 2306 116c 2C       		.uleb128 0x2c
 2307 116d 08       		.byte	0x8
 2308 116e 03020000 		.long	0x203
 2309 1172 2C       		.uleb128 0x2c
 2310 1173 08       		.byte	0x8
 2311 1174 33020000 		.long	0x233
 2312 1178 1D       		.uleb128 0x1d
 2313 1179 01       		.byte	0x1
 2314 117a 02       		.byte	0x2
 2315 117b 00000000 		.long	.LASF181
 2316 117f 26       		.uleb128 0x26
 2317 1180 08       		.byte	0x8
 2318 1181 33020000 		.long	0x233
 2319 1185 26       		.uleb128 0x26
 2320 1186 08       		.byte	0x8
 2321 1187 03020000 		.long	0x203
 2322 118b 2C       		.uleb128 0x2c
 2323 118c 08       		.byte	0x8
 2324 118d 5A030000 		.long	0x35a
 2325 1191 07       		.uleb128 0x7
 2326 1192 00000000 		.long	.LASF182
 2327 1196 60       		.byte	0x60
 2328 1197 14       		.byte	0x14
 2329 1198 35       		.byte	0x35
 2330 1199 BE120000 		.long	0x12be
 2331 119d 1B       		.uleb128 0x1b
 2332 119e 00000000 		.long	.LASF183
 2333 11a2 14       		.byte	0x14
 2334 11a3 39       		.byte	0x39
 2335 11a4 A10C0000 		.long	0xca1
 2336 11a8 00       		.byte	0
 2337 11a9 1B       		.uleb128 0x1b
 2338 11aa 00000000 		.long	.LASF184
 2339 11ae 14       		.byte	0x14
 2340 11af 3A       		.byte	0x3a
 2341 11b0 A10C0000 		.long	0xca1
 2342 11b4 08       		.byte	0x8
 2343 11b5 1B       		.uleb128 0x1b
 2344 11b6 00000000 		.long	.LASF185
 2345 11ba 14       		.byte	0x14
 2346 11bb 40       		.byte	0x40
 2347 11bc A10C0000 		.long	0xca1
 2348 11c0 10       		.byte	0x10
 2349 11c1 1B       		.uleb128 0x1b
 2350 11c2 00000000 		.long	.LASF186
 2351 11c6 14       		.byte	0x14
 2352 11c7 46       		.byte	0x46
 2353 11c8 A10C0000 		.long	0xca1
 2354 11cc 18       		.byte	0x18
 2355 11cd 1B       		.uleb128 0x1b
 2356 11ce 00000000 		.long	.LASF187
 2357 11d2 14       		.byte	0x14
 2358 11d3 47       		.byte	0x47
 2359 11d4 A10C0000 		.long	0xca1
GAS LISTING /tmp/cce2OND8.s 			page 47


 2360 11d8 20       		.byte	0x20
 2361 11d9 1B       		.uleb128 0x1b
 2362 11da 00000000 		.long	.LASF188
 2363 11de 14       		.byte	0x14
 2364 11df 48       		.byte	0x48
 2365 11e0 A10C0000 		.long	0xca1
 2366 11e4 28       		.byte	0x28
 2367 11e5 1B       		.uleb128 0x1b
 2368 11e6 00000000 		.long	.LASF189
 2369 11ea 14       		.byte	0x14
 2370 11eb 49       		.byte	0x49
 2371 11ec A10C0000 		.long	0xca1
 2372 11f0 30       		.byte	0x30
 2373 11f1 1B       		.uleb128 0x1b
 2374 11f2 00000000 		.long	.LASF190
 2375 11f6 14       		.byte	0x14
 2376 11f7 4A       		.byte	0x4a
 2377 11f8 A10C0000 		.long	0xca1
 2378 11fc 38       		.byte	0x38
 2379 11fd 1B       		.uleb128 0x1b
 2380 11fe 00000000 		.long	.LASF191
 2381 1202 14       		.byte	0x14
 2382 1203 4B       		.byte	0x4b
 2383 1204 A10C0000 		.long	0xca1
 2384 1208 40       		.byte	0x40
 2385 1209 1B       		.uleb128 0x1b
 2386 120a 00000000 		.long	.LASF192
 2387 120e 14       		.byte	0x14
 2388 120f 4C       		.byte	0x4c
 2389 1210 A10C0000 		.long	0xca1
 2390 1214 48       		.byte	0x48
 2391 1215 1B       		.uleb128 0x1b
 2392 1216 00000000 		.long	.LASF193
 2393 121a 14       		.byte	0x14
 2394 121b 4D       		.byte	0x4d
 2395 121c 50090000 		.long	0x950
 2396 1220 50       		.byte	0x50
 2397 1221 1B       		.uleb128 0x1b
 2398 1222 00000000 		.long	.LASF194
 2399 1226 14       		.byte	0x14
 2400 1227 4E       		.byte	0x4e
 2401 1228 50090000 		.long	0x950
 2402 122c 51       		.byte	0x51
 2403 122d 1B       		.uleb128 0x1b
 2404 122e 00000000 		.long	.LASF195
 2405 1232 14       		.byte	0x14
 2406 1233 50       		.byte	0x50
 2407 1234 50090000 		.long	0x950
 2408 1238 52       		.byte	0x52
 2409 1239 1B       		.uleb128 0x1b
 2410 123a 00000000 		.long	.LASF196
 2411 123e 14       		.byte	0x14
 2412 123f 52       		.byte	0x52
 2413 1240 50090000 		.long	0x950
 2414 1244 53       		.byte	0x53
 2415 1245 1B       		.uleb128 0x1b
 2416 1246 00000000 		.long	.LASF197
GAS LISTING /tmp/cce2OND8.s 			page 48


 2417 124a 14       		.byte	0x14
 2418 124b 54       		.byte	0x54
 2419 124c 50090000 		.long	0x950
 2420 1250 54       		.byte	0x54
 2421 1251 1B       		.uleb128 0x1b
 2422 1252 00000000 		.long	.LASF198
 2423 1256 14       		.byte	0x14
 2424 1257 56       		.byte	0x56
 2425 1258 50090000 		.long	0x950
 2426 125c 55       		.byte	0x55
 2427 125d 1B       		.uleb128 0x1b
 2428 125e 00000000 		.long	.LASF199
 2429 1262 14       		.byte	0x14
 2430 1263 5D       		.byte	0x5d
 2431 1264 50090000 		.long	0x950
 2432 1268 56       		.byte	0x56
 2433 1269 1B       		.uleb128 0x1b
 2434 126a 00000000 		.long	.LASF200
 2435 126e 14       		.byte	0x14
 2436 126f 5E       		.byte	0x5e
 2437 1270 50090000 		.long	0x950
 2438 1274 57       		.byte	0x57
 2439 1275 1B       		.uleb128 0x1b
 2440 1276 00000000 		.long	.LASF201
 2441 127a 14       		.byte	0x14
 2442 127b 61       		.byte	0x61
 2443 127c 50090000 		.long	0x950
 2444 1280 58       		.byte	0x58
 2445 1281 1B       		.uleb128 0x1b
 2446 1282 00000000 		.long	.LASF202
 2447 1286 14       		.byte	0x14
 2448 1287 63       		.byte	0x63
 2449 1288 50090000 		.long	0x950
 2450 128c 59       		.byte	0x59
 2451 128d 1B       		.uleb128 0x1b
 2452 128e 00000000 		.long	.LASF203
 2453 1292 14       		.byte	0x14
 2454 1293 65       		.byte	0x65
 2455 1294 50090000 		.long	0x950
 2456 1298 5A       		.byte	0x5a
 2457 1299 1B       		.uleb128 0x1b
 2458 129a 00000000 		.long	.LASF204
 2459 129e 14       		.byte	0x14
 2460 129f 67       		.byte	0x67
 2461 12a0 50090000 		.long	0x950
 2462 12a4 5B       		.byte	0x5b
 2463 12a5 1B       		.uleb128 0x1b
 2464 12a6 00000000 		.long	.LASF205
 2465 12aa 14       		.byte	0x14
 2466 12ab 6E       		.byte	0x6e
 2467 12ac 50090000 		.long	0x950
 2468 12b0 5C       		.byte	0x5c
 2469 12b1 1B       		.uleb128 0x1b
 2470 12b2 00000000 		.long	.LASF206
 2471 12b6 14       		.byte	0x14
 2472 12b7 6F       		.byte	0x6f
 2473 12b8 50090000 		.long	0x950
GAS LISTING /tmp/cce2OND8.s 			page 49


 2474 12bc 5D       		.byte	0x5d
 2475 12bd 00       		.byte	0
 2476 12be 2A       		.uleb128 0x2a
 2477 12bf 00000000 		.long	.LASF207
 2478 12c3 14       		.byte	0x14
 2479 12c4 7C       		.byte	0x7c
 2480 12c5 A10C0000 		.long	0xca1
 2481 12c9 D8120000 		.long	0x12d8
 2482 12cd 0A       		.uleb128 0xa
 2483 12ce 57090000 		.long	0x957
 2484 12d2 0A       		.uleb128 0xa
 2485 12d3 80090000 		.long	0x980
 2486 12d7 00       		.byte	0
 2487 12d8 2D       		.uleb128 0x2d
 2488 12d9 00000000 		.long	.LASF209
 2489 12dd 14       		.byte	0x14
 2490 12de 7F       		.byte	0x7f
 2491 12df E3120000 		.long	0x12e3
 2492 12e3 26       		.uleb128 0x26
 2493 12e4 08       		.byte	0x8
 2494 12e5 91110000 		.long	0x1191
 2495 12e9 08       		.uleb128 0x8
 2496 12ea 00000000 		.long	.LASF210
 2497 12ee 15       		.byte	0x15
 2498 12ef 28       		.byte	0x28
 2499 12f0 57090000 		.long	0x957
 2500 12f4 08       		.uleb128 0x8
 2501 12f5 00000000 		.long	.LASF211
 2502 12f9 15       		.byte	0x15
 2503 12fa 83       		.byte	0x83
 2504 12fb 2C0F0000 		.long	0xf2c
 2505 12ff 08       		.uleb128 0x8
 2506 1300 00000000 		.long	.LASF212
 2507 1304 15       		.byte	0x15
 2508 1305 84       		.byte	0x84
 2509 1306 2C0F0000 		.long	0xf2c
 2510 130a 0B       		.uleb128 0xb
 2511 130b 78110000 		.long	0x1178
 2512 130f 0B       		.uleb128 0xb
 2513 1310 E9080000 		.long	0x8e9
 2514 1314 08       		.uleb128 0x8
 2515 1315 00000000 		.long	.LASF213
 2516 1319 16       		.byte	0x16
 2517 131a 34       		.byte	0x34
 2518 131b E9080000 		.long	0x8e9
 2519 131f 08       		.uleb128 0x8
 2520 1320 00000000 		.long	.LASF214
 2521 1324 16       		.byte	0x16
 2522 1325 BA       		.byte	0xba
 2523 1326 2A130000 		.long	0x132a
 2524 132a 26       		.uleb128 0x26
 2525 132b 08       		.byte	0x8
 2526 132c 30130000 		.long	0x1330
 2527 1330 0B       		.uleb128 0xb
 2528 1331 E9120000 		.long	0x12e9
 2529 1335 2A       		.uleb128 0x2a
 2530 1336 00000000 		.long	.LASF215
GAS LISTING /tmp/cce2OND8.s 			page 50


 2531 133a 16       		.byte	0x16
 2532 133b AF       		.byte	0xaf
 2533 133c 57090000 		.long	0x957
 2534 1340 4F130000 		.long	0x134f
 2535 1344 0A       		.uleb128 0xa
 2536 1345 F0080000 		.long	0x8f0
 2537 1349 0A       		.uleb128 0xa
 2538 134a 14130000 		.long	0x1314
 2539 134e 00       		.byte	0
 2540 134f 2A       		.uleb128 0x2a
 2541 1350 00000000 		.long	.LASF216
 2542 1354 16       		.byte	0x16
 2543 1355 DD       		.byte	0xdd
 2544 1356 F0080000 		.long	0x8f0
 2545 135a 69130000 		.long	0x1369
 2546 135e 0A       		.uleb128 0xa
 2547 135f F0080000 		.long	0x8f0
 2548 1363 0A       		.uleb128 0xa
 2549 1364 1F130000 		.long	0x131f
 2550 1368 00       		.byte	0
 2551 1369 2A       		.uleb128 0x2a
 2552 136a 00000000 		.long	.LASF217
 2553 136e 16       		.byte	0x16
 2554 136f DA       		.byte	0xda
 2555 1370 1F130000 		.long	0x131f
 2556 1374 7E130000 		.long	0x137e
 2557 1378 0A       		.uleb128 0xa
 2558 1379 80090000 		.long	0x980
 2559 137d 00       		.byte	0
 2560 137e 2A       		.uleb128 0x2a
 2561 137f 00000000 		.long	.LASF218
 2562 1383 16       		.byte	0x16
 2563 1384 AB       		.byte	0xab
 2564 1385 14130000 		.long	0x1314
 2565 1389 93130000 		.long	0x1393
 2566 138d 0A       		.uleb128 0xa
 2567 138e 80090000 		.long	0x980
 2568 1392 00       		.byte	0
 2569 1393 0B       		.uleb128 0xb
 2570 1394 52110000 		.long	0x1152
 2571 1398 0B       		.uleb128 0xb
 2572 1399 2C0F0000 		.long	0xf2c
 2573 139d 20       		.uleb128 0x20
 2574 139e 10       		.byte	0x10
 2575 139f 17       		.byte	0x17
 2576 13a0 16       		.byte	0x16
 2577 13a1 00000000 		.long	.LASF220
 2578 13a5 C2130000 		.long	0x13c2
 2579 13a9 1B       		.uleb128 0x1b
 2580 13aa 00000000 		.long	.LASF221
 2581 13ae 17       		.byte	0x17
 2582 13af 17       		.byte	0x17
 2583 13b0 F4120000 		.long	0x12f4
 2584 13b4 00       		.byte	0
 2585 13b5 1B       		.uleb128 0x1b
 2586 13b6 00000000 		.long	.LASF222
 2587 13ba 17       		.byte	0x17
GAS LISTING /tmp/cce2OND8.s 			page 51


 2588 13bb 18       		.byte	0x18
 2589 13bc 5E090000 		.long	0x95e
 2590 13c0 08       		.byte	0x8
 2591 13c1 00       		.byte	0
 2592 13c2 08       		.uleb128 0x8
 2593 13c3 00000000 		.long	.LASF223
 2594 13c7 17       		.byte	0x17
 2595 13c8 19       		.byte	0x19
 2596 13c9 9D130000 		.long	0x139d
 2597 13cd 2E       		.uleb128 0x2e
 2598 13ce 00000000 		.long	.LASF275
 2599 13d2 0F       		.byte	0xf
 2600 13d3 96       		.byte	0x96
 2601 13d4 07       		.uleb128 0x7
 2602 13d5 00000000 		.long	.LASF224
 2603 13d9 18       		.byte	0x18
 2604 13da 0F       		.byte	0xf
 2605 13db 9C       		.byte	0x9c
 2606 13dc 05140000 		.long	0x1405
 2607 13e0 1B       		.uleb128 0x1b
 2608 13e1 00000000 		.long	.LASF225
 2609 13e5 0F       		.byte	0xf
 2610 13e6 9D       		.byte	0x9d
 2611 13e7 05140000 		.long	0x1405
 2612 13eb 00       		.byte	0
 2613 13ec 1B       		.uleb128 0x1b
 2614 13ed 00000000 		.long	.LASF226
 2615 13f1 0F       		.byte	0xf
 2616 13f2 9E       		.byte	0x9e
 2617 13f3 0B140000 		.long	0x140b
 2618 13f7 08       		.byte	0x8
 2619 13f8 1B       		.uleb128 0x1b
 2620 13f9 00000000 		.long	.LASF227
 2621 13fd 0F       		.byte	0xf
 2622 13fe A2       		.byte	0xa2
 2623 13ff 57090000 		.long	0x957
 2624 1403 10       		.byte	0x10
 2625 1404 00       		.byte	0
 2626 1405 26       		.uleb128 0x26
 2627 1406 08       		.byte	0x8
 2628 1407 D4130000 		.long	0x13d4
 2629 140b 26       		.uleb128 0x26
 2630 140c 08       		.byte	0x8
 2631 140d 09070000 		.long	0x709
 2632 1411 23       		.uleb128 0x23
 2633 1412 50090000 		.long	0x950
 2634 1416 21140000 		.long	0x1421
 2635 141a 24       		.uleb128 0x24
 2636 141b 91080000 		.long	0x891
 2637 141f 00       		.byte	0
 2638 1420 00       		.byte	0
 2639 1421 26       		.uleb128 0x26
 2640 1422 08       		.byte	0x8
 2641 1423 CD130000 		.long	0x13cd
 2642 1427 23       		.uleb128 0x23
 2643 1428 50090000 		.long	0x950
 2644 142c 37140000 		.long	0x1437
GAS LISTING /tmp/cce2OND8.s 			page 52


 2645 1430 24       		.uleb128 0x24
 2646 1431 91080000 		.long	0x891
 2647 1435 13       		.byte	0x13
 2648 1436 00       		.byte	0
 2649 1437 08       		.uleb128 0x8
 2650 1438 00000000 		.long	.LASF228
 2651 143c 0E       		.byte	0xe
 2652 143d 6E       		.byte	0x6e
 2653 143e C2130000 		.long	0x13c2
 2654 1442 2F       		.uleb128 0x2f
 2655 1443 00000000 		.long	.LASF245
 2656 1447 0E       		.byte	0xe
 2657 1448 3A03     		.value	0x33a
 2658 144a 54140000 		.long	0x1454
 2659 144e 0A       		.uleb128 0xa
 2660 144f 54140000 		.long	0x1454
 2661 1453 00       		.byte	0
 2662 1454 26       		.uleb128 0x26
 2663 1455 08       		.byte	0x8
 2664 1456 FE060000 		.long	0x6fe
 2665 145a 2A       		.uleb128 0x2a
 2666 145b 00000000 		.long	.LASF229
 2667 145f 0E       		.byte	0xe
 2668 1460 ED       		.byte	0xed
 2669 1461 57090000 		.long	0x957
 2670 1465 6F140000 		.long	0x146f
 2671 1469 0A       		.uleb128 0xa
 2672 146a 54140000 		.long	0x1454
 2673 146e 00       		.byte	0
 2674 146f 27       		.uleb128 0x27
 2675 1470 00000000 		.long	.LASF230
 2676 1474 0E       		.byte	0xe
 2677 1475 3C03     		.value	0x33c
 2678 1477 57090000 		.long	0x957
 2679 147b 85140000 		.long	0x1485
 2680 147f 0A       		.uleb128 0xa
 2681 1480 54140000 		.long	0x1454
 2682 1484 00       		.byte	0
 2683 1485 27       		.uleb128 0x27
 2684 1486 00000000 		.long	.LASF231
 2685 148a 0E       		.byte	0xe
 2686 148b 3E03     		.value	0x33e
 2687 148d 57090000 		.long	0x957
 2688 1491 9B140000 		.long	0x149b
 2689 1495 0A       		.uleb128 0xa
 2690 1496 54140000 		.long	0x1454
 2691 149a 00       		.byte	0
 2692 149b 2A       		.uleb128 0x2a
 2693 149c 00000000 		.long	.LASF232
 2694 14a0 0E       		.byte	0xe
 2695 14a1 F2       		.byte	0xf2
 2696 14a2 57090000 		.long	0x957
 2697 14a6 B0140000 		.long	0x14b0
 2698 14aa 0A       		.uleb128 0xa
 2699 14ab 54140000 		.long	0x1454
 2700 14af 00       		.byte	0
 2701 14b0 27       		.uleb128 0x27
GAS LISTING /tmp/cce2OND8.s 			page 53


 2702 14b1 00000000 		.long	.LASF233
 2703 14b5 0E       		.byte	0xe
 2704 14b6 1302     		.value	0x213
 2705 14b8 57090000 		.long	0x957
 2706 14bc C6140000 		.long	0x14c6
 2707 14c0 0A       		.uleb128 0xa
 2708 14c1 54140000 		.long	0x1454
 2709 14c5 00       		.byte	0
 2710 14c6 27       		.uleb128 0x27
 2711 14c7 00000000 		.long	.LASF234
 2712 14cb 0E       		.byte	0xe
 2713 14cc 1E03     		.value	0x31e
 2714 14ce 57090000 		.long	0x957
 2715 14d2 E1140000 		.long	0x14e1
 2716 14d6 0A       		.uleb128 0xa
 2717 14d7 54140000 		.long	0x1454
 2718 14db 0A       		.uleb128 0xa
 2719 14dc E1140000 		.long	0x14e1
 2720 14e0 00       		.byte	0
 2721 14e1 26       		.uleb128 0x26
 2722 14e2 08       		.byte	0x8
 2723 14e3 37140000 		.long	0x1437
 2724 14e7 27       		.uleb128 0x27
 2725 14e8 00000000 		.long	.LASF235
 2726 14ec 0E       		.byte	0xe
 2727 14ed 6E02     		.value	0x26e
 2728 14ef A10C0000 		.long	0xca1
 2729 14f3 07150000 		.long	0x1507
 2730 14f7 0A       		.uleb128 0xa
 2731 14f8 A10C0000 		.long	0xca1
 2732 14fc 0A       		.uleb128 0xa
 2733 14fd 57090000 		.long	0x957
 2734 1501 0A       		.uleb128 0xa
 2735 1502 54140000 		.long	0x1454
 2736 1506 00       		.byte	0
 2737 1507 27       		.uleb128 0x27
 2738 1508 00000000 		.long	.LASF236
 2739 150c 0E       		.byte	0xe
 2740 150d 1001     		.value	0x110
 2741 150f 54140000 		.long	0x1454
 2742 1513 22150000 		.long	0x1522
 2743 1517 0A       		.uleb128 0xa
 2744 1518 80090000 		.long	0x980
 2745 151c 0A       		.uleb128 0xa
 2746 151d 80090000 		.long	0x980
 2747 1521 00       		.byte	0
 2748 1522 27       		.uleb128 0x27
 2749 1523 00000000 		.long	.LASF237
 2750 1527 0E       		.byte	0xe
 2751 1528 C502     		.value	0x2c5
 2752 152a DE080000 		.long	0x8de
 2753 152e 47150000 		.long	0x1547
 2754 1532 0A       		.uleb128 0xa
 2755 1533 DC080000 		.long	0x8dc
 2756 1537 0A       		.uleb128 0xa
 2757 1538 DE080000 		.long	0x8de
 2758 153c 0A       		.uleb128 0xa
GAS LISTING /tmp/cce2OND8.s 			page 54


 2759 153d DE080000 		.long	0x8de
 2760 1541 0A       		.uleb128 0xa
 2761 1542 54140000 		.long	0x1454
 2762 1546 00       		.byte	0
 2763 1547 27       		.uleb128 0x27
 2764 1548 00000000 		.long	.LASF238
 2765 154c 0E       		.byte	0xe
 2766 154d 1601     		.value	0x116
 2767 154f 54140000 		.long	0x1454
 2768 1553 67150000 		.long	0x1567
 2769 1557 0A       		.uleb128 0xa
 2770 1558 80090000 		.long	0x980
 2771 155c 0A       		.uleb128 0xa
 2772 155d 80090000 		.long	0x980
 2773 1561 0A       		.uleb128 0xa
 2774 1562 54140000 		.long	0x1454
 2775 1566 00       		.byte	0
 2776 1567 27       		.uleb128 0x27
 2777 1568 00000000 		.long	.LASF239
 2778 156c 0E       		.byte	0xe
 2779 156d ED02     		.value	0x2ed
 2780 156f 57090000 		.long	0x957
 2781 1573 87150000 		.long	0x1587
 2782 1577 0A       		.uleb128 0xa
 2783 1578 54140000 		.long	0x1454
 2784 157c 0A       		.uleb128 0xa
 2785 157d 2C0F0000 		.long	0xf2c
 2786 1581 0A       		.uleb128 0xa
 2787 1582 57090000 		.long	0x957
 2788 1586 00       		.byte	0
 2789 1587 27       		.uleb128 0x27
 2790 1588 00000000 		.long	.LASF240
 2791 158c 0E       		.byte	0xe
 2792 158d 2303     		.value	0x323
 2793 158f 57090000 		.long	0x957
 2794 1593 A2150000 		.long	0x15a2
 2795 1597 0A       		.uleb128 0xa
 2796 1598 54140000 		.long	0x1454
 2797 159c 0A       		.uleb128 0xa
 2798 159d A2150000 		.long	0x15a2
 2799 15a1 00       		.byte	0
 2800 15a2 26       		.uleb128 0x26
 2801 15a3 08       		.byte	0x8
 2802 15a4 A8150000 		.long	0x15a8
 2803 15a8 0B       		.uleb128 0xb
 2804 15a9 37140000 		.long	0x1437
 2805 15ad 27       		.uleb128 0x27
 2806 15ae 00000000 		.long	.LASF241
 2807 15b2 0E       		.byte	0xe
 2808 15b3 F202     		.value	0x2f2
 2809 15b5 2C0F0000 		.long	0xf2c
 2810 15b9 C3150000 		.long	0x15c3
 2811 15bd 0A       		.uleb128 0xa
 2812 15be 54140000 		.long	0x1454
 2813 15c2 00       		.byte	0
 2814 15c3 27       		.uleb128 0x27
 2815 15c4 00000000 		.long	.LASF242
GAS LISTING /tmp/cce2OND8.s 			page 55


 2816 15c8 0E       		.byte	0xe
 2817 15c9 1402     		.value	0x214
 2818 15cb 57090000 		.long	0x957
 2819 15cf D9150000 		.long	0x15d9
 2820 15d3 0A       		.uleb128 0xa
 2821 15d4 54140000 		.long	0x1454
 2822 15d8 00       		.byte	0
 2823 15d9 29       		.uleb128 0x29
 2824 15da 00000000 		.long	.LASF243
 2825 15de 0E       		.byte	0xe
 2826 15df 1A02     		.value	0x21a
 2827 15e1 57090000 		.long	0x957
 2828 15e5 27       		.uleb128 0x27
 2829 15e6 00000000 		.long	.LASF244
 2830 15ea 0E       		.byte	0xe
 2831 15eb 7E02     		.value	0x27e
 2832 15ed A10C0000 		.long	0xca1
 2833 15f1 FB150000 		.long	0x15fb
 2834 15f5 0A       		.uleb128 0xa
 2835 15f6 A10C0000 		.long	0xca1
 2836 15fa 00       		.byte	0
 2837 15fb 2F       		.uleb128 0x2f
 2838 15fc 00000000 		.long	.LASF246
 2839 1600 0E       		.byte	0xe
 2840 1601 4E03     		.value	0x34e
 2841 1603 0D160000 		.long	0x160d
 2842 1607 0A       		.uleb128 0xa
 2843 1608 80090000 		.long	0x980
 2844 160c 00       		.byte	0
 2845 160d 2A       		.uleb128 0x2a
 2846 160e 00000000 		.long	.LASF247
 2847 1612 0E       		.byte	0xe
 2848 1613 B2       		.byte	0xb2
 2849 1614 57090000 		.long	0x957
 2850 1618 22160000 		.long	0x1622
 2851 161c 0A       		.uleb128 0xa
 2852 161d 80090000 		.long	0x980
 2853 1621 00       		.byte	0
 2854 1622 2A       		.uleb128 0x2a
 2855 1623 00000000 		.long	.LASF248
 2856 1627 0E       		.byte	0xe
 2857 1628 B4       		.byte	0xb4
 2858 1629 57090000 		.long	0x957
 2859 162d 3C160000 		.long	0x163c
 2860 1631 0A       		.uleb128 0xa
 2861 1632 80090000 		.long	0x980
 2862 1636 0A       		.uleb128 0xa
 2863 1637 80090000 		.long	0x980
 2864 163b 00       		.byte	0
 2865 163c 2F       		.uleb128 0x2f
 2866 163d 00000000 		.long	.LASF249
 2867 1641 0E       		.byte	0xe
 2868 1642 F702     		.value	0x2f7
 2869 1644 4E160000 		.long	0x164e
 2870 1648 0A       		.uleb128 0xa
 2871 1649 54140000 		.long	0x1454
 2872 164d 00       		.byte	0
GAS LISTING /tmp/cce2OND8.s 			page 56


 2873 164e 2F       		.uleb128 0x2f
 2874 164f 00000000 		.long	.LASF250
 2875 1653 0E       		.byte	0xe
 2876 1654 4C01     		.value	0x14c
 2877 1656 65160000 		.long	0x1665
 2878 165a 0A       		.uleb128 0xa
 2879 165b 54140000 		.long	0x1454
 2880 165f 0A       		.uleb128 0xa
 2881 1660 A10C0000 		.long	0xca1
 2882 1664 00       		.byte	0
 2883 1665 27       		.uleb128 0x27
 2884 1666 00000000 		.long	.LASF251
 2885 166a 0E       		.byte	0xe
 2886 166b 5001     		.value	0x150
 2887 166d 57090000 		.long	0x957
 2888 1671 8A160000 		.long	0x168a
 2889 1675 0A       		.uleb128 0xa
 2890 1676 54140000 		.long	0x1454
 2891 167a 0A       		.uleb128 0xa
 2892 167b A10C0000 		.long	0xca1
 2893 167f 0A       		.uleb128 0xa
 2894 1680 57090000 		.long	0x957
 2895 1684 0A       		.uleb128 0xa
 2896 1685 DE080000 		.long	0x8de
 2897 1689 00       		.byte	0
 2898 168a 2D       		.uleb128 0x2d
 2899 168b 00000000 		.long	.LASF252
 2900 168f 0E       		.byte	0xe
 2901 1690 C3       		.byte	0xc3
 2902 1691 54140000 		.long	0x1454
 2903 1695 2A       		.uleb128 0x2a
 2904 1696 00000000 		.long	.LASF253
 2905 169a 0E       		.byte	0xe
 2906 169b D1       		.byte	0xd1
 2907 169c A10C0000 		.long	0xca1
 2908 16a0 AA160000 		.long	0x16aa
 2909 16a4 0A       		.uleb128 0xa
 2910 16a5 A10C0000 		.long	0xca1
 2911 16a9 00       		.byte	0
 2912 16aa 27       		.uleb128 0x27
 2913 16ab 00000000 		.long	.LASF254
 2914 16af 0E       		.byte	0xe
 2915 16b0 BE02     		.value	0x2be
 2916 16b2 57090000 		.long	0x957
 2917 16b6 C5160000 		.long	0x16c5
 2918 16ba 0A       		.uleb128 0xa
 2919 16bb 57090000 		.long	0x957
 2920 16bf 0A       		.uleb128 0xa
 2921 16c0 54140000 		.long	0x1454
 2922 16c4 00       		.byte	0
 2923 16c5 30       		.uleb128 0x30
 2924 16c6 30050000 		.long	0x530
 2925 16ca 00000000 		.quad	.LFB644
 2925      00000000 
 2926 16d2 12000000 		.quad	.LFE644-.LFB644
 2926      00000000 
 2927 16da 01       		.uleb128 0x1
GAS LISTING /tmp/cce2OND8.s 			page 57


 2928 16db 9C       		.byte	0x9c
 2929 16dc FD160000 		.long	0x16fd
 2930 16e0 31       		.uleb128 0x31
 2931 16e1 5F5F6100 		.string	"__a"
 2932 16e5 01       		.byte	0x1
 2933 16e6 81       		.byte	0x81
 2934 16e7 EA030000 		.long	0x3ea
 2935 16eb 02       		.uleb128 0x2
 2936 16ec 91       		.byte	0x91
 2937 16ed 6C       		.sleb128 -20
 2938 16ee 31       		.uleb128 0x31
 2939 16ef 5F5F6200 		.string	"__b"
 2940 16f3 01       		.byte	0x1
 2941 16f4 81       		.byte	0x81
 2942 16f5 EA030000 		.long	0x3ea
 2943 16f9 02       		.uleb128 0x2
 2944 16fa 91       		.byte	0x91
 2945 16fb 68       		.sleb128 -24
 2946 16fc 00       		.byte	0
 2947 16fd 32       		.uleb128 0x32
 2948 16fe 00000000 		.long	.LASF276
 2949 1702 02       		.byte	0x2
 2950 1703 28       		.byte	0x28
 2951 1704 57090000 		.long	0x957
 2952 1708 00000000 		.quad	.LFB1085
 2952      00000000 
 2953 1710 C5010000 		.quad	.LFE1085-.LFB1085
 2953      00000000 
 2954 1718 01       		.uleb128 0x1
 2955 1719 9C       		.byte	0x9c
 2956 171a 7F170000 		.long	0x177f
 2957 171e 33       		.uleb128 0x33
 2958 171f 00000000 		.long	.LASF255
 2959 1723 02       		.byte	0x2
 2960 1724 2A       		.byte	0x2a
 2961 1725 4E050000 		.long	0x54e
 2962 1729 03       		.uleb128 0x3
 2963 172a 91       		.byte	0x91
 2964 172b D077     		.sleb128 -1072
 2965 172d 33       		.uleb128 0x33
 2966 172e 00000000 		.long	.LASF256
 2967 1732 02       		.byte	0x2
 2968 1733 33       		.byte	0x33
 2969 1734 4E050000 		.long	0x54e
 2970 1738 03       		.uleb128 0x3
 2971 1739 91       		.byte	0x91
 2972 173a D07B     		.sleb128 -560
 2973 173c 34       		.uleb128 0x34
 2974 173d 00000000 		.quad	.LBB2
 2974      00000000 
 2975 1745 66000000 		.quad	.LBE2-.LBB2
 2975      00000000 
 2976 174d 5F170000 		.long	0x175f
 2977 1751 35       		.uleb128 0x35
 2978 1752 6900     		.string	"i"
 2979 1754 02       		.byte	0x2
 2980 1755 2E       		.byte	0x2e
GAS LISTING /tmp/cce2OND8.s 			page 58


 2981 1756 57090000 		.long	0x957
 2982 175a 03       		.uleb128 0x3
 2983 175b 91       		.byte	0x91
 2984 175c C877     		.sleb128 -1080
 2985 175e 00       		.byte	0
 2986 175f 36       		.uleb128 0x36
 2987 1760 00000000 		.quad	.LBB3
 2987      00000000 
 2988 1768 66000000 		.quad	.LBE3-.LBB3
 2988      00000000 
 2989 1770 35       		.uleb128 0x35
 2990 1771 6900     		.string	"i"
 2991 1773 02       		.byte	0x2
 2992 1774 35       		.byte	0x35
 2993 1775 57090000 		.long	0x957
 2994 1779 03       		.uleb128 0x3
 2995 177a 91       		.byte	0x91
 2996 177b CC77     		.sleb128 -1076
 2997 177d 00       		.byte	0
 2998 177e 00       		.byte	0
 2999 177f 37       		.uleb128 0x37
 3000 1780 C3050000 		.long	0x5c3
 3001 1784 00000000 		.long	.LASF257
 3002 1788 80808080 		.sleb128 -2147483648
 3002      78
 3003 178d 38       		.uleb128 0x38
 3004 178e CE050000 		.long	0x5ce
 3005 1792 00000000 		.long	.LASF258
 3006 1796 FFFFFF7F 		.long	0x7fffffff
 3007 179a 39       		.uleb128 0x39
 3008 179b 26060000 		.long	0x626
 3009 179f 00000000 		.long	.LASF259
 3010 17a3 40       		.byte	0x40
 3011 17a4 39       		.uleb128 0x39
 3012 17a5 52060000 		.long	0x652
 3013 17a9 00000000 		.long	.LASF260
 3014 17ad 7F       		.byte	0x7f
 3015 17ae 37       		.uleb128 0x37
 3016 17af 89060000 		.long	0x689
 3017 17b3 00000000 		.long	.LASF261
 3018 17b7 80807E   		.sleb128 -32768
 3019 17ba 3A       		.uleb128 0x3a
 3020 17bb 94060000 		.long	0x694
 3021 17bf 00000000 		.long	.LASF262
 3022 17c3 FF7F     		.value	0x7fff
 3023 17c5 37       		.uleb128 0x37
 3024 17c6 C7060000 		.long	0x6c7
 3025 17ca 00000000 		.long	.LASF263
 3026 17ce 80808080 		.sleb128 -9223372036854775808
 3026      80808080 
 3026      807F
 3027 17d8 3B       		.uleb128 0x3b
 3028 17d9 D2060000 		.long	0x6d2
 3029 17dd 00000000 		.long	.LASF264
 3030 17e1 FFFFFFFF 		.quad	0x7fffffffffffffff
 3030      FFFFFF7F 
 3031 17e9 00       		.byte	0
GAS LISTING /tmp/cce2OND8.s 			page 59


 3032              		.section	.debug_abbrev,"",@progbits
 3033              	.Ldebug_abbrev0:
 3034 0000 01       		.uleb128 0x1
 3035 0001 11       		.uleb128 0x11
 3036 0002 01       		.byte	0x1
 3037 0003 25       		.uleb128 0x25
 3038 0004 0E       		.uleb128 0xe
 3039 0005 13       		.uleb128 0x13
 3040 0006 0B       		.uleb128 0xb
 3041 0007 03       		.uleb128 0x3
 3042 0008 0E       		.uleb128 0xe
 3043 0009 1B       		.uleb128 0x1b
 3044 000a 0E       		.uleb128 0xe
 3045 000b 55       		.uleb128 0x55
 3046 000c 17       		.uleb128 0x17
 3047 000d 11       		.uleb128 0x11
 3048 000e 01       		.uleb128 0x1
 3049 000f 10       		.uleb128 0x10
 3050 0010 17       		.uleb128 0x17
 3051 0011 00       		.byte	0
 3052 0012 00       		.byte	0
 3053 0013 02       		.uleb128 0x2
 3054 0014 39       		.uleb128 0x39
 3055 0015 01       		.byte	0x1
 3056 0016 03       		.uleb128 0x3
 3057 0017 08       		.uleb128 0x8
 3058 0018 3A       		.uleb128 0x3a
 3059 0019 0B       		.uleb128 0xb
 3060 001a 3B       		.uleb128 0x3b
 3061 001b 0B       		.uleb128 0xb
 3062 001c 01       		.uleb128 0x1
 3063 001d 13       		.uleb128 0x13
 3064 001e 00       		.byte	0
 3065 001f 00       		.byte	0
 3066 0020 03       		.uleb128 0x3
 3067 0021 39       		.uleb128 0x39
 3068 0022 00       		.byte	0
 3069 0023 03       		.uleb128 0x3
 3070 0024 0E       		.uleb128 0xe
 3071 0025 3A       		.uleb128 0x3a
 3072 0026 0B       		.uleb128 0xb
 3073 0027 3B       		.uleb128 0x3b
 3074 0028 0B       		.uleb128 0xb
 3075 0029 00       		.byte	0
 3076 002a 00       		.byte	0
 3077 002b 04       		.uleb128 0x4
 3078 002c 3A       		.uleb128 0x3a
 3079 002d 00       		.byte	0
 3080 002e 3A       		.uleb128 0x3a
 3081 002f 0B       		.uleb128 0xb
 3082 0030 3B       		.uleb128 0x3b
 3083 0031 0B       		.uleb128 0xb
 3084 0032 18       		.uleb128 0x18
 3085 0033 13       		.uleb128 0x13
 3086 0034 00       		.byte	0
 3087 0035 00       		.byte	0
 3088 0036 05       		.uleb128 0x5
GAS LISTING /tmp/cce2OND8.s 			page 60


 3089 0037 08       		.uleb128 0x8
 3090 0038 00       		.byte	0
 3091 0039 3A       		.uleb128 0x3a
 3092 003a 0B       		.uleb128 0xb
 3093 003b 3B       		.uleb128 0x3b
 3094 003c 0B       		.uleb128 0xb
 3095 003d 18       		.uleb128 0x18
 3096 003e 13       		.uleb128 0x13
 3097 003f 00       		.byte	0
 3098 0040 00       		.byte	0
 3099 0041 06       		.uleb128 0x6
 3100 0042 08       		.uleb128 0x8
 3101 0043 00       		.byte	0
 3102 0044 3A       		.uleb128 0x3a
 3103 0045 0B       		.uleb128 0xb
 3104 0046 3B       		.uleb128 0x3b
 3105 0047 05       		.uleb128 0x5
 3106 0048 18       		.uleb128 0x18
 3107 0049 13       		.uleb128 0x13
 3108 004a 00       		.byte	0
 3109 004b 00       		.byte	0
 3110 004c 07       		.uleb128 0x7
 3111 004d 13       		.uleb128 0x13
 3112 004e 01       		.byte	0x1
 3113 004f 03       		.uleb128 0x3
 3114 0050 0E       		.uleb128 0xe
 3115 0051 0B       		.uleb128 0xb
 3116 0052 0B       		.uleb128 0xb
 3117 0053 3A       		.uleb128 0x3a
 3118 0054 0B       		.uleb128 0xb
 3119 0055 3B       		.uleb128 0x3b
 3120 0056 0B       		.uleb128 0xb
 3121 0057 01       		.uleb128 0x1
 3122 0058 13       		.uleb128 0x13
 3123 0059 00       		.byte	0
 3124 005a 00       		.byte	0
 3125 005b 08       		.uleb128 0x8
 3126 005c 16       		.uleb128 0x16
 3127 005d 00       		.byte	0
 3128 005e 03       		.uleb128 0x3
 3129 005f 0E       		.uleb128 0xe
 3130 0060 3A       		.uleb128 0x3a
 3131 0061 0B       		.uleb128 0xb
 3132 0062 3B       		.uleb128 0x3b
 3133 0063 0B       		.uleb128 0xb
 3134 0064 49       		.uleb128 0x49
 3135 0065 13       		.uleb128 0x13
 3136 0066 00       		.byte	0
 3137 0067 00       		.byte	0
 3138 0068 09       		.uleb128 0x9
 3139 0069 2E       		.uleb128 0x2e
 3140 006a 01       		.byte	0x1
 3141 006b 3F       		.uleb128 0x3f
 3142 006c 19       		.uleb128 0x19
 3143 006d 03       		.uleb128 0x3
 3144 006e 0E       		.uleb128 0xe
 3145 006f 3A       		.uleb128 0x3a
GAS LISTING /tmp/cce2OND8.s 			page 61


 3146 0070 0B       		.uleb128 0xb
 3147 0071 3B       		.uleb128 0x3b
 3148 0072 0B       		.uleb128 0xb
 3149 0073 6E       		.uleb128 0x6e
 3150 0074 0E       		.uleb128 0xe
 3151 0075 3C       		.uleb128 0x3c
 3152 0076 19       		.uleb128 0x19
 3153 0077 01       		.uleb128 0x1
 3154 0078 13       		.uleb128 0x13
 3155 0079 00       		.byte	0
 3156 007a 00       		.byte	0
 3157 007b 0A       		.uleb128 0xa
 3158 007c 05       		.uleb128 0x5
 3159 007d 00       		.byte	0
 3160 007e 49       		.uleb128 0x49
 3161 007f 13       		.uleb128 0x13
 3162 0080 00       		.byte	0
 3163 0081 00       		.byte	0
 3164 0082 0B       		.uleb128 0xb
 3165 0083 26       		.uleb128 0x26
 3166 0084 00       		.byte	0
 3167 0085 49       		.uleb128 0x49
 3168 0086 13       		.uleb128 0x13
 3169 0087 00       		.byte	0
 3170 0088 00       		.byte	0
 3171 0089 0C       		.uleb128 0xc
 3172 008a 2E       		.uleb128 0x2e
 3173 008b 01       		.byte	0x1
 3174 008c 3F       		.uleb128 0x3f
 3175 008d 19       		.uleb128 0x19
 3176 008e 03       		.uleb128 0x3
 3177 008f 08       		.uleb128 0x8
 3178 0090 3A       		.uleb128 0x3a
 3179 0091 0B       		.uleb128 0xb
 3180 0092 3B       		.uleb128 0x3b
 3181 0093 0B       		.uleb128 0xb
 3182 0094 6E       		.uleb128 0x6e
 3183 0095 0E       		.uleb128 0xe
 3184 0096 49       		.uleb128 0x49
 3185 0097 13       		.uleb128 0x13
 3186 0098 3C       		.uleb128 0x3c
 3187 0099 19       		.uleb128 0x19
 3188 009a 01       		.uleb128 0x1
 3189 009b 13       		.uleb128 0x13
 3190 009c 00       		.byte	0
 3191 009d 00       		.byte	0
 3192 009e 0D       		.uleb128 0xd
 3193 009f 2E       		.uleb128 0x2e
 3194 00a0 01       		.byte	0x1
 3195 00a1 3F       		.uleb128 0x3f
 3196 00a2 19       		.uleb128 0x19
 3197 00a3 03       		.uleb128 0x3
 3198 00a4 0E       		.uleb128 0xe
 3199 00a5 3A       		.uleb128 0x3a
 3200 00a6 0B       		.uleb128 0xb
 3201 00a7 3B       		.uleb128 0x3b
 3202 00a8 05       		.uleb128 0x5
GAS LISTING /tmp/cce2OND8.s 			page 62


 3203 00a9 6E       		.uleb128 0x6e
 3204 00aa 0E       		.uleb128 0xe
 3205 00ab 49       		.uleb128 0x49
 3206 00ac 13       		.uleb128 0x13
 3207 00ad 3C       		.uleb128 0x3c
 3208 00ae 19       		.uleb128 0x19
 3209 00af 01       		.uleb128 0x1
 3210 00b0 13       		.uleb128 0x13
 3211 00b1 00       		.byte	0
 3212 00b2 00       		.byte	0
 3213 00b3 0E       		.uleb128 0xe
 3214 00b4 2E       		.uleb128 0x2e
 3215 00b5 00       		.byte	0
 3216 00b6 3F       		.uleb128 0x3f
 3217 00b7 19       		.uleb128 0x19
 3218 00b8 03       		.uleb128 0x3
 3219 00b9 08       		.uleb128 0x8
 3220 00ba 3A       		.uleb128 0x3a
 3221 00bb 0B       		.uleb128 0xb
 3222 00bc 3B       		.uleb128 0x3b
 3223 00bd 05       		.uleb128 0x5
 3224 00be 6E       		.uleb128 0x6e
 3225 00bf 0E       		.uleb128 0xe
 3226 00c0 49       		.uleb128 0x49
 3227 00c1 13       		.uleb128 0x13
 3228 00c2 3C       		.uleb128 0x3c
 3229 00c3 19       		.uleb128 0x19
 3230 00c4 00       		.byte	0
 3231 00c5 00       		.byte	0
 3232 00c6 0F       		.uleb128 0xf
 3233 00c7 2E       		.uleb128 0x2e
 3234 00c8 01       		.byte	0x1
 3235 00c9 3F       		.uleb128 0x3f
 3236 00ca 19       		.uleb128 0x19
 3237 00cb 03       		.uleb128 0x3
 3238 00cc 0E       		.uleb128 0xe
 3239 00cd 3A       		.uleb128 0x3a
 3240 00ce 0B       		.uleb128 0xb
 3241 00cf 3B       		.uleb128 0x3b
 3242 00d0 05       		.uleb128 0x5
 3243 00d1 6E       		.uleb128 0x6e
 3244 00d2 0E       		.uleb128 0xe
 3245 00d3 49       		.uleb128 0x49
 3246 00d4 13       		.uleb128 0x13
 3247 00d5 3C       		.uleb128 0x3c
 3248 00d6 19       		.uleb128 0x19
 3249 00d7 00       		.byte	0
 3250 00d8 00       		.byte	0
 3251 00d9 10       		.uleb128 0x10
 3252 00da 04       		.uleb128 0x4
 3253 00db 01       		.byte	0x1
 3254 00dc 03       		.uleb128 0x3
 3255 00dd 0E       		.uleb128 0xe
 3256 00de 0B       		.uleb128 0xb
 3257 00df 0B       		.uleb128 0xb
 3258 00e0 49       		.uleb128 0x49
 3259 00e1 13       		.uleb128 0x13
GAS LISTING /tmp/cce2OND8.s 			page 63


 3260 00e2 3A       		.uleb128 0x3a
 3261 00e3 0B       		.uleb128 0xb
 3262 00e4 3B       		.uleb128 0x3b
 3263 00e5 0B       		.uleb128 0xb
 3264 00e6 01       		.uleb128 0x1
 3265 00e7 13       		.uleb128 0x13
 3266 00e8 00       		.byte	0
 3267 00e9 00       		.byte	0
 3268 00ea 11       		.uleb128 0x11
 3269 00eb 28       		.uleb128 0x28
 3270 00ec 00       		.byte	0
 3271 00ed 03       		.uleb128 0x3
 3272 00ee 0E       		.uleb128 0xe
 3273 00ef 1C       		.uleb128 0x1c
 3274 00f0 0B       		.uleb128 0xb
 3275 00f1 00       		.byte	0
 3276 00f2 00       		.byte	0
 3277 00f3 12       		.uleb128 0x12
 3278 00f4 28       		.uleb128 0x28
 3279 00f5 00       		.byte	0
 3280 00f6 03       		.uleb128 0x3
 3281 00f7 0E       		.uleb128 0xe
 3282 00f8 1C       		.uleb128 0x1c
 3283 00f9 06       		.uleb128 0x6
 3284 00fa 00       		.byte	0
 3285 00fb 00       		.byte	0
 3286 00fc 13       		.uleb128 0x13
 3287 00fd 28       		.uleb128 0x28
 3288 00fe 00       		.byte	0
 3289 00ff 03       		.uleb128 0x3
 3290 0100 0E       		.uleb128 0xe
 3291 0101 1C       		.uleb128 0x1c
 3292 0102 0D       		.uleb128 0xd
 3293 0103 00       		.byte	0
 3294 0104 00       		.byte	0
 3295 0105 14       		.uleb128 0x14
 3296 0106 2E       		.uleb128 0x2e
 3297 0107 01       		.byte	0x1
 3298 0108 3F       		.uleb128 0x3f
 3299 0109 19       		.uleb128 0x19
 3300 010a 03       		.uleb128 0x3
 3301 010b 0E       		.uleb128 0xe
 3302 010c 3A       		.uleb128 0x3a
 3303 010d 0B       		.uleb128 0xb
 3304 010e 3B       		.uleb128 0x3b
 3305 010f 0B       		.uleb128 0xb
 3306 0110 6E       		.uleb128 0x6e
 3307 0111 0E       		.uleb128 0xe
 3308 0112 49       		.uleb128 0x49
 3309 0113 13       		.uleb128 0x13
 3310 0114 3C       		.uleb128 0x3c
 3311 0115 19       		.uleb128 0x19
 3312 0116 01       		.uleb128 0x1
 3313 0117 13       		.uleb128 0x13
 3314 0118 00       		.byte	0
 3315 0119 00       		.byte	0
 3316 011a 15       		.uleb128 0x15
GAS LISTING /tmp/cce2OND8.s 			page 64


 3317 011b 02       		.uleb128 0x2
 3318 011c 01       		.byte	0x1
 3319 011d 03       		.uleb128 0x3
 3320 011e 0E       		.uleb128 0xe
 3321 011f 3C       		.uleb128 0x3c
 3322 0120 19       		.uleb128 0x19
 3323 0121 00       		.byte	0
 3324 0122 00       		.byte	0
 3325 0123 16       		.uleb128 0x16
 3326 0124 2F       		.uleb128 0x2f
 3327 0125 00       		.byte	0
 3328 0126 03       		.uleb128 0x3
 3329 0127 0E       		.uleb128 0xe
 3330 0128 49       		.uleb128 0x49
 3331 0129 13       		.uleb128 0x13
 3332 012a 00       		.byte	0
 3333 012b 00       		.byte	0
 3334 012c 17       		.uleb128 0x17
 3335 012d 2F       		.uleb128 0x2f
 3336 012e 00       		.byte	0
 3337 012f 03       		.uleb128 0x3
 3338 0130 0E       		.uleb128 0xe
 3339 0131 49       		.uleb128 0x49
 3340 0132 13       		.uleb128 0x13
 3341 0133 1E       		.uleb128 0x1e
 3342 0134 19       		.uleb128 0x19
 3343 0135 00       		.byte	0
 3344 0136 00       		.byte	0
 3345 0137 18       		.uleb128 0x18
 3346 0138 39       		.uleb128 0x39
 3347 0139 01       		.byte	0x1
 3348 013a 03       		.uleb128 0x3
 3349 013b 0E       		.uleb128 0xe
 3350 013c 3A       		.uleb128 0x3a
 3351 013d 0B       		.uleb128 0xb
 3352 013e 3B       		.uleb128 0x3b
 3353 013f 0B       		.uleb128 0xb
 3354 0140 01       		.uleb128 0x1
 3355 0141 13       		.uleb128 0x13
 3356 0142 00       		.byte	0
 3357 0143 00       		.byte	0
 3358 0144 19       		.uleb128 0x19
 3359 0145 0D       		.uleb128 0xd
 3360 0146 00       		.byte	0
 3361 0147 03       		.uleb128 0x3
 3362 0148 0E       		.uleb128 0xe
 3363 0149 3A       		.uleb128 0x3a
 3364 014a 0B       		.uleb128 0xb
 3365 014b 3B       		.uleb128 0x3b
 3366 014c 0B       		.uleb128 0xb
 3367 014d 49       		.uleb128 0x49
 3368 014e 13       		.uleb128 0x13
 3369 014f 3F       		.uleb128 0x3f
 3370 0150 19       		.uleb128 0x19
 3371 0151 3C       		.uleb128 0x3c
 3372 0152 19       		.uleb128 0x19
 3373 0153 00       		.byte	0
GAS LISTING /tmp/cce2OND8.s 			page 65


 3374 0154 00       		.byte	0
 3375 0155 1A       		.uleb128 0x1a
 3376 0156 13       		.uleb128 0x13
 3377 0157 01       		.byte	0x1
 3378 0158 03       		.uleb128 0x3
 3379 0159 0E       		.uleb128 0xe
 3380 015a 0B       		.uleb128 0xb
 3381 015b 0B       		.uleb128 0xb
 3382 015c 3A       		.uleb128 0x3a
 3383 015d 0B       		.uleb128 0xb
 3384 015e 3B       		.uleb128 0x3b
 3385 015f 0B       		.uleb128 0xb
 3386 0160 00       		.byte	0
 3387 0161 00       		.byte	0
 3388 0162 1B       		.uleb128 0x1b
 3389 0163 0D       		.uleb128 0xd
 3390 0164 00       		.byte	0
 3391 0165 03       		.uleb128 0x3
 3392 0166 0E       		.uleb128 0xe
 3393 0167 3A       		.uleb128 0x3a
 3394 0168 0B       		.uleb128 0xb
 3395 0169 3B       		.uleb128 0x3b
 3396 016a 0B       		.uleb128 0xb
 3397 016b 49       		.uleb128 0x49
 3398 016c 13       		.uleb128 0x13
 3399 016d 38       		.uleb128 0x38
 3400 016e 0B       		.uleb128 0xb
 3401 016f 00       		.byte	0
 3402 0170 00       		.byte	0
 3403 0171 1C       		.uleb128 0x1c
 3404 0172 0D       		.uleb128 0xd
 3405 0173 00       		.byte	0
 3406 0174 03       		.uleb128 0x3
 3407 0175 0E       		.uleb128 0xe
 3408 0176 3A       		.uleb128 0x3a
 3409 0177 0B       		.uleb128 0xb
 3410 0178 3B       		.uleb128 0x3b
 3411 0179 05       		.uleb128 0x5
 3412 017a 49       		.uleb128 0x49
 3413 017b 13       		.uleb128 0x13
 3414 017c 38       		.uleb128 0x38
 3415 017d 0B       		.uleb128 0xb
 3416 017e 00       		.byte	0
 3417 017f 00       		.byte	0
 3418 0180 1D       		.uleb128 0x1d
 3419 0181 24       		.uleb128 0x24
 3420 0182 00       		.byte	0
 3421 0183 0B       		.uleb128 0xb
 3422 0184 0B       		.uleb128 0xb
 3423 0185 3E       		.uleb128 0x3e
 3424 0186 0B       		.uleb128 0xb
 3425 0187 03       		.uleb128 0x3
 3426 0188 0E       		.uleb128 0xe
 3427 0189 00       		.byte	0
 3428 018a 00       		.byte	0
 3429 018b 1E       		.uleb128 0x1e
 3430 018c 0F       		.uleb128 0xf
GAS LISTING /tmp/cce2OND8.s 			page 66


 3431 018d 00       		.byte	0
 3432 018e 0B       		.uleb128 0xb
 3433 018f 0B       		.uleb128 0xb
 3434 0190 00       		.byte	0
 3435 0191 00       		.byte	0
 3436 0192 1F       		.uleb128 0x1f
 3437 0193 16       		.uleb128 0x16
 3438 0194 00       		.byte	0
 3439 0195 03       		.uleb128 0x3
 3440 0196 0E       		.uleb128 0xe
 3441 0197 3A       		.uleb128 0x3a
 3442 0198 0B       		.uleb128 0xb
 3443 0199 3B       		.uleb128 0x3b
 3444 019a 05       		.uleb128 0x5
 3445 019b 49       		.uleb128 0x49
 3446 019c 13       		.uleb128 0x13
 3447 019d 00       		.byte	0
 3448 019e 00       		.byte	0
 3449 019f 20       		.uleb128 0x20
 3450 01a0 13       		.uleb128 0x13
 3451 01a1 01       		.byte	0x1
 3452 01a2 0B       		.uleb128 0xb
 3453 01a3 0B       		.uleb128 0xb
 3454 01a4 3A       		.uleb128 0x3a
 3455 01a5 0B       		.uleb128 0xb
 3456 01a6 3B       		.uleb128 0x3b
 3457 01a7 0B       		.uleb128 0xb
 3458 01a8 6E       		.uleb128 0x6e
 3459 01a9 0E       		.uleb128 0xe
 3460 01aa 01       		.uleb128 0x1
 3461 01ab 13       		.uleb128 0x13
 3462 01ac 00       		.byte	0
 3463 01ad 00       		.byte	0
 3464 01ae 21       		.uleb128 0x21
 3465 01af 17       		.uleb128 0x17
 3466 01b0 01       		.byte	0x1
 3467 01b1 0B       		.uleb128 0xb
 3468 01b2 0B       		.uleb128 0xb
 3469 01b3 3A       		.uleb128 0x3a
 3470 01b4 0B       		.uleb128 0xb
 3471 01b5 3B       		.uleb128 0x3b
 3472 01b6 0B       		.uleb128 0xb
 3473 01b7 01       		.uleb128 0x1
 3474 01b8 13       		.uleb128 0x13
 3475 01b9 00       		.byte	0
 3476 01ba 00       		.byte	0
 3477 01bb 22       		.uleb128 0x22
 3478 01bc 0D       		.uleb128 0xd
 3479 01bd 00       		.byte	0
 3480 01be 03       		.uleb128 0x3
 3481 01bf 0E       		.uleb128 0xe
 3482 01c0 3A       		.uleb128 0x3a
 3483 01c1 0B       		.uleb128 0xb
 3484 01c2 3B       		.uleb128 0x3b
 3485 01c3 0B       		.uleb128 0xb
 3486 01c4 49       		.uleb128 0x49
 3487 01c5 13       		.uleb128 0x13
GAS LISTING /tmp/cce2OND8.s 			page 67


 3488 01c6 00       		.byte	0
 3489 01c7 00       		.byte	0
 3490 01c8 23       		.uleb128 0x23
 3491 01c9 01       		.uleb128 0x1
 3492 01ca 01       		.byte	0x1
 3493 01cb 49       		.uleb128 0x49
 3494 01cc 13       		.uleb128 0x13
 3495 01cd 01       		.uleb128 0x1
 3496 01ce 13       		.uleb128 0x13
 3497 01cf 00       		.byte	0
 3498 01d0 00       		.byte	0
 3499 01d1 24       		.uleb128 0x24
 3500 01d2 21       		.uleb128 0x21
 3501 01d3 00       		.byte	0
 3502 01d4 49       		.uleb128 0x49
 3503 01d5 13       		.uleb128 0x13
 3504 01d6 2F       		.uleb128 0x2f
 3505 01d7 0B       		.uleb128 0xb
 3506 01d8 00       		.byte	0
 3507 01d9 00       		.byte	0
 3508 01da 25       		.uleb128 0x25
 3509 01db 24       		.uleb128 0x24
 3510 01dc 00       		.byte	0
 3511 01dd 0B       		.uleb128 0xb
 3512 01de 0B       		.uleb128 0xb
 3513 01df 3E       		.uleb128 0x3e
 3514 01e0 0B       		.uleb128 0xb
 3515 01e1 03       		.uleb128 0x3
 3516 01e2 08       		.uleb128 0x8
 3517 01e3 00       		.byte	0
 3518 01e4 00       		.byte	0
 3519 01e5 26       		.uleb128 0x26
 3520 01e6 0F       		.uleb128 0xf
 3521 01e7 00       		.byte	0
 3522 01e8 0B       		.uleb128 0xb
 3523 01e9 0B       		.uleb128 0xb
 3524 01ea 49       		.uleb128 0x49
 3525 01eb 13       		.uleb128 0x13
 3526 01ec 00       		.byte	0
 3527 01ed 00       		.byte	0
 3528 01ee 27       		.uleb128 0x27
 3529 01ef 2E       		.uleb128 0x2e
 3530 01f0 01       		.byte	0x1
 3531 01f1 3F       		.uleb128 0x3f
 3532 01f2 19       		.uleb128 0x19
 3533 01f3 03       		.uleb128 0x3
 3534 01f4 0E       		.uleb128 0xe
 3535 01f5 3A       		.uleb128 0x3a
 3536 01f6 0B       		.uleb128 0xb
 3537 01f7 3B       		.uleb128 0x3b
 3538 01f8 05       		.uleb128 0x5
 3539 01f9 49       		.uleb128 0x49
 3540 01fa 13       		.uleb128 0x13
 3541 01fb 3C       		.uleb128 0x3c
 3542 01fc 19       		.uleb128 0x19
 3543 01fd 01       		.uleb128 0x1
 3544 01fe 13       		.uleb128 0x13
GAS LISTING /tmp/cce2OND8.s 			page 68


 3545 01ff 00       		.byte	0
 3546 0200 00       		.byte	0
 3547 0201 28       		.uleb128 0x28
 3548 0202 18       		.uleb128 0x18
 3549 0203 00       		.byte	0
 3550 0204 00       		.byte	0
 3551 0205 00       		.byte	0
 3552 0206 29       		.uleb128 0x29
 3553 0207 2E       		.uleb128 0x2e
 3554 0208 00       		.byte	0
 3555 0209 3F       		.uleb128 0x3f
 3556 020a 19       		.uleb128 0x19
 3557 020b 03       		.uleb128 0x3
 3558 020c 0E       		.uleb128 0xe
 3559 020d 3A       		.uleb128 0x3a
 3560 020e 0B       		.uleb128 0xb
 3561 020f 3B       		.uleb128 0x3b
 3562 0210 05       		.uleb128 0x5
 3563 0211 49       		.uleb128 0x49
 3564 0212 13       		.uleb128 0x13
 3565 0213 3C       		.uleb128 0x3c
 3566 0214 19       		.uleb128 0x19
 3567 0215 00       		.byte	0
 3568 0216 00       		.byte	0
 3569 0217 2A       		.uleb128 0x2a
 3570 0218 2E       		.uleb128 0x2e
 3571 0219 01       		.byte	0x1
 3572 021a 3F       		.uleb128 0x3f
 3573 021b 19       		.uleb128 0x19
 3574 021c 03       		.uleb128 0x3
 3575 021d 0E       		.uleb128 0xe
 3576 021e 3A       		.uleb128 0x3a
 3577 021f 0B       		.uleb128 0xb
 3578 0220 3B       		.uleb128 0x3b
 3579 0221 0B       		.uleb128 0xb
 3580 0222 49       		.uleb128 0x49
 3581 0223 13       		.uleb128 0x13
 3582 0224 3C       		.uleb128 0x3c
 3583 0225 19       		.uleb128 0x19
 3584 0226 01       		.uleb128 0x1
 3585 0227 13       		.uleb128 0x13
 3586 0228 00       		.byte	0
 3587 0229 00       		.byte	0
 3588 022a 2B       		.uleb128 0x2b
 3589 022b 13       		.uleb128 0x13
 3590 022c 01       		.byte	0x1
 3591 022d 03       		.uleb128 0x3
 3592 022e 08       		.uleb128 0x8
 3593 022f 0B       		.uleb128 0xb
 3594 0230 0B       		.uleb128 0xb
 3595 0231 3A       		.uleb128 0x3a
 3596 0232 0B       		.uleb128 0xb
 3597 0233 3B       		.uleb128 0x3b
 3598 0234 0B       		.uleb128 0xb
 3599 0235 01       		.uleb128 0x1
 3600 0236 13       		.uleb128 0x13
 3601 0237 00       		.byte	0
GAS LISTING /tmp/cce2OND8.s 			page 69


 3602 0238 00       		.byte	0
 3603 0239 2C       		.uleb128 0x2c
 3604 023a 10       		.uleb128 0x10
 3605 023b 00       		.byte	0
 3606 023c 0B       		.uleb128 0xb
 3607 023d 0B       		.uleb128 0xb
 3608 023e 49       		.uleb128 0x49
 3609 023f 13       		.uleb128 0x13
 3610 0240 00       		.byte	0
 3611 0241 00       		.byte	0
 3612 0242 2D       		.uleb128 0x2d
 3613 0243 2E       		.uleb128 0x2e
 3614 0244 00       		.byte	0
 3615 0245 3F       		.uleb128 0x3f
 3616 0246 19       		.uleb128 0x19
 3617 0247 03       		.uleb128 0x3
 3618 0248 0E       		.uleb128 0xe
 3619 0249 3A       		.uleb128 0x3a
 3620 024a 0B       		.uleb128 0xb
 3621 024b 3B       		.uleb128 0x3b
 3622 024c 0B       		.uleb128 0xb
 3623 024d 49       		.uleb128 0x49
 3624 024e 13       		.uleb128 0x13
 3625 024f 3C       		.uleb128 0x3c
 3626 0250 19       		.uleb128 0x19
 3627 0251 00       		.byte	0
 3628 0252 00       		.byte	0
 3629 0253 2E       		.uleb128 0x2e
 3630 0254 16       		.uleb128 0x16
 3631 0255 00       		.byte	0
 3632 0256 03       		.uleb128 0x3
 3633 0257 0E       		.uleb128 0xe
 3634 0258 3A       		.uleb128 0x3a
 3635 0259 0B       		.uleb128 0xb
 3636 025a 3B       		.uleb128 0x3b
 3637 025b 0B       		.uleb128 0xb
 3638 025c 00       		.byte	0
 3639 025d 00       		.byte	0
 3640 025e 2F       		.uleb128 0x2f
 3641 025f 2E       		.uleb128 0x2e
 3642 0260 01       		.byte	0x1
 3643 0261 3F       		.uleb128 0x3f
 3644 0262 19       		.uleb128 0x19
 3645 0263 03       		.uleb128 0x3
 3646 0264 0E       		.uleb128 0xe
 3647 0265 3A       		.uleb128 0x3a
 3648 0266 0B       		.uleb128 0xb
 3649 0267 3B       		.uleb128 0x3b
 3650 0268 05       		.uleb128 0x5
 3651 0269 3C       		.uleb128 0x3c
 3652 026a 19       		.uleb128 0x19
 3653 026b 01       		.uleb128 0x1
 3654 026c 13       		.uleb128 0x13
 3655 026d 00       		.byte	0
 3656 026e 00       		.byte	0
 3657 026f 30       		.uleb128 0x30
 3658 0270 2E       		.uleb128 0x2e
GAS LISTING /tmp/cce2OND8.s 			page 70


 3659 0271 01       		.byte	0x1
 3660 0272 47       		.uleb128 0x47
 3661 0273 13       		.uleb128 0x13
 3662 0274 11       		.uleb128 0x11
 3663 0275 01       		.uleb128 0x1
 3664 0276 12       		.uleb128 0x12
 3665 0277 07       		.uleb128 0x7
 3666 0278 40       		.uleb128 0x40
 3667 0279 18       		.uleb128 0x18
 3668 027a 9742     		.uleb128 0x2117
 3669 027c 19       		.uleb128 0x19
 3670 027d 01       		.uleb128 0x1
 3671 027e 13       		.uleb128 0x13
 3672 027f 00       		.byte	0
 3673 0280 00       		.byte	0
 3674 0281 31       		.uleb128 0x31
 3675 0282 05       		.uleb128 0x5
 3676 0283 00       		.byte	0
 3677 0284 03       		.uleb128 0x3
 3678 0285 08       		.uleb128 0x8
 3679 0286 3A       		.uleb128 0x3a
 3680 0287 0B       		.uleb128 0xb
 3681 0288 3B       		.uleb128 0x3b
 3682 0289 0B       		.uleb128 0xb
 3683 028a 49       		.uleb128 0x49
 3684 028b 13       		.uleb128 0x13
 3685 028c 02       		.uleb128 0x2
 3686 028d 18       		.uleb128 0x18
 3687 028e 00       		.byte	0
 3688 028f 00       		.byte	0
 3689 0290 32       		.uleb128 0x32
 3690 0291 2E       		.uleb128 0x2e
 3691 0292 01       		.byte	0x1
 3692 0293 3F       		.uleb128 0x3f
 3693 0294 19       		.uleb128 0x19
 3694 0295 03       		.uleb128 0x3
 3695 0296 0E       		.uleb128 0xe
 3696 0297 3A       		.uleb128 0x3a
 3697 0298 0B       		.uleb128 0xb
 3698 0299 3B       		.uleb128 0x3b
 3699 029a 0B       		.uleb128 0xb
 3700 029b 49       		.uleb128 0x49
 3701 029c 13       		.uleb128 0x13
 3702 029d 11       		.uleb128 0x11
 3703 029e 01       		.uleb128 0x1
 3704 029f 12       		.uleb128 0x12
 3705 02a0 07       		.uleb128 0x7
 3706 02a1 40       		.uleb128 0x40
 3707 02a2 18       		.uleb128 0x18
 3708 02a3 9642     		.uleb128 0x2116
 3709 02a5 19       		.uleb128 0x19
 3710 02a6 01       		.uleb128 0x1
 3711 02a7 13       		.uleb128 0x13
 3712 02a8 00       		.byte	0
 3713 02a9 00       		.byte	0
 3714 02aa 33       		.uleb128 0x33
 3715 02ab 34       		.uleb128 0x34
GAS LISTING /tmp/cce2OND8.s 			page 71


 3716 02ac 00       		.byte	0
 3717 02ad 03       		.uleb128 0x3
 3718 02ae 0E       		.uleb128 0xe
 3719 02af 3A       		.uleb128 0x3a
 3720 02b0 0B       		.uleb128 0xb
 3721 02b1 3B       		.uleb128 0x3b
 3722 02b2 0B       		.uleb128 0xb
 3723 02b3 49       		.uleb128 0x49
 3724 02b4 13       		.uleb128 0x13
 3725 02b5 02       		.uleb128 0x2
 3726 02b6 18       		.uleb128 0x18
 3727 02b7 00       		.byte	0
 3728 02b8 00       		.byte	0
 3729 02b9 34       		.uleb128 0x34
 3730 02ba 0B       		.uleb128 0xb
 3731 02bb 01       		.byte	0x1
 3732 02bc 11       		.uleb128 0x11
 3733 02bd 01       		.uleb128 0x1
 3734 02be 12       		.uleb128 0x12
 3735 02bf 07       		.uleb128 0x7
 3736 02c0 01       		.uleb128 0x1
 3737 02c1 13       		.uleb128 0x13
 3738 02c2 00       		.byte	0
 3739 02c3 00       		.byte	0
 3740 02c4 35       		.uleb128 0x35
 3741 02c5 34       		.uleb128 0x34
 3742 02c6 00       		.byte	0
 3743 02c7 03       		.uleb128 0x3
 3744 02c8 08       		.uleb128 0x8
 3745 02c9 3A       		.uleb128 0x3a
 3746 02ca 0B       		.uleb128 0xb
 3747 02cb 3B       		.uleb128 0x3b
 3748 02cc 0B       		.uleb128 0xb
 3749 02cd 49       		.uleb128 0x49
 3750 02ce 13       		.uleb128 0x13
 3751 02cf 02       		.uleb128 0x2
 3752 02d0 18       		.uleb128 0x18
 3753 02d1 00       		.byte	0
 3754 02d2 00       		.byte	0
 3755 02d3 36       		.uleb128 0x36
 3756 02d4 0B       		.uleb128 0xb
 3757 02d5 01       		.byte	0x1
 3758 02d6 11       		.uleb128 0x11
 3759 02d7 01       		.uleb128 0x1
 3760 02d8 12       		.uleb128 0x12
 3761 02d9 07       		.uleb128 0x7
 3762 02da 00       		.byte	0
 3763 02db 00       		.byte	0
 3764 02dc 37       		.uleb128 0x37
 3765 02dd 34       		.uleb128 0x34
 3766 02de 00       		.byte	0
 3767 02df 47       		.uleb128 0x47
 3768 02e0 13       		.uleb128 0x13
 3769 02e1 6E       		.uleb128 0x6e
 3770 02e2 0E       		.uleb128 0xe
 3771 02e3 1C       		.uleb128 0x1c
 3772 02e4 0D       		.uleb128 0xd
GAS LISTING /tmp/cce2OND8.s 			page 72


 3773 02e5 00       		.byte	0
 3774 02e6 00       		.byte	0
 3775 02e7 38       		.uleb128 0x38
 3776 02e8 34       		.uleb128 0x34
 3777 02e9 00       		.byte	0
 3778 02ea 47       		.uleb128 0x47
 3779 02eb 13       		.uleb128 0x13
 3780 02ec 6E       		.uleb128 0x6e
 3781 02ed 0E       		.uleb128 0xe
 3782 02ee 1C       		.uleb128 0x1c
 3783 02ef 06       		.uleb128 0x6
 3784 02f0 00       		.byte	0
 3785 02f1 00       		.byte	0
 3786 02f2 39       		.uleb128 0x39
 3787 02f3 34       		.uleb128 0x34
 3788 02f4 00       		.byte	0
 3789 02f5 47       		.uleb128 0x47
 3790 02f6 13       		.uleb128 0x13
 3791 02f7 6E       		.uleb128 0x6e
 3792 02f8 0E       		.uleb128 0xe
 3793 02f9 1C       		.uleb128 0x1c
 3794 02fa 0B       		.uleb128 0xb
 3795 02fb 00       		.byte	0
 3796 02fc 00       		.byte	0
 3797 02fd 3A       		.uleb128 0x3a
 3798 02fe 34       		.uleb128 0x34
 3799 02ff 00       		.byte	0
 3800 0300 47       		.uleb128 0x47
 3801 0301 13       		.uleb128 0x13
 3802 0302 6E       		.uleb128 0x6e
 3803 0303 0E       		.uleb128 0xe
 3804 0304 1C       		.uleb128 0x1c
 3805 0305 05       		.uleb128 0x5
 3806 0306 00       		.byte	0
 3807 0307 00       		.byte	0
 3808 0308 3B       		.uleb128 0x3b
 3809 0309 34       		.uleb128 0x34
 3810 030a 00       		.byte	0
 3811 030b 47       		.uleb128 0x47
 3812 030c 13       		.uleb128 0x13
 3813 030d 6E       		.uleb128 0x6e
 3814 030e 0E       		.uleb128 0xe
 3815 030f 1C       		.uleb128 0x1c
 3816 0310 07       		.uleb128 0x7
 3817 0311 00       		.byte	0
 3818 0312 00       		.byte	0
 3819 0313 00       		.byte	0
 3820              		.section	.debug_aranges,"",@progbits
 3821 0000 3C000000 		.long	0x3c
 3822 0004 0200     		.value	0x2
 3823 0006 00000000 		.long	.Ldebug_info0
 3824 000a 08       		.byte	0x8
 3825 000b 00       		.byte	0
 3826 000c 0000     		.value	0
 3827 000e 0000     		.value	0
 3828 0010 00000000 		.quad	.Ltext0
 3828      00000000 
GAS LISTING /tmp/cce2OND8.s 			page 73


 3829 0018 C5010000 		.quad	.Letext0-.Ltext0
 3829      00000000 
 3830 0020 00000000 		.quad	.LFB644
 3830      00000000 
 3831 0028 12000000 		.quad	.LFE644-.LFB644
 3831      00000000 
 3832 0030 00000000 		.quad	0
 3832      00000000 
 3833 0038 00000000 		.quad	0
 3833      00000000 
 3834              		.section	.debug_ranges,"",@progbits
 3835              	.Ldebug_ranges0:
 3836 0000 00000000 		.quad	.Ltext0
 3836      00000000 
 3837 0008 00000000 		.quad	.Letext0
 3837      00000000 
 3838 0010 00000000 		.quad	.LFB644
 3838      00000000 
 3839 0018 00000000 		.quad	.LFE644
 3839      00000000 
 3840 0020 00000000 		.quad	0
 3840      00000000 
 3841 0028 00000000 		.quad	0
 3841      00000000 
 3842              		.section	.debug_line,"",@progbits
 3843              	.Ldebug_line0:
 3844 0000 9E020000 		.section	.debug_str,"MS",@progbits,1
 3844      02001802 
 3844      00000101 
 3844      FB0E0D00 
 3844      01010101 
 3845              	.LASF233:
 3846 0000 66676574 		.string	"fgetc"
 3846      6300
 3847              	.LASF25:
 3848 0006 73697A65 		.string	"size_t"
 3848      5F7400
 3849              	.LASF88:
 3850 000d 73697A65 		.string	"sizetype"
 3850      74797065 
 3850      00
 3851              	.LASF134:
 3852 0016 746D5F68 		.string	"tm_hour"
 3852      6F757200 
 3853              	.LASF44:
 3854 001e 5F5F6973 		.string	"__is_signed"
 3854      5F736967 
 3854      6E656400 
 3855              	.LASF35:
 3856 002a 5F535F69 		.string	"_S_ios_openmode_min"
 3856      6F735F6F 
 3856      70656E6D 
 3856      6F64655F 
 3856      6D696E00 
 3857              	.LASF41:
 3858 003e 5F5F6E75 		.string	"__numeric_traits_integer<int>"
 3858      6D657269 
GAS LISTING /tmp/cce2OND8.s 			page 74


 3858      635F7472 
 3858      61697473 
 3858      5F696E74 
 3859              	.LASF228:
 3860 005c 66706F73 		.string	"fpos_t"
 3860      5F7400
 3861              	.LASF258:
 3862 0063 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIiE5__maxE"
 3862      5F5F676E 
 3862      755F6378 
 3862      7832345F 
 3862      5F6E756D 
 3863              	.LASF64:
 3864 0095 5F494F5F 		.string	"_IO_save_end"
 3864      73617665 
 3864      5F656E64 
 3864      00
 3865              	.LASF43:
 3866 00a2 5F5F6D61 		.string	"__max"
 3866      7800
 3867              	.LASF130:
 3868 00a8 77637363 		.string	"wcscspn"
 3868      73706E00 
 3869              	.LASF209:
 3870 00b0 6C6F6361 		.string	"localeconv"
 3870      6C65636F 
 3870      6E7600
 3871              	.LASF220:
 3872 00bb 395F475F 		.string	"9_G_fpos_t"
 3872      66706F73 
 3872      5F7400
 3873              	.LASF194:
 3874 00c6 66726163 		.string	"frac_digits"
 3874      5F646967 
 3874      69747300 
 3875              	.LASF57:
 3876 00d2 5F494F5F 		.string	"_IO_write_base"
 3876      77726974 
 3876      655F6261 
 3876      736500
 3877              	.LASF73:
 3878 00e1 5F6C6F63 		.string	"_lock"
 3878      6B00
 3879              	.LASF186:
 3880 00e7 696E745F 		.string	"int_curr_symbol"
 3880      63757272 
 3880      5F73796D 
 3880      626F6C00 
 3881              	.LASF165:
 3882 00f7 77637363 		.string	"wcschr"
 3882      687200
 3883              	.LASF197:
 3884 00fe 6E5F6373 		.string	"n_cs_precedes"
 3884      5F707265 
 3884      63656465 
 3884      7300
 3885              	.LASF62:
GAS LISTING /tmp/cce2OND8.s 			page 75


 3886 010c 5F494F5F 		.string	"_IO_save_base"
 3886      73617665 
 3886      5F626173 
 3886      6500
 3887              	.LASF111:
 3888 011a 6D627274 		.string	"mbrtowc"
 3888      6F776300 
 3889              	.LASF157:
 3890 0122 77637378 		.string	"wcsxfrm"
 3890      66726D00 
 3891              	.LASF193:
 3892 012a 696E745F 		.string	"int_frac_digits"
 3892      66726163 
 3892      5F646967 
 3892      69747300 
 3893              	.LASF234:
 3894 013a 66676574 		.string	"fgetpos"
 3894      706F7300 
 3895              	.LASF221:
 3896 0142 5F5F706F 		.string	"__pos"
 3896      7300
 3897              	.LASF66:
 3898 0148 5F636861 		.string	"_chain"
 3898      696E00
 3899              	.LASF128:
 3900 014f 77637363 		.string	"wcscoll"
 3900      6F6C6C00 
 3901              	.LASF245:
 3902 0157 636C6561 		.string	"clearerr"
 3902      72657272 
 3902      00
 3903              	.LASF70:
 3904 0160 5F637572 		.string	"_cur_column"
 3904      5F636F6C 
 3904      756D6E00 
 3905              	.LASF191:
 3906 016c 706F7369 		.string	"positive_sign"
 3906      74697665 
 3906      5F736967 
 3906      6E00
 3907              	.LASF92:
 3908 017a 5F5F7763 		.string	"__wch"
 3908      6800
 3909              	.LASF13:
 3910 0180 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4moveEPcPKcm"
 3910      74313163 
 3910      6861725F 
 3910      74726169 
 3910      74734963 
 3911              	.LASF21:
 3912 01a2 5F5A4E53 		.string	"_ZNSt11char_traitsIcE11to_int_typeERKc"
 3912      74313163 
 3912      6861725F 
 3912      74726169 
 3912      74734963 
 3913              	.LASF188:
 3914 01c9 6D6F6E5F 		.string	"mon_decimal_point"
GAS LISTING /tmp/cce2OND8.s 			page 76


 3914      64656369 
 3914      6D616C5F 
 3914      706F696E 
 3914      7400
 3915              	.LASF155:
 3916 01db 6C6F6E67 		.string	"long int"
 3916      20696E74 
 3916      00
 3917              	.LASF140:
 3918 01e4 746D5F69 		.string	"tm_isdst"
 3918      73647374 
 3918      00
 3919              	.LASF49:
 3920 01ed 5F5F6E75 		.string	"__numeric_traits_integer<char>"
 3920      6D657269 
 3920      635F7472 
 3920      61697473 
 3920      5F696E74 
 3921              	.LASF123:
 3922 020c 76777072 		.string	"vwprintf"
 3922      696E7466 
 3922      00
 3923              	.LASF271:
 3924 0215 5F496F73 		.string	"_Ios_Openmode"
 3924      5F4F7065 
 3924      6E6D6F64 
 3924      6500
 3925              	.LASF3:
 3926 0223 696E745F 		.string	"int_type"
 3926      74797065 
 3926      00
 3927              	.LASF224:
 3928 022c 5F494F5F 		.string	"_IO_marker"
 3928      6D61726B 
 3928      657200
 3929              	.LASF276:
 3930 0237 6D61696E 		.string	"main"
 3930      00
 3931              	.LASF203:
 3932 023c 696E745F 		.string	"int_n_cs_precedes"
 3932      6E5F6373 
 3932      5F707265 
 3932      63656465 
 3932      7300
 3933              	.LASF216:
 3934 024e 746F7763 		.string	"towctrans"
 3934      7472616E 
 3934      7300
 3935              	.LASF14:
 3936 0258 636F7079 		.string	"copy"
 3936      00
 3937              	.LASF5:
 3938 025d 5F5A4E53 		.string	"_ZNSt11char_traitsIcE2ltERKcS2_"
 3938      74313163 
 3938      6861725F 
 3938      74726169 
 3938      74734963 
GAS LISTING /tmp/cce2OND8.s 			page 77


 3939              	.LASF37:
 3940 027d 6F667374 		.string	"ofstream"
 3940      7265616D 
 3940      00
 3941              	.LASF47:
 3942 0286 5F56616C 		.string	"_Value"
 3942      756500
 3943              	.LASF256:
 3944 028d 73717561 		.string	"square_file2"
 3944      72655F66 
 3944      696C6532 
 3944      00
 3945              	.LASF139:
 3946 029a 746D5F79 		.string	"tm_yday"
 3946      64617900 
 3947              	.LASF178:
 3948 02a2 7369676E 		.string	"signed char"
 3948      65642063 
 3948      68617200 
 3949              	.LASF52:
 3950 02ae 5F494F5F 		.string	"_IO_FILE"
 3950      46494C45 
 3950      00
 3951              	.LASF247:
 3952 02b7 72656D6F 		.string	"remove"
 3952      766500
 3953              	.LASF95:
 3954 02be 5F5F7661 		.string	"__value"
 3954      6C756500 
 3955              	.LASF213:
 3956 02c6 77637479 		.string	"wctype_t"
 3956      70655F74 
 3956      00
 3957              	.LASF101:
 3958 02cf 66676574 		.string	"fgetwc"
 3958      776300
 3959              	.LASF208:
 3960 02d6 67657477 		.string	"getwchar"
 3960      63686172 
 3960      00
 3961              	.LASF102:
 3962 02df 66676574 		.string	"fgetws"
 3962      777300
 3963              	.LASF2:
 3964 02e6 63686172 		.string	"char_type"
 3964      5F747970 
 3964      6500
 3965              	.LASF177:
 3966 02f0 756E7369 		.string	"unsigned char"
 3966      676E6564 
 3966      20636861 
 3966      7200
 3967              	.LASF198:
 3968 02fe 6E5F7365 		.string	"n_sep_by_space"
 3968      705F6279 
 3968      5F737061 
 3968      636500
GAS LISTING /tmp/cce2OND8.s 			page 78


 3969              	.LASF229:
 3970 030d 66636C6F 		.string	"fclose"
 3970      736500
 3971              	.LASF170:
 3972 0314 776D656D 		.string	"wmemchr"
 3972      63687200 
 3973              	.LASF262:
 3974 031c 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIsE5__maxE"
 3974      5F5F676E 
 3974      755F6378 
 3974      7832345F 
 3974      5F6E756D 
 3975              	.LASF29:
 3976 034e 5F535F62 		.string	"_S_bin"
 3976      696E00
 3977              	.LASF36:
 3978 0355 6F706572 		.string	"operator|"
 3978      61746F72 
 3978      7C00
 3979              	.LASF24:
 3980 035f 6E6F745F 		.string	"not_eof"
 3980      656F6600 
 3981              	.LASF116:
 3982 0367 73777072 		.string	"swprintf"
 3982      696E7466 
 3982      00
 3983              	.LASF167:
 3984 0370 77637370 		.string	"wcspbrk"
 3984      62726B00 
 3985              	.LASF31:
 3986 0378 5F535F6F 		.string	"_S_out"
 3986      757400
 3987              	.LASF96:
 3988 037f 63686172 		.string	"char"
 3988      00
 3989              	.LASF27:
 3990 0384 5F535F61 		.string	"_S_app"
 3990      707000
 3991              	.LASF98:
 3992 038b 6D627374 		.string	"mbstate_t"
 3992      6174655F 
 3992      7400
 3993              	.LASF218:
 3994 0395 77637479 		.string	"wctype"
 3994      706500
 3995              	.LASF145:
 3996 039c 7763736E 		.string	"wcsncmp"
 3996      636D7000 
 3997              	.LASF275:
 3998 03a4 5F494F5F 		.string	"_IO_lock_t"
 3998      6C6F636B 
 3998      5F7400
 3999              	.LASF206:
 4000 03af 696E745F 		.string	"int_n_sign_posn"
 4000      6E5F7369 
 4000      676E5F70 
 4000      6F736E00 
GAS LISTING /tmp/cce2OND8.s 			page 79


 4001              	.LASF200:
 4002 03bf 6E5F7369 		.string	"n_sign_posn"
 4002      676E5F70 
 4002      6F736E00 
 4003              	.LASF161:
 4004 03cb 776D656D 		.string	"wmemmove"
 4004      6D6F7665 
 4004      00
 4005              	.LASF242:
 4006 03d4 67657463 		.string	"getc"
 4006      00
 4007              	.LASF42:
 4008 03d9 5F5F6D69 		.string	"__min"
 4008      6E00
 4009              	.LASF100:
 4010 03df 62746F77 		.string	"btowc"
 4010      6300
 4011              	.LASF244:
 4012 03e5 67657473 		.string	"gets"
 4012      00
 4013              	.LASF54:
 4014 03ea 5F494F5F 		.string	"_IO_read_ptr"
 4014      72656164 
 4014      5F707472 
 4014      00
 4015              	.LASF164:
 4016 03f7 77736361 		.string	"wscanf"
 4016      6E6600
 4017              	.LASF189:
 4018 03fe 6D6F6E5F 		.string	"mon_thousands_sep"
 4018      74686F75 
 4018      73616E64 
 4018      735F7365 
 4018      7000
 4019              	.LASF118:
 4020 0410 756E6765 		.string	"ungetwc"
 4020      74776300 
 4021              	.LASF85:
 4022 0418 66705F6F 		.string	"fp_offset"
 4022      66667365 
 4022      7400
 4023              	.LASF241:
 4024 0422 6674656C 		.string	"ftell"
 4024      6C00
 4025              	.LASF26:
 4026 0428 70747264 		.string	"ptrdiff_t"
 4026      6966665F 
 4026      7400
 4027              	.LASF257:
 4028 0432 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIiE5__minE"
 4028      5F5F676E 
 4028      755F6378 
 4028      7832345F 
 4028      5F6E756D 
 4029              	.LASF214:
 4030 0464 77637472 		.string	"wctrans_t"
 4030      616E735F 
GAS LISTING /tmp/cce2OND8.s 			page 80


 4030      7400
 4031              	.LASF110:
 4032 046e 6D62726C 		.string	"mbrlen"
 4032      656E00
 4033              	.LASF227:
 4034 0475 5F706F73 		.string	"_pos"
 4034      00
 4035              	.LASF192:
 4036 047a 6E656761 		.string	"negative_sign"
 4036      74697665 
 4036      5F736967 
 4036      6E00
 4037              	.LASF201:
 4038 0488 696E745F 		.string	"int_p_cs_precedes"
 4038      705F6373 
 4038      5F707265 
 4038      63656465 
 4038      7300
 4039              	.LASF107:
 4040 049a 66777072 		.string	"fwprintf"
 4040      696E7466 
 4040      00
 4041              	.LASF65:
 4042 04a3 5F6D6172 		.string	"_markers"
 4042      6B657273 
 4042      00
 4043              	.LASF175:
 4044 04ac 77637374 		.string	"wcstoull"
 4044      6F756C6C 
 4044      00
 4045              	.LASF6:
 4046 04b5 636F6D70 		.string	"compare"
 4046      61726500 
 4047              	.LASF223:
 4048 04bd 5F475F66 		.string	"_G_fpos_t"
 4048      706F735F 
 4048      7400
 4049              	.LASF129:
 4050 04c7 77637363 		.string	"wcscpy"
 4050      707900
 4051              	.LASF46:
 4052 04ce 5F436861 		.string	"_CharT"
 4052      725400
 4053              	.LASF121:
 4054 04d5 76737770 		.string	"vswprintf"
 4054      72696E74 
 4054      6600
 4055              	.LASF162:
 4056 04df 776D656D 		.string	"wmemset"
 4056      73657400 
 4057              	.LASF106:
 4058 04e7 66776964 		.string	"fwide"
 4058      6500
 4059              	.LASF132:
 4060 04ed 746D5F73 		.string	"tm_sec"
 4060      656300
 4061              	.LASF74:
GAS LISTING /tmp/cce2OND8.s 			page 81


 4062 04f4 5F6F6666 		.string	"_offset"
 4062      73657400 
 4063              	.LASF146:
 4064 04fc 7763736E 		.string	"wcsncpy"
 4064      63707900 
 4065              	.LASF166:
 4066 0504 5F5A5374 		.string	"_ZStorSt13_Ios_OpenmodeS_"
 4066      6F725374 
 4066      31335F49 
 4066      6F735F4F 
 4066      70656E6D 
 4067              	.LASF115:
 4068 051e 70757477 		.string	"putwchar"
 4068      63686172 
 4068      00
 4069              	.LASF159:
 4070 0527 776D656D 		.string	"wmemcmp"
 4070      636D7000 
 4071              	.LASF28:
 4072 052f 5F535F61 		.string	"_S_ate"
 4072      746500
 4073              	.LASF15:
 4074 0536 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4copyEPcPKcm"
 4074      74313163 
 4074      6861725F 
 4074      74726169 
 4074      74734963 
 4075              	.LASF10:
 4076 0558 66696E64 		.string	"find"
 4076      00
 4077              	.LASF237:
 4078 055d 66726561 		.string	"fread"
 4078      6400
 4079              	.LASF12:
 4080 0563 6D6F7665 		.string	"move"
 4080      00
 4081              	.LASF90:
 4082 0568 6C6F6E67 		.string	"long unsigned int"
 4082      20756E73 
 4082      69676E65 
 4082      6420696E 
 4082      7400
 4083              	.LASF30:
 4084 057a 5F535F69 		.string	"_S_in"
 4084      6E00
 4085              	.LASF68:
 4086 0580 5F666C61 		.string	"_flags2"
 4086      67733200 
 4087              	.LASF180:
 4088 0588 5F5F676E 		.string	"__gnu_debug"
 4088      755F6465 
 4088      62756700 
 4089              	.LASF56:
 4090 0594 5F494F5F 		.string	"_IO_read_base"
 4090      72656164 
 4090      5F626173 
 4090      6500
GAS LISTING /tmp/cce2OND8.s 			page 82


 4091              	.LASF231:
 4092 05a2 66657272 		.string	"ferror"
 4092      6F7200
 4093              	.LASF267:
 4094 05a9 2F686F6D 		.string	"/home/vinu/cs4230/par-lang/c++/discover_modern_c++/basics"
 4094      652F7669 
 4094      6E752F63 
 4094      73343233 
 4094      302F7061 
 4095              	.LASF119:
 4096 05e3 76667770 		.string	"vfwprintf"
 4096      72696E74 
 4096      6600
 4097              	.LASF81:
 4098 05ed 5F756E75 		.string	"_unused2"
 4098      73656432 
 4098      00
 4099              	.LASF196:
 4100 05f6 705F7365 		.string	"p_sep_by_space"
 4100      705F6279 
 4100      5F737061 
 4100      636500
 4101              	.LASF22:
 4102 0605 65715F69 		.string	"eq_int_type"
 4102      6E745F74 
 4102      79706500 
 4103              	.LASF19:
 4104 0611 5F5A4E53 		.string	"_ZNSt11char_traitsIcE12to_char_typeERKi"
 4104      74313163 
 4104      6861725F 
 4104      74726169 
 4104      74734963 
 4105              	.LASF240:
 4106 0639 66736574 		.string	"fsetpos"
 4106      706F7300 
 4107              	.LASF32:
 4108 0641 5F535F74 		.string	"_S_trunc"
 4108      72756E63 
 4108      00
 4109              	.LASF204:
 4110 064a 696E745F 		.string	"int_n_sep_by_space"
 4110      6E5F7365 
 4110      705F6279 
 4110      5F737061 
 4110      636500
 4111              	.LASF174:
 4112 065d 6C6F6E67 		.string	"long long int"
 4112      206C6F6E 
 4112      6720696E 
 4112      7400
 4113              	.LASF127:
 4114 066b 77637363 		.string	"wcscmp"
 4114      6D7000
 4115              	.LASF97:
 4116 0672 5F5F6D62 		.string	"__mbstate_t"
 4116      73746174 
 4116      655F7400 
GAS LISTING /tmp/cce2OND8.s 			page 83


 4117              	.LASF160:
 4118 067e 776D656D 		.string	"wmemcpy"
 4118      63707900 
 4119              	.LASF136:
 4120 0686 746D5F6D 		.string	"tm_mon"
 4120      6F6E00
 4121              	.LASF154:
 4122 068d 77637374 		.string	"wcstol"
 4122      6F6C00
 4123              	.LASF150:
 4124 0694 646F7562 		.string	"double"
 4124      6C6500
 4125              	.LASF11:
 4126 069b 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4findEPKcmRS1_"
 4126      74313163 
 4126      6861725F 
 4126      74726169 
 4126      74734963 
 4127              	.LASF59:
 4128 06bf 5F494F5F 		.string	"_IO_write_end"
 4128      77726974 
 4128      655F656E 
 4128      6400
 4129              	.LASF261:
 4130 06cd 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIsE5__minE"
 4130      5F5F676E 
 4130      755F6378 
 4130      7832345F 
 4130      5F6E756D 
 4131              	.LASF158:
 4132 06ff 7763746F 		.string	"wctob"
 4132      6200
 4133              	.LASF84:
 4134 0705 67705F6F 		.string	"gp_offset"
 4134      66667365 
 4134      7400
 4135              	.LASF259:
 4136 070f 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerImE8__digitsE"
 4136      5F5F676E 
 4136      755F6378 
 4136      7832345F 
 4136      5F6E756D 
 4137              	.LASF152:
 4138 0744 666C6F61 		.string	"float"
 4138      7400
 4139              	.LASF133:
 4140 074a 746D5F6D 		.string	"tm_min"
 4140      696E00
 4141              	.LASF60:
 4142 0751 5F494F5F 		.string	"_IO_buf_base"
 4142      6275665F 
 4142      62617365 
 4142      00
 4143              	.LASF89:
 4144 075e 756E7369 		.string	"unsigned int"
 4144      676E6564 
 4144      20696E74 
GAS LISTING /tmp/cce2OND8.s 			page 84


 4144      00
 4145              	.LASF263:
 4146 076b 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIlE5__minE"
 4146      5F5F676E 
 4146      755F6378 
 4146      7832345F 
 4146      5F6E756D 
 4147              	.LASF40:
 4148 079d 63686172 		.string	"char_traits<char>"
 4148      5F747261 
 4148      6974733C 
 4148      63686172 
 4148      3E00
 4149              	.LASF246:
 4150 07af 70657272 		.string	"perror"
 4150      6F7200
 4151              	.LASF33:
 4152 07b6 5F535F69 		.string	"_S_ios_openmode_end"
 4152      6F735F6F 
 4152      70656E6D 
 4152      6F64655F 
 4152      656E6400 
 4153              	.LASF148:
 4154 07ca 77637373 		.string	"wcsspn"
 4154      706E00
 4155              	.LASF199:
 4156 07d1 705F7369 		.string	"p_sign_posn"
 4156      676E5F70 
 4156      6F736E00 
 4157              	.LASF23:
 4158 07dd 5F5A4E53 		.string	"_ZNSt11char_traitsIcE11eq_int_typeERKiS2_"
 4158      74313163 
 4158      6861725F 
 4158      74726169 
 4158      74734963 
 4159              	.LASF69:
 4160 0807 5F6F6C64 		.string	"_old_offset"
 4160      5F6F6666 
 4160      73657400 
 4161              	.LASF75:
 4162 0813 5F5F7061 		.string	"__pad1"
 4162      643100
 4163              	.LASF76:
 4164 081a 5F5F7061 		.string	"__pad2"
 4164      643200
 4165              	.LASF77:
 4166 0821 5F5F7061 		.string	"__pad3"
 4166      643300
 4167              	.LASF78:
 4168 0828 5F5F7061 		.string	"__pad4"
 4168      643400
 4169              	.LASF79:
 4170 082f 5F5F7061 		.string	"__pad5"
 4170      643500
 4171              	.LASF251:
 4172 0836 73657476 		.string	"setvbuf"
 4172      62756600 
GAS LISTING /tmp/cce2OND8.s 			page 85


 4173              	.LASF226:
 4174 083e 5F736275 		.string	"_sbuf"
 4174      6600
 4175              	.LASF86:
 4176 0844 6F766572 		.string	"overflow_arg_area"
 4176      666C6F77 
 4176      5F617267 
 4176      5F617265 
 4176      6100
 4177              	.LASF248:
 4178 0856 72656E61 		.string	"rename"
 4178      6D6500
 4179              	.LASF53:
 4180 085d 5F666C61 		.string	"_flags"
 4180      677300
 4181              	.LASF80:
 4182 0864 5F6D6F64 		.string	"_mode"
 4182      6500
 4183              	.LASF183:
 4184 086a 64656369 		.string	"decimal_point"
 4184      6D616C5F 
 4184      706F696E 
 4184      7400
 4185              	.LASF243:
 4186 0878 67657463 		.string	"getchar"
 4186      68617200 
 4187              	.LASF94:
 4188 0880 5F5F636F 		.string	"__count"
 4188      756E7400 
 4189              	.LASF38:
 4190 0888 5F5F676E 		.string	"__gnu_cxx"
 4190      755F6378 
 4190      7800
 4191              	.LASF181:
 4192 0892 626F6F6C 		.string	"bool"
 4192      00
 4193              	.LASF265:
 4194 0897 474E5520 		.string	"GNU C++ 5.4.1 20160904 -mtune=generic -march=x86-64 -g -O0 -fstack-protector-strong"
 4194      432B2B20 
 4194      352E342E 
 4194      31203230 
 4194      31363039 
 4195              	.LASF230:
 4196 08eb 66656F66 		.string	"feof"
 4196      00
 4197              	.LASF17:
 4198 08f0 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6assignEPcmc"
 4198      74313163 
 4198      6861725F 
 4198      74726169 
 4198      74734963 
 4199              	.LASF172:
 4200 0912 6C6F6E67 		.string	"long double"
 4200      20646F75 
 4200      626C6500 
 4201              	.LASF114:
 4202 091e 70757477 		.string	"putwc"
GAS LISTING /tmp/cce2OND8.s 			page 86


 4202      6300
 4203              	.LASF51:
 4204 0924 46494C45 		.string	"FILE"
 4204      00
 4205              	.LASF274:
 4206 0929 5F5F6E75 		.string	"__numeric_traits_integer<long int>"
 4206      6D657269 
 4206      635F7472 
 4206      61697473 
 4206      5F696E74 
 4207              	.LASF135:
 4208 094c 746D5F6D 		.string	"tm_mday"
 4208      64617900 
 4209              	.LASF93:
 4210 0954 5F5F7763 		.string	"__wchb"
 4210      686200
 4211              	.LASF8:
 4212 095b 5F5A4E53 		.string	"_ZNSt11char_traitsIcE7compareEPKcS2_m"
 4212      74313163 
 4212      6861725F 
 4212      74726169 
 4212      74734963 
 4213              	.LASF176:
 4214 0981 6C6F6E67 		.string	"long long unsigned int"
 4214      206C6F6E 
 4214      6720756E 
 4214      7369676E 
 4214      65642069 
 4215              	.LASF87:
 4216 0998 7265675F 		.string	"reg_save_area"
 4216      73617665 
 4216      5F617265 
 4216      6100
 4217              	.LASF171:
 4218 09a6 77637374 		.string	"wcstold"
 4218      6F6C6400 
 4219              	.LASF202:
 4220 09ae 696E745F 		.string	"int_p_sep_by_space"
 4220      705F7365 
 4220      705F6279 
 4220      5F737061 
 4220      636500
 4221              	.LASF7:
 4222 09c1 6C656E67 		.string	"length"
 4222      746800
 4223              	.LASF173:
 4224 09c8 77637374 		.string	"wcstoll"
 4224      6F6C6C00 
 4225              	.LASF211:
 4226 09d0 5F5F6F66 		.string	"__off_t"
 4226      665F7400 
 4227              	.LASF169:
 4228 09d8 77637373 		.string	"wcsstr"
 4228      747200
 4229              	.LASF260:
 4230 09df 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIcE5__maxE"
 4230      5F5F676E 
GAS LISTING /tmp/cce2OND8.s 			page 87


 4230      755F6378 
 4230      7832345F 
 4230      5F6E756D 
 4231              	.LASF250:
 4232 0a11 73657462 		.string	"setbuf"
 4232      756600
 4233              	.LASF266:
 4234 0a18 696F2E63 		.string	"io.cpp"
 4234      707000
 4235              	.LASF147:
 4236 0a1f 77637372 		.string	"wcsrtombs"
 4236      746F6D62 
 4236      7300
 4237              	.LASF138:
 4238 0a29 746D5F77 		.string	"tm_wday"
 4238      64617900 
 4239              	.LASF4:
 4240 0a31 5F5A4E53 		.string	"_ZNSt11char_traitsIcE2eqERKcS2_"
 4240      74313163 
 4240      6861725F 
 4240      74726169 
 4240      74734963 
 4241              	.LASF112:
 4242 0a51 6D627369 		.string	"mbsinit"
 4242      6E697400 
 4243              	.LASF117:
 4244 0a59 73777363 		.string	"swscanf"
 4244      616E6600 
 4245              	.LASF45:
 4246 0a61 5F5F6469 		.string	"__digits"
 4246      67697473 
 4246      00
 4247              	.LASF149:
 4248 0a6a 77637374 		.string	"wcstod"
 4248      6F6400
 4249              	.LASF151:
 4250 0a71 77637374 		.string	"wcstof"
 4250      6F6600
 4251              	.LASF153:
 4252 0a78 77637374 		.string	"wcstok"
 4252      6F6B00
 4253              	.LASF0:
 4254 0a7f 5F5F6378 		.string	"__cxx11"
 4254      78313100 
 4255              	.LASF82:
 4256 0a87 5F5F4649 		.string	"__FILE"
 4256      4C4500
 4257              	.LASF63:
 4258 0a8e 5F494F5F 		.string	"_IO_backup_base"
 4258      6261636B 
 4258      75705F62 
 4258      61736500 
 4259              	.LASF207:
 4260 0a9e 7365746C 		.string	"setlocale"
 4260      6F63616C 
 4260      6500
 4261              	.LASF72:
GAS LISTING /tmp/cce2OND8.s 			page 88


 4262 0aa8 5F73686F 		.string	"_shortbuf"
 4262      72746275 
 4262      6600
 4263              	.LASF168:
 4264 0ab2 77637372 		.string	"wcsrchr"
 4264      63687200 
 4265              	.LASF108:
 4266 0aba 66777363 		.string	"fwscanf"
 4266      616E6600 
 4267              	.LASF91:
 4268 0ac2 77696E74 		.string	"wint_t"
 4268      5F7400
 4269              	.LASF34:
 4270 0ac9 5F535F69 		.string	"_S_ios_openmode_max"
 4270      6F735F6F 
 4270      70656E6D 
 4270      6F64655F 
 4270      6D617800 
 4271              	.LASF225:
 4272 0add 5F6E6578 		.string	"_next"
 4272      7400
 4273              	.LASF212:
 4274 0ae3 5F5F6F66 		.string	"__off64_t"
 4274      6636345F 
 4274      7400
 4275              	.LASF236:
 4276 0aed 666F7065 		.string	"fopen"
 4276      6E00
 4277              	.LASF217:
 4278 0af3 77637472 		.string	"wctrans"
 4278      616E7300 
 4279              	.LASF184:
 4280 0afb 74686F75 		.string	"thousands_sep"
 4280      73616E64 
 4280      735F7365 
 4280      7000
 4281              	.LASF249:
 4282 0b09 72657769 		.string	"rewind"
 4282      6E6400
 4283              	.LASF61:
 4284 0b10 5F494F5F 		.string	"_IO_buf_end"
 4284      6275665F 
 4284      656E6400 
 4285              	.LASF143:
 4286 0b1c 7763736C 		.string	"wcslen"
 4286      656E00
 4287              	.LASF20:
 4288 0b23 746F5F69 		.string	"to_int_type"
 4288      6E745F74 
 4288      79706500 
 4289              	.LASF18:
 4290 0b2f 746F5F63 		.string	"to_char_type"
 4290      6861725F 
 4290      74797065 
 4290      00
 4291              	.LASF1:
 4292 0b3c 5F5F6465 		.string	"__debug"
GAS LISTING /tmp/cce2OND8.s 			page 89


 4292      62756700 
 4293              	.LASF141:
 4294 0b44 746D5F67 		.string	"tm_gmtoff"
 4294      6D746F66 
 4294      6600
 4295              	.LASF187:
 4296 0b4e 63757272 		.string	"currency_symbol"
 4296      656E6379 
 4296      5F73796D 
 4296      626F6C00 
 4297              	.LASF179:
 4298 0b5e 73686F72 		.string	"short int"
 4298      7420696E 
 4298      7400
 4299              	.LASF9:
 4300 0b68 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6lengthEPKc"
 4300      74313163 
 4300      6861725F 
 4300      74726169 
 4300      74734963 
 4301              	.LASF131:
 4302 0b89 77637366 		.string	"wcsftime"
 4302      74696D65 
 4302      00
 4303              	.LASF222:
 4304 0b92 5F5F7374 		.string	"__state"
 4304      61746500 
 4305              	.LASF239:
 4306 0b9a 66736565 		.string	"fseek"
 4306      6B00
 4307              	.LASF253:
 4308 0ba0 746D706E 		.string	"tmpnam"
 4308      616D00
 4309              	.LASF255:
 4310 0ba7 73717561 		.string	"square_file"
 4310      72655F66 
 4310      696C6500 
 4311              	.LASF71:
 4312 0bb3 5F767461 		.string	"_vtable_offset"
 4312      626C655F 
 4312      6F666673 
 4312      657400
 4313              	.LASF190:
 4314 0bc2 6D6F6E5F 		.string	"mon_grouping"
 4314      67726F75 
 4314      70696E67 
 4314      00
 4315              	.LASF268:
 4316 0bcf 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6assignERcRKc"
 4316      74313163 
 4316      6861725F 
 4316      74726169 
 4316      74734963 
 4317              	.LASF126:
 4318 0bf2 77637363 		.string	"wcscat"
 4318      617400
 4319              	.LASF252:
GAS LISTING /tmp/cce2OND8.s 			page 90


 4320 0bf9 746D7066 		.string	"tmpfile"
 4320      696C6500 
 4321              	.LASF219:
 4322 0c01 31315F5F 		.string	"11__mbstate_t"
 4322      6D627374 
 4322      6174655F 
 4322      7400
 4323              	.LASF205:
 4324 0c0f 696E745F 		.string	"int_p_sign_posn"
 4324      705F7369 
 4324      676E5F70 
 4324      6F736E00 
 4325              	.LASF142:
 4326 0c1f 746D5F7A 		.string	"tm_zone"
 4326      6F6E6500 
 4327              	.LASF254:
 4328 0c27 756E6765 		.string	"ungetc"
 4328      746300
 4329              	.LASF124:
 4330 0c2e 76777363 		.string	"vwscanf"
 4330      616E6600 
 4331              	.LASF125:
 4332 0c36 77637274 		.string	"wcrtomb"
 4332      6F6D6200 
 4333              	.LASF182:
 4334 0c3e 6C636F6E 		.string	"lconv"
 4334      7600
 4335              	.LASF55:
 4336 0c44 5F494F5F 		.string	"_IO_read_end"
 4336      72656164 
 4336      5F656E64 
 4336      00
 4337              	.LASF269:
 4338 0c51 5F5A4E53 		.string	"_ZNSt11char_traitsIcE3eofEv"
 4338      74313163 
 4338      6861725F 
 4338      74726169 
 4338      74734963 
 4339              	.LASF144:
 4340 0c6d 7763736E 		.string	"wcsncat"
 4340      63617400 
 4341              	.LASF50:
 4342 0c75 5F5F6E75 		.string	"__numeric_traits_integer<short int>"
 4342      6D657269 
 4342      635F7472 
 4342      61697473 
 4342      5F696E74 
 4343              	.LASF104:
 4344 0c99 66707574 		.string	"fputwc"
 4344      776300
 4345              	.LASF67:
 4346 0ca0 5F66696C 		.string	"_fileno"
 4346      656E6F00 
 4347              	.LASF105:
 4348 0ca8 66707574 		.string	"fputws"
 4348      777300
 4349              	.LASF122:
GAS LISTING /tmp/cce2OND8.s 			page 91


 4350 0caf 76737773 		.string	"vswscanf"
 4350      63616E66 
 4350      00
 4351              	.LASF113:
 4352 0cb8 6D627372 		.string	"mbsrtowcs"
 4352      746F7763 
 4352      7300
 4353              	.LASF195:
 4354 0cc2 705F6373 		.string	"p_cs_precedes"
 4354      5F707265 
 4354      63656465 
 4354      7300
 4355              	.LASF48:
 4356 0cd0 5F5F6E75 		.string	"__numeric_traits_integer<long unsigned int>"
 4356      6D657269 
 4356      635F7472 
 4356      61697473 
 4356      5F696E74 
 4357              	.LASF137:
 4358 0cfc 746D5F79 		.string	"tm_year"
 4358      65617200 
 4359              	.LASF99:
 4360 0d04 73686F72 		.string	"short unsigned int"
 4360      7420756E 
 4360      7369676E 
 4360      65642069 
 4360      6E7400
 4361              	.LASF273:
 4362 0d17 5F547261 		.string	"_Traits"
 4362      69747300 
 4363              	.LASF39:
 4364 0d1f 5F5F6F70 		.string	"__ops"
 4364      7300
 4365              	.LASF120:
 4366 0d25 76667773 		.string	"vfwscanf"
 4366      63616E66 
 4366      00
 4367              	.LASF58:
 4368 0d2e 5F494F5F 		.string	"_IO_write_ptr"
 4368      77726974 
 4368      655F7074 
 4368      7200
 4369              	.LASF210:
 4370 0d3c 5F5F696E 		.string	"__int32_t"
 4370      7433325F 
 4370      7400
 4371              	.LASF109:
 4372 0d46 67657477 		.string	"getwc"
 4372      6300
 4373              	.LASF235:
 4374 0d4c 66676574 		.string	"fgets"
 4374      7300
 4375              	.LASF215:
 4376 0d52 69737763 		.string	"iswctype"
 4376      74797065 
 4376      00
 4377              	.LASF16:
GAS LISTING /tmp/cce2OND8.s 			page 92


 4378 0d5b 61737369 		.string	"assign"
 4378      676E00
 4379              	.LASF185:
 4380 0d62 67726F75 		.string	"grouping"
 4380      70696E67 
 4380      00
 4381              	.LASF163:
 4382 0d6b 77707269 		.string	"wprintf"
 4382      6E746600 
 4383              	.LASF264:
 4384 0d73 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIlE5__maxE"
 4384      5F5F676E 
 4384      755F6378 
 4384      7832345F 
 4384      5F6E756D 
 4385              	.LASF270:
 4386 0da5 5F5A4E53 		.string	"_ZNSt11char_traitsIcE7not_eofERKi"
 4386      74313163 
 4386      6861725F 
 4386      74726169 
 4386      74734963 
 4387              	.LASF232:
 4388 0dc7 66666C75 		.string	"fflush"
 4388      736800
 4389              	.LASF103:
 4390 0dce 77636861 		.string	"wchar_t"
 4390      725F7400 
 4391              	.LASF83:
 4392 0dd6 74797065 		.string	"typedef __va_list_tag __va_list_tag"
 4392      64656620 
 4392      5F5F7661 
 4392      5F6C6973 
 4392      745F7461 
 4393              	.LASF156:
 4394 0dfa 77637374 		.string	"wcstoul"
 4394      6F756C00 
 4395              	.LASF272:
 4396 0e02 62617369 		.string	"basic_ofstream<char, std::char_traits<char> >"
 4396      635F6F66 
 4396      73747265 
 4396      616D3C63 
 4396      6861722C 
 4397              	.LASF238:
 4398 0e30 6672656F 		.string	"freopen"
 4398      70656E00 
 4399              		.ident	"GCC: (Ubuntu 5.4.1-2ubuntu1~16.04) 5.4.1 20160904"
 4400              		.section	.note.GNU-stack,"",@progbits
GAS LISTING /tmp/cce2OND8.s 			page 93


DEFINED SYMBOLS
                            *ABS*:0000000000000000 io.cpp
     /tmp/cce2OND8.s:7      .text._ZStorSt13_Ios_OpenmodeS_:0000000000000000 _ZStorSt13_Ios_OpenmodeS_
     /tmp/cce2OND8.s:38     .text:0000000000000000 main

UNDEFINED SYMBOLS
__gxx_personality_v0
_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1Ev
_ZNSt14basic_ofstreamIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode
_ZNSolsEi
_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
_ZNSolsEPFRSoS_E
_ZNSt14basic_ofstreamIcSt11char_traitsIcEE5closeEv
_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1EPKcSt13_Ios_Openmode
_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev
_Unwind_Resume
__stack_chk_fail
