GAS LISTING /tmp/ccMbRvxK.s 			page 1


   1              		.file	"new_delete.cpp"
   2              		.text
   3              	.Ltext0:
   4              		.section	.rodata
   5              		.type	_ZStL19piecewise_construct, @object
   6              		.size	_ZStL19piecewise_construct, 1
   7              	_ZStL19piecewise_construct:
   8 0000 00       		.zero	1
   9              		.local	_ZStL8__ioinit
  10              		.comm	_ZStL8__ioinit,1,1
  11              		.text
  12              		.globl	main
  13              		.type	main, @function
  14              	main:
  15              	.LFB1383:
  16              		.file 1 "new_delete.cpp"
   1:new_delete.cpp **** #include <iostream>
   2:new_delete.cpp **** 
   3:new_delete.cpp **** int main(int argc, char* argv[])
   4:new_delete.cpp **** {
  17              		.loc 1 4 0
  18              		.cfi_startproc
  19 0000 55       		pushq	%rbp
  20              		.cfi_def_cfa_offset 16
  21              		.cfi_offset 6, -16
  22 0001 4889E5   		movq	%rsp, %rbp
  23              		.cfi_def_cfa_register 6
  24 0004 4883EC20 		subq	$32, %rsp
  25 0008 897DEC   		movl	%edi, -20(%rbp)
  26 000b 488975E0 		movq	%rsi, -32(%rbp)
  27              		.loc 1 4 0
  28 000f 64488B04 		movq	%fs:40, %rax
  28      25280000 
  28      00
  29 0018 488945F8 		movq	%rax, -8(%rbp)
  30 001c 31C0     		xorl	%eax, %eax
   5:new_delete.cpp ****   std::cout << argc << std::endl;
  31              		.loc 1 5 0
  32 001e 8B45EC   		movl	-20(%rbp), %eax
  33 0021 89C6     		movl	%eax, %esi
  34 0023 BF000000 		movl	$_ZSt4cout, %edi
  34      00
  35 0028 E8000000 		call	_ZNSolsEi
  35      00
  36 002d BE000000 		movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
  36      00
  37 0032 4889C7   		movq	%rax, %rdi
  38 0035 E8000000 		call	_ZNSolsEPFRSoS_E
  38      00
   6:new_delete.cpp ****   std::cout << argv << std::endl;
  39              		.loc 1 6 0
  40 003a 488B45E0 		movq	-32(%rbp), %rax
  41 003e 4889C6   		movq	%rax, %rsi
  42 0041 BF000000 		movl	$_ZSt4cout, %edi
  42      00
  43 0046 E8000000 		call	_ZNSolsEPKv
  43      00
GAS LISTING /tmp/ccMbRvxK.s 			page 2


  44 004b BE000000 		movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
  44      00
  45 0050 4889C7   		movq	%rax, %rdi
  46 0053 E8000000 		call	_ZNSolsEPFRSoS_E
  46      00
   7:new_delete.cpp ****   std::cout << *argv << std::endl;
  47              		.loc 1 7 0
  48 0058 488B45E0 		movq	-32(%rbp), %rax
  49 005c 488B00   		movq	(%rax), %rax
  50 005f 4889C6   		movq	%rax, %rsi
  51 0062 BF000000 		movl	$_ZSt4cout, %edi
  51      00
  52 0067 E8000000 		call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
  52      00
  53 006c BE000000 		movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
  53      00
  54 0071 4889C7   		movq	%rax, %rdi
  55 0074 E8000000 		call	_ZNSolsEPFRSoS_E
  55      00
   8:new_delete.cpp ****   std::cout << *argv[1] << std::endl;
  56              		.loc 1 8 0
  57 0079 488B45E0 		movq	-32(%rbp), %rax
  58 007d 4883C008 		addq	$8, %rax
  59 0081 488B00   		movq	(%rax), %rax
  60 0084 0FB600   		movzbl	(%rax), %eax
  61 0087 0FBEC0   		movsbl	%al, %eax
  62 008a 89C6     		movl	%eax, %esi
  63 008c BF000000 		movl	$_ZSt4cout, %edi
  63      00
  64 0091 E8000000 		call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c
  64      00
  65 0096 BE000000 		movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
  65      00
  66 009b 4889C7   		movq	%rax, %rdi
  67 009e E8000000 		call	_ZNSolsEPFRSoS_E
  67      00
   9:new_delete.cpp ****   std::cout << *argv[2] << std::endl;
  68              		.loc 1 9 0
  69 00a3 488B45E0 		movq	-32(%rbp), %rax
  70 00a7 4883C010 		addq	$16, %rax
  71 00ab 488B00   		movq	(%rax), %rax
  72 00ae 0FB600   		movzbl	(%rax), %eax
  73 00b1 0FBEC0   		movsbl	%al, %eax
  74 00b4 89C6     		movl	%eax, %esi
  75 00b6 BF000000 		movl	$_ZSt4cout, %edi
  75      00
  76 00bb E8000000 		call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c
  76      00
  77 00c0 BE000000 		movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
  77      00
  78 00c5 4889C7   		movq	%rax, %rdi
  79 00c8 E8000000 		call	_ZNSolsEPFRSoS_E
  79      00
  10:new_delete.cpp ****   int *ip = new int;
  80              		.loc 1 10 0
  81 00cd BF040000 		movl	$4, %edi
  81      00
GAS LISTING /tmp/ccMbRvxK.s 			page 3


  82 00d2 E8000000 		call	_Znwm
  82      00
  83 00d7 488945F0 		movq	%rax, -16(%rbp)
  11:new_delete.cpp **** 
  12:new_delete.cpp ****   std::cout << ip << std::endl;
  84              		.loc 1 12 0
  85 00db 488B45F0 		movq	-16(%rbp), %rax
  86 00df 4889C6   		movq	%rax, %rsi
  87 00e2 BF000000 		movl	$_ZSt4cout, %edi
  87      00
  88 00e7 E8000000 		call	_ZNSolsEPKv
  88      00
  89 00ec BE000000 		movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
  89      00
  90 00f1 4889C7   		movq	%rax, %rdi
  91 00f4 E8000000 		call	_ZNSolsEPFRSoS_E
  91      00
  13:new_delete.cpp ****   std::cout << *ip << std::endl ;
  92              		.loc 1 13 0
  93 00f9 488B45F0 		movq	-16(%rbp), %rax
  94 00fd 8B00     		movl	(%rax), %eax
  95 00ff 89C6     		movl	%eax, %esi
  96 0101 BF000000 		movl	$_ZSt4cout, %edi
  96      00
  97 0106 E8000000 		call	_ZNSolsEi
  97      00
  98 010b BE000000 		movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
  98      00
  99 0110 4889C7   		movq	%rax, %rdi
 100 0113 E8000000 		call	_ZNSolsEPFRSoS_E
 100      00
  14:new_delete.cpp ****   std::cout << &ip << std::endl  ;
 101              		.loc 1 14 0
 102 0118 488D45F0 		leaq	-16(%rbp), %rax
 103 011c 4889C6   		movq	%rax, %rsi
 104 011f BF000000 		movl	$_ZSt4cout, %edi
 104      00
 105 0124 E8000000 		call	_ZNSolsEPKv
 105      00
 106 0129 BE000000 		movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
 106      00
 107 012e 4889C7   		movq	%rax, %rdi
 108 0131 E8000000 		call	_ZNSolsEPFRSoS_E
 108      00
  15:new_delete.cpp **** 
  16:new_delete.cpp ****   delete ip;
 109              		.loc 1 16 0
 110 0136 488B45F0 		movq	-16(%rbp), %rax
 111 013a 4889C7   		movq	%rax, %rdi
 112 013d E8000000 		call	_ZdlPv
 112      00
  17:new_delete.cpp **** }
 113              		.loc 1 17 0
 114 0142 B8000000 		movl	$0, %eax
 114      00
 115 0147 488B55F8 		movq	-8(%rbp), %rdx
 116 014b 64483314 		xorq	%fs:40, %rdx
GAS LISTING /tmp/ccMbRvxK.s 			page 4


 116      25280000 
 116      00
 117 0154 7405     		je	.L3
 118 0156 E8000000 		call	__stack_chk_fail
 118      00
 119              	.L3:
 120 015b C9       		leave
 121              		.cfi_def_cfa 7, 8
 122 015c C3       		ret
 123              		.cfi_endproc
 124              	.LFE1383:
 125              		.size	main, .-main
 126              		.type	_Z41__static_initialization_and_destruction_0ii, @function
 127              	_Z41__static_initialization_and_destruction_0ii:
 128              	.LFB1576:
 129              		.loc 1 17 0
 130              		.cfi_startproc
 131 015d 55       		pushq	%rbp
 132              		.cfi_def_cfa_offset 16
 133              		.cfi_offset 6, -16
 134 015e 4889E5   		movq	%rsp, %rbp
 135              		.cfi_def_cfa_register 6
 136 0161 4883EC10 		subq	$16, %rsp
 137 0165 897DFC   		movl	%edi, -4(%rbp)
 138 0168 8975F8   		movl	%esi, -8(%rbp)
 139              		.loc 1 17 0
 140 016b 837DFC01 		cmpl	$1, -4(%rbp)
 141 016f 7527     		jne	.L6
 142              		.loc 1 17 0 is_stmt 0 discriminator 1
 143 0171 817DF8FF 		cmpl	$65535, -8(%rbp)
 143      FF0000
 144 0178 751E     		jne	.L6
 145              		.file 2 "/usr/include/c++/5/iostream"
   1:/usr/include/c++/5/iostream **** // Standard iostream objects -*- C++ -*-
   2:/usr/include/c++/5/iostream **** 
   3:/usr/include/c++/5/iostream **** // Copyright (C) 1997-2015 Free Software Foundation, Inc.
   4:/usr/include/c++/5/iostream **** //
   5:/usr/include/c++/5/iostream **** // This file is part of the GNU ISO C++ Library.  This library is free
   6:/usr/include/c++/5/iostream **** // software; you can redistribute it and/or modify it under the
   7:/usr/include/c++/5/iostream **** // terms of the GNU General Public License as published by the
   8:/usr/include/c++/5/iostream **** // Free Software Foundation; either version 3, or (at your option)
   9:/usr/include/c++/5/iostream **** // any later version.
  10:/usr/include/c++/5/iostream **** 
  11:/usr/include/c++/5/iostream **** // This library is distributed in the hope that it will be useful,
  12:/usr/include/c++/5/iostream **** // but WITHOUT ANY WARRANTY; without even the implied warranty of
  13:/usr/include/c++/5/iostream **** // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  14:/usr/include/c++/5/iostream **** // GNU General Public License for more details.
  15:/usr/include/c++/5/iostream **** 
  16:/usr/include/c++/5/iostream **** // Under Section 7 of GPL version 3, you are granted additional
  17:/usr/include/c++/5/iostream **** // permissions described in the GCC Runtime Library Exception, version
  18:/usr/include/c++/5/iostream **** // 3.1, as published by the Free Software Foundation.
  19:/usr/include/c++/5/iostream **** 
  20:/usr/include/c++/5/iostream **** // You should have received a copy of the GNU General Public License and
  21:/usr/include/c++/5/iostream **** // a copy of the GCC Runtime Library Exception along with this program;
  22:/usr/include/c++/5/iostream **** // see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
  23:/usr/include/c++/5/iostream **** // <http://www.gnu.org/licenses/>.
  24:/usr/include/c++/5/iostream **** 
GAS LISTING /tmp/ccMbRvxK.s 			page 5


  25:/usr/include/c++/5/iostream **** /** @file include/iostream
  26:/usr/include/c++/5/iostream ****  *  This is a Standard C++ Library header.
  27:/usr/include/c++/5/iostream ****  */
  28:/usr/include/c++/5/iostream **** 
  29:/usr/include/c++/5/iostream **** //
  30:/usr/include/c++/5/iostream **** // ISO C++ 14882: 27.3  Standard iostream objects
  31:/usr/include/c++/5/iostream **** //
  32:/usr/include/c++/5/iostream **** 
  33:/usr/include/c++/5/iostream **** #ifndef _GLIBCXX_IOSTREAM
  34:/usr/include/c++/5/iostream **** #define _GLIBCXX_IOSTREAM 1
  35:/usr/include/c++/5/iostream **** 
  36:/usr/include/c++/5/iostream **** #pragma GCC system_header
  37:/usr/include/c++/5/iostream **** 
  38:/usr/include/c++/5/iostream **** #include <bits/c++config.h>
  39:/usr/include/c++/5/iostream **** #include <ostream>
  40:/usr/include/c++/5/iostream **** #include <istream>
  41:/usr/include/c++/5/iostream **** 
  42:/usr/include/c++/5/iostream **** namespace std _GLIBCXX_VISIBILITY(default)
  43:/usr/include/c++/5/iostream **** {
  44:/usr/include/c++/5/iostream **** _GLIBCXX_BEGIN_NAMESPACE_VERSION
  45:/usr/include/c++/5/iostream **** 
  46:/usr/include/c++/5/iostream ****   /**
  47:/usr/include/c++/5/iostream ****    *  @name Standard Stream Objects
  48:/usr/include/c++/5/iostream ****    *
  49:/usr/include/c++/5/iostream ****    *  The &lt;iostream&gt; header declares the eight <em>standard stream
  50:/usr/include/c++/5/iostream ****    *  objects</em>.  For other declarations, see
  51:/usr/include/c++/5/iostream ****    *  http://gcc.gnu.org/onlinedocs/libstdc++/manual/io.html
  52:/usr/include/c++/5/iostream ****    *  and the @link iosfwd I/O forward declarations @endlink
  53:/usr/include/c++/5/iostream ****    *
  54:/usr/include/c++/5/iostream ****    *  They are required by default to cooperate with the global C
  55:/usr/include/c++/5/iostream ****    *  library's @c FILE streams, and to be available during program
  56:/usr/include/c++/5/iostream ****    *  startup and termination. For more information, see the section of the
  57:/usr/include/c++/5/iostream ****    *  manual linked to above.
  58:/usr/include/c++/5/iostream ****   */
  59:/usr/include/c++/5/iostream ****   //@{
  60:/usr/include/c++/5/iostream ****   extern istream cin;		/// Linked to standard input
  61:/usr/include/c++/5/iostream ****   extern ostream cout;		/// Linked to standard output
  62:/usr/include/c++/5/iostream ****   extern ostream cerr;		/// Linked to standard error (unbuffered)
  63:/usr/include/c++/5/iostream ****   extern ostream clog;		/// Linked to standard error (buffered)
  64:/usr/include/c++/5/iostream **** 
  65:/usr/include/c++/5/iostream **** #ifdef _GLIBCXX_USE_WCHAR_T
  66:/usr/include/c++/5/iostream ****   extern wistream wcin;		/// Linked to standard input
  67:/usr/include/c++/5/iostream ****   extern wostream wcout;	/// Linked to standard output
  68:/usr/include/c++/5/iostream ****   extern wostream wcerr;	/// Linked to standard error (unbuffered)
  69:/usr/include/c++/5/iostream ****   extern wostream wclog;	/// Linked to standard error (buffered)
  70:/usr/include/c++/5/iostream **** #endif
  71:/usr/include/c++/5/iostream ****   //@}
  72:/usr/include/c++/5/iostream **** 
  73:/usr/include/c++/5/iostream ****   // For construction of filebuffers for cout, cin, cerr, clog et. al.
  74:/usr/include/c++/5/iostream ****   static ios_base::Init __ioinit;
 146              		.loc 2 74 0 is_stmt 1
 147 017a BF000000 		movl	$_ZStL8__ioinit, %edi
 147      00
 148 017f E8000000 		call	_ZNSt8ios_base4InitC1Ev
 148      00
 149 0184 BA000000 		movl	$__dso_handle, %edx
 149      00
GAS LISTING /tmp/ccMbRvxK.s 			page 6


 150 0189 BE000000 		movl	$_ZStL8__ioinit, %esi
 150      00
 151 018e BF000000 		movl	$_ZNSt8ios_base4InitD1Ev, %edi
 151      00
 152 0193 E8000000 		call	__cxa_atexit
 152      00
 153              	.L6:
 154              		.loc 1 17 0
 155 0198 90       		nop
 156 0199 C9       		leave
 157              		.cfi_def_cfa 7, 8
 158 019a C3       		ret
 159              		.cfi_endproc
 160              	.LFE1576:
 161              		.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destructi
 162              		.type	_GLOBAL__sub_I_main, @function
 163              	_GLOBAL__sub_I_main:
 164              	.LFB1577:
 165              		.loc 1 17 0
 166              		.cfi_startproc
 167 019b 55       		pushq	%rbp
 168              		.cfi_def_cfa_offset 16
 169              		.cfi_offset 6, -16
 170 019c 4889E5   		movq	%rsp, %rbp
 171              		.cfi_def_cfa_register 6
 172              		.loc 1 17 0
 173 019f BEFFFF00 		movl	$65535, %esi
 173      00
 174 01a4 BF010000 		movl	$1, %edi
 174      00
 175 01a9 E8AFFFFF 		call	_Z41__static_initialization_and_destruction_0ii
 175      FF
 176 01ae 5D       		popq	%rbp
 177              		.cfi_def_cfa 7, 8
 178 01af C3       		ret
 179              		.cfi_endproc
 180              	.LFE1577:
 181              		.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
 182              		.section	.init_array,"aw"
 183              		.align 8
 184 0000 00000000 		.quad	_GLOBAL__sub_I_main
 184      00000000 
 185              		.text
 186              	.Letext0:
 187              		.file 3 "/usr/include/c++/5/cwchar"
 188              		.file 4 "/usr/include/c++/5/bits/exception_ptr.h"
 189              		.file 5 "/usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h"
 190              		.file 6 "/usr/include/c++/5/type_traits"
 191              		.file 7 "/usr/include/c++/5/debug/debug.h"
 192              		.file 8 "/usr/include/c++/5/bits/char_traits.h"
 193              		.file 9 "/usr/include/c++/5/cstdint"
 194              		.file 10 "/usr/include/c++/5/clocale"
 195              		.file 11 "/usr/include/c++/5/cstdlib"
 196              		.file 12 "/usr/include/c++/5/cstdio"
 197              		.file 13 "/usr/include/c++/5/system_error"
 198              		.file 14 "/usr/include/c++/5/bits/ios_base.h"
 199              		.file 15 "/usr/include/c++/5/cwctype"
GAS LISTING /tmp/ccMbRvxK.s 			page 7


 200              		.file 16 "/usr/include/c++/5/iosfwd"
 201              		.file 17 "/usr/include/c++/5/bits/predefined_ops.h"
 202              		.file 18 "/usr/include/c++/5/ext/new_allocator.h"
 203              		.file 19 "/usr/include/c++/5/ext/numeric_traits.h"
 204              		.file 20 "/usr/include/stdio.h"
 205              		.file 21 "/usr/include/libio.h"
 206              		.file 22 "<built-in>"
 207              		.file 23 "/usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h"
 208              		.file 24 "/usr/include/wchar.h"
 209              		.file 25 "/usr/include/time.h"
 210              		.file 26 "/usr/include/stdint.h"
 211              		.file 27 "/usr/include/locale.h"
 212              		.file 28 "/usr/include/x86_64-linux-gnu/bits/types.h"
 213              		.file 29 "/usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h"
 214              		.file 30 "/usr/include/stdlib.h"
 215              		.file 31 "/usr/include/_G_config.h"
 216              		.file 32 "/usr/include/wctype.h"
 217              		.file 33 "/usr/include/c++/5/bits/stl_pair.h"
 218              		.section	.debug_info,"",@progbits
 219              	.Ldebug_info0:
 220 0000 8A260000 		.long	0x268a
 221 0004 0400     		.value	0x4
 222 0006 00000000 		.long	.Ldebug_abbrev0
 223 000a 08       		.byte	0x8
 224 000b 01       		.uleb128 0x1
 225 000c 00000000 		.long	.LASF446
 226 0010 04       		.byte	0x4
 227 0011 00000000 		.long	.LASF447
 228 0015 00000000 		.long	.LASF448
 229 0019 00000000 		.quad	.Ltext0
 229      00000000 
 230 0021 B0010000 		.quad	.Letext0-.Ltext0
 230      00000000 
 231 0029 00000000 		.long	.Ldebug_line0
 232 002d 02       		.uleb128 0x2
 233 002e 73746400 		.string	"std"
 234 0032 16       		.byte	0x16
 235 0033 00       		.byte	0
 236 0034 920D0000 		.long	0xd92
 237 0038 03       		.uleb128 0x3
 238 0039 00000000 		.long	.LASF31
 239 003d 05       		.byte	0x5
 240 003e DA       		.byte	0xda
 241 003f 04       		.uleb128 0x4
 242 0040 05       		.byte	0x5
 243 0041 DA       		.byte	0xda
 244 0042 38000000 		.long	0x38
 245 0046 05       		.uleb128 0x5
 246 0047 03       		.byte	0x3
 247 0048 40       		.byte	0x40
 248 0049 9E120000 		.long	0x129e
 249 004d 05       		.uleb128 0x5
 250 004e 03       		.byte	0x3
 251 004f 8B       		.byte	0x8b
 252 0050 25120000 		.long	0x1225
 253 0054 05       		.uleb128 0x5
 254 0055 03       		.byte	0x3
GAS LISTING /tmp/ccMbRvxK.s 			page 8


 255 0056 8D       		.byte	0x8d
 256 0057 C0120000 		.long	0x12c0
 257 005b 05       		.uleb128 0x5
 258 005c 03       		.byte	0x3
 259 005d 8E       		.byte	0x8e
 260 005e D6120000 		.long	0x12d6
 261 0062 05       		.uleb128 0x5
 262 0063 03       		.byte	0x3
 263 0064 8F       		.byte	0x8f
 264 0065 F2120000 		.long	0x12f2
 265 0069 05       		.uleb128 0x5
 266 006a 03       		.byte	0x3
 267 006b 90       		.byte	0x90
 268 006c 1F130000 		.long	0x131f
 269 0070 05       		.uleb128 0x5
 270 0071 03       		.byte	0x3
 271 0072 91       		.byte	0x91
 272 0073 3A130000 		.long	0x133a
 273 0077 05       		.uleb128 0x5
 274 0078 03       		.byte	0x3
 275 0079 92       		.byte	0x92
 276 007a 60130000 		.long	0x1360
 277 007e 05       		.uleb128 0x5
 278 007f 03       		.byte	0x3
 279 0080 93       		.byte	0x93
 280 0081 7B130000 		.long	0x137b
 281 0085 05       		.uleb128 0x5
 282 0086 03       		.byte	0x3
 283 0087 94       		.byte	0x94
 284 0088 97130000 		.long	0x1397
 285 008c 05       		.uleb128 0x5
 286 008d 03       		.byte	0x3
 287 008e 95       		.byte	0x95
 288 008f B3130000 		.long	0x13b3
 289 0093 05       		.uleb128 0x5
 290 0094 03       		.byte	0x3
 291 0095 96       		.byte	0x96
 292 0096 C9130000 		.long	0x13c9
 293 009a 05       		.uleb128 0x5
 294 009b 03       		.byte	0x3
 295 009c 97       		.byte	0x97
 296 009d D5130000 		.long	0x13d5
 297 00a1 05       		.uleb128 0x5
 298 00a2 03       		.byte	0x3
 299 00a3 98       		.byte	0x98
 300 00a4 FB130000 		.long	0x13fb
 301 00a8 05       		.uleb128 0x5
 302 00a9 03       		.byte	0x3
 303 00aa 99       		.byte	0x99
 304 00ab 20140000 		.long	0x1420
 305 00af 05       		.uleb128 0x5
 306 00b0 03       		.byte	0x3
 307 00b1 9A       		.byte	0x9a
 308 00b2 41140000 		.long	0x1441
 309 00b6 05       		.uleb128 0x5
 310 00b7 03       		.byte	0x3
 311 00b8 9B       		.byte	0x9b
GAS LISTING /tmp/ccMbRvxK.s 			page 9


 312 00b9 6C140000 		.long	0x146c
 313 00bd 05       		.uleb128 0x5
 314 00be 03       		.byte	0x3
 315 00bf 9C       		.byte	0x9c
 316 00c0 87140000 		.long	0x1487
 317 00c4 05       		.uleb128 0x5
 318 00c5 03       		.byte	0x3
 319 00c6 9E       		.byte	0x9e
 320 00c7 9D140000 		.long	0x149d
 321 00cb 05       		.uleb128 0x5
 322 00cc 03       		.byte	0x3
 323 00cd A0       		.byte	0xa0
 324 00ce BE140000 		.long	0x14be
 325 00d2 05       		.uleb128 0x5
 326 00d3 03       		.byte	0x3
 327 00d4 A1       		.byte	0xa1
 328 00d5 DA140000 		.long	0x14da
 329 00d9 05       		.uleb128 0x5
 330 00da 03       		.byte	0x3
 331 00db A2       		.byte	0xa2
 332 00dc F5140000 		.long	0x14f5
 333 00e0 05       		.uleb128 0x5
 334 00e1 03       		.byte	0x3
 335 00e2 A4       		.byte	0xa4
 336 00e3 1B150000 		.long	0x151b
 337 00e7 05       		.uleb128 0x5
 338 00e8 03       		.byte	0x3
 339 00e9 A7       		.byte	0xa7
 340 00ea 3B150000 		.long	0x153b
 341 00ee 05       		.uleb128 0x5
 342 00ef 03       		.byte	0x3
 343 00f0 AA       		.byte	0xaa
 344 00f1 60150000 		.long	0x1560
 345 00f5 05       		.uleb128 0x5
 346 00f6 03       		.byte	0x3
 347 00f7 AC       		.byte	0xac
 348 00f8 80150000 		.long	0x1580
 349 00fc 05       		.uleb128 0x5
 350 00fd 03       		.byte	0x3
 351 00fe AE       		.byte	0xae
 352 00ff 9B150000 		.long	0x159b
 353 0103 05       		.uleb128 0x5
 354 0104 03       		.byte	0x3
 355 0105 B0       		.byte	0xb0
 356 0106 B6150000 		.long	0x15b6
 357 010a 05       		.uleb128 0x5
 358 010b 03       		.byte	0x3
 359 010c B1       		.byte	0xb1
 360 010d DC150000 		.long	0x15dc
 361 0111 05       		.uleb128 0x5
 362 0112 03       		.byte	0x3
 363 0113 B2       		.byte	0xb2
 364 0114 F6150000 		.long	0x15f6
 365 0118 05       		.uleb128 0x5
 366 0119 03       		.byte	0x3
 367 011a B3       		.byte	0xb3
 368 011b 10160000 		.long	0x1610
GAS LISTING /tmp/ccMbRvxK.s 			page 10


 369 011f 05       		.uleb128 0x5
 370 0120 03       		.byte	0x3
 371 0121 B4       		.byte	0xb4
 372 0122 2A160000 		.long	0x162a
 373 0126 05       		.uleb128 0x5
 374 0127 03       		.byte	0x3
 375 0128 B5       		.byte	0xb5
 376 0129 44160000 		.long	0x1644
 377 012d 05       		.uleb128 0x5
 378 012e 03       		.byte	0x3
 379 012f B6       		.byte	0xb6
 380 0130 5E160000 		.long	0x165e
 381 0134 05       		.uleb128 0x5
 382 0135 03       		.byte	0x3
 383 0136 B7       		.byte	0xb7
 384 0137 1E170000 		.long	0x171e
 385 013b 05       		.uleb128 0x5
 386 013c 03       		.byte	0x3
 387 013d B8       		.byte	0xb8
 388 013e 34170000 		.long	0x1734
 389 0142 05       		.uleb128 0x5
 390 0143 03       		.byte	0x3
 391 0144 B9       		.byte	0xb9
 392 0145 53170000 		.long	0x1753
 393 0149 05       		.uleb128 0x5
 394 014a 03       		.byte	0x3
 395 014b BA       		.byte	0xba
 396 014c 72170000 		.long	0x1772
 397 0150 05       		.uleb128 0x5
 398 0151 03       		.byte	0x3
 399 0152 BB       		.byte	0xbb
 400 0153 91170000 		.long	0x1791
 401 0157 05       		.uleb128 0x5
 402 0158 03       		.byte	0x3
 403 0159 BC       		.byte	0xbc
 404 015a BC170000 		.long	0x17bc
 405 015e 05       		.uleb128 0x5
 406 015f 03       		.byte	0x3
 407 0160 BD       		.byte	0xbd
 408 0161 D7170000 		.long	0x17d7
 409 0165 05       		.uleb128 0x5
 410 0166 03       		.byte	0x3
 411 0167 BF       		.byte	0xbf
 412 0168 FF170000 		.long	0x17ff
 413 016c 05       		.uleb128 0x5
 414 016d 03       		.byte	0x3
 415 016e C1       		.byte	0xc1
 416 016f 21180000 		.long	0x1821
 417 0173 05       		.uleb128 0x5
 418 0174 03       		.byte	0x3
 419 0175 C2       		.byte	0xc2
 420 0176 41180000 		.long	0x1841
 421 017a 05       		.uleb128 0x5
 422 017b 03       		.byte	0x3
 423 017c C3       		.byte	0xc3
 424 017d 68180000 		.long	0x1868
 425 0181 05       		.uleb128 0x5
GAS LISTING /tmp/ccMbRvxK.s 			page 11


 426 0182 03       		.byte	0x3
 427 0183 C4       		.byte	0xc4
 428 0184 88180000 		.long	0x1888
 429 0188 05       		.uleb128 0x5
 430 0189 03       		.byte	0x3
 431 018a C5       		.byte	0xc5
 432 018b A7180000 		.long	0x18a7
 433 018f 05       		.uleb128 0x5
 434 0190 03       		.byte	0x3
 435 0191 C6       		.byte	0xc6
 436 0192 BD180000 		.long	0x18bd
 437 0196 05       		.uleb128 0x5
 438 0197 03       		.byte	0x3
 439 0198 C7       		.byte	0xc7
 440 0199 DD180000 		.long	0x18dd
 441 019d 05       		.uleb128 0x5
 442 019e 03       		.byte	0x3
 443 019f C8       		.byte	0xc8
 444 01a0 FD180000 		.long	0x18fd
 445 01a4 05       		.uleb128 0x5
 446 01a5 03       		.byte	0x3
 447 01a6 C9       		.byte	0xc9
 448 01a7 1D190000 		.long	0x191d
 449 01ab 05       		.uleb128 0x5
 450 01ac 03       		.byte	0x3
 451 01ad CA       		.byte	0xca
 452 01ae 3D190000 		.long	0x193d
 453 01b2 05       		.uleb128 0x5
 454 01b3 03       		.byte	0x3
 455 01b4 CB       		.byte	0xcb
 456 01b5 54190000 		.long	0x1954
 457 01b9 05       		.uleb128 0x5
 458 01ba 03       		.byte	0x3
 459 01bb CC       		.byte	0xcc
 460 01bc 6B190000 		.long	0x196b
 461 01c0 05       		.uleb128 0x5
 462 01c1 03       		.byte	0x3
 463 01c2 CD       		.byte	0xcd
 464 01c3 89190000 		.long	0x1989
 465 01c7 05       		.uleb128 0x5
 466 01c8 03       		.byte	0x3
 467 01c9 CE       		.byte	0xce
 468 01ca A8190000 		.long	0x19a8
 469 01ce 05       		.uleb128 0x5
 470 01cf 03       		.byte	0x3
 471 01d0 CF       		.byte	0xcf
 472 01d1 C6190000 		.long	0x19c6
 473 01d5 05       		.uleb128 0x5
 474 01d6 03       		.byte	0x3
 475 01d7 D0       		.byte	0xd0
 476 01d8 E5190000 		.long	0x19e5
 477 01dc 06       		.uleb128 0x6
 478 01dd 03       		.byte	0x3
 479 01de 0801     		.value	0x108
 480 01e0 091A0000 		.long	0x1a09
 481 01e4 06       		.uleb128 0x6
 482 01e5 03       		.byte	0x3
GAS LISTING /tmp/ccMbRvxK.s 			page 12


 483 01e6 0901     		.value	0x109
 484 01e8 2B1A0000 		.long	0x1a2b
 485 01ec 06       		.uleb128 0x6
 486 01ed 03       		.byte	0x3
 487 01ee 0A01     		.value	0x10a
 488 01f0 521A0000 		.long	0x1a52
 489 01f4 06       		.uleb128 0x6
 490 01f5 03       		.byte	0x3
 491 01f6 1801     		.value	0x118
 492 01f8 FF170000 		.long	0x17ff
 493 01fc 06       		.uleb128 0x6
 494 01fd 03       		.byte	0x3
 495 01fe 1B01     		.value	0x11b
 496 0200 1B150000 		.long	0x151b
 497 0204 06       		.uleb128 0x6
 498 0205 03       		.byte	0x3
 499 0206 1E01     		.value	0x11e
 500 0208 60150000 		.long	0x1560
 501 020c 06       		.uleb128 0x6
 502 020d 03       		.byte	0x3
 503 020e 2101     		.value	0x121
 504 0210 9B150000 		.long	0x159b
 505 0214 06       		.uleb128 0x6
 506 0215 03       		.byte	0x3
 507 0216 2501     		.value	0x125
 508 0218 091A0000 		.long	0x1a09
 509 021c 06       		.uleb128 0x6
 510 021d 03       		.byte	0x3
 511 021e 2601     		.value	0x126
 512 0220 2B1A0000 		.long	0x1a2b
 513 0224 06       		.uleb128 0x6
 514 0225 03       		.byte	0x3
 515 0226 2701     		.value	0x127
 516 0228 521A0000 		.long	0x1a52
 517 022c 07       		.uleb128 0x7
 518 022d 00000000 		.long	.LASF0
 519 0231 04       		.byte	0x4
 520 0232 36       		.byte	0x36
 521 0233 F6030000 		.long	0x3f6
 522 0237 08       		.uleb128 0x8
 523 0238 00000000 		.long	.LASF5
 524 023c 08       		.byte	0x8
 525 023d 04       		.byte	0x4
 526 023e 4B       		.byte	0x4b
 527 023f F0030000 		.long	0x3f0
 528 0243 09       		.uleb128 0x9
 529 0244 00000000 		.long	.LASF158
 530 0248 04       		.byte	0x4
 531 0249 4D       		.byte	0x4d
 532 024a 11120000 		.long	0x1211
 533 024e 00       		.byte	0
 534 024f 0A       		.uleb128 0xa
 535 0250 00000000 		.long	.LASF5
 536 0254 04       		.byte	0x4
 537 0255 4F       		.byte	0x4f
 538 0256 00000000 		.long	.LASF7
 539 025a 62020000 		.long	0x262
GAS LISTING /tmp/ccMbRvxK.s 			page 13


 540 025e 6D020000 		.long	0x26d
 541 0262 0B       		.uleb128 0xb
 542 0263 791A0000 		.long	0x1a79
 543 0267 0C       		.uleb128 0xc
 544 0268 11120000 		.long	0x1211
 545 026c 00       		.byte	0
 546 026d 0D       		.uleb128 0xd
 547 026e 00000000 		.long	.LASF1
 548 0272 04       		.byte	0x4
 549 0273 51       		.byte	0x51
 550 0274 00000000 		.long	.LASF3
 551 0278 80020000 		.long	0x280
 552 027c 86020000 		.long	0x286
 553 0280 0B       		.uleb128 0xb
 554 0281 791A0000 		.long	0x1a79
 555 0285 00       		.byte	0
 556 0286 0D       		.uleb128 0xd
 557 0287 00000000 		.long	.LASF2
 558 028b 04       		.byte	0x4
 559 028c 52       		.byte	0x52
 560 028d 00000000 		.long	.LASF4
 561 0291 99020000 		.long	0x299
 562 0295 9F020000 		.long	0x29f
 563 0299 0B       		.uleb128 0xb
 564 029a 791A0000 		.long	0x1a79
 565 029e 00       		.byte	0
 566 029f 0E       		.uleb128 0xe
 567 02a0 00000000 		.long	.LASF6
 568 02a4 04       		.byte	0x4
 569 02a5 54       		.byte	0x54
 570 02a6 00000000 		.long	.LASF8
 571 02aa 11120000 		.long	0x1211
 572 02ae B6020000 		.long	0x2b6
 573 02b2 BC020000 		.long	0x2bc
 574 02b6 0B       		.uleb128 0xb
 575 02b7 7F1A0000 		.long	0x1a7f
 576 02bb 00       		.byte	0
 577 02bc 0F       		.uleb128 0xf
 578 02bd 00000000 		.long	.LASF5
 579 02c1 04       		.byte	0x4
 580 02c2 5A       		.byte	0x5a
 581 02c3 00000000 		.long	.LASF9
 582 02c7 01       		.byte	0x1
 583 02c8 D0020000 		.long	0x2d0
 584 02cc D6020000 		.long	0x2d6
 585 02d0 0B       		.uleb128 0xb
 586 02d1 791A0000 		.long	0x1a79
 587 02d5 00       		.byte	0
 588 02d6 0F       		.uleb128 0xf
 589 02d7 00000000 		.long	.LASF5
 590 02db 04       		.byte	0x4
 591 02dc 5C       		.byte	0x5c
 592 02dd 00000000 		.long	.LASF10
 593 02e1 01       		.byte	0x1
 594 02e2 EA020000 		.long	0x2ea
 595 02e6 F5020000 		.long	0x2f5
 596 02ea 0B       		.uleb128 0xb
GAS LISTING /tmp/ccMbRvxK.s 			page 14


 597 02eb 791A0000 		.long	0x1a79
 598 02ef 0C       		.uleb128 0xc
 599 02f0 851A0000 		.long	0x1a85
 600 02f4 00       		.byte	0
 601 02f5 0F       		.uleb128 0xf
 602 02f6 00000000 		.long	.LASF5
 603 02fa 04       		.byte	0x4
 604 02fb 5F       		.byte	0x5f
 605 02fc 00000000 		.long	.LASF11
 606 0300 01       		.byte	0x1
 607 0301 09030000 		.long	0x309
 608 0305 14030000 		.long	0x314
 609 0309 0B       		.uleb128 0xb
 610 030a 791A0000 		.long	0x1a79
 611 030e 0C       		.uleb128 0xc
 612 030f FD030000 		.long	0x3fd
 613 0313 00       		.byte	0
 614 0314 0F       		.uleb128 0xf
 615 0315 00000000 		.long	.LASF5
 616 0319 04       		.byte	0x4
 617 031a 63       		.byte	0x63
 618 031b 00000000 		.long	.LASF12
 619 031f 01       		.byte	0x1
 620 0320 28030000 		.long	0x328
 621 0324 33030000 		.long	0x333
 622 0328 0B       		.uleb128 0xb
 623 0329 791A0000 		.long	0x1a79
 624 032d 0C       		.uleb128 0xc
 625 032e 901A0000 		.long	0x1a90
 626 0332 00       		.byte	0
 627 0333 10       		.uleb128 0x10
 628 0334 00000000 		.long	.LASF13
 629 0338 04       		.byte	0x4
 630 0339 70       		.byte	0x70
 631 033a 00000000 		.long	.LASF14
 632 033e 961A0000 		.long	0x1a96
 633 0342 01       		.byte	0x1
 634 0343 4B030000 		.long	0x34b
 635 0347 56030000 		.long	0x356
 636 034b 0B       		.uleb128 0xb
 637 034c 791A0000 		.long	0x1a79
 638 0350 0C       		.uleb128 0xc
 639 0351 851A0000 		.long	0x1a85
 640 0355 00       		.byte	0
 641 0356 10       		.uleb128 0x10
 642 0357 00000000 		.long	.LASF13
 643 035b 04       		.byte	0x4
 644 035c 74       		.byte	0x74
 645 035d 00000000 		.long	.LASF15
 646 0361 961A0000 		.long	0x1a96
 647 0365 01       		.byte	0x1
 648 0366 6E030000 		.long	0x36e
 649 036a 79030000 		.long	0x379
 650 036e 0B       		.uleb128 0xb
 651 036f 791A0000 		.long	0x1a79
 652 0373 0C       		.uleb128 0xc
 653 0374 901A0000 		.long	0x1a90
GAS LISTING /tmp/ccMbRvxK.s 			page 15


 654 0378 00       		.byte	0
 655 0379 0F       		.uleb128 0xf
 656 037a 00000000 		.long	.LASF16
 657 037e 04       		.byte	0x4
 658 037f 7B       		.byte	0x7b
 659 0380 00000000 		.long	.LASF17
 660 0384 01       		.byte	0x1
 661 0385 8D030000 		.long	0x38d
 662 0389 98030000 		.long	0x398
 663 038d 0B       		.uleb128 0xb
 664 038e 791A0000 		.long	0x1a79
 665 0392 0B       		.uleb128 0xb
 666 0393 8C120000 		.long	0x128c
 667 0397 00       		.byte	0
 668 0398 0F       		.uleb128 0xf
 669 0399 00000000 		.long	.LASF18
 670 039d 04       		.byte	0x4
 671 039e 7E       		.byte	0x7e
 672 039f 00000000 		.long	.LASF19
 673 03a3 01       		.byte	0x1
 674 03a4 AC030000 		.long	0x3ac
 675 03a8 B7030000 		.long	0x3b7
 676 03ac 0B       		.uleb128 0xb
 677 03ad 791A0000 		.long	0x1a79
 678 03b1 0C       		.uleb128 0xc
 679 03b2 961A0000 		.long	0x1a96
 680 03b6 00       		.byte	0
 681 03b7 11       		.uleb128 0x11
 682 03b8 00000000 		.long	.LASF449
 683 03bc 04       		.byte	0x4
 684 03bd 8A       		.byte	0x8a
 685 03be 00000000 		.long	.LASF450
 686 03c2 9C1A0000 		.long	0x1a9c
 687 03c6 01       		.byte	0x1
 688 03c7 CF030000 		.long	0x3cf
 689 03cb D5030000 		.long	0x3d5
 690 03cf 0B       		.uleb128 0xb
 691 03d0 7F1A0000 		.long	0x1a7f
 692 03d4 00       		.byte	0
 693 03d5 12       		.uleb128 0x12
 694 03d6 00000000 		.long	.LASF20
 695 03da 04       		.byte	0x4
 696 03db 93       		.byte	0x93
 697 03dc 00000000 		.long	.LASF21
 698 03e0 A31A0000 		.long	0x1aa3
 699 03e4 01       		.byte	0x1
 700 03e5 E9030000 		.long	0x3e9
 701 03e9 0B       		.uleb128 0xb
 702 03ea 7F1A0000 		.long	0x1a7f
 703 03ee 00       		.byte	0
 704 03ef 00       		.byte	0
 705 03f0 13       		.uleb128 0x13
 706 03f1 37020000 		.long	0x237
 707 03f5 00       		.byte	0
 708 03f6 05       		.uleb128 0x5
 709 03f7 04       		.byte	0x4
 710 03f8 3A       		.byte	0x3a
GAS LISTING /tmp/ccMbRvxK.s 			page 16


 711 03f9 37020000 		.long	0x237
 712 03fd 14       		.uleb128 0x14
 713 03fe 00000000 		.long	.LASF22
 714 0402 05       		.byte	0x5
 715 0403 C8       		.byte	0xc8
 716 0404 8B1A0000 		.long	0x1a8b
 717 0408 15       		.uleb128 0x15
 718 0409 00000000 		.long	.LASF451
 719 040d 13       		.uleb128 0x13
 720 040e 08040000 		.long	0x408
 721 0412 16       		.uleb128 0x16
 722 0413 00000000 		.long	.LASF26
 723 0417 01       		.byte	0x1
 724 0418 06       		.byte	0x6
 725 0419 45       		.byte	0x45
 726 041a 65040000 		.long	0x465
 727 041e 17       		.uleb128 0x17
 728 041f 00000000 		.long	.LASF28
 729 0423 06       		.byte	0x6
 730 0424 47       		.byte	0x47
 731 0425 BE1A0000 		.long	0x1abe
 732 0429 14       		.uleb128 0x14
 733 042a 00000000 		.long	.LASF23
 734 042e 06       		.byte	0x6
 735 042f 48       		.byte	0x48
 736 0430 9C1A0000 		.long	0x1a9c
 737 0434 0E       		.uleb128 0xe
 738 0435 00000000 		.long	.LASF24
 739 0439 06       		.byte	0x6
 740 043a 4A       		.byte	0x4a
 741 043b 00000000 		.long	.LASF25
 742 043f 29040000 		.long	0x429
 743 0443 4B040000 		.long	0x44b
 744 0447 51040000 		.long	0x451
 745 044b 0B       		.uleb128 0xb
 746 044c C31A0000 		.long	0x1ac3
 747 0450 00       		.byte	0
 748 0451 18       		.uleb128 0x18
 749 0452 5F547000 		.string	"_Tp"
 750 0456 9C1A0000 		.long	0x1a9c
 751 045a 19       		.uleb128 0x19
 752 045b 5F5F7600 		.string	"__v"
 753 045f 9C1A0000 		.long	0x1a9c
 754 0463 00       		.byte	0
 755 0464 00       		.byte	0
 756 0465 13       		.uleb128 0x13
 757 0466 12040000 		.long	0x412
 758 046a 16       		.uleb128 0x16
 759 046b 00000000 		.long	.LASF27
 760 046f 01       		.byte	0x1
 761 0470 06       		.byte	0x6
 762 0471 45       		.byte	0x45
 763 0472 BD040000 		.long	0x4bd
 764 0476 17       		.uleb128 0x17
 765 0477 00000000 		.long	.LASF28
 766 047b 06       		.byte	0x6
 767 047c 47       		.byte	0x47
GAS LISTING /tmp/ccMbRvxK.s 			page 17


 768 047d BE1A0000 		.long	0x1abe
 769 0481 14       		.uleb128 0x14
 770 0482 00000000 		.long	.LASF23
 771 0486 06       		.byte	0x6
 772 0487 48       		.byte	0x48
 773 0488 9C1A0000 		.long	0x1a9c
 774 048c 0E       		.uleb128 0xe
 775 048d 00000000 		.long	.LASF29
 776 0491 06       		.byte	0x6
 777 0492 4A       		.byte	0x4a
 778 0493 00000000 		.long	.LASF30
 779 0497 81040000 		.long	0x481
 780 049b A3040000 		.long	0x4a3
 781 049f A9040000 		.long	0x4a9
 782 04a3 0B       		.uleb128 0xb
 783 04a4 C91A0000 		.long	0x1ac9
 784 04a8 00       		.byte	0
 785 04a9 18       		.uleb128 0x18
 786 04aa 5F547000 		.string	"_Tp"
 787 04ae 9C1A0000 		.long	0x1a9c
 788 04b2 19       		.uleb128 0x19
 789 04b3 5F5F7600 		.string	"__v"
 790 04b7 9C1A0000 		.long	0x1a9c
 791 04bb 01       		.byte	0x1
 792 04bc 00       		.byte	0
 793 04bd 13       		.uleb128 0x13
 794 04be 6A040000 		.long	0x46a
 795 04c2 1A       		.uleb128 0x1a
 796 04c3 00000000 		.long	.LASF452
 797 04c7 01       		.byte	0x1
 798 04c8 21       		.byte	0x21
 799 04c9 4C       		.byte	0x4c
 800 04ca 03       		.uleb128 0x3
 801 04cb 00000000 		.long	.LASF32
 802 04cf 07       		.byte	0x7
 803 04d0 30       		.byte	0x30
 804 04d1 16       		.uleb128 0x16
 805 04d2 00000000 		.long	.LASF33
 806 04d6 01       		.byte	0x1
 807 04d7 08       		.byte	0x8
 808 04d8 E9       		.byte	0xe9
 809 04d9 99060000 		.long	0x699
 810 04dd 14       		.uleb128 0x14
 811 04de 00000000 		.long	.LASF34
 812 04e2 08       		.byte	0x8
 813 04e3 EB       		.byte	0xeb
 814 04e4 85120000 		.long	0x1285
 815 04e8 14       		.uleb128 0x14
 816 04e9 00000000 		.long	.LASF35
 817 04ed 08       		.byte	0x8
 818 04ee EC       		.byte	0xec
 819 04ef 8C120000 		.long	0x128c
 820 04f3 1B       		.uleb128 0x1b
 821 04f4 00000000 		.long	.LASF48
 822 04f8 08       		.byte	0x8
 823 04f9 F2       		.byte	0xf2
 824 04fa 00000000 		.long	.LASF453
GAS LISTING /tmp/ccMbRvxK.s 			page 18


 825 04fe 0D050000 		.long	0x50d
 826 0502 0C       		.uleb128 0xc
 827 0503 E71A0000 		.long	0x1ae7
 828 0507 0C       		.uleb128 0xc
 829 0508 ED1A0000 		.long	0x1aed
 830 050c 00       		.byte	0
 831 050d 13       		.uleb128 0x13
 832 050e DD040000 		.long	0x4dd
 833 0512 1C       		.uleb128 0x1c
 834 0513 657100   		.string	"eq"
 835 0516 08       		.byte	0x8
 836 0517 F6       		.byte	0xf6
 837 0518 00000000 		.long	.LASF36
 838 051c 9C1A0000 		.long	0x1a9c
 839 0520 2F050000 		.long	0x52f
 840 0524 0C       		.uleb128 0xc
 841 0525 ED1A0000 		.long	0x1aed
 842 0529 0C       		.uleb128 0xc
 843 052a ED1A0000 		.long	0x1aed
 844 052e 00       		.byte	0
 845 052f 1C       		.uleb128 0x1c
 846 0530 6C7400   		.string	"lt"
 847 0533 08       		.byte	0x8
 848 0534 FA       		.byte	0xfa
 849 0535 00000000 		.long	.LASF37
 850 0539 9C1A0000 		.long	0x1a9c
 851 053d 4C050000 		.long	0x54c
 852 0541 0C       		.uleb128 0xc
 853 0542 ED1A0000 		.long	0x1aed
 854 0546 0C       		.uleb128 0xc
 855 0547 ED1A0000 		.long	0x1aed
 856 054b 00       		.byte	0
 857 054c 1D       		.uleb128 0x1d
 858 054d 00000000 		.long	.LASF38
 859 0551 08       		.byte	0x8
 860 0552 0201     		.value	0x102
 861 0554 00000000 		.long	.LASF40
 862 0558 8C120000 		.long	0x128c
 863 055c 70050000 		.long	0x570
 864 0560 0C       		.uleb128 0xc
 865 0561 F31A0000 		.long	0x1af3
 866 0565 0C       		.uleb128 0xc
 867 0566 F31A0000 		.long	0x1af3
 868 056a 0C       		.uleb128 0xc
 869 056b 99060000 		.long	0x699
 870 056f 00       		.byte	0
 871 0570 1D       		.uleb128 0x1d
 872 0571 00000000 		.long	.LASF39
 873 0575 08       		.byte	0x8
 874 0576 0A01     		.value	0x10a
 875 0578 00000000 		.long	.LASF41
 876 057c 99060000 		.long	0x699
 877 0580 8A050000 		.long	0x58a
 878 0584 0C       		.uleb128 0xc
 879 0585 F31A0000 		.long	0x1af3
 880 0589 00       		.byte	0
 881 058a 1D       		.uleb128 0x1d
GAS LISTING /tmp/ccMbRvxK.s 			page 19


 882 058b 00000000 		.long	.LASF42
 883 058f 08       		.byte	0x8
 884 0590 0E01     		.value	0x10e
 885 0592 00000000 		.long	.LASF43
 886 0596 F31A0000 		.long	0x1af3
 887 059a AE050000 		.long	0x5ae
 888 059e 0C       		.uleb128 0xc
 889 059f F31A0000 		.long	0x1af3
 890 05a3 0C       		.uleb128 0xc
 891 05a4 99060000 		.long	0x699
 892 05a8 0C       		.uleb128 0xc
 893 05a9 ED1A0000 		.long	0x1aed
 894 05ad 00       		.byte	0
 895 05ae 1D       		.uleb128 0x1d
 896 05af 00000000 		.long	.LASF44
 897 05b3 08       		.byte	0x8
 898 05b4 1601     		.value	0x116
 899 05b6 00000000 		.long	.LASF45
 900 05ba F91A0000 		.long	0x1af9
 901 05be D2050000 		.long	0x5d2
 902 05c2 0C       		.uleb128 0xc
 903 05c3 F91A0000 		.long	0x1af9
 904 05c7 0C       		.uleb128 0xc
 905 05c8 F31A0000 		.long	0x1af3
 906 05cc 0C       		.uleb128 0xc
 907 05cd 99060000 		.long	0x699
 908 05d1 00       		.byte	0
 909 05d2 1D       		.uleb128 0x1d
 910 05d3 00000000 		.long	.LASF46
 911 05d7 08       		.byte	0x8
 912 05d8 1E01     		.value	0x11e
 913 05da 00000000 		.long	.LASF47
 914 05de F91A0000 		.long	0x1af9
 915 05e2 F6050000 		.long	0x5f6
 916 05e6 0C       		.uleb128 0xc
 917 05e7 F91A0000 		.long	0x1af9
 918 05eb 0C       		.uleb128 0xc
 919 05ec F31A0000 		.long	0x1af3
 920 05f0 0C       		.uleb128 0xc
 921 05f1 99060000 		.long	0x699
 922 05f5 00       		.byte	0
 923 05f6 1D       		.uleb128 0x1d
 924 05f7 00000000 		.long	.LASF48
 925 05fb 08       		.byte	0x8
 926 05fc 2601     		.value	0x126
 927 05fe 00000000 		.long	.LASF49
 928 0602 F91A0000 		.long	0x1af9
 929 0606 1A060000 		.long	0x61a
 930 060a 0C       		.uleb128 0xc
 931 060b F91A0000 		.long	0x1af9
 932 060f 0C       		.uleb128 0xc
 933 0610 99060000 		.long	0x699
 934 0614 0C       		.uleb128 0xc
 935 0615 DD040000 		.long	0x4dd
 936 0619 00       		.byte	0
 937 061a 1D       		.uleb128 0x1d
 938 061b 00000000 		.long	.LASF50
GAS LISTING /tmp/ccMbRvxK.s 			page 20


 939 061f 08       		.byte	0x8
 940 0620 2E01     		.value	0x12e
 941 0622 00000000 		.long	.LASF51
 942 0626 DD040000 		.long	0x4dd
 943 062a 34060000 		.long	0x634
 944 062e 0C       		.uleb128 0xc
 945 062f FF1A0000 		.long	0x1aff
 946 0633 00       		.byte	0
 947 0634 13       		.uleb128 0x13
 948 0635 E8040000 		.long	0x4e8
 949 0639 1D       		.uleb128 0x1d
 950 063a 00000000 		.long	.LASF52
 951 063e 08       		.byte	0x8
 952 063f 3401     		.value	0x134
 953 0641 00000000 		.long	.LASF53
 954 0645 E8040000 		.long	0x4e8
 955 0649 53060000 		.long	0x653
 956 064d 0C       		.uleb128 0xc
 957 064e ED1A0000 		.long	0x1aed
 958 0652 00       		.byte	0
 959 0653 1D       		.uleb128 0x1d
 960 0654 00000000 		.long	.LASF54
 961 0658 08       		.byte	0x8
 962 0659 3801     		.value	0x138
 963 065b 00000000 		.long	.LASF55
 964 065f 9C1A0000 		.long	0x1a9c
 965 0663 72060000 		.long	0x672
 966 0667 0C       		.uleb128 0xc
 967 0668 FF1A0000 		.long	0x1aff
 968 066c 0C       		.uleb128 0xc
 969 066d FF1A0000 		.long	0x1aff
 970 0671 00       		.byte	0
 971 0672 1E       		.uleb128 0x1e
 972 0673 656F6600 		.string	"eof"
 973 0677 08       		.byte	0x8
 974 0678 3C01     		.value	0x13c
 975 067a 00000000 		.long	.LASF454
 976 067e E8040000 		.long	0x4e8
 977 0682 1F       		.uleb128 0x1f
 978 0683 00000000 		.long	.LASF56
 979 0687 08       		.byte	0x8
 980 0688 4001     		.value	0x140
 981 068a 00000000 		.long	.LASF455
 982 068e E8040000 		.long	0x4e8
 983 0692 0C       		.uleb128 0xc
 984 0693 FF1A0000 		.long	0x1aff
 985 0697 00       		.byte	0
 986 0698 00       		.byte	0
 987 0699 14       		.uleb128 0x14
 988 069a 00000000 		.long	.LASF57
 989 069e 05       		.byte	0x5
 990 069f C4       		.byte	0xc4
 991 06a0 1E120000 		.long	0x121e
 992 06a4 05       		.uleb128 0x5
 993 06a5 09       		.byte	0x9
 994 06a6 30       		.byte	0x30
 995 06a7 051B0000 		.long	0x1b05
GAS LISTING /tmp/ccMbRvxK.s 			page 21


 996 06ab 05       		.uleb128 0x5
 997 06ac 09       		.byte	0x9
 998 06ad 31       		.byte	0x31
 999 06ae 101B0000 		.long	0x1b10
 1000 06b2 05       		.uleb128 0x5
 1001 06b3 09       		.byte	0x9
 1002 06b4 32       		.byte	0x32
 1003 06b5 1B1B0000 		.long	0x1b1b
 1004 06b9 05       		.uleb128 0x5
 1005 06ba 09       		.byte	0x9
 1006 06bb 33       		.byte	0x33
 1007 06bc 261B0000 		.long	0x1b26
 1008 06c0 05       		.uleb128 0x5
 1009 06c1 09       		.byte	0x9
 1010 06c2 35       		.byte	0x35
 1011 06c3 B51B0000 		.long	0x1bb5
 1012 06c7 05       		.uleb128 0x5
 1013 06c8 09       		.byte	0x9
 1014 06c9 36       		.byte	0x36
 1015 06ca C01B0000 		.long	0x1bc0
 1016 06ce 05       		.uleb128 0x5
 1017 06cf 09       		.byte	0x9
 1018 06d0 37       		.byte	0x37
 1019 06d1 CB1B0000 		.long	0x1bcb
 1020 06d5 05       		.uleb128 0x5
 1021 06d6 09       		.byte	0x9
 1022 06d7 38       		.byte	0x38
 1023 06d8 D61B0000 		.long	0x1bd6
 1024 06dc 05       		.uleb128 0x5
 1025 06dd 09       		.byte	0x9
 1026 06de 3A       		.byte	0x3a
 1027 06df 5D1B0000 		.long	0x1b5d
 1028 06e3 05       		.uleb128 0x5
 1029 06e4 09       		.byte	0x9
 1030 06e5 3B       		.byte	0x3b
 1031 06e6 681B0000 		.long	0x1b68
 1032 06ea 05       		.uleb128 0x5
 1033 06eb 09       		.byte	0x9
 1034 06ec 3C       		.byte	0x3c
 1035 06ed 731B0000 		.long	0x1b73
 1036 06f1 05       		.uleb128 0x5
 1037 06f2 09       		.byte	0x9
 1038 06f3 3D       		.byte	0x3d
 1039 06f4 7E1B0000 		.long	0x1b7e
 1040 06f8 05       		.uleb128 0x5
 1041 06f9 09       		.byte	0x9
 1042 06fa 3F       		.byte	0x3f
 1043 06fb 231C0000 		.long	0x1c23
 1044 06ff 05       		.uleb128 0x5
 1045 0700 09       		.byte	0x9
 1046 0701 40       		.byte	0x40
 1047 0702 0D1C0000 		.long	0x1c0d
 1048 0706 05       		.uleb128 0x5
 1049 0707 09       		.byte	0x9
 1050 0708 42       		.byte	0x42
 1051 0709 311B0000 		.long	0x1b31
 1052 070d 05       		.uleb128 0x5
GAS LISTING /tmp/ccMbRvxK.s 			page 22


 1053 070e 09       		.byte	0x9
 1054 070f 43       		.byte	0x43
 1055 0710 3C1B0000 		.long	0x1b3c
 1056 0714 05       		.uleb128 0x5
 1057 0715 09       		.byte	0x9
 1058 0716 44       		.byte	0x44
 1059 0717 471B0000 		.long	0x1b47
 1060 071b 05       		.uleb128 0x5
 1061 071c 09       		.byte	0x9
 1062 071d 45       		.byte	0x45
 1063 071e 521B0000 		.long	0x1b52
 1064 0722 05       		.uleb128 0x5
 1065 0723 09       		.byte	0x9
 1066 0724 47       		.byte	0x47
 1067 0725 E11B0000 		.long	0x1be1
 1068 0729 05       		.uleb128 0x5
 1069 072a 09       		.byte	0x9
 1070 072b 48       		.byte	0x48
 1071 072c EC1B0000 		.long	0x1bec
 1072 0730 05       		.uleb128 0x5
 1073 0731 09       		.byte	0x9
 1074 0732 49       		.byte	0x49
 1075 0733 F71B0000 		.long	0x1bf7
 1076 0737 05       		.uleb128 0x5
 1077 0738 09       		.byte	0x9
 1078 0739 4A       		.byte	0x4a
 1079 073a 021C0000 		.long	0x1c02
 1080 073e 05       		.uleb128 0x5
 1081 073f 09       		.byte	0x9
 1082 0740 4C       		.byte	0x4c
 1083 0741 891B0000 		.long	0x1b89
 1084 0745 05       		.uleb128 0x5
 1085 0746 09       		.byte	0x9
 1086 0747 4D       		.byte	0x4d
 1087 0748 941B0000 		.long	0x1b94
 1088 074c 05       		.uleb128 0x5
 1089 074d 09       		.byte	0x9
 1090 074e 4E       		.byte	0x4e
 1091 074f 9F1B0000 		.long	0x1b9f
 1092 0753 05       		.uleb128 0x5
 1093 0754 09       		.byte	0x9
 1094 0755 4F       		.byte	0x4f
 1095 0756 AA1B0000 		.long	0x1baa
 1096 075a 05       		.uleb128 0x5
 1097 075b 09       		.byte	0x9
 1098 075c 51       		.byte	0x51
 1099 075d 2E1C0000 		.long	0x1c2e
 1100 0761 05       		.uleb128 0x5
 1101 0762 09       		.byte	0x9
 1102 0763 52       		.byte	0x52
 1103 0764 181C0000 		.long	0x1c18
 1104 0768 05       		.uleb128 0x5
 1105 0769 0A       		.byte	0xa
 1106 076a 35       		.byte	0x35
 1107 076b 471C0000 		.long	0x1c47
 1108 076f 05       		.uleb128 0x5
 1109 0770 0A       		.byte	0xa
GAS LISTING /tmp/ccMbRvxK.s 			page 23


 1110 0771 36       		.byte	0x36
 1111 0772 741D0000 		.long	0x1d74
 1112 0776 05       		.uleb128 0x5
 1113 0777 0A       		.byte	0xa
 1114 0778 37       		.byte	0x37
 1115 0779 8E1D0000 		.long	0x1d8e
 1116 077d 14       		.uleb128 0x14
 1117 077e 00000000 		.long	.LASF58
 1118 0782 05       		.byte	0x5
 1119 0783 C5       		.byte	0xc5
 1120 0784 61180000 		.long	0x1861
 1121 0788 05       		.uleb128 0x5
 1122 0789 0B       		.byte	0xb
 1123 078a 76       		.byte	0x76
 1124 078b F71D0000 		.long	0x1df7
 1125 078f 05       		.uleb128 0x5
 1126 0790 0B       		.byte	0xb
 1127 0791 77       		.byte	0x77
 1128 0792 271E0000 		.long	0x1e27
 1129 0796 05       		.uleb128 0x5
 1130 0797 0B       		.byte	0xb
 1131 0798 7B       		.byte	0x7b
 1132 0799 881E0000 		.long	0x1e88
 1133 079d 05       		.uleb128 0x5
 1134 079e 0B       		.byte	0xb
 1135 079f 7E       		.byte	0x7e
 1136 07a0 A51E0000 		.long	0x1ea5
 1137 07a4 05       		.uleb128 0x5
 1138 07a5 0B       		.byte	0xb
 1139 07a6 81       		.byte	0x81
 1140 07a7 BF1E0000 		.long	0x1ebf
 1141 07ab 05       		.uleb128 0x5
 1142 07ac 0B       		.byte	0xb
 1143 07ad 82       		.byte	0x82
 1144 07ae D41E0000 		.long	0x1ed4
 1145 07b2 05       		.uleb128 0x5
 1146 07b3 0B       		.byte	0xb
 1147 07b4 83       		.byte	0x83
 1148 07b5 E91E0000 		.long	0x1ee9
 1149 07b9 05       		.uleb128 0x5
 1150 07ba 0B       		.byte	0xb
 1151 07bb 84       		.byte	0x84
 1152 07bc FE1E0000 		.long	0x1efe
 1153 07c0 05       		.uleb128 0x5
 1154 07c1 0B       		.byte	0xb
 1155 07c2 86       		.byte	0x86
 1156 07c3 281F0000 		.long	0x1f28
 1157 07c7 05       		.uleb128 0x5
 1158 07c8 0B       		.byte	0xb
 1159 07c9 89       		.byte	0x89
 1160 07ca 431F0000 		.long	0x1f43
 1161 07ce 05       		.uleb128 0x5
 1162 07cf 0B       		.byte	0xb
 1163 07d0 8B       		.byte	0x8b
 1164 07d1 591F0000 		.long	0x1f59
 1165 07d5 05       		.uleb128 0x5
 1166 07d6 0B       		.byte	0xb
GAS LISTING /tmp/ccMbRvxK.s 			page 24


 1167 07d7 8E       		.byte	0x8e
 1168 07d8 741F0000 		.long	0x1f74
 1169 07dc 05       		.uleb128 0x5
 1170 07dd 0B       		.byte	0xb
 1171 07de 8F       		.byte	0x8f
 1172 07df 8F1F0000 		.long	0x1f8f
 1173 07e3 05       		.uleb128 0x5
 1174 07e4 0B       		.byte	0xb
 1175 07e5 90       		.byte	0x90
 1176 07e6 AF1F0000 		.long	0x1faf
 1177 07ea 05       		.uleb128 0x5
 1178 07eb 0B       		.byte	0xb
 1179 07ec 92       		.byte	0x92
 1180 07ed CF1F0000 		.long	0x1fcf
 1181 07f1 05       		.uleb128 0x5
 1182 07f2 0B       		.byte	0xb
 1183 07f3 95       		.byte	0x95
 1184 07f4 F01F0000 		.long	0x1ff0
 1185 07f8 05       		.uleb128 0x5
 1186 07f9 0B       		.byte	0xb
 1187 07fa 98       		.byte	0x98
 1188 07fb 02200000 		.long	0x2002
 1189 07ff 05       		.uleb128 0x5
 1190 0800 0B       		.byte	0xb
 1191 0801 9A       		.byte	0x9a
 1192 0802 0E200000 		.long	0x200e
 1193 0806 05       		.uleb128 0x5
 1194 0807 0B       		.byte	0xb
 1195 0808 9B       		.byte	0x9b
 1196 0809 20200000 		.long	0x2020
 1197 080d 05       		.uleb128 0x5
 1198 080e 0B       		.byte	0xb
 1199 080f 9C       		.byte	0x9c
 1200 0810 40200000 		.long	0x2040
 1201 0814 05       		.uleb128 0x5
 1202 0815 0B       		.byte	0xb
 1203 0816 9D       		.byte	0x9d
 1204 0817 5F200000 		.long	0x205f
 1205 081b 05       		.uleb128 0x5
 1206 081c 0B       		.byte	0xb
 1207 081d 9E       		.byte	0x9e
 1208 081e 7E200000 		.long	0x207e
 1209 0822 05       		.uleb128 0x5
 1210 0823 0B       		.byte	0xb
 1211 0824 A0       		.byte	0xa0
 1212 0825 94200000 		.long	0x2094
 1213 0829 05       		.uleb128 0x5
 1214 082a 0B       		.byte	0xb
 1215 082b A1       		.byte	0xa1
 1216 082c B4200000 		.long	0x20b4
 1217 0830 05       		.uleb128 0x5
 1218 0831 0B       		.byte	0xb
 1219 0832 FE       		.byte	0xfe
 1220 0833 571E0000 		.long	0x1e57
 1221 0837 06       		.uleb128 0x6
 1222 0838 0B       		.byte	0xb
 1223 0839 0301     		.value	0x103
GAS LISTING /tmp/ccMbRvxK.s 			page 25


 1224 083b 4A0E0000 		.long	0xe4a
 1225 083f 06       		.uleb128 0x6
 1226 0840 0B       		.byte	0xb
 1227 0841 0401     		.value	0x104
 1228 0843 CF200000 		.long	0x20cf
 1229 0847 06       		.uleb128 0x6
 1230 0848 0B       		.byte	0xb
 1231 0849 0601     		.value	0x106
 1232 084b EA200000 		.long	0x20ea
 1233 084f 06       		.uleb128 0x6
 1234 0850 0B       		.byte	0xb
 1235 0851 0701     		.value	0x107
 1236 0853 3D210000 		.long	0x213d
 1237 0857 06       		.uleb128 0x6
 1238 0858 0B       		.byte	0xb
 1239 0859 0801     		.value	0x108
 1240 085b FF200000 		.long	0x20ff
 1241 085f 06       		.uleb128 0x6
 1242 0860 0B       		.byte	0xb
 1243 0861 0901     		.value	0x109
 1244 0863 1E210000 		.long	0x211e
 1245 0867 06       		.uleb128 0x6
 1246 0868 0B       		.byte	0xb
 1247 0869 0A01     		.value	0x10a
 1248 086b 57210000 		.long	0x2157
 1249 086f 05       		.uleb128 0x5
 1250 0870 0C       		.byte	0xc
 1251 0871 62       		.byte	0x62
 1252 0872 33100000 		.long	0x1033
 1253 0876 05       		.uleb128 0x5
 1254 0877 0C       		.byte	0xc
 1255 0878 63       		.byte	0x63
 1256 0879 0B220000 		.long	0x220b
 1257 087d 05       		.uleb128 0x5
 1258 087e 0C       		.byte	0xc
 1259 087f 65       		.byte	0x65
 1260 0880 16220000 		.long	0x2216
 1261 0884 05       		.uleb128 0x5
 1262 0885 0C       		.byte	0xc
 1263 0886 66       		.byte	0x66
 1264 0887 2E220000 		.long	0x222e
 1265 088b 05       		.uleb128 0x5
 1266 088c 0C       		.byte	0xc
 1267 088d 67       		.byte	0x67
 1268 088e 43220000 		.long	0x2243
 1269 0892 05       		.uleb128 0x5
 1270 0893 0C       		.byte	0xc
 1271 0894 68       		.byte	0x68
 1272 0895 59220000 		.long	0x2259
 1273 0899 05       		.uleb128 0x5
 1274 089a 0C       		.byte	0xc
 1275 089b 69       		.byte	0x69
 1276 089c 6F220000 		.long	0x226f
 1277 08a0 05       		.uleb128 0x5
 1278 08a1 0C       		.byte	0xc
 1279 08a2 6A       		.byte	0x6a
 1280 08a3 84220000 		.long	0x2284
GAS LISTING /tmp/ccMbRvxK.s 			page 26


 1281 08a7 05       		.uleb128 0x5
 1282 08a8 0C       		.byte	0xc
 1283 08a9 6B       		.byte	0x6b
 1284 08aa 9A220000 		.long	0x229a
 1285 08ae 05       		.uleb128 0x5
 1286 08af 0C       		.byte	0xc
 1287 08b0 6C       		.byte	0x6c
 1288 08b1 BB220000 		.long	0x22bb
 1289 08b5 05       		.uleb128 0x5
 1290 08b6 0C       		.byte	0xc
 1291 08b7 6D       		.byte	0x6d
 1292 08b8 DB220000 		.long	0x22db
 1293 08bc 05       		.uleb128 0x5
 1294 08bd 0C       		.byte	0xc
 1295 08be 71       		.byte	0x71
 1296 08bf F6220000 		.long	0x22f6
 1297 08c3 05       		.uleb128 0x5
 1298 08c4 0C       		.byte	0xc
 1299 08c5 72       		.byte	0x72
 1300 08c6 1B230000 		.long	0x231b
 1301 08ca 05       		.uleb128 0x5
 1302 08cb 0C       		.byte	0xc
 1303 08cc 74       		.byte	0x74
 1304 08cd 3B230000 		.long	0x233b
 1305 08d1 05       		.uleb128 0x5
 1306 08d2 0C       		.byte	0xc
 1307 08d3 75       		.byte	0x75
 1308 08d4 5B230000 		.long	0x235b
 1309 08d8 05       		.uleb128 0x5
 1310 08d9 0C       		.byte	0xc
 1311 08da 76       		.byte	0x76
 1312 08db 81230000 		.long	0x2381
 1313 08df 05       		.uleb128 0x5
 1314 08e0 0C       		.byte	0xc
 1315 08e1 78       		.byte	0x78
 1316 08e2 97230000 		.long	0x2397
 1317 08e6 05       		.uleb128 0x5
 1318 08e7 0C       		.byte	0xc
 1319 08e8 79       		.byte	0x79
 1320 08e9 AD230000 		.long	0x23ad
 1321 08ed 05       		.uleb128 0x5
 1322 08ee 0C       		.byte	0xc
 1323 08ef 7C       		.byte	0x7c
 1324 08f0 B9230000 		.long	0x23b9
 1325 08f4 05       		.uleb128 0x5
 1326 08f5 0C       		.byte	0xc
 1327 08f6 7E       		.byte	0x7e
 1328 08f7 CF230000 		.long	0x23cf
 1329 08fb 05       		.uleb128 0x5
 1330 08fc 0C       		.byte	0xc
 1331 08fd 83       		.byte	0x83
 1332 08fe E1230000 		.long	0x23e1
 1333 0902 05       		.uleb128 0x5
 1334 0903 0C       		.byte	0xc
 1335 0904 84       		.byte	0x84
 1336 0905 F6230000 		.long	0x23f6
 1337 0909 05       		.uleb128 0x5
GAS LISTING /tmp/ccMbRvxK.s 			page 27


 1338 090a 0C       		.byte	0xc
 1339 090b 85       		.byte	0x85
 1340 090c 10240000 		.long	0x2410
 1341 0910 05       		.uleb128 0x5
 1342 0911 0C       		.byte	0xc
 1343 0912 87       		.byte	0x87
 1344 0913 22240000 		.long	0x2422
 1345 0917 05       		.uleb128 0x5
 1346 0918 0C       		.byte	0xc
 1347 0919 88       		.byte	0x88
 1348 091a 39240000 		.long	0x2439
 1349 091e 05       		.uleb128 0x5
 1350 091f 0C       		.byte	0xc
 1351 0920 8B       		.byte	0x8b
 1352 0921 5E240000 		.long	0x245e
 1353 0925 05       		.uleb128 0x5
 1354 0926 0C       		.byte	0xc
 1355 0927 8D       		.byte	0x8d
 1356 0928 69240000 		.long	0x2469
 1357 092c 05       		.uleb128 0x5
 1358 092d 0C       		.byte	0xc
 1359 092e 8F       		.byte	0x8f
 1360 092f 7E240000 		.long	0x247e
 1361 0933 20       		.uleb128 0x20
 1362 0934 5F563200 		.string	"_V2"
 1363 0938 0D       		.byte	0xd
 1364 0939 3F       		.byte	0x3f
 1365 093a 04       		.uleb128 0x4
 1366 093b 0D       		.byte	0xd
 1367 093c 3F       		.byte	0x3f
 1368 093d 33090000 		.long	0x933
 1369 0941 21       		.uleb128 0x21
 1370 0942 00000000 		.long	.LASF80
 1371 0946 04       		.byte	0x4
 1372 0947 8C120000 		.long	0x128c
 1373 094b 0E       		.byte	0xe
 1374 094c 39       		.byte	0x39
 1375 094d E2090000 		.long	0x9e2
 1376 0951 22       		.uleb128 0x22
 1377 0952 00000000 		.long	.LASF59
 1378 0956 01       		.byte	0x1
 1379 0957 22       		.uleb128 0x22
 1380 0958 00000000 		.long	.LASF60
 1381 095c 02       		.byte	0x2
 1382 095d 22       		.uleb128 0x22
 1383 095e 00000000 		.long	.LASF61
 1384 0962 04       		.byte	0x4
 1385 0963 22       		.uleb128 0x22
 1386 0964 00000000 		.long	.LASF62
 1387 0968 08       		.byte	0x8
 1388 0969 22       		.uleb128 0x22
 1389 096a 00000000 		.long	.LASF63
 1390 096e 10       		.byte	0x10
 1391 096f 22       		.uleb128 0x22
 1392 0970 00000000 		.long	.LASF64
 1393 0974 20       		.byte	0x20
 1394 0975 22       		.uleb128 0x22
GAS LISTING /tmp/ccMbRvxK.s 			page 28


 1395 0976 00000000 		.long	.LASF65
 1396 097a 40       		.byte	0x40
 1397 097b 22       		.uleb128 0x22
 1398 097c 00000000 		.long	.LASF66
 1399 0980 80       		.byte	0x80
 1400 0981 23       		.uleb128 0x23
 1401 0982 00000000 		.long	.LASF67
 1402 0986 0001     		.value	0x100
 1403 0988 23       		.uleb128 0x23
 1404 0989 00000000 		.long	.LASF68
 1405 098d 0002     		.value	0x200
 1406 098f 23       		.uleb128 0x23
 1407 0990 00000000 		.long	.LASF69
 1408 0994 0004     		.value	0x400
 1409 0996 23       		.uleb128 0x23
 1410 0997 00000000 		.long	.LASF70
 1411 099b 0008     		.value	0x800
 1412 099d 23       		.uleb128 0x23
 1413 099e 00000000 		.long	.LASF71
 1414 09a2 0010     		.value	0x1000
 1415 09a4 23       		.uleb128 0x23
 1416 09a5 00000000 		.long	.LASF72
 1417 09a9 0020     		.value	0x2000
 1418 09ab 23       		.uleb128 0x23
 1419 09ac 00000000 		.long	.LASF73
 1420 09b0 0040     		.value	0x4000
 1421 09b2 22       		.uleb128 0x22
 1422 09b3 00000000 		.long	.LASF74
 1423 09b7 B0       		.byte	0xb0
 1424 09b8 22       		.uleb128 0x22
 1425 09b9 00000000 		.long	.LASF75
 1426 09bd 4A       		.byte	0x4a
 1427 09be 23       		.uleb128 0x23
 1428 09bf 00000000 		.long	.LASF76
 1429 09c3 0401     		.value	0x104
 1430 09c5 24       		.uleb128 0x24
 1431 09c6 00000000 		.long	.LASF77
 1432 09ca 00000100 		.long	0x10000
 1433 09ce 24       		.uleb128 0x24
 1434 09cf 00000000 		.long	.LASF78
 1435 09d3 FFFFFF7F 		.long	0x7fffffff
 1436 09d7 25       		.uleb128 0x25
 1437 09d8 00000000 		.long	.LASF79
 1438 09dc 80808080 		.sleb128 -2147483648
 1438      78
 1439 09e1 00       		.byte	0
 1440 09e2 21       		.uleb128 0x21
 1441 09e3 00000000 		.long	.LASF81
 1442 09e7 04       		.byte	0x4
 1443 09e8 8C120000 		.long	0x128c
 1444 09ec 0E       		.byte	0xe
 1445 09ed 6F       		.byte	0x6f
 1446 09ee 330A0000 		.long	0xa33
 1447 09f2 22       		.uleb128 0x22
 1448 09f3 00000000 		.long	.LASF82
 1449 09f7 01       		.byte	0x1
 1450 09f8 22       		.uleb128 0x22
GAS LISTING /tmp/ccMbRvxK.s 			page 29


 1451 09f9 00000000 		.long	.LASF83
 1452 09fd 02       		.byte	0x2
 1453 09fe 22       		.uleb128 0x22
 1454 09ff 00000000 		.long	.LASF84
 1455 0a03 04       		.byte	0x4
 1456 0a04 22       		.uleb128 0x22
 1457 0a05 00000000 		.long	.LASF85
 1458 0a09 08       		.byte	0x8
 1459 0a0a 22       		.uleb128 0x22
 1460 0a0b 00000000 		.long	.LASF86
 1461 0a0f 10       		.byte	0x10
 1462 0a10 22       		.uleb128 0x22
 1463 0a11 00000000 		.long	.LASF87
 1464 0a15 20       		.byte	0x20
 1465 0a16 24       		.uleb128 0x24
 1466 0a17 00000000 		.long	.LASF88
 1467 0a1b 00000100 		.long	0x10000
 1468 0a1f 24       		.uleb128 0x24
 1469 0a20 00000000 		.long	.LASF89
 1470 0a24 FFFFFF7F 		.long	0x7fffffff
 1471 0a28 25       		.uleb128 0x25
 1472 0a29 00000000 		.long	.LASF90
 1473 0a2d 80808080 		.sleb128 -2147483648
 1473      78
 1474 0a32 00       		.byte	0
 1475 0a33 21       		.uleb128 0x21
 1476 0a34 00000000 		.long	.LASF91
 1477 0a38 04       		.byte	0x4
 1478 0a39 8C120000 		.long	0x128c
 1479 0a3d 0E       		.byte	0xe
 1480 0a3e 99       		.byte	0x99
 1481 0a3f 780A0000 		.long	0xa78
 1482 0a43 22       		.uleb128 0x22
 1483 0a44 00000000 		.long	.LASF92
 1484 0a48 00       		.byte	0
 1485 0a49 22       		.uleb128 0x22
 1486 0a4a 00000000 		.long	.LASF93
 1487 0a4e 01       		.byte	0x1
 1488 0a4f 22       		.uleb128 0x22
 1489 0a50 00000000 		.long	.LASF94
 1490 0a54 02       		.byte	0x2
 1491 0a55 22       		.uleb128 0x22
 1492 0a56 00000000 		.long	.LASF95
 1493 0a5a 04       		.byte	0x4
 1494 0a5b 24       		.uleb128 0x24
 1495 0a5c 00000000 		.long	.LASF96
 1496 0a60 00000100 		.long	0x10000
 1497 0a64 24       		.uleb128 0x24
 1498 0a65 00000000 		.long	.LASF97
 1499 0a69 FFFFFF7F 		.long	0x7fffffff
 1500 0a6d 25       		.uleb128 0x25
 1501 0a6e 00000000 		.long	.LASF98
 1502 0a72 80808080 		.sleb128 -2147483648
 1502      78
 1503 0a77 00       		.byte	0
 1504 0a78 21       		.uleb128 0x21
 1505 0a79 00000000 		.long	.LASF99
GAS LISTING /tmp/ccMbRvxK.s 			page 30


 1506 0a7d 04       		.byte	0x4
 1507 0a7e 0A120000 		.long	0x120a
 1508 0a82 0E       		.byte	0xe
 1509 0a83 C1       		.byte	0xc1
 1510 0a84 A40A0000 		.long	0xaa4
 1511 0a88 22       		.uleb128 0x22
 1512 0a89 00000000 		.long	.LASF100
 1513 0a8d 00       		.byte	0
 1514 0a8e 22       		.uleb128 0x22
 1515 0a8f 00000000 		.long	.LASF101
 1516 0a93 01       		.byte	0x1
 1517 0a94 22       		.uleb128 0x22
 1518 0a95 00000000 		.long	.LASF102
 1519 0a99 02       		.byte	0x2
 1520 0a9a 24       		.uleb128 0x24
 1521 0a9b 00000000 		.long	.LASF103
 1522 0a9f 00000100 		.long	0x10000
 1523 0aa3 00       		.byte	0
 1524 0aa4 26       		.uleb128 0x26
 1525 0aa5 00000000 		.long	.LASF134
 1526 0aa9 0D0D0000 		.long	0xd0d
 1527 0aad 27       		.uleb128 0x27
 1528 0aae 00000000 		.long	.LASF106
 1529 0ab2 01       		.byte	0x1
 1530 0ab3 0E       		.byte	0xe
 1531 0ab4 5902     		.value	0x259
 1532 0ab6 01       		.byte	0x1
 1533 0ab7 0B0B0000 		.long	0xb0b
 1534 0abb 28       		.uleb128 0x28
 1535 0abc 00000000 		.long	.LASF104
 1536 0ac0 0E       		.byte	0xe
 1537 0ac1 6102     		.value	0x261
 1538 0ac3 C01D0000 		.long	0x1dc0
 1539 0ac7 28       		.uleb128 0x28
 1540 0ac8 00000000 		.long	.LASF105
 1541 0acc 0E       		.byte	0xe
 1542 0acd 6202     		.value	0x262
 1543 0acf 9C1A0000 		.long	0x1a9c
 1544 0ad3 29       		.uleb128 0x29
 1545 0ad4 00000000 		.long	.LASF106
 1546 0ad8 0E       		.byte	0xe
 1547 0ad9 5D02     		.value	0x25d
 1548 0adb 00000000 		.long	.LASF456
 1549 0adf 01       		.byte	0x1
 1550 0ae0 E80A0000 		.long	0xae8
 1551 0ae4 EE0A0000 		.long	0xaee
 1552 0ae8 0B       		.uleb128 0xb
 1553 0ae9 99240000 		.long	0x2499
 1554 0aed 00       		.byte	0
 1555 0aee 2A       		.uleb128 0x2a
 1556 0aef 00000000 		.long	.LASF107
 1557 0af3 0E       		.byte	0xe
 1558 0af4 5E02     		.value	0x25e
 1559 0af6 00000000 		.long	.LASF108
 1560 0afa 01       		.byte	0x1
 1561 0afb FF0A0000 		.long	0xaff
 1562 0aff 0B       		.uleb128 0xb
GAS LISTING /tmp/ccMbRvxK.s 			page 31


 1563 0b00 99240000 		.long	0x2499
 1564 0b04 0B       		.uleb128 0xb
 1565 0b05 8C120000 		.long	0x128c
 1566 0b09 00       		.byte	0
 1567 0b0a 00       		.byte	0
 1568 0b0b 2B       		.uleb128 0x2b
 1569 0b0c 00000000 		.long	.LASF124
 1570 0b10 0E       		.byte	0xe
 1571 0b11 4301     		.value	0x143
 1572 0b13 41090000 		.long	0x941
 1573 0b17 01       		.byte	0x1
 1574 0b18 2C       		.uleb128 0x2c
 1575 0b19 00000000 		.long	.LASF109
 1576 0b1d 0E       		.byte	0xe
 1577 0b1e 4601     		.value	0x146
 1578 0b20 260B0000 		.long	0xb26
 1579 0b24 01       		.byte	0x1
 1580 0b25 01       		.byte	0x1
 1581 0b26 13       		.uleb128 0x13
 1582 0b27 0B0B0000 		.long	0xb0b
 1583 0b2b 2D       		.uleb128 0x2d
 1584 0b2c 64656300 		.string	"dec"
 1585 0b30 0E       		.byte	0xe
 1586 0b31 4901     		.value	0x149
 1587 0b33 260B0000 		.long	0xb26
 1588 0b37 01       		.byte	0x1
 1589 0b38 02       		.byte	0x2
 1590 0b39 2C       		.uleb128 0x2c
 1591 0b3a 00000000 		.long	.LASF110
 1592 0b3e 0E       		.byte	0xe
 1593 0b3f 4C01     		.value	0x14c
 1594 0b41 260B0000 		.long	0xb26
 1595 0b45 01       		.byte	0x1
 1596 0b46 04       		.byte	0x4
 1597 0b47 2D       		.uleb128 0x2d
 1598 0b48 68657800 		.string	"hex"
 1599 0b4c 0E       		.byte	0xe
 1600 0b4d 4F01     		.value	0x14f
 1601 0b4f 260B0000 		.long	0xb26
 1602 0b53 01       		.byte	0x1
 1603 0b54 08       		.byte	0x8
 1604 0b55 2C       		.uleb128 0x2c
 1605 0b56 00000000 		.long	.LASF111
 1606 0b5a 0E       		.byte	0xe
 1607 0b5b 5401     		.value	0x154
 1608 0b5d 260B0000 		.long	0xb26
 1609 0b61 01       		.byte	0x1
 1610 0b62 10       		.byte	0x10
 1611 0b63 2C       		.uleb128 0x2c
 1612 0b64 00000000 		.long	.LASF112
 1613 0b68 0E       		.byte	0xe
 1614 0b69 5801     		.value	0x158
 1615 0b6b 260B0000 		.long	0xb26
 1616 0b6f 01       		.byte	0x1
 1617 0b70 20       		.byte	0x20
 1618 0b71 2D       		.uleb128 0x2d
 1619 0b72 6F637400 		.string	"oct"
GAS LISTING /tmp/ccMbRvxK.s 			page 32


 1620 0b76 0E       		.byte	0xe
 1621 0b77 5B01     		.value	0x15b
 1622 0b79 260B0000 		.long	0xb26
 1623 0b7d 01       		.byte	0x1
 1624 0b7e 40       		.byte	0x40
 1625 0b7f 2C       		.uleb128 0x2c
 1626 0b80 00000000 		.long	.LASF113
 1627 0b84 0E       		.byte	0xe
 1628 0b85 5F01     		.value	0x15f
 1629 0b87 260B0000 		.long	0xb26
 1630 0b8b 01       		.byte	0x1
 1631 0b8c 80       		.byte	0x80
 1632 0b8d 2E       		.uleb128 0x2e
 1633 0b8e 00000000 		.long	.LASF114
 1634 0b92 0E       		.byte	0xe
 1635 0b93 6201     		.value	0x162
 1636 0b95 260B0000 		.long	0xb26
 1637 0b99 01       		.byte	0x1
 1638 0b9a 0001     		.value	0x100
 1639 0b9c 2E       		.uleb128 0x2e
 1640 0b9d 00000000 		.long	.LASF115
 1641 0ba1 0E       		.byte	0xe
 1642 0ba2 6601     		.value	0x166
 1643 0ba4 260B0000 		.long	0xb26
 1644 0ba8 01       		.byte	0x1
 1645 0ba9 0002     		.value	0x200
 1646 0bab 2E       		.uleb128 0x2e
 1647 0bac 00000000 		.long	.LASF116
 1648 0bb0 0E       		.byte	0xe
 1649 0bb1 6A01     		.value	0x16a
 1650 0bb3 260B0000 		.long	0xb26
 1651 0bb7 01       		.byte	0x1
 1652 0bb8 0004     		.value	0x400
 1653 0bba 2E       		.uleb128 0x2e
 1654 0bbb 00000000 		.long	.LASF117
 1655 0bbf 0E       		.byte	0xe
 1656 0bc0 6D01     		.value	0x16d
 1657 0bc2 260B0000 		.long	0xb26
 1658 0bc6 01       		.byte	0x1
 1659 0bc7 0008     		.value	0x800
 1660 0bc9 2E       		.uleb128 0x2e
 1661 0bca 00000000 		.long	.LASF118
 1662 0bce 0E       		.byte	0xe
 1663 0bcf 7001     		.value	0x170
 1664 0bd1 260B0000 		.long	0xb26
 1665 0bd5 01       		.byte	0x1
 1666 0bd6 0010     		.value	0x1000
 1667 0bd8 2E       		.uleb128 0x2e
 1668 0bd9 00000000 		.long	.LASF119
 1669 0bdd 0E       		.byte	0xe
 1670 0bde 7301     		.value	0x173
 1671 0be0 260B0000 		.long	0xb26
 1672 0be4 01       		.byte	0x1
 1673 0be5 0020     		.value	0x2000
 1674 0be7 2E       		.uleb128 0x2e
 1675 0be8 00000000 		.long	.LASF120
 1676 0bec 0E       		.byte	0xe
GAS LISTING /tmp/ccMbRvxK.s 			page 33


 1677 0bed 7701     		.value	0x177
 1678 0bef 260B0000 		.long	0xb26
 1679 0bf3 01       		.byte	0x1
 1680 0bf4 0040     		.value	0x4000
 1681 0bf6 2C       		.uleb128 0x2c
 1682 0bf7 00000000 		.long	.LASF121
 1683 0bfb 0E       		.byte	0xe
 1684 0bfc 7A01     		.value	0x17a
 1685 0bfe 260B0000 		.long	0xb26
 1686 0c02 01       		.byte	0x1
 1687 0c03 B0       		.byte	0xb0
 1688 0c04 2C       		.uleb128 0x2c
 1689 0c05 00000000 		.long	.LASF122
 1690 0c09 0E       		.byte	0xe
 1691 0c0a 7D01     		.value	0x17d
 1692 0c0c 260B0000 		.long	0xb26
 1693 0c10 01       		.byte	0x1
 1694 0c11 4A       		.byte	0x4a
 1695 0c12 2E       		.uleb128 0x2e
 1696 0c13 00000000 		.long	.LASF123
 1697 0c17 0E       		.byte	0xe
 1698 0c18 8001     		.value	0x180
 1699 0c1a 260B0000 		.long	0xb26
 1700 0c1e 01       		.byte	0x1
 1701 0c1f 0401     		.value	0x104
 1702 0c21 2B       		.uleb128 0x2b
 1703 0c22 00000000 		.long	.LASF125
 1704 0c26 0E       		.byte	0xe
 1705 0c27 8E01     		.value	0x18e
 1706 0c29 330A0000 		.long	0xa33
 1707 0c2d 01       		.byte	0x1
 1708 0c2e 2C       		.uleb128 0x2c
 1709 0c2f 00000000 		.long	.LASF126
 1710 0c33 0E       		.byte	0xe
 1711 0c34 9201     		.value	0x192
 1712 0c36 3C0C0000 		.long	0xc3c
 1713 0c3a 01       		.byte	0x1
 1714 0c3b 01       		.byte	0x1
 1715 0c3c 13       		.uleb128 0x13
 1716 0c3d 210C0000 		.long	0xc21
 1717 0c41 2C       		.uleb128 0x2c
 1718 0c42 00000000 		.long	.LASF127
 1719 0c46 0E       		.byte	0xe
 1720 0c47 9501     		.value	0x195
 1721 0c49 3C0C0000 		.long	0xc3c
 1722 0c4d 01       		.byte	0x1
 1723 0c4e 02       		.byte	0x2
 1724 0c4f 2C       		.uleb128 0x2c
 1725 0c50 00000000 		.long	.LASF128
 1726 0c54 0E       		.byte	0xe
 1727 0c55 9A01     		.value	0x19a
 1728 0c57 3C0C0000 		.long	0xc3c
 1729 0c5b 01       		.byte	0x1
 1730 0c5c 04       		.byte	0x4
 1731 0c5d 2C       		.uleb128 0x2c
 1732 0c5e 00000000 		.long	.LASF129
 1733 0c62 0E       		.byte	0xe
GAS LISTING /tmp/ccMbRvxK.s 			page 34


 1734 0c63 9D01     		.value	0x19d
 1735 0c65 3C0C0000 		.long	0xc3c
 1736 0c69 01       		.byte	0x1
 1737 0c6a 00       		.byte	0
 1738 0c6b 2B       		.uleb128 0x2b
 1739 0c6c 00000000 		.long	.LASF130
 1740 0c70 0E       		.byte	0xe
 1741 0c71 AD01     		.value	0x1ad
 1742 0c73 E2090000 		.long	0x9e2
 1743 0c77 01       		.byte	0x1
 1744 0c78 2D       		.uleb128 0x2d
 1745 0c79 61707000 		.string	"app"
 1746 0c7d 0E       		.byte	0xe
 1747 0c7e B001     		.value	0x1b0
 1748 0c80 860C0000 		.long	0xc86
 1749 0c84 01       		.byte	0x1
 1750 0c85 01       		.byte	0x1
 1751 0c86 13       		.uleb128 0x13
 1752 0c87 6B0C0000 		.long	0xc6b
 1753 0c8b 2D       		.uleb128 0x2d
 1754 0c8c 61746500 		.string	"ate"
 1755 0c90 0E       		.byte	0xe
 1756 0c91 B301     		.value	0x1b3
 1757 0c93 860C0000 		.long	0xc86
 1758 0c97 01       		.byte	0x1
 1759 0c98 02       		.byte	0x2
 1760 0c99 2C       		.uleb128 0x2c
 1761 0c9a 00000000 		.long	.LASF131
 1762 0c9e 0E       		.byte	0xe
 1763 0c9f B801     		.value	0x1b8
 1764 0ca1 860C0000 		.long	0xc86
 1765 0ca5 01       		.byte	0x1
 1766 0ca6 04       		.byte	0x4
 1767 0ca7 2D       		.uleb128 0x2d
 1768 0ca8 696E00   		.string	"in"
 1769 0cab 0E       		.byte	0xe
 1770 0cac BB01     		.value	0x1bb
 1771 0cae 860C0000 		.long	0xc86
 1772 0cb2 01       		.byte	0x1
 1773 0cb3 08       		.byte	0x8
 1774 0cb4 2D       		.uleb128 0x2d
 1775 0cb5 6F757400 		.string	"out"
 1776 0cb9 0E       		.byte	0xe
 1777 0cba BE01     		.value	0x1be
 1778 0cbc 860C0000 		.long	0xc86
 1779 0cc0 01       		.byte	0x1
 1780 0cc1 10       		.byte	0x10
 1781 0cc2 2C       		.uleb128 0x2c
 1782 0cc3 00000000 		.long	.LASF132
 1783 0cc7 0E       		.byte	0xe
 1784 0cc8 C101     		.value	0x1c1
 1785 0cca 860C0000 		.long	0xc86
 1786 0cce 01       		.byte	0x1
 1787 0ccf 20       		.byte	0x20
 1788 0cd0 2B       		.uleb128 0x2b
 1789 0cd1 00000000 		.long	.LASF133
 1790 0cd5 0E       		.byte	0xe
GAS LISTING /tmp/ccMbRvxK.s 			page 35


 1791 0cd6 CD01     		.value	0x1cd
 1792 0cd8 780A0000 		.long	0xa78
 1793 0cdc 01       		.byte	0x1
 1794 0cdd 2D       		.uleb128 0x2d
 1795 0cde 62656700 		.string	"beg"
 1796 0ce2 0E       		.byte	0xe
 1797 0ce3 D001     		.value	0x1d0
 1798 0ce5 EB0C0000 		.long	0xceb
 1799 0ce9 01       		.byte	0x1
 1800 0cea 00       		.byte	0
 1801 0ceb 13       		.uleb128 0x13
 1802 0cec D00C0000 		.long	0xcd0
 1803 0cf0 2D       		.uleb128 0x2d
 1804 0cf1 63757200 		.string	"cur"
 1805 0cf5 0E       		.byte	0xe
 1806 0cf6 D301     		.value	0x1d3
 1807 0cf8 EB0C0000 		.long	0xceb
 1808 0cfc 01       		.byte	0x1
 1809 0cfd 01       		.byte	0x1
 1810 0cfe 2D       		.uleb128 0x2d
 1811 0cff 656E6400 		.string	"end"
 1812 0d03 0E       		.byte	0xe
 1813 0d04 D601     		.value	0x1d6
 1814 0d06 EB0C0000 		.long	0xceb
 1815 0d0a 01       		.byte	0x1
 1816 0d0b 02       		.byte	0x2
 1817 0d0c 00       		.byte	0
 1818 0d0d 05       		.uleb128 0x5
 1819 0d0e 0F       		.byte	0xf
 1820 0d0f 52       		.byte	0x52
 1821 0d10 AA240000 		.long	0x24aa
 1822 0d14 05       		.uleb128 0x5
 1823 0d15 0F       		.byte	0xf
 1824 0d16 53       		.byte	0x53
 1825 0d17 9F240000 		.long	0x249f
 1826 0d1b 05       		.uleb128 0x5
 1827 0d1c 0F       		.byte	0xf
 1828 0d1d 54       		.byte	0x54
 1829 0d1e 25120000 		.long	0x1225
 1830 0d22 05       		.uleb128 0x5
 1831 0d23 0F       		.byte	0xf
 1832 0d24 5C       		.byte	0x5c
 1833 0d25 C0240000 		.long	0x24c0
 1834 0d29 05       		.uleb128 0x5
 1835 0d2a 0F       		.byte	0xf
 1836 0d2b 65       		.byte	0x65
 1837 0d2c DA240000 		.long	0x24da
 1838 0d30 05       		.uleb128 0x5
 1839 0d31 0F       		.byte	0xf
 1840 0d32 68       		.byte	0x68
 1841 0d33 F4240000 		.long	0x24f4
 1842 0d37 05       		.uleb128 0x5
 1843 0d38 0F       		.byte	0xf
 1844 0d39 69       		.byte	0x69
 1845 0d3a 09250000 		.long	0x2509
 1846 0d3e 26       		.uleb128 0x26
 1847 0d3f 00000000 		.long	.LASF135
GAS LISTING /tmp/ccMbRvxK.s 			page 36


 1848 0d43 5A0D0000 		.long	0xd5a
 1849 0d47 2F       		.uleb128 0x2f
 1850 0d48 00000000 		.long	.LASF136
 1851 0d4c 85120000 		.long	0x1285
 1852 0d50 30       		.uleb128 0x30
 1853 0d51 00000000 		.long	.LASF457
 1854 0d55 D1040000 		.long	0x4d1
 1855 0d59 00       		.byte	0
 1856 0d5a 31       		.uleb128 0x31
 1857 0d5b 00000000 		.long	.LASF458
 1858 0d5f 21       		.byte	0x21
 1859 0d60 4F       		.byte	0x4f
 1860 0d61 670D0000 		.long	0xd67
 1861 0d65 01       		.byte	0x1
 1862 0d66 00       		.byte	0
 1863 0d67 13       		.uleb128 0x13
 1864 0d68 C2040000 		.long	0x4c2
 1865 0d6c 14       		.uleb128 0x14
 1866 0d6d 00000000 		.long	.LASF137
 1867 0d71 10       		.byte	0x10
 1868 0d72 8D       		.byte	0x8d
 1869 0d73 3E0D0000 		.long	0xd3e
 1870 0d77 32       		.uleb128 0x32
 1871 0d78 00000000 		.long	.LASF459
 1872 0d7c 02       		.byte	0x2
 1873 0d7d 3D       		.byte	0x3d
 1874 0d7e 00000000 		.long	.LASF460
 1875 0d82 6C0D0000 		.long	0xd6c
 1876 0d86 33       		.uleb128 0x33
 1877 0d87 00000000 		.long	.LASF429
 1878 0d8b 02       		.byte	0x2
 1879 0d8c 4A       		.byte	0x4a
 1880 0d8d AD0A0000 		.long	0xaad
 1881 0d91 00       		.byte	0
 1882 0d92 07       		.uleb128 0x7
 1883 0d93 00000000 		.long	.LASF138
 1884 0d97 05       		.byte	0x5
 1885 0d98 DD       		.byte	0xdd
 1886 0d99 33100000 		.long	0x1033
 1887 0d9d 03       		.uleb128 0x3
 1888 0d9e 00000000 		.long	.LASF31
 1889 0da2 05       		.byte	0x5
 1890 0da3 DE       		.byte	0xde
 1891 0da4 04       		.uleb128 0x4
 1892 0da5 05       		.byte	0x5
 1893 0da6 DE       		.byte	0xde
 1894 0da7 9D0D0000 		.long	0xd9d
 1895 0dab 05       		.uleb128 0x5
 1896 0dac 03       		.byte	0x3
 1897 0dad F8       		.byte	0xf8
 1898 0dae 091A0000 		.long	0x1a09
 1899 0db2 06       		.uleb128 0x6
 1900 0db3 03       		.byte	0x3
 1901 0db4 0101     		.value	0x101
 1902 0db6 2B1A0000 		.long	0x1a2b
 1903 0dba 06       		.uleb128 0x6
 1904 0dbb 03       		.byte	0x3
GAS LISTING /tmp/ccMbRvxK.s 			page 37


 1905 0dbc 0201     		.value	0x102
 1906 0dbe 521A0000 		.long	0x1a52
 1907 0dc2 03       		.uleb128 0x3
 1908 0dc3 00000000 		.long	.LASF139
 1909 0dc7 11       		.byte	0x11
 1910 0dc8 24       		.byte	0x24
 1911 0dc9 05       		.uleb128 0x5
 1912 0dca 12       		.byte	0x12
 1913 0dcb 2C       		.byte	0x2c
 1914 0dcc 99060000 		.long	0x699
 1915 0dd0 05       		.uleb128 0x5
 1916 0dd1 12       		.byte	0x12
 1917 0dd2 2D       		.byte	0x2d
 1918 0dd3 7D070000 		.long	0x77d
 1919 0dd7 16       		.uleb128 0x16
 1920 0dd8 00000000 		.long	.LASF140
 1921 0ddc 01       		.byte	0x1
 1922 0ddd 13       		.byte	0x13
 1923 0dde 37       		.byte	0x37
 1924 0ddf 190E0000 		.long	0xe19
 1925 0de3 17       		.uleb128 0x17
 1926 0de4 00000000 		.long	.LASF141
 1927 0de8 13       		.byte	0x13
 1928 0de9 3A       		.byte	0x3a
 1929 0dea B0120000 		.long	0x12b0
 1930 0dee 17       		.uleb128 0x17
 1931 0def 00000000 		.long	.LASF142
 1932 0df3 13       		.byte	0x13
 1933 0df4 3B       		.byte	0x3b
 1934 0df5 B0120000 		.long	0x12b0
 1935 0df9 17       		.uleb128 0x17
 1936 0dfa 00000000 		.long	.LASF143
 1937 0dfe 13       		.byte	0x13
 1938 0dff 3F       		.byte	0x3f
 1939 0e00 BE1A0000 		.long	0x1abe
 1940 0e04 17       		.uleb128 0x17
 1941 0e05 00000000 		.long	.LASF144
 1942 0e09 13       		.byte	0x13
 1943 0e0a 40       		.byte	0x40
 1944 0e0b B0120000 		.long	0x12b0
 1945 0e0f 2F       		.uleb128 0x2f
 1946 0e10 00000000 		.long	.LASF145
 1947 0e14 8C120000 		.long	0x128c
 1948 0e18 00       		.byte	0
 1949 0e19 05       		.uleb128 0x5
 1950 0e1a 0B       		.byte	0xb
 1951 0e1b D6       		.byte	0xd6
 1952 0e1c 571E0000 		.long	0x1e57
 1953 0e20 05       		.uleb128 0x5
 1954 0e21 0B       		.byte	0xb
 1955 0e22 E6       		.byte	0xe6
 1956 0e23 CF200000 		.long	0x20cf
 1957 0e27 05       		.uleb128 0x5
 1958 0e28 0B       		.byte	0xb
 1959 0e29 F1       		.byte	0xf1
 1960 0e2a EA200000 		.long	0x20ea
 1961 0e2e 05       		.uleb128 0x5
GAS LISTING /tmp/ccMbRvxK.s 			page 38


 1962 0e2f 0B       		.byte	0xb
 1963 0e30 F2       		.byte	0xf2
 1964 0e31 FF200000 		.long	0x20ff
 1965 0e35 05       		.uleb128 0x5
 1966 0e36 0B       		.byte	0xb
 1967 0e37 F3       		.byte	0xf3
 1968 0e38 1E210000 		.long	0x211e
 1969 0e3c 05       		.uleb128 0x5
 1970 0e3d 0B       		.byte	0xb
 1971 0e3e F5       		.byte	0xf5
 1972 0e3f 3D210000 		.long	0x213d
 1973 0e43 05       		.uleb128 0x5
 1974 0e44 0B       		.byte	0xb
 1975 0e45 F6       		.byte	0xf6
 1976 0e46 57210000 		.long	0x2157
 1977 0e4a 1C       		.uleb128 0x1c
 1978 0e4b 64697600 		.string	"div"
 1979 0e4f 0B       		.byte	0xb
 1980 0e50 E3       		.byte	0xe3
 1981 0e51 00000000 		.long	.LASF146
 1982 0e55 571E0000 		.long	0x1e57
 1983 0e59 680E0000 		.long	0xe68
 1984 0e5d 0C       		.uleb128 0xc
 1985 0e5e 4B1A0000 		.long	0x1a4b
 1986 0e62 0C       		.uleb128 0xc
 1987 0e63 4B1A0000 		.long	0x1a4b
 1988 0e67 00       		.byte	0
 1989 0e68 16       		.uleb128 0x16
 1990 0e69 00000000 		.long	.LASF147
 1991 0e6d 01       		.byte	0x1
 1992 0e6e 13       		.byte	0x13
 1993 0e6f 64       		.byte	0x64
 1994 0e70 AA0E0000 		.long	0xeaa
 1995 0e74 17       		.uleb128 0x17
 1996 0e75 00000000 		.long	.LASF148
 1997 0e79 13       		.byte	0x13
 1998 0e7a 67       		.byte	0x67
 1999 0e7b B0120000 		.long	0x12b0
 2000 0e7f 17       		.uleb128 0x17
 2001 0e80 00000000 		.long	.LASF143
 2002 0e84 13       		.byte	0x13
 2003 0e85 6A       		.byte	0x6a
 2004 0e86 BE1A0000 		.long	0x1abe
 2005 0e8a 17       		.uleb128 0x17
 2006 0e8b 00000000 		.long	.LASF149
 2007 0e8f 13       		.byte	0x13
 2008 0e90 6B       		.byte	0x6b
 2009 0e91 B0120000 		.long	0x12b0
 2010 0e95 17       		.uleb128 0x17
 2011 0e96 00000000 		.long	.LASF150
 2012 0e9a 13       		.byte	0x13
 2013 0e9b 6C       		.byte	0x6c
 2014 0e9c B0120000 		.long	0x12b0
 2015 0ea0 2F       		.uleb128 0x2f
 2016 0ea1 00000000 		.long	.LASF145
 2017 0ea5 1A180000 		.long	0x181a
 2018 0ea9 00       		.byte	0
GAS LISTING /tmp/ccMbRvxK.s 			page 39


 2019 0eaa 16       		.uleb128 0x16
 2020 0eab 00000000 		.long	.LASF151
 2021 0eaf 01       		.byte	0x1
 2022 0eb0 13       		.byte	0x13
 2023 0eb1 64       		.byte	0x64
 2024 0eb2 EC0E0000 		.long	0xeec
 2025 0eb6 17       		.uleb128 0x17
 2026 0eb7 00000000 		.long	.LASF148
 2027 0ebb 13       		.byte	0x13
 2028 0ebc 67       		.byte	0x67
 2029 0ebd B0120000 		.long	0x12b0
 2030 0ec1 17       		.uleb128 0x17
 2031 0ec2 00000000 		.long	.LASF143
 2032 0ec6 13       		.byte	0x13
 2033 0ec7 6A       		.byte	0x6a
 2034 0ec8 BE1A0000 		.long	0x1abe
 2035 0ecc 17       		.uleb128 0x17
 2036 0ecd 00000000 		.long	.LASF149
 2037 0ed1 13       		.byte	0x13
 2038 0ed2 6B       		.byte	0x6b
 2039 0ed3 B0120000 		.long	0x12b0
 2040 0ed7 17       		.uleb128 0x17
 2041 0ed8 00000000 		.long	.LASF150
 2042 0edc 13       		.byte	0x13
 2043 0edd 6C       		.byte	0x6c
 2044 0ede B0120000 		.long	0x12b0
 2045 0ee2 2F       		.uleb128 0x2f
 2046 0ee3 00000000 		.long	.LASF145
 2047 0ee7 F2170000 		.long	0x17f2
 2048 0eeb 00       		.byte	0
 2049 0eec 16       		.uleb128 0x16
 2050 0eed 00000000 		.long	.LASF152
 2051 0ef1 01       		.byte	0x1
 2052 0ef2 13       		.byte	0x13
 2053 0ef3 64       		.byte	0x64
 2054 0ef4 2E0F0000 		.long	0xf2e
 2055 0ef8 17       		.uleb128 0x17
 2056 0ef9 00000000 		.long	.LASF148
 2057 0efd 13       		.byte	0x13
 2058 0efe 67       		.byte	0x67
 2059 0eff B0120000 		.long	0x12b0
 2060 0f03 17       		.uleb128 0x17
 2061 0f04 00000000 		.long	.LASF143
 2062 0f08 13       		.byte	0x13
 2063 0f09 6A       		.byte	0x6a
 2064 0f0a BE1A0000 		.long	0x1abe
 2065 0f0e 17       		.uleb128 0x17
 2066 0f0f 00000000 		.long	.LASF149
 2067 0f13 13       		.byte	0x13
 2068 0f14 6B       		.byte	0x6b
 2069 0f15 B0120000 		.long	0x12b0
 2070 0f19 17       		.uleb128 0x17
 2071 0f1a 00000000 		.long	.LASF150
 2072 0f1e 13       		.byte	0x13
 2073 0f1f 6C       		.byte	0x6c
 2074 0f20 B0120000 		.long	0x12b0
 2075 0f24 2F       		.uleb128 0x2f
GAS LISTING /tmp/ccMbRvxK.s 			page 40


 2076 0f25 00000000 		.long	.LASF145
 2077 0f29 241A0000 		.long	0x1a24
 2078 0f2d 00       		.byte	0
 2079 0f2e 16       		.uleb128 0x16
 2080 0f2f 00000000 		.long	.LASF153
 2081 0f33 01       		.byte	0x1
 2082 0f34 13       		.byte	0x13
 2083 0f35 37       		.byte	0x37
 2084 0f36 700F0000 		.long	0xf70
 2085 0f3a 17       		.uleb128 0x17
 2086 0f3b 00000000 		.long	.LASF141
 2087 0f3f 13       		.byte	0x13
 2088 0f40 3A       		.byte	0x3a
 2089 0f41 CF1A0000 		.long	0x1acf
 2090 0f45 17       		.uleb128 0x17
 2091 0f46 00000000 		.long	.LASF142
 2092 0f4a 13       		.byte	0x13
 2093 0f4b 3B       		.byte	0x3b
 2094 0f4c CF1A0000 		.long	0x1acf
 2095 0f50 17       		.uleb128 0x17
 2096 0f51 00000000 		.long	.LASF143
 2097 0f55 13       		.byte	0x13
 2098 0f56 3F       		.byte	0x3f
 2099 0f57 BE1A0000 		.long	0x1abe
 2100 0f5b 17       		.uleb128 0x17
 2101 0f5c 00000000 		.long	.LASF144
 2102 0f60 13       		.byte	0x13
 2103 0f61 40       		.byte	0x40
 2104 0f62 B0120000 		.long	0x12b0
 2105 0f66 2F       		.uleb128 0x2f
 2106 0f67 00000000 		.long	.LASF145
 2107 0f6b 1E120000 		.long	0x121e
 2108 0f6f 00       		.byte	0
 2109 0f70 16       		.uleb128 0x16
 2110 0f71 00000000 		.long	.LASF154
 2111 0f75 01       		.byte	0x1
 2112 0f76 13       		.byte	0x13
 2113 0f77 37       		.byte	0x37
 2114 0f78 B20F0000 		.long	0xfb2
 2115 0f7c 17       		.uleb128 0x17
 2116 0f7d 00000000 		.long	.LASF141
 2117 0f81 13       		.byte	0x13
 2118 0f82 3A       		.byte	0x3a
 2119 0f83 BB120000 		.long	0x12bb
 2120 0f87 17       		.uleb128 0x17
 2121 0f88 00000000 		.long	.LASF142
 2122 0f8c 13       		.byte	0x13
 2123 0f8d 3B       		.byte	0x3b
 2124 0f8e BB120000 		.long	0x12bb
 2125 0f92 17       		.uleb128 0x17
 2126 0f93 00000000 		.long	.LASF143
 2127 0f97 13       		.byte	0x13
 2128 0f98 3F       		.byte	0x3f
 2129 0f99 BE1A0000 		.long	0x1abe
 2130 0f9d 17       		.uleb128 0x17
 2131 0f9e 00000000 		.long	.LASF144
 2132 0fa2 13       		.byte	0x13
GAS LISTING /tmp/ccMbRvxK.s 			page 41


 2133 0fa3 40       		.byte	0x40
 2134 0fa4 B0120000 		.long	0x12b0
 2135 0fa8 2F       		.uleb128 0x2f
 2136 0fa9 00000000 		.long	.LASF145
 2137 0fad 85120000 		.long	0x1285
 2138 0fb1 00       		.byte	0
 2139 0fb2 16       		.uleb128 0x16
 2140 0fb3 00000000 		.long	.LASF155
 2141 0fb7 01       		.byte	0x1
 2142 0fb8 13       		.byte	0x13
 2143 0fb9 37       		.byte	0x37
 2144 0fba F40F0000 		.long	0xff4
 2145 0fbe 17       		.uleb128 0x17
 2146 0fbf 00000000 		.long	.LASF141
 2147 0fc3 13       		.byte	0x13
 2148 0fc4 3A       		.byte	0x3a
 2149 0fc5 1E250000 		.long	0x251e
 2150 0fc9 17       		.uleb128 0x17
 2151 0fca 00000000 		.long	.LASF142
 2152 0fce 13       		.byte	0x13
 2153 0fcf 3B       		.byte	0x3b
 2154 0fd0 1E250000 		.long	0x251e
 2155 0fd4 17       		.uleb128 0x17
 2156 0fd5 00000000 		.long	.LASF143
 2157 0fd9 13       		.byte	0x13
 2158 0fda 3F       		.byte	0x3f
 2159 0fdb BE1A0000 		.long	0x1abe
 2160 0fdf 17       		.uleb128 0x17
 2161 0fe0 00000000 		.long	.LASF144
 2162 0fe4 13       		.byte	0x13
 2163 0fe5 40       		.byte	0x40
 2164 0fe6 B0120000 		.long	0x12b0
 2165 0fea 2F       		.uleb128 0x2f
 2166 0feb 00000000 		.long	.LASF145
 2167 0fef B71A0000 		.long	0x1ab7
 2168 0ff3 00       		.byte	0
 2169 0ff4 34       		.uleb128 0x34
 2170 0ff5 00000000 		.long	.LASF461
 2171 0ff9 01       		.byte	0x1
 2172 0ffa 13       		.byte	0x13
 2173 0ffb 37       		.byte	0x37
 2174 0ffc 17       		.uleb128 0x17
 2175 0ffd 00000000 		.long	.LASF141
 2176 1001 13       		.byte	0x13
 2177 1002 3A       		.byte	0x3a
 2178 1003 23250000 		.long	0x2523
 2179 1007 17       		.uleb128 0x17
 2180 1008 00000000 		.long	.LASF142
 2181 100c 13       		.byte	0x13
 2182 100d 3B       		.byte	0x3b
 2183 100e 23250000 		.long	0x2523
 2184 1012 17       		.uleb128 0x17
 2185 1013 00000000 		.long	.LASF143
 2186 1017 13       		.byte	0x13
 2187 1018 3F       		.byte	0x3f
 2188 1019 BE1A0000 		.long	0x1abe
 2189 101d 17       		.uleb128 0x17
GAS LISTING /tmp/ccMbRvxK.s 			page 42


 2190 101e 00000000 		.long	.LASF144
 2191 1022 13       		.byte	0x13
 2192 1023 40       		.byte	0x40
 2193 1024 B0120000 		.long	0x12b0
 2194 1028 2F       		.uleb128 0x2f
 2195 1029 00000000 		.long	.LASF145
 2196 102d 61180000 		.long	0x1861
 2197 1031 00       		.byte	0
 2198 1032 00       		.byte	0
 2199 1033 14       		.uleb128 0x14
 2200 1034 00000000 		.long	.LASF156
 2201 1038 14       		.byte	0x14
 2202 1039 30       		.byte	0x30
 2203 103a 3E100000 		.long	0x103e
 2204 103e 16       		.uleb128 0x16
 2205 103f 00000000 		.long	.LASF157
 2206 1043 D8       		.byte	0xd8
 2207 1044 15       		.byte	0x15
 2208 1045 F1       		.byte	0xf1
 2209 1046 BB110000 		.long	0x11bb
 2210 104a 09       		.uleb128 0x9
 2211 104b 00000000 		.long	.LASF159
 2212 104f 15       		.byte	0x15
 2213 1050 F2       		.byte	0xf2
 2214 1051 8C120000 		.long	0x128c
 2215 1055 00       		.byte	0
 2216 1056 09       		.uleb128 0x9
 2217 1057 00000000 		.long	.LASF160
 2218 105b 15       		.byte	0x15
 2219 105c F7       		.byte	0xf7
 2220 105d D6150000 		.long	0x15d6
 2221 1061 08       		.byte	0x8
 2222 1062 09       		.uleb128 0x9
 2223 1063 00000000 		.long	.LASF161
 2224 1067 15       		.byte	0x15
 2225 1068 F8       		.byte	0xf8
 2226 1069 D6150000 		.long	0x15d6
 2227 106d 10       		.byte	0x10
 2228 106e 09       		.uleb128 0x9
 2229 106f 00000000 		.long	.LASF162
 2230 1073 15       		.byte	0x15
 2231 1074 F9       		.byte	0xf9
 2232 1075 D6150000 		.long	0x15d6
 2233 1079 18       		.byte	0x18
 2234 107a 09       		.uleb128 0x9
 2235 107b 00000000 		.long	.LASF163
 2236 107f 15       		.byte	0x15
 2237 1080 FA       		.byte	0xfa
 2238 1081 D6150000 		.long	0x15d6
 2239 1085 20       		.byte	0x20
 2240 1086 09       		.uleb128 0x9
 2241 1087 00000000 		.long	.LASF164
 2242 108b 15       		.byte	0x15
 2243 108c FB       		.byte	0xfb
 2244 108d D6150000 		.long	0x15d6
 2245 1091 28       		.byte	0x28
 2246 1092 09       		.uleb128 0x9
GAS LISTING /tmp/ccMbRvxK.s 			page 43


 2247 1093 00000000 		.long	.LASF165
 2248 1097 15       		.byte	0x15
 2249 1098 FC       		.byte	0xfc
 2250 1099 D6150000 		.long	0x15d6
 2251 109d 30       		.byte	0x30
 2252 109e 09       		.uleb128 0x9
 2253 109f 00000000 		.long	.LASF166
 2254 10a3 15       		.byte	0x15
 2255 10a4 FD       		.byte	0xfd
 2256 10a5 D6150000 		.long	0x15d6
 2257 10a9 38       		.byte	0x38
 2258 10aa 09       		.uleb128 0x9
 2259 10ab 00000000 		.long	.LASF167
 2260 10af 15       		.byte	0x15
 2261 10b0 FE       		.byte	0xfe
 2262 10b1 D6150000 		.long	0x15d6
 2263 10b5 40       		.byte	0x40
 2264 10b6 35       		.uleb128 0x35
 2265 10b7 00000000 		.long	.LASF168
 2266 10bb 15       		.byte	0x15
 2267 10bc 0001     		.value	0x100
 2268 10be D6150000 		.long	0x15d6
 2269 10c2 48       		.byte	0x48
 2270 10c3 35       		.uleb128 0x35
 2271 10c4 00000000 		.long	.LASF169
 2272 10c8 15       		.byte	0x15
 2273 10c9 0101     		.value	0x101
 2274 10cb D6150000 		.long	0x15d6
 2275 10cf 50       		.byte	0x50
 2276 10d0 35       		.uleb128 0x35
 2277 10d1 00000000 		.long	.LASF170
 2278 10d5 15       		.byte	0x15
 2279 10d6 0201     		.value	0x102
 2280 10d8 D6150000 		.long	0x15d6
 2281 10dc 58       		.byte	0x58
 2282 10dd 35       		.uleb128 0x35
 2283 10de 00000000 		.long	.LASF171
 2284 10e2 15       		.byte	0x15
 2285 10e3 0401     		.value	0x104
 2286 10e5 D9210000 		.long	0x21d9
 2287 10e9 60       		.byte	0x60
 2288 10ea 35       		.uleb128 0x35
 2289 10eb 00000000 		.long	.LASF172
 2290 10ef 15       		.byte	0x15
 2291 10f0 0601     		.value	0x106
 2292 10f2 DF210000 		.long	0x21df
 2293 10f6 68       		.byte	0x68
 2294 10f7 35       		.uleb128 0x35
 2295 10f8 00000000 		.long	.LASF173
 2296 10fc 15       		.byte	0x15
 2297 10fd 0801     		.value	0x108
 2298 10ff 8C120000 		.long	0x128c
 2299 1103 70       		.byte	0x70
 2300 1104 35       		.uleb128 0x35
 2301 1105 00000000 		.long	.LASF174
 2302 1109 15       		.byte	0x15
 2303 110a 0C01     		.value	0x10c
GAS LISTING /tmp/ccMbRvxK.s 			page 44


 2304 110c 8C120000 		.long	0x128c
 2305 1110 74       		.byte	0x74
 2306 1111 35       		.uleb128 0x35
 2307 1112 00000000 		.long	.LASF175
 2308 1116 15       		.byte	0x15
 2309 1117 0E01     		.value	0x10e
 2310 1119 AA1D0000 		.long	0x1daa
 2311 111d 78       		.byte	0x78
 2312 111e 35       		.uleb128 0x35
 2313 111f 00000000 		.long	.LASF176
 2314 1123 15       		.byte	0x15
 2315 1124 1201     		.value	0x112
 2316 1126 A9120000 		.long	0x12a9
 2317 112a 80       		.byte	0x80
 2318 112b 35       		.uleb128 0x35
 2319 112c 00000000 		.long	.LASF177
 2320 1130 15       		.byte	0x15
 2321 1131 1301     		.value	0x113
 2322 1133 B01A0000 		.long	0x1ab0
 2323 1137 82       		.byte	0x82
 2324 1138 35       		.uleb128 0x35
 2325 1139 00000000 		.long	.LASF178
 2326 113d 15       		.byte	0x15
 2327 113e 1401     		.value	0x114
 2328 1140 E5210000 		.long	0x21e5
 2329 1144 83       		.byte	0x83
 2330 1145 35       		.uleb128 0x35
 2331 1146 00000000 		.long	.LASF179
 2332 114a 15       		.byte	0x15
 2333 114b 1801     		.value	0x118
 2334 114d F5210000 		.long	0x21f5
 2335 1151 88       		.byte	0x88
 2336 1152 35       		.uleb128 0x35
 2337 1153 00000000 		.long	.LASF180
 2338 1157 15       		.byte	0x15
 2339 1158 2101     		.value	0x121
 2340 115a B51D0000 		.long	0x1db5
 2341 115e 90       		.byte	0x90
 2342 115f 35       		.uleb128 0x35
 2343 1160 00000000 		.long	.LASF181
 2344 1164 15       		.byte	0x15
 2345 1165 2901     		.value	0x129
 2346 1167 11120000 		.long	0x1211
 2347 116b 98       		.byte	0x98
 2348 116c 35       		.uleb128 0x35
 2349 116d 00000000 		.long	.LASF182
 2350 1171 15       		.byte	0x15
 2351 1172 2A01     		.value	0x12a
 2352 1174 11120000 		.long	0x1211
 2353 1178 A0       		.byte	0xa0
 2354 1179 35       		.uleb128 0x35
 2355 117a 00000000 		.long	.LASF183
 2356 117e 15       		.byte	0x15
 2357 117f 2B01     		.value	0x12b
 2358 1181 11120000 		.long	0x1211
 2359 1185 A8       		.byte	0xa8
 2360 1186 35       		.uleb128 0x35
GAS LISTING /tmp/ccMbRvxK.s 			page 45


 2361 1187 00000000 		.long	.LASF184
 2362 118b 15       		.byte	0x15
 2363 118c 2C01     		.value	0x12c
 2364 118e 11120000 		.long	0x1211
 2365 1192 B0       		.byte	0xb0
 2366 1193 35       		.uleb128 0x35
 2367 1194 00000000 		.long	.LASF185
 2368 1198 15       		.byte	0x15
 2369 1199 2E01     		.value	0x12e
 2370 119b 13120000 		.long	0x1213
 2371 119f B8       		.byte	0xb8
 2372 11a0 35       		.uleb128 0x35
 2373 11a1 00000000 		.long	.LASF186
 2374 11a5 15       		.byte	0x15
 2375 11a6 2F01     		.value	0x12f
 2376 11a8 8C120000 		.long	0x128c
 2377 11ac C0       		.byte	0xc0
 2378 11ad 35       		.uleb128 0x35
 2379 11ae 00000000 		.long	.LASF187
 2380 11b2 15       		.byte	0x15
 2381 11b3 3101     		.value	0x131
 2382 11b5 FB210000 		.long	0x21fb
 2383 11b9 C4       		.byte	0xc4
 2384 11ba 00       		.byte	0
 2385 11bb 14       		.uleb128 0x14
 2386 11bc 00000000 		.long	.LASF188
 2387 11c0 14       		.byte	0x14
 2388 11c1 40       		.byte	0x40
 2389 11c2 3E100000 		.long	0x103e
 2390 11c6 36       		.uleb128 0x36
 2391 11c7 08       		.byte	0x8
 2392 11c8 07       		.byte	0x7
 2393 11c9 00000000 		.long	.LASF194
 2394 11cd 16       		.uleb128 0x16
 2395 11ce 00000000 		.long	.LASF189
 2396 11d2 18       		.byte	0x18
 2397 11d3 16       		.byte	0x16
 2398 11d4 00       		.byte	0
 2399 11d5 0A120000 		.long	0x120a
 2400 11d9 09       		.uleb128 0x9
 2401 11da 00000000 		.long	.LASF190
 2402 11de 16       		.byte	0x16
 2403 11df 00       		.byte	0
 2404 11e0 0A120000 		.long	0x120a
 2405 11e4 00       		.byte	0
 2406 11e5 09       		.uleb128 0x9
 2407 11e6 00000000 		.long	.LASF191
 2408 11ea 16       		.byte	0x16
 2409 11eb 00       		.byte	0
 2410 11ec 0A120000 		.long	0x120a
 2411 11f0 04       		.byte	0x4
 2412 11f1 09       		.uleb128 0x9
 2413 11f2 00000000 		.long	.LASF192
 2414 11f6 16       		.byte	0x16
 2415 11f7 00       		.byte	0
 2416 11f8 11120000 		.long	0x1211
 2417 11fc 08       		.byte	0x8
GAS LISTING /tmp/ccMbRvxK.s 			page 46


 2418 11fd 09       		.uleb128 0x9
 2419 11fe 00000000 		.long	.LASF193
 2420 1202 16       		.byte	0x16
 2421 1203 00       		.byte	0
 2422 1204 11120000 		.long	0x1211
 2423 1208 10       		.byte	0x10
 2424 1209 00       		.byte	0
 2425 120a 36       		.uleb128 0x36
 2426 120b 04       		.byte	0x4
 2427 120c 07       		.byte	0x7
 2428 120d 00000000 		.long	.LASF195
 2429 1211 37       		.uleb128 0x37
 2430 1212 08       		.byte	0x8
 2431 1213 14       		.uleb128 0x14
 2432 1214 00000000 		.long	.LASF57
 2433 1218 17       		.byte	0x17
 2434 1219 D8       		.byte	0xd8
 2435 121a 1E120000 		.long	0x121e
 2436 121e 36       		.uleb128 0x36
 2437 121f 08       		.byte	0x8
 2438 1220 07       		.byte	0x7
 2439 1221 00000000 		.long	.LASF196
 2440 1225 38       		.uleb128 0x38
 2441 1226 00000000 		.long	.LASF197
 2442 122a 17       		.byte	0x17
 2443 122b 6501     		.value	0x165
 2444 122d 0A120000 		.long	0x120a
 2445 1231 39       		.uleb128 0x39
 2446 1232 08       		.byte	0x8
 2447 1233 18       		.byte	0x18
 2448 1234 53       		.byte	0x53
 2449 1235 00000000 		.long	.LASF349
 2450 1239 75120000 		.long	0x1275
 2451 123d 3A       		.uleb128 0x3a
 2452 123e 04       		.byte	0x4
 2453 123f 18       		.byte	0x18
 2454 1240 56       		.byte	0x56
 2455 1241 5C120000 		.long	0x125c
 2456 1245 3B       		.uleb128 0x3b
 2457 1246 00000000 		.long	.LASF198
 2458 124a 18       		.byte	0x18
 2459 124b 58       		.byte	0x58
 2460 124c 0A120000 		.long	0x120a
 2461 1250 3B       		.uleb128 0x3b
 2462 1251 00000000 		.long	.LASF199
 2463 1255 18       		.byte	0x18
 2464 1256 5C       		.byte	0x5c
 2465 1257 75120000 		.long	0x1275
 2466 125b 00       		.byte	0
 2467 125c 09       		.uleb128 0x9
 2468 125d 00000000 		.long	.LASF200
 2469 1261 18       		.byte	0x18
 2470 1262 54       		.byte	0x54
 2471 1263 8C120000 		.long	0x128c
 2472 1267 00       		.byte	0
 2473 1268 09       		.uleb128 0x9
 2474 1269 00000000 		.long	.LASF201
GAS LISTING /tmp/ccMbRvxK.s 			page 47


 2475 126d 18       		.byte	0x18
 2476 126e 5D       		.byte	0x5d
 2477 126f 3D120000 		.long	0x123d
 2478 1273 04       		.byte	0x4
 2479 1274 00       		.byte	0
 2480 1275 3C       		.uleb128 0x3c
 2481 1276 85120000 		.long	0x1285
 2482 127a 85120000 		.long	0x1285
 2483 127e 3D       		.uleb128 0x3d
 2484 127f C6110000 		.long	0x11c6
 2485 1283 03       		.byte	0x3
 2486 1284 00       		.byte	0
 2487 1285 36       		.uleb128 0x36
 2488 1286 01       		.byte	0x1
 2489 1287 06       		.byte	0x6
 2490 1288 00000000 		.long	.LASF202
 2491 128c 3E       		.uleb128 0x3e
 2492 128d 04       		.byte	0x4
 2493 128e 05       		.byte	0x5
 2494 128f 696E7400 		.string	"int"
 2495 1293 14       		.uleb128 0x14
 2496 1294 00000000 		.long	.LASF203
 2497 1298 18       		.byte	0x18
 2498 1299 5E       		.byte	0x5e
 2499 129a 31120000 		.long	0x1231
 2500 129e 14       		.uleb128 0x14
 2501 129f 00000000 		.long	.LASF204
 2502 12a3 18       		.byte	0x18
 2503 12a4 6A       		.byte	0x6a
 2504 12a5 93120000 		.long	0x1293
 2505 12a9 36       		.uleb128 0x36
 2506 12aa 02       		.byte	0x2
 2507 12ab 07       		.byte	0x7
 2508 12ac 00000000 		.long	.LASF205
 2509 12b0 13       		.uleb128 0x13
 2510 12b1 8C120000 		.long	0x128c
 2511 12b5 3F       		.uleb128 0x3f
 2512 12b6 08       		.byte	0x8
 2513 12b7 BB120000 		.long	0x12bb
 2514 12bb 13       		.uleb128 0x13
 2515 12bc 85120000 		.long	0x1285
 2516 12c0 40       		.uleb128 0x40
 2517 12c1 00000000 		.long	.LASF206
 2518 12c5 18       		.byte	0x18
 2519 12c6 6401     		.value	0x164
 2520 12c8 25120000 		.long	0x1225
 2521 12cc D6120000 		.long	0x12d6
 2522 12d0 0C       		.uleb128 0xc
 2523 12d1 8C120000 		.long	0x128c
 2524 12d5 00       		.byte	0
 2525 12d6 40       		.uleb128 0x40
 2526 12d7 00000000 		.long	.LASF207
 2527 12db 18       		.byte	0x18
 2528 12dc EC02     		.value	0x2ec
 2529 12de 25120000 		.long	0x1225
 2530 12e2 EC120000 		.long	0x12ec
 2531 12e6 0C       		.uleb128 0xc
GAS LISTING /tmp/ccMbRvxK.s 			page 48


 2532 12e7 EC120000 		.long	0x12ec
 2533 12eb 00       		.byte	0
 2534 12ec 3F       		.uleb128 0x3f
 2535 12ed 08       		.byte	0x8
 2536 12ee BB110000 		.long	0x11bb
 2537 12f2 40       		.uleb128 0x40
 2538 12f3 00000000 		.long	.LASF208
 2539 12f7 18       		.byte	0x18
 2540 12f8 0903     		.value	0x309
 2541 12fa 12130000 		.long	0x1312
 2542 12fe 12130000 		.long	0x1312
 2543 1302 0C       		.uleb128 0xc
 2544 1303 12130000 		.long	0x1312
 2545 1307 0C       		.uleb128 0xc
 2546 1308 8C120000 		.long	0x128c
 2547 130c 0C       		.uleb128 0xc
 2548 130d EC120000 		.long	0x12ec
 2549 1311 00       		.byte	0
 2550 1312 3F       		.uleb128 0x3f
 2551 1313 08       		.byte	0x8
 2552 1314 18130000 		.long	0x1318
 2553 1318 36       		.uleb128 0x36
 2554 1319 04       		.byte	0x4
 2555 131a 05       		.byte	0x5
 2556 131b 00000000 		.long	.LASF209
 2557 131f 40       		.uleb128 0x40
 2558 1320 00000000 		.long	.LASF210
 2559 1324 18       		.byte	0x18
 2560 1325 FA02     		.value	0x2fa
 2561 1327 25120000 		.long	0x1225
 2562 132b 3A130000 		.long	0x133a
 2563 132f 0C       		.uleb128 0xc
 2564 1330 18130000 		.long	0x1318
 2565 1334 0C       		.uleb128 0xc
 2566 1335 EC120000 		.long	0x12ec
 2567 1339 00       		.byte	0
 2568 133a 40       		.uleb128 0x40
 2569 133b 00000000 		.long	.LASF211
 2570 133f 18       		.byte	0x18
 2571 1340 1003     		.value	0x310
 2572 1342 8C120000 		.long	0x128c
 2573 1346 55130000 		.long	0x1355
 2574 134a 0C       		.uleb128 0xc
 2575 134b 55130000 		.long	0x1355
 2576 134f 0C       		.uleb128 0xc
 2577 1350 EC120000 		.long	0x12ec
 2578 1354 00       		.byte	0
 2579 1355 3F       		.uleb128 0x3f
 2580 1356 08       		.byte	0x8
 2581 1357 5B130000 		.long	0x135b
 2582 135b 13       		.uleb128 0x13
 2583 135c 18130000 		.long	0x1318
 2584 1360 40       		.uleb128 0x40
 2585 1361 00000000 		.long	.LASF212
 2586 1365 18       		.byte	0x18
 2587 1366 4E02     		.value	0x24e
 2588 1368 8C120000 		.long	0x128c
GAS LISTING /tmp/ccMbRvxK.s 			page 49


 2589 136c 7B130000 		.long	0x137b
 2590 1370 0C       		.uleb128 0xc
 2591 1371 EC120000 		.long	0x12ec
 2592 1375 0C       		.uleb128 0xc
 2593 1376 8C120000 		.long	0x128c
 2594 137a 00       		.byte	0
 2595 137b 40       		.uleb128 0x40
 2596 137c 00000000 		.long	.LASF213
 2597 1380 18       		.byte	0x18
 2598 1381 5502     		.value	0x255
 2599 1383 8C120000 		.long	0x128c
 2600 1387 97130000 		.long	0x1397
 2601 138b 0C       		.uleb128 0xc
 2602 138c EC120000 		.long	0x12ec
 2603 1390 0C       		.uleb128 0xc
 2604 1391 55130000 		.long	0x1355
 2605 1395 41       		.uleb128 0x41
 2606 1396 00       		.byte	0
 2607 1397 40       		.uleb128 0x40
 2608 1398 00000000 		.long	.LASF214
 2609 139c 18       		.byte	0x18
 2610 139d 7E02     		.value	0x27e
 2611 139f 8C120000 		.long	0x128c
 2612 13a3 B3130000 		.long	0x13b3
 2613 13a7 0C       		.uleb128 0xc
 2614 13a8 EC120000 		.long	0x12ec
 2615 13ac 0C       		.uleb128 0xc
 2616 13ad 55130000 		.long	0x1355
 2617 13b1 41       		.uleb128 0x41
 2618 13b2 00       		.byte	0
 2619 13b3 40       		.uleb128 0x40
 2620 13b4 00000000 		.long	.LASF215
 2621 13b8 18       		.byte	0x18
 2622 13b9 ED02     		.value	0x2ed
 2623 13bb 25120000 		.long	0x1225
 2624 13bf C9130000 		.long	0x13c9
 2625 13c3 0C       		.uleb128 0xc
 2626 13c4 EC120000 		.long	0x12ec
 2627 13c8 00       		.byte	0
 2628 13c9 42       		.uleb128 0x42
 2629 13ca 00000000 		.long	.LASF343
 2630 13ce 18       		.byte	0x18
 2631 13cf F302     		.value	0x2f3
 2632 13d1 25120000 		.long	0x1225
 2633 13d5 40       		.uleb128 0x40
 2634 13d6 00000000 		.long	.LASF216
 2635 13da 18       		.byte	0x18
 2636 13db 7B01     		.value	0x17b
 2637 13dd 13120000 		.long	0x1213
 2638 13e1 F5130000 		.long	0x13f5
 2639 13e5 0C       		.uleb128 0xc
 2640 13e6 B5120000 		.long	0x12b5
 2641 13ea 0C       		.uleb128 0xc
 2642 13eb 13120000 		.long	0x1213
 2643 13ef 0C       		.uleb128 0xc
 2644 13f0 F5130000 		.long	0x13f5
 2645 13f4 00       		.byte	0
GAS LISTING /tmp/ccMbRvxK.s 			page 50


 2646 13f5 3F       		.uleb128 0x3f
 2647 13f6 08       		.byte	0x8
 2648 13f7 9E120000 		.long	0x129e
 2649 13fb 40       		.uleb128 0x40
 2650 13fc 00000000 		.long	.LASF217
 2651 1400 18       		.byte	0x18
 2652 1401 7001     		.value	0x170
 2653 1403 13120000 		.long	0x1213
 2654 1407 20140000 		.long	0x1420
 2655 140b 0C       		.uleb128 0xc
 2656 140c 12130000 		.long	0x1312
 2657 1410 0C       		.uleb128 0xc
 2658 1411 B5120000 		.long	0x12b5
 2659 1415 0C       		.uleb128 0xc
 2660 1416 13120000 		.long	0x1213
 2661 141a 0C       		.uleb128 0xc
 2662 141b F5130000 		.long	0x13f5
 2663 141f 00       		.byte	0
 2664 1420 40       		.uleb128 0x40
 2665 1421 00000000 		.long	.LASF218
 2666 1425 18       		.byte	0x18
 2667 1426 6C01     		.value	0x16c
 2668 1428 8C120000 		.long	0x128c
 2669 142c 36140000 		.long	0x1436
 2670 1430 0C       		.uleb128 0xc
 2671 1431 36140000 		.long	0x1436
 2672 1435 00       		.byte	0
 2673 1436 3F       		.uleb128 0x3f
 2674 1437 08       		.byte	0x8
 2675 1438 3C140000 		.long	0x143c
 2676 143c 13       		.uleb128 0x13
 2677 143d 9E120000 		.long	0x129e
 2678 1441 40       		.uleb128 0x40
 2679 1442 00000000 		.long	.LASF219
 2680 1446 18       		.byte	0x18
 2681 1447 9B01     		.value	0x19b
 2682 1449 13120000 		.long	0x1213
 2683 144d 66140000 		.long	0x1466
 2684 1451 0C       		.uleb128 0xc
 2685 1452 12130000 		.long	0x1312
 2686 1456 0C       		.uleb128 0xc
 2687 1457 66140000 		.long	0x1466
 2688 145b 0C       		.uleb128 0xc
 2689 145c 13120000 		.long	0x1213
 2690 1460 0C       		.uleb128 0xc
 2691 1461 F5130000 		.long	0x13f5
 2692 1465 00       		.byte	0
 2693 1466 3F       		.uleb128 0x3f
 2694 1467 08       		.byte	0x8
 2695 1468 B5120000 		.long	0x12b5
 2696 146c 40       		.uleb128 0x40
 2697 146d 00000000 		.long	.LASF220
 2698 1471 18       		.byte	0x18
 2699 1472 FB02     		.value	0x2fb
 2700 1474 25120000 		.long	0x1225
 2701 1478 87140000 		.long	0x1487
 2702 147c 0C       		.uleb128 0xc
GAS LISTING /tmp/ccMbRvxK.s 			page 51


 2703 147d 18130000 		.long	0x1318
 2704 1481 0C       		.uleb128 0xc
 2705 1482 EC120000 		.long	0x12ec
 2706 1486 00       		.byte	0
 2707 1487 40       		.uleb128 0x40
 2708 1488 00000000 		.long	.LASF221
 2709 148c 18       		.byte	0x18
 2710 148d 0103     		.value	0x301
 2711 148f 25120000 		.long	0x1225
 2712 1493 9D140000 		.long	0x149d
 2713 1497 0C       		.uleb128 0xc
 2714 1498 18130000 		.long	0x1318
 2715 149c 00       		.byte	0
 2716 149d 40       		.uleb128 0x40
 2717 149e 00000000 		.long	.LASF222
 2718 14a2 18       		.byte	0x18
 2719 14a3 5F02     		.value	0x25f
 2720 14a5 8C120000 		.long	0x128c
 2721 14a9 BE140000 		.long	0x14be
 2722 14ad 0C       		.uleb128 0xc
 2723 14ae 12130000 		.long	0x1312
 2724 14b2 0C       		.uleb128 0xc
 2725 14b3 13120000 		.long	0x1213
 2726 14b7 0C       		.uleb128 0xc
 2727 14b8 55130000 		.long	0x1355
 2728 14bc 41       		.uleb128 0x41
 2729 14bd 00       		.byte	0
 2730 14be 40       		.uleb128 0x40
 2731 14bf 00000000 		.long	.LASF223
 2732 14c3 18       		.byte	0x18
 2733 14c4 8802     		.value	0x288
 2734 14c6 8C120000 		.long	0x128c
 2735 14ca DA140000 		.long	0x14da
 2736 14ce 0C       		.uleb128 0xc
 2737 14cf 55130000 		.long	0x1355
 2738 14d3 0C       		.uleb128 0xc
 2739 14d4 55130000 		.long	0x1355
 2740 14d8 41       		.uleb128 0x41
 2741 14d9 00       		.byte	0
 2742 14da 40       		.uleb128 0x40
 2743 14db 00000000 		.long	.LASF224
 2744 14df 18       		.byte	0x18
 2745 14e0 1803     		.value	0x318
 2746 14e2 25120000 		.long	0x1225
 2747 14e6 F5140000 		.long	0x14f5
 2748 14ea 0C       		.uleb128 0xc
 2749 14eb 25120000 		.long	0x1225
 2750 14ef 0C       		.uleb128 0xc
 2751 14f0 EC120000 		.long	0x12ec
 2752 14f4 00       		.byte	0
 2753 14f5 40       		.uleb128 0x40
 2754 14f6 00000000 		.long	.LASF225
 2755 14fa 18       		.byte	0x18
 2756 14fb 6702     		.value	0x267
 2757 14fd 8C120000 		.long	0x128c
 2758 1501 15150000 		.long	0x1515
 2759 1505 0C       		.uleb128 0xc
GAS LISTING /tmp/ccMbRvxK.s 			page 52


 2760 1506 EC120000 		.long	0x12ec
 2761 150a 0C       		.uleb128 0xc
 2762 150b 55130000 		.long	0x1355
 2763 150f 0C       		.uleb128 0xc
 2764 1510 15150000 		.long	0x1515
 2765 1514 00       		.byte	0
 2766 1515 3F       		.uleb128 0x3f
 2767 1516 08       		.byte	0x8
 2768 1517 CD110000 		.long	0x11cd
 2769 151b 40       		.uleb128 0x40
 2770 151c 00000000 		.long	.LASF226
 2771 1520 18       		.byte	0x18
 2772 1521 B402     		.value	0x2b4
 2773 1523 8C120000 		.long	0x128c
 2774 1527 3B150000 		.long	0x153b
 2775 152b 0C       		.uleb128 0xc
 2776 152c EC120000 		.long	0x12ec
 2777 1530 0C       		.uleb128 0xc
 2778 1531 55130000 		.long	0x1355
 2779 1535 0C       		.uleb128 0xc
 2780 1536 15150000 		.long	0x1515
 2781 153a 00       		.byte	0
 2782 153b 40       		.uleb128 0x40
 2783 153c 00000000 		.long	.LASF227
 2784 1540 18       		.byte	0x18
 2785 1541 7402     		.value	0x274
 2786 1543 8C120000 		.long	0x128c
 2787 1547 60150000 		.long	0x1560
 2788 154b 0C       		.uleb128 0xc
 2789 154c 12130000 		.long	0x1312
 2790 1550 0C       		.uleb128 0xc
 2791 1551 13120000 		.long	0x1213
 2792 1555 0C       		.uleb128 0xc
 2793 1556 55130000 		.long	0x1355
 2794 155a 0C       		.uleb128 0xc
 2795 155b 15150000 		.long	0x1515
 2796 155f 00       		.byte	0
 2797 1560 40       		.uleb128 0x40
 2798 1561 00000000 		.long	.LASF228
 2799 1565 18       		.byte	0x18
 2800 1566 C002     		.value	0x2c0
 2801 1568 8C120000 		.long	0x128c
 2802 156c 80150000 		.long	0x1580
 2803 1570 0C       		.uleb128 0xc
 2804 1571 55130000 		.long	0x1355
 2805 1575 0C       		.uleb128 0xc
 2806 1576 55130000 		.long	0x1355
 2807 157a 0C       		.uleb128 0xc
 2808 157b 15150000 		.long	0x1515
 2809 157f 00       		.byte	0
 2810 1580 40       		.uleb128 0x40
 2811 1581 00000000 		.long	.LASF229
 2812 1585 18       		.byte	0x18
 2813 1586 6F02     		.value	0x26f
 2814 1588 8C120000 		.long	0x128c
 2815 158c 9B150000 		.long	0x159b
 2816 1590 0C       		.uleb128 0xc
GAS LISTING /tmp/ccMbRvxK.s 			page 53


 2817 1591 55130000 		.long	0x1355
 2818 1595 0C       		.uleb128 0xc
 2819 1596 15150000 		.long	0x1515
 2820 159a 00       		.byte	0
 2821 159b 40       		.uleb128 0x40
 2822 159c 00000000 		.long	.LASF230
 2823 15a0 18       		.byte	0x18
 2824 15a1 BC02     		.value	0x2bc
 2825 15a3 8C120000 		.long	0x128c
 2826 15a7 B6150000 		.long	0x15b6
 2827 15ab 0C       		.uleb128 0xc
 2828 15ac 55130000 		.long	0x1355
 2829 15b0 0C       		.uleb128 0xc
 2830 15b1 15150000 		.long	0x1515
 2831 15b5 00       		.byte	0
 2832 15b6 40       		.uleb128 0x40
 2833 15b7 00000000 		.long	.LASF231
 2834 15bb 18       		.byte	0x18
 2835 15bc 7501     		.value	0x175
 2836 15be 13120000 		.long	0x1213
 2837 15c2 D6150000 		.long	0x15d6
 2838 15c6 0C       		.uleb128 0xc
 2839 15c7 D6150000 		.long	0x15d6
 2840 15cb 0C       		.uleb128 0xc
 2841 15cc 18130000 		.long	0x1318
 2842 15d0 0C       		.uleb128 0xc
 2843 15d1 F5130000 		.long	0x13f5
 2844 15d5 00       		.byte	0
 2845 15d6 3F       		.uleb128 0x3f
 2846 15d7 08       		.byte	0x8
 2847 15d8 85120000 		.long	0x1285
 2848 15dc 43       		.uleb128 0x43
 2849 15dd 00000000 		.long	.LASF232
 2850 15e1 18       		.byte	0x18
 2851 15e2 9D       		.byte	0x9d
 2852 15e3 12130000 		.long	0x1312
 2853 15e7 F6150000 		.long	0x15f6
 2854 15eb 0C       		.uleb128 0xc
 2855 15ec 12130000 		.long	0x1312
 2856 15f0 0C       		.uleb128 0xc
 2857 15f1 55130000 		.long	0x1355
 2858 15f5 00       		.byte	0
 2859 15f6 43       		.uleb128 0x43
 2860 15f7 00000000 		.long	.LASF233
 2861 15fb 18       		.byte	0x18
 2862 15fc A6       		.byte	0xa6
 2863 15fd 8C120000 		.long	0x128c
 2864 1601 10160000 		.long	0x1610
 2865 1605 0C       		.uleb128 0xc
 2866 1606 55130000 		.long	0x1355
 2867 160a 0C       		.uleb128 0xc
 2868 160b 55130000 		.long	0x1355
 2869 160f 00       		.byte	0
 2870 1610 43       		.uleb128 0x43
 2871 1611 00000000 		.long	.LASF234
 2872 1615 18       		.byte	0x18
 2873 1616 C3       		.byte	0xc3
GAS LISTING /tmp/ccMbRvxK.s 			page 54


 2874 1617 8C120000 		.long	0x128c
 2875 161b 2A160000 		.long	0x162a
 2876 161f 0C       		.uleb128 0xc
 2877 1620 55130000 		.long	0x1355
 2878 1624 0C       		.uleb128 0xc
 2879 1625 55130000 		.long	0x1355
 2880 1629 00       		.byte	0
 2881 162a 43       		.uleb128 0x43
 2882 162b 00000000 		.long	.LASF235
 2883 162f 18       		.byte	0x18
 2884 1630 93       		.byte	0x93
 2885 1631 12130000 		.long	0x1312
 2886 1635 44160000 		.long	0x1644
 2887 1639 0C       		.uleb128 0xc
 2888 163a 12130000 		.long	0x1312
 2889 163e 0C       		.uleb128 0xc
 2890 163f 55130000 		.long	0x1355
 2891 1643 00       		.byte	0
 2892 1644 43       		.uleb128 0x43
 2893 1645 00000000 		.long	.LASF236
 2894 1649 18       		.byte	0x18
 2895 164a FF       		.byte	0xff
 2896 164b 13120000 		.long	0x1213
 2897 164f 5E160000 		.long	0x165e
 2898 1653 0C       		.uleb128 0xc
 2899 1654 55130000 		.long	0x1355
 2900 1658 0C       		.uleb128 0xc
 2901 1659 55130000 		.long	0x1355
 2902 165d 00       		.byte	0
 2903 165e 40       		.uleb128 0x40
 2904 165f 00000000 		.long	.LASF237
 2905 1663 18       		.byte	0x18
 2906 1664 5A03     		.value	0x35a
 2907 1666 13120000 		.long	0x1213
 2908 166a 83160000 		.long	0x1683
 2909 166e 0C       		.uleb128 0xc
 2910 166f 12130000 		.long	0x1312
 2911 1673 0C       		.uleb128 0xc
 2912 1674 13120000 		.long	0x1213
 2913 1678 0C       		.uleb128 0xc
 2914 1679 55130000 		.long	0x1355
 2915 167d 0C       		.uleb128 0xc
 2916 167e 83160000 		.long	0x1683
 2917 1682 00       		.byte	0
 2918 1683 3F       		.uleb128 0x3f
 2919 1684 08       		.byte	0x8
 2920 1685 19170000 		.long	0x1719
 2921 1689 44       		.uleb128 0x44
 2922 168a 746D00   		.string	"tm"
 2923 168d 38       		.byte	0x38
 2924 168e 19       		.byte	0x19
 2925 168f 85       		.byte	0x85
 2926 1690 19170000 		.long	0x1719
 2927 1694 09       		.uleb128 0x9
 2928 1695 00000000 		.long	.LASF238
 2929 1699 19       		.byte	0x19
 2930 169a 87       		.byte	0x87
GAS LISTING /tmp/ccMbRvxK.s 			page 55


 2931 169b 8C120000 		.long	0x128c
 2932 169f 00       		.byte	0
 2933 16a0 09       		.uleb128 0x9
 2934 16a1 00000000 		.long	.LASF239
 2935 16a5 19       		.byte	0x19
 2936 16a6 88       		.byte	0x88
 2937 16a7 8C120000 		.long	0x128c
 2938 16ab 04       		.byte	0x4
 2939 16ac 09       		.uleb128 0x9
 2940 16ad 00000000 		.long	.LASF240
 2941 16b1 19       		.byte	0x19
 2942 16b2 89       		.byte	0x89
 2943 16b3 8C120000 		.long	0x128c
 2944 16b7 08       		.byte	0x8
 2945 16b8 09       		.uleb128 0x9
 2946 16b9 00000000 		.long	.LASF241
 2947 16bd 19       		.byte	0x19
 2948 16be 8A       		.byte	0x8a
 2949 16bf 8C120000 		.long	0x128c
 2950 16c3 0C       		.byte	0xc
 2951 16c4 09       		.uleb128 0x9
 2952 16c5 00000000 		.long	.LASF242
 2953 16c9 19       		.byte	0x19
 2954 16ca 8B       		.byte	0x8b
 2955 16cb 8C120000 		.long	0x128c
 2956 16cf 10       		.byte	0x10
 2957 16d0 09       		.uleb128 0x9
 2958 16d1 00000000 		.long	.LASF243
 2959 16d5 19       		.byte	0x19
 2960 16d6 8C       		.byte	0x8c
 2961 16d7 8C120000 		.long	0x128c
 2962 16db 14       		.byte	0x14
 2963 16dc 09       		.uleb128 0x9
 2964 16dd 00000000 		.long	.LASF244
 2965 16e1 19       		.byte	0x19
 2966 16e2 8D       		.byte	0x8d
 2967 16e3 8C120000 		.long	0x128c
 2968 16e7 18       		.byte	0x18
 2969 16e8 09       		.uleb128 0x9
 2970 16e9 00000000 		.long	.LASF245
 2971 16ed 19       		.byte	0x19
 2972 16ee 8E       		.byte	0x8e
 2973 16ef 8C120000 		.long	0x128c
 2974 16f3 1C       		.byte	0x1c
 2975 16f4 09       		.uleb128 0x9
 2976 16f5 00000000 		.long	.LASF246
 2977 16f9 19       		.byte	0x19
 2978 16fa 8F       		.byte	0x8f
 2979 16fb 8C120000 		.long	0x128c
 2980 16ff 20       		.byte	0x20
 2981 1700 09       		.uleb128 0x9
 2982 1701 00000000 		.long	.LASF247
 2983 1705 19       		.byte	0x19
 2984 1706 92       		.byte	0x92
 2985 1707 61180000 		.long	0x1861
 2986 170b 28       		.byte	0x28
 2987 170c 09       		.uleb128 0x9
GAS LISTING /tmp/ccMbRvxK.s 			page 56


 2988 170d 00000000 		.long	.LASF248
 2989 1711 19       		.byte	0x19
 2990 1712 93       		.byte	0x93
 2991 1713 B5120000 		.long	0x12b5
 2992 1717 30       		.byte	0x30
 2993 1718 00       		.byte	0
 2994 1719 13       		.uleb128 0x13
 2995 171a 89160000 		.long	0x1689
 2996 171e 40       		.uleb128 0x40
 2997 171f 00000000 		.long	.LASF249
 2998 1723 18       		.byte	0x18
 2999 1724 2201     		.value	0x122
 3000 1726 13120000 		.long	0x1213
 3001 172a 34170000 		.long	0x1734
 3002 172e 0C       		.uleb128 0xc
 3003 172f 55130000 		.long	0x1355
 3004 1733 00       		.byte	0
 3005 1734 43       		.uleb128 0x43
 3006 1735 00000000 		.long	.LASF250
 3007 1739 18       		.byte	0x18
 3008 173a A1       		.byte	0xa1
 3009 173b 12130000 		.long	0x1312
 3010 173f 53170000 		.long	0x1753
 3011 1743 0C       		.uleb128 0xc
 3012 1744 12130000 		.long	0x1312
 3013 1748 0C       		.uleb128 0xc
 3014 1749 55130000 		.long	0x1355
 3015 174d 0C       		.uleb128 0xc
 3016 174e 13120000 		.long	0x1213
 3017 1752 00       		.byte	0
 3018 1753 43       		.uleb128 0x43
 3019 1754 00000000 		.long	.LASF251
 3020 1758 18       		.byte	0x18
 3021 1759 A9       		.byte	0xa9
 3022 175a 8C120000 		.long	0x128c
 3023 175e 72170000 		.long	0x1772
 3024 1762 0C       		.uleb128 0xc
 3025 1763 55130000 		.long	0x1355
 3026 1767 0C       		.uleb128 0xc
 3027 1768 55130000 		.long	0x1355
 3028 176c 0C       		.uleb128 0xc
 3029 176d 13120000 		.long	0x1213
 3030 1771 00       		.byte	0
 3031 1772 43       		.uleb128 0x43
 3032 1773 00000000 		.long	.LASF252
 3033 1777 18       		.byte	0x18
 3034 1778 98       		.byte	0x98
 3035 1779 12130000 		.long	0x1312
 3036 177d 91170000 		.long	0x1791
 3037 1781 0C       		.uleb128 0xc
 3038 1782 12130000 		.long	0x1312
 3039 1786 0C       		.uleb128 0xc
 3040 1787 55130000 		.long	0x1355
 3041 178b 0C       		.uleb128 0xc
 3042 178c 13120000 		.long	0x1213
 3043 1790 00       		.byte	0
 3044 1791 40       		.uleb128 0x40
GAS LISTING /tmp/ccMbRvxK.s 			page 57


 3045 1792 00000000 		.long	.LASF253
 3046 1796 18       		.byte	0x18
 3047 1797 A101     		.value	0x1a1
 3048 1799 13120000 		.long	0x1213
 3049 179d B6170000 		.long	0x17b6
 3050 17a1 0C       		.uleb128 0xc
 3051 17a2 D6150000 		.long	0x15d6
 3052 17a6 0C       		.uleb128 0xc
 3053 17a7 B6170000 		.long	0x17b6
 3054 17ab 0C       		.uleb128 0xc
 3055 17ac 13120000 		.long	0x1213
 3056 17b0 0C       		.uleb128 0xc
 3057 17b1 F5130000 		.long	0x13f5
 3058 17b5 00       		.byte	0
 3059 17b6 3F       		.uleb128 0x3f
 3060 17b7 08       		.byte	0x8
 3061 17b8 55130000 		.long	0x1355
 3062 17bc 40       		.uleb128 0x40
 3063 17bd 00000000 		.long	.LASF254
 3064 17c1 18       		.byte	0x18
 3065 17c2 0301     		.value	0x103
 3066 17c4 13120000 		.long	0x1213
 3067 17c8 D7170000 		.long	0x17d7
 3068 17cc 0C       		.uleb128 0xc
 3069 17cd 55130000 		.long	0x1355
 3070 17d1 0C       		.uleb128 0xc
 3071 17d2 55130000 		.long	0x1355
 3072 17d6 00       		.byte	0
 3073 17d7 40       		.uleb128 0x40
 3074 17d8 00000000 		.long	.LASF255
 3075 17dc 18       		.byte	0x18
 3076 17dd C501     		.value	0x1c5
 3077 17df F2170000 		.long	0x17f2
 3078 17e3 F2170000 		.long	0x17f2
 3079 17e7 0C       		.uleb128 0xc
 3080 17e8 55130000 		.long	0x1355
 3081 17ec 0C       		.uleb128 0xc
 3082 17ed F9170000 		.long	0x17f9
 3083 17f1 00       		.byte	0
 3084 17f2 36       		.uleb128 0x36
 3085 17f3 08       		.byte	0x8
 3086 17f4 04       		.byte	0x4
 3087 17f5 00000000 		.long	.LASF256
 3088 17f9 3F       		.uleb128 0x3f
 3089 17fa 08       		.byte	0x8
 3090 17fb 12130000 		.long	0x1312
 3091 17ff 40       		.uleb128 0x40
 3092 1800 00000000 		.long	.LASF257
 3093 1804 18       		.byte	0x18
 3094 1805 CC01     		.value	0x1cc
 3095 1807 1A180000 		.long	0x181a
 3096 180b 1A180000 		.long	0x181a
 3097 180f 0C       		.uleb128 0xc
 3098 1810 55130000 		.long	0x1355
 3099 1814 0C       		.uleb128 0xc
 3100 1815 F9170000 		.long	0x17f9
 3101 1819 00       		.byte	0
GAS LISTING /tmp/ccMbRvxK.s 			page 58


 3102 181a 36       		.uleb128 0x36
 3103 181b 04       		.byte	0x4
 3104 181c 04       		.byte	0x4
 3105 181d 00000000 		.long	.LASF258
 3106 1821 40       		.uleb128 0x40
 3107 1822 00000000 		.long	.LASF259
 3108 1826 18       		.byte	0x18
 3109 1827 1D01     		.value	0x11d
 3110 1829 12130000 		.long	0x1312
 3111 182d 41180000 		.long	0x1841
 3112 1831 0C       		.uleb128 0xc
 3113 1832 12130000 		.long	0x1312
 3114 1836 0C       		.uleb128 0xc
 3115 1837 55130000 		.long	0x1355
 3116 183b 0C       		.uleb128 0xc
 3117 183c F9170000 		.long	0x17f9
 3118 1840 00       		.byte	0
 3119 1841 40       		.uleb128 0x40
 3120 1842 00000000 		.long	.LASF260
 3121 1846 18       		.byte	0x18
 3122 1847 D701     		.value	0x1d7
 3123 1849 61180000 		.long	0x1861
 3124 184d 61180000 		.long	0x1861
 3125 1851 0C       		.uleb128 0xc
 3126 1852 55130000 		.long	0x1355
 3127 1856 0C       		.uleb128 0xc
 3128 1857 F9170000 		.long	0x17f9
 3129 185b 0C       		.uleb128 0xc
 3130 185c 8C120000 		.long	0x128c
 3131 1860 00       		.byte	0
 3132 1861 36       		.uleb128 0x36
 3133 1862 08       		.byte	0x8
 3134 1863 05       		.byte	0x5
 3135 1864 00000000 		.long	.LASF261
 3136 1868 40       		.uleb128 0x40
 3137 1869 00000000 		.long	.LASF262
 3138 186d 18       		.byte	0x18
 3139 186e DC01     		.value	0x1dc
 3140 1870 1E120000 		.long	0x121e
 3141 1874 88180000 		.long	0x1888
 3142 1878 0C       		.uleb128 0xc
 3143 1879 55130000 		.long	0x1355
 3144 187d 0C       		.uleb128 0xc
 3145 187e F9170000 		.long	0x17f9
 3146 1882 0C       		.uleb128 0xc
 3147 1883 8C120000 		.long	0x128c
 3148 1887 00       		.byte	0
 3149 1888 43       		.uleb128 0x43
 3150 1889 00000000 		.long	.LASF263
 3151 188d 18       		.byte	0x18
 3152 188e C7       		.byte	0xc7
 3153 188f 13120000 		.long	0x1213
 3154 1893 A7180000 		.long	0x18a7
 3155 1897 0C       		.uleb128 0xc
 3156 1898 12130000 		.long	0x1312
 3157 189c 0C       		.uleb128 0xc
 3158 189d 55130000 		.long	0x1355
GAS LISTING /tmp/ccMbRvxK.s 			page 59


 3159 18a1 0C       		.uleb128 0xc
 3160 18a2 13120000 		.long	0x1213
 3161 18a6 00       		.byte	0
 3162 18a7 40       		.uleb128 0x40
 3163 18a8 00000000 		.long	.LASF264
 3164 18ac 18       		.byte	0x18
 3165 18ad 6801     		.value	0x168
 3166 18af 8C120000 		.long	0x128c
 3167 18b3 BD180000 		.long	0x18bd
 3168 18b7 0C       		.uleb128 0xc
 3169 18b8 25120000 		.long	0x1225
 3170 18bc 00       		.byte	0
 3171 18bd 40       		.uleb128 0x40
 3172 18be 00000000 		.long	.LASF265
 3173 18c2 18       		.byte	0x18
 3174 18c3 4801     		.value	0x148
 3175 18c5 8C120000 		.long	0x128c
 3176 18c9 DD180000 		.long	0x18dd
 3177 18cd 0C       		.uleb128 0xc
 3178 18ce 55130000 		.long	0x1355
 3179 18d2 0C       		.uleb128 0xc
 3180 18d3 55130000 		.long	0x1355
 3181 18d7 0C       		.uleb128 0xc
 3182 18d8 13120000 		.long	0x1213
 3183 18dc 00       		.byte	0
 3184 18dd 40       		.uleb128 0x40
 3185 18de 00000000 		.long	.LASF266
 3186 18e2 18       		.byte	0x18
 3187 18e3 4C01     		.value	0x14c
 3188 18e5 12130000 		.long	0x1312
 3189 18e9 FD180000 		.long	0x18fd
 3190 18ed 0C       		.uleb128 0xc
 3191 18ee 12130000 		.long	0x1312
 3192 18f2 0C       		.uleb128 0xc
 3193 18f3 55130000 		.long	0x1355
 3194 18f7 0C       		.uleb128 0xc
 3195 18f8 13120000 		.long	0x1213
 3196 18fc 00       		.byte	0
 3197 18fd 40       		.uleb128 0x40
 3198 18fe 00000000 		.long	.LASF267
 3199 1902 18       		.byte	0x18
 3200 1903 5101     		.value	0x151
 3201 1905 12130000 		.long	0x1312
 3202 1909 1D190000 		.long	0x191d
 3203 190d 0C       		.uleb128 0xc
 3204 190e 12130000 		.long	0x1312
 3205 1912 0C       		.uleb128 0xc
 3206 1913 55130000 		.long	0x1355
 3207 1917 0C       		.uleb128 0xc
 3208 1918 13120000 		.long	0x1213
 3209 191c 00       		.byte	0
 3210 191d 40       		.uleb128 0x40
 3211 191e 00000000 		.long	.LASF268
 3212 1922 18       		.byte	0x18
 3213 1923 5501     		.value	0x155
 3214 1925 12130000 		.long	0x1312
 3215 1929 3D190000 		.long	0x193d
GAS LISTING /tmp/ccMbRvxK.s 			page 60


 3216 192d 0C       		.uleb128 0xc
 3217 192e 12130000 		.long	0x1312
 3218 1932 0C       		.uleb128 0xc
 3219 1933 18130000 		.long	0x1318
 3220 1937 0C       		.uleb128 0xc
 3221 1938 13120000 		.long	0x1213
 3222 193c 00       		.byte	0
 3223 193d 40       		.uleb128 0x40
 3224 193e 00000000 		.long	.LASF269
 3225 1942 18       		.byte	0x18
 3226 1943 5C02     		.value	0x25c
 3227 1945 8C120000 		.long	0x128c
 3228 1949 54190000 		.long	0x1954
 3229 194d 0C       		.uleb128 0xc
 3230 194e 55130000 		.long	0x1355
 3231 1952 41       		.uleb128 0x41
 3232 1953 00       		.byte	0
 3233 1954 40       		.uleb128 0x40
 3234 1955 00000000 		.long	.LASF270
 3235 1959 18       		.byte	0x18
 3236 195a 8502     		.value	0x285
 3237 195c 8C120000 		.long	0x128c
 3238 1960 6B190000 		.long	0x196b
 3239 1964 0C       		.uleb128 0xc
 3240 1965 55130000 		.long	0x1355
 3241 1969 41       		.uleb128 0x41
 3242 196a 00       		.byte	0
 3243 196b 45       		.uleb128 0x45
 3244 196c 00000000 		.long	.LASF271
 3245 1970 18       		.byte	0x18
 3246 1971 E3       		.byte	0xe3
 3247 1972 00000000 		.long	.LASF271
 3248 1976 55130000 		.long	0x1355
 3249 197a 89190000 		.long	0x1989
 3250 197e 0C       		.uleb128 0xc
 3251 197f 55130000 		.long	0x1355
 3252 1983 0C       		.uleb128 0xc
 3253 1984 18130000 		.long	0x1318
 3254 1988 00       		.byte	0
 3255 1989 1D       		.uleb128 0x1d
 3256 198a 00000000 		.long	.LASF272
 3257 198e 18       		.byte	0x18
 3258 198f 0901     		.value	0x109
 3259 1991 00000000 		.long	.LASF272
 3260 1995 55130000 		.long	0x1355
 3261 1999 A8190000 		.long	0x19a8
 3262 199d 0C       		.uleb128 0xc
 3263 199e 55130000 		.long	0x1355
 3264 19a2 0C       		.uleb128 0xc
 3265 19a3 55130000 		.long	0x1355
 3266 19a7 00       		.byte	0
 3267 19a8 45       		.uleb128 0x45
 3268 19a9 00000000 		.long	.LASF273
 3269 19ad 18       		.byte	0x18
 3270 19ae ED       		.byte	0xed
 3271 19af 00000000 		.long	.LASF273
 3272 19b3 55130000 		.long	0x1355
GAS LISTING /tmp/ccMbRvxK.s 			page 61


 3273 19b7 C6190000 		.long	0x19c6
 3274 19bb 0C       		.uleb128 0xc
 3275 19bc 55130000 		.long	0x1355
 3276 19c0 0C       		.uleb128 0xc
 3277 19c1 18130000 		.long	0x1318
 3278 19c5 00       		.byte	0
 3279 19c6 1D       		.uleb128 0x1d
 3280 19c7 00000000 		.long	.LASF274
 3281 19cb 18       		.byte	0x18
 3282 19cc 1401     		.value	0x114
 3283 19ce 00000000 		.long	.LASF274
 3284 19d2 55130000 		.long	0x1355
 3285 19d6 E5190000 		.long	0x19e5
 3286 19da 0C       		.uleb128 0xc
 3287 19db 55130000 		.long	0x1355
 3288 19df 0C       		.uleb128 0xc
 3289 19e0 55130000 		.long	0x1355
 3290 19e4 00       		.byte	0
 3291 19e5 1D       		.uleb128 0x1d
 3292 19e6 00000000 		.long	.LASF275
 3293 19ea 18       		.byte	0x18
 3294 19eb 3F01     		.value	0x13f
 3295 19ed 00000000 		.long	.LASF275
 3296 19f1 55130000 		.long	0x1355
 3297 19f5 091A0000 		.long	0x1a09
 3298 19f9 0C       		.uleb128 0xc
 3299 19fa 55130000 		.long	0x1355
 3300 19fe 0C       		.uleb128 0xc
 3301 19ff 18130000 		.long	0x1318
 3302 1a03 0C       		.uleb128 0xc
 3303 1a04 13120000 		.long	0x1213
 3304 1a08 00       		.byte	0
 3305 1a09 40       		.uleb128 0x40
 3306 1a0a 00000000 		.long	.LASF276
 3307 1a0e 18       		.byte	0x18
 3308 1a0f CE01     		.value	0x1ce
 3309 1a11 241A0000 		.long	0x1a24
 3310 1a15 241A0000 		.long	0x1a24
 3311 1a19 0C       		.uleb128 0xc
 3312 1a1a 55130000 		.long	0x1355
 3313 1a1e 0C       		.uleb128 0xc
 3314 1a1f F9170000 		.long	0x17f9
 3315 1a23 00       		.byte	0
 3316 1a24 36       		.uleb128 0x36
 3317 1a25 10       		.byte	0x10
 3318 1a26 04       		.byte	0x4
 3319 1a27 00000000 		.long	.LASF277
 3320 1a2b 40       		.uleb128 0x40
 3321 1a2c 00000000 		.long	.LASF278
 3322 1a30 18       		.byte	0x18
 3323 1a31 E601     		.value	0x1e6
 3324 1a33 4B1A0000 		.long	0x1a4b
 3325 1a37 4B1A0000 		.long	0x1a4b
 3326 1a3b 0C       		.uleb128 0xc
 3327 1a3c 55130000 		.long	0x1355
 3328 1a40 0C       		.uleb128 0xc
 3329 1a41 F9170000 		.long	0x17f9
GAS LISTING /tmp/ccMbRvxK.s 			page 62


 3330 1a45 0C       		.uleb128 0xc
 3331 1a46 8C120000 		.long	0x128c
 3332 1a4a 00       		.byte	0
 3333 1a4b 36       		.uleb128 0x36
 3334 1a4c 08       		.byte	0x8
 3335 1a4d 05       		.byte	0x5
 3336 1a4e 00000000 		.long	.LASF279
 3337 1a52 40       		.uleb128 0x40
 3338 1a53 00000000 		.long	.LASF280
 3339 1a57 18       		.byte	0x18
 3340 1a58 ED01     		.value	0x1ed
 3341 1a5a 721A0000 		.long	0x1a72
 3342 1a5e 721A0000 		.long	0x1a72
 3343 1a62 0C       		.uleb128 0xc
 3344 1a63 55130000 		.long	0x1355
 3345 1a67 0C       		.uleb128 0xc
 3346 1a68 F9170000 		.long	0x17f9
 3347 1a6c 0C       		.uleb128 0xc
 3348 1a6d 8C120000 		.long	0x128c
 3349 1a71 00       		.byte	0
 3350 1a72 36       		.uleb128 0x36
 3351 1a73 08       		.byte	0x8
 3352 1a74 07       		.byte	0x7
 3353 1a75 00000000 		.long	.LASF281
 3354 1a79 3F       		.uleb128 0x3f
 3355 1a7a 08       		.byte	0x8
 3356 1a7b 37020000 		.long	0x237
 3357 1a7f 3F       		.uleb128 0x3f
 3358 1a80 08       		.byte	0x8
 3359 1a81 F0030000 		.long	0x3f0
 3360 1a85 46       		.uleb128 0x46
 3361 1a86 08       		.byte	0x8
 3362 1a87 F0030000 		.long	0x3f0
 3363 1a8b 47       		.uleb128 0x47
 3364 1a8c 00000000 		.long	.LASF462
 3365 1a90 48       		.uleb128 0x48
 3366 1a91 08       		.byte	0x8
 3367 1a92 37020000 		.long	0x237
 3368 1a96 46       		.uleb128 0x46
 3369 1a97 08       		.byte	0x8
 3370 1a98 37020000 		.long	0x237
 3371 1a9c 36       		.uleb128 0x36
 3372 1a9d 01       		.byte	0x1
 3373 1a9e 02       		.byte	0x2
 3374 1a9f 00000000 		.long	.LASF282
 3375 1aa3 3F       		.uleb128 0x3f
 3376 1aa4 08       		.byte	0x8
 3377 1aa5 0D040000 		.long	0x40d
 3378 1aa9 36       		.uleb128 0x36
 3379 1aaa 01       		.byte	0x1
 3380 1aab 08       		.byte	0x8
 3381 1aac 00000000 		.long	.LASF283
 3382 1ab0 36       		.uleb128 0x36
 3383 1ab1 01       		.byte	0x1
 3384 1ab2 06       		.byte	0x6
 3385 1ab3 00000000 		.long	.LASF284
 3386 1ab7 36       		.uleb128 0x36
GAS LISTING /tmp/ccMbRvxK.s 			page 63


 3387 1ab8 02       		.byte	0x2
 3388 1ab9 05       		.byte	0x5
 3389 1aba 00000000 		.long	.LASF285
 3390 1abe 13       		.uleb128 0x13
 3391 1abf 9C1A0000 		.long	0x1a9c
 3392 1ac3 3F       		.uleb128 0x3f
 3393 1ac4 08       		.byte	0x8
 3394 1ac5 65040000 		.long	0x465
 3395 1ac9 3F       		.uleb128 0x3f
 3396 1aca 08       		.byte	0x8
 3397 1acb BD040000 		.long	0x4bd
 3398 1acf 13       		.uleb128 0x13
 3399 1ad0 1E120000 		.long	0x121e
 3400 1ad4 07       		.uleb128 0x7
 3401 1ad5 00000000 		.long	.LASF286
 3402 1ad9 07       		.byte	0x7
 3403 1ada 37       		.byte	0x37
 3404 1adb E71A0000 		.long	0x1ae7
 3405 1adf 04       		.uleb128 0x4
 3406 1ae0 07       		.byte	0x7
 3407 1ae1 38       		.byte	0x38
 3408 1ae2 CA040000 		.long	0x4ca
 3409 1ae6 00       		.byte	0
 3410 1ae7 46       		.uleb128 0x46
 3411 1ae8 08       		.byte	0x8
 3412 1ae9 DD040000 		.long	0x4dd
 3413 1aed 46       		.uleb128 0x46
 3414 1aee 08       		.byte	0x8
 3415 1aef 0D050000 		.long	0x50d
 3416 1af3 3F       		.uleb128 0x3f
 3417 1af4 08       		.byte	0x8
 3418 1af5 0D050000 		.long	0x50d
 3419 1af9 3F       		.uleb128 0x3f
 3420 1afa 08       		.byte	0x8
 3421 1afb DD040000 		.long	0x4dd
 3422 1aff 46       		.uleb128 0x46
 3423 1b00 08       		.byte	0x8
 3424 1b01 34060000 		.long	0x634
 3425 1b05 14       		.uleb128 0x14
 3426 1b06 00000000 		.long	.LASF287
 3427 1b0a 1A       		.byte	0x1a
 3428 1b0b 24       		.byte	0x24
 3429 1b0c B01A0000 		.long	0x1ab0
 3430 1b10 14       		.uleb128 0x14
 3431 1b11 00000000 		.long	.LASF288
 3432 1b15 1A       		.byte	0x1a
 3433 1b16 25       		.byte	0x25
 3434 1b17 B71A0000 		.long	0x1ab7
 3435 1b1b 14       		.uleb128 0x14
 3436 1b1c 00000000 		.long	.LASF289
 3437 1b20 1A       		.byte	0x1a
 3438 1b21 26       		.byte	0x26
 3439 1b22 8C120000 		.long	0x128c
 3440 1b26 14       		.uleb128 0x14
 3441 1b27 00000000 		.long	.LASF290
 3442 1b2b 1A       		.byte	0x1a
 3443 1b2c 28       		.byte	0x28
GAS LISTING /tmp/ccMbRvxK.s 			page 64


 3444 1b2d 61180000 		.long	0x1861
 3445 1b31 14       		.uleb128 0x14
 3446 1b32 00000000 		.long	.LASF291
 3447 1b36 1A       		.byte	0x1a
 3448 1b37 30       		.byte	0x30
 3449 1b38 A91A0000 		.long	0x1aa9
 3450 1b3c 14       		.uleb128 0x14
 3451 1b3d 00000000 		.long	.LASF292
 3452 1b41 1A       		.byte	0x1a
 3453 1b42 31       		.byte	0x31
 3454 1b43 A9120000 		.long	0x12a9
 3455 1b47 14       		.uleb128 0x14
 3456 1b48 00000000 		.long	.LASF293
 3457 1b4c 1A       		.byte	0x1a
 3458 1b4d 33       		.byte	0x33
 3459 1b4e 0A120000 		.long	0x120a
 3460 1b52 14       		.uleb128 0x14
 3461 1b53 00000000 		.long	.LASF294
 3462 1b57 1A       		.byte	0x1a
 3463 1b58 37       		.byte	0x37
 3464 1b59 1E120000 		.long	0x121e
 3465 1b5d 14       		.uleb128 0x14
 3466 1b5e 00000000 		.long	.LASF295
 3467 1b62 1A       		.byte	0x1a
 3468 1b63 41       		.byte	0x41
 3469 1b64 B01A0000 		.long	0x1ab0
 3470 1b68 14       		.uleb128 0x14
 3471 1b69 00000000 		.long	.LASF296
 3472 1b6d 1A       		.byte	0x1a
 3473 1b6e 42       		.byte	0x42
 3474 1b6f B71A0000 		.long	0x1ab7
 3475 1b73 14       		.uleb128 0x14
 3476 1b74 00000000 		.long	.LASF297
 3477 1b78 1A       		.byte	0x1a
 3478 1b79 43       		.byte	0x43
 3479 1b7a 8C120000 		.long	0x128c
 3480 1b7e 14       		.uleb128 0x14
 3481 1b7f 00000000 		.long	.LASF298
 3482 1b83 1A       		.byte	0x1a
 3483 1b84 45       		.byte	0x45
 3484 1b85 61180000 		.long	0x1861
 3485 1b89 14       		.uleb128 0x14
 3486 1b8a 00000000 		.long	.LASF299
 3487 1b8e 1A       		.byte	0x1a
 3488 1b8f 4C       		.byte	0x4c
 3489 1b90 A91A0000 		.long	0x1aa9
 3490 1b94 14       		.uleb128 0x14
 3491 1b95 00000000 		.long	.LASF300
 3492 1b99 1A       		.byte	0x1a
 3493 1b9a 4D       		.byte	0x4d
 3494 1b9b A9120000 		.long	0x12a9
 3495 1b9f 14       		.uleb128 0x14
 3496 1ba0 00000000 		.long	.LASF301
 3497 1ba4 1A       		.byte	0x1a
 3498 1ba5 4E       		.byte	0x4e
 3499 1ba6 0A120000 		.long	0x120a
 3500 1baa 14       		.uleb128 0x14
GAS LISTING /tmp/ccMbRvxK.s 			page 65


 3501 1bab 00000000 		.long	.LASF302
 3502 1baf 1A       		.byte	0x1a
 3503 1bb0 50       		.byte	0x50
 3504 1bb1 1E120000 		.long	0x121e
 3505 1bb5 14       		.uleb128 0x14
 3506 1bb6 00000000 		.long	.LASF303
 3507 1bba 1A       		.byte	0x1a
 3508 1bbb 5A       		.byte	0x5a
 3509 1bbc B01A0000 		.long	0x1ab0
 3510 1bc0 14       		.uleb128 0x14
 3511 1bc1 00000000 		.long	.LASF304
 3512 1bc5 1A       		.byte	0x1a
 3513 1bc6 5C       		.byte	0x5c
 3514 1bc7 61180000 		.long	0x1861
 3515 1bcb 14       		.uleb128 0x14
 3516 1bcc 00000000 		.long	.LASF305
 3517 1bd0 1A       		.byte	0x1a
 3518 1bd1 5D       		.byte	0x5d
 3519 1bd2 61180000 		.long	0x1861
 3520 1bd6 14       		.uleb128 0x14
 3521 1bd7 00000000 		.long	.LASF306
 3522 1bdb 1A       		.byte	0x1a
 3523 1bdc 5E       		.byte	0x5e
 3524 1bdd 61180000 		.long	0x1861
 3525 1be1 14       		.uleb128 0x14
 3526 1be2 00000000 		.long	.LASF307
 3527 1be6 1A       		.byte	0x1a
 3528 1be7 67       		.byte	0x67
 3529 1be8 A91A0000 		.long	0x1aa9
 3530 1bec 14       		.uleb128 0x14
 3531 1bed 00000000 		.long	.LASF308
 3532 1bf1 1A       		.byte	0x1a
 3533 1bf2 69       		.byte	0x69
 3534 1bf3 1E120000 		.long	0x121e
 3535 1bf7 14       		.uleb128 0x14
 3536 1bf8 00000000 		.long	.LASF309
 3537 1bfc 1A       		.byte	0x1a
 3538 1bfd 6A       		.byte	0x6a
 3539 1bfe 1E120000 		.long	0x121e
 3540 1c02 14       		.uleb128 0x14
 3541 1c03 00000000 		.long	.LASF310
 3542 1c07 1A       		.byte	0x1a
 3543 1c08 6B       		.byte	0x6b
 3544 1c09 1E120000 		.long	0x121e
 3545 1c0d 14       		.uleb128 0x14
 3546 1c0e 00000000 		.long	.LASF311
 3547 1c12 1A       		.byte	0x1a
 3548 1c13 77       		.byte	0x77
 3549 1c14 61180000 		.long	0x1861
 3550 1c18 14       		.uleb128 0x14
 3551 1c19 00000000 		.long	.LASF312
 3552 1c1d 1A       		.byte	0x1a
 3553 1c1e 7A       		.byte	0x7a
 3554 1c1f 1E120000 		.long	0x121e
 3555 1c23 14       		.uleb128 0x14
 3556 1c24 00000000 		.long	.LASF313
 3557 1c28 1A       		.byte	0x1a
GAS LISTING /tmp/ccMbRvxK.s 			page 66


 3558 1c29 86       		.byte	0x86
 3559 1c2a 61180000 		.long	0x1861
 3560 1c2e 14       		.uleb128 0x14
 3561 1c2f 00000000 		.long	.LASF314
 3562 1c33 1A       		.byte	0x1a
 3563 1c34 87       		.byte	0x87
 3564 1c35 1E120000 		.long	0x121e
 3565 1c39 36       		.uleb128 0x36
 3566 1c3a 02       		.byte	0x2
 3567 1c3b 10       		.byte	0x10
 3568 1c3c 00000000 		.long	.LASF315
 3569 1c40 36       		.uleb128 0x36
 3570 1c41 04       		.byte	0x4
 3571 1c42 10       		.byte	0x10
 3572 1c43 00000000 		.long	.LASF316
 3573 1c47 16       		.uleb128 0x16
 3574 1c48 00000000 		.long	.LASF317
 3575 1c4c 60       		.byte	0x60
 3576 1c4d 1B       		.byte	0x1b
 3577 1c4e 35       		.byte	0x35
 3578 1c4f 741D0000 		.long	0x1d74
 3579 1c53 09       		.uleb128 0x9
 3580 1c54 00000000 		.long	.LASF318
 3581 1c58 1B       		.byte	0x1b
 3582 1c59 39       		.byte	0x39
 3583 1c5a D6150000 		.long	0x15d6
 3584 1c5e 00       		.byte	0
 3585 1c5f 09       		.uleb128 0x9
 3586 1c60 00000000 		.long	.LASF319
 3587 1c64 1B       		.byte	0x1b
 3588 1c65 3A       		.byte	0x3a
 3589 1c66 D6150000 		.long	0x15d6
 3590 1c6a 08       		.byte	0x8
 3591 1c6b 09       		.uleb128 0x9
 3592 1c6c 00000000 		.long	.LASF320
 3593 1c70 1B       		.byte	0x1b
 3594 1c71 40       		.byte	0x40
 3595 1c72 D6150000 		.long	0x15d6
 3596 1c76 10       		.byte	0x10
 3597 1c77 09       		.uleb128 0x9
 3598 1c78 00000000 		.long	.LASF321
 3599 1c7c 1B       		.byte	0x1b
 3600 1c7d 46       		.byte	0x46
 3601 1c7e D6150000 		.long	0x15d6
 3602 1c82 18       		.byte	0x18
 3603 1c83 09       		.uleb128 0x9
 3604 1c84 00000000 		.long	.LASF322
 3605 1c88 1B       		.byte	0x1b
 3606 1c89 47       		.byte	0x47
 3607 1c8a D6150000 		.long	0x15d6
 3608 1c8e 20       		.byte	0x20
 3609 1c8f 09       		.uleb128 0x9
 3610 1c90 00000000 		.long	.LASF323
 3611 1c94 1B       		.byte	0x1b
 3612 1c95 48       		.byte	0x48
 3613 1c96 D6150000 		.long	0x15d6
 3614 1c9a 28       		.byte	0x28
GAS LISTING /tmp/ccMbRvxK.s 			page 67


 3615 1c9b 09       		.uleb128 0x9
 3616 1c9c 00000000 		.long	.LASF324
 3617 1ca0 1B       		.byte	0x1b
 3618 1ca1 49       		.byte	0x49
 3619 1ca2 D6150000 		.long	0x15d6
 3620 1ca6 30       		.byte	0x30
 3621 1ca7 09       		.uleb128 0x9
 3622 1ca8 00000000 		.long	.LASF325
 3623 1cac 1B       		.byte	0x1b
 3624 1cad 4A       		.byte	0x4a
 3625 1cae D6150000 		.long	0x15d6
 3626 1cb2 38       		.byte	0x38
 3627 1cb3 09       		.uleb128 0x9
 3628 1cb4 00000000 		.long	.LASF326
 3629 1cb8 1B       		.byte	0x1b
 3630 1cb9 4B       		.byte	0x4b
 3631 1cba D6150000 		.long	0x15d6
 3632 1cbe 40       		.byte	0x40
 3633 1cbf 09       		.uleb128 0x9
 3634 1cc0 00000000 		.long	.LASF327
 3635 1cc4 1B       		.byte	0x1b
 3636 1cc5 4C       		.byte	0x4c
 3637 1cc6 D6150000 		.long	0x15d6
 3638 1cca 48       		.byte	0x48
 3639 1ccb 09       		.uleb128 0x9
 3640 1ccc 00000000 		.long	.LASF328
 3641 1cd0 1B       		.byte	0x1b
 3642 1cd1 4D       		.byte	0x4d
 3643 1cd2 85120000 		.long	0x1285
 3644 1cd6 50       		.byte	0x50
 3645 1cd7 09       		.uleb128 0x9
 3646 1cd8 00000000 		.long	.LASF329
 3647 1cdc 1B       		.byte	0x1b
 3648 1cdd 4E       		.byte	0x4e
 3649 1cde 85120000 		.long	0x1285
 3650 1ce2 51       		.byte	0x51
 3651 1ce3 09       		.uleb128 0x9
 3652 1ce4 00000000 		.long	.LASF330
 3653 1ce8 1B       		.byte	0x1b
 3654 1ce9 50       		.byte	0x50
 3655 1cea 85120000 		.long	0x1285
 3656 1cee 52       		.byte	0x52
 3657 1cef 09       		.uleb128 0x9
 3658 1cf0 00000000 		.long	.LASF331
 3659 1cf4 1B       		.byte	0x1b
 3660 1cf5 52       		.byte	0x52
 3661 1cf6 85120000 		.long	0x1285
 3662 1cfa 53       		.byte	0x53
 3663 1cfb 09       		.uleb128 0x9
 3664 1cfc 00000000 		.long	.LASF332
 3665 1d00 1B       		.byte	0x1b
 3666 1d01 54       		.byte	0x54
 3667 1d02 85120000 		.long	0x1285
 3668 1d06 54       		.byte	0x54
 3669 1d07 09       		.uleb128 0x9
 3670 1d08 00000000 		.long	.LASF333
 3671 1d0c 1B       		.byte	0x1b
GAS LISTING /tmp/ccMbRvxK.s 			page 68


 3672 1d0d 56       		.byte	0x56
 3673 1d0e 85120000 		.long	0x1285
 3674 1d12 55       		.byte	0x55
 3675 1d13 09       		.uleb128 0x9
 3676 1d14 00000000 		.long	.LASF334
 3677 1d18 1B       		.byte	0x1b
 3678 1d19 5D       		.byte	0x5d
 3679 1d1a 85120000 		.long	0x1285
 3680 1d1e 56       		.byte	0x56
 3681 1d1f 09       		.uleb128 0x9
 3682 1d20 00000000 		.long	.LASF335
 3683 1d24 1B       		.byte	0x1b
 3684 1d25 5E       		.byte	0x5e
 3685 1d26 85120000 		.long	0x1285
 3686 1d2a 57       		.byte	0x57
 3687 1d2b 09       		.uleb128 0x9
 3688 1d2c 00000000 		.long	.LASF336
 3689 1d30 1B       		.byte	0x1b
 3690 1d31 61       		.byte	0x61
 3691 1d32 85120000 		.long	0x1285
 3692 1d36 58       		.byte	0x58
 3693 1d37 09       		.uleb128 0x9
 3694 1d38 00000000 		.long	.LASF337
 3695 1d3c 1B       		.byte	0x1b
 3696 1d3d 63       		.byte	0x63
 3697 1d3e 85120000 		.long	0x1285
 3698 1d42 59       		.byte	0x59
 3699 1d43 09       		.uleb128 0x9
 3700 1d44 00000000 		.long	.LASF338
 3701 1d48 1B       		.byte	0x1b
 3702 1d49 65       		.byte	0x65
 3703 1d4a 85120000 		.long	0x1285
 3704 1d4e 5A       		.byte	0x5a
 3705 1d4f 09       		.uleb128 0x9
 3706 1d50 00000000 		.long	.LASF339
 3707 1d54 1B       		.byte	0x1b
 3708 1d55 67       		.byte	0x67
 3709 1d56 85120000 		.long	0x1285
 3710 1d5a 5B       		.byte	0x5b
 3711 1d5b 09       		.uleb128 0x9
 3712 1d5c 00000000 		.long	.LASF340
 3713 1d60 1B       		.byte	0x1b
 3714 1d61 6E       		.byte	0x6e
 3715 1d62 85120000 		.long	0x1285
 3716 1d66 5C       		.byte	0x5c
 3717 1d67 09       		.uleb128 0x9
 3718 1d68 00000000 		.long	.LASF341
 3719 1d6c 1B       		.byte	0x1b
 3720 1d6d 6F       		.byte	0x6f
 3721 1d6e 85120000 		.long	0x1285
 3722 1d72 5D       		.byte	0x5d
 3723 1d73 00       		.byte	0
 3724 1d74 43       		.uleb128 0x43
 3725 1d75 00000000 		.long	.LASF342
 3726 1d79 1B       		.byte	0x1b
 3727 1d7a 7C       		.byte	0x7c
 3728 1d7b D6150000 		.long	0x15d6
GAS LISTING /tmp/ccMbRvxK.s 			page 69


 3729 1d7f 8E1D0000 		.long	0x1d8e
 3730 1d83 0C       		.uleb128 0xc
 3731 1d84 8C120000 		.long	0x128c
 3732 1d88 0C       		.uleb128 0xc
 3733 1d89 B5120000 		.long	0x12b5
 3734 1d8d 00       		.byte	0
 3735 1d8e 49       		.uleb128 0x49
 3736 1d8f 00000000 		.long	.LASF344
 3737 1d93 1B       		.byte	0x1b
 3738 1d94 7F       		.byte	0x7f
 3739 1d95 991D0000 		.long	0x1d99
 3740 1d99 3F       		.uleb128 0x3f
 3741 1d9a 08       		.byte	0x8
 3742 1d9b 471C0000 		.long	0x1c47
 3743 1d9f 14       		.uleb128 0x14
 3744 1da0 00000000 		.long	.LASF345
 3745 1da4 1C       		.byte	0x1c
 3746 1da5 28       		.byte	0x28
 3747 1da6 8C120000 		.long	0x128c
 3748 1daa 14       		.uleb128 0x14
 3749 1dab 00000000 		.long	.LASF346
 3750 1daf 1C       		.byte	0x1c
 3751 1db0 83       		.byte	0x83
 3752 1db1 61180000 		.long	0x1861
 3753 1db5 14       		.uleb128 0x14
 3754 1db6 00000000 		.long	.LASF347
 3755 1dba 1C       		.byte	0x1c
 3756 1dbb 84       		.byte	0x84
 3757 1dbc 61180000 		.long	0x1861
 3758 1dc0 14       		.uleb128 0x14
 3759 1dc1 00000000 		.long	.LASF348
 3760 1dc5 1D       		.byte	0x1d
 3761 1dc6 20       		.byte	0x20
 3762 1dc7 8C120000 		.long	0x128c
 3763 1dcb 3F       		.uleb128 0x3f
 3764 1dcc 08       		.byte	0x8
 3765 1dcd D11D0000 		.long	0x1dd1
 3766 1dd1 4A       		.uleb128 0x4a
 3767 1dd2 39       		.uleb128 0x39
 3768 1dd3 08       		.byte	0x8
 3769 1dd4 1E       		.byte	0x1e
 3770 1dd5 62       		.byte	0x62
 3771 1dd6 00000000 		.long	.LASF350
 3772 1dda F71D0000 		.long	0x1df7
 3773 1dde 09       		.uleb128 0x9
 3774 1ddf 00000000 		.long	.LASF351
 3775 1de3 1E       		.byte	0x1e
 3776 1de4 63       		.byte	0x63
 3777 1de5 8C120000 		.long	0x128c
 3778 1de9 00       		.byte	0
 3779 1dea 4B       		.uleb128 0x4b
 3780 1deb 72656D00 		.string	"rem"
 3781 1def 1E       		.byte	0x1e
 3782 1df0 64       		.byte	0x64
 3783 1df1 8C120000 		.long	0x128c
 3784 1df5 04       		.byte	0x4
 3785 1df6 00       		.byte	0
GAS LISTING /tmp/ccMbRvxK.s 			page 70


 3786 1df7 14       		.uleb128 0x14
 3787 1df8 00000000 		.long	.LASF352
 3788 1dfc 1E       		.byte	0x1e
 3789 1dfd 65       		.byte	0x65
 3790 1dfe D21D0000 		.long	0x1dd2
 3791 1e02 39       		.uleb128 0x39
 3792 1e03 10       		.byte	0x10
 3793 1e04 1E       		.byte	0x1e
 3794 1e05 6A       		.byte	0x6a
 3795 1e06 00000000 		.long	.LASF353
 3796 1e0a 271E0000 		.long	0x1e27
 3797 1e0e 09       		.uleb128 0x9
 3798 1e0f 00000000 		.long	.LASF351
 3799 1e13 1E       		.byte	0x1e
 3800 1e14 6B       		.byte	0x6b
 3801 1e15 61180000 		.long	0x1861
 3802 1e19 00       		.byte	0
 3803 1e1a 4B       		.uleb128 0x4b
 3804 1e1b 72656D00 		.string	"rem"
 3805 1e1f 1E       		.byte	0x1e
 3806 1e20 6C       		.byte	0x6c
 3807 1e21 61180000 		.long	0x1861
 3808 1e25 08       		.byte	0x8
 3809 1e26 00       		.byte	0
 3810 1e27 14       		.uleb128 0x14
 3811 1e28 00000000 		.long	.LASF354
 3812 1e2c 1E       		.byte	0x1e
 3813 1e2d 6D       		.byte	0x6d
 3814 1e2e 021E0000 		.long	0x1e02
 3815 1e32 39       		.uleb128 0x39
 3816 1e33 10       		.byte	0x10
 3817 1e34 1E       		.byte	0x1e
 3818 1e35 76       		.byte	0x76
 3819 1e36 00000000 		.long	.LASF355
 3820 1e3a 571E0000 		.long	0x1e57
 3821 1e3e 09       		.uleb128 0x9
 3822 1e3f 00000000 		.long	.LASF351
 3823 1e43 1E       		.byte	0x1e
 3824 1e44 77       		.byte	0x77
 3825 1e45 4B1A0000 		.long	0x1a4b
 3826 1e49 00       		.byte	0
 3827 1e4a 4B       		.uleb128 0x4b
 3828 1e4b 72656D00 		.string	"rem"
 3829 1e4f 1E       		.byte	0x1e
 3830 1e50 78       		.byte	0x78
 3831 1e51 4B1A0000 		.long	0x1a4b
 3832 1e55 08       		.byte	0x8
 3833 1e56 00       		.byte	0
 3834 1e57 14       		.uleb128 0x14
 3835 1e58 00000000 		.long	.LASF356
 3836 1e5c 1E       		.byte	0x1e
 3837 1e5d 79       		.byte	0x79
 3838 1e5e 321E0000 		.long	0x1e32
 3839 1e62 38       		.uleb128 0x38
 3840 1e63 00000000 		.long	.LASF357
 3841 1e67 1E       		.byte	0x1e
 3842 1e68 E502     		.value	0x2e5
GAS LISTING /tmp/ccMbRvxK.s 			page 71


 3843 1e6a 6E1E0000 		.long	0x1e6e
 3844 1e6e 3F       		.uleb128 0x3f
 3845 1e6f 08       		.byte	0x8
 3846 1e70 741E0000 		.long	0x1e74
 3847 1e74 4C       		.uleb128 0x4c
 3848 1e75 8C120000 		.long	0x128c
 3849 1e79 881E0000 		.long	0x1e88
 3850 1e7d 0C       		.uleb128 0xc
 3851 1e7e CB1D0000 		.long	0x1dcb
 3852 1e82 0C       		.uleb128 0xc
 3853 1e83 CB1D0000 		.long	0x1dcb
 3854 1e87 00       		.byte	0
 3855 1e88 40       		.uleb128 0x40
 3856 1e89 00000000 		.long	.LASF358
 3857 1e8d 1E       		.byte	0x1e
 3858 1e8e 0702     		.value	0x207
 3859 1e90 8C120000 		.long	0x128c
 3860 1e94 9E1E0000 		.long	0x1e9e
 3861 1e98 0C       		.uleb128 0xc
 3862 1e99 9E1E0000 		.long	0x1e9e
 3863 1e9d 00       		.byte	0
 3864 1e9e 3F       		.uleb128 0x3f
 3865 1e9f 08       		.byte	0x8
 3866 1ea0 A41E0000 		.long	0x1ea4
 3867 1ea4 4D       		.uleb128 0x4d
 3868 1ea5 1D       		.uleb128 0x1d
 3869 1ea6 00000000 		.long	.LASF359
 3870 1eaa 1E       		.byte	0x1e
 3871 1eab 0C02     		.value	0x20c
 3872 1ead 00000000 		.long	.LASF359
 3873 1eb1 8C120000 		.long	0x128c
 3874 1eb5 BF1E0000 		.long	0x1ebf
 3875 1eb9 0C       		.uleb128 0xc
 3876 1eba 9E1E0000 		.long	0x1e9e
 3877 1ebe 00       		.byte	0
 3878 1ebf 43       		.uleb128 0x43
 3879 1ec0 00000000 		.long	.LASF360
 3880 1ec4 1E       		.byte	0x1e
 3881 1ec5 90       		.byte	0x90
 3882 1ec6 F2170000 		.long	0x17f2
 3883 1eca D41E0000 		.long	0x1ed4
 3884 1ece 0C       		.uleb128 0xc
 3885 1ecf B5120000 		.long	0x12b5
 3886 1ed3 00       		.byte	0
 3887 1ed4 43       		.uleb128 0x43
 3888 1ed5 00000000 		.long	.LASF361
 3889 1ed9 1E       		.byte	0x1e
 3890 1eda 93       		.byte	0x93
 3891 1edb 8C120000 		.long	0x128c
 3892 1edf E91E0000 		.long	0x1ee9
 3893 1ee3 0C       		.uleb128 0xc
 3894 1ee4 B5120000 		.long	0x12b5
 3895 1ee8 00       		.byte	0
 3896 1ee9 43       		.uleb128 0x43
 3897 1eea 00000000 		.long	.LASF362
 3898 1eee 1E       		.byte	0x1e
 3899 1eef 96       		.byte	0x96
GAS LISTING /tmp/ccMbRvxK.s 			page 72


 3900 1ef0 61180000 		.long	0x1861
 3901 1ef4 FE1E0000 		.long	0x1efe
 3902 1ef8 0C       		.uleb128 0xc
 3903 1ef9 B5120000 		.long	0x12b5
 3904 1efd 00       		.byte	0
 3905 1efe 40       		.uleb128 0x40
 3906 1eff 00000000 		.long	.LASF363
 3907 1f03 1E       		.byte	0x1e
 3908 1f04 F202     		.value	0x2f2
 3909 1f06 11120000 		.long	0x1211
 3910 1f0a 281F0000 		.long	0x1f28
 3911 1f0e 0C       		.uleb128 0xc
 3912 1f0f CB1D0000 		.long	0x1dcb
 3913 1f13 0C       		.uleb128 0xc
 3914 1f14 CB1D0000 		.long	0x1dcb
 3915 1f18 0C       		.uleb128 0xc
 3916 1f19 13120000 		.long	0x1213
 3917 1f1d 0C       		.uleb128 0xc
 3918 1f1e 13120000 		.long	0x1213
 3919 1f22 0C       		.uleb128 0xc
 3920 1f23 621E0000 		.long	0x1e62
 3921 1f27 00       		.byte	0
 3922 1f28 4E       		.uleb128 0x4e
 3923 1f29 64697600 		.string	"div"
 3924 1f2d 1E       		.byte	0x1e
 3925 1f2e 1403     		.value	0x314
 3926 1f30 F71D0000 		.long	0x1df7
 3927 1f34 431F0000 		.long	0x1f43
 3928 1f38 0C       		.uleb128 0xc
 3929 1f39 8C120000 		.long	0x128c
 3930 1f3d 0C       		.uleb128 0xc
 3931 1f3e 8C120000 		.long	0x128c
 3932 1f42 00       		.byte	0
 3933 1f43 40       		.uleb128 0x40
 3934 1f44 00000000 		.long	.LASF364
 3935 1f48 1E       		.byte	0x1e
 3936 1f49 3402     		.value	0x234
 3937 1f4b D6150000 		.long	0x15d6
 3938 1f4f 591F0000 		.long	0x1f59
 3939 1f53 0C       		.uleb128 0xc
 3940 1f54 B5120000 		.long	0x12b5
 3941 1f58 00       		.byte	0
 3942 1f59 40       		.uleb128 0x40
 3943 1f5a 00000000 		.long	.LASF365
 3944 1f5e 1E       		.byte	0x1e
 3945 1f5f 1603     		.value	0x316
 3946 1f61 271E0000 		.long	0x1e27
 3947 1f65 741F0000 		.long	0x1f74
 3948 1f69 0C       		.uleb128 0xc
 3949 1f6a 61180000 		.long	0x1861
 3950 1f6e 0C       		.uleb128 0xc
 3951 1f6f 61180000 		.long	0x1861
 3952 1f73 00       		.byte	0
 3953 1f74 40       		.uleb128 0x40
 3954 1f75 00000000 		.long	.LASF366
 3955 1f79 1E       		.byte	0x1e
 3956 1f7a 5E03     		.value	0x35e
GAS LISTING /tmp/ccMbRvxK.s 			page 73


 3957 1f7c 8C120000 		.long	0x128c
 3958 1f80 8F1F0000 		.long	0x1f8f
 3959 1f84 0C       		.uleb128 0xc
 3960 1f85 B5120000 		.long	0x12b5
 3961 1f89 0C       		.uleb128 0xc
 3962 1f8a 13120000 		.long	0x1213
 3963 1f8e 00       		.byte	0
 3964 1f8f 40       		.uleb128 0x40
 3965 1f90 00000000 		.long	.LASF367
 3966 1f94 1E       		.byte	0x1e
 3967 1f95 6903     		.value	0x369
 3968 1f97 13120000 		.long	0x1213
 3969 1f9b AF1F0000 		.long	0x1faf
 3970 1f9f 0C       		.uleb128 0xc
 3971 1fa0 12130000 		.long	0x1312
 3972 1fa4 0C       		.uleb128 0xc
 3973 1fa5 B5120000 		.long	0x12b5
 3974 1fa9 0C       		.uleb128 0xc
 3975 1faa 13120000 		.long	0x1213
 3976 1fae 00       		.byte	0
 3977 1faf 40       		.uleb128 0x40
 3978 1fb0 00000000 		.long	.LASF368
 3979 1fb4 1E       		.byte	0x1e
 3980 1fb5 6103     		.value	0x361
 3981 1fb7 8C120000 		.long	0x128c
 3982 1fbb CF1F0000 		.long	0x1fcf
 3983 1fbf 0C       		.uleb128 0xc
 3984 1fc0 12130000 		.long	0x1312
 3985 1fc4 0C       		.uleb128 0xc
 3986 1fc5 B5120000 		.long	0x12b5
 3987 1fc9 0C       		.uleb128 0xc
 3988 1fca 13120000 		.long	0x1213
 3989 1fce 00       		.byte	0
 3990 1fcf 4F       		.uleb128 0x4f
 3991 1fd0 00000000 		.long	.LASF371
 3992 1fd4 1E       		.byte	0x1e
 3993 1fd5 FC02     		.value	0x2fc
 3994 1fd7 F01F0000 		.long	0x1ff0
 3995 1fdb 0C       		.uleb128 0xc
 3996 1fdc 11120000 		.long	0x1211
 3997 1fe0 0C       		.uleb128 0xc
 3998 1fe1 13120000 		.long	0x1213
 3999 1fe5 0C       		.uleb128 0xc
 4000 1fe6 13120000 		.long	0x1213
 4001 1fea 0C       		.uleb128 0xc
 4002 1feb 621E0000 		.long	0x1e62
 4003 1fef 00       		.byte	0
 4004 1ff0 50       		.uleb128 0x50
 4005 1ff1 00000000 		.long	.LASF369
 4006 1ff5 1E       		.byte	0x1e
 4007 1ff6 2502     		.value	0x225
 4008 1ff8 02200000 		.long	0x2002
 4009 1ffc 0C       		.uleb128 0xc
 4010 1ffd 8C120000 		.long	0x128c
 4011 2001 00       		.byte	0
 4012 2002 42       		.uleb128 0x42
 4013 2003 00000000 		.long	.LASF370
GAS LISTING /tmp/ccMbRvxK.s 			page 74


 4014 2007 1E       		.byte	0x1e
 4015 2008 7601     		.value	0x176
 4016 200a 8C120000 		.long	0x128c
 4017 200e 4F       		.uleb128 0x4f
 4018 200f 00000000 		.long	.LASF372
 4019 2013 1E       		.byte	0x1e
 4020 2014 7801     		.value	0x178
 4021 2016 20200000 		.long	0x2020
 4022 201a 0C       		.uleb128 0xc
 4023 201b 0A120000 		.long	0x120a
 4024 201f 00       		.byte	0
 4025 2020 43       		.uleb128 0x43
 4026 2021 00000000 		.long	.LASF373
 4027 2025 1E       		.byte	0x1e
 4028 2026 A4       		.byte	0xa4
 4029 2027 F2170000 		.long	0x17f2
 4030 202b 3A200000 		.long	0x203a
 4031 202f 0C       		.uleb128 0xc
 4032 2030 B5120000 		.long	0x12b5
 4033 2034 0C       		.uleb128 0xc
 4034 2035 3A200000 		.long	0x203a
 4035 2039 00       		.byte	0
 4036 203a 3F       		.uleb128 0x3f
 4037 203b 08       		.byte	0x8
 4038 203c D6150000 		.long	0x15d6
 4039 2040 43       		.uleb128 0x43
 4040 2041 00000000 		.long	.LASF374
 4041 2045 1E       		.byte	0x1e
 4042 2046 B7       		.byte	0xb7
 4043 2047 61180000 		.long	0x1861
 4044 204b 5F200000 		.long	0x205f
 4045 204f 0C       		.uleb128 0xc
 4046 2050 B5120000 		.long	0x12b5
 4047 2054 0C       		.uleb128 0xc
 4048 2055 3A200000 		.long	0x203a
 4049 2059 0C       		.uleb128 0xc
 4050 205a 8C120000 		.long	0x128c
 4051 205e 00       		.byte	0
 4052 205f 43       		.uleb128 0x43
 4053 2060 00000000 		.long	.LASF375
 4054 2064 1E       		.byte	0x1e
 4055 2065 BB       		.byte	0xbb
 4056 2066 1E120000 		.long	0x121e
 4057 206a 7E200000 		.long	0x207e
 4058 206e 0C       		.uleb128 0xc
 4059 206f B5120000 		.long	0x12b5
 4060 2073 0C       		.uleb128 0xc
 4061 2074 3A200000 		.long	0x203a
 4062 2078 0C       		.uleb128 0xc
 4063 2079 8C120000 		.long	0x128c
 4064 207d 00       		.byte	0
 4065 207e 40       		.uleb128 0x40
 4066 207f 00000000 		.long	.LASF376
 4067 2083 1E       		.byte	0x1e
 4068 2084 CC02     		.value	0x2cc
 4069 2086 8C120000 		.long	0x128c
 4070 208a 94200000 		.long	0x2094
GAS LISTING /tmp/ccMbRvxK.s 			page 75


 4071 208e 0C       		.uleb128 0xc
 4072 208f B5120000 		.long	0x12b5
 4073 2093 00       		.byte	0
 4074 2094 40       		.uleb128 0x40
 4075 2095 00000000 		.long	.LASF377
 4076 2099 1E       		.byte	0x1e
 4077 209a 6C03     		.value	0x36c
 4078 209c 13120000 		.long	0x1213
 4079 20a0 B4200000 		.long	0x20b4
 4080 20a4 0C       		.uleb128 0xc
 4081 20a5 D6150000 		.long	0x15d6
 4082 20a9 0C       		.uleb128 0xc
 4083 20aa 55130000 		.long	0x1355
 4084 20ae 0C       		.uleb128 0xc
 4085 20af 13120000 		.long	0x1213
 4086 20b3 00       		.byte	0
 4087 20b4 40       		.uleb128 0x40
 4088 20b5 00000000 		.long	.LASF378
 4089 20b9 1E       		.byte	0x1e
 4090 20ba 6503     		.value	0x365
 4091 20bc 8C120000 		.long	0x128c
 4092 20c0 CF200000 		.long	0x20cf
 4093 20c4 0C       		.uleb128 0xc
 4094 20c5 D6150000 		.long	0x15d6
 4095 20c9 0C       		.uleb128 0xc
 4096 20ca 18130000 		.long	0x1318
 4097 20ce 00       		.byte	0
 4098 20cf 40       		.uleb128 0x40
 4099 20d0 00000000 		.long	.LASF379
 4100 20d4 1E       		.byte	0x1e
 4101 20d5 1C03     		.value	0x31c
 4102 20d7 571E0000 		.long	0x1e57
 4103 20db EA200000 		.long	0x20ea
 4104 20df 0C       		.uleb128 0xc
 4105 20e0 4B1A0000 		.long	0x1a4b
 4106 20e4 0C       		.uleb128 0xc
 4107 20e5 4B1A0000 		.long	0x1a4b
 4108 20e9 00       		.byte	0
 4109 20ea 43       		.uleb128 0x43
 4110 20eb 00000000 		.long	.LASF380
 4111 20ef 1E       		.byte	0x1e
 4112 20f0 9D       		.byte	0x9d
 4113 20f1 4B1A0000 		.long	0x1a4b
 4114 20f5 FF200000 		.long	0x20ff
 4115 20f9 0C       		.uleb128 0xc
 4116 20fa B5120000 		.long	0x12b5
 4117 20fe 00       		.byte	0
 4118 20ff 43       		.uleb128 0x43
 4119 2100 00000000 		.long	.LASF381
 4120 2104 1E       		.byte	0x1e
 4121 2105 D1       		.byte	0xd1
 4122 2106 4B1A0000 		.long	0x1a4b
 4123 210a 1E210000 		.long	0x211e
 4124 210e 0C       		.uleb128 0xc
 4125 210f B5120000 		.long	0x12b5
 4126 2113 0C       		.uleb128 0xc
 4127 2114 3A200000 		.long	0x203a
GAS LISTING /tmp/ccMbRvxK.s 			page 76


 4128 2118 0C       		.uleb128 0xc
 4129 2119 8C120000 		.long	0x128c
 4130 211d 00       		.byte	0
 4131 211e 43       		.uleb128 0x43
 4132 211f 00000000 		.long	.LASF382
 4133 2123 1E       		.byte	0x1e
 4134 2124 D6       		.byte	0xd6
 4135 2125 721A0000 		.long	0x1a72
 4136 2129 3D210000 		.long	0x213d
 4137 212d 0C       		.uleb128 0xc
 4138 212e B5120000 		.long	0x12b5
 4139 2132 0C       		.uleb128 0xc
 4140 2133 3A200000 		.long	0x203a
 4141 2137 0C       		.uleb128 0xc
 4142 2138 8C120000 		.long	0x128c
 4143 213c 00       		.byte	0
 4144 213d 43       		.uleb128 0x43
 4145 213e 00000000 		.long	.LASF383
 4146 2142 1E       		.byte	0x1e
 4147 2143 AC       		.byte	0xac
 4148 2144 1A180000 		.long	0x181a
 4149 2148 57210000 		.long	0x2157
 4150 214c 0C       		.uleb128 0xc
 4151 214d B5120000 		.long	0x12b5
 4152 2151 0C       		.uleb128 0xc
 4153 2152 3A200000 		.long	0x203a
 4154 2156 00       		.byte	0
 4155 2157 43       		.uleb128 0x43
 4156 2158 00000000 		.long	.LASF384
 4157 215c 1E       		.byte	0x1e
 4158 215d AF       		.byte	0xaf
 4159 215e 241A0000 		.long	0x1a24
 4160 2162 71210000 		.long	0x2171
 4161 2166 0C       		.uleb128 0xc
 4162 2167 B5120000 		.long	0x12b5
 4163 216b 0C       		.uleb128 0xc
 4164 216c 3A200000 		.long	0x203a
 4165 2170 00       		.byte	0
 4166 2171 39       		.uleb128 0x39
 4167 2172 10       		.byte	0x10
 4168 2173 1F       		.byte	0x1f
 4169 2174 16       		.byte	0x16
 4170 2175 00000000 		.long	.LASF385
 4171 2179 96210000 		.long	0x2196
 4172 217d 09       		.uleb128 0x9
 4173 217e 00000000 		.long	.LASF386
 4174 2182 1F       		.byte	0x1f
 4175 2183 17       		.byte	0x17
 4176 2184 AA1D0000 		.long	0x1daa
 4177 2188 00       		.byte	0
 4178 2189 09       		.uleb128 0x9
 4179 218a 00000000 		.long	.LASF387
 4180 218e 1F       		.byte	0x1f
 4181 218f 18       		.byte	0x18
 4182 2190 93120000 		.long	0x1293
 4183 2194 08       		.byte	0x8
 4184 2195 00       		.byte	0
GAS LISTING /tmp/ccMbRvxK.s 			page 77


 4185 2196 14       		.uleb128 0x14
 4186 2197 00000000 		.long	.LASF388
 4187 219b 1F       		.byte	0x1f
 4188 219c 19       		.byte	0x19
 4189 219d 71210000 		.long	0x2171
 4190 21a1 51       		.uleb128 0x51
 4191 21a2 00000000 		.long	.LASF463
 4192 21a6 15       		.byte	0x15
 4193 21a7 96       		.byte	0x96
 4194 21a8 16       		.uleb128 0x16
 4195 21a9 00000000 		.long	.LASF389
 4196 21ad 18       		.byte	0x18
 4197 21ae 15       		.byte	0x15
 4198 21af 9C       		.byte	0x9c
 4199 21b0 D9210000 		.long	0x21d9
 4200 21b4 09       		.uleb128 0x9
 4201 21b5 00000000 		.long	.LASF390
 4202 21b9 15       		.byte	0x15
 4203 21ba 9D       		.byte	0x9d
 4204 21bb D9210000 		.long	0x21d9
 4205 21bf 00       		.byte	0
 4206 21c0 09       		.uleb128 0x9
 4207 21c1 00000000 		.long	.LASF391
 4208 21c5 15       		.byte	0x15
 4209 21c6 9E       		.byte	0x9e
 4210 21c7 DF210000 		.long	0x21df
 4211 21cb 08       		.byte	0x8
 4212 21cc 09       		.uleb128 0x9
 4213 21cd 00000000 		.long	.LASF392
 4214 21d1 15       		.byte	0x15
 4215 21d2 A2       		.byte	0xa2
 4216 21d3 8C120000 		.long	0x128c
 4217 21d7 10       		.byte	0x10
 4218 21d8 00       		.byte	0
 4219 21d9 3F       		.uleb128 0x3f
 4220 21da 08       		.byte	0x8
 4221 21db A8210000 		.long	0x21a8
 4222 21df 3F       		.uleb128 0x3f
 4223 21e0 08       		.byte	0x8
 4224 21e1 3E100000 		.long	0x103e
 4225 21e5 3C       		.uleb128 0x3c
 4226 21e6 85120000 		.long	0x1285
 4227 21ea F5210000 		.long	0x21f5
 4228 21ee 3D       		.uleb128 0x3d
 4229 21ef C6110000 		.long	0x11c6
 4230 21f3 00       		.byte	0
 4231 21f4 00       		.byte	0
 4232 21f5 3F       		.uleb128 0x3f
 4233 21f6 08       		.byte	0x8
 4234 21f7 A1210000 		.long	0x21a1
 4235 21fb 3C       		.uleb128 0x3c
 4236 21fc 85120000 		.long	0x1285
 4237 2200 0B220000 		.long	0x220b
 4238 2204 3D       		.uleb128 0x3d
 4239 2205 C6110000 		.long	0x11c6
 4240 2209 13       		.byte	0x13
 4241 220a 00       		.byte	0
GAS LISTING /tmp/ccMbRvxK.s 			page 78


 4242 220b 14       		.uleb128 0x14
 4243 220c 00000000 		.long	.LASF393
 4244 2210 14       		.byte	0x14
 4245 2211 6E       		.byte	0x6e
 4246 2212 96210000 		.long	0x2196
 4247 2216 4F       		.uleb128 0x4f
 4248 2217 00000000 		.long	.LASF394
 4249 221b 14       		.byte	0x14
 4250 221c 3A03     		.value	0x33a
 4251 221e 28220000 		.long	0x2228
 4252 2222 0C       		.uleb128 0xc
 4253 2223 28220000 		.long	0x2228
 4254 2227 00       		.byte	0
 4255 2228 3F       		.uleb128 0x3f
 4256 2229 08       		.byte	0x8
 4257 222a 33100000 		.long	0x1033
 4258 222e 43       		.uleb128 0x43
 4259 222f 00000000 		.long	.LASF395
 4260 2233 14       		.byte	0x14
 4261 2234 ED       		.byte	0xed
 4262 2235 8C120000 		.long	0x128c
 4263 2239 43220000 		.long	0x2243
 4264 223d 0C       		.uleb128 0xc
 4265 223e 28220000 		.long	0x2228
 4266 2242 00       		.byte	0
 4267 2243 40       		.uleb128 0x40
 4268 2244 00000000 		.long	.LASF396
 4269 2248 14       		.byte	0x14
 4270 2249 3C03     		.value	0x33c
 4271 224b 8C120000 		.long	0x128c
 4272 224f 59220000 		.long	0x2259
 4273 2253 0C       		.uleb128 0xc
 4274 2254 28220000 		.long	0x2228
 4275 2258 00       		.byte	0
 4276 2259 40       		.uleb128 0x40
 4277 225a 00000000 		.long	.LASF397
 4278 225e 14       		.byte	0x14
 4279 225f 3E03     		.value	0x33e
 4280 2261 8C120000 		.long	0x128c
 4281 2265 6F220000 		.long	0x226f
 4282 2269 0C       		.uleb128 0xc
 4283 226a 28220000 		.long	0x2228
 4284 226e 00       		.byte	0
 4285 226f 43       		.uleb128 0x43
 4286 2270 00000000 		.long	.LASF398
 4287 2274 14       		.byte	0x14
 4288 2275 F2       		.byte	0xf2
 4289 2276 8C120000 		.long	0x128c
 4290 227a 84220000 		.long	0x2284
 4291 227e 0C       		.uleb128 0xc
 4292 227f 28220000 		.long	0x2228
 4293 2283 00       		.byte	0
 4294 2284 40       		.uleb128 0x40
 4295 2285 00000000 		.long	.LASF399
 4296 2289 14       		.byte	0x14
 4297 228a 1302     		.value	0x213
 4298 228c 8C120000 		.long	0x128c
GAS LISTING /tmp/ccMbRvxK.s 			page 79


 4299 2290 9A220000 		.long	0x229a
 4300 2294 0C       		.uleb128 0xc
 4301 2295 28220000 		.long	0x2228
 4302 2299 00       		.byte	0
 4303 229a 40       		.uleb128 0x40
 4304 229b 00000000 		.long	.LASF400
 4305 229f 14       		.byte	0x14
 4306 22a0 1E03     		.value	0x31e
 4307 22a2 8C120000 		.long	0x128c
 4308 22a6 B5220000 		.long	0x22b5
 4309 22aa 0C       		.uleb128 0xc
 4310 22ab 28220000 		.long	0x2228
 4311 22af 0C       		.uleb128 0xc
 4312 22b0 B5220000 		.long	0x22b5
 4313 22b4 00       		.byte	0
 4314 22b5 3F       		.uleb128 0x3f
 4315 22b6 08       		.byte	0x8
 4316 22b7 0B220000 		.long	0x220b
 4317 22bb 40       		.uleb128 0x40
 4318 22bc 00000000 		.long	.LASF401
 4319 22c0 14       		.byte	0x14
 4320 22c1 6E02     		.value	0x26e
 4321 22c3 D6150000 		.long	0x15d6
 4322 22c7 DB220000 		.long	0x22db
 4323 22cb 0C       		.uleb128 0xc
 4324 22cc D6150000 		.long	0x15d6
 4325 22d0 0C       		.uleb128 0xc
 4326 22d1 8C120000 		.long	0x128c
 4327 22d5 0C       		.uleb128 0xc
 4328 22d6 28220000 		.long	0x2228
 4329 22da 00       		.byte	0
 4330 22db 40       		.uleb128 0x40
 4331 22dc 00000000 		.long	.LASF402
 4332 22e0 14       		.byte	0x14
 4333 22e1 1001     		.value	0x110
 4334 22e3 28220000 		.long	0x2228
 4335 22e7 F6220000 		.long	0x22f6
 4336 22eb 0C       		.uleb128 0xc
 4337 22ec B5120000 		.long	0x12b5
 4338 22f0 0C       		.uleb128 0xc
 4339 22f1 B5120000 		.long	0x12b5
 4340 22f5 00       		.byte	0
 4341 22f6 40       		.uleb128 0x40
 4342 22f7 00000000 		.long	.LASF403
 4343 22fb 14       		.byte	0x14
 4344 22fc C502     		.value	0x2c5
 4345 22fe 13120000 		.long	0x1213
 4346 2302 1B230000 		.long	0x231b
 4347 2306 0C       		.uleb128 0xc
 4348 2307 11120000 		.long	0x1211
 4349 230b 0C       		.uleb128 0xc
 4350 230c 13120000 		.long	0x1213
 4351 2310 0C       		.uleb128 0xc
 4352 2311 13120000 		.long	0x1213
 4353 2315 0C       		.uleb128 0xc
 4354 2316 28220000 		.long	0x2228
 4355 231a 00       		.byte	0
GAS LISTING /tmp/ccMbRvxK.s 			page 80


 4356 231b 40       		.uleb128 0x40
 4357 231c 00000000 		.long	.LASF404
 4358 2320 14       		.byte	0x14
 4359 2321 1601     		.value	0x116
 4360 2323 28220000 		.long	0x2228
 4361 2327 3B230000 		.long	0x233b
 4362 232b 0C       		.uleb128 0xc
 4363 232c B5120000 		.long	0x12b5
 4364 2330 0C       		.uleb128 0xc
 4365 2331 B5120000 		.long	0x12b5
 4366 2335 0C       		.uleb128 0xc
 4367 2336 28220000 		.long	0x2228
 4368 233a 00       		.byte	0
 4369 233b 40       		.uleb128 0x40
 4370 233c 00000000 		.long	.LASF405
 4371 2340 14       		.byte	0x14
 4372 2341 ED02     		.value	0x2ed
 4373 2343 8C120000 		.long	0x128c
 4374 2347 5B230000 		.long	0x235b
 4375 234b 0C       		.uleb128 0xc
 4376 234c 28220000 		.long	0x2228
 4377 2350 0C       		.uleb128 0xc
 4378 2351 61180000 		.long	0x1861
 4379 2355 0C       		.uleb128 0xc
 4380 2356 8C120000 		.long	0x128c
 4381 235a 00       		.byte	0
 4382 235b 40       		.uleb128 0x40
 4383 235c 00000000 		.long	.LASF406
 4384 2360 14       		.byte	0x14
 4385 2361 2303     		.value	0x323
 4386 2363 8C120000 		.long	0x128c
 4387 2367 76230000 		.long	0x2376
 4388 236b 0C       		.uleb128 0xc
 4389 236c 28220000 		.long	0x2228
 4390 2370 0C       		.uleb128 0xc
 4391 2371 76230000 		.long	0x2376
 4392 2375 00       		.byte	0
 4393 2376 3F       		.uleb128 0x3f
 4394 2377 08       		.byte	0x8
 4395 2378 7C230000 		.long	0x237c
 4396 237c 13       		.uleb128 0x13
 4397 237d 0B220000 		.long	0x220b
 4398 2381 40       		.uleb128 0x40
 4399 2382 00000000 		.long	.LASF407
 4400 2386 14       		.byte	0x14
 4401 2387 F202     		.value	0x2f2
 4402 2389 61180000 		.long	0x1861
 4403 238d 97230000 		.long	0x2397
 4404 2391 0C       		.uleb128 0xc
 4405 2392 28220000 		.long	0x2228
 4406 2396 00       		.byte	0
 4407 2397 40       		.uleb128 0x40
 4408 2398 00000000 		.long	.LASF408
 4409 239c 14       		.byte	0x14
 4410 239d 1402     		.value	0x214
 4411 239f 8C120000 		.long	0x128c
 4412 23a3 AD230000 		.long	0x23ad
GAS LISTING /tmp/ccMbRvxK.s 			page 81


 4413 23a7 0C       		.uleb128 0xc
 4414 23a8 28220000 		.long	0x2228
 4415 23ac 00       		.byte	0
 4416 23ad 42       		.uleb128 0x42
 4417 23ae 00000000 		.long	.LASF409
 4418 23b2 14       		.byte	0x14
 4419 23b3 1A02     		.value	0x21a
 4420 23b5 8C120000 		.long	0x128c
 4421 23b9 40       		.uleb128 0x40
 4422 23ba 00000000 		.long	.LASF410
 4423 23be 14       		.byte	0x14
 4424 23bf 7E02     		.value	0x27e
 4425 23c1 D6150000 		.long	0x15d6
 4426 23c5 CF230000 		.long	0x23cf
 4427 23c9 0C       		.uleb128 0xc
 4428 23ca D6150000 		.long	0x15d6
 4429 23ce 00       		.byte	0
 4430 23cf 4F       		.uleb128 0x4f
 4431 23d0 00000000 		.long	.LASF411
 4432 23d4 14       		.byte	0x14
 4433 23d5 4E03     		.value	0x34e
 4434 23d7 E1230000 		.long	0x23e1
 4435 23db 0C       		.uleb128 0xc
 4436 23dc B5120000 		.long	0x12b5
 4437 23e0 00       		.byte	0
 4438 23e1 43       		.uleb128 0x43
 4439 23e2 00000000 		.long	.LASF412
 4440 23e6 14       		.byte	0x14
 4441 23e7 B2       		.byte	0xb2
 4442 23e8 8C120000 		.long	0x128c
 4443 23ec F6230000 		.long	0x23f6
 4444 23f0 0C       		.uleb128 0xc
 4445 23f1 B5120000 		.long	0x12b5
 4446 23f5 00       		.byte	0
 4447 23f6 43       		.uleb128 0x43
 4448 23f7 00000000 		.long	.LASF413
 4449 23fb 14       		.byte	0x14
 4450 23fc B4       		.byte	0xb4
 4451 23fd 8C120000 		.long	0x128c
 4452 2401 10240000 		.long	0x2410
 4453 2405 0C       		.uleb128 0xc
 4454 2406 B5120000 		.long	0x12b5
 4455 240a 0C       		.uleb128 0xc
 4456 240b B5120000 		.long	0x12b5
 4457 240f 00       		.byte	0
 4458 2410 4F       		.uleb128 0x4f
 4459 2411 00000000 		.long	.LASF414
 4460 2415 14       		.byte	0x14
 4461 2416 F702     		.value	0x2f7
 4462 2418 22240000 		.long	0x2422
 4463 241c 0C       		.uleb128 0xc
 4464 241d 28220000 		.long	0x2228
 4465 2421 00       		.byte	0
 4466 2422 4F       		.uleb128 0x4f
 4467 2423 00000000 		.long	.LASF415
 4468 2427 14       		.byte	0x14
 4469 2428 4C01     		.value	0x14c
GAS LISTING /tmp/ccMbRvxK.s 			page 82


 4470 242a 39240000 		.long	0x2439
 4471 242e 0C       		.uleb128 0xc
 4472 242f 28220000 		.long	0x2228
 4473 2433 0C       		.uleb128 0xc
 4474 2434 D6150000 		.long	0x15d6
 4475 2438 00       		.byte	0
 4476 2439 40       		.uleb128 0x40
 4477 243a 00000000 		.long	.LASF416
 4478 243e 14       		.byte	0x14
 4479 243f 5001     		.value	0x150
 4480 2441 8C120000 		.long	0x128c
 4481 2445 5E240000 		.long	0x245e
 4482 2449 0C       		.uleb128 0xc
 4483 244a 28220000 		.long	0x2228
 4484 244e 0C       		.uleb128 0xc
 4485 244f D6150000 		.long	0x15d6
 4486 2453 0C       		.uleb128 0xc
 4487 2454 8C120000 		.long	0x128c
 4488 2458 0C       		.uleb128 0xc
 4489 2459 13120000 		.long	0x1213
 4490 245d 00       		.byte	0
 4491 245e 49       		.uleb128 0x49
 4492 245f 00000000 		.long	.LASF417
 4493 2463 14       		.byte	0x14
 4494 2464 C3       		.byte	0xc3
 4495 2465 28220000 		.long	0x2228
 4496 2469 43       		.uleb128 0x43
 4497 246a 00000000 		.long	.LASF418
 4498 246e 14       		.byte	0x14
 4499 246f D1       		.byte	0xd1
 4500 2470 D6150000 		.long	0x15d6
 4501 2474 7E240000 		.long	0x247e
 4502 2478 0C       		.uleb128 0xc
 4503 2479 D6150000 		.long	0x15d6
 4504 247d 00       		.byte	0
 4505 247e 40       		.uleb128 0x40
 4506 247f 00000000 		.long	.LASF419
 4507 2483 14       		.byte	0x14
 4508 2484 BE02     		.value	0x2be
 4509 2486 8C120000 		.long	0x128c
 4510 248a 99240000 		.long	0x2499
 4511 248e 0C       		.uleb128 0xc
 4512 248f 8C120000 		.long	0x128c
 4513 2493 0C       		.uleb128 0xc
 4514 2494 28220000 		.long	0x2228
 4515 2498 00       		.byte	0
 4516 2499 3F       		.uleb128 0x3f
 4517 249a 08       		.byte	0x8
 4518 249b AD0A0000 		.long	0xaad
 4519 249f 14       		.uleb128 0x14
 4520 24a0 00000000 		.long	.LASF420
 4521 24a4 20       		.byte	0x20
 4522 24a5 34       		.byte	0x34
 4523 24a6 1E120000 		.long	0x121e
 4524 24aa 14       		.uleb128 0x14
 4525 24ab 00000000 		.long	.LASF421
 4526 24af 20       		.byte	0x20
GAS LISTING /tmp/ccMbRvxK.s 			page 83


 4527 24b0 BA       		.byte	0xba
 4528 24b1 B5240000 		.long	0x24b5
 4529 24b5 3F       		.uleb128 0x3f
 4530 24b6 08       		.byte	0x8
 4531 24b7 BB240000 		.long	0x24bb
 4532 24bb 13       		.uleb128 0x13
 4533 24bc 9F1D0000 		.long	0x1d9f
 4534 24c0 43       		.uleb128 0x43
 4535 24c1 00000000 		.long	.LASF422
 4536 24c5 20       		.byte	0x20
 4537 24c6 AF       		.byte	0xaf
 4538 24c7 8C120000 		.long	0x128c
 4539 24cb DA240000 		.long	0x24da
 4540 24cf 0C       		.uleb128 0xc
 4541 24d0 25120000 		.long	0x1225
 4542 24d4 0C       		.uleb128 0xc
 4543 24d5 9F240000 		.long	0x249f
 4544 24d9 00       		.byte	0
 4545 24da 43       		.uleb128 0x43
 4546 24db 00000000 		.long	.LASF423
 4547 24df 20       		.byte	0x20
 4548 24e0 DD       		.byte	0xdd
 4549 24e1 25120000 		.long	0x1225
 4550 24e5 F4240000 		.long	0x24f4
 4551 24e9 0C       		.uleb128 0xc
 4552 24ea 25120000 		.long	0x1225
 4553 24ee 0C       		.uleb128 0xc
 4554 24ef AA240000 		.long	0x24aa
 4555 24f3 00       		.byte	0
 4556 24f4 43       		.uleb128 0x43
 4557 24f5 00000000 		.long	.LASF424
 4558 24f9 20       		.byte	0x20
 4559 24fa DA       		.byte	0xda
 4560 24fb AA240000 		.long	0x24aa
 4561 24ff 09250000 		.long	0x2509
 4562 2503 0C       		.uleb128 0xc
 4563 2504 B5120000 		.long	0x12b5
 4564 2508 00       		.byte	0
 4565 2509 43       		.uleb128 0x43
 4566 250a 00000000 		.long	.LASF425
 4567 250e 20       		.byte	0x20
 4568 250f AB       		.byte	0xab
 4569 2510 9F240000 		.long	0x249f
 4570 2514 1E250000 		.long	0x251e
 4571 2518 0C       		.uleb128 0xc
 4572 2519 B5120000 		.long	0x12b5
 4573 251d 00       		.byte	0
 4574 251e 13       		.uleb128 0x13
 4575 251f B71A0000 		.long	0x1ab7
 4576 2523 13       		.uleb128 0x13
 4577 2524 61180000 		.long	0x1861
 4578 2528 52       		.uleb128 0x52
 4579 2529 00000000 		.long	.LASF426
 4580 252d 01       		.byte	0x1
 4581 252e 03       		.byte	0x3
 4582 252f 8C120000 		.long	0x128c
 4583 2533 00000000 		.quad	.LFB1383
GAS LISTING /tmp/ccMbRvxK.s 			page 84


 4583      00000000 
 4584 253b 5D010000 		.quad	.LFE1383-.LFB1383
 4584      00000000 
 4585 2543 01       		.uleb128 0x1
 4586 2544 9C       		.byte	0x9c
 4587 2545 73250000 		.long	0x2573
 4588 2549 53       		.uleb128 0x53
 4589 254a 00000000 		.long	.LASF427
 4590 254e 01       		.byte	0x1
 4591 254f 03       		.byte	0x3
 4592 2550 8C120000 		.long	0x128c
 4593 2554 02       		.uleb128 0x2
 4594 2555 91       		.byte	0x91
 4595 2556 5C       		.sleb128 -36
 4596 2557 53       		.uleb128 0x53
 4597 2558 00000000 		.long	.LASF428
 4598 255c 01       		.byte	0x1
 4599 255d 03       		.byte	0x3
 4600 255e 3A200000 		.long	0x203a
 4601 2562 02       		.uleb128 0x2
 4602 2563 91       		.byte	0x91
 4603 2564 50       		.sleb128 -48
 4604 2565 54       		.uleb128 0x54
 4605 2566 697000   		.string	"ip"
 4606 2569 01       		.byte	0x1
 4607 256a 0A       		.byte	0xa
 4608 256b 73250000 		.long	0x2573
 4609 256f 02       		.uleb128 0x2
 4610 2570 91       		.byte	0x91
 4611 2571 60       		.sleb128 -32
 4612 2572 00       		.byte	0
 4613 2573 3F       		.uleb128 0x3f
 4614 2574 08       		.byte	0x8
 4615 2575 8C120000 		.long	0x128c
 4616 2579 55       		.uleb128 0x55
 4617 257a 00000000 		.long	.LASF464
 4618 257e 00000000 		.quad	.LFB1576
 4618      00000000 
 4619 2586 3E000000 		.quad	.LFE1576-.LFB1576
 4619      00000000 
 4620 258e 01       		.uleb128 0x1
 4621 258f 9C       		.byte	0x9c
 4622 2590 B1250000 		.long	0x25b1
 4623 2594 53       		.uleb128 0x53
 4624 2595 00000000 		.long	.LASF430
 4625 2599 01       		.byte	0x1
 4626 259a 11       		.byte	0x11
 4627 259b 8C120000 		.long	0x128c
 4628 259f 02       		.uleb128 0x2
 4629 25a0 91       		.byte	0x91
 4630 25a1 6C       		.sleb128 -20
 4631 25a2 53       		.uleb128 0x53
 4632 25a3 00000000 		.long	.LASF431
 4633 25a7 01       		.byte	0x1
 4634 25a8 11       		.byte	0x11
 4635 25a9 8C120000 		.long	0x128c
 4636 25ad 02       		.uleb128 0x2
GAS LISTING /tmp/ccMbRvxK.s 			page 85


 4637 25ae 91       		.byte	0x91
 4638 25af 68       		.sleb128 -24
 4639 25b0 00       		.byte	0
 4640 25b1 56       		.uleb128 0x56
 4641 25b2 00000000 		.long	.LASF465
 4642 25b6 00000000 		.quad	.LFB1577
 4642      00000000 
 4643 25be 15000000 		.quad	.LFE1577-.LFB1577
 4643      00000000 
 4644 25c6 01       		.uleb128 0x1
 4645 25c7 9C       		.byte	0x9c
 4646 25c8 57       		.uleb128 0x57
 4647 25c9 00000000 		.long	.LASF432
 4648 25cd 11120000 		.long	0x1211
 4649 25d1 58       		.uleb128 0x58
 4650 25d2 5A0D0000 		.long	0xd5a
 4651 25d6 09       		.uleb128 0x9
 4652 25d7 03       		.byte	0x3
 4653 25d8 00000000 		.quad	_ZStL19piecewise_construct
 4653      00000000 
 4654 25e0 58       		.uleb128 0x58
 4655 25e1 860D0000 		.long	0xd86
 4656 25e5 09       		.uleb128 0x9
 4657 25e6 03       		.byte	0x3
 4658 25e7 00000000 		.quad	_ZStL8__ioinit
 4658      00000000 
 4659 25ef 59       		.uleb128 0x59
 4660 25f0 1E040000 		.long	0x41e
 4661 25f4 00000000 		.long	.LASF433
 4662 25f8 00       		.byte	0
 4663 25f9 59       		.uleb128 0x59
 4664 25fa 76040000 		.long	0x476
 4665 25fe 00000000 		.long	.LASF434
 4666 2602 01       		.byte	0x1
 4667 2603 5A       		.uleb128 0x5a
 4668 2604 E30D0000 		.long	0xde3
 4669 2608 00000000 		.long	.LASF435
 4670 260c 80808080 		.sleb128 -2147483648
 4670      78
 4671 2611 5B       		.uleb128 0x5b
 4672 2612 EE0D0000 		.long	0xdee
 4673 2616 00000000 		.long	.LASF436
 4674 261a FFFFFF7F 		.long	0x7fffffff
 4675 261e 59       		.uleb128 0x59
 4676 261f 950E0000 		.long	0xe95
 4677 2623 00000000 		.long	.LASF437
 4678 2627 26       		.byte	0x26
 4679 2628 5C       		.uleb128 0x5c
 4680 2629 D70E0000 		.long	0xed7
 4681 262d 00000000 		.long	.LASF438
 4682 2631 3401     		.value	0x134
 4683 2633 5C       		.uleb128 0x5c
 4684 2634 190F0000 		.long	0xf19
 4685 2638 00000000 		.long	.LASF439
 4686 263c 4413     		.value	0x1344
 4687 263e 59       		.uleb128 0x59
 4688 263f 5B0F0000 		.long	0xf5b
GAS LISTING /tmp/ccMbRvxK.s 			page 86


 4689 2643 00000000 		.long	.LASF440
 4690 2647 40       		.byte	0x40
 4691 2648 59       		.uleb128 0x59
 4692 2649 870F0000 		.long	0xf87
 4693 264d 00000000 		.long	.LASF441
 4694 2651 7F       		.byte	0x7f
 4695 2652 5A       		.uleb128 0x5a
 4696 2653 BE0F0000 		.long	0xfbe
 4697 2657 00000000 		.long	.LASF442
 4698 265b 80807E   		.sleb128 -32768
 4699 265e 5C       		.uleb128 0x5c
 4700 265f C90F0000 		.long	0xfc9
 4701 2663 00000000 		.long	.LASF443
 4702 2667 FF7F     		.value	0x7fff
 4703 2669 5A       		.uleb128 0x5a
 4704 266a FC0F0000 		.long	0xffc
 4705 266e 00000000 		.long	.LASF444
 4706 2672 80808080 		.sleb128 -9223372036854775808
 4706      80808080 
 4706      807F
 4707 267c 5D       		.uleb128 0x5d
 4708 267d 07100000 		.long	0x1007
 4709 2681 00000000 		.long	.LASF445
 4710 2685 FFFFFFFF 		.quad	0x7fffffffffffffff
 4710      FFFFFF7F 
 4711 268d 00       		.byte	0
 4712              		.section	.debug_abbrev,"",@progbits
 4713              	.Ldebug_abbrev0:
 4714 0000 01       		.uleb128 0x1
 4715 0001 11       		.uleb128 0x11
 4716 0002 01       		.byte	0x1
 4717 0003 25       		.uleb128 0x25
 4718 0004 0E       		.uleb128 0xe
 4719 0005 13       		.uleb128 0x13
 4720 0006 0B       		.uleb128 0xb
 4721 0007 03       		.uleb128 0x3
 4722 0008 0E       		.uleb128 0xe
 4723 0009 1B       		.uleb128 0x1b
 4724 000a 0E       		.uleb128 0xe
 4725 000b 11       		.uleb128 0x11
 4726 000c 01       		.uleb128 0x1
 4727 000d 12       		.uleb128 0x12
 4728 000e 07       		.uleb128 0x7
 4729 000f 10       		.uleb128 0x10
 4730 0010 17       		.uleb128 0x17
 4731 0011 00       		.byte	0
 4732 0012 00       		.byte	0
 4733 0013 02       		.uleb128 0x2
 4734 0014 39       		.uleb128 0x39
 4735 0015 01       		.byte	0x1
 4736 0016 03       		.uleb128 0x3
 4737 0017 08       		.uleb128 0x8
 4738 0018 3A       		.uleb128 0x3a
 4739 0019 0B       		.uleb128 0xb
 4740 001a 3B       		.uleb128 0x3b
 4741 001b 0B       		.uleb128 0xb
 4742 001c 01       		.uleb128 0x1
GAS LISTING /tmp/ccMbRvxK.s 			page 87


 4743 001d 13       		.uleb128 0x13
 4744 001e 00       		.byte	0
 4745 001f 00       		.byte	0
 4746 0020 03       		.uleb128 0x3
 4747 0021 39       		.uleb128 0x39
 4748 0022 00       		.byte	0
 4749 0023 03       		.uleb128 0x3
 4750 0024 0E       		.uleb128 0xe
 4751 0025 3A       		.uleb128 0x3a
 4752 0026 0B       		.uleb128 0xb
 4753 0027 3B       		.uleb128 0x3b
 4754 0028 0B       		.uleb128 0xb
 4755 0029 00       		.byte	0
 4756 002a 00       		.byte	0
 4757 002b 04       		.uleb128 0x4
 4758 002c 3A       		.uleb128 0x3a
 4759 002d 00       		.byte	0
 4760 002e 3A       		.uleb128 0x3a
 4761 002f 0B       		.uleb128 0xb
 4762 0030 3B       		.uleb128 0x3b
 4763 0031 0B       		.uleb128 0xb
 4764 0032 18       		.uleb128 0x18
 4765 0033 13       		.uleb128 0x13
 4766 0034 00       		.byte	0
 4767 0035 00       		.byte	0
 4768 0036 05       		.uleb128 0x5
 4769 0037 08       		.uleb128 0x8
 4770 0038 00       		.byte	0
 4771 0039 3A       		.uleb128 0x3a
 4772 003a 0B       		.uleb128 0xb
 4773 003b 3B       		.uleb128 0x3b
 4774 003c 0B       		.uleb128 0xb
 4775 003d 18       		.uleb128 0x18
 4776 003e 13       		.uleb128 0x13
 4777 003f 00       		.byte	0
 4778 0040 00       		.byte	0
 4779 0041 06       		.uleb128 0x6
 4780 0042 08       		.uleb128 0x8
 4781 0043 00       		.byte	0
 4782 0044 3A       		.uleb128 0x3a
 4783 0045 0B       		.uleb128 0xb
 4784 0046 3B       		.uleb128 0x3b
 4785 0047 05       		.uleb128 0x5
 4786 0048 18       		.uleb128 0x18
 4787 0049 13       		.uleb128 0x13
 4788 004a 00       		.byte	0
 4789 004b 00       		.byte	0
 4790 004c 07       		.uleb128 0x7
 4791 004d 39       		.uleb128 0x39
 4792 004e 01       		.byte	0x1
 4793 004f 03       		.uleb128 0x3
 4794 0050 0E       		.uleb128 0xe
 4795 0051 3A       		.uleb128 0x3a
 4796 0052 0B       		.uleb128 0xb
 4797 0053 3B       		.uleb128 0x3b
 4798 0054 0B       		.uleb128 0xb
 4799 0055 01       		.uleb128 0x1
GAS LISTING /tmp/ccMbRvxK.s 			page 88


 4800 0056 13       		.uleb128 0x13
 4801 0057 00       		.byte	0
 4802 0058 00       		.byte	0
 4803 0059 08       		.uleb128 0x8
 4804 005a 02       		.uleb128 0x2
 4805 005b 01       		.byte	0x1
 4806 005c 03       		.uleb128 0x3
 4807 005d 0E       		.uleb128 0xe
 4808 005e 0B       		.uleb128 0xb
 4809 005f 0B       		.uleb128 0xb
 4810 0060 3A       		.uleb128 0x3a
 4811 0061 0B       		.uleb128 0xb
 4812 0062 3B       		.uleb128 0x3b
 4813 0063 0B       		.uleb128 0xb
 4814 0064 01       		.uleb128 0x1
 4815 0065 13       		.uleb128 0x13
 4816 0066 00       		.byte	0
 4817 0067 00       		.byte	0
 4818 0068 09       		.uleb128 0x9
 4819 0069 0D       		.uleb128 0xd
 4820 006a 00       		.byte	0
 4821 006b 03       		.uleb128 0x3
 4822 006c 0E       		.uleb128 0xe
 4823 006d 3A       		.uleb128 0x3a
 4824 006e 0B       		.uleb128 0xb
 4825 006f 3B       		.uleb128 0x3b
 4826 0070 0B       		.uleb128 0xb
 4827 0071 49       		.uleb128 0x49
 4828 0072 13       		.uleb128 0x13
 4829 0073 38       		.uleb128 0x38
 4830 0074 0B       		.uleb128 0xb
 4831 0075 00       		.byte	0
 4832 0076 00       		.byte	0
 4833 0077 0A       		.uleb128 0xa
 4834 0078 2E       		.uleb128 0x2e
 4835 0079 01       		.byte	0x1
 4836 007a 3F       		.uleb128 0x3f
 4837 007b 19       		.uleb128 0x19
 4838 007c 03       		.uleb128 0x3
 4839 007d 0E       		.uleb128 0xe
 4840 007e 3A       		.uleb128 0x3a
 4841 007f 0B       		.uleb128 0xb
 4842 0080 3B       		.uleb128 0x3b
 4843 0081 0B       		.uleb128 0xb
 4844 0082 6E       		.uleb128 0x6e
 4845 0083 0E       		.uleb128 0xe
 4846 0084 3C       		.uleb128 0x3c
 4847 0085 19       		.uleb128 0x19
 4848 0086 63       		.uleb128 0x63
 4849 0087 19       		.uleb128 0x19
 4850 0088 64       		.uleb128 0x64
 4851 0089 13       		.uleb128 0x13
 4852 008a 01       		.uleb128 0x1
 4853 008b 13       		.uleb128 0x13
 4854 008c 00       		.byte	0
 4855 008d 00       		.byte	0
 4856 008e 0B       		.uleb128 0xb
GAS LISTING /tmp/ccMbRvxK.s 			page 89


 4857 008f 05       		.uleb128 0x5
 4858 0090 00       		.byte	0
 4859 0091 49       		.uleb128 0x49
 4860 0092 13       		.uleb128 0x13
 4861 0093 34       		.uleb128 0x34
 4862 0094 19       		.uleb128 0x19
 4863 0095 00       		.byte	0
 4864 0096 00       		.byte	0
 4865 0097 0C       		.uleb128 0xc
 4866 0098 05       		.uleb128 0x5
 4867 0099 00       		.byte	0
 4868 009a 49       		.uleb128 0x49
 4869 009b 13       		.uleb128 0x13
 4870 009c 00       		.byte	0
 4871 009d 00       		.byte	0
 4872 009e 0D       		.uleb128 0xd
 4873 009f 2E       		.uleb128 0x2e
 4874 00a0 01       		.byte	0x1
 4875 00a1 3F       		.uleb128 0x3f
 4876 00a2 19       		.uleb128 0x19
 4877 00a3 03       		.uleb128 0x3
 4878 00a4 0E       		.uleb128 0xe
 4879 00a5 3A       		.uleb128 0x3a
 4880 00a6 0B       		.uleb128 0xb
 4881 00a7 3B       		.uleb128 0x3b
 4882 00a8 0B       		.uleb128 0xb
 4883 00a9 6E       		.uleb128 0x6e
 4884 00aa 0E       		.uleb128 0xe
 4885 00ab 3C       		.uleb128 0x3c
 4886 00ac 19       		.uleb128 0x19
 4887 00ad 64       		.uleb128 0x64
 4888 00ae 13       		.uleb128 0x13
 4889 00af 01       		.uleb128 0x1
 4890 00b0 13       		.uleb128 0x13
 4891 00b1 00       		.byte	0
 4892 00b2 00       		.byte	0
 4893 00b3 0E       		.uleb128 0xe
 4894 00b4 2E       		.uleb128 0x2e
 4895 00b5 01       		.byte	0x1
 4896 00b6 3F       		.uleb128 0x3f
 4897 00b7 19       		.uleb128 0x19
 4898 00b8 03       		.uleb128 0x3
 4899 00b9 0E       		.uleb128 0xe
 4900 00ba 3A       		.uleb128 0x3a
 4901 00bb 0B       		.uleb128 0xb
 4902 00bc 3B       		.uleb128 0x3b
 4903 00bd 0B       		.uleb128 0xb
 4904 00be 6E       		.uleb128 0x6e
 4905 00bf 0E       		.uleb128 0xe
 4906 00c0 49       		.uleb128 0x49
 4907 00c1 13       		.uleb128 0x13
 4908 00c2 3C       		.uleb128 0x3c
 4909 00c3 19       		.uleb128 0x19
 4910 00c4 64       		.uleb128 0x64
 4911 00c5 13       		.uleb128 0x13
 4912 00c6 01       		.uleb128 0x1
 4913 00c7 13       		.uleb128 0x13
GAS LISTING /tmp/ccMbRvxK.s 			page 90


 4914 00c8 00       		.byte	0
 4915 00c9 00       		.byte	0
 4916 00ca 0F       		.uleb128 0xf
 4917 00cb 2E       		.uleb128 0x2e
 4918 00cc 01       		.byte	0x1
 4919 00cd 3F       		.uleb128 0x3f
 4920 00ce 19       		.uleb128 0x19
 4921 00cf 03       		.uleb128 0x3
 4922 00d0 0E       		.uleb128 0xe
 4923 00d1 3A       		.uleb128 0x3a
 4924 00d2 0B       		.uleb128 0xb
 4925 00d3 3B       		.uleb128 0x3b
 4926 00d4 0B       		.uleb128 0xb
 4927 00d5 6E       		.uleb128 0x6e
 4928 00d6 0E       		.uleb128 0xe
 4929 00d7 32       		.uleb128 0x32
 4930 00d8 0B       		.uleb128 0xb
 4931 00d9 3C       		.uleb128 0x3c
 4932 00da 19       		.uleb128 0x19
 4933 00db 64       		.uleb128 0x64
 4934 00dc 13       		.uleb128 0x13
 4935 00dd 01       		.uleb128 0x1
 4936 00de 13       		.uleb128 0x13
 4937 00df 00       		.byte	0
 4938 00e0 00       		.byte	0
 4939 00e1 10       		.uleb128 0x10
 4940 00e2 2E       		.uleb128 0x2e
 4941 00e3 01       		.byte	0x1
 4942 00e4 3F       		.uleb128 0x3f
 4943 00e5 19       		.uleb128 0x19
 4944 00e6 03       		.uleb128 0x3
 4945 00e7 0E       		.uleb128 0xe
 4946 00e8 3A       		.uleb128 0x3a
 4947 00e9 0B       		.uleb128 0xb
 4948 00ea 3B       		.uleb128 0x3b
 4949 00eb 0B       		.uleb128 0xb
 4950 00ec 6E       		.uleb128 0x6e
 4951 00ed 0E       		.uleb128 0xe
 4952 00ee 49       		.uleb128 0x49
 4953 00ef 13       		.uleb128 0x13
 4954 00f0 32       		.uleb128 0x32
 4955 00f1 0B       		.uleb128 0xb
 4956 00f2 3C       		.uleb128 0x3c
 4957 00f3 19       		.uleb128 0x19
 4958 00f4 64       		.uleb128 0x64
 4959 00f5 13       		.uleb128 0x13
 4960 00f6 01       		.uleb128 0x1
 4961 00f7 13       		.uleb128 0x13
 4962 00f8 00       		.byte	0
 4963 00f9 00       		.byte	0
 4964 00fa 11       		.uleb128 0x11
 4965 00fb 2E       		.uleb128 0x2e
 4966 00fc 01       		.byte	0x1
 4967 00fd 3F       		.uleb128 0x3f
 4968 00fe 19       		.uleb128 0x19
 4969 00ff 03       		.uleb128 0x3
 4970 0100 0E       		.uleb128 0xe
GAS LISTING /tmp/ccMbRvxK.s 			page 91


 4971 0101 3A       		.uleb128 0x3a
 4972 0102 0B       		.uleb128 0xb
 4973 0103 3B       		.uleb128 0x3b
 4974 0104 0B       		.uleb128 0xb
 4975 0105 6E       		.uleb128 0x6e
 4976 0106 0E       		.uleb128 0xe
 4977 0107 49       		.uleb128 0x49
 4978 0108 13       		.uleb128 0x13
 4979 0109 32       		.uleb128 0x32
 4980 010a 0B       		.uleb128 0xb
 4981 010b 3C       		.uleb128 0x3c
 4982 010c 19       		.uleb128 0x19
 4983 010d 63       		.uleb128 0x63
 4984 010e 19       		.uleb128 0x19
 4985 010f 64       		.uleb128 0x64
 4986 0110 13       		.uleb128 0x13
 4987 0111 01       		.uleb128 0x1
 4988 0112 13       		.uleb128 0x13
 4989 0113 00       		.byte	0
 4990 0114 00       		.byte	0
 4991 0115 12       		.uleb128 0x12
 4992 0116 2E       		.uleb128 0x2e
 4993 0117 01       		.byte	0x1
 4994 0118 3F       		.uleb128 0x3f
 4995 0119 19       		.uleb128 0x19
 4996 011a 03       		.uleb128 0x3
 4997 011b 0E       		.uleb128 0xe
 4998 011c 3A       		.uleb128 0x3a
 4999 011d 0B       		.uleb128 0xb
 5000 011e 3B       		.uleb128 0x3b
 5001 011f 0B       		.uleb128 0xb
 5002 0120 6E       		.uleb128 0x6e
 5003 0121 0E       		.uleb128 0xe
 5004 0122 49       		.uleb128 0x49
 5005 0123 13       		.uleb128 0x13
 5006 0124 32       		.uleb128 0x32
 5007 0125 0B       		.uleb128 0xb
 5008 0126 3C       		.uleb128 0x3c
 5009 0127 19       		.uleb128 0x19
 5010 0128 64       		.uleb128 0x64
 5011 0129 13       		.uleb128 0x13
 5012 012a 00       		.byte	0
 5013 012b 00       		.byte	0
 5014 012c 13       		.uleb128 0x13
 5015 012d 26       		.uleb128 0x26
 5016 012e 00       		.byte	0
 5017 012f 49       		.uleb128 0x49
 5018 0130 13       		.uleb128 0x13
 5019 0131 00       		.byte	0
 5020 0132 00       		.byte	0
 5021 0133 14       		.uleb128 0x14
 5022 0134 16       		.uleb128 0x16
 5023 0135 00       		.byte	0
 5024 0136 03       		.uleb128 0x3
 5025 0137 0E       		.uleb128 0xe
 5026 0138 3A       		.uleb128 0x3a
 5027 0139 0B       		.uleb128 0xb
GAS LISTING /tmp/ccMbRvxK.s 			page 92


 5028 013a 3B       		.uleb128 0x3b
 5029 013b 0B       		.uleb128 0xb
 5030 013c 49       		.uleb128 0x49
 5031 013d 13       		.uleb128 0x13
 5032 013e 00       		.byte	0
 5033 013f 00       		.byte	0
 5034 0140 15       		.uleb128 0x15
 5035 0141 02       		.uleb128 0x2
 5036 0142 00       		.byte	0
 5037 0143 03       		.uleb128 0x3
 5038 0144 0E       		.uleb128 0xe
 5039 0145 3C       		.uleb128 0x3c
 5040 0146 19       		.uleb128 0x19
 5041 0147 00       		.byte	0
 5042 0148 00       		.byte	0
 5043 0149 16       		.uleb128 0x16
 5044 014a 13       		.uleb128 0x13
 5045 014b 01       		.byte	0x1
 5046 014c 03       		.uleb128 0x3
 5047 014d 0E       		.uleb128 0xe
 5048 014e 0B       		.uleb128 0xb
 5049 014f 0B       		.uleb128 0xb
 5050 0150 3A       		.uleb128 0x3a
 5051 0151 0B       		.uleb128 0xb
 5052 0152 3B       		.uleb128 0x3b
 5053 0153 0B       		.uleb128 0xb
 5054 0154 01       		.uleb128 0x1
 5055 0155 13       		.uleb128 0x13
 5056 0156 00       		.byte	0
 5057 0157 00       		.byte	0
 5058 0158 17       		.uleb128 0x17
 5059 0159 0D       		.uleb128 0xd
 5060 015a 00       		.byte	0
 5061 015b 03       		.uleb128 0x3
 5062 015c 0E       		.uleb128 0xe
 5063 015d 3A       		.uleb128 0x3a
 5064 015e 0B       		.uleb128 0xb
 5065 015f 3B       		.uleb128 0x3b
 5066 0160 0B       		.uleb128 0xb
 5067 0161 49       		.uleb128 0x49
 5068 0162 13       		.uleb128 0x13
 5069 0163 3F       		.uleb128 0x3f
 5070 0164 19       		.uleb128 0x19
 5071 0165 3C       		.uleb128 0x3c
 5072 0166 19       		.uleb128 0x19
 5073 0167 00       		.byte	0
 5074 0168 00       		.byte	0
 5075 0169 18       		.uleb128 0x18
 5076 016a 2F       		.uleb128 0x2f
 5077 016b 00       		.byte	0
 5078 016c 03       		.uleb128 0x3
 5079 016d 08       		.uleb128 0x8
 5080 016e 49       		.uleb128 0x49
 5081 016f 13       		.uleb128 0x13
 5082 0170 00       		.byte	0
 5083 0171 00       		.byte	0
 5084 0172 19       		.uleb128 0x19
GAS LISTING /tmp/ccMbRvxK.s 			page 93


 5085 0173 30       		.uleb128 0x30
 5086 0174 00       		.byte	0
 5087 0175 03       		.uleb128 0x3
 5088 0176 08       		.uleb128 0x8
 5089 0177 49       		.uleb128 0x49
 5090 0178 13       		.uleb128 0x13
 5091 0179 1C       		.uleb128 0x1c
 5092 017a 0B       		.uleb128 0xb
 5093 017b 00       		.byte	0
 5094 017c 00       		.byte	0
 5095 017d 1A       		.uleb128 0x1a
 5096 017e 13       		.uleb128 0x13
 5097 017f 00       		.byte	0
 5098 0180 03       		.uleb128 0x3
 5099 0181 0E       		.uleb128 0xe
 5100 0182 0B       		.uleb128 0xb
 5101 0183 0B       		.uleb128 0xb
 5102 0184 3A       		.uleb128 0x3a
 5103 0185 0B       		.uleb128 0xb
 5104 0186 3B       		.uleb128 0x3b
 5105 0187 0B       		.uleb128 0xb
 5106 0188 00       		.byte	0
 5107 0189 00       		.byte	0
 5108 018a 1B       		.uleb128 0x1b
 5109 018b 2E       		.uleb128 0x2e
 5110 018c 01       		.byte	0x1
 5111 018d 3F       		.uleb128 0x3f
 5112 018e 19       		.uleb128 0x19
 5113 018f 03       		.uleb128 0x3
 5114 0190 0E       		.uleb128 0xe
 5115 0191 3A       		.uleb128 0x3a
 5116 0192 0B       		.uleb128 0xb
 5117 0193 3B       		.uleb128 0x3b
 5118 0194 0B       		.uleb128 0xb
 5119 0195 6E       		.uleb128 0x6e
 5120 0196 0E       		.uleb128 0xe
 5121 0197 3C       		.uleb128 0x3c
 5122 0198 19       		.uleb128 0x19
 5123 0199 01       		.uleb128 0x1
 5124 019a 13       		.uleb128 0x13
 5125 019b 00       		.byte	0
 5126 019c 00       		.byte	0
 5127 019d 1C       		.uleb128 0x1c
 5128 019e 2E       		.uleb128 0x2e
 5129 019f 01       		.byte	0x1
 5130 01a0 3F       		.uleb128 0x3f
 5131 01a1 19       		.uleb128 0x19
 5132 01a2 03       		.uleb128 0x3
 5133 01a3 08       		.uleb128 0x8
 5134 01a4 3A       		.uleb128 0x3a
 5135 01a5 0B       		.uleb128 0xb
 5136 01a6 3B       		.uleb128 0x3b
 5137 01a7 0B       		.uleb128 0xb
 5138 01a8 6E       		.uleb128 0x6e
 5139 01a9 0E       		.uleb128 0xe
 5140 01aa 49       		.uleb128 0x49
 5141 01ab 13       		.uleb128 0x13
GAS LISTING /tmp/ccMbRvxK.s 			page 94


 5142 01ac 3C       		.uleb128 0x3c
 5143 01ad 19       		.uleb128 0x19
 5144 01ae 01       		.uleb128 0x1
 5145 01af 13       		.uleb128 0x13
 5146 01b0 00       		.byte	0
 5147 01b1 00       		.byte	0
 5148 01b2 1D       		.uleb128 0x1d
 5149 01b3 2E       		.uleb128 0x2e
 5150 01b4 01       		.byte	0x1
 5151 01b5 3F       		.uleb128 0x3f
 5152 01b6 19       		.uleb128 0x19
 5153 01b7 03       		.uleb128 0x3
 5154 01b8 0E       		.uleb128 0xe
 5155 01b9 3A       		.uleb128 0x3a
 5156 01ba 0B       		.uleb128 0xb
 5157 01bb 3B       		.uleb128 0x3b
 5158 01bc 05       		.uleb128 0x5
 5159 01bd 6E       		.uleb128 0x6e
 5160 01be 0E       		.uleb128 0xe
 5161 01bf 49       		.uleb128 0x49
 5162 01c0 13       		.uleb128 0x13
 5163 01c1 3C       		.uleb128 0x3c
 5164 01c2 19       		.uleb128 0x19
 5165 01c3 01       		.uleb128 0x1
 5166 01c4 13       		.uleb128 0x13
 5167 01c5 00       		.byte	0
 5168 01c6 00       		.byte	0
 5169 01c7 1E       		.uleb128 0x1e
 5170 01c8 2E       		.uleb128 0x2e
 5171 01c9 00       		.byte	0
 5172 01ca 3F       		.uleb128 0x3f
 5173 01cb 19       		.uleb128 0x19
 5174 01cc 03       		.uleb128 0x3
 5175 01cd 08       		.uleb128 0x8
 5176 01ce 3A       		.uleb128 0x3a
 5177 01cf 0B       		.uleb128 0xb
 5178 01d0 3B       		.uleb128 0x3b
 5179 01d1 05       		.uleb128 0x5
 5180 01d2 6E       		.uleb128 0x6e
 5181 01d3 0E       		.uleb128 0xe
 5182 01d4 49       		.uleb128 0x49
 5183 01d5 13       		.uleb128 0x13
 5184 01d6 3C       		.uleb128 0x3c
 5185 01d7 19       		.uleb128 0x19
 5186 01d8 00       		.byte	0
 5187 01d9 00       		.byte	0
 5188 01da 1F       		.uleb128 0x1f
 5189 01db 2E       		.uleb128 0x2e
 5190 01dc 01       		.byte	0x1
 5191 01dd 3F       		.uleb128 0x3f
 5192 01de 19       		.uleb128 0x19
 5193 01df 03       		.uleb128 0x3
 5194 01e0 0E       		.uleb128 0xe
 5195 01e1 3A       		.uleb128 0x3a
 5196 01e2 0B       		.uleb128 0xb
 5197 01e3 3B       		.uleb128 0x3b
 5198 01e4 05       		.uleb128 0x5
GAS LISTING /tmp/ccMbRvxK.s 			page 95


 5199 01e5 6E       		.uleb128 0x6e
 5200 01e6 0E       		.uleb128 0xe
 5201 01e7 49       		.uleb128 0x49
 5202 01e8 13       		.uleb128 0x13
 5203 01e9 3C       		.uleb128 0x3c
 5204 01ea 19       		.uleb128 0x19
 5205 01eb 00       		.byte	0
 5206 01ec 00       		.byte	0
 5207 01ed 20       		.uleb128 0x20
 5208 01ee 39       		.uleb128 0x39
 5209 01ef 00       		.byte	0
 5210 01f0 03       		.uleb128 0x3
 5211 01f1 08       		.uleb128 0x8
 5212 01f2 3A       		.uleb128 0x3a
 5213 01f3 0B       		.uleb128 0xb
 5214 01f4 3B       		.uleb128 0x3b
 5215 01f5 0B       		.uleb128 0xb
 5216 01f6 00       		.byte	0
 5217 01f7 00       		.byte	0
 5218 01f8 21       		.uleb128 0x21
 5219 01f9 04       		.uleb128 0x4
 5220 01fa 01       		.byte	0x1
 5221 01fb 03       		.uleb128 0x3
 5222 01fc 0E       		.uleb128 0xe
 5223 01fd 0B       		.uleb128 0xb
 5224 01fe 0B       		.uleb128 0xb
 5225 01ff 49       		.uleb128 0x49
 5226 0200 13       		.uleb128 0x13
 5227 0201 3A       		.uleb128 0x3a
 5228 0202 0B       		.uleb128 0xb
 5229 0203 3B       		.uleb128 0x3b
 5230 0204 0B       		.uleb128 0xb
 5231 0205 01       		.uleb128 0x1
 5232 0206 13       		.uleb128 0x13
 5233 0207 00       		.byte	0
 5234 0208 00       		.byte	0
 5235 0209 22       		.uleb128 0x22
 5236 020a 28       		.uleb128 0x28
 5237 020b 00       		.byte	0
 5238 020c 03       		.uleb128 0x3
 5239 020d 0E       		.uleb128 0xe
 5240 020e 1C       		.uleb128 0x1c
 5241 020f 0B       		.uleb128 0xb
 5242 0210 00       		.byte	0
 5243 0211 00       		.byte	0
 5244 0212 23       		.uleb128 0x23
 5245 0213 28       		.uleb128 0x28
 5246 0214 00       		.byte	0
 5247 0215 03       		.uleb128 0x3
 5248 0216 0E       		.uleb128 0xe
 5249 0217 1C       		.uleb128 0x1c
 5250 0218 05       		.uleb128 0x5
 5251 0219 00       		.byte	0
 5252 021a 00       		.byte	0
 5253 021b 24       		.uleb128 0x24
 5254 021c 28       		.uleb128 0x28
 5255 021d 00       		.byte	0
GAS LISTING /tmp/ccMbRvxK.s 			page 96


 5256 021e 03       		.uleb128 0x3
 5257 021f 0E       		.uleb128 0xe
 5258 0220 1C       		.uleb128 0x1c
 5259 0221 06       		.uleb128 0x6
 5260 0222 00       		.byte	0
 5261 0223 00       		.byte	0
 5262 0224 25       		.uleb128 0x25
 5263 0225 28       		.uleb128 0x28
 5264 0226 00       		.byte	0
 5265 0227 03       		.uleb128 0x3
 5266 0228 0E       		.uleb128 0xe
 5267 0229 1C       		.uleb128 0x1c
 5268 022a 0D       		.uleb128 0xd
 5269 022b 00       		.byte	0
 5270 022c 00       		.byte	0
 5271 022d 26       		.uleb128 0x26
 5272 022e 02       		.uleb128 0x2
 5273 022f 01       		.byte	0x1
 5274 0230 03       		.uleb128 0x3
 5275 0231 0E       		.uleb128 0xe
 5276 0232 3C       		.uleb128 0x3c
 5277 0233 19       		.uleb128 0x19
 5278 0234 01       		.uleb128 0x1
 5279 0235 13       		.uleb128 0x13
 5280 0236 00       		.byte	0
 5281 0237 00       		.byte	0
 5282 0238 27       		.uleb128 0x27
 5283 0239 02       		.uleb128 0x2
 5284 023a 01       		.byte	0x1
 5285 023b 03       		.uleb128 0x3
 5286 023c 0E       		.uleb128 0xe
 5287 023d 0B       		.uleb128 0xb
 5288 023e 0B       		.uleb128 0xb
 5289 023f 3A       		.uleb128 0x3a
 5290 0240 0B       		.uleb128 0xb
 5291 0241 3B       		.uleb128 0x3b
 5292 0242 05       		.uleb128 0x5
 5293 0243 32       		.uleb128 0x32
 5294 0244 0B       		.uleb128 0xb
 5295 0245 01       		.uleb128 0x1
 5296 0246 13       		.uleb128 0x13
 5297 0247 00       		.byte	0
 5298 0248 00       		.byte	0
 5299 0249 28       		.uleb128 0x28
 5300 024a 0D       		.uleb128 0xd
 5301 024b 00       		.byte	0
 5302 024c 03       		.uleb128 0x3
 5303 024d 0E       		.uleb128 0xe
 5304 024e 3A       		.uleb128 0x3a
 5305 024f 0B       		.uleb128 0xb
 5306 0250 3B       		.uleb128 0x3b
 5307 0251 05       		.uleb128 0x5
 5308 0252 49       		.uleb128 0x49
 5309 0253 13       		.uleb128 0x13
 5310 0254 3F       		.uleb128 0x3f
 5311 0255 19       		.uleb128 0x19
 5312 0256 3C       		.uleb128 0x3c
GAS LISTING /tmp/ccMbRvxK.s 			page 97


 5313 0257 19       		.uleb128 0x19
 5314 0258 00       		.byte	0
 5315 0259 00       		.byte	0
 5316 025a 29       		.uleb128 0x29
 5317 025b 2E       		.uleb128 0x2e
 5318 025c 01       		.byte	0x1
 5319 025d 3F       		.uleb128 0x3f
 5320 025e 19       		.uleb128 0x19
 5321 025f 03       		.uleb128 0x3
 5322 0260 0E       		.uleb128 0xe
 5323 0261 3A       		.uleb128 0x3a
 5324 0262 0B       		.uleb128 0xb
 5325 0263 3B       		.uleb128 0x3b
 5326 0264 05       		.uleb128 0x5
 5327 0265 6E       		.uleb128 0x6e
 5328 0266 0E       		.uleb128 0xe
 5329 0267 32       		.uleb128 0x32
 5330 0268 0B       		.uleb128 0xb
 5331 0269 3C       		.uleb128 0x3c
 5332 026a 19       		.uleb128 0x19
 5333 026b 64       		.uleb128 0x64
 5334 026c 13       		.uleb128 0x13
 5335 026d 01       		.uleb128 0x1
 5336 026e 13       		.uleb128 0x13
 5337 026f 00       		.byte	0
 5338 0270 00       		.byte	0
 5339 0271 2A       		.uleb128 0x2a
 5340 0272 2E       		.uleb128 0x2e
 5341 0273 01       		.byte	0x1
 5342 0274 3F       		.uleb128 0x3f
 5343 0275 19       		.uleb128 0x19
 5344 0276 03       		.uleb128 0x3
 5345 0277 0E       		.uleb128 0xe
 5346 0278 3A       		.uleb128 0x3a
 5347 0279 0B       		.uleb128 0xb
 5348 027a 3B       		.uleb128 0x3b
 5349 027b 05       		.uleb128 0x5
 5350 027c 6E       		.uleb128 0x6e
 5351 027d 0E       		.uleb128 0xe
 5352 027e 32       		.uleb128 0x32
 5353 027f 0B       		.uleb128 0xb
 5354 0280 3C       		.uleb128 0x3c
 5355 0281 19       		.uleb128 0x19
 5356 0282 64       		.uleb128 0x64
 5357 0283 13       		.uleb128 0x13
 5358 0284 00       		.byte	0
 5359 0285 00       		.byte	0
 5360 0286 2B       		.uleb128 0x2b
 5361 0287 16       		.uleb128 0x16
 5362 0288 00       		.byte	0
 5363 0289 03       		.uleb128 0x3
 5364 028a 0E       		.uleb128 0xe
 5365 028b 3A       		.uleb128 0x3a
 5366 028c 0B       		.uleb128 0xb
 5367 028d 3B       		.uleb128 0x3b
 5368 028e 05       		.uleb128 0x5
 5369 028f 49       		.uleb128 0x49
GAS LISTING /tmp/ccMbRvxK.s 			page 98


 5370 0290 13       		.uleb128 0x13
 5371 0291 32       		.uleb128 0x32
 5372 0292 0B       		.uleb128 0xb
 5373 0293 00       		.byte	0
 5374 0294 00       		.byte	0
 5375 0295 2C       		.uleb128 0x2c
 5376 0296 0D       		.uleb128 0xd
 5377 0297 00       		.byte	0
 5378 0298 03       		.uleb128 0x3
 5379 0299 0E       		.uleb128 0xe
 5380 029a 3A       		.uleb128 0x3a
 5381 029b 0B       		.uleb128 0xb
 5382 029c 3B       		.uleb128 0x3b
 5383 029d 05       		.uleb128 0x5
 5384 029e 49       		.uleb128 0x49
 5385 029f 13       		.uleb128 0x13
 5386 02a0 3F       		.uleb128 0x3f
 5387 02a1 19       		.uleb128 0x19
 5388 02a2 32       		.uleb128 0x32
 5389 02a3 0B       		.uleb128 0xb
 5390 02a4 3C       		.uleb128 0x3c
 5391 02a5 19       		.uleb128 0x19
 5392 02a6 1C       		.uleb128 0x1c
 5393 02a7 0B       		.uleb128 0xb
 5394 02a8 00       		.byte	0
 5395 02a9 00       		.byte	0
 5396 02aa 2D       		.uleb128 0x2d
 5397 02ab 0D       		.uleb128 0xd
 5398 02ac 00       		.byte	0
 5399 02ad 03       		.uleb128 0x3
 5400 02ae 08       		.uleb128 0x8
 5401 02af 3A       		.uleb128 0x3a
 5402 02b0 0B       		.uleb128 0xb
 5403 02b1 3B       		.uleb128 0x3b
 5404 02b2 05       		.uleb128 0x5
 5405 02b3 49       		.uleb128 0x49
 5406 02b4 13       		.uleb128 0x13
 5407 02b5 3F       		.uleb128 0x3f
 5408 02b6 19       		.uleb128 0x19
 5409 02b7 32       		.uleb128 0x32
 5410 02b8 0B       		.uleb128 0xb
 5411 02b9 3C       		.uleb128 0x3c
 5412 02ba 19       		.uleb128 0x19
 5413 02bb 1C       		.uleb128 0x1c
 5414 02bc 0B       		.uleb128 0xb
 5415 02bd 00       		.byte	0
 5416 02be 00       		.byte	0
 5417 02bf 2E       		.uleb128 0x2e
 5418 02c0 0D       		.uleb128 0xd
 5419 02c1 00       		.byte	0
 5420 02c2 03       		.uleb128 0x3
 5421 02c3 0E       		.uleb128 0xe
 5422 02c4 3A       		.uleb128 0x3a
 5423 02c5 0B       		.uleb128 0xb
 5424 02c6 3B       		.uleb128 0x3b
 5425 02c7 05       		.uleb128 0x5
 5426 02c8 49       		.uleb128 0x49
GAS LISTING /tmp/ccMbRvxK.s 			page 99


 5427 02c9 13       		.uleb128 0x13
 5428 02ca 3F       		.uleb128 0x3f
 5429 02cb 19       		.uleb128 0x19
 5430 02cc 32       		.uleb128 0x32
 5431 02cd 0B       		.uleb128 0xb
 5432 02ce 3C       		.uleb128 0x3c
 5433 02cf 19       		.uleb128 0x19
 5434 02d0 1C       		.uleb128 0x1c
 5435 02d1 05       		.uleb128 0x5
 5436 02d2 00       		.byte	0
 5437 02d3 00       		.byte	0
 5438 02d4 2F       		.uleb128 0x2f
 5439 02d5 2F       		.uleb128 0x2f
 5440 02d6 00       		.byte	0
 5441 02d7 03       		.uleb128 0x3
 5442 02d8 0E       		.uleb128 0xe
 5443 02d9 49       		.uleb128 0x49
 5444 02da 13       		.uleb128 0x13
 5445 02db 00       		.byte	0
 5446 02dc 00       		.byte	0
 5447 02dd 30       		.uleb128 0x30
 5448 02de 2F       		.uleb128 0x2f
 5449 02df 00       		.byte	0
 5450 02e0 03       		.uleb128 0x3
 5451 02e1 0E       		.uleb128 0xe
 5452 02e2 49       		.uleb128 0x49
 5453 02e3 13       		.uleb128 0x13
 5454 02e4 1E       		.uleb128 0x1e
 5455 02e5 19       		.uleb128 0x19
 5456 02e6 00       		.byte	0
 5457 02e7 00       		.byte	0
 5458 02e8 31       		.uleb128 0x31
 5459 02e9 34       		.uleb128 0x34
 5460 02ea 00       		.byte	0
 5461 02eb 03       		.uleb128 0x3
 5462 02ec 0E       		.uleb128 0xe
 5463 02ed 3A       		.uleb128 0x3a
 5464 02ee 0B       		.uleb128 0xb
 5465 02ef 3B       		.uleb128 0x3b
 5466 02f0 0B       		.uleb128 0xb
 5467 02f1 49       		.uleb128 0x49
 5468 02f2 13       		.uleb128 0x13
 5469 02f3 3C       		.uleb128 0x3c
 5470 02f4 19       		.uleb128 0x19
 5471 02f5 1C       		.uleb128 0x1c
 5472 02f6 0A       		.uleb128 0xa
 5473 02f7 00       		.byte	0
 5474 02f8 00       		.byte	0
 5475 02f9 32       		.uleb128 0x32
 5476 02fa 34       		.uleb128 0x34
 5477 02fb 00       		.byte	0
 5478 02fc 03       		.uleb128 0x3
 5479 02fd 0E       		.uleb128 0xe
 5480 02fe 3A       		.uleb128 0x3a
 5481 02ff 0B       		.uleb128 0xb
 5482 0300 3B       		.uleb128 0x3b
 5483 0301 0B       		.uleb128 0xb
GAS LISTING /tmp/ccMbRvxK.s 			page 100


 5484 0302 6E       		.uleb128 0x6e
 5485 0303 0E       		.uleb128 0xe
 5486 0304 49       		.uleb128 0x49
 5487 0305 13       		.uleb128 0x13
 5488 0306 3F       		.uleb128 0x3f
 5489 0307 19       		.uleb128 0x19
 5490 0308 3C       		.uleb128 0x3c
 5491 0309 19       		.uleb128 0x19
 5492 030a 00       		.byte	0
 5493 030b 00       		.byte	0
 5494 030c 33       		.uleb128 0x33
 5495 030d 34       		.uleb128 0x34
 5496 030e 00       		.byte	0
 5497 030f 03       		.uleb128 0x3
 5498 0310 0E       		.uleb128 0xe
 5499 0311 3A       		.uleb128 0x3a
 5500 0312 0B       		.uleb128 0xb
 5501 0313 3B       		.uleb128 0x3b
 5502 0314 0B       		.uleb128 0xb
 5503 0315 49       		.uleb128 0x49
 5504 0316 13       		.uleb128 0x13
 5505 0317 3C       		.uleb128 0x3c
 5506 0318 19       		.uleb128 0x19
 5507 0319 00       		.byte	0
 5508 031a 00       		.byte	0
 5509 031b 34       		.uleb128 0x34
 5510 031c 13       		.uleb128 0x13
 5511 031d 01       		.byte	0x1
 5512 031e 03       		.uleb128 0x3
 5513 031f 0E       		.uleb128 0xe
 5514 0320 0B       		.uleb128 0xb
 5515 0321 0B       		.uleb128 0xb
 5516 0322 3A       		.uleb128 0x3a
 5517 0323 0B       		.uleb128 0xb
 5518 0324 3B       		.uleb128 0x3b
 5519 0325 0B       		.uleb128 0xb
 5520 0326 00       		.byte	0
 5521 0327 00       		.byte	0
 5522 0328 35       		.uleb128 0x35
 5523 0329 0D       		.uleb128 0xd
 5524 032a 00       		.byte	0
 5525 032b 03       		.uleb128 0x3
 5526 032c 0E       		.uleb128 0xe
 5527 032d 3A       		.uleb128 0x3a
 5528 032e 0B       		.uleb128 0xb
 5529 032f 3B       		.uleb128 0x3b
 5530 0330 05       		.uleb128 0x5
 5531 0331 49       		.uleb128 0x49
 5532 0332 13       		.uleb128 0x13
 5533 0333 38       		.uleb128 0x38
 5534 0334 0B       		.uleb128 0xb
 5535 0335 00       		.byte	0
 5536 0336 00       		.byte	0
 5537 0337 36       		.uleb128 0x36
 5538 0338 24       		.uleb128 0x24
 5539 0339 00       		.byte	0
 5540 033a 0B       		.uleb128 0xb
GAS LISTING /tmp/ccMbRvxK.s 			page 101


 5541 033b 0B       		.uleb128 0xb
 5542 033c 3E       		.uleb128 0x3e
 5543 033d 0B       		.uleb128 0xb
 5544 033e 03       		.uleb128 0x3
 5545 033f 0E       		.uleb128 0xe
 5546 0340 00       		.byte	0
 5547 0341 00       		.byte	0
 5548 0342 37       		.uleb128 0x37
 5549 0343 0F       		.uleb128 0xf
 5550 0344 00       		.byte	0
 5551 0345 0B       		.uleb128 0xb
 5552 0346 0B       		.uleb128 0xb
 5553 0347 00       		.byte	0
 5554 0348 00       		.byte	0
 5555 0349 38       		.uleb128 0x38
 5556 034a 16       		.uleb128 0x16
 5557 034b 00       		.byte	0
 5558 034c 03       		.uleb128 0x3
 5559 034d 0E       		.uleb128 0xe
 5560 034e 3A       		.uleb128 0x3a
 5561 034f 0B       		.uleb128 0xb
 5562 0350 3B       		.uleb128 0x3b
 5563 0351 05       		.uleb128 0x5
 5564 0352 49       		.uleb128 0x49
 5565 0353 13       		.uleb128 0x13
 5566 0354 00       		.byte	0
 5567 0355 00       		.byte	0
 5568 0356 39       		.uleb128 0x39
 5569 0357 13       		.uleb128 0x13
 5570 0358 01       		.byte	0x1
 5571 0359 0B       		.uleb128 0xb
 5572 035a 0B       		.uleb128 0xb
 5573 035b 3A       		.uleb128 0x3a
 5574 035c 0B       		.uleb128 0xb
 5575 035d 3B       		.uleb128 0x3b
 5576 035e 0B       		.uleb128 0xb
 5577 035f 6E       		.uleb128 0x6e
 5578 0360 0E       		.uleb128 0xe
 5579 0361 01       		.uleb128 0x1
 5580 0362 13       		.uleb128 0x13
 5581 0363 00       		.byte	0
 5582 0364 00       		.byte	0
 5583 0365 3A       		.uleb128 0x3a
 5584 0366 17       		.uleb128 0x17
 5585 0367 01       		.byte	0x1
 5586 0368 0B       		.uleb128 0xb
 5587 0369 0B       		.uleb128 0xb
 5588 036a 3A       		.uleb128 0x3a
 5589 036b 0B       		.uleb128 0xb
 5590 036c 3B       		.uleb128 0x3b
 5591 036d 0B       		.uleb128 0xb
 5592 036e 01       		.uleb128 0x1
 5593 036f 13       		.uleb128 0x13
 5594 0370 00       		.byte	0
 5595 0371 00       		.byte	0
 5596 0372 3B       		.uleb128 0x3b
 5597 0373 0D       		.uleb128 0xd
GAS LISTING /tmp/ccMbRvxK.s 			page 102


 5598 0374 00       		.byte	0
 5599 0375 03       		.uleb128 0x3
 5600 0376 0E       		.uleb128 0xe
 5601 0377 3A       		.uleb128 0x3a
 5602 0378 0B       		.uleb128 0xb
 5603 0379 3B       		.uleb128 0x3b
 5604 037a 0B       		.uleb128 0xb
 5605 037b 49       		.uleb128 0x49
 5606 037c 13       		.uleb128 0x13
 5607 037d 00       		.byte	0
 5608 037e 00       		.byte	0
 5609 037f 3C       		.uleb128 0x3c
 5610 0380 01       		.uleb128 0x1
 5611 0381 01       		.byte	0x1
 5612 0382 49       		.uleb128 0x49
 5613 0383 13       		.uleb128 0x13
 5614 0384 01       		.uleb128 0x1
 5615 0385 13       		.uleb128 0x13
 5616 0386 00       		.byte	0
 5617 0387 00       		.byte	0
 5618 0388 3D       		.uleb128 0x3d
 5619 0389 21       		.uleb128 0x21
 5620 038a 00       		.byte	0
 5621 038b 49       		.uleb128 0x49
 5622 038c 13       		.uleb128 0x13
 5623 038d 2F       		.uleb128 0x2f
 5624 038e 0B       		.uleb128 0xb
 5625 038f 00       		.byte	0
 5626 0390 00       		.byte	0
 5627 0391 3E       		.uleb128 0x3e
 5628 0392 24       		.uleb128 0x24
 5629 0393 00       		.byte	0
 5630 0394 0B       		.uleb128 0xb
 5631 0395 0B       		.uleb128 0xb
 5632 0396 3E       		.uleb128 0x3e
 5633 0397 0B       		.uleb128 0xb
 5634 0398 03       		.uleb128 0x3
 5635 0399 08       		.uleb128 0x8
 5636 039a 00       		.byte	0
 5637 039b 00       		.byte	0
 5638 039c 3F       		.uleb128 0x3f
 5639 039d 0F       		.uleb128 0xf
 5640 039e 00       		.byte	0
 5641 039f 0B       		.uleb128 0xb
 5642 03a0 0B       		.uleb128 0xb
 5643 03a1 49       		.uleb128 0x49
 5644 03a2 13       		.uleb128 0x13
 5645 03a3 00       		.byte	0
 5646 03a4 00       		.byte	0
 5647 03a5 40       		.uleb128 0x40
 5648 03a6 2E       		.uleb128 0x2e
 5649 03a7 01       		.byte	0x1
 5650 03a8 3F       		.uleb128 0x3f
 5651 03a9 19       		.uleb128 0x19
 5652 03aa 03       		.uleb128 0x3
 5653 03ab 0E       		.uleb128 0xe
 5654 03ac 3A       		.uleb128 0x3a
GAS LISTING /tmp/ccMbRvxK.s 			page 103


 5655 03ad 0B       		.uleb128 0xb
 5656 03ae 3B       		.uleb128 0x3b
 5657 03af 05       		.uleb128 0x5
 5658 03b0 49       		.uleb128 0x49
 5659 03b1 13       		.uleb128 0x13
 5660 03b2 3C       		.uleb128 0x3c
 5661 03b3 19       		.uleb128 0x19
 5662 03b4 01       		.uleb128 0x1
 5663 03b5 13       		.uleb128 0x13
 5664 03b6 00       		.byte	0
 5665 03b7 00       		.byte	0
 5666 03b8 41       		.uleb128 0x41
 5667 03b9 18       		.uleb128 0x18
 5668 03ba 00       		.byte	0
 5669 03bb 00       		.byte	0
 5670 03bc 00       		.byte	0
 5671 03bd 42       		.uleb128 0x42
 5672 03be 2E       		.uleb128 0x2e
 5673 03bf 00       		.byte	0
 5674 03c0 3F       		.uleb128 0x3f
 5675 03c1 19       		.uleb128 0x19
 5676 03c2 03       		.uleb128 0x3
 5677 03c3 0E       		.uleb128 0xe
 5678 03c4 3A       		.uleb128 0x3a
 5679 03c5 0B       		.uleb128 0xb
 5680 03c6 3B       		.uleb128 0x3b
 5681 03c7 05       		.uleb128 0x5
 5682 03c8 49       		.uleb128 0x49
 5683 03c9 13       		.uleb128 0x13
 5684 03ca 3C       		.uleb128 0x3c
 5685 03cb 19       		.uleb128 0x19
 5686 03cc 00       		.byte	0
 5687 03cd 00       		.byte	0
 5688 03ce 43       		.uleb128 0x43
 5689 03cf 2E       		.uleb128 0x2e
 5690 03d0 01       		.byte	0x1
 5691 03d1 3F       		.uleb128 0x3f
 5692 03d2 19       		.uleb128 0x19
 5693 03d3 03       		.uleb128 0x3
 5694 03d4 0E       		.uleb128 0xe
 5695 03d5 3A       		.uleb128 0x3a
 5696 03d6 0B       		.uleb128 0xb
 5697 03d7 3B       		.uleb128 0x3b
 5698 03d8 0B       		.uleb128 0xb
 5699 03d9 49       		.uleb128 0x49
 5700 03da 13       		.uleb128 0x13
 5701 03db 3C       		.uleb128 0x3c
 5702 03dc 19       		.uleb128 0x19
 5703 03dd 01       		.uleb128 0x1
 5704 03de 13       		.uleb128 0x13
 5705 03df 00       		.byte	0
 5706 03e0 00       		.byte	0
 5707 03e1 44       		.uleb128 0x44
 5708 03e2 13       		.uleb128 0x13
 5709 03e3 01       		.byte	0x1
 5710 03e4 03       		.uleb128 0x3
 5711 03e5 08       		.uleb128 0x8
GAS LISTING /tmp/ccMbRvxK.s 			page 104


 5712 03e6 0B       		.uleb128 0xb
 5713 03e7 0B       		.uleb128 0xb
 5714 03e8 3A       		.uleb128 0x3a
 5715 03e9 0B       		.uleb128 0xb
 5716 03ea 3B       		.uleb128 0x3b
 5717 03eb 0B       		.uleb128 0xb
 5718 03ec 01       		.uleb128 0x1
 5719 03ed 13       		.uleb128 0x13
 5720 03ee 00       		.byte	0
 5721 03ef 00       		.byte	0
 5722 03f0 45       		.uleb128 0x45
 5723 03f1 2E       		.uleb128 0x2e
 5724 03f2 01       		.byte	0x1
 5725 03f3 3F       		.uleb128 0x3f
 5726 03f4 19       		.uleb128 0x19
 5727 03f5 03       		.uleb128 0x3
 5728 03f6 0E       		.uleb128 0xe
 5729 03f7 3A       		.uleb128 0x3a
 5730 03f8 0B       		.uleb128 0xb
 5731 03f9 3B       		.uleb128 0x3b
 5732 03fa 0B       		.uleb128 0xb
 5733 03fb 6E       		.uleb128 0x6e
 5734 03fc 0E       		.uleb128 0xe
 5735 03fd 49       		.uleb128 0x49
 5736 03fe 13       		.uleb128 0x13
 5737 03ff 3C       		.uleb128 0x3c
 5738 0400 19       		.uleb128 0x19
 5739 0401 01       		.uleb128 0x1
 5740 0402 13       		.uleb128 0x13
 5741 0403 00       		.byte	0
 5742 0404 00       		.byte	0
 5743 0405 46       		.uleb128 0x46
 5744 0406 10       		.uleb128 0x10
 5745 0407 00       		.byte	0
 5746 0408 0B       		.uleb128 0xb
 5747 0409 0B       		.uleb128 0xb
 5748 040a 49       		.uleb128 0x49
 5749 040b 13       		.uleb128 0x13
 5750 040c 00       		.byte	0
 5751 040d 00       		.byte	0
 5752 040e 47       		.uleb128 0x47
 5753 040f 3B       		.uleb128 0x3b
 5754 0410 00       		.byte	0
 5755 0411 03       		.uleb128 0x3
 5756 0412 0E       		.uleb128 0xe
 5757 0413 00       		.byte	0
 5758 0414 00       		.byte	0
 5759 0415 48       		.uleb128 0x48
 5760 0416 42       		.uleb128 0x42
 5761 0417 00       		.byte	0
 5762 0418 0B       		.uleb128 0xb
 5763 0419 0B       		.uleb128 0xb
 5764 041a 49       		.uleb128 0x49
 5765 041b 13       		.uleb128 0x13
 5766 041c 00       		.byte	0
 5767 041d 00       		.byte	0
 5768 041e 49       		.uleb128 0x49
GAS LISTING /tmp/ccMbRvxK.s 			page 105


 5769 041f 2E       		.uleb128 0x2e
 5770 0420 00       		.byte	0
 5771 0421 3F       		.uleb128 0x3f
 5772 0422 19       		.uleb128 0x19
 5773 0423 03       		.uleb128 0x3
 5774 0424 0E       		.uleb128 0xe
 5775 0425 3A       		.uleb128 0x3a
 5776 0426 0B       		.uleb128 0xb
 5777 0427 3B       		.uleb128 0x3b
 5778 0428 0B       		.uleb128 0xb
 5779 0429 49       		.uleb128 0x49
 5780 042a 13       		.uleb128 0x13
 5781 042b 3C       		.uleb128 0x3c
 5782 042c 19       		.uleb128 0x19
 5783 042d 00       		.byte	0
 5784 042e 00       		.byte	0
 5785 042f 4A       		.uleb128 0x4a
 5786 0430 26       		.uleb128 0x26
 5787 0431 00       		.byte	0
 5788 0432 00       		.byte	0
 5789 0433 00       		.byte	0
 5790 0434 4B       		.uleb128 0x4b
 5791 0435 0D       		.uleb128 0xd
 5792 0436 00       		.byte	0
 5793 0437 03       		.uleb128 0x3
 5794 0438 08       		.uleb128 0x8
 5795 0439 3A       		.uleb128 0x3a
 5796 043a 0B       		.uleb128 0xb
 5797 043b 3B       		.uleb128 0x3b
 5798 043c 0B       		.uleb128 0xb
 5799 043d 49       		.uleb128 0x49
 5800 043e 13       		.uleb128 0x13
 5801 043f 38       		.uleb128 0x38
 5802 0440 0B       		.uleb128 0xb
 5803 0441 00       		.byte	0
 5804 0442 00       		.byte	0
 5805 0443 4C       		.uleb128 0x4c
 5806 0444 15       		.uleb128 0x15
 5807 0445 01       		.byte	0x1
 5808 0446 49       		.uleb128 0x49
 5809 0447 13       		.uleb128 0x13
 5810 0448 01       		.uleb128 0x1
 5811 0449 13       		.uleb128 0x13
 5812 044a 00       		.byte	0
 5813 044b 00       		.byte	0
 5814 044c 4D       		.uleb128 0x4d
 5815 044d 15       		.uleb128 0x15
 5816 044e 00       		.byte	0
 5817 044f 00       		.byte	0
 5818 0450 00       		.byte	0
 5819 0451 4E       		.uleb128 0x4e
 5820 0452 2E       		.uleb128 0x2e
 5821 0453 01       		.byte	0x1
 5822 0454 3F       		.uleb128 0x3f
 5823 0455 19       		.uleb128 0x19
 5824 0456 03       		.uleb128 0x3
 5825 0457 08       		.uleb128 0x8
GAS LISTING /tmp/ccMbRvxK.s 			page 106


 5826 0458 3A       		.uleb128 0x3a
 5827 0459 0B       		.uleb128 0xb
 5828 045a 3B       		.uleb128 0x3b
 5829 045b 05       		.uleb128 0x5
 5830 045c 49       		.uleb128 0x49
 5831 045d 13       		.uleb128 0x13
 5832 045e 3C       		.uleb128 0x3c
 5833 045f 19       		.uleb128 0x19
 5834 0460 01       		.uleb128 0x1
 5835 0461 13       		.uleb128 0x13
 5836 0462 00       		.byte	0
 5837 0463 00       		.byte	0
 5838 0464 4F       		.uleb128 0x4f
 5839 0465 2E       		.uleb128 0x2e
 5840 0466 01       		.byte	0x1
 5841 0467 3F       		.uleb128 0x3f
 5842 0468 19       		.uleb128 0x19
 5843 0469 03       		.uleb128 0x3
 5844 046a 0E       		.uleb128 0xe
 5845 046b 3A       		.uleb128 0x3a
 5846 046c 0B       		.uleb128 0xb
 5847 046d 3B       		.uleb128 0x3b
 5848 046e 05       		.uleb128 0x5
 5849 046f 3C       		.uleb128 0x3c
 5850 0470 19       		.uleb128 0x19
 5851 0471 01       		.uleb128 0x1
 5852 0472 13       		.uleb128 0x13
 5853 0473 00       		.byte	0
 5854 0474 00       		.byte	0
 5855 0475 50       		.uleb128 0x50
 5856 0476 2E       		.uleb128 0x2e
 5857 0477 01       		.byte	0x1
 5858 0478 3F       		.uleb128 0x3f
 5859 0479 19       		.uleb128 0x19
 5860 047a 03       		.uleb128 0x3
 5861 047b 0E       		.uleb128 0xe
 5862 047c 3A       		.uleb128 0x3a
 5863 047d 0B       		.uleb128 0xb
 5864 047e 3B       		.uleb128 0x3b
 5865 047f 05       		.uleb128 0x5
 5866 0480 8701     		.uleb128 0x87
 5867 0482 19       		.uleb128 0x19
 5868 0483 3C       		.uleb128 0x3c
 5869 0484 19       		.uleb128 0x19
 5870 0485 01       		.uleb128 0x1
 5871 0486 13       		.uleb128 0x13
 5872 0487 00       		.byte	0
 5873 0488 00       		.byte	0
 5874 0489 51       		.uleb128 0x51
 5875 048a 16       		.uleb128 0x16
 5876 048b 00       		.byte	0
 5877 048c 03       		.uleb128 0x3
 5878 048d 0E       		.uleb128 0xe
 5879 048e 3A       		.uleb128 0x3a
 5880 048f 0B       		.uleb128 0xb
 5881 0490 3B       		.uleb128 0x3b
 5882 0491 0B       		.uleb128 0xb
GAS LISTING /tmp/ccMbRvxK.s 			page 107


 5883 0492 00       		.byte	0
 5884 0493 00       		.byte	0
 5885 0494 52       		.uleb128 0x52
 5886 0495 2E       		.uleb128 0x2e
 5887 0496 01       		.byte	0x1
 5888 0497 3F       		.uleb128 0x3f
 5889 0498 19       		.uleb128 0x19
 5890 0499 03       		.uleb128 0x3
 5891 049a 0E       		.uleb128 0xe
 5892 049b 3A       		.uleb128 0x3a
 5893 049c 0B       		.uleb128 0xb
 5894 049d 3B       		.uleb128 0x3b
 5895 049e 0B       		.uleb128 0xb
 5896 049f 49       		.uleb128 0x49
 5897 04a0 13       		.uleb128 0x13
 5898 04a1 11       		.uleb128 0x11
 5899 04a2 01       		.uleb128 0x1
 5900 04a3 12       		.uleb128 0x12
 5901 04a4 07       		.uleb128 0x7
 5902 04a5 40       		.uleb128 0x40
 5903 04a6 18       		.uleb128 0x18
 5904 04a7 9642     		.uleb128 0x2116
 5905 04a9 19       		.uleb128 0x19
 5906 04aa 01       		.uleb128 0x1
 5907 04ab 13       		.uleb128 0x13
 5908 04ac 00       		.byte	0
 5909 04ad 00       		.byte	0
 5910 04ae 53       		.uleb128 0x53
 5911 04af 05       		.uleb128 0x5
 5912 04b0 00       		.byte	0
 5913 04b1 03       		.uleb128 0x3
 5914 04b2 0E       		.uleb128 0xe
 5915 04b3 3A       		.uleb128 0x3a
 5916 04b4 0B       		.uleb128 0xb
 5917 04b5 3B       		.uleb128 0x3b
 5918 04b6 0B       		.uleb128 0xb
 5919 04b7 49       		.uleb128 0x49
 5920 04b8 13       		.uleb128 0x13
 5921 04b9 02       		.uleb128 0x2
 5922 04ba 18       		.uleb128 0x18
 5923 04bb 00       		.byte	0
 5924 04bc 00       		.byte	0
 5925 04bd 54       		.uleb128 0x54
 5926 04be 34       		.uleb128 0x34
 5927 04bf 00       		.byte	0
 5928 04c0 03       		.uleb128 0x3
 5929 04c1 08       		.uleb128 0x8
 5930 04c2 3A       		.uleb128 0x3a
 5931 04c3 0B       		.uleb128 0xb
 5932 04c4 3B       		.uleb128 0x3b
 5933 04c5 0B       		.uleb128 0xb
 5934 04c6 49       		.uleb128 0x49
 5935 04c7 13       		.uleb128 0x13
 5936 04c8 02       		.uleb128 0x2
 5937 04c9 18       		.uleb128 0x18
 5938 04ca 00       		.byte	0
 5939 04cb 00       		.byte	0
GAS LISTING /tmp/ccMbRvxK.s 			page 108


 5940 04cc 55       		.uleb128 0x55
 5941 04cd 2E       		.uleb128 0x2e
 5942 04ce 01       		.byte	0x1
 5943 04cf 03       		.uleb128 0x3
 5944 04d0 0E       		.uleb128 0xe
 5945 04d1 34       		.uleb128 0x34
 5946 04d2 19       		.uleb128 0x19
 5947 04d3 11       		.uleb128 0x11
 5948 04d4 01       		.uleb128 0x1
 5949 04d5 12       		.uleb128 0x12
 5950 04d6 07       		.uleb128 0x7
 5951 04d7 40       		.uleb128 0x40
 5952 04d8 18       		.uleb128 0x18
 5953 04d9 9642     		.uleb128 0x2116
 5954 04db 19       		.uleb128 0x19
 5955 04dc 01       		.uleb128 0x1
 5956 04dd 13       		.uleb128 0x13
 5957 04de 00       		.byte	0
 5958 04df 00       		.byte	0
 5959 04e0 56       		.uleb128 0x56
 5960 04e1 2E       		.uleb128 0x2e
 5961 04e2 00       		.byte	0
 5962 04e3 03       		.uleb128 0x3
 5963 04e4 0E       		.uleb128 0xe
 5964 04e5 34       		.uleb128 0x34
 5965 04e6 19       		.uleb128 0x19
 5966 04e7 11       		.uleb128 0x11
 5967 04e8 01       		.uleb128 0x1
 5968 04e9 12       		.uleb128 0x12
 5969 04ea 07       		.uleb128 0x7
 5970 04eb 40       		.uleb128 0x40
 5971 04ec 18       		.uleb128 0x18
 5972 04ed 9642     		.uleb128 0x2116
 5973 04ef 19       		.uleb128 0x19
 5974 04f0 00       		.byte	0
 5975 04f1 00       		.byte	0
 5976 04f2 57       		.uleb128 0x57
 5977 04f3 34       		.uleb128 0x34
 5978 04f4 00       		.byte	0
 5979 04f5 03       		.uleb128 0x3
 5980 04f6 0E       		.uleb128 0xe
 5981 04f7 49       		.uleb128 0x49
 5982 04f8 13       		.uleb128 0x13
 5983 04f9 3F       		.uleb128 0x3f
 5984 04fa 19       		.uleb128 0x19
 5985 04fb 34       		.uleb128 0x34
 5986 04fc 19       		.uleb128 0x19
 5987 04fd 3C       		.uleb128 0x3c
 5988 04fe 19       		.uleb128 0x19
 5989 04ff 00       		.byte	0
 5990 0500 00       		.byte	0
 5991 0501 58       		.uleb128 0x58
 5992 0502 34       		.uleb128 0x34
 5993 0503 00       		.byte	0
 5994 0504 47       		.uleb128 0x47
 5995 0505 13       		.uleb128 0x13
 5996 0506 02       		.uleb128 0x2
GAS LISTING /tmp/ccMbRvxK.s 			page 109


 5997 0507 18       		.uleb128 0x18
 5998 0508 00       		.byte	0
 5999 0509 00       		.byte	0
 6000 050a 59       		.uleb128 0x59
 6001 050b 34       		.uleb128 0x34
 6002 050c 00       		.byte	0
 6003 050d 47       		.uleb128 0x47
 6004 050e 13       		.uleb128 0x13
 6005 050f 6E       		.uleb128 0x6e
 6006 0510 0E       		.uleb128 0xe
 6007 0511 1C       		.uleb128 0x1c
 6008 0512 0B       		.uleb128 0xb
 6009 0513 00       		.byte	0
 6010 0514 00       		.byte	0
 6011 0515 5A       		.uleb128 0x5a
 6012 0516 34       		.uleb128 0x34
 6013 0517 00       		.byte	0
 6014 0518 47       		.uleb128 0x47
 6015 0519 13       		.uleb128 0x13
 6016 051a 6E       		.uleb128 0x6e
 6017 051b 0E       		.uleb128 0xe
 6018 051c 1C       		.uleb128 0x1c
 6019 051d 0D       		.uleb128 0xd
 6020 051e 00       		.byte	0
 6021 051f 00       		.byte	0
 6022 0520 5B       		.uleb128 0x5b
 6023 0521 34       		.uleb128 0x34
 6024 0522 00       		.byte	0
 6025 0523 47       		.uleb128 0x47
 6026 0524 13       		.uleb128 0x13
 6027 0525 6E       		.uleb128 0x6e
 6028 0526 0E       		.uleb128 0xe
 6029 0527 1C       		.uleb128 0x1c
 6030 0528 06       		.uleb128 0x6
 6031 0529 00       		.byte	0
 6032 052a 00       		.byte	0
 6033 052b 5C       		.uleb128 0x5c
 6034 052c 34       		.uleb128 0x34
 6035 052d 00       		.byte	0
 6036 052e 47       		.uleb128 0x47
 6037 052f 13       		.uleb128 0x13
 6038 0530 6E       		.uleb128 0x6e
 6039 0531 0E       		.uleb128 0xe
 6040 0532 1C       		.uleb128 0x1c
 6041 0533 05       		.uleb128 0x5
 6042 0534 00       		.byte	0
 6043 0535 00       		.byte	0
 6044 0536 5D       		.uleb128 0x5d
 6045 0537 34       		.uleb128 0x34
 6046 0538 00       		.byte	0
 6047 0539 47       		.uleb128 0x47
 6048 053a 13       		.uleb128 0x13
 6049 053b 6E       		.uleb128 0x6e
 6050 053c 0E       		.uleb128 0xe
 6051 053d 1C       		.uleb128 0x1c
 6052 053e 07       		.uleb128 0x7
 6053 053f 00       		.byte	0
GAS LISTING /tmp/ccMbRvxK.s 			page 110


 6054 0540 00       		.byte	0
 6055 0541 00       		.byte	0
 6056              		.section	.debug_aranges,"",@progbits
 6057 0000 2C000000 		.long	0x2c
 6058 0004 0200     		.value	0x2
 6059 0006 00000000 		.long	.Ldebug_info0
 6060 000a 08       		.byte	0x8
 6061 000b 00       		.byte	0
 6062 000c 0000     		.value	0
 6063 000e 0000     		.value	0
 6064 0010 00000000 		.quad	.Ltext0
 6064      00000000 
 6065 0018 B0010000 		.quad	.Letext0-.Ltext0
 6065      00000000 
 6066 0020 00000000 		.quad	0
 6066      00000000 
 6067 0028 00000000 		.quad	0
 6067      00000000 
 6068              		.section	.debug_line,"",@progbits
 6069              	.Ldebug_line0:
 6070 0000 EE020000 		.section	.debug_str,"MS",@progbits,1
 6070      0200AB02 
 6070      00000101 
 6070      FB0E0D00 
 6070      01010101 
 6071              	.LASF364:
 6072 0000 67657465 		.string	"getenv"
 6072      6E7600
 6073              	.LASF308:
 6074 0007 75696E74 		.string	"uint_fast16_t"
 6074      5F666173 
 6074      7431365F 
 6074      7400
 6075              	.LASF261:
 6076 0015 6C6F6E67 		.string	"long int"
 6076      20696E74 
 6076      00
 6077              	.LASF32:
 6078 001e 5F5F6465 		.string	"__debug"
 6078      62756700 
 6079              	.LASF336:
 6080 0026 696E745F 		.string	"int_p_cs_precedes"
 6080      705F6373 
 6080      5F707265 
 6080      63656465 
 6080      7300
 6081              	.LASF302:
 6082 0038 75696E74 		.string	"uint_least64_t"
 6082      5F6C6561 
 6082      73743634 
 6082      5F7400
 6083              	.LASF382:
 6084 0047 73747274 		.string	"strtoull"
 6084      6F756C6C 
 6084      00
 6085              	.LASF263:
 6086 0050 77637378 		.string	"wcsxfrm"
GAS LISTING /tmp/ccMbRvxK.s 			page 111


 6086      66726D00 
 6087              	.LASF103:
 6088 0058 5F535F69 		.string	"_S_ios_seekdir_end"
 6088      6F735F73 
 6088      65656B64 
 6088      69725F65 
 6088      6E6400
 6089              	.LASF316:
 6090 006b 63686172 		.string	"char32_t"
 6090      33325F74 
 6090      00
 6091              	.LASF16:
 6092 0074 7E657863 		.string	"~exception_ptr"
 6092      65707469 
 6092      6F6E5F70 
 6092      747200
 6093              	.LASF129:
 6094 0083 676F6F64 		.string	"goodbit"
 6094      62697400 
 6095              	.LASF178:
 6096 008b 5F73686F 		.string	"_shortbuf"
 6096      72746275 
 6096      6600
 6097              	.LASF370:
 6098 0095 72616E64 		.string	"rand"
 6098      00
 6099              	.LASF184:
 6100 009a 5F5F7061 		.string	"__pad4"
 6100      643400
 6101              	.LASF463:
 6102 00a1 5F494F5F 		.string	"_IO_lock_t"
 6102      6C6F636B 
 6102      5F7400
 6103              	.LASF416:
 6104 00ac 73657476 		.string	"setvbuf"
 6104      62756600 
 6105              	.LASF190:
 6106 00b4 67705F6F 		.string	"gp_offset"
 6106      66667365 
 6106      7400
 6107              	.LASF412:
 6108 00be 72656D6F 		.string	"remove"
 6108      766500
 6109              	.LASF376:
 6110 00c5 73797374 		.string	"system"
 6110      656D00
 6111              	.LASF48:
 6112 00cc 61737369 		.string	"assign"
 6112      676E00
 6113              	.LASF245:
 6114 00d3 746D5F79 		.string	"tm_yday"
 6114      64617900 
 6115              	.LASF167:
 6116 00db 5F494F5F 		.string	"_IO_buf_end"
 6116      6275665F 
 6116      656E6400 
 6117              	.LASF53:
GAS LISTING /tmp/ccMbRvxK.s 			page 112


 6118 00e7 5F5A4E53 		.string	"_ZNSt11char_traitsIcE11to_int_typeERKc"
 6118      74313163 
 6118      6861725F 
 6118      74726169 
 6118      74734963 
 6119              	.LASF177:
 6120 010e 5F767461 		.string	"_vtable_offset"
 6120      626C655F 
 6120      6F666673 
 6120      657400
 6121              	.LASF95:
 6122 011d 5F535F66 		.string	"_S_failbit"
 6122      61696C62 
 6122      697400
 6123              	.LASF447:
 6124 0128 6E65775F 		.string	"new_delete.cpp"
 6124      64656C65 
 6124      74652E63 
 6124      707000
 6125              	.LASF410:
 6126 0137 67657473 		.string	"gets"
 6126      00
 6127              	.LASF83:
 6128 013c 5F535F61 		.string	"_S_ate"
 6128      746500
 6129              	.LASF398:
 6130 0143 66666C75 		.string	"fflush"
 6130      736800
 6131              	.LASF433:
 6132 014a 5F5A4E53 		.string	"_ZNSt17integral_constantIbLb0EE5valueE"
 6132      74313769 
 6132      6E746567 
 6132      72616C5F 
 6132      636F6E73 
 6133              	.LASF73:
 6134 0171 5F535F75 		.string	"_S_uppercase"
 6134      70706572 
 6134      63617365 
 6134      00
 6135              	.LASF295:
 6136 017e 696E745F 		.string	"int_least8_t"
 6136      6C656173 
 6136      74385F74 
 6136      00
 6137              	.LASF226:
 6138 018b 76667773 		.string	"vfwscanf"
 6138      63616E66 
 6138      00
 6139              	.LASF19:
 6140 0194 5F5A4E53 		.string	"_ZNSt15__exception_ptr13exception_ptr4swapERS0_"
 6140      7431355F 
 6140      5F657863 
 6140      65707469 
 6140      6F6E5F70 
 6141              	.LASF423:
 6142 01c4 746F7763 		.string	"towctrans"
 6142      7472616E 
GAS LISTING /tmp/ccMbRvxK.s 			page 113


 6142      7300
 6143              	.LASF165:
 6144 01ce 5F494F5F 		.string	"_IO_write_end"
 6144      77726974 
 6144      655F656E 
 6144      6400
 6145              	.LASF195:
 6146 01dc 756E7369 		.string	"unsigned int"
 6146      676E6564 
 6146      20696E74 
 6146      00
 6147              	.LASF138:
 6148 01e9 5F5F676E 		.string	"__gnu_cxx"
 6148      755F6378 
 6148      7800
 6149              	.LASF61:
 6150 01f3 5F535F66 		.string	"_S_fixed"
 6150      69786564 
 6150      00
 6151              	.LASF0:
 6152 01fc 5F5F6578 		.string	"__exception_ptr"
 6152      63657074 
 6152      696F6E5F 
 6152      70747200 
 6153              	.LASF88:
 6154 020c 5F535F69 		.string	"_S_ios_openmode_end"
 6154      6F735F6F 
 6154      70656E6D 
 6154      6F64655F 
 6154      656E6400 
 6155              	.LASF76:
 6156 0220 5F535F66 		.string	"_S_floatfield"
 6156      6C6F6174 
 6156      6669656C 
 6156      6400
 6157              	.LASF159:
 6158 022e 5F666C61 		.string	"_flags"
 6158      677300
 6159              	.LASF313:
 6160 0235 696E746D 		.string	"intmax_t"
 6160      61785F74 
 6160      00
 6161              	.LASF310:
 6162 023e 75696E74 		.string	"uint_fast64_t"
 6162      5F666173 
 6162      7436345F 
 6162      7400
 6163              	.LASF304:
 6164 024c 696E745F 		.string	"int_fast16_t"
 6164      66617374 
 6164      31365F74 
 6164      00
 6165              	.LASF345:
 6166 0259 5F5F696E 		.string	"__int32_t"
 6166      7433325F 
 6166      7400
 6167              	.LASF108:
GAS LISTING /tmp/ccMbRvxK.s 			page 114


 6168 0263 5F5A4E53 		.string	"_ZNSt8ios_base4InitD4Ev"
 6168      7438696F 
 6168      735F6261 
 6168      73653449 
 6168      6E697444 
 6169              	.LASF209:
 6170 027b 77636861 		.string	"wchar_t"
 6170      725F7400 
 6171              	.LASF104:
 6172 0283 5F535F72 		.string	"_S_refcount"
 6172      6566636F 
 6172      756E7400 
 6173              	.LASF362:
 6174 028f 61746F6C 		.string	"atol"
 6174      00
 6175              	.LASF4:
 6176 0294 5F5A4E53 		.string	"_ZNSt15__exception_ptr13exception_ptr10_M_releaseEv"
 6176      7431355F 
 6176      5F657863 
 6176      65707469 
 6176      6F6E5F70 
 6177              	.LASF230:
 6178 02c8 76777363 		.string	"vwscanf"
 6178      616E6600 
 6179              	.LASF322:
 6180 02d0 63757272 		.string	"currency_symbol"
 6180      656E6379 
 6180      5F73796D 
 6180      626F6C00 
 6181              	.LASF171:
 6182 02e0 5F6D6172 		.string	"_markers"
 6182      6B657273 
 6182      00
 6183              	.LASF58:
 6184 02e9 70747264 		.string	"ptrdiff_t"
 6184      6966665F 
 6184      7400
 6185              	.LASF228:
 6186 02f3 76737773 		.string	"vswscanf"
 6186      63616E66 
 6186      00
 6187              	.LASF37:
 6188 02fc 5F5A4E53 		.string	"_ZNSt11char_traitsIcE2ltERKcS2_"
 6188      74313163 
 6188      6861725F 
 6188      74726169 
 6188      74734963 
 6189              	.LASF329:
 6190 031c 66726163 		.string	"frac_digits"
 6190      5F646967 
 6190      69747300 
 6191              	.LASF315:
 6192 0328 63686172 		.string	"char16_t"
 6192      31365F74 
 6192      00
 6193              	.LASF323:
 6194 0331 6D6F6E5F 		.string	"mon_decimal_point"
GAS LISTING /tmp/ccMbRvxK.s 			page 115


 6194      64656369 
 6194      6D616C5F 
 6194      706F696E 
 6194      7400
 6195              	.LASF96:
 6196 0343 5F535F69 		.string	"_S_ios_iostate_end"
 6196      6F735F69 
 6196      6F737461 
 6196      74655F65 
 6196      6E6400
 6197              	.LASF78:
 6198 0356 5F535F69 		.string	"_S_ios_fmtflags_max"
 6198      6F735F66 
 6198      6D74666C 
 6198      6167735F 
 6198      6D617800 
 6199              	.LASF435:
 6200 036a 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIiE5__minE"
 6200      5F5F676E 
 6200      755F6378 
 6200      7832345F 
 6200      5F6E756D 
 6201              	.LASF22:
 6202 039c 6E756C6C 		.string	"nullptr_t"
 6202      7074725F 
 6202      7400
 6203              	.LASF139:
 6204 03a6 5F5F6F70 		.string	"__ops"
 6204      7300
 6205              	.LASF148:
 6206 03ac 5F5F6D61 		.string	"__max_digits10"
 6206      785F6469 
 6206      67697473 
 6206      313000
 6207              	.LASF419:
 6208 03bb 756E6765 		.string	"ungetc"
 6208      746300
 6209              	.LASF235:
 6210 03c2 77637363 		.string	"wcscpy"
 6210      707900
 6211              	.LASF25:
 6212 03c9 5F5A4E4B 		.string	"_ZNKSt17integral_constantIbLb0EEcvbEv"
 6212      53743137 
 6212      696E7465 
 6212      6772616C 
 6212      5F636F6E 
 6213              	.LASF455:
 6214 03ef 5F5A4E53 		.string	"_ZNSt11char_traitsIcE7not_eofERKi"
 6214      74313163 
 6214      6861725F 
 6214      74726169 
 6214      74734963 
 6215              	.LASF232:
 6216 0411 77637363 		.string	"wcscat"
 6216      617400
 6217              	.LASF437:
 6218 0418 5F5A4E39 		.string	"_ZN9__gnu_cxx25__numeric_traits_floatingIfE16__max_exponent10E"
GAS LISTING /tmp/ccMbRvxK.s 			page 116


 6218      5F5F676E 
 6218      755F6378 
 6218      7832355F 
 6218      5F6E756D 
 6219              	.LASF318:
 6220 0457 64656369 		.string	"decimal_point"
 6220      6D616C5F 
 6220      706F696E 
 6220      7400
 6221              	.LASF155:
 6222 0465 5F5F6E75 		.string	"__numeric_traits_integer<short int>"
 6222      6D657269 
 6222      635F7472 
 6222      61697473 
 6222      5F696E74 
 6223              	.LASF84:
 6224 0489 5F535F62 		.string	"_S_bin"
 6224      696E00
 6225              	.LASF333:
 6226 0490 6E5F7365 		.string	"n_sep_by_space"
 6226      705F6279 
 6226      5F737061 
 6226      636500
 6227              	.LASF387:
 6228 049f 5F5F7374 		.string	"__state"
 6228      61746500 
 6229              	.LASF243:
 6230 04a7 746D5F79 		.string	"tm_year"
 6230      65617200 
 6231              	.LASF46:
 6232 04af 636F7079 		.string	"copy"
 6232      00
 6233              	.LASF97:
 6234 04b4 5F535F69 		.string	"_S_ios_iostate_max"
 6234      6F735F69 
 6234      6F737461 
 6234      74655F6D 
 6234      617800
 6235              	.LASF29:
 6236 04c7 6F706572 		.string	"operator std::integral_constant<bool, true>::value_type"
 6236      61746F72 
 6236      20737464 
 6236      3A3A696E 
 6236      74656772 
 6237              	.LASF306:
 6238 04ff 696E745F 		.string	"int_fast64_t"
 6238      66617374 
 6238      36345F74 
 6238      00
 6239              	.LASF286:
 6240 050c 5F5F676E 		.string	"__gnu_debug"
 6240      755F6465 
 6240      62756700 
 6241              	.LASF64:
 6242 0518 5F535F6C 		.string	"_S_left"
 6242      65667400 
 6243              	.LASF214:
GAS LISTING /tmp/ccMbRvxK.s 			page 117


 6244 0520 66777363 		.string	"fwscanf"
 6244      616E6600 
 6245              	.LASF381:
 6246 0528 73747274 		.string	"strtoll"
 6246      6F6C6C00 
 6247              	.LASF300:
 6248 0530 75696E74 		.string	"uint_least16_t"
 6248      5F6C6561 
 6248      73743136 
 6248      5F7400
 6249              	.LASF293:
 6250 053f 75696E74 		.string	"uint32_t"
 6250      33325F74 
 6250      00
 6251              	.LASF287:
 6252 0548 696E7438 		.string	"int8_t"
 6252      5F7400
 6253              	.LASF331:
 6254 054f 705F7365 		.string	"p_sep_by_space"
 6254      705F6279 
 6254      5F737061 
 6254      636500
 6255              	.LASF368:
 6256 055e 6D62746F 		.string	"mbtowc"
 6256      776300
 6257              	.LASF432:
 6258 0565 5F5F6473 		.string	"__dso_handle"
 6258      6F5F6861 
 6258      6E646C65 
 6258      00
 6259              	.LASF393:
 6260 0572 66706F73 		.string	"fpos_t"
 6260      5F7400
 6261              	.LASF6:
 6262 0579 5F4D5F67 		.string	"_M_get"
 6262      657400
 6263              	.LASF200:
 6264 0580 5F5F636F 		.string	"__count"
 6264      756E7400 
 6265              	.LASF154:
 6266 0588 5F5F6E75 		.string	"__numeric_traits_integer<char>"
 6266      6D657269 
 6266      635F7472 
 6266      61697473 
 6266      5F696E74 
 6267              	.LASF258:
 6268 05a7 666C6F61 		.string	"float"
 6268      7400
 6269              	.LASF242:
 6270 05ad 746D5F6D 		.string	"tm_mon"
 6270      6F6E00
 6271              	.LASF176:
 6272 05b4 5F637572 		.string	"_cur_column"
 6272      5F636F6C 
 6272      756D6E00 
 6273              	.LASF400:
 6274 05c0 66676574 		.string	"fgetpos"
GAS LISTING /tmp/ccMbRvxK.s 			page 118


 6274      706F7300 
 6275              	.LASF136:
 6276 05c8 5F436861 		.string	"_CharT"
 6276      725400
 6277              	.LASF288:
 6278 05cf 696E7431 		.string	"int16_t"
 6278      365F7400 
 6279              	.LASF229:
 6280 05d7 76777072 		.string	"vwprintf"
 6280      696E7466 
 6280      00
 6281              	.LASF146:
 6282 05e0 5F5A4E39 		.string	"_ZN9__gnu_cxx3divExx"
 6282      5F5F676E 
 6282      755F6378 
 6282      78336469 
 6282      76457878 
 6283              	.LASF296:
 6284 05f5 696E745F 		.string	"int_least16_t"
 6284      6C656173 
 6284      7431365F 
 6284      7400
 6285              	.LASF314:
 6286 0603 75696E74 		.string	"uintmax_t"
 6286      6D61785F 
 6286      7400
 6287              	.LASF215:
 6288 060d 67657477 		.string	"getwc"
 6288      6300
 6289              	.LASF281:
 6290 0613 6C6F6E67 		.string	"long long unsigned int"
 6290      206C6F6E 
 6290      6720756E 
 6290      7369676E 
 6290      65642069 
 6291              	.LASF102:
 6292 062a 5F535F65 		.string	"_S_end"
 6292      6E6400
 6293              	.LASF12:
 6294 0631 5F5A4E53 		.string	"_ZNSt15__exception_ptr13exception_ptrC4EOS0_"
 6294      7431355F 
 6294      5F657863 
 6294      65707469 
 6294      6F6E5F70 
 6295              	.LASF152:
 6296 065e 5F5F6E75 		.string	"__numeric_traits_floating<long double>"
 6296      6D657269 
 6296      635F7472 
 6296      61697473 
 6296      5F666C6F 
 6297              	.LASF262:
 6298 0685 77637374 		.string	"wcstoul"
 6298      6F756C00 
 6299              	.LASF60:
 6300 068d 5F535F64 		.string	"_S_dec"
 6300      656300
 6301              	.LASF62:
GAS LISTING /tmp/ccMbRvxK.s 			page 119


 6302 0694 5F535F68 		.string	"_S_hex"
 6302      657800
 6303              	.LASF341:
 6304 069b 696E745F 		.string	"int_n_sign_posn"
 6304      6E5F7369 
 6304      676E5F70 
 6304      6F736E00 
 6305              	.LASF74:
 6306 06ab 5F535F61 		.string	"_S_adjustfield"
 6306      646A7573 
 6306      74666965 
 6306      6C6400
 6307              	.LASF378:
 6308 06ba 7763746F 		.string	"wctomb"
 6308      6D6200
 6309              	.LASF119:
 6310 06c1 756E6974 		.string	"unitbuf"
 6310      62756600 
 6311              	.LASF344:
 6312 06c9 6C6F6361 		.string	"localeconv"
 6312      6C65636F 
 6312      6E7600
 6313              	.LASF188:
 6314 06d4 5F5F4649 		.string	"__FILE"
 6314      4C4500
 6315              	.LASF59:
 6316 06db 5F535F62 		.string	"_S_boolalpha"
 6316      6F6F6C61 
 6316      6C706861 
 6316      00
 6317              	.LASF269:
 6318 06e8 77707269 		.string	"wprintf"
 6318      6E746600 
 6319              	.LASF121:
 6320 06f0 61646A75 		.string	"adjustfield"
 6320      73746669 
 6320      656C6400 
 6321              	.LASF180:
 6322 06fc 5F6F6666 		.string	"_offset"
 6322      73657400 
 6323              	.LASF52:
 6324 0704 746F5F69 		.string	"to_int_type"
 6324      6E745F74 
 6324      79706500 
 6325              	.LASF231:
 6326 0710 77637274 		.string	"wcrtomb"
 6326      6F6D6200 
 6327              	.LASF460:
 6328 0718 5F5A5374 		.string	"_ZSt4cout"
 6328      34636F75 
 6328      7400
 6329              	.LASF158:
 6330 0722 5F4D5F65 		.string	"_M_exception_object"
 6330      78636570 
 6330      74696F6E 
 6330      5F6F626A 
 6330      65637400 
GAS LISTING /tmp/ccMbRvxK.s 			page 120


 6331              	.LASF379:
 6332 0736 6C6C6469 		.string	"lldiv"
 6332      7600
 6333              	.LASF434:
 6334 073c 5F5A4E53 		.string	"_ZNSt17integral_constantIbLb1EE5valueE"
 6334      74313769 
 6334      6E746567 
 6334      72616C5F 
 6334      636F6E73 
 6335              	.LASF9:
 6336 0763 5F5A4E53 		.string	"_ZNSt15__exception_ptr13exception_ptrC4Ev"
 6336      7431355F 
 6336      5F657863 
 6336      65707469 
 6336      6F6E5F70 
 6337              	.LASF125:
 6338 078d 696F7374 		.string	"iostate"
 6338      61746500 
 6339              	.LASF28:
 6340 0795 76616C75 		.string	"value"
 6340      6500
 6341              	.LASF173:
 6342 079b 5F66696C 		.string	"_fileno"
 6342      656E6F00 
 6343              	.LASF225:
 6344 07a3 76667770 		.string	"vfwprintf"
 6344      72696E74 
 6344      6600
 6345              	.LASF183:
 6346 07ad 5F5F7061 		.string	"__pad3"
 6346      643300
 6347              	.LASF110:
 6348 07b4 66697865 		.string	"fixed"
 6348      6400
 6349              	.LASF402:
 6350 07ba 666F7065 		.string	"fopen"
 6350      6E00
 6351              	.LASF334:
 6352 07c0 705F7369 		.string	"p_sign_posn"
 6352      676E5F70 
 6352      6F736E00 
 6353              	.LASF430:
 6354 07cc 5F5F696E 		.string	"__initialize_p"
 6354      69746961 
 6354      6C697A65 
 6354      5F7000
 6355              	.LASF120:
 6356 07db 75707065 		.string	"uppercase"
 6356      72636173 
 6356      6500
 6357              	.LASF57:
 6358 07e5 73697A65 		.string	"size_t"
 6358      5F7400
 6359              	.LASF44:
 6360 07ec 6D6F7665 		.string	"move"
 6360      00
 6361              	.LASF147:
GAS LISTING /tmp/ccMbRvxK.s 			page 121


 6362 07f1 5F5F6E75 		.string	"__numeric_traits_floating<float>"
 6362      6D657269 
 6362      635F7472 
 6362      61697473 
 6362      5F666C6F 
 6363              	.LASF123:
 6364 0812 666C6F61 		.string	"floatfield"
 6364      74666965 
 6364      6C6400
 6365              	.LASF290:
 6366 081d 696E7436 		.string	"int64_t"
 6366      345F7400 
 6367              	.LASF298:
 6368 0825 696E745F 		.string	"int_least64_t"
 6368      6C656173 
 6368      7436345F 
 6368      7400
 6369              	.LASF151:
 6370 0833 5F5F6E75 		.string	"__numeric_traits_floating<double>"
 6370      6D657269 
 6370      635F7472 
 6370      61697473 
 6370      5F666C6F 
 6371              	.LASF117:
 6372 0855 73686F77 		.string	"showpos"
 6372      706F7300 
 6373              	.LASF438:
 6374 085d 5F5A4E39 		.string	"_ZN9__gnu_cxx25__numeric_traits_floatingIdE16__max_exponent10E"
 6374      5F5F676E 
 6374      755F6378 
 6374      7832355F 
 6374      5F6E756D 
 6375              	.LASF203:
 6376 089c 5F5F6D62 		.string	"__mbstate_t"
 6376      73746174 
 6376      655F7400 
 6377              	.LASF299:
 6378 08a8 75696E74 		.string	"uint_least8_t"
 6378      5F6C6561 
 6378      7374385F 
 6378      7400
 6379              	.LASF162:
 6380 08b6 5F494F5F 		.string	"_IO_read_base"
 6380      72656164 
 6380      5F626173 
 6380      6500
 6381              	.LASF101:
 6382 08c4 5F535F63 		.string	"_S_cur"
 6382      757200
 6383              	.LASF431:
 6384 08cb 5F5F7072 		.string	"__priority"
 6384      696F7269 
 6384      747900
 6385              	.LASF90:
 6386 08d6 5F535F69 		.string	"_S_ios_openmode_min"
 6386      6F735F6F 
 6386      70656E6D 
GAS LISTING /tmp/ccMbRvxK.s 			page 122


 6386      6F64655F 
 6386      6D696E00 
 6387              	.LASF363:
 6388 08ea 62736561 		.string	"bsearch"
 6388      72636800 
 6389              	.LASF170:
 6390 08f2 5F494F5F 		.string	"_IO_save_end"
 6390      73617665 
 6390      5F656E64 
 6390      00
 6391              	.LASF390:
 6392 08ff 5F6E6578 		.string	"_next"
 6392      7400
 6393              	.LASF328:
 6394 0905 696E745F 		.string	"int_frac_digits"
 6394      66726163 
 6394      5F646967 
 6394      69747300 
 6395              	.LASF394:
 6396 0915 636C6561 		.string	"clearerr"
 6396      72657272 
 6396      00
 6397              	.LASF274:
 6398 091e 77637373 		.string	"wcsstr"
 6398      747200
 6399              	.LASF212:
 6400 0925 66776964 		.string	"fwide"
 6400      6500
 6401              	.LASF338:
 6402 092b 696E745F 		.string	"int_n_cs_precedes"
 6402      6E5F6373 
 6402      5F707265 
 6402      63656465 
 6402      7300
 6403              	.LASF66:
 6404 093d 5F535F72 		.string	"_S_right"
 6404      69676874 
 6404      00
 6405              	.LASF69:
 6406 0946 5F535F73 		.string	"_S_showpoint"
 6406      686F7770 
 6406      6F696E74 
 6406      00
 6407              	.LASF42:
 6408 0953 66696E64 		.string	"find"
 6408      00
 6409              	.LASF135:
 6410 0958 62617369 		.string	"basic_ostream<char, std::char_traits<char> >"
 6410      635F6F73 
 6410      74726561 
 6410      6D3C6368 
 6410      61722C20 
 6411              	.LASF327:
 6412 0985 6E656761 		.string	"negative_sign"
 6412      74697665 
 6412      5F736967 
 6412      6E00
GAS LISTING /tmp/ccMbRvxK.s 			page 123


 6413              	.LASF404:
 6414 0993 6672656F 		.string	"freopen"
 6414      70656E00 
 6415              	.LASF428:
 6416 099b 61726776 		.string	"argv"
 6416      00
 6417              	.LASF201:
 6418 09a0 5F5F7661 		.string	"__value"
 6418      6C756500 
 6419              	.LASF452:
 6420 09a8 70696563 		.string	"piecewise_construct_t"
 6420      65776973 
 6420      655F636F 
 6420      6E737472 
 6420      7563745F 
 6421              	.LASF320:
 6422 09be 67726F75 		.string	"grouping"
 6422      70696E67 
 6422      00
 6423              	.LASF270:
 6424 09c7 77736361 		.string	"wscanf"
 6424      6E6600
 6425              	.LASF68:
 6426 09ce 5F535F73 		.string	"_S_showbase"
 6426      686F7762 
 6426      61736500 
 6427              	.LASF7:
 6428 09da 5F5A4E53 		.string	"_ZNSt15__exception_ptr13exception_ptrC4EPv"
 6428      7431355F 
 6428      5F657863 
 6428      65707469 
 6428      6F6E5F70 
 6429              	.LASF465:
 6430 0a05 5F474C4F 		.string	"_GLOBAL__sub_I_main"
 6430      42414C5F 
 6430      5F737562 
 6430      5F495F6D 
 6430      61696E00 
 6431              	.LASF248:
 6432 0a19 746D5F7A 		.string	"tm_zone"
 6432      6F6E6500 
 6433              	.LASF85:
 6434 0a21 5F535F69 		.string	"_S_in"
 6434      6E00
 6435              	.LASF462:
 6436 0a27 6465636C 		.string	"decltype(nullptr)"
 6436      74797065 
 6436      286E756C 
 6436      6C707472 
 6436      2900
 6437              	.LASF116:
 6438 0a39 73686F77 		.string	"showpoint"
 6438      706F696E 
 6438      7400
 6439              	.LASF202:
 6440 0a43 63686172 		.string	"char"
 6440      00
GAS LISTING /tmp/ccMbRvxK.s 			page 124


 6441              	.LASF186:
 6442 0a48 5F6D6F64 		.string	"_mode"
 6442      6500
 6443              	.LASF350:
 6444 0a4e 35646976 		.string	"5div_t"
 6444      5F7400
 6445              	.LASF223:
 6446 0a55 73777363 		.string	"swscanf"
 6446      616E6600 
 6447              	.LASF397:
 6448 0a5d 66657272 		.string	"ferror"
 6448      6F7200
 6449              	.LASF389:
 6450 0a64 5F494F5F 		.string	"_IO_marker"
 6450      6D61726B 
 6450      657200
 6451              	.LASF35:
 6452 0a6f 696E745F 		.string	"int_type"
 6452      74797065 
 6452      00
 6453              	.LASF163:
 6454 0a78 5F494F5F 		.string	"_IO_write_base"
 6454      77726974 
 6454      655F6261 
 6454      736500
 6455              	.LASF160:
 6456 0a87 5F494F5F 		.string	"_IO_read_ptr"
 6456      72656164 
 6456      5F707472 
 6456      00
 6457              	.LASF425:
 6458 0a94 77637479 		.string	"wctype"
 6458      706500
 6459              	.LASF98:
 6460 0a9b 5F535F69 		.string	"_S_ios_iostate_min"
 6460      6F735F69 
 6460      6F737461 
 6460      74655F6D 
 6460      696E00
 6461              	.LASF149:
 6462 0aae 5F5F6469 		.string	"__digits10"
 6462      67697473 
 6462      313000
 6463              	.LASF279:
 6464 0ab9 6C6F6E67 		.string	"long long int"
 6464      206C6F6E 
 6464      6720696E 
 6464      7400
 6465              	.LASF142:
 6466 0ac7 5F5F6D61 		.string	"__max"
 6466      7800
 6467              	.LASF369:
 6468 0acd 71756963 		.string	"quick_exit"
 6468      6B5F6578 
 6468      697400
 6469              	.LASF198:
 6470 0ad8 5F5F7763 		.string	"__wch"
GAS LISTING /tmp/ccMbRvxK.s 			page 125


 6470      6800
 6471              	.LASF461:
 6472 0ade 5F5F6E75 		.string	"__numeric_traits_integer<long int>"
 6472      6D657269 
 6472      635F7472 
 6472      61697473 
 6472      5F696E74 
 6473              	.LASF291:
 6474 0b01 75696E74 		.string	"uint8_t"
 6474      385F7400 
 6475              	.LASF17:
 6476 0b09 5F5A4E53 		.string	"_ZNSt15__exception_ptr13exception_ptrD4Ev"
 6476      7431355F 
 6476      5F657863 
 6476      65707469 
 6476      6F6E5F70 
 6477              	.LASF351:
 6478 0b33 71756F74 		.string	"quot"
 6478      00
 6479              	.LASF219:
 6480 0b38 6D627372 		.string	"mbsrtowcs"
 6480      746F7763 
 6480      7300
 6481              	.LASF100:
 6482 0b42 5F535F62 		.string	"_S_beg"
 6482      656700
 6483              	.LASF413:
 6484 0b49 72656E61 		.string	"rename"
 6484      6D6500
 6485              	.LASF386:
 6486 0b50 5F5F706F 		.string	"__pos"
 6486      7300
 6487              	.LASF421:
 6488 0b56 77637472 		.string	"wctrans_t"
 6488      616E735F 
 6488      7400
 6489              	.LASF445:
 6490 0b60 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIlE5__maxE"
 6490      5F5F676E 
 6490      755F6378 
 6490      7832345F 
 6490      5F6E756D 
 6491              	.LASF367:
 6492 0b92 6D627374 		.string	"mbstowcs"
 6492      6F776373 
 6492      00
 6493              	.LASF113:
 6494 0b9b 72696768 		.string	"right"
 6494      7400
 6495              	.LASF18:
 6496 0ba1 73776170 		.string	"swap"
 6496      00
 6497              	.LASF5:
 6498 0ba6 65786365 		.string	"exception_ptr"
 6498      7074696F 
 6498      6E5F7074 
 6498      7200
GAS LISTING /tmp/ccMbRvxK.s 			page 126


 6499              	.LASF257:
 6500 0bb4 77637374 		.string	"wcstof"
 6500      6F6600
 6501              	.LASF254:
 6502 0bbb 77637373 		.string	"wcsspn"
 6502      706E00
 6503              	.LASF443:
 6504 0bc2 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIsE5__maxE"
 6504      5F5F676E 
 6504      755F6378 
 6504      7832345F 
 6504      5F6E756D 
 6505              	.LASF253:
 6506 0bf4 77637372 		.string	"wcsrtombs"
 6506      746F6D62 
 6506      7300
 6507              	.LASF21:
 6508 0bfe 5F5A4E4B 		.string	"_ZNKSt15__exception_ptr13exception_ptr20__cxa_exception_typeEv"
 6508      53743135 
 6508      5F5F6578 
 6508      63657074 
 6508      696F6E5F 
 6509              	.LASF448:
 6510 0c3d 2F686F6D 		.string	"/home/vinu/cs4230/par-lang/c++/discover_modern_c++/basics"
 6510      652F7669 
 6510      6E752F63 
 6510      73343233 
 6510      302F7061 
 6511              	.LASF415:
 6512 0c77 73657462 		.string	"setbuf"
 6512      756600
 6513              	.LASF411:
 6514 0c7e 70657272 		.string	"perror"
 6514      6F7200
 6515              	.LASF459:
 6516 0c85 636F7574 		.string	"cout"
 6516      00
 6517              	.LASF114:
 6518 0c8a 73636965 		.string	"scientific"
 6518      6E746966 
 6518      696300
 6519              	.LASF65:
 6520 0c95 5F535F6F 		.string	"_S_oct"
 6520      637400
 6521              	.LASF93:
 6522 0c9c 5F535F62 		.string	"_S_badbit"
 6522      61646269 
 6522      7400
 6523              	.LASF325:
 6524 0ca6 6D6F6E5F 		.string	"mon_grouping"
 6524      67726F75 
 6524      70696E67 
 6524      00
 6525              	.LASF280:
 6526 0cb3 77637374 		.string	"wcstoull"
 6526      6F756C6C 
 6526      00
GAS LISTING /tmp/ccMbRvxK.s 			page 127


 6527              	.LASF453:
 6528 0cbc 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6assignERcRKc"
 6528      74313163 
 6528      6861725F 
 6528      74726169 
 6528      74734963 
 6529              	.LASF456:
 6530 0cdf 5F5A4E53 		.string	"_ZNSt8ios_base4InitC4Ev"
 6530      7438696F 
 6530      735F6261 
 6530      73653449 
 6530      6E697443 
 6531              	.LASF282:
 6532 0cf7 626F6F6C 		.string	"bool"
 6532      00
 6533              	.LASF31:
 6534 0cfc 5F5F6378 		.string	"__cxx11"
 6534      78313100 
 6535              	.LASF109:
 6536 0d04 626F6F6C 		.string	"boolalpha"
 6536      616C7068 
 6536      6100
 6537              	.LASF330:
 6538 0d0e 705F6373 		.string	"p_cs_precedes"
 6538      5F707265 
 6538      63656465 
 6538      7300
 6539              	.LASF30:
 6540 0d1c 5F5A4E4B 		.string	"_ZNKSt17integral_constantIbLb1EEcvbEv"
 6540      53743137 
 6540      696E7465 
 6540      6772616C 
 6540      5F636F6E 
 6541              	.LASF34:
 6542 0d42 63686172 		.string	"char_type"
 6542      5F747970 
 6542      6500
 6543              	.LASF128:
 6544 0d4c 6661696C 		.string	"failbit"
 6544      62697400 
 6545              	.LASF303:
 6546 0d54 696E745F 		.string	"int_fast8_t"
 6546      66617374 
 6546      385F7400 
 6547              	.LASF405:
 6548 0d60 66736565 		.string	"fseek"
 6548      6B00
 6549              	.LASF365:
 6550 0d66 6C646976 		.string	"ldiv"
 6550      00
 6551              	.LASF388:
 6552 0d6b 5F475F66 		.string	"_G_fpos_t"
 6552      706F735F 
 6552      7400
 6553              	.LASF208:
 6554 0d75 66676574 		.string	"fgetws"
 6554      777300
GAS LISTING /tmp/ccMbRvxK.s 			page 128


 6555              	.LASF75:
 6556 0d7c 5F535F62 		.string	"_S_basefield"
 6556      61736566 
 6556      69656C64 
 6556      00
 6557              	.LASF458:
 6558 0d89 70696563 		.string	"piecewise_construct"
 6558      65776973 
 6558      655F636F 
 6558      6E737472 
 6558      75637400 
 6559              	.LASF13:
 6560 0d9d 6F706572 		.string	"operator="
 6560      61746F72 
 6560      3D00
 6561              	.LASF126:
 6562 0da7 62616462 		.string	"badbit"
 6562      697400
 6563              	.LASF372:
 6564 0dae 7372616E 		.string	"srand"
 6564      6400
 6565              	.LASF277:
 6566 0db4 6C6F6E67 		.string	"long double"
 6566      20646F75 
 6566      626C6500 
 6567              	.LASF24:
 6568 0dc0 6F706572 		.string	"operator std::integral_constant<bool, false>::value_type"
 6568      61746F72 
 6568      20737464 
 6568      3A3A696E 
 6568      74656772 
 6569              	.LASF169:
 6570 0df9 5F494F5F 		.string	"_IO_backup_base"
 6570      6261636B 
 6570      75705F62 
 6570      61736500 
 6571              	.LASF406:
 6572 0e09 66736574 		.string	"fsetpos"
 6572      706F7300 
 6573              	.LASF92:
 6574 0e11 5F535F67 		.string	"_S_goodbit"
 6574      6F6F6462 
 6574      697400
 6575              	.LASF67:
 6576 0e1c 5F535F73 		.string	"_S_scientific"
 6576      6369656E 
 6576      74696669 
 6576      6300
 6577              	.LASF309:
 6578 0e2a 75696E74 		.string	"uint_fast32_t"
 6578      5F666173 
 6578      7433325F 
 6578      7400
 6579              	.LASF181:
 6580 0e38 5F5F7061 		.string	"__pad1"
 6580      643100
 6581              	.LASF407:
GAS LISTING /tmp/ccMbRvxK.s 			page 129


 6582 0e3f 6674656C 		.string	"ftell"
 6582      6C00
 6583              	.LASF185:
 6584 0e45 5F5F7061 		.string	"__pad5"
 6584      643500
 6585              	.LASF196:
 6586 0e4c 6C6F6E67 		.string	"long unsigned int"
 6586      20756E73 
 6586      69676E65 
 6586      6420696E 
 6586      7400
 6587              	.LASF143:
 6588 0e5e 5F5F6973 		.string	"__is_signed"
 6588      5F736967 
 6588      6E656400 
 6589              	.LASF224:
 6590 0e6a 756E6765 		.string	"ungetwc"
 6590      74776300 
 6591              	.LASF124:
 6592 0e72 666D7466 		.string	"fmtflags"
 6592      6C616773 
 6592      00
 6593              	.LASF145:
 6594 0e7b 5F56616C 		.string	"_Value"
 6594      756500
 6595              	.LASF94:
 6596 0e82 5F535F65 		.string	"_S_eofbit"
 6596      6F666269 
 6596      7400
 6597              	.LASF399:
 6598 0e8c 66676574 		.string	"fgetc"
 6598      6300
 6599              	.LASF420:
 6600 0e92 77637479 		.string	"wctype_t"
 6600      70655F74 
 6600      00
 6601              	.LASF427:
 6602 0e9b 61726763 		.string	"argc"
 6602      00
 6603              	.LASF414:
 6604 0ea0 72657769 		.string	"rewind"
 6604      6E6400
 6605              	.LASF241:
 6606 0ea7 746D5F6D 		.string	"tm_mday"
 6606      64617900 
 6607              	.LASF233:
 6608 0eaf 77637363 		.string	"wcscmp"
 6608      6D7000
 6609              	.LASF401:
 6610 0eb6 66676574 		.string	"fgets"
 6610      7300
 6611              	.LASF436:
 6612 0ebc 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIiE5__maxE"
 6612      5F5F676E 
 6612      755F6378 
 6612      7832345F 
 6612      5F6E756D 
GAS LISTING /tmp/ccMbRvxK.s 			page 130


 6613              	.LASF271:
 6614 0eee 77637363 		.string	"wcschr"
 6614      687200
 6615              	.LASF418:
 6616 0ef5 746D706E 		.string	"tmpnam"
 6616      616D00
 6617              	.LASF210:
 6618 0efc 66707574 		.string	"fputwc"
 6618      776300
 6619              	.LASF311:
 6620 0f03 696E7470 		.string	"intptr_t"
 6620      74725F74 
 6620      00
 6621              	.LASF27:
 6622 0f0c 696E7465 		.string	"integral_constant<bool, true>"
 6622      6772616C 
 6622      5F636F6E 
 6622      7374616E 
 6622      743C626F 
 6623              	.LASF292:
 6624 0f2a 75696E74 		.string	"uint16_t"
 6624      31365F74 
 6624      00
 6625              	.LASF130:
 6626 0f33 6F70656E 		.string	"openmode"
 6626      6D6F6465 
 6626      00
 6627              	.LASF234:
 6628 0f3c 77637363 		.string	"wcscoll"
 6628      6F6C6C00 
 6629              	.LASF426:
 6630 0f44 6D61696E 		.string	"main"
 6630      00
 6631              	.LASF105:
 6632 0f49 5F535F73 		.string	"_S_synced_with_stdio"
 6632      796E6365 
 6632      645F7769 
 6632      74685F73 
 6632      7464696F 
 6633              	.LASF127:
 6634 0f5e 656F6662 		.string	"eofbit"
 6634      697400
 6635              	.LASF211:
 6636 0f65 66707574 		.string	"fputws"
 6636      777300
 6637              	.LASF45:
 6638 0f6c 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4moveEPcPKcm"
 6638      74313163 
 6638      6861725F 
 6638      74726169 
 6638      74734963 
 6639              	.LASF71:
 6640 0f8e 5F535F73 		.string	"_S_skipws"
 6640      6B697077 
 6640      7300
 6641              	.LASF464:
 6642 0f98 5F5F7374 		.string	"__static_initialization_and_destruction_0"
GAS LISTING /tmp/ccMbRvxK.s 			page 131


 6642      61746963 
 6642      5F696E69 
 6642      7469616C 
 6642      697A6174 
 6643              	.LASF134:
 6644 0fc2 696F735F 		.string	"ios_base"
 6644      62617365 
 6644      00
 6645              	.LASF220:
 6646 0fcb 70757477 		.string	"putwc"
 6646      6300
 6647              	.LASF206:
 6648 0fd1 62746F77 		.string	"btowc"
 6648      6300
 6649              	.LASF283:
 6650 0fd7 756E7369 		.string	"unsigned char"
 6650      676E6564 
 6650      20636861 
 6650      7200
 6651              	.LASF38:
 6652 0fe5 636F6D70 		.string	"compare"
 6652      61726500 
 6653              	.LASF395:
 6654 0fed 66636C6F 		.string	"fclose"
 6654      736500
 6655              	.LASF168:
 6656 0ff4 5F494F5F 		.string	"_IO_save_base"
 6656      73617665 
 6656      5F626173 
 6656      6500
 6657              	.LASF305:
 6658 1002 696E745F 		.string	"int_fast32_t"
 6658      66617374 
 6658      33325F74 
 6658      00
 6659              	.LASF217:
 6660 100f 6D627274 		.string	"mbrtowc"
 6660      6F776300 
 6661              	.LASF161:
 6662 1017 5F494F5F 		.string	"_IO_read_end"
 6662      72656164 
 6662      5F656E64 
 6662      00
 6663              	.LASF422:
 6664 1024 69737763 		.string	"iswctype"
 6664      74797065 
 6664      00
 6665              	.LASF218:
 6666 102d 6D627369 		.string	"mbsinit"
 6666      6E697400 
 6667              	.LASF275:
 6668 1035 776D656D 		.string	"wmemchr"
 6668      63687200 
 6669              	.LASF446:
 6670 103d 474E5520 		.string	"GNU C++11 5.4.1 20160904 -mtune=generic -march=x86-64 -g -O0 -std=c++11 -fstack-protector
 6670      432B2B31 
 6670      3120352E 
GAS LISTING /tmp/ccMbRvxK.s 			page 132


 6670      342E3120 
 6670      32303136 
 6671              	.LASF285:
 6672 109e 73686F72 		.string	"short int"
 6672      7420696E 
 6672      7400
 6673              	.LASF454:
 6674 10a8 5F5A4E53 		.string	"_ZNSt11char_traitsIcE3eofEv"
 6674      74313163 
 6674      6861725F 
 6674      74726169 
 6674      74734963 
 6675              	.LASF266:
 6676 10c4 776D656D 		.string	"wmemcpy"
 6676      63707900 
 6677              	.LASF79:
 6678 10cc 5F535F69 		.string	"_S_ios_fmtflags_min"
 6678      6F735F66 
 6678      6D74666C 
 6678      6167735F 
 6678      6D696E00 
 6679              	.LASF107:
 6680 10e0 7E496E69 		.string	"~Init"
 6680      7400
 6681              	.LASF321:
 6682 10e6 696E745F 		.string	"int_curr_symbol"
 6682      63757272 
 6682      5F73796D 
 6682      626F6C00 
 6683              	.LASF144:
 6684 10f6 5F5F6469 		.string	"__digits"
 6684      67697473 
 6684      00
 6685              	.LASF326:
 6686 10ff 706F7369 		.string	"positive_sign"
 6686      74697665 
 6686      5F736967 
 6686      6E00
 6687              	.LASF20:
 6688 110d 5F5F6378 		.string	"__cxa_exception_type"
 6688      615F6578 
 6688      63657074 
 6688      696F6E5F 
 6688      74797065 
 6689              	.LASF86:
 6690 1122 5F535F6F 		.string	"_S_out"
 6690      757400
 6691              	.LASF216:
 6692 1129 6D62726C 		.string	"mbrlen"
 6692      656E00
 6693              	.LASF392:
 6694 1130 5F706F73 		.string	"_pos"
 6694      00
 6695              	.LASF39:
 6696 1135 6C656E67 		.string	"length"
 6696      746800
 6697              	.LASF403:
GAS LISTING /tmp/ccMbRvxK.s 			page 133


 6698 113c 66726561 		.string	"fread"
 6698      6400
 6699              	.LASF451:
 6700 1142 74797065 		.string	"type_info"
 6700      5F696E66 
 6700      6F00
 6701              	.LASF335:
 6702 114c 6E5F7369 		.string	"n_sign_posn"
 6702      676E5F70 
 6702      6F736E00 
 6703              	.LASF15:
 6704 1158 5F5A4E53 		.string	"_ZNSt15__exception_ptr13exception_ptraSEOS0_"
 6704      7431355F 
 6704      5F657863 
 6704      65707469 
 6704      6F6E5F70 
 6705              	.LASF349:
 6706 1185 31315F5F 		.string	"11__mbstate_t"
 6706      6D627374 
 6706      6174655F 
 6706      7400
 6707              	.LASF358:
 6708 1193 61746578 		.string	"atexit"
 6708      697400
 6709              	.LASF33:
 6710 119a 63686172 		.string	"char_traits<char>"
 6710      5F747261 
 6710      6974733C 
 6710      63686172 
 6710      3E00
 6711              	.LASF91:
 6712 11ac 5F496F73 		.string	"_Ios_Iostate"
 6712      5F496F73 
 6712      74617465 
 6712      00
 6713              	.LASF221:
 6714 11b9 70757477 		.string	"putwchar"
 6714      63686172 
 6714      00
 6715              	.LASF81:
 6716 11c2 5F496F73 		.string	"_Ios_Openmode"
 6716      5F4F7065 
 6716      6E6D6F64 
 6716      6500
 6717              	.LASF273:
 6718 11d0 77637372 		.string	"wcsrchr"
 6718      63687200 
 6719              	.LASF150:
 6720 11d8 5F5F6D61 		.string	"__max_exponent10"
 6720      785F6578 
 6720      706F6E65 
 6720      6E743130 
 6720      00
 6721              	.LASF50:
 6722 11e9 746F5F63 		.string	"to_char_type"
 6722      6861725F 
 6722      74797065 
GAS LISTING /tmp/ccMbRvxK.s 			page 134


 6722      00
 6723              	.LASF343:
 6724 11f6 67657477 		.string	"getwchar"
 6724      63686172 
 6724      00
 6725              	.LASF63:
 6726 11ff 5F535F69 		.string	"_S_internal"
 6726      6E746572 
 6726      6E616C00 
 6727              	.LASF199:
 6728 120b 5F5F7763 		.string	"__wchb"
 6728      686200
 6729              	.LASF294:
 6730 1212 75696E74 		.string	"uint64_t"
 6730      36345F74 
 6730      00
 6731              	.LASF339:
 6732 121b 696E745F 		.string	"int_n_sep_by_space"
 6732      6E5F7365 
 6732      705F6279 
 6732      5F737061 
 6732      636500
 6733              	.LASF140:
 6734 122e 5F5F6E75 		.string	"__numeric_traits_integer<int>"
 6734      6D657269 
 6734      635F7472 
 6734      61697473 
 6734      5F696E74 
 6735              	.LASF442:
 6736 124c 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIsE5__minE"
 6736      5F5F676E 
 6736      755F6378 
 6736      7832345F 
 6736      5F6E756D 
 6737              	.LASF251:
 6738 127e 7763736E 		.string	"wcsncmp"
 6738      636D7000 
 6739              	.LASF131:
 6740 1286 62696E61 		.string	"binary"
 6740      727900
 6741              	.LASF182:
 6742 128d 5F5F7061 		.string	"__pad2"
 6742      643200
 6743              	.LASF354:
 6744 1294 6C646976 		.string	"ldiv_t"
 6744      5F7400
 6745              	.LASF112:
 6746 129b 6C656674 		.string	"left"
 6746      00
 6747              	.LASF191:
 6748 12a0 66705F6F 		.string	"fp_offset"
 6748      66667365 
 6748      7400
 6749              	.LASF237:
 6750 12aa 77637366 		.string	"wcsftime"
 6750      74696D65 
 6750      00
GAS LISTING /tmp/ccMbRvxK.s 			page 135


 6751              	.LASF41:
 6752 12b3 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6lengthEPKc"
 6752      74313163 
 6752      6861725F 
 6752      74726169 
 6752      74734963 
 6753              	.LASF337:
 6754 12d4 696E745F 		.string	"int_p_sep_by_space"
 6754      705F7365 
 6754      705F6279 
 6754      5F737061 
 6754      636500
 6755              	.LASF118:
 6756 12e7 736B6970 		.string	"skipws"
 6756      777300
 6757              	.LASF55:
 6758 12ee 5F5A4E53 		.string	"_ZNSt11char_traitsIcE11eq_int_typeERKiS2_"
 6758      74313163 
 6758      6861725F 
 6758      74726169 
 6758      74734963 
 6759              	.LASF408:
 6760 1318 67657463 		.string	"getc"
 6760      00
 6761              	.LASF301:
 6762 131d 75696E74 		.string	"uint_least32_t"
 6762      5F6C6561 
 6762      73743332 
 6762      5F7400
 6763              	.LASF449:
 6764 132c 6F706572 		.string	"operator bool"
 6764      61746F72 
 6764      20626F6F 
 6764      6C00
 6765              	.LASF359:
 6766 133a 61745F71 		.string	"at_quick_exit"
 6766      7569636B 
 6766      5F657869 
 6766      7400
 6767              	.LASF49:
 6768 1348 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6assignEPcmc"
 6768      74313163 
 6768      6861725F 
 6768      74726169 
 6768      74734963 
 6769              	.LASF51:
 6770 136a 5F5A4E53 		.string	"_ZNSt11char_traitsIcE12to_char_typeERKi"
 6770      74313163 
 6770      6861725F 
 6770      74726169 
 6770      74734963 
 6771              	.LASF267:
 6772 1392 776D656D 		.string	"wmemmove"
 6772      6D6F7665 
 6772      00
 6773              	.LASF450:
 6774 139b 5F5A4E4B 		.string	"_ZNKSt15__exception_ptr13exception_ptrcvbEv"
GAS LISTING /tmp/ccMbRvxK.s 			page 136


 6774      53743135 
 6774      5F5F6578 
 6774      63657074 
 6774      696F6E5F 
 6775              	.LASF80:
 6776 13c7 5F496F73 		.string	"_Ios_Fmtflags"
 6776      5F466D74 
 6776      666C6167 
 6776      7300
 6777              	.LASF312:
 6778 13d5 75696E74 		.string	"uintptr_t"
 6778      7074725F 
 6778      7400
 6779              	.LASF26:
 6780 13df 696E7465 		.string	"integral_constant<bool, false>"
 6780      6772616C 
 6780      5F636F6E 
 6780      7374616E 
 6780      743C626F 
 6781              	.LASF1:
 6782 13fe 5F4D5F61 		.string	"_M_addref"
 6782      64647265 
 6782      6600
 6783              	.LASF179:
 6784 1408 5F6C6F63 		.string	"_lock"
 6784      6B00
 6785              	.LASF194:
 6786 140e 73697A65 		.string	"sizetype"
 6786      74797065 
 6786      00
 6787              	.LASF375:
 6788 1417 73747274 		.string	"strtoul"
 6788      6F756C00 
 6789              	.LASF317:
 6790 141f 6C636F6E 		.string	"lconv"
 6790      7600
 6791              	.LASF175:
 6792 1425 5F6F6C64 		.string	"_old_offset"
 6792      5F6F6666 
 6792      73657400 
 6793              	.LASF87:
 6794 1431 5F535F74 		.string	"_S_trunc"
 6794      72756E63 
 6794      00
 6795              	.LASF157:
 6796 143a 5F494F5F 		.string	"_IO_FILE"
 6796      46494C45 
 6796      00
 6797              	.LASF348:
 6798 1443 5F41746F 		.string	"_Atomic_word"
 6798      6D69635F 
 6798      776F7264 
 6798      00
 6799              	.LASF197:
 6800 1450 77696E74 		.string	"wint_t"
 6800      5F7400
 6801              	.LASF193:
GAS LISTING /tmp/ccMbRvxK.s 			page 137


 6802 1457 7265675F 		.string	"reg_save_area"
 6802      73617665 
 6802      5F617265 
 6802      6100
 6803              	.LASF289:
 6804 1465 696E7433 		.string	"int32_t"
 6804      325F7400 
 6805              	.LASF82:
 6806 146d 5F535F61 		.string	"_S_app"
 6806      707000
 6807              	.LASF56:
 6808 1474 6E6F745F 		.string	"not_eof"
 6808      656F6600 
 6809              	.LASF297:
 6810 147c 696E745F 		.string	"int_least32_t"
 6810      6C656173 
 6810      7433325F 
 6810      7400
 6811              	.LASF255:
 6812 148a 77637374 		.string	"wcstod"
 6812      6F6400
 6813              	.LASF272:
 6814 1491 77637370 		.string	"wcspbrk"
 6814      62726B00 
 6815              	.LASF11:
 6816 1499 5F5A4E53 		.string	"_ZNSt15__exception_ptr13exception_ptrC4EDn"
 6816      7431355F 
 6816      5F657863 
 6816      65707469 
 6816      6F6E5F70 
 6817              	.LASF239:
 6818 14c4 746D5F6D 		.string	"tm_min"
 6818      696E00
 6819              	.LASF204:
 6820 14cb 6D627374 		.string	"mbstate_t"
 6820      6174655F 
 6820      7400
 6821              	.LASF259:
 6822 14d5 77637374 		.string	"wcstok"
 6822      6F6B00
 6823              	.LASF260:
 6824 14dc 77637374 		.string	"wcstol"
 6824      6F6C00
 6825              	.LASF268:
 6826 14e3 776D656D 		.string	"wmemset"
 6826      73657400 
 6827              	.LASF77:
 6828 14eb 5F535F69 		.string	"_S_ios_fmtflags_end"
 6828      6F735F66 
 6828      6D74666C 
 6828      6167735F 
 6828      656E6400 
 6829              	.LASF342:
 6830 14ff 7365746C 		.string	"setlocale"
 6830      6F63616C 
 6830      6500
 6831              	.LASF89:
GAS LISTING /tmp/ccMbRvxK.s 			page 138


 6832 1509 5F535F69 		.string	"_S_ios_openmode_max"
 6832      6F735F6F 
 6832      70656E6D 
 6832      6F64655F 
 6832      6D617800 
 6833              	.LASF352:
 6834 151d 6469765F 		.string	"div_t"
 6834      7400
 6835              	.LASF43:
 6836 1523 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4findEPKcmRS1_"
 6836      74313163 
 6836      6861725F 
 6836      74726169 
 6836      74734963 
 6837              	.LASF8:
 6838 1547 5F5A4E4B 		.string	"_ZNKSt15__exception_ptr13exception_ptr6_M_getEv"
 6838      53743135 
 6838      5F5F6578 
 6838      63657074 
 6838      696F6E5F 
 6839              	.LASF385:
 6840 1577 395F475F 		.string	"9_G_fpos_t"
 6840      66706F73 
 6840      5F7400
 6841              	.LASF391:
 6842 1582 5F736275 		.string	"_sbuf"
 6842      6600
 6843              	.LASF417:
 6844 1588 746D7066 		.string	"tmpfile"
 6844      696C6500 
 6845              	.LASF409:
 6846 1590 67657463 		.string	"getchar"
 6846      68617200 
 6847              	.LASF111:
 6848 1598 696E7465 		.string	"internal"
 6848      726E616C 
 6848      00
 6849              	.LASF164:
 6850 15a1 5F494F5F 		.string	"_IO_write_ptr"
 6850      77726974 
 6850      655F7074 
 6850      7200
 6851              	.LASF54:
 6852 15af 65715F69 		.string	"eq_int_type"
 6852      6E745F74 
 6852      79706500 
 6853              	.LASF319:
 6854 15bb 74686F75 		.string	"thousands_sep"
 6854      73616E64 
 6854      735F7365 
 6854      7000
 6855              	.LASF2:
 6856 15c9 5F4D5F72 		.string	"_M_release"
 6856      656C6561 
 6856      736500
 6857              	.LASF373:
 6858 15d4 73747274 		.string	"strtod"
GAS LISTING /tmp/ccMbRvxK.s 			page 139


 6858      6F6400
 6859              	.LASF383:
 6860 15db 73747274 		.string	"strtof"
 6860      6F6600
 6861              	.LASF307:
 6862 15e2 75696E74 		.string	"uint_fast8_t"
 6862      5F666173 
 6862      74385F74 
 6862      00
 6863              	.LASF396:
 6864 15ef 66656F66 		.string	"feof"
 6864      00
 6865              	.LASF240:
 6866 15f4 746D5F68 		.string	"tm_hour"
 6866      6F757200 
 6867              	.LASF377:
 6868 15fc 77637374 		.string	"wcstombs"
 6868      6F6D6273 
 6868      00
 6869              	.LASF374:
 6870 1605 73747274 		.string	"strtol"
 6870      6F6C00
 6871              	.LASF213:
 6872 160c 66777072 		.string	"fwprintf"
 6872      696E7466 
 6872      00
 6873              	.LASF366:
 6874 1615 6D626C65 		.string	"mblen"
 6874      6E00
 6875              	.LASF192:
 6876 161b 6F766572 		.string	"overflow_arg_area"
 6876      666C6F77 
 6876      5F617267 
 6876      5F617265 
 6876      6100
 6877              	.LASF106:
 6878 162d 496E6974 		.string	"Init"
 6878      00
 6879              	.LASF357:
 6880 1632 5F5F636F 		.string	"__compar_fn_t"
 6880      6D706172 
 6880      5F666E5F 
 6880      7400
 6881              	.LASF276:
 6882 1640 77637374 		.string	"wcstold"
 6882      6F6C6400 
 6883              	.LASF264:
 6884 1648 7763746F 		.string	"wctob"
 6884      6200
 6885              	.LASF353:
 6886 164e 366C6469 		.string	"6ldiv_t"
 6886      765F7400 
 6887              	.LASF278:
 6888 1656 77637374 		.string	"wcstoll"
 6888      6F6C6C00 
 6889              	.LASF380:
 6890 165e 61746F6C 		.string	"atoll"
GAS LISTING /tmp/ccMbRvxK.s 			page 140


 6890      6C00
 6891              	.LASF141:
 6892 1664 5F5F6D69 		.string	"__min"
 6892      6E00
 6893              	.LASF222:
 6894 166a 73777072 		.string	"swprintf"
 6894      696E7466 
 6894      00
 6895              	.LASF10:
 6896 1673 5F5A4E53 		.string	"_ZNSt15__exception_ptr13exception_ptrC4ERKS0_"
 6896      7431355F 
 6896      5F657863 
 6896      65707469 
 6896      6F6E5F70 
 6897              	.LASF23:
 6898 16a1 76616C75 		.string	"value_type"
 6898      655F7479 
 6898      706500
 6899              	.LASF441:
 6900 16ac 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIcE5__maxE"
 6900      5F5F676E 
 6900      755F6378 
 6900      7832345F 
 6900      5F6E756D 
 6901              	.LASF384:
 6902 16de 73747274 		.string	"strtold"
 6902      6F6C6400 
 6903              	.LASF346:
 6904 16e6 5F5F6F66 		.string	"__off_t"
 6904      665F7400 
 6905              	.LASF355:
 6906 16ee 376C6C64 		.string	"7lldiv_t"
 6906      69765F74 
 6906      00
 6907              	.LASF132:
 6908 16f7 7472756E 		.string	"trunc"
 6908      6300
 6909              	.LASF284:
 6910 16fd 7369676E 		.string	"signed char"
 6910      65642063 
 6910      68617200 
 6911              	.LASF324:
 6912 1709 6D6F6E5F 		.string	"mon_thousands_sep"
 6912      74686F75 
 6912      73616E64 
 6912      735F7365 
 6912      7000
 6913              	.LASF122:
 6914 171b 62617365 		.string	"basefield"
 6914      6669656C 
 6914      6400
 6915              	.LASF205:
 6916 1725 73686F72 		.string	"short unsigned int"
 6916      7420756E 
 6916      7369676E 
 6916      65642069 
 6916      6E7400
GAS LISTING /tmp/ccMbRvxK.s 			page 141


 6917              	.LASF238:
 6918 1738 746D5F73 		.string	"tm_sec"
 6918      656300
 6919              	.LASF356:
 6920 173f 6C6C6469 		.string	"lldiv_t"
 6920      765F7400 
 6921              	.LASF72:
 6922 1747 5F535F75 		.string	"_S_unitbuf"
 6922      6E697462 
 6922      756600
 6923              	.LASF360:
 6924 1752 61746F66 		.string	"atof"
 6924      00
 6925              	.LASF236:
 6926 1757 77637363 		.string	"wcscspn"
 6926      73706E00 
 6927              	.LASF361:
 6928 175f 61746F69 		.string	"atoi"
 6928      00
 6929              	.LASF332:
 6930 1764 6E5F6373 		.string	"n_cs_precedes"
 6930      5F707265 
 6930      63656465 
 6930      7300
 6931              	.LASF246:
 6932 1772 746D5F69 		.string	"tm_isdst"
 6932      73647374 
 6932      00
 6933              	.LASF133:
 6934 177b 7365656B 		.string	"seekdir"
 6934      64697200 
 6935              	.LASF14:
 6936 1783 5F5A4E53 		.string	"_ZNSt15__exception_ptr13exception_ptraSERKS0_"
 6936      7431355F 
 6936      5F657863 
 6936      65707469 
 6936      6F6E5F70 
 6937              	.LASF440:
 6938 17b1 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerImE8__digitsE"
 6938      5F5F676E 
 6938      755F6378 
 6938      7832345F 
 6938      5F6E756D 
 6939              	.LASF252:
 6940 17e6 7763736E 		.string	"wcsncpy"
 6940      63707900 
 6941              	.LASF207:
 6942 17ee 66676574 		.string	"fgetwc"
 6942      776300
 6943              	.LASF457:
 6944 17f5 5F547261 		.string	"_Traits"
 6944      69747300 
 6945              	.LASF47:
 6946 17fd 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4copyEPcPKcm"
 6946      74313163 
 6946      6861725F 
 6946      74726169 
GAS LISTING /tmp/ccMbRvxK.s 			page 142


 6946      74734963 
 6947              	.LASF256:
 6948 181f 646F7562 		.string	"double"
 6948      6C6500
 6949              	.LASF250:
 6950 1826 7763736E 		.string	"wcsncat"
 6950      63617400 
 6951              	.LASF247:
 6952 182e 746D5F67 		.string	"tm_gmtoff"
 6952      6D746F66 
 6952      6600
 6953              	.LASF115:
 6954 1838 73686F77 		.string	"showbase"
 6954      62617365 
 6954      00
 6955              	.LASF137:
 6956 1841 6F737472 		.string	"ostream"
 6956      65616D00 
 6957              	.LASF172:
 6958 1849 5F636861 		.string	"_chain"
 6958      696E00
 6959              	.LASF153:
 6960 1850 5F5F6E75 		.string	"__numeric_traits_integer<long unsigned int>"
 6960      6D657269 
 6960      635F7472 
 6960      61697473 
 6960      5F696E74 
 6961              	.LASF3:
 6962 187c 5F5A4E53 		.string	"_ZNSt15__exception_ptr13exception_ptr9_M_addrefEv"
 6962      7431355F 
 6962      5F657863 
 6962      65707469 
 6962      6F6E5F70 
 6963              	.LASF156:
 6964 18ae 46494C45 		.string	"FILE"
 6964      00
 6965              	.LASF424:
 6966 18b3 77637472 		.string	"wctrans"
 6966      616E7300 
 6967              	.LASF227:
 6968 18bb 76737770 		.string	"vswprintf"
 6968      72696E74 
 6968      6600
 6969              	.LASF244:
 6970 18c5 746D5F77 		.string	"tm_wday"
 6970      64617900 
 6971              	.LASF174:
 6972 18cd 5F666C61 		.string	"_flags2"
 6972      67733200 
 6973              	.LASF439:
 6974 18d5 5F5A4E39 		.string	"_ZN9__gnu_cxx25__numeric_traits_floatingIeE16__max_exponent10E"
 6974      5F5F676E 
 6974      755F6378 
 6974      7832355F 
 6974      5F6E756D 
 6975              	.LASF40:
 6976 1914 5F5A4E53 		.string	"_ZNSt11char_traitsIcE7compareEPKcS2_m"
GAS LISTING /tmp/ccMbRvxK.s 			page 143


 6976      74313163 
 6976      6861725F 
 6976      74726169 
 6976      74734963 
 6977              	.LASF340:
 6978 193a 696E745F 		.string	"int_p_sign_posn"
 6978      705F7369 
 6978      676E5F70 
 6978      6F736E00 
 6979              	.LASF189:
 6980 194a 74797065 		.string	"typedef __va_list_tag __va_list_tag"
 6980      64656620 
 6980      5F5F7661 
 6980      5F6C6973 
 6980      745F7461 
 6981              	.LASF99:
 6982 196e 5F496F73 		.string	"_Ios_Seekdir"
 6982      5F536565 
 6982      6B646972 
 6982      00
 6983              	.LASF70:
 6984 197b 5F535F73 		.string	"_S_showpos"
 6984      686F7770 
 6984      6F7300
 6985              	.LASF36:
 6986 1986 5F5A4E53 		.string	"_ZNSt11char_traitsIcE2eqERKcS2_"
 6986      74313163 
 6986      6861725F 
 6986      74726169 
 6986      74734963 
 6987              	.LASF249:
 6988 19a6 7763736C 		.string	"wcslen"
 6988      656E00
 6989              	.LASF347:
 6990 19ad 5F5F6F66 		.string	"__off64_t"
 6990      6636345F 
 6990      7400
 6991              	.LASF429:
 6992 19b7 5F5F696F 		.string	"__ioinit"
 6992      696E6974 
 6992      00
 6993              	.LASF187:
 6994 19c0 5F756E75 		.string	"_unused2"
 6994      73656432 
 6994      00
 6995              	.LASF166:
 6996 19c9 5F494F5F 		.string	"_IO_buf_base"
 6996      6275665F 
 6996      62617365 
 6996      00
 6997              	.LASF444:
 6998 19d6 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIlE5__minE"
 6998      5F5F676E 
 6998      755F6378 
 6998      7832345F 
 6998      5F6E756D 
 6999              	.LASF265:
GAS LISTING /tmp/ccMbRvxK.s 			page 144


 7000 1a08 776D656D 		.string	"wmemcmp"
 7000      636D7000 
 7001              	.LASF371:
 7002 1a10 71736F72 		.string	"qsort"
 7002      7400
 7003              		.hidden	__dso_handle
 7004              		.ident	"GCC: (Ubuntu 5.4.1-2ubuntu1~16.04) 5.4.1 20160904"
 7005              		.section	.note.GNU-stack,"",@progbits
GAS LISTING /tmp/ccMbRvxK.s 			page 145


DEFINED SYMBOLS
                            *ABS*:0000000000000000 new_delete.cpp
     /tmp/ccMbRvxK.s:7      .rodata:0000000000000000 _ZStL19piecewise_construct
                             .bss:0000000000000000 _ZStL8__ioinit
     /tmp/ccMbRvxK.s:14     .text:0000000000000000 main
     /tmp/ccMbRvxK.s:127    .text:000000000000015d _Z41__static_initialization_and_destruction_0ii
     /tmp/ccMbRvxK.s:163    .text:000000000000019b _GLOBAL__sub_I_main

UNDEFINED SYMBOLS
_ZSt4cout
_ZNSolsEi
_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
_ZNSolsEPFRSoS_E
_ZNSolsEPKv
_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c
_Znwm
_ZdlPv
__stack_chk_fail
_ZNSt8ios_base4InitC1Ev
__dso_handle
_ZNSt8ios_base4InitD1Ev
__cxa_atexit
