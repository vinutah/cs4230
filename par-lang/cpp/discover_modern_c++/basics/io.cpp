/*
 * c++ uses a convienient abstraction called stream to perform I/O
 * operations in a sequential media such as screen or keyboard
 *
 * stream is an object where a program can insert characters or extract them
 *
 * standard input and output stream objects are declared in <iostream>
 *
 * 1 Standard Output
 *    ostream
 *    Insertion operator <<
 *    difference beteween \n and std::endl
 *
 * 2 Standard Input
 *    istream
 *    Extraction operator >>
 *    cin stream, interpretes them as int
 *
 *    istream
 *    ostream
 *    iostream
 *
 * 3 Input Output with files
 *    c++ gives classes for io with files
 *
 *    ofstream
 *    ifstream
 *    fstream
 *
 * 4
 * 5
 * 6
 *
 *
 *
 */

#include <fstream>

int main()
{
  std::ofstream square_file;

  square_file.open("squares1.txt");

  for (int i = 0; i < 10; ++i)
      square_file << i << "^2 = " << i*i << std::endl ;

  square_file.close();

  std::ofstream square_file2("squares2.txt");

  for (int i= 0; i < 10; ++i)
    square_file2 << i << "^2 = " << i*i << std::endl;

  return 0;
}
