GAS LISTING /tmp/cc45yn9z.s 			page 1


   1              		.file	"io-error-handling.cpp"
   2              		.text
   3              	.Ltext0:
   4              		.local	_ZStL8__ioinit
   5              		.comm	_ZStL8__ioinit,1,1
   6              		.section	.rodata
   7              	.LC0:
   8 0000 736F6D65 		.string	"some_missing_file.xyz"
   8      5F6D6973 
   8      73696E67 
   8      5F66696C 
   8      652E7879 
   9              	.LC1:
  10 0016 69202D3E 		.string	"i -> "
  10      2000
  11              	.LC2:
  12 001c 64202D3E 		.string	"d -> "
  12      2000
  13              		.text
  14              		.globl	main
  15              		.type	main, @function
  16              	main:
  17              	.LFB1085:
  18              		.file 1 "io-error-handling.cpp"
   1:io-error-handling.cpp **** /*
   2:io-error-handling.cpp ****  * dealing with io errors
   3:io-error-handling.cpp ****  *
   4:io-error-handling.cpp ****  * c++ io is not fail-safe
   5:io-error-handling.cpp ****  *
   6:io-error-handling.cpp ****  * errors can be reported in different ways
   7:io-error-handling.cpp ****  *  our error handling must comply to them ?
   8:io-error-handling.cpp ****  *
   9:io-error-handling.cpp ****  * example program
  10:io-error-handling.cpp ****  */
  11:io-error-handling.cpp **** 
  12:io-error-handling.cpp **** #include <fstream>
  13:io-error-handling.cpp **** #include <iostream>
  14:io-error-handling.cpp **** 
  15:io-error-handling.cpp **** 
  16:io-error-handling.cpp **** int main()
  17:io-error-handling.cpp **** {
  19              		.loc 1 17 0
  20              		.cfi_startproc
  21              		.cfi_personality 0x3,__gxx_personality_v0
  22              		.cfi_lsda 0x3,.LLSDA1085
  23 0000 55       		pushq	%rbp
  24              		.cfi_def_cfa_offset 16
  25              		.cfi_offset 6, -16
  26 0001 4889E5   		movq	%rsp, %rbp
  27              		.cfi_def_cfa_register 6
  28 0004 4154     		pushq	%r12
  29 0006 53       		pushq	%rbx
  30 0007 4881EC30 		subq	$560, %rsp
  30      020000
  31              		.cfi_offset 12, -24
  32              		.cfi_offset 3, -32
  33              		.loc 1 17 0
GAS LISTING /tmp/cc45yn9z.s 			page 2


  34 000e 64488B04 		movq	%fs:40, %rax
  34      25280000 
  34      00
  35 0017 488945E8 		movq	%rax, -24(%rbp)
  36 001b 31C0     		xorl	%eax, %eax
  18:io-error-handling.cpp ****   std::ifstream infile("some_missing_file.xyz");
  37              		.loc 1 18 0
  38 001d 488D85E0 		leaq	-544(%rbp), %rax
  38      FDFFFF
  39 0024 BA080000 		movl	$8, %edx
  39      00
  40 0029 BE000000 		movl	$.LC0, %esi
  40      00
  41 002e 4889C7   		movq	%rax, %rdi
  42              	.LEHB0:
  43 0031 E8000000 		call	_ZNSt14basic_ifstreamIcSt11char_traitsIcEEC1EPKcSt13_Ios_Openmode
  43      00
  44              	.LEHE0:
  19:io-error-handling.cpp **** 
  20:io-error-handling.cpp ****   int i;
  21:io-error-handling.cpp ****   double d;
  22:io-error-handling.cpp **** 
  23:io-error-handling.cpp ****   infile >> i >> d;
  45              		.loc 1 23 0
  46 0036 488D95D4 		leaq	-556(%rbp), %rdx
  46      FDFFFF
  47 003d 488D85E0 		leaq	-544(%rbp), %rax
  47      FDFFFF
  48 0044 4889D6   		movq	%rdx, %rsi
  49 0047 4889C7   		movq	%rax, %rdi
  50              	.LEHB1:
  51 004a E8000000 		call	_ZNSirsERi
  51      00
  52 004f 4889C2   		movq	%rax, %rdx
  53 0052 488D85D8 		leaq	-552(%rbp), %rax
  53      FDFFFF
  54 0059 4889C6   		movq	%rax, %rsi
  55 005c 4889D7   		movq	%rdx, %rdi
  56 005f E8000000 		call	_ZNSirsERd
  56      00
  24:io-error-handling.cpp **** 
  25:io-error-handling.cpp ****   std::cout << "i -> " << i << std::endl << "d -> " << d << std::endl ;
  57              		.loc 1 25 0
  58 0064 488B9DD8 		movq	-552(%rbp), %rbx
  58      FDFFFF
  59 006b 448BA5D4 		movl	-556(%rbp), %r12d
  59      FDFFFF
  60 0072 BE000000 		movl	$.LC1, %esi
  60      00
  61 0077 BF000000 		movl	$_ZSt4cout, %edi
  61      00
  62 007c E8000000 		call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
  62      00
  63 0081 4489E6   		movl	%r12d, %esi
  64 0084 4889C7   		movq	%rax, %rdi
  65 0087 E8000000 		call	_ZNSolsEi
  65      00
GAS LISTING /tmp/cc45yn9z.s 			page 3


  66              		.loc 1 25 0 is_stmt 0 discriminator 1
  67 008c BE000000 		movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
  67      00
  68 0091 4889C7   		movq	%rax, %rdi
  69 0094 E8000000 		call	_ZNSolsEPFRSoS_E
  69      00
  70              		.loc 1 25 0 discriminator 2
  71 0099 BE000000 		movl	$.LC2, %esi
  71      00
  72 009e 4889C7   		movq	%rax, %rdi
  73 00a1 E8000000 		call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
  73      00
  74              		.loc 1 25 0 discriminator 3
  75 00a6 48899DC8 		movq	%rbx, -568(%rbp)
  75      FDFFFF
  76 00ad F20F1085 		movsd	-568(%rbp), %xmm0
  76      C8FDFFFF 
  77 00b5 4889C7   		movq	%rax, %rdi
  78 00b8 E8000000 		call	_ZNSolsEd
  78      00
  79              		.loc 1 25 0 discriminator 4
  80 00bd BE000000 		movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
  80      00
  81 00c2 4889C7   		movq	%rax, %rdi
  82 00c5 E8000000 		call	_ZNSolsEPFRSoS_E
  82      00
  26:io-error-handling.cpp **** 
  27:io-error-handling.cpp ****   infile.close();
  83              		.loc 1 27 0 is_stmt 1
  84 00ca 488D85E0 		leaq	-544(%rbp), %rax
  84      FDFFFF
  85 00d1 4889C7   		movq	%rax, %rdi
  86 00d4 E8000000 		call	_ZNSt14basic_ifstreamIcSt11char_traitsIcEE5closeEv
  86      00
  87              	.LEHE1:
  18:io-error-handling.cpp **** 
  88              		.loc 1 18 0
  89 00d9 488D85E0 		leaq	-544(%rbp), %rax
  89      FDFFFF
  90 00e0 4889C7   		movq	%rax, %rdi
  91              	.LEHB2:
  92 00e3 E8000000 		call	_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev
  92      00
  93              	.LEHE2:
  28:io-error-handling.cpp **** }
  94              		.loc 1 28 0
  95 00e8 B8000000 		movl	$0, %eax
  95      00
  96 00ed 488B4DE8 		movq	-24(%rbp), %rcx
  97 00f1 6448330C 		xorq	%fs:40, %rcx
  97      25280000 
  97      00
  98 00fa 7424     		je	.L4
  99 00fc EB1D     		jmp	.L6
 100              	.L5:
 101 00fe 4889C3   		movq	%rax, %rbx
  18:io-error-handling.cpp **** 
GAS LISTING /tmp/cc45yn9z.s 			page 4


 102              		.loc 1 18 0
 103 0101 488D85E0 		leaq	-544(%rbp), %rax
 103      FDFFFF
 104 0108 4889C7   		movq	%rax, %rdi
 105 010b E8000000 		call	_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev
 105      00
 106 0110 4889D8   		movq	%rbx, %rax
 107 0113 4889C7   		movq	%rax, %rdi
 108              	.LEHB3:
 109 0116 E8000000 		call	_Unwind_Resume
 109      00
 110              	.LEHE3:
 111              	.L6:
 112              		.loc 1 28 0
 113 011b E8000000 		call	__stack_chk_fail
 113      00
 114              	.L4:
 115 0120 4881C430 		addq	$560, %rsp
 115      020000
 116 0127 5B       		popq	%rbx
 117 0128 415C     		popq	%r12
 118 012a 5D       		popq	%rbp
 119              		.cfi_def_cfa 7, 8
 120 012b C3       		ret
 121              		.cfi_endproc
 122              	.LFE1085:
 123              		.globl	__gxx_personality_v0
 124              		.section	.gcc_except_table,"a",@progbits
 125              	.LLSDA1085:
 126 0000 FF       		.byte	0xff
 127 0001 FF       		.byte	0xff
 128 0002 01       		.byte	0x1
 129 0003 14       		.uleb128 .LLSDACSE1085-.LLSDACSB1085
 130              	.LLSDACSB1085:
 131 0004 31       		.uleb128 .LEHB0-.LFB1085
 132 0005 05       		.uleb128 .LEHE0-.LEHB0
 133 0006 00       		.uleb128 0
 134 0007 00       		.uleb128 0
 135 0008 4A       		.uleb128 .LEHB1-.LFB1085
 136 0009 8F01     		.uleb128 .LEHE1-.LEHB1
 137 000b FE01     		.uleb128 .L5-.LFB1085
 138 000d 00       		.uleb128 0
 139 000e E301     		.uleb128 .LEHB2-.LFB1085
 140 0010 05       		.uleb128 .LEHE2-.LEHB2
 141 0011 00       		.uleb128 0
 142 0012 00       		.uleb128 0
 143 0013 9602     		.uleb128 .LEHB3-.LFB1085
 144 0015 05       		.uleb128 .LEHE3-.LEHB3
 145 0016 00       		.uleb128 0
 146 0017 00       		.uleb128 0
 147              	.LLSDACSE1085:
 148              		.text
 149              		.size	main, .-main
 150              		.type	_Z41__static_initialization_and_destruction_0ii, @function
 151              	_Z41__static_initialization_and_destruction_0ii:
 152              	.LFB1127:
 153              		.loc 1 28 0
GAS LISTING /tmp/cc45yn9z.s 			page 5


 154              		.cfi_startproc
 155 012c 55       		pushq	%rbp
 156              		.cfi_def_cfa_offset 16
 157              		.cfi_offset 6, -16
 158 012d 4889E5   		movq	%rsp, %rbp
 159              		.cfi_def_cfa_register 6
 160 0130 4883EC10 		subq	$16, %rsp
 161 0134 897DFC   		movl	%edi, -4(%rbp)
 162 0137 8975F8   		movl	%esi, -8(%rbp)
 163              		.loc 1 28 0
 164 013a 837DFC01 		cmpl	$1, -4(%rbp)
 165 013e 7527     		jne	.L9
 166              		.loc 1 28 0 is_stmt 0 discriminator 1
 167 0140 817DF8FF 		cmpl	$65535, -8(%rbp)
 167      FF0000
 168 0147 751E     		jne	.L9
 169              		.file 2 "/usr/include/c++/5/iostream"
   1:/usr/include/c++/5/iostream **** // Standard iostream objects -*- C++ -*-
   2:/usr/include/c++/5/iostream **** 
   3:/usr/include/c++/5/iostream **** // Copyright (C) 1997-2015 Free Software Foundation, Inc.
   4:/usr/include/c++/5/iostream **** //
   5:/usr/include/c++/5/iostream **** // This file is part of the GNU ISO C++ Library.  This library is free
   6:/usr/include/c++/5/iostream **** // software; you can redistribute it and/or modify it under the
   7:/usr/include/c++/5/iostream **** // terms of the GNU General Public License as published by the
   8:/usr/include/c++/5/iostream **** // Free Software Foundation; either version 3, or (at your option)
   9:/usr/include/c++/5/iostream **** // any later version.
  10:/usr/include/c++/5/iostream **** 
  11:/usr/include/c++/5/iostream **** // This library is distributed in the hope that it will be useful,
  12:/usr/include/c++/5/iostream **** // but WITHOUT ANY WARRANTY; without even the implied warranty of
  13:/usr/include/c++/5/iostream **** // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  14:/usr/include/c++/5/iostream **** // GNU General Public License for more details.
  15:/usr/include/c++/5/iostream **** 
  16:/usr/include/c++/5/iostream **** // Under Section 7 of GPL version 3, you are granted additional
  17:/usr/include/c++/5/iostream **** // permissions described in the GCC Runtime Library Exception, version
  18:/usr/include/c++/5/iostream **** // 3.1, as published by the Free Software Foundation.
  19:/usr/include/c++/5/iostream **** 
  20:/usr/include/c++/5/iostream **** // You should have received a copy of the GNU General Public License and
  21:/usr/include/c++/5/iostream **** // a copy of the GCC Runtime Library Exception along with this program;
  22:/usr/include/c++/5/iostream **** // see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
  23:/usr/include/c++/5/iostream **** // <http://www.gnu.org/licenses/>.
  24:/usr/include/c++/5/iostream **** 
  25:/usr/include/c++/5/iostream **** /** @file include/iostream
  26:/usr/include/c++/5/iostream ****  *  This is a Standard C++ Library header.
  27:/usr/include/c++/5/iostream ****  */
  28:/usr/include/c++/5/iostream **** 
  29:/usr/include/c++/5/iostream **** //
  30:/usr/include/c++/5/iostream **** // ISO C++ 14882: 27.3  Standard iostream objects
  31:/usr/include/c++/5/iostream **** //
  32:/usr/include/c++/5/iostream **** 
  33:/usr/include/c++/5/iostream **** #ifndef _GLIBCXX_IOSTREAM
  34:/usr/include/c++/5/iostream **** #define _GLIBCXX_IOSTREAM 1
  35:/usr/include/c++/5/iostream **** 
  36:/usr/include/c++/5/iostream **** #pragma GCC system_header
  37:/usr/include/c++/5/iostream **** 
  38:/usr/include/c++/5/iostream **** #include <bits/c++config.h>
  39:/usr/include/c++/5/iostream **** #include <ostream>
  40:/usr/include/c++/5/iostream **** #include <istream>
GAS LISTING /tmp/cc45yn9z.s 			page 6


  41:/usr/include/c++/5/iostream **** 
  42:/usr/include/c++/5/iostream **** namespace std _GLIBCXX_VISIBILITY(default)
  43:/usr/include/c++/5/iostream **** {
  44:/usr/include/c++/5/iostream **** _GLIBCXX_BEGIN_NAMESPACE_VERSION
  45:/usr/include/c++/5/iostream **** 
  46:/usr/include/c++/5/iostream ****   /**
  47:/usr/include/c++/5/iostream ****    *  @name Standard Stream Objects
  48:/usr/include/c++/5/iostream ****    *
  49:/usr/include/c++/5/iostream ****    *  The &lt;iostream&gt; header declares the eight <em>standard stream
  50:/usr/include/c++/5/iostream ****    *  objects</em>.  For other declarations, see
  51:/usr/include/c++/5/iostream ****    *  http://gcc.gnu.org/onlinedocs/libstdc++/manual/io.html
  52:/usr/include/c++/5/iostream ****    *  and the @link iosfwd I/O forward declarations @endlink
  53:/usr/include/c++/5/iostream ****    *
  54:/usr/include/c++/5/iostream ****    *  They are required by default to cooperate with the global C
  55:/usr/include/c++/5/iostream ****    *  library's @c FILE streams, and to be available during program
  56:/usr/include/c++/5/iostream ****    *  startup and termination. For more information, see the section of the
  57:/usr/include/c++/5/iostream ****    *  manual linked to above.
  58:/usr/include/c++/5/iostream ****   */
  59:/usr/include/c++/5/iostream ****   //@{
  60:/usr/include/c++/5/iostream ****   extern istream cin;		/// Linked to standard input
  61:/usr/include/c++/5/iostream ****   extern ostream cout;		/// Linked to standard output
  62:/usr/include/c++/5/iostream ****   extern ostream cerr;		/// Linked to standard error (unbuffered)
  63:/usr/include/c++/5/iostream ****   extern ostream clog;		/// Linked to standard error (buffered)
  64:/usr/include/c++/5/iostream **** 
  65:/usr/include/c++/5/iostream **** #ifdef _GLIBCXX_USE_WCHAR_T
  66:/usr/include/c++/5/iostream ****   extern wistream wcin;		/// Linked to standard input
  67:/usr/include/c++/5/iostream ****   extern wostream wcout;	/// Linked to standard output
  68:/usr/include/c++/5/iostream ****   extern wostream wcerr;	/// Linked to standard error (unbuffered)
  69:/usr/include/c++/5/iostream ****   extern wostream wclog;	/// Linked to standard error (buffered)
  70:/usr/include/c++/5/iostream **** #endif
  71:/usr/include/c++/5/iostream ****   //@}
  72:/usr/include/c++/5/iostream **** 
  73:/usr/include/c++/5/iostream ****   // For construction of filebuffers for cout, cin, cerr, clog et. al.
  74:/usr/include/c++/5/iostream ****   static ios_base::Init __ioinit;
 170              		.loc 2 74 0 is_stmt 1
 171 0149 BF000000 		movl	$_ZStL8__ioinit, %edi
 171      00
 172 014e E8000000 		call	_ZNSt8ios_base4InitC1Ev
 172      00
 173 0153 BA000000 		movl	$__dso_handle, %edx
 173      00
 174 0158 BE000000 		movl	$_ZStL8__ioinit, %esi
 174      00
 175 015d BF000000 		movl	$_ZNSt8ios_base4InitD1Ev, %edi
 175      00
 176 0162 E8000000 		call	__cxa_atexit
 176      00
 177              	.L9:
 178              		.loc 1 28 0
 179 0167 90       		nop
 180 0168 C9       		leave
 181              		.cfi_def_cfa 7, 8
 182 0169 C3       		ret
 183              		.cfi_endproc
 184              	.LFE1127:
 185              		.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destructi
 186              		.type	_GLOBAL__sub_I_main, @function
GAS LISTING /tmp/cc45yn9z.s 			page 7


 187              	_GLOBAL__sub_I_main:
 188              	.LFB1128:
 189              		.loc 1 28 0
 190              		.cfi_startproc
 191 016a 55       		pushq	%rbp
 192              		.cfi_def_cfa_offset 16
 193              		.cfi_offset 6, -16
 194 016b 4889E5   		movq	%rsp, %rbp
 195              		.cfi_def_cfa_register 6
 196              		.loc 1 28 0
 197 016e BEFFFF00 		movl	$65535, %esi
 197      00
 198 0173 BF010000 		movl	$1, %edi
 198      00
 199 0178 E8AFFFFF 		call	_Z41__static_initialization_and_destruction_0ii
 199      FF
 200 017d 5D       		popq	%rbp
 201              		.cfi_def_cfa 7, 8
 202 017e C3       		ret
 203              		.cfi_endproc
 204              	.LFE1128:
 205              		.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
 206              		.section	.init_array,"aw"
 207              		.align 8
 208 0000 00000000 		.quad	_GLOBAL__sub_I_main
 208      00000000 
 209              		.text
 210              	.Letext0:
 211              		.file 3 "/usr/include/c++/5/cwchar"
 212              		.file 4 "/usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h"
 213              		.file 5 "/usr/include/c++/5/debug/debug.h"
 214              		.file 6 "/usr/include/c++/5/bits/char_traits.h"
 215              		.file 7 "/usr/include/c++/5/clocale"
 216              		.file 8 "/usr/include/c++/5/bits/ios_base.h"
 217              		.file 9 "/usr/include/c++/5/cwctype"
 218              		.file 10 "/usr/include/c++/5/cstdio"
 219              		.file 11 "/usr/include/c++/5/iosfwd"
 220              		.file 12 "/usr/include/c++/5/bits/predefined_ops.h"
 221              		.file 13 "/usr/include/c++/5/ext/new_allocator.h"
 222              		.file 14 "/usr/include/c++/5/ext/numeric_traits.h"
 223              		.file 15 "/usr/include/stdio.h"
 224              		.file 16 "/usr/include/libio.h"
 225              		.file 17 "<built-in>"
 226              		.file 18 "/usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h"
 227              		.file 19 "/usr/include/wchar.h"
 228              		.file 20 "/usr/include/time.h"
 229              		.file 21 "/usr/include/locale.h"
 230              		.file 22 "/usr/include/x86_64-linux-gnu/bits/types.h"
 231              		.file 23 "/usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h"
 232              		.file 24 "/usr/include/wctype.h"
 233              		.file 25 "/usr/include/_G_config.h"
 234              		.section	.debug_info,"",@progbits
 235              	.Ldebug_info0:
 236 0000 951B0000 		.long	0x1b95
 237 0004 0400     		.value	0x4
 238 0006 00000000 		.long	.Ldebug_abbrev0
 239 000a 08       		.byte	0x8
GAS LISTING /tmp/cc45yn9z.s 			page 8


 240 000b 01       		.uleb128 0x1
 241 000c 00000000 		.long	.LASF337
 242 0010 04       		.byte	0x4
 243 0011 00000000 		.long	.LASF338
 244 0015 00000000 		.long	.LASF339
 245 0019 00000000 		.quad	.Ltext0
 245      00000000 
 246 0021 7F010000 		.quad	.Letext0-.Ltext0
 246      00000000 
 247 0029 00000000 		.long	.Ldebug_line0
 248 002d 02       		.uleb128 0x2
 249 002e 73746400 		.string	"std"
 250 0032 11       		.byte	0x11
 251 0033 00       		.byte	0
 252 0034 18090000 		.long	0x918
 253 0038 03       		.uleb128 0x3
 254 0039 00000000 		.long	.LASF0
 255 003d 04       		.byte	0x4
 256 003e DA       		.byte	0xda
 257 003f 04       		.uleb128 0x4
 258 0040 04       		.byte	0x4
 259 0041 DA       		.byte	0xda
 260 0042 38000000 		.long	0x38
 261 0046 05       		.uleb128 0x5
 262 0047 03       		.byte	0x3
 263 0048 40       		.byte	0x40
 264 0049 0F0D0000 		.long	0xd0f
 265 004d 05       		.uleb128 0x5
 266 004e 03       		.byte	0x3
 267 004f 8B       		.byte	0x8b
 268 0050 960C0000 		.long	0xc96
 269 0054 05       		.uleb128 0x5
 270 0055 03       		.byte	0x3
 271 0056 8D       		.byte	0x8d
 272 0057 310D0000 		.long	0xd31
 273 005b 05       		.uleb128 0x5
 274 005c 03       		.byte	0x3
 275 005d 8E       		.byte	0x8e
 276 005e 470D0000 		.long	0xd47
 277 0062 05       		.uleb128 0x5
 278 0063 03       		.byte	0x3
 279 0064 8F       		.byte	0x8f
 280 0065 630D0000 		.long	0xd63
 281 0069 05       		.uleb128 0x5
 282 006a 03       		.byte	0x3
 283 006b 90       		.byte	0x90
 284 006c 900D0000 		.long	0xd90
 285 0070 05       		.uleb128 0x5
 286 0071 03       		.byte	0x3
 287 0072 91       		.byte	0x91
 288 0073 AB0D0000 		.long	0xdab
 289 0077 05       		.uleb128 0x5
 290 0078 03       		.byte	0x3
 291 0079 92       		.byte	0x92
 292 007a D10D0000 		.long	0xdd1
 293 007e 05       		.uleb128 0x5
 294 007f 03       		.byte	0x3
GAS LISTING /tmp/cc45yn9z.s 			page 9


 295 0080 93       		.byte	0x93
 296 0081 EC0D0000 		.long	0xdec
 297 0085 05       		.uleb128 0x5
 298 0086 03       		.byte	0x3
 299 0087 94       		.byte	0x94
 300 0088 080E0000 		.long	0xe08
 301 008c 05       		.uleb128 0x5
 302 008d 03       		.byte	0x3
 303 008e 95       		.byte	0x95
 304 008f 240E0000 		.long	0xe24
 305 0093 05       		.uleb128 0x5
 306 0094 03       		.byte	0x3
 307 0095 96       		.byte	0x96
 308 0096 3A0E0000 		.long	0xe3a
 309 009a 05       		.uleb128 0x5
 310 009b 03       		.byte	0x3
 311 009c 97       		.byte	0x97
 312 009d 460E0000 		.long	0xe46
 313 00a1 05       		.uleb128 0x5
 314 00a2 03       		.byte	0x3
 315 00a3 98       		.byte	0x98
 316 00a4 6C0E0000 		.long	0xe6c
 317 00a8 05       		.uleb128 0x5
 318 00a9 03       		.byte	0x3
 319 00aa 99       		.byte	0x99
 320 00ab 910E0000 		.long	0xe91
 321 00af 05       		.uleb128 0x5
 322 00b0 03       		.byte	0x3
 323 00b1 9A       		.byte	0x9a
 324 00b2 B20E0000 		.long	0xeb2
 325 00b6 05       		.uleb128 0x5
 326 00b7 03       		.byte	0x3
 327 00b8 9B       		.byte	0x9b
 328 00b9 DD0E0000 		.long	0xedd
 329 00bd 05       		.uleb128 0x5
 330 00be 03       		.byte	0x3
 331 00bf 9C       		.byte	0x9c
 332 00c0 F80E0000 		.long	0xef8
 333 00c4 05       		.uleb128 0x5
 334 00c5 03       		.byte	0x3
 335 00c6 9E       		.byte	0x9e
 336 00c7 0E0F0000 		.long	0xf0e
 337 00cb 05       		.uleb128 0x5
 338 00cc 03       		.byte	0x3
 339 00cd A0       		.byte	0xa0
 340 00ce 2F0F0000 		.long	0xf2f
 341 00d2 05       		.uleb128 0x5
 342 00d3 03       		.byte	0x3
 343 00d4 A1       		.byte	0xa1
 344 00d5 4B0F0000 		.long	0xf4b
 345 00d9 05       		.uleb128 0x5
 346 00da 03       		.byte	0x3
 347 00db A2       		.byte	0xa2
 348 00dc 660F0000 		.long	0xf66
 349 00e0 05       		.uleb128 0x5
 350 00e1 03       		.byte	0x3
 351 00e2 A4       		.byte	0xa4
GAS LISTING /tmp/cc45yn9z.s 			page 10


 352 00e3 8C0F0000 		.long	0xf8c
 353 00e7 05       		.uleb128 0x5
 354 00e8 03       		.byte	0x3
 355 00e9 A7       		.byte	0xa7
 356 00ea AC0F0000 		.long	0xfac
 357 00ee 05       		.uleb128 0x5
 358 00ef 03       		.byte	0x3
 359 00f0 AA       		.byte	0xaa
 360 00f1 D10F0000 		.long	0xfd1
 361 00f5 05       		.uleb128 0x5
 362 00f6 03       		.byte	0x3
 363 00f7 AC       		.byte	0xac
 364 00f8 F10F0000 		.long	0xff1
 365 00fc 05       		.uleb128 0x5
 366 00fd 03       		.byte	0x3
 367 00fe AE       		.byte	0xae
 368 00ff 0C100000 		.long	0x100c
 369 0103 05       		.uleb128 0x5
 370 0104 03       		.byte	0x3
 371 0105 B0       		.byte	0xb0
 372 0106 27100000 		.long	0x1027
 373 010a 05       		.uleb128 0x5
 374 010b 03       		.byte	0x3
 375 010c B1       		.byte	0xb1
 376 010d 4D100000 		.long	0x104d
 377 0111 05       		.uleb128 0x5
 378 0112 03       		.byte	0x3
 379 0113 B2       		.byte	0xb2
 380 0114 67100000 		.long	0x1067
 381 0118 05       		.uleb128 0x5
 382 0119 03       		.byte	0x3
 383 011a B3       		.byte	0xb3
 384 011b 81100000 		.long	0x1081
 385 011f 05       		.uleb128 0x5
 386 0120 03       		.byte	0x3
 387 0121 B4       		.byte	0xb4
 388 0122 9B100000 		.long	0x109b
 389 0126 05       		.uleb128 0x5
 390 0127 03       		.byte	0x3
 391 0128 B5       		.byte	0xb5
 392 0129 B5100000 		.long	0x10b5
 393 012d 05       		.uleb128 0x5
 394 012e 03       		.byte	0x3
 395 012f B6       		.byte	0xb6
 396 0130 CF100000 		.long	0x10cf
 397 0134 05       		.uleb128 0x5
 398 0135 03       		.byte	0x3
 399 0136 B7       		.byte	0xb7
 400 0137 8F110000 		.long	0x118f
 401 013b 05       		.uleb128 0x5
 402 013c 03       		.byte	0x3
 403 013d B8       		.byte	0xb8
 404 013e A5110000 		.long	0x11a5
 405 0142 05       		.uleb128 0x5
 406 0143 03       		.byte	0x3
 407 0144 B9       		.byte	0xb9
 408 0145 C4110000 		.long	0x11c4
GAS LISTING /tmp/cc45yn9z.s 			page 11


 409 0149 05       		.uleb128 0x5
 410 014a 03       		.byte	0x3
 411 014b BA       		.byte	0xba
 412 014c E3110000 		.long	0x11e3
 413 0150 05       		.uleb128 0x5
 414 0151 03       		.byte	0x3
 415 0152 BB       		.byte	0xbb
 416 0153 02120000 		.long	0x1202
 417 0157 05       		.uleb128 0x5
 418 0158 03       		.byte	0x3
 419 0159 BC       		.byte	0xbc
 420 015a 2D120000 		.long	0x122d
 421 015e 05       		.uleb128 0x5
 422 015f 03       		.byte	0x3
 423 0160 BD       		.byte	0xbd
 424 0161 48120000 		.long	0x1248
 425 0165 05       		.uleb128 0x5
 426 0166 03       		.byte	0x3
 427 0167 BF       		.byte	0xbf
 428 0168 70120000 		.long	0x1270
 429 016c 05       		.uleb128 0x5
 430 016d 03       		.byte	0x3
 431 016e C1       		.byte	0xc1
 432 016f 92120000 		.long	0x1292
 433 0173 05       		.uleb128 0x5
 434 0174 03       		.byte	0x3
 435 0175 C2       		.byte	0xc2
 436 0176 B2120000 		.long	0x12b2
 437 017a 05       		.uleb128 0x5
 438 017b 03       		.byte	0x3
 439 017c C3       		.byte	0xc3
 440 017d D9120000 		.long	0x12d9
 441 0181 05       		.uleb128 0x5
 442 0182 03       		.byte	0x3
 443 0183 C4       		.byte	0xc4
 444 0184 F9120000 		.long	0x12f9
 445 0188 05       		.uleb128 0x5
 446 0189 03       		.byte	0x3
 447 018a C5       		.byte	0xc5
 448 018b 18130000 		.long	0x1318
 449 018f 05       		.uleb128 0x5
 450 0190 03       		.byte	0x3
 451 0191 C6       		.byte	0xc6
 452 0192 2E130000 		.long	0x132e
 453 0196 05       		.uleb128 0x5
 454 0197 03       		.byte	0x3
 455 0198 C7       		.byte	0xc7
 456 0199 4E130000 		.long	0x134e
 457 019d 05       		.uleb128 0x5
 458 019e 03       		.byte	0x3
 459 019f C8       		.byte	0xc8
 460 01a0 6E130000 		.long	0x136e
 461 01a4 05       		.uleb128 0x5
 462 01a5 03       		.byte	0x3
 463 01a6 C9       		.byte	0xc9
 464 01a7 8E130000 		.long	0x138e
 465 01ab 05       		.uleb128 0x5
GAS LISTING /tmp/cc45yn9z.s 			page 12


 466 01ac 03       		.byte	0x3
 467 01ad CA       		.byte	0xca
 468 01ae AE130000 		.long	0x13ae
 469 01b2 05       		.uleb128 0x5
 470 01b3 03       		.byte	0x3
 471 01b4 CB       		.byte	0xcb
 472 01b5 C5130000 		.long	0x13c5
 473 01b9 05       		.uleb128 0x5
 474 01ba 03       		.byte	0x3
 475 01bb CC       		.byte	0xcc
 476 01bc DC130000 		.long	0x13dc
 477 01c0 05       		.uleb128 0x5
 478 01c1 03       		.byte	0x3
 479 01c2 CD       		.byte	0xcd
 480 01c3 FA130000 		.long	0x13fa
 481 01c7 05       		.uleb128 0x5
 482 01c8 03       		.byte	0x3
 483 01c9 CE       		.byte	0xce
 484 01ca 19140000 		.long	0x1419
 485 01ce 05       		.uleb128 0x5
 486 01cf 03       		.byte	0x3
 487 01d0 CF       		.byte	0xcf
 488 01d1 37140000 		.long	0x1437
 489 01d5 05       		.uleb128 0x5
 490 01d6 03       		.byte	0x3
 491 01d7 D0       		.byte	0xd0
 492 01d8 56140000 		.long	0x1456
 493 01dc 06       		.uleb128 0x6
 494 01dd 03       		.byte	0x3
 495 01de 0801     		.value	0x108
 496 01e0 7A140000 		.long	0x147a
 497 01e4 06       		.uleb128 0x6
 498 01e5 03       		.byte	0x3
 499 01e6 0901     		.value	0x109
 500 01e8 9C140000 		.long	0x149c
 501 01ec 06       		.uleb128 0x6
 502 01ed 03       		.byte	0x3
 503 01ee 0A01     		.value	0x10a
 504 01f0 C3140000 		.long	0x14c3
 505 01f4 03       		.uleb128 0x3
 506 01f5 00000000 		.long	.LASF1
 507 01f9 05       		.byte	0x5
 508 01fa 30       		.byte	0x30
 509 01fb 07       		.uleb128 0x7
 510 01fc 00000000 		.long	.LASF110
 511 0200 01       		.byte	0x1
 512 0201 06       		.byte	0x6
 513 0202 E9       		.byte	0xe9
 514 0203 C3030000 		.long	0x3c3
 515 0207 08       		.uleb128 0x8
 516 0208 00000000 		.long	.LASF2
 517 020c 06       		.byte	0x6
 518 020d EB       		.byte	0xeb
 519 020e F60C0000 		.long	0xcf6
 520 0212 08       		.uleb128 0x8
 521 0213 00000000 		.long	.LASF3
 522 0217 06       		.byte	0x6
GAS LISTING /tmp/cc45yn9z.s 			page 13


 523 0218 EC       		.byte	0xec
 524 0219 FD0C0000 		.long	0xcfd
 525 021d 09       		.uleb128 0x9
 526 021e 00000000 		.long	.LASF16
 527 0222 06       		.byte	0x6
 528 0223 F2       		.byte	0xf2
 529 0224 00000000 		.long	.LASF340
 530 0228 37020000 		.long	0x237
 531 022c 0A       		.uleb128 0xa
 532 022d 12150000 		.long	0x1512
 533 0231 0A       		.uleb128 0xa
 534 0232 18150000 		.long	0x1518
 535 0236 00       		.byte	0
 536 0237 0B       		.uleb128 0xb
 537 0238 07020000 		.long	0x207
 538 023c 0C       		.uleb128 0xc
 539 023d 657100   		.string	"eq"
 540 0240 06       		.byte	0x6
 541 0241 F6       		.byte	0xf6
 542 0242 00000000 		.long	.LASF4
 543 0246 1E150000 		.long	0x151e
 544 024a 59020000 		.long	0x259
 545 024e 0A       		.uleb128 0xa
 546 024f 18150000 		.long	0x1518
 547 0253 0A       		.uleb128 0xa
 548 0254 18150000 		.long	0x1518
 549 0258 00       		.byte	0
 550 0259 0C       		.uleb128 0xc
 551 025a 6C7400   		.string	"lt"
 552 025d 06       		.byte	0x6
 553 025e FA       		.byte	0xfa
 554 025f 00000000 		.long	.LASF5
 555 0263 1E150000 		.long	0x151e
 556 0267 76020000 		.long	0x276
 557 026b 0A       		.uleb128 0xa
 558 026c 18150000 		.long	0x1518
 559 0270 0A       		.uleb128 0xa
 560 0271 18150000 		.long	0x1518
 561 0275 00       		.byte	0
 562 0276 0D       		.uleb128 0xd
 563 0277 00000000 		.long	.LASF6
 564 027b 06       		.byte	0x6
 565 027c 0201     		.value	0x102
 566 027e 00000000 		.long	.LASF8
 567 0282 FD0C0000 		.long	0xcfd
 568 0286 9A020000 		.long	0x29a
 569 028a 0A       		.uleb128 0xa
 570 028b 25150000 		.long	0x1525
 571 028f 0A       		.uleb128 0xa
 572 0290 25150000 		.long	0x1525
 573 0294 0A       		.uleb128 0xa
 574 0295 C3030000 		.long	0x3c3
 575 0299 00       		.byte	0
 576 029a 0D       		.uleb128 0xd
 577 029b 00000000 		.long	.LASF7
 578 029f 06       		.byte	0x6
 579 02a0 0A01     		.value	0x10a
GAS LISTING /tmp/cc45yn9z.s 			page 14


 580 02a2 00000000 		.long	.LASF9
 581 02a6 C3030000 		.long	0x3c3
 582 02aa B4020000 		.long	0x2b4
 583 02ae 0A       		.uleb128 0xa
 584 02af 25150000 		.long	0x1525
 585 02b3 00       		.byte	0
 586 02b4 0D       		.uleb128 0xd
 587 02b5 00000000 		.long	.LASF10
 588 02b9 06       		.byte	0x6
 589 02ba 0E01     		.value	0x10e
 590 02bc 00000000 		.long	.LASF11
 591 02c0 25150000 		.long	0x1525
 592 02c4 D8020000 		.long	0x2d8
 593 02c8 0A       		.uleb128 0xa
 594 02c9 25150000 		.long	0x1525
 595 02cd 0A       		.uleb128 0xa
 596 02ce C3030000 		.long	0x3c3
 597 02d2 0A       		.uleb128 0xa
 598 02d3 18150000 		.long	0x1518
 599 02d7 00       		.byte	0
 600 02d8 0D       		.uleb128 0xd
 601 02d9 00000000 		.long	.LASF12
 602 02dd 06       		.byte	0x6
 603 02de 1601     		.value	0x116
 604 02e0 00000000 		.long	.LASF13
 605 02e4 2B150000 		.long	0x152b
 606 02e8 FC020000 		.long	0x2fc
 607 02ec 0A       		.uleb128 0xa
 608 02ed 2B150000 		.long	0x152b
 609 02f1 0A       		.uleb128 0xa
 610 02f2 25150000 		.long	0x1525
 611 02f6 0A       		.uleb128 0xa
 612 02f7 C3030000 		.long	0x3c3
 613 02fb 00       		.byte	0
 614 02fc 0D       		.uleb128 0xd
 615 02fd 00000000 		.long	.LASF14
 616 0301 06       		.byte	0x6
 617 0302 1E01     		.value	0x11e
 618 0304 00000000 		.long	.LASF15
 619 0308 2B150000 		.long	0x152b
 620 030c 20030000 		.long	0x320
 621 0310 0A       		.uleb128 0xa
 622 0311 2B150000 		.long	0x152b
 623 0315 0A       		.uleb128 0xa
 624 0316 25150000 		.long	0x1525
 625 031a 0A       		.uleb128 0xa
 626 031b C3030000 		.long	0x3c3
 627 031f 00       		.byte	0
 628 0320 0D       		.uleb128 0xd
 629 0321 00000000 		.long	.LASF16
 630 0325 06       		.byte	0x6
 631 0326 2601     		.value	0x126
 632 0328 00000000 		.long	.LASF17
 633 032c 2B150000 		.long	0x152b
 634 0330 44030000 		.long	0x344
 635 0334 0A       		.uleb128 0xa
 636 0335 2B150000 		.long	0x152b
GAS LISTING /tmp/cc45yn9z.s 			page 15


 637 0339 0A       		.uleb128 0xa
 638 033a C3030000 		.long	0x3c3
 639 033e 0A       		.uleb128 0xa
 640 033f 07020000 		.long	0x207
 641 0343 00       		.byte	0
 642 0344 0D       		.uleb128 0xd
 643 0345 00000000 		.long	.LASF18
 644 0349 06       		.byte	0x6
 645 034a 2E01     		.value	0x12e
 646 034c 00000000 		.long	.LASF19
 647 0350 07020000 		.long	0x207
 648 0354 5E030000 		.long	0x35e
 649 0358 0A       		.uleb128 0xa
 650 0359 31150000 		.long	0x1531
 651 035d 00       		.byte	0
 652 035e 0B       		.uleb128 0xb
 653 035f 12020000 		.long	0x212
 654 0363 0D       		.uleb128 0xd
 655 0364 00000000 		.long	.LASF20
 656 0368 06       		.byte	0x6
 657 0369 3401     		.value	0x134
 658 036b 00000000 		.long	.LASF21
 659 036f 12020000 		.long	0x212
 660 0373 7D030000 		.long	0x37d
 661 0377 0A       		.uleb128 0xa
 662 0378 18150000 		.long	0x1518
 663 037c 00       		.byte	0
 664 037d 0D       		.uleb128 0xd
 665 037e 00000000 		.long	.LASF22
 666 0382 06       		.byte	0x6
 667 0383 3801     		.value	0x138
 668 0385 00000000 		.long	.LASF23
 669 0389 1E150000 		.long	0x151e
 670 038d 9C030000 		.long	0x39c
 671 0391 0A       		.uleb128 0xa
 672 0392 31150000 		.long	0x1531
 673 0396 0A       		.uleb128 0xa
 674 0397 31150000 		.long	0x1531
 675 039b 00       		.byte	0
 676 039c 0E       		.uleb128 0xe
 677 039d 656F6600 		.string	"eof"
 678 03a1 06       		.byte	0x6
 679 03a2 3C01     		.value	0x13c
 680 03a4 00000000 		.long	.LASF341
 681 03a8 12020000 		.long	0x212
 682 03ac 0F       		.uleb128 0xf
 683 03ad 00000000 		.long	.LASF24
 684 03b1 06       		.byte	0x6
 685 03b2 4001     		.value	0x140
 686 03b4 00000000 		.long	.LASF342
 687 03b8 12020000 		.long	0x212
 688 03bc 0A       		.uleb128 0xa
 689 03bd 31150000 		.long	0x1531
 690 03c1 00       		.byte	0
 691 03c2 00       		.byte	0
 692 03c3 08       		.uleb128 0x8
 693 03c4 00000000 		.long	.LASF25
GAS LISTING /tmp/cc45yn9z.s 			page 16


 694 03c8 04       		.byte	0x4
 695 03c9 C4       		.byte	0xc4
 696 03ca 8F0C0000 		.long	0xc8f
 697 03ce 05       		.uleb128 0x5
 698 03cf 07       		.byte	0x7
 699 03d0 35       		.byte	0x35
 700 03d1 37150000 		.long	0x1537
 701 03d5 05       		.uleb128 0x5
 702 03d6 07       		.byte	0x7
 703 03d7 36       		.byte	0x36
 704 03d8 64160000 		.long	0x1664
 705 03dc 05       		.uleb128 0x5
 706 03dd 07       		.byte	0x7
 707 03de 37       		.byte	0x37
 708 03df 7E160000 		.long	0x167e
 709 03e3 08       		.uleb128 0x8
 710 03e4 00000000 		.long	.LASF26
 711 03e8 04       		.byte	0x4
 712 03e9 C5       		.byte	0xc5
 713 03ea D2120000 		.long	0x12d2
 714 03ee 10       		.uleb128 0x10
 715 03ef 00000000 		.long	.LASF48
 716 03f3 04       		.byte	0x4
 717 03f4 FD0C0000 		.long	0xcfd
 718 03f8 08       		.byte	0x8
 719 03f9 39       		.byte	0x39
 720 03fa 8F040000 		.long	0x48f
 721 03fe 11       		.uleb128 0x11
 722 03ff 00000000 		.long	.LASF27
 723 0403 01       		.byte	0x1
 724 0404 11       		.uleb128 0x11
 725 0405 00000000 		.long	.LASF28
 726 0409 02       		.byte	0x2
 727 040a 11       		.uleb128 0x11
 728 040b 00000000 		.long	.LASF29
 729 040f 04       		.byte	0x4
 730 0410 11       		.uleb128 0x11
 731 0411 00000000 		.long	.LASF30
 732 0415 08       		.byte	0x8
 733 0416 11       		.uleb128 0x11
 734 0417 00000000 		.long	.LASF31
 735 041b 10       		.byte	0x10
 736 041c 11       		.uleb128 0x11
 737 041d 00000000 		.long	.LASF32
 738 0421 20       		.byte	0x20
 739 0422 11       		.uleb128 0x11
 740 0423 00000000 		.long	.LASF33
 741 0427 40       		.byte	0x40
 742 0428 11       		.uleb128 0x11
 743 0429 00000000 		.long	.LASF34
 744 042d 80       		.byte	0x80
 745 042e 12       		.uleb128 0x12
 746 042f 00000000 		.long	.LASF35
 747 0433 0001     		.value	0x100
 748 0435 12       		.uleb128 0x12
 749 0436 00000000 		.long	.LASF36
 750 043a 0002     		.value	0x200
GAS LISTING /tmp/cc45yn9z.s 			page 17


 751 043c 12       		.uleb128 0x12
 752 043d 00000000 		.long	.LASF37
 753 0441 0004     		.value	0x400
 754 0443 12       		.uleb128 0x12
 755 0444 00000000 		.long	.LASF38
 756 0448 0008     		.value	0x800
 757 044a 12       		.uleb128 0x12
 758 044b 00000000 		.long	.LASF39
 759 044f 0010     		.value	0x1000
 760 0451 12       		.uleb128 0x12
 761 0452 00000000 		.long	.LASF40
 762 0456 0020     		.value	0x2000
 763 0458 12       		.uleb128 0x12
 764 0459 00000000 		.long	.LASF41
 765 045d 0040     		.value	0x4000
 766 045f 11       		.uleb128 0x11
 767 0460 00000000 		.long	.LASF42
 768 0464 B0       		.byte	0xb0
 769 0465 11       		.uleb128 0x11
 770 0466 00000000 		.long	.LASF43
 771 046a 4A       		.byte	0x4a
 772 046b 12       		.uleb128 0x12
 773 046c 00000000 		.long	.LASF44
 774 0470 0401     		.value	0x104
 775 0472 13       		.uleb128 0x13
 776 0473 00000000 		.long	.LASF45
 777 0477 00000100 		.long	0x10000
 778 047b 13       		.uleb128 0x13
 779 047c 00000000 		.long	.LASF46
 780 0480 FFFFFF7F 		.long	0x7fffffff
 781 0484 14       		.uleb128 0x14
 782 0485 00000000 		.long	.LASF47
 783 0489 80808080 		.sleb128 -2147483648
 783      78
 784 048e 00       		.byte	0
 785 048f 10       		.uleb128 0x10
 786 0490 00000000 		.long	.LASF49
 787 0494 04       		.byte	0x4
 788 0495 FD0C0000 		.long	0xcfd
 789 0499 08       		.byte	0x8
 790 049a 6F       		.byte	0x6f
 791 049b E0040000 		.long	0x4e0
 792 049f 11       		.uleb128 0x11
 793 04a0 00000000 		.long	.LASF50
 794 04a4 01       		.byte	0x1
 795 04a5 11       		.uleb128 0x11
 796 04a6 00000000 		.long	.LASF51
 797 04aa 02       		.byte	0x2
 798 04ab 11       		.uleb128 0x11
 799 04ac 00000000 		.long	.LASF52
 800 04b0 04       		.byte	0x4
 801 04b1 11       		.uleb128 0x11
 802 04b2 00000000 		.long	.LASF53
 803 04b6 08       		.byte	0x8
 804 04b7 11       		.uleb128 0x11
 805 04b8 00000000 		.long	.LASF54
 806 04bc 10       		.byte	0x10
GAS LISTING /tmp/cc45yn9z.s 			page 18


 807 04bd 11       		.uleb128 0x11
 808 04be 00000000 		.long	.LASF55
 809 04c2 20       		.byte	0x20
 810 04c3 13       		.uleb128 0x13
 811 04c4 00000000 		.long	.LASF56
 812 04c8 00000100 		.long	0x10000
 813 04cc 13       		.uleb128 0x13
 814 04cd 00000000 		.long	.LASF57
 815 04d1 FFFFFF7F 		.long	0x7fffffff
 816 04d5 14       		.uleb128 0x14
 817 04d6 00000000 		.long	.LASF58
 818 04da 80808080 		.sleb128 -2147483648
 818      78
 819 04df 00       		.byte	0
 820 04e0 10       		.uleb128 0x10
 821 04e1 00000000 		.long	.LASF59
 822 04e5 04       		.byte	0x4
 823 04e6 FD0C0000 		.long	0xcfd
 824 04ea 08       		.byte	0x8
 825 04eb 99       		.byte	0x99
 826 04ec 25050000 		.long	0x525
 827 04f0 11       		.uleb128 0x11
 828 04f1 00000000 		.long	.LASF60
 829 04f5 00       		.byte	0
 830 04f6 11       		.uleb128 0x11
 831 04f7 00000000 		.long	.LASF61
 832 04fb 01       		.byte	0x1
 833 04fc 11       		.uleb128 0x11
 834 04fd 00000000 		.long	.LASF62
 835 0501 02       		.byte	0x2
 836 0502 11       		.uleb128 0x11
 837 0503 00000000 		.long	.LASF63
 838 0507 04       		.byte	0x4
 839 0508 13       		.uleb128 0x13
 840 0509 00000000 		.long	.LASF64
 841 050d 00000100 		.long	0x10000
 842 0511 13       		.uleb128 0x13
 843 0512 00000000 		.long	.LASF65
 844 0516 FFFFFF7F 		.long	0x7fffffff
 845 051a 14       		.uleb128 0x14
 846 051b 00000000 		.long	.LASF66
 847 051f 80808080 		.sleb128 -2147483648
 847      78
 848 0524 00       		.byte	0
 849 0525 10       		.uleb128 0x10
 850 0526 00000000 		.long	.LASF67
 851 052a 04       		.byte	0x4
 852 052b 7B0C0000 		.long	0xc7b
 853 052f 08       		.byte	0x8
 854 0530 C1       		.byte	0xc1
 855 0531 51050000 		.long	0x551
 856 0535 11       		.uleb128 0x11
 857 0536 00000000 		.long	.LASF68
 858 053a 00       		.byte	0
 859 053b 11       		.uleb128 0x11
 860 053c 00000000 		.long	.LASF69
 861 0540 01       		.byte	0x1
GAS LISTING /tmp/cc45yn9z.s 			page 19


 862 0541 11       		.uleb128 0x11
 863 0542 00000000 		.long	.LASF70
 864 0546 02       		.byte	0x2
 865 0547 13       		.uleb128 0x13
 866 0548 00000000 		.long	.LASF71
 867 054c 00000100 		.long	0x10000
 868 0550 00       		.byte	0
 869 0551 15       		.uleb128 0x15
 870 0552 00000000 		.long	.LASF101
 871 0556 BA070000 		.long	0x7ba
 872 055a 16       		.uleb128 0x16
 873 055b 00000000 		.long	.LASF343
 874 055f 01       		.byte	0x1
 875 0560 08       		.byte	0x8
 876 0561 5902     		.value	0x259
 877 0563 01       		.byte	0x1
 878 0564 B8050000 		.long	0x5b8
 879 0568 17       		.uleb128 0x17
 880 0569 00000000 		.long	.LASF72
 881 056d 08       		.byte	0x8
 882 056e 6102     		.value	0x261
 883 0570 B0160000 		.long	0x16b0
 884 0574 17       		.uleb128 0x17
 885 0575 00000000 		.long	.LASF73
 886 0579 08       		.byte	0x8
 887 057a 6202     		.value	0x262
 888 057c 1E150000 		.long	0x151e
 889 0580 18       		.uleb128 0x18
 890 0581 00000000 		.long	.LASF343
 891 0585 08       		.byte	0x8
 892 0586 5D02     		.value	0x25d
 893 0588 00000000 		.long	.LASF344
 894 058c 01       		.byte	0x1
 895 058d 95050000 		.long	0x595
 896 0591 9B050000 		.long	0x59b
 897 0595 19       		.uleb128 0x19
 898 0596 C5160000 		.long	0x16c5
 899 059a 00       		.byte	0
 900 059b 1A       		.uleb128 0x1a
 901 059c 00000000 		.long	.LASF74
 902 05a0 08       		.byte	0x8
 903 05a1 5E02     		.value	0x25e
 904 05a3 00000000 		.long	.LASF75
 905 05a7 01       		.byte	0x1
 906 05a8 AC050000 		.long	0x5ac
 907 05ac 19       		.uleb128 0x19
 908 05ad C5160000 		.long	0x16c5
 909 05b1 19       		.uleb128 0x19
 910 05b2 FD0C0000 		.long	0xcfd
 911 05b6 00       		.byte	0
 912 05b7 00       		.byte	0
 913 05b8 1B       		.uleb128 0x1b
 914 05b9 00000000 		.long	.LASF76
 915 05bd 08       		.byte	0x8
 916 05be AD01     		.value	0x1ad
 917 05c0 8F040000 		.long	0x48f
 918 05c4 01       		.byte	0x1
GAS LISTING /tmp/cc45yn9z.s 			page 20


 919 05c5 1B       		.uleb128 0x1b
 920 05c6 00000000 		.long	.LASF77
 921 05ca 08       		.byte	0x8
 922 05cb CD01     		.value	0x1cd
 923 05cd 25050000 		.long	0x525
 924 05d1 01       		.byte	0x1
 925 05d2 1B       		.uleb128 0x1b
 926 05d3 00000000 		.long	.LASF78
 927 05d7 08       		.byte	0x8
 928 05d8 4301     		.value	0x143
 929 05da EE030000 		.long	0x3ee
 930 05de 01       		.byte	0x1
 931 05df 1C       		.uleb128 0x1c
 932 05e0 00000000 		.long	.LASF79
 933 05e4 08       		.byte	0x8
 934 05e5 4601     		.value	0x146
 935 05e7 ED050000 		.long	0x5ed
 936 05eb 01       		.byte	0x1
 937 05ec 01       		.byte	0x1
 938 05ed 0B       		.uleb128 0xb
 939 05ee D2050000 		.long	0x5d2
 940 05f2 1D       		.uleb128 0x1d
 941 05f3 64656300 		.string	"dec"
 942 05f7 08       		.byte	0x8
 943 05f8 4901     		.value	0x149
 944 05fa ED050000 		.long	0x5ed
 945 05fe 01       		.byte	0x1
 946 05ff 02       		.byte	0x2
 947 0600 1C       		.uleb128 0x1c
 948 0601 00000000 		.long	.LASF80
 949 0605 08       		.byte	0x8
 950 0606 4C01     		.value	0x14c
 951 0608 ED050000 		.long	0x5ed
 952 060c 01       		.byte	0x1
 953 060d 04       		.byte	0x4
 954 060e 1D       		.uleb128 0x1d
 955 060f 68657800 		.string	"hex"
 956 0613 08       		.byte	0x8
 957 0614 4F01     		.value	0x14f
 958 0616 ED050000 		.long	0x5ed
 959 061a 01       		.byte	0x1
 960 061b 08       		.byte	0x8
 961 061c 1C       		.uleb128 0x1c
 962 061d 00000000 		.long	.LASF81
 963 0621 08       		.byte	0x8
 964 0622 5401     		.value	0x154
 965 0624 ED050000 		.long	0x5ed
 966 0628 01       		.byte	0x1
 967 0629 10       		.byte	0x10
 968 062a 1C       		.uleb128 0x1c
 969 062b 00000000 		.long	.LASF82
 970 062f 08       		.byte	0x8
 971 0630 5801     		.value	0x158
 972 0632 ED050000 		.long	0x5ed
 973 0636 01       		.byte	0x1
 974 0637 20       		.byte	0x20
 975 0638 1D       		.uleb128 0x1d
GAS LISTING /tmp/cc45yn9z.s 			page 21


 976 0639 6F637400 		.string	"oct"
 977 063d 08       		.byte	0x8
 978 063e 5B01     		.value	0x15b
 979 0640 ED050000 		.long	0x5ed
 980 0644 01       		.byte	0x1
 981 0645 40       		.byte	0x40
 982 0646 1C       		.uleb128 0x1c
 983 0647 00000000 		.long	.LASF83
 984 064b 08       		.byte	0x8
 985 064c 5F01     		.value	0x15f
 986 064e ED050000 		.long	0x5ed
 987 0652 01       		.byte	0x1
 988 0653 80       		.byte	0x80
 989 0654 1E       		.uleb128 0x1e
 990 0655 00000000 		.long	.LASF84
 991 0659 08       		.byte	0x8
 992 065a 6201     		.value	0x162
 993 065c ED050000 		.long	0x5ed
 994 0660 01       		.byte	0x1
 995 0661 0001     		.value	0x100
 996 0663 1E       		.uleb128 0x1e
 997 0664 00000000 		.long	.LASF85
 998 0668 08       		.byte	0x8
 999 0669 6601     		.value	0x166
 1000 066b ED050000 		.long	0x5ed
 1001 066f 01       		.byte	0x1
 1002 0670 0002     		.value	0x200
 1003 0672 1E       		.uleb128 0x1e
 1004 0673 00000000 		.long	.LASF86
 1005 0677 08       		.byte	0x8
 1006 0678 6A01     		.value	0x16a
 1007 067a ED050000 		.long	0x5ed
 1008 067e 01       		.byte	0x1
 1009 067f 0004     		.value	0x400
 1010 0681 1E       		.uleb128 0x1e
 1011 0682 00000000 		.long	.LASF87
 1012 0686 08       		.byte	0x8
 1013 0687 6D01     		.value	0x16d
 1014 0689 ED050000 		.long	0x5ed
 1015 068d 01       		.byte	0x1
 1016 068e 0008     		.value	0x800
 1017 0690 1E       		.uleb128 0x1e
 1018 0691 00000000 		.long	.LASF88
 1019 0695 08       		.byte	0x8
 1020 0696 7001     		.value	0x170
 1021 0698 ED050000 		.long	0x5ed
 1022 069c 01       		.byte	0x1
 1023 069d 0010     		.value	0x1000
 1024 069f 1E       		.uleb128 0x1e
 1025 06a0 00000000 		.long	.LASF89
 1026 06a4 08       		.byte	0x8
 1027 06a5 7301     		.value	0x173
 1028 06a7 ED050000 		.long	0x5ed
 1029 06ab 01       		.byte	0x1
 1030 06ac 0020     		.value	0x2000
 1031 06ae 1E       		.uleb128 0x1e
 1032 06af 00000000 		.long	.LASF90
GAS LISTING /tmp/cc45yn9z.s 			page 22


 1033 06b3 08       		.byte	0x8
 1034 06b4 7701     		.value	0x177
 1035 06b6 ED050000 		.long	0x5ed
 1036 06ba 01       		.byte	0x1
 1037 06bb 0040     		.value	0x4000
 1038 06bd 1C       		.uleb128 0x1c
 1039 06be 00000000 		.long	.LASF91
 1040 06c2 08       		.byte	0x8
 1041 06c3 7A01     		.value	0x17a
 1042 06c5 ED050000 		.long	0x5ed
 1043 06c9 01       		.byte	0x1
 1044 06ca B0       		.byte	0xb0
 1045 06cb 1C       		.uleb128 0x1c
 1046 06cc 00000000 		.long	.LASF92
 1047 06d0 08       		.byte	0x8
 1048 06d1 7D01     		.value	0x17d
 1049 06d3 ED050000 		.long	0x5ed
 1050 06d7 01       		.byte	0x1
 1051 06d8 4A       		.byte	0x4a
 1052 06d9 1E       		.uleb128 0x1e
 1053 06da 00000000 		.long	.LASF93
 1054 06de 08       		.byte	0x8
 1055 06df 8001     		.value	0x180
 1056 06e1 ED050000 		.long	0x5ed
 1057 06e5 01       		.byte	0x1
 1058 06e6 0401     		.value	0x104
 1059 06e8 1B       		.uleb128 0x1b
 1060 06e9 00000000 		.long	.LASF94
 1061 06ed 08       		.byte	0x8
 1062 06ee 8E01     		.value	0x18e
 1063 06f0 E0040000 		.long	0x4e0
 1064 06f4 01       		.byte	0x1
 1065 06f5 1C       		.uleb128 0x1c
 1066 06f6 00000000 		.long	.LASF95
 1067 06fa 08       		.byte	0x8
 1068 06fb 9201     		.value	0x192
 1069 06fd 03070000 		.long	0x703
 1070 0701 01       		.byte	0x1
 1071 0702 01       		.byte	0x1
 1072 0703 0B       		.uleb128 0xb
 1073 0704 E8060000 		.long	0x6e8
 1074 0708 1C       		.uleb128 0x1c
 1075 0709 00000000 		.long	.LASF96
 1076 070d 08       		.byte	0x8
 1077 070e 9501     		.value	0x195
 1078 0710 03070000 		.long	0x703
 1079 0714 01       		.byte	0x1
 1080 0715 02       		.byte	0x2
 1081 0716 1C       		.uleb128 0x1c
 1082 0717 00000000 		.long	.LASF97
 1083 071b 08       		.byte	0x8
 1084 071c 9A01     		.value	0x19a
 1085 071e 03070000 		.long	0x703
 1086 0722 01       		.byte	0x1
 1087 0723 04       		.byte	0x4
 1088 0724 1C       		.uleb128 0x1c
 1089 0725 00000000 		.long	.LASF98
GAS LISTING /tmp/cc45yn9z.s 			page 23


 1090 0729 08       		.byte	0x8
 1091 072a 9D01     		.value	0x19d
 1092 072c 03070000 		.long	0x703
 1093 0730 01       		.byte	0x1
 1094 0731 00       		.byte	0
 1095 0732 1D       		.uleb128 0x1d
 1096 0733 61707000 		.string	"app"
 1097 0737 08       		.byte	0x8
 1098 0738 B001     		.value	0x1b0
 1099 073a 40070000 		.long	0x740
 1100 073e 01       		.byte	0x1
 1101 073f 01       		.byte	0x1
 1102 0740 0B       		.uleb128 0xb
 1103 0741 B8050000 		.long	0x5b8
 1104 0745 1D       		.uleb128 0x1d
 1105 0746 61746500 		.string	"ate"
 1106 074a 08       		.byte	0x8
 1107 074b B301     		.value	0x1b3
 1108 074d 40070000 		.long	0x740
 1109 0751 01       		.byte	0x1
 1110 0752 02       		.byte	0x2
 1111 0753 1C       		.uleb128 0x1c
 1112 0754 00000000 		.long	.LASF99
 1113 0758 08       		.byte	0x8
 1114 0759 B801     		.value	0x1b8
 1115 075b 40070000 		.long	0x740
 1116 075f 01       		.byte	0x1
 1117 0760 04       		.byte	0x4
 1118 0761 1D       		.uleb128 0x1d
 1119 0762 696E00   		.string	"in"
 1120 0765 08       		.byte	0x8
 1121 0766 BB01     		.value	0x1bb
 1122 0768 40070000 		.long	0x740
 1123 076c 01       		.byte	0x1
 1124 076d 08       		.byte	0x8
 1125 076e 1D       		.uleb128 0x1d
 1126 076f 6F757400 		.string	"out"
 1127 0773 08       		.byte	0x8
 1128 0774 BE01     		.value	0x1be
 1129 0776 40070000 		.long	0x740
 1130 077a 01       		.byte	0x1
 1131 077b 10       		.byte	0x10
 1132 077c 1C       		.uleb128 0x1c
 1133 077d 00000000 		.long	.LASF100
 1134 0781 08       		.byte	0x8
 1135 0782 C101     		.value	0x1c1
 1136 0784 40070000 		.long	0x740
 1137 0788 01       		.byte	0x1
 1138 0789 20       		.byte	0x20
 1139 078a 1D       		.uleb128 0x1d
 1140 078b 62656700 		.string	"beg"
 1141 078f 08       		.byte	0x8
 1142 0790 D001     		.value	0x1d0
 1143 0792 98070000 		.long	0x798
 1144 0796 01       		.byte	0x1
 1145 0797 00       		.byte	0
 1146 0798 0B       		.uleb128 0xb
GAS LISTING /tmp/cc45yn9z.s 			page 24


 1147 0799 C5050000 		.long	0x5c5
 1148 079d 1D       		.uleb128 0x1d
 1149 079e 63757200 		.string	"cur"
 1150 07a2 08       		.byte	0x8
 1151 07a3 D301     		.value	0x1d3
 1152 07a5 98070000 		.long	0x798
 1153 07a9 01       		.byte	0x1
 1154 07aa 01       		.byte	0x1
 1155 07ab 1D       		.uleb128 0x1d
 1156 07ac 656E6400 		.string	"end"
 1157 07b0 08       		.byte	0x8
 1158 07b1 D601     		.value	0x1d6
 1159 07b3 98070000 		.long	0x798
 1160 07b7 01       		.byte	0x1
 1161 07b8 02       		.byte	0x2
 1162 07b9 00       		.byte	0
 1163 07ba 05       		.uleb128 0x5
 1164 07bb 09       		.byte	0x9
 1165 07bc 52       		.byte	0x52
 1166 07bd D6160000 		.long	0x16d6
 1167 07c1 05       		.uleb128 0x5
 1168 07c2 09       		.byte	0x9
 1169 07c3 53       		.byte	0x53
 1170 07c4 CB160000 		.long	0x16cb
 1171 07c8 05       		.uleb128 0x5
 1172 07c9 09       		.byte	0x9
 1173 07ca 54       		.byte	0x54
 1174 07cb 960C0000 		.long	0xc96
 1175 07cf 05       		.uleb128 0x5
 1176 07d0 09       		.byte	0x9
 1177 07d1 5C       		.byte	0x5c
 1178 07d2 EC160000 		.long	0x16ec
 1179 07d6 05       		.uleb128 0x5
 1180 07d7 09       		.byte	0x9
 1181 07d8 65       		.byte	0x65
 1182 07d9 06170000 		.long	0x1706
 1183 07dd 05       		.uleb128 0x5
 1184 07de 09       		.byte	0x9
 1185 07df 68       		.byte	0x68
 1186 07e0 20170000 		.long	0x1720
 1187 07e4 05       		.uleb128 0x5
 1188 07e5 09       		.byte	0x9
 1189 07e6 69       		.byte	0x69
 1190 07e7 35170000 		.long	0x1735
 1191 07eb 15       		.uleb128 0x15
 1192 07ec 00000000 		.long	.LASF102
 1193 07f0 07080000 		.long	0x807
 1194 07f4 1F       		.uleb128 0x1f
 1195 07f5 00000000 		.long	.LASF105
 1196 07f9 F60C0000 		.long	0xcf6
 1197 07fd 20       		.uleb128 0x20
 1198 07fe 00000000 		.long	.LASF106
 1199 0802 FB010000 		.long	0x1fb
 1200 0806 00       		.byte	0
 1201 0807 05       		.uleb128 0x5
 1202 0808 0A       		.byte	0xa
 1203 0809 62       		.byte	0x62
GAS LISTING /tmp/cc45yn9z.s 			page 25


 1204 080a A40A0000 		.long	0xaa4
 1205 080e 05       		.uleb128 0x5
 1206 080f 0A       		.byte	0xa
 1207 0810 63       		.byte	0x63
 1208 0811 EE170000 		.long	0x17ee
 1209 0815 05       		.uleb128 0x5
 1210 0816 0A       		.byte	0xa
 1211 0817 65       		.byte	0x65
 1212 0818 F9170000 		.long	0x17f9
 1213 081c 05       		.uleb128 0x5
 1214 081d 0A       		.byte	0xa
 1215 081e 66       		.byte	0x66
 1216 081f 11180000 		.long	0x1811
 1217 0823 05       		.uleb128 0x5
 1218 0824 0A       		.byte	0xa
 1219 0825 67       		.byte	0x67
 1220 0826 26180000 		.long	0x1826
 1221 082a 05       		.uleb128 0x5
 1222 082b 0A       		.byte	0xa
 1223 082c 68       		.byte	0x68
 1224 082d 3C180000 		.long	0x183c
 1225 0831 05       		.uleb128 0x5
 1226 0832 0A       		.byte	0xa
 1227 0833 69       		.byte	0x69
 1228 0834 52180000 		.long	0x1852
 1229 0838 05       		.uleb128 0x5
 1230 0839 0A       		.byte	0xa
 1231 083a 6A       		.byte	0x6a
 1232 083b 67180000 		.long	0x1867
 1233 083f 05       		.uleb128 0x5
 1234 0840 0A       		.byte	0xa
 1235 0841 6B       		.byte	0x6b
 1236 0842 7D180000 		.long	0x187d
 1237 0846 05       		.uleb128 0x5
 1238 0847 0A       		.byte	0xa
 1239 0848 6C       		.byte	0x6c
 1240 0849 9E180000 		.long	0x189e
 1241 084d 05       		.uleb128 0x5
 1242 084e 0A       		.byte	0xa
 1243 084f 6D       		.byte	0x6d
 1244 0850 BE180000 		.long	0x18be
 1245 0854 05       		.uleb128 0x5
 1246 0855 0A       		.byte	0xa
 1247 0856 71       		.byte	0x71
 1248 0857 D9180000 		.long	0x18d9
 1249 085b 05       		.uleb128 0x5
 1250 085c 0A       		.byte	0xa
 1251 085d 72       		.byte	0x72
 1252 085e FE180000 		.long	0x18fe
 1253 0862 05       		.uleb128 0x5
 1254 0863 0A       		.byte	0xa
 1255 0864 74       		.byte	0x74
 1256 0865 1E190000 		.long	0x191e
 1257 0869 05       		.uleb128 0x5
 1258 086a 0A       		.byte	0xa
 1259 086b 75       		.byte	0x75
 1260 086c 3E190000 		.long	0x193e
GAS LISTING /tmp/cc45yn9z.s 			page 26


 1261 0870 05       		.uleb128 0x5
 1262 0871 0A       		.byte	0xa
 1263 0872 76       		.byte	0x76
 1264 0873 64190000 		.long	0x1964
 1265 0877 05       		.uleb128 0x5
 1266 0878 0A       		.byte	0xa
 1267 0879 78       		.byte	0x78
 1268 087a 7A190000 		.long	0x197a
 1269 087e 05       		.uleb128 0x5
 1270 087f 0A       		.byte	0xa
 1271 0880 79       		.byte	0x79
 1272 0881 90190000 		.long	0x1990
 1273 0885 05       		.uleb128 0x5
 1274 0886 0A       		.byte	0xa
 1275 0887 7C       		.byte	0x7c
 1276 0888 9C190000 		.long	0x199c
 1277 088c 05       		.uleb128 0x5
 1278 088d 0A       		.byte	0xa
 1279 088e 7E       		.byte	0x7e
 1280 088f B2190000 		.long	0x19b2
 1281 0893 05       		.uleb128 0x5
 1282 0894 0A       		.byte	0xa
 1283 0895 83       		.byte	0x83
 1284 0896 C4190000 		.long	0x19c4
 1285 089a 05       		.uleb128 0x5
 1286 089b 0A       		.byte	0xa
 1287 089c 84       		.byte	0x84
 1288 089d D9190000 		.long	0x19d9
 1289 08a1 05       		.uleb128 0x5
 1290 08a2 0A       		.byte	0xa
 1291 08a3 85       		.byte	0x85
 1292 08a4 F3190000 		.long	0x19f3
 1293 08a8 05       		.uleb128 0x5
 1294 08a9 0A       		.byte	0xa
 1295 08aa 87       		.byte	0x87
 1296 08ab 051A0000 		.long	0x1a05
 1297 08af 05       		.uleb128 0x5
 1298 08b0 0A       		.byte	0xa
 1299 08b1 88       		.byte	0x88
 1300 08b2 1C1A0000 		.long	0x1a1c
 1301 08b6 05       		.uleb128 0x5
 1302 08b7 0A       		.byte	0xa
 1303 08b8 8B       		.byte	0x8b
 1304 08b9 411A0000 		.long	0x1a41
 1305 08bd 05       		.uleb128 0x5
 1306 08be 0A       		.byte	0xa
 1307 08bf 8D       		.byte	0x8d
 1308 08c0 4C1A0000 		.long	0x1a4c
 1309 08c4 05       		.uleb128 0x5
 1310 08c5 0A       		.byte	0xa
 1311 08c6 8F       		.byte	0x8f
 1312 08c7 611A0000 		.long	0x1a61
 1313 08cb 08       		.uleb128 0x8
 1314 08cc 00000000 		.long	.LASF103
 1315 08d0 0B       		.byte	0xb
 1316 08d1 A2       		.byte	0xa2
 1317 08d2 D6080000 		.long	0x8d6
GAS LISTING /tmp/cc45yn9z.s 			page 27


 1318 08d6 15       		.uleb128 0x15
 1319 08d7 00000000 		.long	.LASF104
 1320 08db F2080000 		.long	0x8f2
 1321 08df 1F       		.uleb128 0x1f
 1322 08e0 00000000 		.long	.LASF105
 1323 08e4 F60C0000 		.long	0xcf6
 1324 08e8 20       		.uleb128 0x20
 1325 08e9 00000000 		.long	.LASF106
 1326 08ed FB010000 		.long	0x1fb
 1327 08f1 00       		.byte	0
 1328 08f2 08       		.uleb128 0x8
 1329 08f3 00000000 		.long	.LASF107
 1330 08f7 0B       		.byte	0xb
 1331 08f8 8D       		.byte	0x8d
 1332 08f9 EB070000 		.long	0x7eb
 1333 08fd 21       		.uleb128 0x21
 1334 08fe 00000000 		.long	.LASF345
 1335 0902 02       		.byte	0x2
 1336 0903 3D       		.byte	0x3d
 1337 0904 00000000 		.long	.LASF346
 1338 0908 F2080000 		.long	0x8f2
 1339 090c 22       		.uleb128 0x22
 1340 090d 00000000 		.long	.LASF324
 1341 0911 02       		.byte	0x2
 1342 0912 4A       		.byte	0x4a
 1343 0913 5A050000 		.long	0x55a
 1344 0917 00       		.byte	0
 1345 0918 23       		.uleb128 0x23
 1346 0919 00000000 		.long	.LASF108
 1347 091d 04       		.byte	0x4
 1348 091e DD       		.byte	0xdd
 1349 091f A40A0000 		.long	0xaa4
 1350 0923 03       		.uleb128 0x3
 1351 0924 00000000 		.long	.LASF0
 1352 0928 04       		.byte	0x4
 1353 0929 DE       		.byte	0xde
 1354 092a 04       		.uleb128 0x4
 1355 092b 04       		.byte	0x4
 1356 092c DE       		.byte	0xde
 1357 092d 23090000 		.long	0x923
 1358 0931 05       		.uleb128 0x5
 1359 0932 03       		.byte	0x3
 1360 0933 F8       		.byte	0xf8
 1361 0934 7A140000 		.long	0x147a
 1362 0938 06       		.uleb128 0x6
 1363 0939 03       		.byte	0x3
 1364 093a 0101     		.value	0x101
 1365 093c 9C140000 		.long	0x149c
 1366 0940 06       		.uleb128 0x6
 1367 0941 03       		.byte	0x3
 1368 0942 0201     		.value	0x102
 1369 0944 C3140000 		.long	0x14c3
 1370 0948 03       		.uleb128 0x3
 1371 0949 00000000 		.long	.LASF109
 1372 094d 0C       		.byte	0xc
 1373 094e 24       		.byte	0x24
 1374 094f 05       		.uleb128 0x5
GAS LISTING /tmp/cc45yn9z.s 			page 28


 1375 0950 0D       		.byte	0xd
 1376 0951 2C       		.byte	0x2c
 1377 0952 C3030000 		.long	0x3c3
 1378 0956 05       		.uleb128 0x5
 1379 0957 0D       		.byte	0xd
 1380 0958 2D       		.byte	0x2d
 1381 0959 E3030000 		.long	0x3e3
 1382 095d 07       		.uleb128 0x7
 1383 095e 00000000 		.long	.LASF111
 1384 0962 01       		.byte	0x1
 1385 0963 0E       		.byte	0xe
 1386 0964 37       		.byte	0x37
 1387 0965 9F090000 		.long	0x99f
 1388 0969 24       		.uleb128 0x24
 1389 096a 00000000 		.long	.LASF112
 1390 096e 0E       		.byte	0xe
 1391 096f 3A       		.byte	0x3a
 1392 0970 210D0000 		.long	0xd21
 1393 0974 24       		.uleb128 0x24
 1394 0975 00000000 		.long	.LASF113
 1395 0979 0E       		.byte	0xe
 1396 097a 3B       		.byte	0x3b
 1397 097b 210D0000 		.long	0xd21
 1398 097f 24       		.uleb128 0x24
 1399 0980 00000000 		.long	.LASF114
 1400 0984 0E       		.byte	0xe
 1401 0985 3F       		.byte	0x3f
 1402 0986 BB160000 		.long	0x16bb
 1403 098a 24       		.uleb128 0x24
 1404 098b 00000000 		.long	.LASF115
 1405 098f 0E       		.byte	0xe
 1406 0990 40       		.byte	0x40
 1407 0991 210D0000 		.long	0xd21
 1408 0995 1F       		.uleb128 0x1f
 1409 0996 00000000 		.long	.LASF116
 1410 099a FD0C0000 		.long	0xcfd
 1411 099e 00       		.byte	0
 1412 099f 07       		.uleb128 0x7
 1413 09a0 00000000 		.long	.LASF117
 1414 09a4 01       		.byte	0x1
 1415 09a5 0E       		.byte	0xe
 1416 09a6 37       		.byte	0x37
 1417 09a7 E1090000 		.long	0x9e1
 1418 09ab 24       		.uleb128 0x24
 1419 09ac 00000000 		.long	.LASF112
 1420 09b0 0E       		.byte	0xe
 1421 09b1 3A       		.byte	0x3a
 1422 09b2 C0160000 		.long	0x16c0
 1423 09b6 24       		.uleb128 0x24
 1424 09b7 00000000 		.long	.LASF113
 1425 09bb 0E       		.byte	0xe
 1426 09bc 3B       		.byte	0x3b
 1427 09bd C0160000 		.long	0x16c0
 1428 09c1 24       		.uleb128 0x24
 1429 09c2 00000000 		.long	.LASF114
 1430 09c6 0E       		.byte	0xe
 1431 09c7 3F       		.byte	0x3f
GAS LISTING /tmp/cc45yn9z.s 			page 29


 1432 09c8 BB160000 		.long	0x16bb
 1433 09cc 24       		.uleb128 0x24
 1434 09cd 00000000 		.long	.LASF115
 1435 09d1 0E       		.byte	0xe
 1436 09d2 40       		.byte	0x40
 1437 09d3 210D0000 		.long	0xd21
 1438 09d7 1F       		.uleb128 0x1f
 1439 09d8 00000000 		.long	.LASF116
 1440 09dc 8F0C0000 		.long	0xc8f
 1441 09e0 00       		.byte	0
 1442 09e1 07       		.uleb128 0x7
 1443 09e2 00000000 		.long	.LASF118
 1444 09e6 01       		.byte	0x1
 1445 09e7 0E       		.byte	0xe
 1446 09e8 37       		.byte	0x37
 1447 09e9 230A0000 		.long	0xa23
 1448 09ed 24       		.uleb128 0x24
 1449 09ee 00000000 		.long	.LASF112
 1450 09f2 0E       		.byte	0xe
 1451 09f3 3A       		.byte	0x3a
 1452 09f4 2C0D0000 		.long	0xd2c
 1453 09f8 24       		.uleb128 0x24
 1454 09f9 00000000 		.long	.LASF113
 1455 09fd 0E       		.byte	0xe
 1456 09fe 3B       		.byte	0x3b
 1457 09ff 2C0D0000 		.long	0xd2c
 1458 0a03 24       		.uleb128 0x24
 1459 0a04 00000000 		.long	.LASF114
 1460 0a08 0E       		.byte	0xe
 1461 0a09 3F       		.byte	0x3f
 1462 0a0a BB160000 		.long	0x16bb
 1463 0a0e 24       		.uleb128 0x24
 1464 0a0f 00000000 		.long	.LASF115
 1465 0a13 0E       		.byte	0xe
 1466 0a14 40       		.byte	0x40
 1467 0a15 210D0000 		.long	0xd21
 1468 0a19 1F       		.uleb128 0x1f
 1469 0a1a 00000000 		.long	.LASF116
 1470 0a1e F60C0000 		.long	0xcf6
 1471 0a22 00       		.byte	0
 1472 0a23 07       		.uleb128 0x7
 1473 0a24 00000000 		.long	.LASF119
 1474 0a28 01       		.byte	0x1
 1475 0a29 0E       		.byte	0xe
 1476 0a2a 37       		.byte	0x37
 1477 0a2b 650A0000 		.long	0xa65
 1478 0a2f 24       		.uleb128 0x24
 1479 0a30 00000000 		.long	.LASF112
 1480 0a34 0E       		.byte	0xe
 1481 0a35 3A       		.byte	0x3a
 1482 0a36 4A170000 		.long	0x174a
 1483 0a3a 24       		.uleb128 0x24
 1484 0a3b 00000000 		.long	.LASF113
 1485 0a3f 0E       		.byte	0xe
 1486 0a40 3B       		.byte	0x3b
 1487 0a41 4A170000 		.long	0x174a
 1488 0a45 24       		.uleb128 0x24
GAS LISTING /tmp/cc45yn9z.s 			page 30


 1489 0a46 00000000 		.long	.LASF114
 1490 0a4a 0E       		.byte	0xe
 1491 0a4b 3F       		.byte	0x3f
 1492 0a4c BB160000 		.long	0x16bb
 1493 0a50 24       		.uleb128 0x24
 1494 0a51 00000000 		.long	.LASF115
 1495 0a55 0E       		.byte	0xe
 1496 0a56 40       		.byte	0x40
 1497 0a57 210D0000 		.long	0xd21
 1498 0a5b 1F       		.uleb128 0x1f
 1499 0a5c 00000000 		.long	.LASF116
 1500 0a60 F8140000 		.long	0x14f8
 1501 0a64 00       		.byte	0
 1502 0a65 25       		.uleb128 0x25
 1503 0a66 00000000 		.long	.LASF347
 1504 0a6a 01       		.byte	0x1
 1505 0a6b 0E       		.byte	0xe
 1506 0a6c 37       		.byte	0x37
 1507 0a6d 24       		.uleb128 0x24
 1508 0a6e 00000000 		.long	.LASF112
 1509 0a72 0E       		.byte	0xe
 1510 0a73 3A       		.byte	0x3a
 1511 0a74 4F170000 		.long	0x174f
 1512 0a78 24       		.uleb128 0x24
 1513 0a79 00000000 		.long	.LASF113
 1514 0a7d 0E       		.byte	0xe
 1515 0a7e 3B       		.byte	0x3b
 1516 0a7f 4F170000 		.long	0x174f
 1517 0a83 24       		.uleb128 0x24
 1518 0a84 00000000 		.long	.LASF114
 1519 0a88 0E       		.byte	0xe
 1520 0a89 3F       		.byte	0x3f
 1521 0a8a BB160000 		.long	0x16bb
 1522 0a8e 24       		.uleb128 0x24
 1523 0a8f 00000000 		.long	.LASF115
 1524 0a93 0E       		.byte	0xe
 1525 0a94 40       		.byte	0x40
 1526 0a95 210D0000 		.long	0xd21
 1527 0a99 1F       		.uleb128 0x1f
 1528 0a9a 00000000 		.long	.LASF116
 1529 0a9e D2120000 		.long	0x12d2
 1530 0aa2 00       		.byte	0
 1531 0aa3 00       		.byte	0
 1532 0aa4 08       		.uleb128 0x8
 1533 0aa5 00000000 		.long	.LASF120
 1534 0aa9 0F       		.byte	0xf
 1535 0aaa 30       		.byte	0x30
 1536 0aab AF0A0000 		.long	0xaaf
 1537 0aaf 07       		.uleb128 0x7
 1538 0ab0 00000000 		.long	.LASF121
 1539 0ab4 D8       		.byte	0xd8
 1540 0ab5 10       		.byte	0x10
 1541 0ab6 F1       		.byte	0xf1
 1542 0ab7 2C0C0000 		.long	0xc2c
 1543 0abb 26       		.uleb128 0x26
 1544 0abc 00000000 		.long	.LASF122
 1545 0ac0 10       		.byte	0x10
GAS LISTING /tmp/cc45yn9z.s 			page 31


 1546 0ac1 F2       		.byte	0xf2
 1547 0ac2 FD0C0000 		.long	0xcfd
 1548 0ac6 00       		.byte	0
 1549 0ac7 26       		.uleb128 0x26
 1550 0ac8 00000000 		.long	.LASF123
 1551 0acc 10       		.byte	0x10
 1552 0acd F7       		.byte	0xf7
 1553 0ace 47100000 		.long	0x1047
 1554 0ad2 08       		.byte	0x8
 1555 0ad3 26       		.uleb128 0x26
 1556 0ad4 00000000 		.long	.LASF124
 1557 0ad8 10       		.byte	0x10
 1558 0ad9 F8       		.byte	0xf8
 1559 0ada 47100000 		.long	0x1047
 1560 0ade 10       		.byte	0x10
 1561 0adf 26       		.uleb128 0x26
 1562 0ae0 00000000 		.long	.LASF125
 1563 0ae4 10       		.byte	0x10
 1564 0ae5 F9       		.byte	0xf9
 1565 0ae6 47100000 		.long	0x1047
 1566 0aea 18       		.byte	0x18
 1567 0aeb 26       		.uleb128 0x26
 1568 0aec 00000000 		.long	.LASF126
 1569 0af0 10       		.byte	0x10
 1570 0af1 FA       		.byte	0xfa
 1571 0af2 47100000 		.long	0x1047
 1572 0af6 20       		.byte	0x20
 1573 0af7 26       		.uleb128 0x26
 1574 0af8 00000000 		.long	.LASF127
 1575 0afc 10       		.byte	0x10
 1576 0afd FB       		.byte	0xfb
 1577 0afe 47100000 		.long	0x1047
 1578 0b02 28       		.byte	0x28
 1579 0b03 26       		.uleb128 0x26
 1580 0b04 00000000 		.long	.LASF128
 1581 0b08 10       		.byte	0x10
 1582 0b09 FC       		.byte	0xfc
 1583 0b0a 47100000 		.long	0x1047
 1584 0b0e 30       		.byte	0x30
 1585 0b0f 26       		.uleb128 0x26
 1586 0b10 00000000 		.long	.LASF129
 1587 0b14 10       		.byte	0x10
 1588 0b15 FD       		.byte	0xfd
 1589 0b16 47100000 		.long	0x1047
 1590 0b1a 38       		.byte	0x38
 1591 0b1b 26       		.uleb128 0x26
 1592 0b1c 00000000 		.long	.LASF130
 1593 0b20 10       		.byte	0x10
 1594 0b21 FE       		.byte	0xfe
 1595 0b22 47100000 		.long	0x1047
 1596 0b26 40       		.byte	0x40
 1597 0b27 27       		.uleb128 0x27
 1598 0b28 00000000 		.long	.LASF131
 1599 0b2c 10       		.byte	0x10
 1600 0b2d 0001     		.value	0x100
 1601 0b2f 47100000 		.long	0x1047
 1602 0b33 48       		.byte	0x48
GAS LISTING /tmp/cc45yn9z.s 			page 32


 1603 0b34 27       		.uleb128 0x27
 1604 0b35 00000000 		.long	.LASF132
 1605 0b39 10       		.byte	0x10
 1606 0b3a 0101     		.value	0x101
 1607 0b3c 47100000 		.long	0x1047
 1608 0b40 50       		.byte	0x50
 1609 0b41 27       		.uleb128 0x27
 1610 0b42 00000000 		.long	.LASF133
 1611 0b46 10       		.byte	0x10
 1612 0b47 0201     		.value	0x102
 1613 0b49 47100000 		.long	0x1047
 1614 0b4d 58       		.byte	0x58
 1615 0b4e 27       		.uleb128 0x27
 1616 0b4f 00000000 		.long	.LASF134
 1617 0b53 10       		.byte	0x10
 1618 0b54 0401     		.value	0x104
 1619 0b56 BC170000 		.long	0x17bc
 1620 0b5a 60       		.byte	0x60
 1621 0b5b 27       		.uleb128 0x27
 1622 0b5c 00000000 		.long	.LASF135
 1623 0b60 10       		.byte	0x10
 1624 0b61 0601     		.value	0x106
 1625 0b63 C2170000 		.long	0x17c2
 1626 0b67 68       		.byte	0x68
 1627 0b68 27       		.uleb128 0x27
 1628 0b69 00000000 		.long	.LASF136
 1629 0b6d 10       		.byte	0x10
 1630 0b6e 0801     		.value	0x108
 1631 0b70 FD0C0000 		.long	0xcfd
 1632 0b74 70       		.byte	0x70
 1633 0b75 27       		.uleb128 0x27
 1634 0b76 00000000 		.long	.LASF137
 1635 0b7a 10       		.byte	0x10
 1636 0b7b 0C01     		.value	0x10c
 1637 0b7d FD0C0000 		.long	0xcfd
 1638 0b81 74       		.byte	0x74
 1639 0b82 27       		.uleb128 0x27
 1640 0b83 00000000 		.long	.LASF138
 1641 0b87 10       		.byte	0x10
 1642 0b88 0E01     		.value	0x10e
 1643 0b8a 9A160000 		.long	0x169a
 1644 0b8e 78       		.byte	0x78
 1645 0b8f 27       		.uleb128 0x27
 1646 0b90 00000000 		.long	.LASF139
 1647 0b94 10       		.byte	0x10
 1648 0b95 1201     		.value	0x112
 1649 0b97 1A0D0000 		.long	0xd1a
 1650 0b9b 80       		.byte	0x80
 1651 0b9c 27       		.uleb128 0x27
 1652 0b9d 00000000 		.long	.LASF140
 1653 0ba1 10       		.byte	0x10
 1654 0ba2 1301     		.value	0x113
 1655 0ba4 F1140000 		.long	0x14f1
 1656 0ba8 82       		.byte	0x82
 1657 0ba9 27       		.uleb128 0x27
 1658 0baa 00000000 		.long	.LASF141
 1659 0bae 10       		.byte	0x10
GAS LISTING /tmp/cc45yn9z.s 			page 33


 1660 0baf 1401     		.value	0x114
 1661 0bb1 C8170000 		.long	0x17c8
 1662 0bb5 83       		.byte	0x83
 1663 0bb6 27       		.uleb128 0x27
 1664 0bb7 00000000 		.long	.LASF142
 1665 0bbb 10       		.byte	0x10
 1666 0bbc 1801     		.value	0x118
 1667 0bbe D8170000 		.long	0x17d8
 1668 0bc2 88       		.byte	0x88
 1669 0bc3 27       		.uleb128 0x27
 1670 0bc4 00000000 		.long	.LASF143
 1671 0bc8 10       		.byte	0x10
 1672 0bc9 2101     		.value	0x121
 1673 0bcb A5160000 		.long	0x16a5
 1674 0bcf 90       		.byte	0x90
 1675 0bd0 27       		.uleb128 0x27
 1676 0bd1 00000000 		.long	.LASF144
 1677 0bd5 10       		.byte	0x10
 1678 0bd6 2901     		.value	0x129
 1679 0bd8 820C0000 		.long	0xc82
 1680 0bdc 98       		.byte	0x98
 1681 0bdd 27       		.uleb128 0x27
 1682 0bde 00000000 		.long	.LASF145
 1683 0be2 10       		.byte	0x10
 1684 0be3 2A01     		.value	0x12a
 1685 0be5 820C0000 		.long	0xc82
 1686 0be9 A0       		.byte	0xa0
 1687 0bea 27       		.uleb128 0x27
 1688 0beb 00000000 		.long	.LASF146
 1689 0bef 10       		.byte	0x10
 1690 0bf0 2B01     		.value	0x12b
 1691 0bf2 820C0000 		.long	0xc82
 1692 0bf6 A8       		.byte	0xa8
 1693 0bf7 27       		.uleb128 0x27
 1694 0bf8 00000000 		.long	.LASF147
 1695 0bfc 10       		.byte	0x10
 1696 0bfd 2C01     		.value	0x12c
 1697 0bff 820C0000 		.long	0xc82
 1698 0c03 B0       		.byte	0xb0
 1699 0c04 27       		.uleb128 0x27
 1700 0c05 00000000 		.long	.LASF148
 1701 0c09 10       		.byte	0x10
 1702 0c0a 2E01     		.value	0x12e
 1703 0c0c 840C0000 		.long	0xc84
 1704 0c10 B8       		.byte	0xb8
 1705 0c11 27       		.uleb128 0x27
 1706 0c12 00000000 		.long	.LASF149
 1707 0c16 10       		.byte	0x10
 1708 0c17 2F01     		.value	0x12f
 1709 0c19 FD0C0000 		.long	0xcfd
 1710 0c1d C0       		.byte	0xc0
 1711 0c1e 27       		.uleb128 0x27
 1712 0c1f 00000000 		.long	.LASF150
 1713 0c23 10       		.byte	0x10
 1714 0c24 3101     		.value	0x131
 1715 0c26 DE170000 		.long	0x17de
 1716 0c2a C4       		.byte	0xc4
GAS LISTING /tmp/cc45yn9z.s 			page 34


 1717 0c2b 00       		.byte	0
 1718 0c2c 08       		.uleb128 0x8
 1719 0c2d 00000000 		.long	.LASF151
 1720 0c31 0F       		.byte	0xf
 1721 0c32 40       		.byte	0x40
 1722 0c33 AF0A0000 		.long	0xaaf
 1723 0c37 28       		.uleb128 0x28
 1724 0c38 08       		.byte	0x8
 1725 0c39 07       		.byte	0x7
 1726 0c3a 00000000 		.long	.LASF157
 1727 0c3e 07       		.uleb128 0x7
 1728 0c3f 00000000 		.long	.LASF152
 1729 0c43 18       		.byte	0x18
 1730 0c44 11       		.byte	0x11
 1731 0c45 00       		.byte	0
 1732 0c46 7B0C0000 		.long	0xc7b
 1733 0c4a 26       		.uleb128 0x26
 1734 0c4b 00000000 		.long	.LASF153
 1735 0c4f 11       		.byte	0x11
 1736 0c50 00       		.byte	0
 1737 0c51 7B0C0000 		.long	0xc7b
 1738 0c55 00       		.byte	0
 1739 0c56 26       		.uleb128 0x26
 1740 0c57 00000000 		.long	.LASF154
 1741 0c5b 11       		.byte	0x11
 1742 0c5c 00       		.byte	0
 1743 0c5d 7B0C0000 		.long	0xc7b
 1744 0c61 04       		.byte	0x4
 1745 0c62 26       		.uleb128 0x26
 1746 0c63 00000000 		.long	.LASF155
 1747 0c67 11       		.byte	0x11
 1748 0c68 00       		.byte	0
 1749 0c69 820C0000 		.long	0xc82
 1750 0c6d 08       		.byte	0x8
 1751 0c6e 26       		.uleb128 0x26
 1752 0c6f 00000000 		.long	.LASF156
 1753 0c73 11       		.byte	0x11
 1754 0c74 00       		.byte	0
 1755 0c75 820C0000 		.long	0xc82
 1756 0c79 10       		.byte	0x10
 1757 0c7a 00       		.byte	0
 1758 0c7b 28       		.uleb128 0x28
 1759 0c7c 04       		.byte	0x4
 1760 0c7d 07       		.byte	0x7
 1761 0c7e 00000000 		.long	.LASF158
 1762 0c82 29       		.uleb128 0x29
 1763 0c83 08       		.byte	0x8
 1764 0c84 08       		.uleb128 0x8
 1765 0c85 00000000 		.long	.LASF25
 1766 0c89 12       		.byte	0x12
 1767 0c8a D8       		.byte	0xd8
 1768 0c8b 8F0C0000 		.long	0xc8f
 1769 0c8f 28       		.uleb128 0x28
 1770 0c90 08       		.byte	0x8
 1771 0c91 07       		.byte	0x7
 1772 0c92 00000000 		.long	.LASF159
 1773 0c96 2A       		.uleb128 0x2a
GAS LISTING /tmp/cc45yn9z.s 			page 35


 1774 0c97 00000000 		.long	.LASF160
 1775 0c9b 12       		.byte	0x12
 1776 0c9c 6501     		.value	0x165
 1777 0c9e 7B0C0000 		.long	0xc7b
 1778 0ca2 2B       		.uleb128 0x2b
 1779 0ca3 08       		.byte	0x8
 1780 0ca4 13       		.byte	0x13
 1781 0ca5 53       		.byte	0x53
 1782 0ca6 00000000 		.long	.LASF288
 1783 0caa E60C0000 		.long	0xce6
 1784 0cae 2C       		.uleb128 0x2c
 1785 0caf 04       		.byte	0x4
 1786 0cb0 13       		.byte	0x13
 1787 0cb1 56       		.byte	0x56
 1788 0cb2 CD0C0000 		.long	0xccd
 1789 0cb6 2D       		.uleb128 0x2d
 1790 0cb7 00000000 		.long	.LASF161
 1791 0cbb 13       		.byte	0x13
 1792 0cbc 58       		.byte	0x58
 1793 0cbd 7B0C0000 		.long	0xc7b
 1794 0cc1 2D       		.uleb128 0x2d
 1795 0cc2 00000000 		.long	.LASF162
 1796 0cc6 13       		.byte	0x13
 1797 0cc7 5C       		.byte	0x5c
 1798 0cc8 E60C0000 		.long	0xce6
 1799 0ccc 00       		.byte	0
 1800 0ccd 26       		.uleb128 0x26
 1801 0cce 00000000 		.long	.LASF163
 1802 0cd2 13       		.byte	0x13
 1803 0cd3 54       		.byte	0x54
 1804 0cd4 FD0C0000 		.long	0xcfd
 1805 0cd8 00       		.byte	0
 1806 0cd9 26       		.uleb128 0x26
 1807 0cda 00000000 		.long	.LASF164
 1808 0cde 13       		.byte	0x13
 1809 0cdf 5D       		.byte	0x5d
 1810 0ce0 AE0C0000 		.long	0xcae
 1811 0ce4 04       		.byte	0x4
 1812 0ce5 00       		.byte	0
 1813 0ce6 2E       		.uleb128 0x2e
 1814 0ce7 F60C0000 		.long	0xcf6
 1815 0ceb F60C0000 		.long	0xcf6
 1816 0cef 2F       		.uleb128 0x2f
 1817 0cf0 370C0000 		.long	0xc37
 1818 0cf4 03       		.byte	0x3
 1819 0cf5 00       		.byte	0
 1820 0cf6 28       		.uleb128 0x28
 1821 0cf7 01       		.byte	0x1
 1822 0cf8 06       		.byte	0x6
 1823 0cf9 00000000 		.long	.LASF165
 1824 0cfd 30       		.uleb128 0x30
 1825 0cfe 04       		.byte	0x4
 1826 0cff 05       		.byte	0x5
 1827 0d00 696E7400 		.string	"int"
 1828 0d04 08       		.uleb128 0x8
 1829 0d05 00000000 		.long	.LASF166
 1830 0d09 13       		.byte	0x13
GAS LISTING /tmp/cc45yn9z.s 			page 36


 1831 0d0a 5E       		.byte	0x5e
 1832 0d0b A20C0000 		.long	0xca2
 1833 0d0f 08       		.uleb128 0x8
 1834 0d10 00000000 		.long	.LASF167
 1835 0d14 13       		.byte	0x13
 1836 0d15 6A       		.byte	0x6a
 1837 0d16 040D0000 		.long	0xd04
 1838 0d1a 28       		.uleb128 0x28
 1839 0d1b 02       		.byte	0x2
 1840 0d1c 07       		.byte	0x7
 1841 0d1d 00000000 		.long	.LASF168
 1842 0d21 0B       		.uleb128 0xb
 1843 0d22 FD0C0000 		.long	0xcfd
 1844 0d26 31       		.uleb128 0x31
 1845 0d27 08       		.byte	0x8
 1846 0d28 2C0D0000 		.long	0xd2c
 1847 0d2c 0B       		.uleb128 0xb
 1848 0d2d F60C0000 		.long	0xcf6
 1849 0d31 32       		.uleb128 0x32
 1850 0d32 00000000 		.long	.LASF169
 1851 0d36 13       		.byte	0x13
 1852 0d37 6401     		.value	0x164
 1853 0d39 960C0000 		.long	0xc96
 1854 0d3d 470D0000 		.long	0xd47
 1855 0d41 0A       		.uleb128 0xa
 1856 0d42 FD0C0000 		.long	0xcfd
 1857 0d46 00       		.byte	0
 1858 0d47 32       		.uleb128 0x32
 1859 0d48 00000000 		.long	.LASF170
 1860 0d4c 13       		.byte	0x13
 1861 0d4d EC02     		.value	0x2ec
 1862 0d4f 960C0000 		.long	0xc96
 1863 0d53 5D0D0000 		.long	0xd5d
 1864 0d57 0A       		.uleb128 0xa
 1865 0d58 5D0D0000 		.long	0xd5d
 1866 0d5c 00       		.byte	0
 1867 0d5d 31       		.uleb128 0x31
 1868 0d5e 08       		.byte	0x8
 1869 0d5f 2C0C0000 		.long	0xc2c
 1870 0d63 32       		.uleb128 0x32
 1871 0d64 00000000 		.long	.LASF171
 1872 0d68 13       		.byte	0x13
 1873 0d69 0903     		.value	0x309
 1874 0d6b 830D0000 		.long	0xd83
 1875 0d6f 830D0000 		.long	0xd83
 1876 0d73 0A       		.uleb128 0xa
 1877 0d74 830D0000 		.long	0xd83
 1878 0d78 0A       		.uleb128 0xa
 1879 0d79 FD0C0000 		.long	0xcfd
 1880 0d7d 0A       		.uleb128 0xa
 1881 0d7e 5D0D0000 		.long	0xd5d
 1882 0d82 00       		.byte	0
 1883 0d83 31       		.uleb128 0x31
 1884 0d84 08       		.byte	0x8
 1885 0d85 890D0000 		.long	0xd89
 1886 0d89 28       		.uleb128 0x28
 1887 0d8a 04       		.byte	0x4
GAS LISTING /tmp/cc45yn9z.s 			page 37


 1888 0d8b 05       		.byte	0x5
 1889 0d8c 00000000 		.long	.LASF172
 1890 0d90 32       		.uleb128 0x32
 1891 0d91 00000000 		.long	.LASF173
 1892 0d95 13       		.byte	0x13
 1893 0d96 FA02     		.value	0x2fa
 1894 0d98 960C0000 		.long	0xc96
 1895 0d9c AB0D0000 		.long	0xdab
 1896 0da0 0A       		.uleb128 0xa
 1897 0da1 890D0000 		.long	0xd89
 1898 0da5 0A       		.uleb128 0xa
 1899 0da6 5D0D0000 		.long	0xd5d
 1900 0daa 00       		.byte	0
 1901 0dab 32       		.uleb128 0x32
 1902 0dac 00000000 		.long	.LASF174
 1903 0db0 13       		.byte	0x13
 1904 0db1 1003     		.value	0x310
 1905 0db3 FD0C0000 		.long	0xcfd
 1906 0db7 C60D0000 		.long	0xdc6
 1907 0dbb 0A       		.uleb128 0xa
 1908 0dbc C60D0000 		.long	0xdc6
 1909 0dc0 0A       		.uleb128 0xa
 1910 0dc1 5D0D0000 		.long	0xd5d
 1911 0dc5 00       		.byte	0
 1912 0dc6 31       		.uleb128 0x31
 1913 0dc7 08       		.byte	0x8
 1914 0dc8 CC0D0000 		.long	0xdcc
 1915 0dcc 0B       		.uleb128 0xb
 1916 0dcd 890D0000 		.long	0xd89
 1917 0dd1 32       		.uleb128 0x32
 1918 0dd2 00000000 		.long	.LASF175
 1919 0dd6 13       		.byte	0x13
 1920 0dd7 4E02     		.value	0x24e
 1921 0dd9 FD0C0000 		.long	0xcfd
 1922 0ddd EC0D0000 		.long	0xdec
 1923 0de1 0A       		.uleb128 0xa
 1924 0de2 5D0D0000 		.long	0xd5d
 1925 0de6 0A       		.uleb128 0xa
 1926 0de7 FD0C0000 		.long	0xcfd
 1927 0deb 00       		.byte	0
 1928 0dec 32       		.uleb128 0x32
 1929 0ded 00000000 		.long	.LASF176
 1930 0df1 13       		.byte	0x13
 1931 0df2 5502     		.value	0x255
 1932 0df4 FD0C0000 		.long	0xcfd
 1933 0df8 080E0000 		.long	0xe08
 1934 0dfc 0A       		.uleb128 0xa
 1935 0dfd 5D0D0000 		.long	0xd5d
 1936 0e01 0A       		.uleb128 0xa
 1937 0e02 C60D0000 		.long	0xdc6
 1938 0e06 33       		.uleb128 0x33
 1939 0e07 00       		.byte	0
 1940 0e08 32       		.uleb128 0x32
 1941 0e09 00000000 		.long	.LASF177
 1942 0e0d 13       		.byte	0x13
 1943 0e0e 7E02     		.value	0x27e
 1944 0e10 FD0C0000 		.long	0xcfd
GAS LISTING /tmp/cc45yn9z.s 			page 38


 1945 0e14 240E0000 		.long	0xe24
 1946 0e18 0A       		.uleb128 0xa
 1947 0e19 5D0D0000 		.long	0xd5d
 1948 0e1d 0A       		.uleb128 0xa
 1949 0e1e C60D0000 		.long	0xdc6
 1950 0e22 33       		.uleb128 0x33
 1951 0e23 00       		.byte	0
 1952 0e24 32       		.uleb128 0x32
 1953 0e25 00000000 		.long	.LASF178
 1954 0e29 13       		.byte	0x13
 1955 0e2a ED02     		.value	0x2ed
 1956 0e2c 960C0000 		.long	0xc96
 1957 0e30 3A0E0000 		.long	0xe3a
 1958 0e34 0A       		.uleb128 0xa
 1959 0e35 5D0D0000 		.long	0xd5d
 1960 0e39 00       		.byte	0
 1961 0e3a 34       		.uleb128 0x34
 1962 0e3b 00000000 		.long	.LASF276
 1963 0e3f 13       		.byte	0x13
 1964 0e40 F302     		.value	0x2f3
 1965 0e42 960C0000 		.long	0xc96
 1966 0e46 32       		.uleb128 0x32
 1967 0e47 00000000 		.long	.LASF179
 1968 0e4b 13       		.byte	0x13
 1969 0e4c 7B01     		.value	0x17b
 1970 0e4e 840C0000 		.long	0xc84
 1971 0e52 660E0000 		.long	0xe66
 1972 0e56 0A       		.uleb128 0xa
 1973 0e57 260D0000 		.long	0xd26
 1974 0e5b 0A       		.uleb128 0xa
 1975 0e5c 840C0000 		.long	0xc84
 1976 0e60 0A       		.uleb128 0xa
 1977 0e61 660E0000 		.long	0xe66
 1978 0e65 00       		.byte	0
 1979 0e66 31       		.uleb128 0x31
 1980 0e67 08       		.byte	0x8
 1981 0e68 0F0D0000 		.long	0xd0f
 1982 0e6c 32       		.uleb128 0x32
 1983 0e6d 00000000 		.long	.LASF180
 1984 0e71 13       		.byte	0x13
 1985 0e72 7001     		.value	0x170
 1986 0e74 840C0000 		.long	0xc84
 1987 0e78 910E0000 		.long	0xe91
 1988 0e7c 0A       		.uleb128 0xa
 1989 0e7d 830D0000 		.long	0xd83
 1990 0e81 0A       		.uleb128 0xa
 1991 0e82 260D0000 		.long	0xd26
 1992 0e86 0A       		.uleb128 0xa
 1993 0e87 840C0000 		.long	0xc84
 1994 0e8b 0A       		.uleb128 0xa
 1995 0e8c 660E0000 		.long	0xe66
 1996 0e90 00       		.byte	0
 1997 0e91 32       		.uleb128 0x32
 1998 0e92 00000000 		.long	.LASF181
 1999 0e96 13       		.byte	0x13
 2000 0e97 6C01     		.value	0x16c
 2001 0e99 FD0C0000 		.long	0xcfd
GAS LISTING /tmp/cc45yn9z.s 			page 39


 2002 0e9d A70E0000 		.long	0xea7
 2003 0ea1 0A       		.uleb128 0xa
 2004 0ea2 A70E0000 		.long	0xea7
 2005 0ea6 00       		.byte	0
 2006 0ea7 31       		.uleb128 0x31
 2007 0ea8 08       		.byte	0x8
 2008 0ea9 AD0E0000 		.long	0xead
 2009 0ead 0B       		.uleb128 0xb
 2010 0eae 0F0D0000 		.long	0xd0f
 2011 0eb2 32       		.uleb128 0x32
 2012 0eb3 00000000 		.long	.LASF182
 2013 0eb7 13       		.byte	0x13
 2014 0eb8 9B01     		.value	0x19b
 2015 0eba 840C0000 		.long	0xc84
 2016 0ebe D70E0000 		.long	0xed7
 2017 0ec2 0A       		.uleb128 0xa
 2018 0ec3 830D0000 		.long	0xd83
 2019 0ec7 0A       		.uleb128 0xa
 2020 0ec8 D70E0000 		.long	0xed7
 2021 0ecc 0A       		.uleb128 0xa
 2022 0ecd 840C0000 		.long	0xc84
 2023 0ed1 0A       		.uleb128 0xa
 2024 0ed2 660E0000 		.long	0xe66
 2025 0ed6 00       		.byte	0
 2026 0ed7 31       		.uleb128 0x31
 2027 0ed8 08       		.byte	0x8
 2028 0ed9 260D0000 		.long	0xd26
 2029 0edd 32       		.uleb128 0x32
 2030 0ede 00000000 		.long	.LASF183
 2031 0ee2 13       		.byte	0x13
 2032 0ee3 FB02     		.value	0x2fb
 2033 0ee5 960C0000 		.long	0xc96
 2034 0ee9 F80E0000 		.long	0xef8
 2035 0eed 0A       		.uleb128 0xa
 2036 0eee 890D0000 		.long	0xd89
 2037 0ef2 0A       		.uleb128 0xa
 2038 0ef3 5D0D0000 		.long	0xd5d
 2039 0ef7 00       		.byte	0
 2040 0ef8 32       		.uleb128 0x32
 2041 0ef9 00000000 		.long	.LASF184
 2042 0efd 13       		.byte	0x13
 2043 0efe 0103     		.value	0x301
 2044 0f00 960C0000 		.long	0xc96
 2045 0f04 0E0F0000 		.long	0xf0e
 2046 0f08 0A       		.uleb128 0xa
 2047 0f09 890D0000 		.long	0xd89
 2048 0f0d 00       		.byte	0
 2049 0f0e 32       		.uleb128 0x32
 2050 0f0f 00000000 		.long	.LASF185
 2051 0f13 13       		.byte	0x13
 2052 0f14 5F02     		.value	0x25f
 2053 0f16 FD0C0000 		.long	0xcfd
 2054 0f1a 2F0F0000 		.long	0xf2f
 2055 0f1e 0A       		.uleb128 0xa
 2056 0f1f 830D0000 		.long	0xd83
 2057 0f23 0A       		.uleb128 0xa
 2058 0f24 840C0000 		.long	0xc84
GAS LISTING /tmp/cc45yn9z.s 			page 40


 2059 0f28 0A       		.uleb128 0xa
 2060 0f29 C60D0000 		.long	0xdc6
 2061 0f2d 33       		.uleb128 0x33
 2062 0f2e 00       		.byte	0
 2063 0f2f 32       		.uleb128 0x32
 2064 0f30 00000000 		.long	.LASF186
 2065 0f34 13       		.byte	0x13
 2066 0f35 8802     		.value	0x288
 2067 0f37 FD0C0000 		.long	0xcfd
 2068 0f3b 4B0F0000 		.long	0xf4b
 2069 0f3f 0A       		.uleb128 0xa
 2070 0f40 C60D0000 		.long	0xdc6
 2071 0f44 0A       		.uleb128 0xa
 2072 0f45 C60D0000 		.long	0xdc6
 2073 0f49 33       		.uleb128 0x33
 2074 0f4a 00       		.byte	0
 2075 0f4b 32       		.uleb128 0x32
 2076 0f4c 00000000 		.long	.LASF187
 2077 0f50 13       		.byte	0x13
 2078 0f51 1803     		.value	0x318
 2079 0f53 960C0000 		.long	0xc96
 2080 0f57 660F0000 		.long	0xf66
 2081 0f5b 0A       		.uleb128 0xa
 2082 0f5c 960C0000 		.long	0xc96
 2083 0f60 0A       		.uleb128 0xa
 2084 0f61 5D0D0000 		.long	0xd5d
 2085 0f65 00       		.byte	0
 2086 0f66 32       		.uleb128 0x32
 2087 0f67 00000000 		.long	.LASF188
 2088 0f6b 13       		.byte	0x13
 2089 0f6c 6702     		.value	0x267
 2090 0f6e FD0C0000 		.long	0xcfd
 2091 0f72 860F0000 		.long	0xf86
 2092 0f76 0A       		.uleb128 0xa
 2093 0f77 5D0D0000 		.long	0xd5d
 2094 0f7b 0A       		.uleb128 0xa
 2095 0f7c C60D0000 		.long	0xdc6
 2096 0f80 0A       		.uleb128 0xa
 2097 0f81 860F0000 		.long	0xf86
 2098 0f85 00       		.byte	0
 2099 0f86 31       		.uleb128 0x31
 2100 0f87 08       		.byte	0x8
 2101 0f88 3E0C0000 		.long	0xc3e
 2102 0f8c 32       		.uleb128 0x32
 2103 0f8d 00000000 		.long	.LASF189
 2104 0f91 13       		.byte	0x13
 2105 0f92 B402     		.value	0x2b4
 2106 0f94 FD0C0000 		.long	0xcfd
 2107 0f98 AC0F0000 		.long	0xfac
 2108 0f9c 0A       		.uleb128 0xa
 2109 0f9d 5D0D0000 		.long	0xd5d
 2110 0fa1 0A       		.uleb128 0xa
 2111 0fa2 C60D0000 		.long	0xdc6
 2112 0fa6 0A       		.uleb128 0xa
 2113 0fa7 860F0000 		.long	0xf86
 2114 0fab 00       		.byte	0
 2115 0fac 32       		.uleb128 0x32
GAS LISTING /tmp/cc45yn9z.s 			page 41


 2116 0fad 00000000 		.long	.LASF190
 2117 0fb1 13       		.byte	0x13
 2118 0fb2 7402     		.value	0x274
 2119 0fb4 FD0C0000 		.long	0xcfd
 2120 0fb8 D10F0000 		.long	0xfd1
 2121 0fbc 0A       		.uleb128 0xa
 2122 0fbd 830D0000 		.long	0xd83
 2123 0fc1 0A       		.uleb128 0xa
 2124 0fc2 840C0000 		.long	0xc84
 2125 0fc6 0A       		.uleb128 0xa
 2126 0fc7 C60D0000 		.long	0xdc6
 2127 0fcb 0A       		.uleb128 0xa
 2128 0fcc 860F0000 		.long	0xf86
 2129 0fd0 00       		.byte	0
 2130 0fd1 32       		.uleb128 0x32
 2131 0fd2 00000000 		.long	.LASF191
 2132 0fd6 13       		.byte	0x13
 2133 0fd7 C002     		.value	0x2c0
 2134 0fd9 FD0C0000 		.long	0xcfd
 2135 0fdd F10F0000 		.long	0xff1
 2136 0fe1 0A       		.uleb128 0xa
 2137 0fe2 C60D0000 		.long	0xdc6
 2138 0fe6 0A       		.uleb128 0xa
 2139 0fe7 C60D0000 		.long	0xdc6
 2140 0feb 0A       		.uleb128 0xa
 2141 0fec 860F0000 		.long	0xf86
 2142 0ff0 00       		.byte	0
 2143 0ff1 32       		.uleb128 0x32
 2144 0ff2 00000000 		.long	.LASF192
 2145 0ff6 13       		.byte	0x13
 2146 0ff7 6F02     		.value	0x26f
 2147 0ff9 FD0C0000 		.long	0xcfd
 2148 0ffd 0C100000 		.long	0x100c
 2149 1001 0A       		.uleb128 0xa
 2150 1002 C60D0000 		.long	0xdc6
 2151 1006 0A       		.uleb128 0xa
 2152 1007 860F0000 		.long	0xf86
 2153 100b 00       		.byte	0
 2154 100c 32       		.uleb128 0x32
 2155 100d 00000000 		.long	.LASF193
 2156 1011 13       		.byte	0x13
 2157 1012 BC02     		.value	0x2bc
 2158 1014 FD0C0000 		.long	0xcfd
 2159 1018 27100000 		.long	0x1027
 2160 101c 0A       		.uleb128 0xa
 2161 101d C60D0000 		.long	0xdc6
 2162 1021 0A       		.uleb128 0xa
 2163 1022 860F0000 		.long	0xf86
 2164 1026 00       		.byte	0
 2165 1027 32       		.uleb128 0x32
 2166 1028 00000000 		.long	.LASF194
 2167 102c 13       		.byte	0x13
 2168 102d 7501     		.value	0x175
 2169 102f 840C0000 		.long	0xc84
 2170 1033 47100000 		.long	0x1047
 2171 1037 0A       		.uleb128 0xa
 2172 1038 47100000 		.long	0x1047
GAS LISTING /tmp/cc45yn9z.s 			page 42


 2173 103c 0A       		.uleb128 0xa
 2174 103d 890D0000 		.long	0xd89
 2175 1041 0A       		.uleb128 0xa
 2176 1042 660E0000 		.long	0xe66
 2177 1046 00       		.byte	0
 2178 1047 31       		.uleb128 0x31
 2179 1048 08       		.byte	0x8
 2180 1049 F60C0000 		.long	0xcf6
 2181 104d 35       		.uleb128 0x35
 2182 104e 00000000 		.long	.LASF195
 2183 1052 13       		.byte	0x13
 2184 1053 9D       		.byte	0x9d
 2185 1054 830D0000 		.long	0xd83
 2186 1058 67100000 		.long	0x1067
 2187 105c 0A       		.uleb128 0xa
 2188 105d 830D0000 		.long	0xd83
 2189 1061 0A       		.uleb128 0xa
 2190 1062 C60D0000 		.long	0xdc6
 2191 1066 00       		.byte	0
 2192 1067 35       		.uleb128 0x35
 2193 1068 00000000 		.long	.LASF196
 2194 106c 13       		.byte	0x13
 2195 106d A6       		.byte	0xa6
 2196 106e FD0C0000 		.long	0xcfd
 2197 1072 81100000 		.long	0x1081
 2198 1076 0A       		.uleb128 0xa
 2199 1077 C60D0000 		.long	0xdc6
 2200 107b 0A       		.uleb128 0xa
 2201 107c C60D0000 		.long	0xdc6
 2202 1080 00       		.byte	0
 2203 1081 35       		.uleb128 0x35
 2204 1082 00000000 		.long	.LASF197
 2205 1086 13       		.byte	0x13
 2206 1087 C3       		.byte	0xc3
 2207 1088 FD0C0000 		.long	0xcfd
 2208 108c 9B100000 		.long	0x109b
 2209 1090 0A       		.uleb128 0xa
 2210 1091 C60D0000 		.long	0xdc6
 2211 1095 0A       		.uleb128 0xa
 2212 1096 C60D0000 		.long	0xdc6
 2213 109a 00       		.byte	0
 2214 109b 35       		.uleb128 0x35
 2215 109c 00000000 		.long	.LASF198
 2216 10a0 13       		.byte	0x13
 2217 10a1 93       		.byte	0x93
 2218 10a2 830D0000 		.long	0xd83
 2219 10a6 B5100000 		.long	0x10b5
 2220 10aa 0A       		.uleb128 0xa
 2221 10ab 830D0000 		.long	0xd83
 2222 10af 0A       		.uleb128 0xa
 2223 10b0 C60D0000 		.long	0xdc6
 2224 10b4 00       		.byte	0
 2225 10b5 35       		.uleb128 0x35
 2226 10b6 00000000 		.long	.LASF199
 2227 10ba 13       		.byte	0x13
 2228 10bb FF       		.byte	0xff
 2229 10bc 840C0000 		.long	0xc84
GAS LISTING /tmp/cc45yn9z.s 			page 43


 2230 10c0 CF100000 		.long	0x10cf
 2231 10c4 0A       		.uleb128 0xa
 2232 10c5 C60D0000 		.long	0xdc6
 2233 10c9 0A       		.uleb128 0xa
 2234 10ca C60D0000 		.long	0xdc6
 2235 10ce 00       		.byte	0
 2236 10cf 32       		.uleb128 0x32
 2237 10d0 00000000 		.long	.LASF200
 2238 10d4 13       		.byte	0x13
 2239 10d5 5A03     		.value	0x35a
 2240 10d7 840C0000 		.long	0xc84
 2241 10db F4100000 		.long	0x10f4
 2242 10df 0A       		.uleb128 0xa
 2243 10e0 830D0000 		.long	0xd83
 2244 10e4 0A       		.uleb128 0xa
 2245 10e5 840C0000 		.long	0xc84
 2246 10e9 0A       		.uleb128 0xa
 2247 10ea C60D0000 		.long	0xdc6
 2248 10ee 0A       		.uleb128 0xa
 2249 10ef F4100000 		.long	0x10f4
 2250 10f3 00       		.byte	0
 2251 10f4 31       		.uleb128 0x31
 2252 10f5 08       		.byte	0x8
 2253 10f6 8A110000 		.long	0x118a
 2254 10fa 36       		.uleb128 0x36
 2255 10fb 746D00   		.string	"tm"
 2256 10fe 38       		.byte	0x38
 2257 10ff 14       		.byte	0x14
 2258 1100 85       		.byte	0x85
 2259 1101 8A110000 		.long	0x118a
 2260 1105 26       		.uleb128 0x26
 2261 1106 00000000 		.long	.LASF201
 2262 110a 14       		.byte	0x14
 2263 110b 87       		.byte	0x87
 2264 110c FD0C0000 		.long	0xcfd
 2265 1110 00       		.byte	0
 2266 1111 26       		.uleb128 0x26
 2267 1112 00000000 		.long	.LASF202
 2268 1116 14       		.byte	0x14
 2269 1117 88       		.byte	0x88
 2270 1118 FD0C0000 		.long	0xcfd
 2271 111c 04       		.byte	0x4
 2272 111d 26       		.uleb128 0x26
 2273 111e 00000000 		.long	.LASF203
 2274 1122 14       		.byte	0x14
 2275 1123 89       		.byte	0x89
 2276 1124 FD0C0000 		.long	0xcfd
 2277 1128 08       		.byte	0x8
 2278 1129 26       		.uleb128 0x26
 2279 112a 00000000 		.long	.LASF204
 2280 112e 14       		.byte	0x14
 2281 112f 8A       		.byte	0x8a
 2282 1130 FD0C0000 		.long	0xcfd
 2283 1134 0C       		.byte	0xc
 2284 1135 26       		.uleb128 0x26
 2285 1136 00000000 		.long	.LASF205
 2286 113a 14       		.byte	0x14
GAS LISTING /tmp/cc45yn9z.s 			page 44


 2287 113b 8B       		.byte	0x8b
 2288 113c FD0C0000 		.long	0xcfd
 2289 1140 10       		.byte	0x10
 2290 1141 26       		.uleb128 0x26
 2291 1142 00000000 		.long	.LASF206
 2292 1146 14       		.byte	0x14
 2293 1147 8C       		.byte	0x8c
 2294 1148 FD0C0000 		.long	0xcfd
 2295 114c 14       		.byte	0x14
 2296 114d 26       		.uleb128 0x26
 2297 114e 00000000 		.long	.LASF207
 2298 1152 14       		.byte	0x14
 2299 1153 8D       		.byte	0x8d
 2300 1154 FD0C0000 		.long	0xcfd
 2301 1158 18       		.byte	0x18
 2302 1159 26       		.uleb128 0x26
 2303 115a 00000000 		.long	.LASF208
 2304 115e 14       		.byte	0x14
 2305 115f 8E       		.byte	0x8e
 2306 1160 FD0C0000 		.long	0xcfd
 2307 1164 1C       		.byte	0x1c
 2308 1165 26       		.uleb128 0x26
 2309 1166 00000000 		.long	.LASF209
 2310 116a 14       		.byte	0x14
 2311 116b 8F       		.byte	0x8f
 2312 116c FD0C0000 		.long	0xcfd
 2313 1170 20       		.byte	0x20
 2314 1171 26       		.uleb128 0x26
 2315 1172 00000000 		.long	.LASF210
 2316 1176 14       		.byte	0x14
 2317 1177 92       		.byte	0x92
 2318 1178 D2120000 		.long	0x12d2
 2319 117c 28       		.byte	0x28
 2320 117d 26       		.uleb128 0x26
 2321 117e 00000000 		.long	.LASF211
 2322 1182 14       		.byte	0x14
 2323 1183 93       		.byte	0x93
 2324 1184 260D0000 		.long	0xd26
 2325 1188 30       		.byte	0x30
 2326 1189 00       		.byte	0
 2327 118a 0B       		.uleb128 0xb
 2328 118b FA100000 		.long	0x10fa
 2329 118f 32       		.uleb128 0x32
 2330 1190 00000000 		.long	.LASF212
 2331 1194 13       		.byte	0x13
 2332 1195 2201     		.value	0x122
 2333 1197 840C0000 		.long	0xc84
 2334 119b A5110000 		.long	0x11a5
 2335 119f 0A       		.uleb128 0xa
 2336 11a0 C60D0000 		.long	0xdc6
 2337 11a4 00       		.byte	0
 2338 11a5 35       		.uleb128 0x35
 2339 11a6 00000000 		.long	.LASF213
 2340 11aa 13       		.byte	0x13
 2341 11ab A1       		.byte	0xa1
 2342 11ac 830D0000 		.long	0xd83
 2343 11b0 C4110000 		.long	0x11c4
GAS LISTING /tmp/cc45yn9z.s 			page 45


 2344 11b4 0A       		.uleb128 0xa
 2345 11b5 830D0000 		.long	0xd83
 2346 11b9 0A       		.uleb128 0xa
 2347 11ba C60D0000 		.long	0xdc6
 2348 11be 0A       		.uleb128 0xa
 2349 11bf 840C0000 		.long	0xc84
 2350 11c3 00       		.byte	0
 2351 11c4 35       		.uleb128 0x35
 2352 11c5 00000000 		.long	.LASF214
 2353 11c9 13       		.byte	0x13
 2354 11ca A9       		.byte	0xa9
 2355 11cb FD0C0000 		.long	0xcfd
 2356 11cf E3110000 		.long	0x11e3
 2357 11d3 0A       		.uleb128 0xa
 2358 11d4 C60D0000 		.long	0xdc6
 2359 11d8 0A       		.uleb128 0xa
 2360 11d9 C60D0000 		.long	0xdc6
 2361 11dd 0A       		.uleb128 0xa
 2362 11de 840C0000 		.long	0xc84
 2363 11e2 00       		.byte	0
 2364 11e3 35       		.uleb128 0x35
 2365 11e4 00000000 		.long	.LASF215
 2366 11e8 13       		.byte	0x13
 2367 11e9 98       		.byte	0x98
 2368 11ea 830D0000 		.long	0xd83
 2369 11ee 02120000 		.long	0x1202
 2370 11f2 0A       		.uleb128 0xa
 2371 11f3 830D0000 		.long	0xd83
 2372 11f7 0A       		.uleb128 0xa
 2373 11f8 C60D0000 		.long	0xdc6
 2374 11fc 0A       		.uleb128 0xa
 2375 11fd 840C0000 		.long	0xc84
 2376 1201 00       		.byte	0
 2377 1202 32       		.uleb128 0x32
 2378 1203 00000000 		.long	.LASF216
 2379 1207 13       		.byte	0x13
 2380 1208 A101     		.value	0x1a1
 2381 120a 840C0000 		.long	0xc84
 2382 120e 27120000 		.long	0x1227
 2383 1212 0A       		.uleb128 0xa
 2384 1213 47100000 		.long	0x1047
 2385 1217 0A       		.uleb128 0xa
 2386 1218 27120000 		.long	0x1227
 2387 121c 0A       		.uleb128 0xa
 2388 121d 840C0000 		.long	0xc84
 2389 1221 0A       		.uleb128 0xa
 2390 1222 660E0000 		.long	0xe66
 2391 1226 00       		.byte	0
 2392 1227 31       		.uleb128 0x31
 2393 1228 08       		.byte	0x8
 2394 1229 C60D0000 		.long	0xdc6
 2395 122d 32       		.uleb128 0x32
 2396 122e 00000000 		.long	.LASF217
 2397 1232 13       		.byte	0x13
 2398 1233 0301     		.value	0x103
 2399 1235 840C0000 		.long	0xc84
 2400 1239 48120000 		.long	0x1248
GAS LISTING /tmp/cc45yn9z.s 			page 46


 2401 123d 0A       		.uleb128 0xa
 2402 123e C60D0000 		.long	0xdc6
 2403 1242 0A       		.uleb128 0xa
 2404 1243 C60D0000 		.long	0xdc6
 2405 1247 00       		.byte	0
 2406 1248 32       		.uleb128 0x32
 2407 1249 00000000 		.long	.LASF218
 2408 124d 13       		.byte	0x13
 2409 124e C501     		.value	0x1c5
 2410 1250 63120000 		.long	0x1263
 2411 1254 63120000 		.long	0x1263
 2412 1258 0A       		.uleb128 0xa
 2413 1259 C60D0000 		.long	0xdc6
 2414 125d 0A       		.uleb128 0xa
 2415 125e 6A120000 		.long	0x126a
 2416 1262 00       		.byte	0
 2417 1263 28       		.uleb128 0x28
 2418 1264 08       		.byte	0x8
 2419 1265 04       		.byte	0x4
 2420 1266 00000000 		.long	.LASF219
 2421 126a 31       		.uleb128 0x31
 2422 126b 08       		.byte	0x8
 2423 126c 830D0000 		.long	0xd83
 2424 1270 32       		.uleb128 0x32
 2425 1271 00000000 		.long	.LASF220
 2426 1275 13       		.byte	0x13
 2427 1276 CC01     		.value	0x1cc
 2428 1278 8B120000 		.long	0x128b
 2429 127c 8B120000 		.long	0x128b
 2430 1280 0A       		.uleb128 0xa
 2431 1281 C60D0000 		.long	0xdc6
 2432 1285 0A       		.uleb128 0xa
 2433 1286 6A120000 		.long	0x126a
 2434 128a 00       		.byte	0
 2435 128b 28       		.uleb128 0x28
 2436 128c 04       		.byte	0x4
 2437 128d 04       		.byte	0x4
 2438 128e 00000000 		.long	.LASF221
 2439 1292 32       		.uleb128 0x32
 2440 1293 00000000 		.long	.LASF222
 2441 1297 13       		.byte	0x13
 2442 1298 1D01     		.value	0x11d
 2443 129a 830D0000 		.long	0xd83
 2444 129e B2120000 		.long	0x12b2
 2445 12a2 0A       		.uleb128 0xa
 2446 12a3 830D0000 		.long	0xd83
 2447 12a7 0A       		.uleb128 0xa
 2448 12a8 C60D0000 		.long	0xdc6
 2449 12ac 0A       		.uleb128 0xa
 2450 12ad 6A120000 		.long	0x126a
 2451 12b1 00       		.byte	0
 2452 12b2 32       		.uleb128 0x32
 2453 12b3 00000000 		.long	.LASF223
 2454 12b7 13       		.byte	0x13
 2455 12b8 D701     		.value	0x1d7
 2456 12ba D2120000 		.long	0x12d2
 2457 12be D2120000 		.long	0x12d2
GAS LISTING /tmp/cc45yn9z.s 			page 47


 2458 12c2 0A       		.uleb128 0xa
 2459 12c3 C60D0000 		.long	0xdc6
 2460 12c7 0A       		.uleb128 0xa
 2461 12c8 6A120000 		.long	0x126a
 2462 12cc 0A       		.uleb128 0xa
 2463 12cd FD0C0000 		.long	0xcfd
 2464 12d1 00       		.byte	0
 2465 12d2 28       		.uleb128 0x28
 2466 12d3 08       		.byte	0x8
 2467 12d4 05       		.byte	0x5
 2468 12d5 00000000 		.long	.LASF224
 2469 12d9 32       		.uleb128 0x32
 2470 12da 00000000 		.long	.LASF225
 2471 12de 13       		.byte	0x13
 2472 12df DC01     		.value	0x1dc
 2473 12e1 8F0C0000 		.long	0xc8f
 2474 12e5 F9120000 		.long	0x12f9
 2475 12e9 0A       		.uleb128 0xa
 2476 12ea C60D0000 		.long	0xdc6
 2477 12ee 0A       		.uleb128 0xa
 2478 12ef 6A120000 		.long	0x126a
 2479 12f3 0A       		.uleb128 0xa
 2480 12f4 FD0C0000 		.long	0xcfd
 2481 12f8 00       		.byte	0
 2482 12f9 35       		.uleb128 0x35
 2483 12fa 00000000 		.long	.LASF226
 2484 12fe 13       		.byte	0x13
 2485 12ff C7       		.byte	0xc7
 2486 1300 840C0000 		.long	0xc84
 2487 1304 18130000 		.long	0x1318
 2488 1308 0A       		.uleb128 0xa
 2489 1309 830D0000 		.long	0xd83
 2490 130d 0A       		.uleb128 0xa
 2491 130e C60D0000 		.long	0xdc6
 2492 1312 0A       		.uleb128 0xa
 2493 1313 840C0000 		.long	0xc84
 2494 1317 00       		.byte	0
 2495 1318 32       		.uleb128 0x32
 2496 1319 00000000 		.long	.LASF227
 2497 131d 13       		.byte	0x13
 2498 131e 6801     		.value	0x168
 2499 1320 FD0C0000 		.long	0xcfd
 2500 1324 2E130000 		.long	0x132e
 2501 1328 0A       		.uleb128 0xa
 2502 1329 960C0000 		.long	0xc96
 2503 132d 00       		.byte	0
 2504 132e 32       		.uleb128 0x32
 2505 132f 00000000 		.long	.LASF228
 2506 1333 13       		.byte	0x13
 2507 1334 4801     		.value	0x148
 2508 1336 FD0C0000 		.long	0xcfd
 2509 133a 4E130000 		.long	0x134e
 2510 133e 0A       		.uleb128 0xa
 2511 133f C60D0000 		.long	0xdc6
 2512 1343 0A       		.uleb128 0xa
 2513 1344 C60D0000 		.long	0xdc6
 2514 1348 0A       		.uleb128 0xa
GAS LISTING /tmp/cc45yn9z.s 			page 48


 2515 1349 840C0000 		.long	0xc84
 2516 134d 00       		.byte	0
 2517 134e 32       		.uleb128 0x32
 2518 134f 00000000 		.long	.LASF229
 2519 1353 13       		.byte	0x13
 2520 1354 4C01     		.value	0x14c
 2521 1356 830D0000 		.long	0xd83
 2522 135a 6E130000 		.long	0x136e
 2523 135e 0A       		.uleb128 0xa
 2524 135f 830D0000 		.long	0xd83
 2525 1363 0A       		.uleb128 0xa
 2526 1364 C60D0000 		.long	0xdc6
 2527 1368 0A       		.uleb128 0xa
 2528 1369 840C0000 		.long	0xc84
 2529 136d 00       		.byte	0
 2530 136e 32       		.uleb128 0x32
 2531 136f 00000000 		.long	.LASF230
 2532 1373 13       		.byte	0x13
 2533 1374 5101     		.value	0x151
 2534 1376 830D0000 		.long	0xd83
 2535 137a 8E130000 		.long	0x138e
 2536 137e 0A       		.uleb128 0xa
 2537 137f 830D0000 		.long	0xd83
 2538 1383 0A       		.uleb128 0xa
 2539 1384 C60D0000 		.long	0xdc6
 2540 1388 0A       		.uleb128 0xa
 2541 1389 840C0000 		.long	0xc84
 2542 138d 00       		.byte	0
 2543 138e 32       		.uleb128 0x32
 2544 138f 00000000 		.long	.LASF231
 2545 1393 13       		.byte	0x13
 2546 1394 5501     		.value	0x155
 2547 1396 830D0000 		.long	0xd83
 2548 139a AE130000 		.long	0x13ae
 2549 139e 0A       		.uleb128 0xa
 2550 139f 830D0000 		.long	0xd83
 2551 13a3 0A       		.uleb128 0xa
 2552 13a4 890D0000 		.long	0xd89
 2553 13a8 0A       		.uleb128 0xa
 2554 13a9 840C0000 		.long	0xc84
 2555 13ad 00       		.byte	0
 2556 13ae 32       		.uleb128 0x32
 2557 13af 00000000 		.long	.LASF232
 2558 13b3 13       		.byte	0x13
 2559 13b4 5C02     		.value	0x25c
 2560 13b6 FD0C0000 		.long	0xcfd
 2561 13ba C5130000 		.long	0x13c5
 2562 13be 0A       		.uleb128 0xa
 2563 13bf C60D0000 		.long	0xdc6
 2564 13c3 33       		.uleb128 0x33
 2565 13c4 00       		.byte	0
 2566 13c5 32       		.uleb128 0x32
 2567 13c6 00000000 		.long	.LASF233
 2568 13ca 13       		.byte	0x13
 2569 13cb 8502     		.value	0x285
 2570 13cd FD0C0000 		.long	0xcfd
 2571 13d1 DC130000 		.long	0x13dc
GAS LISTING /tmp/cc45yn9z.s 			page 49


 2572 13d5 0A       		.uleb128 0xa
 2573 13d6 C60D0000 		.long	0xdc6
 2574 13da 33       		.uleb128 0x33
 2575 13db 00       		.byte	0
 2576 13dc 37       		.uleb128 0x37
 2577 13dd 00000000 		.long	.LASF234
 2578 13e1 13       		.byte	0x13
 2579 13e2 E3       		.byte	0xe3
 2580 13e3 00000000 		.long	.LASF234
 2581 13e7 C60D0000 		.long	0xdc6
 2582 13eb FA130000 		.long	0x13fa
 2583 13ef 0A       		.uleb128 0xa
 2584 13f0 C60D0000 		.long	0xdc6
 2585 13f4 0A       		.uleb128 0xa
 2586 13f5 890D0000 		.long	0xd89
 2587 13f9 00       		.byte	0
 2588 13fa 0D       		.uleb128 0xd
 2589 13fb 00000000 		.long	.LASF235
 2590 13ff 13       		.byte	0x13
 2591 1400 0901     		.value	0x109
 2592 1402 00000000 		.long	.LASF235
 2593 1406 C60D0000 		.long	0xdc6
 2594 140a 19140000 		.long	0x1419
 2595 140e 0A       		.uleb128 0xa
 2596 140f C60D0000 		.long	0xdc6
 2597 1413 0A       		.uleb128 0xa
 2598 1414 C60D0000 		.long	0xdc6
 2599 1418 00       		.byte	0
 2600 1419 37       		.uleb128 0x37
 2601 141a 00000000 		.long	.LASF236
 2602 141e 13       		.byte	0x13
 2603 141f ED       		.byte	0xed
 2604 1420 00000000 		.long	.LASF236
 2605 1424 C60D0000 		.long	0xdc6
 2606 1428 37140000 		.long	0x1437
 2607 142c 0A       		.uleb128 0xa
 2608 142d C60D0000 		.long	0xdc6
 2609 1431 0A       		.uleb128 0xa
 2610 1432 890D0000 		.long	0xd89
 2611 1436 00       		.byte	0
 2612 1437 0D       		.uleb128 0xd
 2613 1438 00000000 		.long	.LASF237
 2614 143c 13       		.byte	0x13
 2615 143d 1401     		.value	0x114
 2616 143f 00000000 		.long	.LASF237
 2617 1443 C60D0000 		.long	0xdc6
 2618 1447 56140000 		.long	0x1456
 2619 144b 0A       		.uleb128 0xa
 2620 144c C60D0000 		.long	0xdc6
 2621 1450 0A       		.uleb128 0xa
 2622 1451 C60D0000 		.long	0xdc6
 2623 1455 00       		.byte	0
 2624 1456 0D       		.uleb128 0xd
 2625 1457 00000000 		.long	.LASF238
 2626 145b 13       		.byte	0x13
 2627 145c 3F01     		.value	0x13f
 2628 145e 00000000 		.long	.LASF238
GAS LISTING /tmp/cc45yn9z.s 			page 50


 2629 1462 C60D0000 		.long	0xdc6
 2630 1466 7A140000 		.long	0x147a
 2631 146a 0A       		.uleb128 0xa
 2632 146b C60D0000 		.long	0xdc6
 2633 146f 0A       		.uleb128 0xa
 2634 1470 890D0000 		.long	0xd89
 2635 1474 0A       		.uleb128 0xa
 2636 1475 840C0000 		.long	0xc84
 2637 1479 00       		.byte	0
 2638 147a 32       		.uleb128 0x32
 2639 147b 00000000 		.long	.LASF239
 2640 147f 13       		.byte	0x13
 2641 1480 CE01     		.value	0x1ce
 2642 1482 95140000 		.long	0x1495
 2643 1486 95140000 		.long	0x1495
 2644 148a 0A       		.uleb128 0xa
 2645 148b C60D0000 		.long	0xdc6
 2646 148f 0A       		.uleb128 0xa
 2647 1490 6A120000 		.long	0x126a
 2648 1494 00       		.byte	0
 2649 1495 28       		.uleb128 0x28
 2650 1496 10       		.byte	0x10
 2651 1497 04       		.byte	0x4
 2652 1498 00000000 		.long	.LASF240
 2653 149c 32       		.uleb128 0x32
 2654 149d 00000000 		.long	.LASF241
 2655 14a1 13       		.byte	0x13
 2656 14a2 E601     		.value	0x1e6
 2657 14a4 BC140000 		.long	0x14bc
 2658 14a8 BC140000 		.long	0x14bc
 2659 14ac 0A       		.uleb128 0xa
 2660 14ad C60D0000 		.long	0xdc6
 2661 14b1 0A       		.uleb128 0xa
 2662 14b2 6A120000 		.long	0x126a
 2663 14b6 0A       		.uleb128 0xa
 2664 14b7 FD0C0000 		.long	0xcfd
 2665 14bb 00       		.byte	0
 2666 14bc 28       		.uleb128 0x28
 2667 14bd 08       		.byte	0x8
 2668 14be 05       		.byte	0x5
 2669 14bf 00000000 		.long	.LASF242
 2670 14c3 32       		.uleb128 0x32
 2671 14c4 00000000 		.long	.LASF243
 2672 14c8 13       		.byte	0x13
 2673 14c9 ED01     		.value	0x1ed
 2674 14cb E3140000 		.long	0x14e3
 2675 14cf E3140000 		.long	0x14e3
 2676 14d3 0A       		.uleb128 0xa
 2677 14d4 C60D0000 		.long	0xdc6
 2678 14d8 0A       		.uleb128 0xa
 2679 14d9 6A120000 		.long	0x126a
 2680 14dd 0A       		.uleb128 0xa
 2681 14de FD0C0000 		.long	0xcfd
 2682 14e2 00       		.byte	0
 2683 14e3 28       		.uleb128 0x28
 2684 14e4 08       		.byte	0x8
 2685 14e5 07       		.byte	0x7
GAS LISTING /tmp/cc45yn9z.s 			page 51


 2686 14e6 00000000 		.long	.LASF244
 2687 14ea 28       		.uleb128 0x28
 2688 14eb 01       		.byte	0x1
 2689 14ec 08       		.byte	0x8
 2690 14ed 00000000 		.long	.LASF245
 2691 14f1 28       		.uleb128 0x28
 2692 14f2 01       		.byte	0x1
 2693 14f3 06       		.byte	0x6
 2694 14f4 00000000 		.long	.LASF246
 2695 14f8 28       		.uleb128 0x28
 2696 14f9 02       		.byte	0x2
 2697 14fa 05       		.byte	0x5
 2698 14fb 00000000 		.long	.LASF247
 2699 14ff 23       		.uleb128 0x23
 2700 1500 00000000 		.long	.LASF248
 2701 1504 05       		.byte	0x5
 2702 1505 37       		.byte	0x37
 2703 1506 12150000 		.long	0x1512
 2704 150a 04       		.uleb128 0x4
 2705 150b 05       		.byte	0x5
 2706 150c 38       		.byte	0x38
 2707 150d F4010000 		.long	0x1f4
 2708 1511 00       		.byte	0
 2709 1512 38       		.uleb128 0x38
 2710 1513 08       		.byte	0x8
 2711 1514 07020000 		.long	0x207
 2712 1518 38       		.uleb128 0x38
 2713 1519 08       		.byte	0x8
 2714 151a 37020000 		.long	0x237
 2715 151e 28       		.uleb128 0x28
 2716 151f 01       		.byte	0x1
 2717 1520 02       		.byte	0x2
 2718 1521 00000000 		.long	.LASF249
 2719 1525 31       		.uleb128 0x31
 2720 1526 08       		.byte	0x8
 2721 1527 37020000 		.long	0x237
 2722 152b 31       		.uleb128 0x31
 2723 152c 08       		.byte	0x8
 2724 152d 07020000 		.long	0x207
 2725 1531 38       		.uleb128 0x38
 2726 1532 08       		.byte	0x8
 2727 1533 5E030000 		.long	0x35e
 2728 1537 07       		.uleb128 0x7
 2729 1538 00000000 		.long	.LASF250
 2730 153c 60       		.byte	0x60
 2731 153d 15       		.byte	0x15
 2732 153e 35       		.byte	0x35
 2733 153f 64160000 		.long	0x1664
 2734 1543 26       		.uleb128 0x26
 2735 1544 00000000 		.long	.LASF251
 2736 1548 15       		.byte	0x15
 2737 1549 39       		.byte	0x39
 2738 154a 47100000 		.long	0x1047
 2739 154e 00       		.byte	0
 2740 154f 26       		.uleb128 0x26
 2741 1550 00000000 		.long	.LASF252
 2742 1554 15       		.byte	0x15
GAS LISTING /tmp/cc45yn9z.s 			page 52


 2743 1555 3A       		.byte	0x3a
 2744 1556 47100000 		.long	0x1047
 2745 155a 08       		.byte	0x8
 2746 155b 26       		.uleb128 0x26
 2747 155c 00000000 		.long	.LASF253
 2748 1560 15       		.byte	0x15
 2749 1561 40       		.byte	0x40
 2750 1562 47100000 		.long	0x1047
 2751 1566 10       		.byte	0x10
 2752 1567 26       		.uleb128 0x26
 2753 1568 00000000 		.long	.LASF254
 2754 156c 15       		.byte	0x15
 2755 156d 46       		.byte	0x46
 2756 156e 47100000 		.long	0x1047
 2757 1572 18       		.byte	0x18
 2758 1573 26       		.uleb128 0x26
 2759 1574 00000000 		.long	.LASF255
 2760 1578 15       		.byte	0x15
 2761 1579 47       		.byte	0x47
 2762 157a 47100000 		.long	0x1047
 2763 157e 20       		.byte	0x20
 2764 157f 26       		.uleb128 0x26
 2765 1580 00000000 		.long	.LASF256
 2766 1584 15       		.byte	0x15
 2767 1585 48       		.byte	0x48
 2768 1586 47100000 		.long	0x1047
 2769 158a 28       		.byte	0x28
 2770 158b 26       		.uleb128 0x26
 2771 158c 00000000 		.long	.LASF257
 2772 1590 15       		.byte	0x15
 2773 1591 49       		.byte	0x49
 2774 1592 47100000 		.long	0x1047
 2775 1596 30       		.byte	0x30
 2776 1597 26       		.uleb128 0x26
 2777 1598 00000000 		.long	.LASF258
 2778 159c 15       		.byte	0x15
 2779 159d 4A       		.byte	0x4a
 2780 159e 47100000 		.long	0x1047
 2781 15a2 38       		.byte	0x38
 2782 15a3 26       		.uleb128 0x26
 2783 15a4 00000000 		.long	.LASF259
 2784 15a8 15       		.byte	0x15
 2785 15a9 4B       		.byte	0x4b
 2786 15aa 47100000 		.long	0x1047
 2787 15ae 40       		.byte	0x40
 2788 15af 26       		.uleb128 0x26
 2789 15b0 00000000 		.long	.LASF260
 2790 15b4 15       		.byte	0x15
 2791 15b5 4C       		.byte	0x4c
 2792 15b6 47100000 		.long	0x1047
 2793 15ba 48       		.byte	0x48
 2794 15bb 26       		.uleb128 0x26
 2795 15bc 00000000 		.long	.LASF261
 2796 15c0 15       		.byte	0x15
 2797 15c1 4D       		.byte	0x4d
 2798 15c2 F60C0000 		.long	0xcf6
 2799 15c6 50       		.byte	0x50
GAS LISTING /tmp/cc45yn9z.s 			page 53


 2800 15c7 26       		.uleb128 0x26
 2801 15c8 00000000 		.long	.LASF262
 2802 15cc 15       		.byte	0x15
 2803 15cd 4E       		.byte	0x4e
 2804 15ce F60C0000 		.long	0xcf6
 2805 15d2 51       		.byte	0x51
 2806 15d3 26       		.uleb128 0x26
 2807 15d4 00000000 		.long	.LASF263
 2808 15d8 15       		.byte	0x15
 2809 15d9 50       		.byte	0x50
 2810 15da F60C0000 		.long	0xcf6
 2811 15de 52       		.byte	0x52
 2812 15df 26       		.uleb128 0x26
 2813 15e0 00000000 		.long	.LASF264
 2814 15e4 15       		.byte	0x15
 2815 15e5 52       		.byte	0x52
 2816 15e6 F60C0000 		.long	0xcf6
 2817 15ea 53       		.byte	0x53
 2818 15eb 26       		.uleb128 0x26
 2819 15ec 00000000 		.long	.LASF265
 2820 15f0 15       		.byte	0x15
 2821 15f1 54       		.byte	0x54
 2822 15f2 F60C0000 		.long	0xcf6
 2823 15f6 54       		.byte	0x54
 2824 15f7 26       		.uleb128 0x26
 2825 15f8 00000000 		.long	.LASF266
 2826 15fc 15       		.byte	0x15
 2827 15fd 56       		.byte	0x56
 2828 15fe F60C0000 		.long	0xcf6
 2829 1602 55       		.byte	0x55
 2830 1603 26       		.uleb128 0x26
 2831 1604 00000000 		.long	.LASF267
 2832 1608 15       		.byte	0x15
 2833 1609 5D       		.byte	0x5d
 2834 160a F60C0000 		.long	0xcf6
 2835 160e 56       		.byte	0x56
 2836 160f 26       		.uleb128 0x26
 2837 1610 00000000 		.long	.LASF268
 2838 1614 15       		.byte	0x15
 2839 1615 5E       		.byte	0x5e
 2840 1616 F60C0000 		.long	0xcf6
 2841 161a 57       		.byte	0x57
 2842 161b 26       		.uleb128 0x26
 2843 161c 00000000 		.long	.LASF269
 2844 1620 15       		.byte	0x15
 2845 1621 61       		.byte	0x61
 2846 1622 F60C0000 		.long	0xcf6
 2847 1626 58       		.byte	0x58
 2848 1627 26       		.uleb128 0x26
 2849 1628 00000000 		.long	.LASF270
 2850 162c 15       		.byte	0x15
 2851 162d 63       		.byte	0x63
 2852 162e F60C0000 		.long	0xcf6
 2853 1632 59       		.byte	0x59
 2854 1633 26       		.uleb128 0x26
 2855 1634 00000000 		.long	.LASF271
 2856 1638 15       		.byte	0x15
GAS LISTING /tmp/cc45yn9z.s 			page 54


 2857 1639 65       		.byte	0x65
 2858 163a F60C0000 		.long	0xcf6
 2859 163e 5A       		.byte	0x5a
 2860 163f 26       		.uleb128 0x26
 2861 1640 00000000 		.long	.LASF272
 2862 1644 15       		.byte	0x15
 2863 1645 67       		.byte	0x67
 2864 1646 F60C0000 		.long	0xcf6
 2865 164a 5B       		.byte	0x5b
 2866 164b 26       		.uleb128 0x26
 2867 164c 00000000 		.long	.LASF273
 2868 1650 15       		.byte	0x15
 2869 1651 6E       		.byte	0x6e
 2870 1652 F60C0000 		.long	0xcf6
 2871 1656 5C       		.byte	0x5c
 2872 1657 26       		.uleb128 0x26
 2873 1658 00000000 		.long	.LASF274
 2874 165c 15       		.byte	0x15
 2875 165d 6F       		.byte	0x6f
 2876 165e F60C0000 		.long	0xcf6
 2877 1662 5D       		.byte	0x5d
 2878 1663 00       		.byte	0
 2879 1664 35       		.uleb128 0x35
 2880 1665 00000000 		.long	.LASF275
 2881 1669 15       		.byte	0x15
 2882 166a 7C       		.byte	0x7c
 2883 166b 47100000 		.long	0x1047
 2884 166f 7E160000 		.long	0x167e
 2885 1673 0A       		.uleb128 0xa
 2886 1674 FD0C0000 		.long	0xcfd
 2887 1678 0A       		.uleb128 0xa
 2888 1679 260D0000 		.long	0xd26
 2889 167d 00       		.byte	0
 2890 167e 39       		.uleb128 0x39
 2891 167f 00000000 		.long	.LASF277
 2892 1683 15       		.byte	0x15
 2893 1684 7F       		.byte	0x7f
 2894 1685 89160000 		.long	0x1689
 2895 1689 31       		.uleb128 0x31
 2896 168a 08       		.byte	0x8
 2897 168b 37150000 		.long	0x1537
 2898 168f 08       		.uleb128 0x8
 2899 1690 00000000 		.long	.LASF278
 2900 1694 16       		.byte	0x16
 2901 1695 28       		.byte	0x28
 2902 1696 FD0C0000 		.long	0xcfd
 2903 169a 08       		.uleb128 0x8
 2904 169b 00000000 		.long	.LASF279
 2905 169f 16       		.byte	0x16
 2906 16a0 83       		.byte	0x83
 2907 16a1 D2120000 		.long	0x12d2
 2908 16a5 08       		.uleb128 0x8
 2909 16a6 00000000 		.long	.LASF280
 2910 16aa 16       		.byte	0x16
 2911 16ab 84       		.byte	0x84
 2912 16ac D2120000 		.long	0x12d2
 2913 16b0 08       		.uleb128 0x8
GAS LISTING /tmp/cc45yn9z.s 			page 55


 2914 16b1 00000000 		.long	.LASF281
 2915 16b5 17       		.byte	0x17
 2916 16b6 20       		.byte	0x20
 2917 16b7 FD0C0000 		.long	0xcfd
 2918 16bb 0B       		.uleb128 0xb
 2919 16bc 1E150000 		.long	0x151e
 2920 16c0 0B       		.uleb128 0xb
 2921 16c1 8F0C0000 		.long	0xc8f
 2922 16c5 31       		.uleb128 0x31
 2923 16c6 08       		.byte	0x8
 2924 16c7 5A050000 		.long	0x55a
 2925 16cb 08       		.uleb128 0x8
 2926 16cc 00000000 		.long	.LASF282
 2927 16d0 18       		.byte	0x18
 2928 16d1 34       		.byte	0x34
 2929 16d2 8F0C0000 		.long	0xc8f
 2930 16d6 08       		.uleb128 0x8
 2931 16d7 00000000 		.long	.LASF283
 2932 16db 18       		.byte	0x18
 2933 16dc BA       		.byte	0xba
 2934 16dd E1160000 		.long	0x16e1
 2935 16e1 31       		.uleb128 0x31
 2936 16e2 08       		.byte	0x8
 2937 16e3 E7160000 		.long	0x16e7
 2938 16e7 0B       		.uleb128 0xb
 2939 16e8 8F160000 		.long	0x168f
 2940 16ec 35       		.uleb128 0x35
 2941 16ed 00000000 		.long	.LASF284
 2942 16f1 18       		.byte	0x18
 2943 16f2 AF       		.byte	0xaf
 2944 16f3 FD0C0000 		.long	0xcfd
 2945 16f7 06170000 		.long	0x1706
 2946 16fb 0A       		.uleb128 0xa
 2947 16fc 960C0000 		.long	0xc96
 2948 1700 0A       		.uleb128 0xa
 2949 1701 CB160000 		.long	0x16cb
 2950 1705 00       		.byte	0
 2951 1706 35       		.uleb128 0x35
 2952 1707 00000000 		.long	.LASF285
 2953 170b 18       		.byte	0x18
 2954 170c DD       		.byte	0xdd
 2955 170d 960C0000 		.long	0xc96
 2956 1711 20170000 		.long	0x1720
 2957 1715 0A       		.uleb128 0xa
 2958 1716 960C0000 		.long	0xc96
 2959 171a 0A       		.uleb128 0xa
 2960 171b D6160000 		.long	0x16d6
 2961 171f 00       		.byte	0
 2962 1720 35       		.uleb128 0x35
 2963 1721 00000000 		.long	.LASF286
 2964 1725 18       		.byte	0x18
 2965 1726 DA       		.byte	0xda
 2966 1727 D6160000 		.long	0x16d6
 2967 172b 35170000 		.long	0x1735
 2968 172f 0A       		.uleb128 0xa
 2969 1730 260D0000 		.long	0xd26
 2970 1734 00       		.byte	0
GAS LISTING /tmp/cc45yn9z.s 			page 56


 2971 1735 35       		.uleb128 0x35
 2972 1736 00000000 		.long	.LASF287
 2973 173a 18       		.byte	0x18
 2974 173b AB       		.byte	0xab
 2975 173c CB160000 		.long	0x16cb
 2976 1740 4A170000 		.long	0x174a
 2977 1744 0A       		.uleb128 0xa
 2978 1745 260D0000 		.long	0xd26
 2979 1749 00       		.byte	0
 2980 174a 0B       		.uleb128 0xb
 2981 174b F8140000 		.long	0x14f8
 2982 174f 0B       		.uleb128 0xb
 2983 1750 D2120000 		.long	0x12d2
 2984 1754 2B       		.uleb128 0x2b
 2985 1755 10       		.byte	0x10
 2986 1756 19       		.byte	0x19
 2987 1757 16       		.byte	0x16
 2988 1758 00000000 		.long	.LASF289
 2989 175c 79170000 		.long	0x1779
 2990 1760 26       		.uleb128 0x26
 2991 1761 00000000 		.long	.LASF290
 2992 1765 19       		.byte	0x19
 2993 1766 17       		.byte	0x17
 2994 1767 9A160000 		.long	0x169a
 2995 176b 00       		.byte	0
 2996 176c 26       		.uleb128 0x26
 2997 176d 00000000 		.long	.LASF291
 2998 1771 19       		.byte	0x19
 2999 1772 18       		.byte	0x18
 3000 1773 040D0000 		.long	0xd04
 3001 1777 08       		.byte	0x8
 3002 1778 00       		.byte	0
 3003 1779 08       		.uleb128 0x8
 3004 177a 00000000 		.long	.LASF292
 3005 177e 19       		.byte	0x19
 3006 177f 19       		.byte	0x19
 3007 1780 54170000 		.long	0x1754
 3008 1784 3A       		.uleb128 0x3a
 3009 1785 00000000 		.long	.LASF348
 3010 1789 10       		.byte	0x10
 3011 178a 96       		.byte	0x96
 3012 178b 07       		.uleb128 0x7
 3013 178c 00000000 		.long	.LASF293
 3014 1790 18       		.byte	0x18
 3015 1791 10       		.byte	0x10
 3016 1792 9C       		.byte	0x9c
 3017 1793 BC170000 		.long	0x17bc
 3018 1797 26       		.uleb128 0x26
 3019 1798 00000000 		.long	.LASF294
 3020 179c 10       		.byte	0x10
 3021 179d 9D       		.byte	0x9d
 3022 179e BC170000 		.long	0x17bc
 3023 17a2 00       		.byte	0
 3024 17a3 26       		.uleb128 0x26
 3025 17a4 00000000 		.long	.LASF295
 3026 17a8 10       		.byte	0x10
 3027 17a9 9E       		.byte	0x9e
GAS LISTING /tmp/cc45yn9z.s 			page 57


 3028 17aa C2170000 		.long	0x17c2
 3029 17ae 08       		.byte	0x8
 3030 17af 26       		.uleb128 0x26
 3031 17b0 00000000 		.long	.LASF296
 3032 17b4 10       		.byte	0x10
 3033 17b5 A2       		.byte	0xa2
 3034 17b6 FD0C0000 		.long	0xcfd
 3035 17ba 10       		.byte	0x10
 3036 17bb 00       		.byte	0
 3037 17bc 31       		.uleb128 0x31
 3038 17bd 08       		.byte	0x8
 3039 17be 8B170000 		.long	0x178b
 3040 17c2 31       		.uleb128 0x31
 3041 17c3 08       		.byte	0x8
 3042 17c4 AF0A0000 		.long	0xaaf
 3043 17c8 2E       		.uleb128 0x2e
 3044 17c9 F60C0000 		.long	0xcf6
 3045 17cd D8170000 		.long	0x17d8
 3046 17d1 2F       		.uleb128 0x2f
 3047 17d2 370C0000 		.long	0xc37
 3048 17d6 00       		.byte	0
 3049 17d7 00       		.byte	0
 3050 17d8 31       		.uleb128 0x31
 3051 17d9 08       		.byte	0x8
 3052 17da 84170000 		.long	0x1784
 3053 17de 2E       		.uleb128 0x2e
 3054 17df F60C0000 		.long	0xcf6
 3055 17e3 EE170000 		.long	0x17ee
 3056 17e7 2F       		.uleb128 0x2f
 3057 17e8 370C0000 		.long	0xc37
 3058 17ec 13       		.byte	0x13
 3059 17ed 00       		.byte	0
 3060 17ee 08       		.uleb128 0x8
 3061 17ef 00000000 		.long	.LASF297
 3062 17f3 0F       		.byte	0xf
 3063 17f4 6E       		.byte	0x6e
 3064 17f5 79170000 		.long	0x1779
 3065 17f9 3B       		.uleb128 0x3b
 3066 17fa 00000000 		.long	.LASF314
 3067 17fe 0F       		.byte	0xf
 3068 17ff 3A03     		.value	0x33a
 3069 1801 0B180000 		.long	0x180b
 3070 1805 0A       		.uleb128 0xa
 3071 1806 0B180000 		.long	0x180b
 3072 180a 00       		.byte	0
 3073 180b 31       		.uleb128 0x31
 3074 180c 08       		.byte	0x8
 3075 180d A40A0000 		.long	0xaa4
 3076 1811 35       		.uleb128 0x35
 3077 1812 00000000 		.long	.LASF298
 3078 1816 0F       		.byte	0xf
 3079 1817 ED       		.byte	0xed
 3080 1818 FD0C0000 		.long	0xcfd
 3081 181c 26180000 		.long	0x1826
 3082 1820 0A       		.uleb128 0xa
 3083 1821 0B180000 		.long	0x180b
 3084 1825 00       		.byte	0
GAS LISTING /tmp/cc45yn9z.s 			page 58


 3085 1826 32       		.uleb128 0x32
 3086 1827 00000000 		.long	.LASF299
 3087 182b 0F       		.byte	0xf
 3088 182c 3C03     		.value	0x33c
 3089 182e FD0C0000 		.long	0xcfd
 3090 1832 3C180000 		.long	0x183c
 3091 1836 0A       		.uleb128 0xa
 3092 1837 0B180000 		.long	0x180b
 3093 183b 00       		.byte	0
 3094 183c 32       		.uleb128 0x32
 3095 183d 00000000 		.long	.LASF300
 3096 1841 0F       		.byte	0xf
 3097 1842 3E03     		.value	0x33e
 3098 1844 FD0C0000 		.long	0xcfd
 3099 1848 52180000 		.long	0x1852
 3100 184c 0A       		.uleb128 0xa
 3101 184d 0B180000 		.long	0x180b
 3102 1851 00       		.byte	0
 3103 1852 35       		.uleb128 0x35
 3104 1853 00000000 		.long	.LASF301
 3105 1857 0F       		.byte	0xf
 3106 1858 F2       		.byte	0xf2
 3107 1859 FD0C0000 		.long	0xcfd
 3108 185d 67180000 		.long	0x1867
 3109 1861 0A       		.uleb128 0xa
 3110 1862 0B180000 		.long	0x180b
 3111 1866 00       		.byte	0
 3112 1867 32       		.uleb128 0x32
 3113 1868 00000000 		.long	.LASF302
 3114 186c 0F       		.byte	0xf
 3115 186d 1302     		.value	0x213
 3116 186f FD0C0000 		.long	0xcfd
 3117 1873 7D180000 		.long	0x187d
 3118 1877 0A       		.uleb128 0xa
 3119 1878 0B180000 		.long	0x180b
 3120 187c 00       		.byte	0
 3121 187d 32       		.uleb128 0x32
 3122 187e 00000000 		.long	.LASF303
 3123 1882 0F       		.byte	0xf
 3124 1883 1E03     		.value	0x31e
 3125 1885 FD0C0000 		.long	0xcfd
 3126 1889 98180000 		.long	0x1898
 3127 188d 0A       		.uleb128 0xa
 3128 188e 0B180000 		.long	0x180b
 3129 1892 0A       		.uleb128 0xa
 3130 1893 98180000 		.long	0x1898
 3131 1897 00       		.byte	0
 3132 1898 31       		.uleb128 0x31
 3133 1899 08       		.byte	0x8
 3134 189a EE170000 		.long	0x17ee
 3135 189e 32       		.uleb128 0x32
 3136 189f 00000000 		.long	.LASF304
 3137 18a3 0F       		.byte	0xf
 3138 18a4 6E02     		.value	0x26e
 3139 18a6 47100000 		.long	0x1047
 3140 18aa BE180000 		.long	0x18be
 3141 18ae 0A       		.uleb128 0xa
GAS LISTING /tmp/cc45yn9z.s 			page 59


 3142 18af 47100000 		.long	0x1047
 3143 18b3 0A       		.uleb128 0xa
 3144 18b4 FD0C0000 		.long	0xcfd
 3145 18b8 0A       		.uleb128 0xa
 3146 18b9 0B180000 		.long	0x180b
 3147 18bd 00       		.byte	0
 3148 18be 32       		.uleb128 0x32
 3149 18bf 00000000 		.long	.LASF305
 3150 18c3 0F       		.byte	0xf
 3151 18c4 1001     		.value	0x110
 3152 18c6 0B180000 		.long	0x180b
 3153 18ca D9180000 		.long	0x18d9
 3154 18ce 0A       		.uleb128 0xa
 3155 18cf 260D0000 		.long	0xd26
 3156 18d3 0A       		.uleb128 0xa
 3157 18d4 260D0000 		.long	0xd26
 3158 18d8 00       		.byte	0
 3159 18d9 32       		.uleb128 0x32
 3160 18da 00000000 		.long	.LASF306
 3161 18de 0F       		.byte	0xf
 3162 18df C502     		.value	0x2c5
 3163 18e1 840C0000 		.long	0xc84
 3164 18e5 FE180000 		.long	0x18fe
 3165 18e9 0A       		.uleb128 0xa
 3166 18ea 820C0000 		.long	0xc82
 3167 18ee 0A       		.uleb128 0xa
 3168 18ef 840C0000 		.long	0xc84
 3169 18f3 0A       		.uleb128 0xa
 3170 18f4 840C0000 		.long	0xc84
 3171 18f8 0A       		.uleb128 0xa
 3172 18f9 0B180000 		.long	0x180b
 3173 18fd 00       		.byte	0
 3174 18fe 32       		.uleb128 0x32
 3175 18ff 00000000 		.long	.LASF307
 3176 1903 0F       		.byte	0xf
 3177 1904 1601     		.value	0x116
 3178 1906 0B180000 		.long	0x180b
 3179 190a 1E190000 		.long	0x191e
 3180 190e 0A       		.uleb128 0xa
 3181 190f 260D0000 		.long	0xd26
 3182 1913 0A       		.uleb128 0xa
 3183 1914 260D0000 		.long	0xd26
 3184 1918 0A       		.uleb128 0xa
 3185 1919 0B180000 		.long	0x180b
 3186 191d 00       		.byte	0
 3187 191e 32       		.uleb128 0x32
 3188 191f 00000000 		.long	.LASF308
 3189 1923 0F       		.byte	0xf
 3190 1924 ED02     		.value	0x2ed
 3191 1926 FD0C0000 		.long	0xcfd
 3192 192a 3E190000 		.long	0x193e
 3193 192e 0A       		.uleb128 0xa
 3194 192f 0B180000 		.long	0x180b
 3195 1933 0A       		.uleb128 0xa
 3196 1934 D2120000 		.long	0x12d2
 3197 1938 0A       		.uleb128 0xa
 3198 1939 FD0C0000 		.long	0xcfd
GAS LISTING /tmp/cc45yn9z.s 			page 60


 3199 193d 00       		.byte	0
 3200 193e 32       		.uleb128 0x32
 3201 193f 00000000 		.long	.LASF309
 3202 1943 0F       		.byte	0xf
 3203 1944 2303     		.value	0x323
 3204 1946 FD0C0000 		.long	0xcfd
 3205 194a 59190000 		.long	0x1959
 3206 194e 0A       		.uleb128 0xa
 3207 194f 0B180000 		.long	0x180b
 3208 1953 0A       		.uleb128 0xa
 3209 1954 59190000 		.long	0x1959
 3210 1958 00       		.byte	0
 3211 1959 31       		.uleb128 0x31
 3212 195a 08       		.byte	0x8
 3213 195b 5F190000 		.long	0x195f
 3214 195f 0B       		.uleb128 0xb
 3215 1960 EE170000 		.long	0x17ee
 3216 1964 32       		.uleb128 0x32
 3217 1965 00000000 		.long	.LASF310
 3218 1969 0F       		.byte	0xf
 3219 196a F202     		.value	0x2f2
 3220 196c D2120000 		.long	0x12d2
 3221 1970 7A190000 		.long	0x197a
 3222 1974 0A       		.uleb128 0xa
 3223 1975 0B180000 		.long	0x180b
 3224 1979 00       		.byte	0
 3225 197a 32       		.uleb128 0x32
 3226 197b 00000000 		.long	.LASF311
 3227 197f 0F       		.byte	0xf
 3228 1980 1402     		.value	0x214
 3229 1982 FD0C0000 		.long	0xcfd
 3230 1986 90190000 		.long	0x1990
 3231 198a 0A       		.uleb128 0xa
 3232 198b 0B180000 		.long	0x180b
 3233 198f 00       		.byte	0
 3234 1990 34       		.uleb128 0x34
 3235 1991 00000000 		.long	.LASF312
 3236 1995 0F       		.byte	0xf
 3237 1996 1A02     		.value	0x21a
 3238 1998 FD0C0000 		.long	0xcfd
 3239 199c 32       		.uleb128 0x32
 3240 199d 00000000 		.long	.LASF313
 3241 19a1 0F       		.byte	0xf
 3242 19a2 7E02     		.value	0x27e
 3243 19a4 47100000 		.long	0x1047
 3244 19a8 B2190000 		.long	0x19b2
 3245 19ac 0A       		.uleb128 0xa
 3246 19ad 47100000 		.long	0x1047
 3247 19b1 00       		.byte	0
 3248 19b2 3B       		.uleb128 0x3b
 3249 19b3 00000000 		.long	.LASF315
 3250 19b7 0F       		.byte	0xf
 3251 19b8 4E03     		.value	0x34e
 3252 19ba C4190000 		.long	0x19c4
 3253 19be 0A       		.uleb128 0xa
 3254 19bf 260D0000 		.long	0xd26
 3255 19c3 00       		.byte	0
GAS LISTING /tmp/cc45yn9z.s 			page 61


 3256 19c4 35       		.uleb128 0x35
 3257 19c5 00000000 		.long	.LASF316
 3258 19c9 0F       		.byte	0xf
 3259 19ca B2       		.byte	0xb2
 3260 19cb FD0C0000 		.long	0xcfd
 3261 19cf D9190000 		.long	0x19d9
 3262 19d3 0A       		.uleb128 0xa
 3263 19d4 260D0000 		.long	0xd26
 3264 19d8 00       		.byte	0
 3265 19d9 35       		.uleb128 0x35
 3266 19da 00000000 		.long	.LASF317
 3267 19de 0F       		.byte	0xf
 3268 19df B4       		.byte	0xb4
 3269 19e0 FD0C0000 		.long	0xcfd
 3270 19e4 F3190000 		.long	0x19f3
 3271 19e8 0A       		.uleb128 0xa
 3272 19e9 260D0000 		.long	0xd26
 3273 19ed 0A       		.uleb128 0xa
 3274 19ee 260D0000 		.long	0xd26
 3275 19f2 00       		.byte	0
 3276 19f3 3B       		.uleb128 0x3b
 3277 19f4 00000000 		.long	.LASF318
 3278 19f8 0F       		.byte	0xf
 3279 19f9 F702     		.value	0x2f7
 3280 19fb 051A0000 		.long	0x1a05
 3281 19ff 0A       		.uleb128 0xa
 3282 1a00 0B180000 		.long	0x180b
 3283 1a04 00       		.byte	0
 3284 1a05 3B       		.uleb128 0x3b
 3285 1a06 00000000 		.long	.LASF319
 3286 1a0a 0F       		.byte	0xf
 3287 1a0b 4C01     		.value	0x14c
 3288 1a0d 1C1A0000 		.long	0x1a1c
 3289 1a11 0A       		.uleb128 0xa
 3290 1a12 0B180000 		.long	0x180b
 3291 1a16 0A       		.uleb128 0xa
 3292 1a17 47100000 		.long	0x1047
 3293 1a1b 00       		.byte	0
 3294 1a1c 32       		.uleb128 0x32
 3295 1a1d 00000000 		.long	.LASF320
 3296 1a21 0F       		.byte	0xf
 3297 1a22 5001     		.value	0x150
 3298 1a24 FD0C0000 		.long	0xcfd
 3299 1a28 411A0000 		.long	0x1a41
 3300 1a2c 0A       		.uleb128 0xa
 3301 1a2d 0B180000 		.long	0x180b
 3302 1a31 0A       		.uleb128 0xa
 3303 1a32 47100000 		.long	0x1047
 3304 1a36 0A       		.uleb128 0xa
 3305 1a37 FD0C0000 		.long	0xcfd
 3306 1a3b 0A       		.uleb128 0xa
 3307 1a3c 840C0000 		.long	0xc84
 3308 1a40 00       		.byte	0
 3309 1a41 39       		.uleb128 0x39
 3310 1a42 00000000 		.long	.LASF321
 3311 1a46 0F       		.byte	0xf
 3312 1a47 C3       		.byte	0xc3
GAS LISTING /tmp/cc45yn9z.s 			page 62


 3313 1a48 0B180000 		.long	0x180b
 3314 1a4c 35       		.uleb128 0x35
 3315 1a4d 00000000 		.long	.LASF322
 3316 1a51 0F       		.byte	0xf
 3317 1a52 D1       		.byte	0xd1
 3318 1a53 47100000 		.long	0x1047
 3319 1a57 611A0000 		.long	0x1a61
 3320 1a5b 0A       		.uleb128 0xa
 3321 1a5c 47100000 		.long	0x1047
 3322 1a60 00       		.byte	0
 3323 1a61 32       		.uleb128 0x32
 3324 1a62 00000000 		.long	.LASF323
 3325 1a66 0F       		.byte	0xf
 3326 1a67 BE02     		.value	0x2be
 3327 1a69 FD0C0000 		.long	0xcfd
 3328 1a6d 7C1A0000 		.long	0x1a7c
 3329 1a71 0A       		.uleb128 0xa
 3330 1a72 FD0C0000 		.long	0xcfd
 3331 1a76 0A       		.uleb128 0xa
 3332 1a77 0B180000 		.long	0x180b
 3333 1a7b 00       		.byte	0
 3334 1a7c 3C       		.uleb128 0x3c
 3335 1a7d 00000000 		.long	.LASF349
 3336 1a81 01       		.byte	0x1
 3337 1a82 10       		.byte	0x10
 3338 1a83 FD0C0000 		.long	0xcfd
 3339 1a87 00000000 		.quad	.LFB1085
 3339      00000000 
 3340 1a8f 2C010000 		.quad	.LFE1085-.LFB1085
 3340      00000000 
 3341 1a97 01       		.uleb128 0x1
 3342 1a98 9C       		.byte	0x9c
 3343 1a99 C71A0000 		.long	0x1ac7
 3344 1a9d 3D       		.uleb128 0x3d
 3345 1a9e 00000000 		.long	.LASF325
 3346 1aa2 01       		.byte	0x1
 3347 1aa3 12       		.byte	0x12
 3348 1aa4 CB080000 		.long	0x8cb
 3349 1aa8 03       		.uleb128 0x3
 3350 1aa9 91       		.byte	0x91
 3351 1aaa D07B     		.sleb128 -560
 3352 1aac 3E       		.uleb128 0x3e
 3353 1aad 6900     		.string	"i"
 3354 1aaf 01       		.byte	0x1
 3355 1ab0 14       		.byte	0x14
 3356 1ab1 FD0C0000 		.long	0xcfd
 3357 1ab5 03       		.uleb128 0x3
 3358 1ab6 91       		.byte	0x91
 3359 1ab7 C47B     		.sleb128 -572
 3360 1ab9 3E       		.uleb128 0x3e
 3361 1aba 6400     		.string	"d"
 3362 1abc 01       		.byte	0x1
 3363 1abd 15       		.byte	0x15
 3364 1abe 63120000 		.long	0x1263
 3365 1ac2 03       		.uleb128 0x3
 3366 1ac3 91       		.byte	0x91
 3367 1ac4 C87B     		.sleb128 -568
GAS LISTING /tmp/cc45yn9z.s 			page 63


 3368 1ac6 00       		.byte	0
 3369 1ac7 3F       		.uleb128 0x3f
 3370 1ac8 00000000 		.long	.LASF350
 3371 1acc 00000000 		.quad	.LFB1127
 3371      00000000 
 3372 1ad4 3E000000 		.quad	.LFE1127-.LFB1127
 3372      00000000 
 3373 1adc 01       		.uleb128 0x1
 3374 1add 9C       		.byte	0x9c
 3375 1ade FF1A0000 		.long	0x1aff
 3376 1ae2 40       		.uleb128 0x40
 3377 1ae3 00000000 		.long	.LASF326
 3378 1ae7 01       		.byte	0x1
 3379 1ae8 1C       		.byte	0x1c
 3380 1ae9 FD0C0000 		.long	0xcfd
 3381 1aed 02       		.uleb128 0x2
 3382 1aee 91       		.byte	0x91
 3383 1aef 6C       		.sleb128 -20
 3384 1af0 40       		.uleb128 0x40
 3385 1af1 00000000 		.long	.LASF327
 3386 1af5 01       		.byte	0x1
 3387 1af6 1C       		.byte	0x1c
 3388 1af7 FD0C0000 		.long	0xcfd
 3389 1afb 02       		.uleb128 0x2
 3390 1afc 91       		.byte	0x91
 3391 1afd 68       		.sleb128 -24
 3392 1afe 00       		.byte	0
 3393 1aff 41       		.uleb128 0x41
 3394 1b00 00000000 		.long	.LASF351
 3395 1b04 00000000 		.quad	.LFB1128
 3395      00000000 
 3396 1b0c 15000000 		.quad	.LFE1128-.LFB1128
 3396      00000000 
 3397 1b14 01       		.uleb128 0x1
 3398 1b15 9C       		.byte	0x9c
 3399 1b16 42       		.uleb128 0x42
 3400 1b17 00000000 		.long	.LASF328
 3401 1b1b 820C0000 		.long	0xc82
 3402 1b1f 43       		.uleb128 0x43
 3403 1b20 0C090000 		.long	0x90c
 3404 1b24 09       		.uleb128 0x9
 3405 1b25 03       		.byte	0x3
 3406 1b26 00000000 		.quad	_ZStL8__ioinit
 3406      00000000 
 3407 1b2e 44       		.uleb128 0x44
 3408 1b2f 69090000 		.long	0x969
 3409 1b33 00000000 		.long	.LASF329
 3410 1b37 80808080 		.sleb128 -2147483648
 3410      78
 3411 1b3c 45       		.uleb128 0x45
 3412 1b3d 74090000 		.long	0x974
 3413 1b41 00000000 		.long	.LASF330
 3414 1b45 FFFFFF7F 		.long	0x7fffffff
 3415 1b49 46       		.uleb128 0x46
 3416 1b4a CC090000 		.long	0x9cc
 3417 1b4e 00000000 		.long	.LASF331
 3418 1b52 40       		.byte	0x40
GAS LISTING /tmp/cc45yn9z.s 			page 64


 3419 1b53 46       		.uleb128 0x46
 3420 1b54 F8090000 		.long	0x9f8
 3421 1b58 00000000 		.long	.LASF332
 3422 1b5c 7F       		.byte	0x7f
 3423 1b5d 44       		.uleb128 0x44
 3424 1b5e 2F0A0000 		.long	0xa2f
 3425 1b62 00000000 		.long	.LASF333
 3426 1b66 80807E   		.sleb128 -32768
 3427 1b69 47       		.uleb128 0x47
 3428 1b6a 3A0A0000 		.long	0xa3a
 3429 1b6e 00000000 		.long	.LASF334
 3430 1b72 FF7F     		.value	0x7fff
 3431 1b74 44       		.uleb128 0x44
 3432 1b75 6D0A0000 		.long	0xa6d
 3433 1b79 00000000 		.long	.LASF335
 3434 1b7d 80808080 		.sleb128 -9223372036854775808
 3434      80808080 
 3434      807F
 3435 1b87 48       		.uleb128 0x48
 3436 1b88 780A0000 		.long	0xa78
 3437 1b8c 00000000 		.long	.LASF336
 3438 1b90 FFFFFFFF 		.quad	0x7fffffffffffffff
 3438      FFFFFF7F 
 3439 1b98 00       		.byte	0
 3440              		.section	.debug_abbrev,"",@progbits
 3441              	.Ldebug_abbrev0:
 3442 0000 01       		.uleb128 0x1
 3443 0001 11       		.uleb128 0x11
 3444 0002 01       		.byte	0x1
 3445 0003 25       		.uleb128 0x25
 3446 0004 0E       		.uleb128 0xe
 3447 0005 13       		.uleb128 0x13
 3448 0006 0B       		.uleb128 0xb
 3449 0007 03       		.uleb128 0x3
 3450 0008 0E       		.uleb128 0xe
 3451 0009 1B       		.uleb128 0x1b
 3452 000a 0E       		.uleb128 0xe
 3453 000b 11       		.uleb128 0x11
 3454 000c 01       		.uleb128 0x1
 3455 000d 12       		.uleb128 0x12
 3456 000e 07       		.uleb128 0x7
 3457 000f 10       		.uleb128 0x10
 3458 0010 17       		.uleb128 0x17
 3459 0011 00       		.byte	0
 3460 0012 00       		.byte	0
 3461 0013 02       		.uleb128 0x2
 3462 0014 39       		.uleb128 0x39
 3463 0015 01       		.byte	0x1
 3464 0016 03       		.uleb128 0x3
 3465 0017 08       		.uleb128 0x8
 3466 0018 3A       		.uleb128 0x3a
 3467 0019 0B       		.uleb128 0xb
 3468 001a 3B       		.uleb128 0x3b
 3469 001b 0B       		.uleb128 0xb
 3470 001c 01       		.uleb128 0x1
 3471 001d 13       		.uleb128 0x13
 3472 001e 00       		.byte	0
GAS LISTING /tmp/cc45yn9z.s 			page 65


 3473 001f 00       		.byte	0
 3474 0020 03       		.uleb128 0x3
 3475 0021 39       		.uleb128 0x39
 3476 0022 00       		.byte	0
 3477 0023 03       		.uleb128 0x3
 3478 0024 0E       		.uleb128 0xe
 3479 0025 3A       		.uleb128 0x3a
 3480 0026 0B       		.uleb128 0xb
 3481 0027 3B       		.uleb128 0x3b
 3482 0028 0B       		.uleb128 0xb
 3483 0029 00       		.byte	0
 3484 002a 00       		.byte	0
 3485 002b 04       		.uleb128 0x4
 3486 002c 3A       		.uleb128 0x3a
 3487 002d 00       		.byte	0
 3488 002e 3A       		.uleb128 0x3a
 3489 002f 0B       		.uleb128 0xb
 3490 0030 3B       		.uleb128 0x3b
 3491 0031 0B       		.uleb128 0xb
 3492 0032 18       		.uleb128 0x18
 3493 0033 13       		.uleb128 0x13
 3494 0034 00       		.byte	0
 3495 0035 00       		.byte	0
 3496 0036 05       		.uleb128 0x5
 3497 0037 08       		.uleb128 0x8
 3498 0038 00       		.byte	0
 3499 0039 3A       		.uleb128 0x3a
 3500 003a 0B       		.uleb128 0xb
 3501 003b 3B       		.uleb128 0x3b
 3502 003c 0B       		.uleb128 0xb
 3503 003d 18       		.uleb128 0x18
 3504 003e 13       		.uleb128 0x13
 3505 003f 00       		.byte	0
 3506 0040 00       		.byte	0
 3507 0041 06       		.uleb128 0x6
 3508 0042 08       		.uleb128 0x8
 3509 0043 00       		.byte	0
 3510 0044 3A       		.uleb128 0x3a
 3511 0045 0B       		.uleb128 0xb
 3512 0046 3B       		.uleb128 0x3b
 3513 0047 05       		.uleb128 0x5
 3514 0048 18       		.uleb128 0x18
 3515 0049 13       		.uleb128 0x13
 3516 004a 00       		.byte	0
 3517 004b 00       		.byte	0
 3518 004c 07       		.uleb128 0x7
 3519 004d 13       		.uleb128 0x13
 3520 004e 01       		.byte	0x1
 3521 004f 03       		.uleb128 0x3
 3522 0050 0E       		.uleb128 0xe
 3523 0051 0B       		.uleb128 0xb
 3524 0052 0B       		.uleb128 0xb
 3525 0053 3A       		.uleb128 0x3a
 3526 0054 0B       		.uleb128 0xb
 3527 0055 3B       		.uleb128 0x3b
 3528 0056 0B       		.uleb128 0xb
 3529 0057 01       		.uleb128 0x1
GAS LISTING /tmp/cc45yn9z.s 			page 66


 3530 0058 13       		.uleb128 0x13
 3531 0059 00       		.byte	0
 3532 005a 00       		.byte	0
 3533 005b 08       		.uleb128 0x8
 3534 005c 16       		.uleb128 0x16
 3535 005d 00       		.byte	0
 3536 005e 03       		.uleb128 0x3
 3537 005f 0E       		.uleb128 0xe
 3538 0060 3A       		.uleb128 0x3a
 3539 0061 0B       		.uleb128 0xb
 3540 0062 3B       		.uleb128 0x3b
 3541 0063 0B       		.uleb128 0xb
 3542 0064 49       		.uleb128 0x49
 3543 0065 13       		.uleb128 0x13
 3544 0066 00       		.byte	0
 3545 0067 00       		.byte	0
 3546 0068 09       		.uleb128 0x9
 3547 0069 2E       		.uleb128 0x2e
 3548 006a 01       		.byte	0x1
 3549 006b 3F       		.uleb128 0x3f
 3550 006c 19       		.uleb128 0x19
 3551 006d 03       		.uleb128 0x3
 3552 006e 0E       		.uleb128 0xe
 3553 006f 3A       		.uleb128 0x3a
 3554 0070 0B       		.uleb128 0xb
 3555 0071 3B       		.uleb128 0x3b
 3556 0072 0B       		.uleb128 0xb
 3557 0073 6E       		.uleb128 0x6e
 3558 0074 0E       		.uleb128 0xe
 3559 0075 3C       		.uleb128 0x3c
 3560 0076 19       		.uleb128 0x19
 3561 0077 01       		.uleb128 0x1
 3562 0078 13       		.uleb128 0x13
 3563 0079 00       		.byte	0
 3564 007a 00       		.byte	0
 3565 007b 0A       		.uleb128 0xa
 3566 007c 05       		.uleb128 0x5
 3567 007d 00       		.byte	0
 3568 007e 49       		.uleb128 0x49
 3569 007f 13       		.uleb128 0x13
 3570 0080 00       		.byte	0
 3571 0081 00       		.byte	0
 3572 0082 0B       		.uleb128 0xb
 3573 0083 26       		.uleb128 0x26
 3574 0084 00       		.byte	0
 3575 0085 49       		.uleb128 0x49
 3576 0086 13       		.uleb128 0x13
 3577 0087 00       		.byte	0
 3578 0088 00       		.byte	0
 3579 0089 0C       		.uleb128 0xc
 3580 008a 2E       		.uleb128 0x2e
 3581 008b 01       		.byte	0x1
 3582 008c 3F       		.uleb128 0x3f
 3583 008d 19       		.uleb128 0x19
 3584 008e 03       		.uleb128 0x3
 3585 008f 08       		.uleb128 0x8
 3586 0090 3A       		.uleb128 0x3a
GAS LISTING /tmp/cc45yn9z.s 			page 67


 3587 0091 0B       		.uleb128 0xb
 3588 0092 3B       		.uleb128 0x3b
 3589 0093 0B       		.uleb128 0xb
 3590 0094 6E       		.uleb128 0x6e
 3591 0095 0E       		.uleb128 0xe
 3592 0096 49       		.uleb128 0x49
 3593 0097 13       		.uleb128 0x13
 3594 0098 3C       		.uleb128 0x3c
 3595 0099 19       		.uleb128 0x19
 3596 009a 01       		.uleb128 0x1
 3597 009b 13       		.uleb128 0x13
 3598 009c 00       		.byte	0
 3599 009d 00       		.byte	0
 3600 009e 0D       		.uleb128 0xd
 3601 009f 2E       		.uleb128 0x2e
 3602 00a0 01       		.byte	0x1
 3603 00a1 3F       		.uleb128 0x3f
 3604 00a2 19       		.uleb128 0x19
 3605 00a3 03       		.uleb128 0x3
 3606 00a4 0E       		.uleb128 0xe
 3607 00a5 3A       		.uleb128 0x3a
 3608 00a6 0B       		.uleb128 0xb
 3609 00a7 3B       		.uleb128 0x3b
 3610 00a8 05       		.uleb128 0x5
 3611 00a9 6E       		.uleb128 0x6e
 3612 00aa 0E       		.uleb128 0xe
 3613 00ab 49       		.uleb128 0x49
 3614 00ac 13       		.uleb128 0x13
 3615 00ad 3C       		.uleb128 0x3c
 3616 00ae 19       		.uleb128 0x19
 3617 00af 01       		.uleb128 0x1
 3618 00b0 13       		.uleb128 0x13
 3619 00b1 00       		.byte	0
 3620 00b2 00       		.byte	0
 3621 00b3 0E       		.uleb128 0xe
 3622 00b4 2E       		.uleb128 0x2e
 3623 00b5 00       		.byte	0
 3624 00b6 3F       		.uleb128 0x3f
 3625 00b7 19       		.uleb128 0x19
 3626 00b8 03       		.uleb128 0x3
 3627 00b9 08       		.uleb128 0x8
 3628 00ba 3A       		.uleb128 0x3a
 3629 00bb 0B       		.uleb128 0xb
 3630 00bc 3B       		.uleb128 0x3b
 3631 00bd 05       		.uleb128 0x5
 3632 00be 6E       		.uleb128 0x6e
 3633 00bf 0E       		.uleb128 0xe
 3634 00c0 49       		.uleb128 0x49
 3635 00c1 13       		.uleb128 0x13
 3636 00c2 3C       		.uleb128 0x3c
 3637 00c3 19       		.uleb128 0x19
 3638 00c4 00       		.byte	0
 3639 00c5 00       		.byte	0
 3640 00c6 0F       		.uleb128 0xf
 3641 00c7 2E       		.uleb128 0x2e
 3642 00c8 01       		.byte	0x1
 3643 00c9 3F       		.uleb128 0x3f
GAS LISTING /tmp/cc45yn9z.s 			page 68


 3644 00ca 19       		.uleb128 0x19
 3645 00cb 03       		.uleb128 0x3
 3646 00cc 0E       		.uleb128 0xe
 3647 00cd 3A       		.uleb128 0x3a
 3648 00ce 0B       		.uleb128 0xb
 3649 00cf 3B       		.uleb128 0x3b
 3650 00d0 05       		.uleb128 0x5
 3651 00d1 6E       		.uleb128 0x6e
 3652 00d2 0E       		.uleb128 0xe
 3653 00d3 49       		.uleb128 0x49
 3654 00d4 13       		.uleb128 0x13
 3655 00d5 3C       		.uleb128 0x3c
 3656 00d6 19       		.uleb128 0x19
 3657 00d7 00       		.byte	0
 3658 00d8 00       		.byte	0
 3659 00d9 10       		.uleb128 0x10
 3660 00da 04       		.uleb128 0x4
 3661 00db 01       		.byte	0x1
 3662 00dc 03       		.uleb128 0x3
 3663 00dd 0E       		.uleb128 0xe
 3664 00de 0B       		.uleb128 0xb
 3665 00df 0B       		.uleb128 0xb
 3666 00e0 49       		.uleb128 0x49
 3667 00e1 13       		.uleb128 0x13
 3668 00e2 3A       		.uleb128 0x3a
 3669 00e3 0B       		.uleb128 0xb
 3670 00e4 3B       		.uleb128 0x3b
 3671 00e5 0B       		.uleb128 0xb
 3672 00e6 01       		.uleb128 0x1
 3673 00e7 13       		.uleb128 0x13
 3674 00e8 00       		.byte	0
 3675 00e9 00       		.byte	0
 3676 00ea 11       		.uleb128 0x11
 3677 00eb 28       		.uleb128 0x28
 3678 00ec 00       		.byte	0
 3679 00ed 03       		.uleb128 0x3
 3680 00ee 0E       		.uleb128 0xe
 3681 00ef 1C       		.uleb128 0x1c
 3682 00f0 0B       		.uleb128 0xb
 3683 00f1 00       		.byte	0
 3684 00f2 00       		.byte	0
 3685 00f3 12       		.uleb128 0x12
 3686 00f4 28       		.uleb128 0x28
 3687 00f5 00       		.byte	0
 3688 00f6 03       		.uleb128 0x3
 3689 00f7 0E       		.uleb128 0xe
 3690 00f8 1C       		.uleb128 0x1c
 3691 00f9 05       		.uleb128 0x5
 3692 00fa 00       		.byte	0
 3693 00fb 00       		.byte	0
 3694 00fc 13       		.uleb128 0x13
 3695 00fd 28       		.uleb128 0x28
 3696 00fe 00       		.byte	0
 3697 00ff 03       		.uleb128 0x3
 3698 0100 0E       		.uleb128 0xe
 3699 0101 1C       		.uleb128 0x1c
 3700 0102 06       		.uleb128 0x6
GAS LISTING /tmp/cc45yn9z.s 			page 69


 3701 0103 00       		.byte	0
 3702 0104 00       		.byte	0
 3703 0105 14       		.uleb128 0x14
 3704 0106 28       		.uleb128 0x28
 3705 0107 00       		.byte	0
 3706 0108 03       		.uleb128 0x3
 3707 0109 0E       		.uleb128 0xe
 3708 010a 1C       		.uleb128 0x1c
 3709 010b 0D       		.uleb128 0xd
 3710 010c 00       		.byte	0
 3711 010d 00       		.byte	0
 3712 010e 15       		.uleb128 0x15
 3713 010f 02       		.uleb128 0x2
 3714 0110 01       		.byte	0x1
 3715 0111 03       		.uleb128 0x3
 3716 0112 0E       		.uleb128 0xe
 3717 0113 3C       		.uleb128 0x3c
 3718 0114 19       		.uleb128 0x19
 3719 0115 01       		.uleb128 0x1
 3720 0116 13       		.uleb128 0x13
 3721 0117 00       		.byte	0
 3722 0118 00       		.byte	0
 3723 0119 16       		.uleb128 0x16
 3724 011a 02       		.uleb128 0x2
 3725 011b 01       		.byte	0x1
 3726 011c 03       		.uleb128 0x3
 3727 011d 0E       		.uleb128 0xe
 3728 011e 0B       		.uleb128 0xb
 3729 011f 0B       		.uleb128 0xb
 3730 0120 3A       		.uleb128 0x3a
 3731 0121 0B       		.uleb128 0xb
 3732 0122 3B       		.uleb128 0x3b
 3733 0123 05       		.uleb128 0x5
 3734 0124 32       		.uleb128 0x32
 3735 0125 0B       		.uleb128 0xb
 3736 0126 01       		.uleb128 0x1
 3737 0127 13       		.uleb128 0x13
 3738 0128 00       		.byte	0
 3739 0129 00       		.byte	0
 3740 012a 17       		.uleb128 0x17
 3741 012b 0D       		.uleb128 0xd
 3742 012c 00       		.byte	0
 3743 012d 03       		.uleb128 0x3
 3744 012e 0E       		.uleb128 0xe
 3745 012f 3A       		.uleb128 0x3a
 3746 0130 0B       		.uleb128 0xb
 3747 0131 3B       		.uleb128 0x3b
 3748 0132 05       		.uleb128 0x5
 3749 0133 49       		.uleb128 0x49
 3750 0134 13       		.uleb128 0x13
 3751 0135 3F       		.uleb128 0x3f
 3752 0136 19       		.uleb128 0x19
 3753 0137 3C       		.uleb128 0x3c
 3754 0138 19       		.uleb128 0x19
 3755 0139 00       		.byte	0
 3756 013a 00       		.byte	0
 3757 013b 18       		.uleb128 0x18
GAS LISTING /tmp/cc45yn9z.s 			page 70


 3758 013c 2E       		.uleb128 0x2e
 3759 013d 01       		.byte	0x1
 3760 013e 3F       		.uleb128 0x3f
 3761 013f 19       		.uleb128 0x19
 3762 0140 03       		.uleb128 0x3
 3763 0141 0E       		.uleb128 0xe
 3764 0142 3A       		.uleb128 0x3a
 3765 0143 0B       		.uleb128 0xb
 3766 0144 3B       		.uleb128 0x3b
 3767 0145 05       		.uleb128 0x5
 3768 0146 6E       		.uleb128 0x6e
 3769 0147 0E       		.uleb128 0xe
 3770 0148 32       		.uleb128 0x32
 3771 0149 0B       		.uleb128 0xb
 3772 014a 3C       		.uleb128 0x3c
 3773 014b 19       		.uleb128 0x19
 3774 014c 64       		.uleb128 0x64
 3775 014d 13       		.uleb128 0x13
 3776 014e 01       		.uleb128 0x1
 3777 014f 13       		.uleb128 0x13
 3778 0150 00       		.byte	0
 3779 0151 00       		.byte	0
 3780 0152 19       		.uleb128 0x19
 3781 0153 05       		.uleb128 0x5
 3782 0154 00       		.byte	0
 3783 0155 49       		.uleb128 0x49
 3784 0156 13       		.uleb128 0x13
 3785 0157 34       		.uleb128 0x34
 3786 0158 19       		.uleb128 0x19
 3787 0159 00       		.byte	0
 3788 015a 00       		.byte	0
 3789 015b 1A       		.uleb128 0x1a
 3790 015c 2E       		.uleb128 0x2e
 3791 015d 01       		.byte	0x1
 3792 015e 3F       		.uleb128 0x3f
 3793 015f 19       		.uleb128 0x19
 3794 0160 03       		.uleb128 0x3
 3795 0161 0E       		.uleb128 0xe
 3796 0162 3A       		.uleb128 0x3a
 3797 0163 0B       		.uleb128 0xb
 3798 0164 3B       		.uleb128 0x3b
 3799 0165 05       		.uleb128 0x5
 3800 0166 6E       		.uleb128 0x6e
 3801 0167 0E       		.uleb128 0xe
 3802 0168 32       		.uleb128 0x32
 3803 0169 0B       		.uleb128 0xb
 3804 016a 3C       		.uleb128 0x3c
 3805 016b 19       		.uleb128 0x19
 3806 016c 64       		.uleb128 0x64
 3807 016d 13       		.uleb128 0x13
 3808 016e 00       		.byte	0
 3809 016f 00       		.byte	0
 3810 0170 1B       		.uleb128 0x1b
 3811 0171 16       		.uleb128 0x16
 3812 0172 00       		.byte	0
 3813 0173 03       		.uleb128 0x3
 3814 0174 0E       		.uleb128 0xe
GAS LISTING /tmp/cc45yn9z.s 			page 71


 3815 0175 3A       		.uleb128 0x3a
 3816 0176 0B       		.uleb128 0xb
 3817 0177 3B       		.uleb128 0x3b
 3818 0178 05       		.uleb128 0x5
 3819 0179 49       		.uleb128 0x49
 3820 017a 13       		.uleb128 0x13
 3821 017b 32       		.uleb128 0x32
 3822 017c 0B       		.uleb128 0xb
 3823 017d 00       		.byte	0
 3824 017e 00       		.byte	0
 3825 017f 1C       		.uleb128 0x1c
 3826 0180 0D       		.uleb128 0xd
 3827 0181 00       		.byte	0
 3828 0182 03       		.uleb128 0x3
 3829 0183 0E       		.uleb128 0xe
 3830 0184 3A       		.uleb128 0x3a
 3831 0185 0B       		.uleb128 0xb
 3832 0186 3B       		.uleb128 0x3b
 3833 0187 05       		.uleb128 0x5
 3834 0188 49       		.uleb128 0x49
 3835 0189 13       		.uleb128 0x13
 3836 018a 3F       		.uleb128 0x3f
 3837 018b 19       		.uleb128 0x19
 3838 018c 32       		.uleb128 0x32
 3839 018d 0B       		.uleb128 0xb
 3840 018e 3C       		.uleb128 0x3c
 3841 018f 19       		.uleb128 0x19
 3842 0190 1C       		.uleb128 0x1c
 3843 0191 0B       		.uleb128 0xb
 3844 0192 00       		.byte	0
 3845 0193 00       		.byte	0
 3846 0194 1D       		.uleb128 0x1d
 3847 0195 0D       		.uleb128 0xd
 3848 0196 00       		.byte	0
 3849 0197 03       		.uleb128 0x3
 3850 0198 08       		.uleb128 0x8
 3851 0199 3A       		.uleb128 0x3a
 3852 019a 0B       		.uleb128 0xb
 3853 019b 3B       		.uleb128 0x3b
 3854 019c 05       		.uleb128 0x5
 3855 019d 49       		.uleb128 0x49
 3856 019e 13       		.uleb128 0x13
 3857 019f 3F       		.uleb128 0x3f
 3858 01a0 19       		.uleb128 0x19
 3859 01a1 32       		.uleb128 0x32
 3860 01a2 0B       		.uleb128 0xb
 3861 01a3 3C       		.uleb128 0x3c
 3862 01a4 19       		.uleb128 0x19
 3863 01a5 1C       		.uleb128 0x1c
 3864 01a6 0B       		.uleb128 0xb
 3865 01a7 00       		.byte	0
 3866 01a8 00       		.byte	0
 3867 01a9 1E       		.uleb128 0x1e
 3868 01aa 0D       		.uleb128 0xd
 3869 01ab 00       		.byte	0
 3870 01ac 03       		.uleb128 0x3
 3871 01ad 0E       		.uleb128 0xe
GAS LISTING /tmp/cc45yn9z.s 			page 72


 3872 01ae 3A       		.uleb128 0x3a
 3873 01af 0B       		.uleb128 0xb
 3874 01b0 3B       		.uleb128 0x3b
 3875 01b1 05       		.uleb128 0x5
 3876 01b2 49       		.uleb128 0x49
 3877 01b3 13       		.uleb128 0x13
 3878 01b4 3F       		.uleb128 0x3f
 3879 01b5 19       		.uleb128 0x19
 3880 01b6 32       		.uleb128 0x32
 3881 01b7 0B       		.uleb128 0xb
 3882 01b8 3C       		.uleb128 0x3c
 3883 01b9 19       		.uleb128 0x19
 3884 01ba 1C       		.uleb128 0x1c
 3885 01bb 05       		.uleb128 0x5
 3886 01bc 00       		.byte	0
 3887 01bd 00       		.byte	0
 3888 01be 1F       		.uleb128 0x1f
 3889 01bf 2F       		.uleb128 0x2f
 3890 01c0 00       		.byte	0
 3891 01c1 03       		.uleb128 0x3
 3892 01c2 0E       		.uleb128 0xe
 3893 01c3 49       		.uleb128 0x49
 3894 01c4 13       		.uleb128 0x13
 3895 01c5 00       		.byte	0
 3896 01c6 00       		.byte	0
 3897 01c7 20       		.uleb128 0x20
 3898 01c8 2F       		.uleb128 0x2f
 3899 01c9 00       		.byte	0
 3900 01ca 03       		.uleb128 0x3
 3901 01cb 0E       		.uleb128 0xe
 3902 01cc 49       		.uleb128 0x49
 3903 01cd 13       		.uleb128 0x13
 3904 01ce 1E       		.uleb128 0x1e
 3905 01cf 19       		.uleb128 0x19
 3906 01d0 00       		.byte	0
 3907 01d1 00       		.byte	0
 3908 01d2 21       		.uleb128 0x21
 3909 01d3 34       		.uleb128 0x34
 3910 01d4 00       		.byte	0
 3911 01d5 03       		.uleb128 0x3
 3912 01d6 0E       		.uleb128 0xe
 3913 01d7 3A       		.uleb128 0x3a
 3914 01d8 0B       		.uleb128 0xb
 3915 01d9 3B       		.uleb128 0x3b
 3916 01da 0B       		.uleb128 0xb
 3917 01db 6E       		.uleb128 0x6e
 3918 01dc 0E       		.uleb128 0xe
 3919 01dd 49       		.uleb128 0x49
 3920 01de 13       		.uleb128 0x13
 3921 01df 3F       		.uleb128 0x3f
 3922 01e0 19       		.uleb128 0x19
 3923 01e1 3C       		.uleb128 0x3c
 3924 01e2 19       		.uleb128 0x19
 3925 01e3 00       		.byte	0
 3926 01e4 00       		.byte	0
 3927 01e5 22       		.uleb128 0x22
 3928 01e6 34       		.uleb128 0x34
GAS LISTING /tmp/cc45yn9z.s 			page 73


 3929 01e7 00       		.byte	0
 3930 01e8 03       		.uleb128 0x3
 3931 01e9 0E       		.uleb128 0xe
 3932 01ea 3A       		.uleb128 0x3a
 3933 01eb 0B       		.uleb128 0xb
 3934 01ec 3B       		.uleb128 0x3b
 3935 01ed 0B       		.uleb128 0xb
 3936 01ee 49       		.uleb128 0x49
 3937 01ef 13       		.uleb128 0x13
 3938 01f0 3C       		.uleb128 0x3c
 3939 01f1 19       		.uleb128 0x19
 3940 01f2 00       		.byte	0
 3941 01f3 00       		.byte	0
 3942 01f4 23       		.uleb128 0x23
 3943 01f5 39       		.uleb128 0x39
 3944 01f6 01       		.byte	0x1
 3945 01f7 03       		.uleb128 0x3
 3946 01f8 0E       		.uleb128 0xe
 3947 01f9 3A       		.uleb128 0x3a
 3948 01fa 0B       		.uleb128 0xb
 3949 01fb 3B       		.uleb128 0x3b
 3950 01fc 0B       		.uleb128 0xb
 3951 01fd 01       		.uleb128 0x1
 3952 01fe 13       		.uleb128 0x13
 3953 01ff 00       		.byte	0
 3954 0200 00       		.byte	0
 3955 0201 24       		.uleb128 0x24
 3956 0202 0D       		.uleb128 0xd
 3957 0203 00       		.byte	0
 3958 0204 03       		.uleb128 0x3
 3959 0205 0E       		.uleb128 0xe
 3960 0206 3A       		.uleb128 0x3a
 3961 0207 0B       		.uleb128 0xb
 3962 0208 3B       		.uleb128 0x3b
 3963 0209 0B       		.uleb128 0xb
 3964 020a 49       		.uleb128 0x49
 3965 020b 13       		.uleb128 0x13
 3966 020c 3F       		.uleb128 0x3f
 3967 020d 19       		.uleb128 0x19
 3968 020e 3C       		.uleb128 0x3c
 3969 020f 19       		.uleb128 0x19
 3970 0210 00       		.byte	0
 3971 0211 00       		.byte	0
 3972 0212 25       		.uleb128 0x25
 3973 0213 13       		.uleb128 0x13
 3974 0214 01       		.byte	0x1
 3975 0215 03       		.uleb128 0x3
 3976 0216 0E       		.uleb128 0xe
 3977 0217 0B       		.uleb128 0xb
 3978 0218 0B       		.uleb128 0xb
 3979 0219 3A       		.uleb128 0x3a
 3980 021a 0B       		.uleb128 0xb
 3981 021b 3B       		.uleb128 0x3b
 3982 021c 0B       		.uleb128 0xb
 3983 021d 00       		.byte	0
 3984 021e 00       		.byte	0
 3985 021f 26       		.uleb128 0x26
GAS LISTING /tmp/cc45yn9z.s 			page 74


 3986 0220 0D       		.uleb128 0xd
 3987 0221 00       		.byte	0
 3988 0222 03       		.uleb128 0x3
 3989 0223 0E       		.uleb128 0xe
 3990 0224 3A       		.uleb128 0x3a
 3991 0225 0B       		.uleb128 0xb
 3992 0226 3B       		.uleb128 0x3b
 3993 0227 0B       		.uleb128 0xb
 3994 0228 49       		.uleb128 0x49
 3995 0229 13       		.uleb128 0x13
 3996 022a 38       		.uleb128 0x38
 3997 022b 0B       		.uleb128 0xb
 3998 022c 00       		.byte	0
 3999 022d 00       		.byte	0
 4000 022e 27       		.uleb128 0x27
 4001 022f 0D       		.uleb128 0xd
 4002 0230 00       		.byte	0
 4003 0231 03       		.uleb128 0x3
 4004 0232 0E       		.uleb128 0xe
 4005 0233 3A       		.uleb128 0x3a
 4006 0234 0B       		.uleb128 0xb
 4007 0235 3B       		.uleb128 0x3b
 4008 0236 05       		.uleb128 0x5
 4009 0237 49       		.uleb128 0x49
 4010 0238 13       		.uleb128 0x13
 4011 0239 38       		.uleb128 0x38
 4012 023a 0B       		.uleb128 0xb
 4013 023b 00       		.byte	0
 4014 023c 00       		.byte	0
 4015 023d 28       		.uleb128 0x28
 4016 023e 24       		.uleb128 0x24
 4017 023f 00       		.byte	0
 4018 0240 0B       		.uleb128 0xb
 4019 0241 0B       		.uleb128 0xb
 4020 0242 3E       		.uleb128 0x3e
 4021 0243 0B       		.uleb128 0xb
 4022 0244 03       		.uleb128 0x3
 4023 0245 0E       		.uleb128 0xe
 4024 0246 00       		.byte	0
 4025 0247 00       		.byte	0
 4026 0248 29       		.uleb128 0x29
 4027 0249 0F       		.uleb128 0xf
 4028 024a 00       		.byte	0
 4029 024b 0B       		.uleb128 0xb
 4030 024c 0B       		.uleb128 0xb
 4031 024d 00       		.byte	0
 4032 024e 00       		.byte	0
 4033 024f 2A       		.uleb128 0x2a
 4034 0250 16       		.uleb128 0x16
 4035 0251 00       		.byte	0
 4036 0252 03       		.uleb128 0x3
 4037 0253 0E       		.uleb128 0xe
 4038 0254 3A       		.uleb128 0x3a
 4039 0255 0B       		.uleb128 0xb
 4040 0256 3B       		.uleb128 0x3b
 4041 0257 05       		.uleb128 0x5
 4042 0258 49       		.uleb128 0x49
GAS LISTING /tmp/cc45yn9z.s 			page 75


 4043 0259 13       		.uleb128 0x13
 4044 025a 00       		.byte	0
 4045 025b 00       		.byte	0
 4046 025c 2B       		.uleb128 0x2b
 4047 025d 13       		.uleb128 0x13
 4048 025e 01       		.byte	0x1
 4049 025f 0B       		.uleb128 0xb
 4050 0260 0B       		.uleb128 0xb
 4051 0261 3A       		.uleb128 0x3a
 4052 0262 0B       		.uleb128 0xb
 4053 0263 3B       		.uleb128 0x3b
 4054 0264 0B       		.uleb128 0xb
 4055 0265 6E       		.uleb128 0x6e
 4056 0266 0E       		.uleb128 0xe
 4057 0267 01       		.uleb128 0x1
 4058 0268 13       		.uleb128 0x13
 4059 0269 00       		.byte	0
 4060 026a 00       		.byte	0
 4061 026b 2C       		.uleb128 0x2c
 4062 026c 17       		.uleb128 0x17
 4063 026d 01       		.byte	0x1
 4064 026e 0B       		.uleb128 0xb
 4065 026f 0B       		.uleb128 0xb
 4066 0270 3A       		.uleb128 0x3a
 4067 0271 0B       		.uleb128 0xb
 4068 0272 3B       		.uleb128 0x3b
 4069 0273 0B       		.uleb128 0xb
 4070 0274 01       		.uleb128 0x1
 4071 0275 13       		.uleb128 0x13
 4072 0276 00       		.byte	0
 4073 0277 00       		.byte	0
 4074 0278 2D       		.uleb128 0x2d
 4075 0279 0D       		.uleb128 0xd
 4076 027a 00       		.byte	0
 4077 027b 03       		.uleb128 0x3
 4078 027c 0E       		.uleb128 0xe
 4079 027d 3A       		.uleb128 0x3a
 4080 027e 0B       		.uleb128 0xb
 4081 027f 3B       		.uleb128 0x3b
 4082 0280 0B       		.uleb128 0xb
 4083 0281 49       		.uleb128 0x49
 4084 0282 13       		.uleb128 0x13
 4085 0283 00       		.byte	0
 4086 0284 00       		.byte	0
 4087 0285 2E       		.uleb128 0x2e
 4088 0286 01       		.uleb128 0x1
 4089 0287 01       		.byte	0x1
 4090 0288 49       		.uleb128 0x49
 4091 0289 13       		.uleb128 0x13
 4092 028a 01       		.uleb128 0x1
 4093 028b 13       		.uleb128 0x13
 4094 028c 00       		.byte	0
 4095 028d 00       		.byte	0
 4096 028e 2F       		.uleb128 0x2f
 4097 028f 21       		.uleb128 0x21
 4098 0290 00       		.byte	0
 4099 0291 49       		.uleb128 0x49
GAS LISTING /tmp/cc45yn9z.s 			page 76


 4100 0292 13       		.uleb128 0x13
 4101 0293 2F       		.uleb128 0x2f
 4102 0294 0B       		.uleb128 0xb
 4103 0295 00       		.byte	0
 4104 0296 00       		.byte	0
 4105 0297 30       		.uleb128 0x30
 4106 0298 24       		.uleb128 0x24
 4107 0299 00       		.byte	0
 4108 029a 0B       		.uleb128 0xb
 4109 029b 0B       		.uleb128 0xb
 4110 029c 3E       		.uleb128 0x3e
 4111 029d 0B       		.uleb128 0xb
 4112 029e 03       		.uleb128 0x3
 4113 029f 08       		.uleb128 0x8
 4114 02a0 00       		.byte	0
 4115 02a1 00       		.byte	0
 4116 02a2 31       		.uleb128 0x31
 4117 02a3 0F       		.uleb128 0xf
 4118 02a4 00       		.byte	0
 4119 02a5 0B       		.uleb128 0xb
 4120 02a6 0B       		.uleb128 0xb
 4121 02a7 49       		.uleb128 0x49
 4122 02a8 13       		.uleb128 0x13
 4123 02a9 00       		.byte	0
 4124 02aa 00       		.byte	0
 4125 02ab 32       		.uleb128 0x32
 4126 02ac 2E       		.uleb128 0x2e
 4127 02ad 01       		.byte	0x1
 4128 02ae 3F       		.uleb128 0x3f
 4129 02af 19       		.uleb128 0x19
 4130 02b0 03       		.uleb128 0x3
 4131 02b1 0E       		.uleb128 0xe
 4132 02b2 3A       		.uleb128 0x3a
 4133 02b3 0B       		.uleb128 0xb
 4134 02b4 3B       		.uleb128 0x3b
 4135 02b5 05       		.uleb128 0x5
 4136 02b6 49       		.uleb128 0x49
 4137 02b7 13       		.uleb128 0x13
 4138 02b8 3C       		.uleb128 0x3c
 4139 02b9 19       		.uleb128 0x19
 4140 02ba 01       		.uleb128 0x1
 4141 02bb 13       		.uleb128 0x13
 4142 02bc 00       		.byte	0
 4143 02bd 00       		.byte	0
 4144 02be 33       		.uleb128 0x33
 4145 02bf 18       		.uleb128 0x18
 4146 02c0 00       		.byte	0
 4147 02c1 00       		.byte	0
 4148 02c2 00       		.byte	0
 4149 02c3 34       		.uleb128 0x34
 4150 02c4 2E       		.uleb128 0x2e
 4151 02c5 00       		.byte	0
 4152 02c6 3F       		.uleb128 0x3f
 4153 02c7 19       		.uleb128 0x19
 4154 02c8 03       		.uleb128 0x3
 4155 02c9 0E       		.uleb128 0xe
 4156 02ca 3A       		.uleb128 0x3a
GAS LISTING /tmp/cc45yn9z.s 			page 77


 4157 02cb 0B       		.uleb128 0xb
 4158 02cc 3B       		.uleb128 0x3b
 4159 02cd 05       		.uleb128 0x5
 4160 02ce 49       		.uleb128 0x49
 4161 02cf 13       		.uleb128 0x13
 4162 02d0 3C       		.uleb128 0x3c
 4163 02d1 19       		.uleb128 0x19
 4164 02d2 00       		.byte	0
 4165 02d3 00       		.byte	0
 4166 02d4 35       		.uleb128 0x35
 4167 02d5 2E       		.uleb128 0x2e
 4168 02d6 01       		.byte	0x1
 4169 02d7 3F       		.uleb128 0x3f
 4170 02d8 19       		.uleb128 0x19
 4171 02d9 03       		.uleb128 0x3
 4172 02da 0E       		.uleb128 0xe
 4173 02db 3A       		.uleb128 0x3a
 4174 02dc 0B       		.uleb128 0xb
 4175 02dd 3B       		.uleb128 0x3b
 4176 02de 0B       		.uleb128 0xb
 4177 02df 49       		.uleb128 0x49
 4178 02e0 13       		.uleb128 0x13
 4179 02e1 3C       		.uleb128 0x3c
 4180 02e2 19       		.uleb128 0x19
 4181 02e3 01       		.uleb128 0x1
 4182 02e4 13       		.uleb128 0x13
 4183 02e5 00       		.byte	0
 4184 02e6 00       		.byte	0
 4185 02e7 36       		.uleb128 0x36
 4186 02e8 13       		.uleb128 0x13
 4187 02e9 01       		.byte	0x1
 4188 02ea 03       		.uleb128 0x3
 4189 02eb 08       		.uleb128 0x8
 4190 02ec 0B       		.uleb128 0xb
 4191 02ed 0B       		.uleb128 0xb
 4192 02ee 3A       		.uleb128 0x3a
 4193 02ef 0B       		.uleb128 0xb
 4194 02f0 3B       		.uleb128 0x3b
 4195 02f1 0B       		.uleb128 0xb
 4196 02f2 01       		.uleb128 0x1
 4197 02f3 13       		.uleb128 0x13
 4198 02f4 00       		.byte	0
 4199 02f5 00       		.byte	0
 4200 02f6 37       		.uleb128 0x37
 4201 02f7 2E       		.uleb128 0x2e
 4202 02f8 01       		.byte	0x1
 4203 02f9 3F       		.uleb128 0x3f
 4204 02fa 19       		.uleb128 0x19
 4205 02fb 03       		.uleb128 0x3
 4206 02fc 0E       		.uleb128 0xe
 4207 02fd 3A       		.uleb128 0x3a
 4208 02fe 0B       		.uleb128 0xb
 4209 02ff 3B       		.uleb128 0x3b
 4210 0300 0B       		.uleb128 0xb
 4211 0301 6E       		.uleb128 0x6e
 4212 0302 0E       		.uleb128 0xe
 4213 0303 49       		.uleb128 0x49
GAS LISTING /tmp/cc45yn9z.s 			page 78


 4214 0304 13       		.uleb128 0x13
 4215 0305 3C       		.uleb128 0x3c
 4216 0306 19       		.uleb128 0x19
 4217 0307 01       		.uleb128 0x1
 4218 0308 13       		.uleb128 0x13
 4219 0309 00       		.byte	0
 4220 030a 00       		.byte	0
 4221 030b 38       		.uleb128 0x38
 4222 030c 10       		.uleb128 0x10
 4223 030d 00       		.byte	0
 4224 030e 0B       		.uleb128 0xb
 4225 030f 0B       		.uleb128 0xb
 4226 0310 49       		.uleb128 0x49
 4227 0311 13       		.uleb128 0x13
 4228 0312 00       		.byte	0
 4229 0313 00       		.byte	0
 4230 0314 39       		.uleb128 0x39
 4231 0315 2E       		.uleb128 0x2e
 4232 0316 00       		.byte	0
 4233 0317 3F       		.uleb128 0x3f
 4234 0318 19       		.uleb128 0x19
 4235 0319 03       		.uleb128 0x3
 4236 031a 0E       		.uleb128 0xe
 4237 031b 3A       		.uleb128 0x3a
 4238 031c 0B       		.uleb128 0xb
 4239 031d 3B       		.uleb128 0x3b
 4240 031e 0B       		.uleb128 0xb
 4241 031f 49       		.uleb128 0x49
 4242 0320 13       		.uleb128 0x13
 4243 0321 3C       		.uleb128 0x3c
 4244 0322 19       		.uleb128 0x19
 4245 0323 00       		.byte	0
 4246 0324 00       		.byte	0
 4247 0325 3A       		.uleb128 0x3a
 4248 0326 16       		.uleb128 0x16
 4249 0327 00       		.byte	0
 4250 0328 03       		.uleb128 0x3
 4251 0329 0E       		.uleb128 0xe
 4252 032a 3A       		.uleb128 0x3a
 4253 032b 0B       		.uleb128 0xb
 4254 032c 3B       		.uleb128 0x3b
 4255 032d 0B       		.uleb128 0xb
 4256 032e 00       		.byte	0
 4257 032f 00       		.byte	0
 4258 0330 3B       		.uleb128 0x3b
 4259 0331 2E       		.uleb128 0x2e
 4260 0332 01       		.byte	0x1
 4261 0333 3F       		.uleb128 0x3f
 4262 0334 19       		.uleb128 0x19
 4263 0335 03       		.uleb128 0x3
 4264 0336 0E       		.uleb128 0xe
 4265 0337 3A       		.uleb128 0x3a
 4266 0338 0B       		.uleb128 0xb
 4267 0339 3B       		.uleb128 0x3b
 4268 033a 05       		.uleb128 0x5
 4269 033b 3C       		.uleb128 0x3c
 4270 033c 19       		.uleb128 0x19
GAS LISTING /tmp/cc45yn9z.s 			page 79


 4271 033d 01       		.uleb128 0x1
 4272 033e 13       		.uleb128 0x13
 4273 033f 00       		.byte	0
 4274 0340 00       		.byte	0
 4275 0341 3C       		.uleb128 0x3c
 4276 0342 2E       		.uleb128 0x2e
 4277 0343 01       		.byte	0x1
 4278 0344 3F       		.uleb128 0x3f
 4279 0345 19       		.uleb128 0x19
 4280 0346 03       		.uleb128 0x3
 4281 0347 0E       		.uleb128 0xe
 4282 0348 3A       		.uleb128 0x3a
 4283 0349 0B       		.uleb128 0xb
 4284 034a 3B       		.uleb128 0x3b
 4285 034b 0B       		.uleb128 0xb
 4286 034c 49       		.uleb128 0x49
 4287 034d 13       		.uleb128 0x13
 4288 034e 11       		.uleb128 0x11
 4289 034f 01       		.uleb128 0x1
 4290 0350 12       		.uleb128 0x12
 4291 0351 07       		.uleb128 0x7
 4292 0352 40       		.uleb128 0x40
 4293 0353 18       		.uleb128 0x18
 4294 0354 9642     		.uleb128 0x2116
 4295 0356 19       		.uleb128 0x19
 4296 0357 01       		.uleb128 0x1
 4297 0358 13       		.uleb128 0x13
 4298 0359 00       		.byte	0
 4299 035a 00       		.byte	0
 4300 035b 3D       		.uleb128 0x3d
 4301 035c 34       		.uleb128 0x34
 4302 035d 00       		.byte	0
 4303 035e 03       		.uleb128 0x3
 4304 035f 0E       		.uleb128 0xe
 4305 0360 3A       		.uleb128 0x3a
 4306 0361 0B       		.uleb128 0xb
 4307 0362 3B       		.uleb128 0x3b
 4308 0363 0B       		.uleb128 0xb
 4309 0364 49       		.uleb128 0x49
 4310 0365 13       		.uleb128 0x13
 4311 0366 02       		.uleb128 0x2
 4312 0367 18       		.uleb128 0x18
 4313 0368 00       		.byte	0
 4314 0369 00       		.byte	0
 4315 036a 3E       		.uleb128 0x3e
 4316 036b 34       		.uleb128 0x34
 4317 036c 00       		.byte	0
 4318 036d 03       		.uleb128 0x3
 4319 036e 08       		.uleb128 0x8
 4320 036f 3A       		.uleb128 0x3a
 4321 0370 0B       		.uleb128 0xb
 4322 0371 3B       		.uleb128 0x3b
 4323 0372 0B       		.uleb128 0xb
 4324 0373 49       		.uleb128 0x49
 4325 0374 13       		.uleb128 0x13
 4326 0375 02       		.uleb128 0x2
 4327 0376 18       		.uleb128 0x18
GAS LISTING /tmp/cc45yn9z.s 			page 80


 4328 0377 00       		.byte	0
 4329 0378 00       		.byte	0
 4330 0379 3F       		.uleb128 0x3f
 4331 037a 2E       		.uleb128 0x2e
 4332 037b 01       		.byte	0x1
 4333 037c 03       		.uleb128 0x3
 4334 037d 0E       		.uleb128 0xe
 4335 037e 34       		.uleb128 0x34
 4336 037f 19       		.uleb128 0x19
 4337 0380 11       		.uleb128 0x11
 4338 0381 01       		.uleb128 0x1
 4339 0382 12       		.uleb128 0x12
 4340 0383 07       		.uleb128 0x7
 4341 0384 40       		.uleb128 0x40
 4342 0385 18       		.uleb128 0x18
 4343 0386 9642     		.uleb128 0x2116
 4344 0388 19       		.uleb128 0x19
 4345 0389 01       		.uleb128 0x1
 4346 038a 13       		.uleb128 0x13
 4347 038b 00       		.byte	0
 4348 038c 00       		.byte	0
 4349 038d 40       		.uleb128 0x40
 4350 038e 05       		.uleb128 0x5
 4351 038f 00       		.byte	0
 4352 0390 03       		.uleb128 0x3
 4353 0391 0E       		.uleb128 0xe
 4354 0392 3A       		.uleb128 0x3a
 4355 0393 0B       		.uleb128 0xb
 4356 0394 3B       		.uleb128 0x3b
 4357 0395 0B       		.uleb128 0xb
 4358 0396 49       		.uleb128 0x49
 4359 0397 13       		.uleb128 0x13
 4360 0398 02       		.uleb128 0x2
 4361 0399 18       		.uleb128 0x18
 4362 039a 00       		.byte	0
 4363 039b 00       		.byte	0
 4364 039c 41       		.uleb128 0x41
 4365 039d 2E       		.uleb128 0x2e
 4366 039e 00       		.byte	0
 4367 039f 03       		.uleb128 0x3
 4368 03a0 0E       		.uleb128 0xe
 4369 03a1 34       		.uleb128 0x34
 4370 03a2 19       		.uleb128 0x19
 4371 03a3 11       		.uleb128 0x11
 4372 03a4 01       		.uleb128 0x1
 4373 03a5 12       		.uleb128 0x12
 4374 03a6 07       		.uleb128 0x7
 4375 03a7 40       		.uleb128 0x40
 4376 03a8 18       		.uleb128 0x18
 4377 03a9 9642     		.uleb128 0x2116
 4378 03ab 19       		.uleb128 0x19
 4379 03ac 00       		.byte	0
 4380 03ad 00       		.byte	0
 4381 03ae 42       		.uleb128 0x42
 4382 03af 34       		.uleb128 0x34
 4383 03b0 00       		.byte	0
 4384 03b1 03       		.uleb128 0x3
GAS LISTING /tmp/cc45yn9z.s 			page 81


 4385 03b2 0E       		.uleb128 0xe
 4386 03b3 49       		.uleb128 0x49
 4387 03b4 13       		.uleb128 0x13
 4388 03b5 3F       		.uleb128 0x3f
 4389 03b6 19       		.uleb128 0x19
 4390 03b7 34       		.uleb128 0x34
 4391 03b8 19       		.uleb128 0x19
 4392 03b9 3C       		.uleb128 0x3c
 4393 03ba 19       		.uleb128 0x19
 4394 03bb 00       		.byte	0
 4395 03bc 00       		.byte	0
 4396 03bd 43       		.uleb128 0x43
 4397 03be 34       		.uleb128 0x34
 4398 03bf 00       		.byte	0
 4399 03c0 47       		.uleb128 0x47
 4400 03c1 13       		.uleb128 0x13
 4401 03c2 02       		.uleb128 0x2
 4402 03c3 18       		.uleb128 0x18
 4403 03c4 00       		.byte	0
 4404 03c5 00       		.byte	0
 4405 03c6 44       		.uleb128 0x44
 4406 03c7 34       		.uleb128 0x34
 4407 03c8 00       		.byte	0
 4408 03c9 47       		.uleb128 0x47
 4409 03ca 13       		.uleb128 0x13
 4410 03cb 6E       		.uleb128 0x6e
 4411 03cc 0E       		.uleb128 0xe
 4412 03cd 1C       		.uleb128 0x1c
 4413 03ce 0D       		.uleb128 0xd
 4414 03cf 00       		.byte	0
 4415 03d0 00       		.byte	0
 4416 03d1 45       		.uleb128 0x45
 4417 03d2 34       		.uleb128 0x34
 4418 03d3 00       		.byte	0
 4419 03d4 47       		.uleb128 0x47
 4420 03d5 13       		.uleb128 0x13
 4421 03d6 6E       		.uleb128 0x6e
 4422 03d7 0E       		.uleb128 0xe
 4423 03d8 1C       		.uleb128 0x1c
 4424 03d9 06       		.uleb128 0x6
 4425 03da 00       		.byte	0
 4426 03db 00       		.byte	0
 4427 03dc 46       		.uleb128 0x46
 4428 03dd 34       		.uleb128 0x34
 4429 03de 00       		.byte	0
 4430 03df 47       		.uleb128 0x47
 4431 03e0 13       		.uleb128 0x13
 4432 03e1 6E       		.uleb128 0x6e
 4433 03e2 0E       		.uleb128 0xe
 4434 03e3 1C       		.uleb128 0x1c
 4435 03e4 0B       		.uleb128 0xb
 4436 03e5 00       		.byte	0
 4437 03e6 00       		.byte	0
 4438 03e7 47       		.uleb128 0x47
 4439 03e8 34       		.uleb128 0x34
 4440 03e9 00       		.byte	0
 4441 03ea 47       		.uleb128 0x47
GAS LISTING /tmp/cc45yn9z.s 			page 82


 4442 03eb 13       		.uleb128 0x13
 4443 03ec 6E       		.uleb128 0x6e
 4444 03ed 0E       		.uleb128 0xe
 4445 03ee 1C       		.uleb128 0x1c
 4446 03ef 05       		.uleb128 0x5
 4447 03f0 00       		.byte	0
 4448 03f1 00       		.byte	0
 4449 03f2 48       		.uleb128 0x48
 4450 03f3 34       		.uleb128 0x34
 4451 03f4 00       		.byte	0
 4452 03f5 47       		.uleb128 0x47
 4453 03f6 13       		.uleb128 0x13
 4454 03f7 6E       		.uleb128 0x6e
 4455 03f8 0E       		.uleb128 0xe
 4456 03f9 1C       		.uleb128 0x1c
 4457 03fa 07       		.uleb128 0x7
 4458 03fb 00       		.byte	0
 4459 03fc 00       		.byte	0
 4460 03fd 00       		.byte	0
 4461              		.section	.debug_aranges,"",@progbits
 4462 0000 2C000000 		.long	0x2c
 4463 0004 0200     		.value	0x2
 4464 0006 00000000 		.long	.Ldebug_info0
 4465 000a 08       		.byte	0x8
 4466 000b 00       		.byte	0
 4467 000c 0000     		.value	0
 4468 000e 0000     		.value	0
 4469 0010 00000000 		.quad	.Ltext0
 4469      00000000 
 4470 0018 7F010000 		.quad	.Letext0-.Ltext0
 4470      00000000 
 4471 0020 00000000 		.quad	0
 4471      00000000 
 4472 0028 00000000 		.quad	0
 4472      00000000 
 4473              		.section	.debug_line,"",@progbits
 4474              	.Ldebug_line0:
 4475 0000 A2020000 		.section	.debug_str,"MS",@progbits,1
 4475      02004402 
 4475      00000101 
 4475      FB0E0D00 
 4475      01010101 
 4476              	.LASF351:
 4477 0000 5F474C4F 		.string	"_GLOBAL__sub_I_main"
 4477      42414C5F 
 4477      5F737562 
 4477      5F495F6D 
 4477      61696E00 
 4478              	.LASF302:
 4479 0014 66676574 		.string	"fgetc"
 4479      6300
 4480              	.LASF70:
 4481 001a 5F535F65 		.string	"_S_end"
 4481      6E6400
 4482              	.LASF25:
 4483 0021 73697A65 		.string	"size_t"
 4483      5F7400
GAS LISTING /tmp/cc45yn9z.s 			page 83


 4484              	.LASF157:
 4485 0028 73697A65 		.string	"sizetype"
 4485      74797065 
 4485      00
 4486              	.LASF203:
 4487 0031 746D5F68 		.string	"tm_hour"
 4487      6F757200 
 4488              	.LASF114:
 4489 0039 5F5F6973 		.string	"__is_signed"
 4489      5F736967 
 4489      6E656400 
 4490              	.LASF58:
 4491 0045 5F535F69 		.string	"_S_ios_openmode_min"
 4491      6F735F6F 
 4491      70656E6D 
 4491      6F64655F 
 4491      6D696E00 
 4492              	.LASF111:
 4493 0059 5F5F6E75 		.string	"__numeric_traits_integer<int>"
 4493      6D657269 
 4493      635F7472 
 4493      61697473 
 4493      5F696E74 
 4494              	.LASF297:
 4495 0077 66706F73 		.string	"fpos_t"
 4495      5F7400
 4496              	.LASF330:
 4497 007e 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIiE5__maxE"
 4497      5F5F676E 
 4497      755F6378 
 4497      7832345F 
 4497      5F6E756D 
 4498              	.LASF79:
 4499 00b0 626F6F6C 		.string	"boolalpha"
 4499      616C7068 
 4499      6100
 4500              	.LASF133:
 4501 00ba 5F494F5F 		.string	"_IO_save_end"
 4501      73617665 
 4501      5F656E64 
 4501      00
 4502              	.LASF84:
 4503 00c7 73636965 		.string	"scientific"
 4503      6E746966 
 4503      696300
 4504              	.LASF113:
 4505 00d2 5F5F6D61 		.string	"__max"
 4505      7800
 4506              	.LASF199:
 4507 00d8 77637363 		.string	"wcscspn"
 4507      73706E00 
 4508              	.LASF277:
 4509 00e0 6C6F6361 		.string	"localeconv"
 4509      6C65636F 
 4509      6E7600
 4510              	.LASF289:
 4511 00eb 395F475F 		.string	"9_G_fpos_t"
GAS LISTING /tmp/cc45yn9z.s 			page 84


 4511      66706F73 
 4511      5F7400
 4512              	.LASF47:
 4513 00f6 5F535F69 		.string	"_S_ios_fmtflags_min"
 4513      6F735F66 
 4513      6D74666C 
 4513      6167735F 
 4513      6D696E00 
 4514              	.LASF262:
 4515 010a 66726163 		.string	"frac_digits"
 4515      5F646967 
 4515      69747300 
 4516              	.LASF126:
 4517 0116 5F494F5F 		.string	"_IO_write_base"
 4517      77726974 
 4517      655F6261 
 4517      736500
 4518              	.LASF65:
 4519 0125 5F535F69 		.string	"_S_ios_iostate_max"
 4519      6F735F69 
 4519      6F737461 
 4519      74655F6D 
 4519      617800
 4520              	.LASF142:
 4521 0138 5F6C6F63 		.string	"_lock"
 4521      6B00
 4522              	.LASF254:
 4523 013e 696E745F 		.string	"int_curr_symbol"
 4523      63757272 
 4523      5F73796D 
 4523      626F6C00 
 4524              	.LASF98:
 4525 014e 676F6F64 		.string	"goodbit"
 4525      62697400 
 4526              	.LASF234:
 4527 0156 77637363 		.string	"wcschr"
 4527      687200
 4528              	.LASF27:
 4529 015d 5F535F62 		.string	"_S_boolalpha"
 4529      6F6F6C61 
 4529      6C706861 
 4529      00
 4530              	.LASF61:
 4531 016a 5F535F62 		.string	"_S_badbit"
 4531      61646269 
 4531      7400
 4532              	.LASF97:
 4533 0174 6661696C 		.string	"failbit"
 4533      62697400 
 4534              	.LASF124:
 4535 017c 5F494F5F 		.string	"_IO_read_end"
 4535      72656164 
 4535      5F656E64 
 4535      00
 4536              	.LASF131:
 4537 0189 5F494F5F 		.string	"_IO_save_base"
 4537      73617665 
GAS LISTING /tmp/cc45yn9z.s 			page 85


 4537      5F626173 
 4537      6500
 4538              	.LASF180:
 4539 0197 6D627274 		.string	"mbrtowc"
 4539      6F776300 
 4540              	.LASF226:
 4541 019f 77637378 		.string	"wcsxfrm"
 4541      66726D00 
 4542              	.LASF261:
 4543 01a7 696E745F 		.string	"int_frac_digits"
 4543      66726163 
 4543      5F646967 
 4543      69747300 
 4544              	.LASF303:
 4545 01b7 66676574 		.string	"fgetpos"
 4545      706F7300 
 4546              	.LASF290:
 4547 01bf 5F5F706F 		.string	"__pos"
 4547      7300
 4548              	.LASF68:
 4549 01c5 5F535F62 		.string	"_S_beg"
 4549      656700
 4550              	.LASF197:
 4551 01cc 77637363 		.string	"wcscoll"
 4551      6F6C6C00 
 4552              	.LASF314:
 4553 01d4 636C6561 		.string	"clearerr"
 4553      72657272 
 4553      00
 4554              	.LASF139:
 4555 01dd 5F637572 		.string	"_cur_column"
 4555      5F636F6C 
 4555      756D6E00 
 4556              	.LASF88:
 4557 01e9 736B6970 		.string	"skipws"
 4557      777300
 4558              	.LASF161:
 4559 01f0 5F5F7763 		.string	"__wch"
 4559      6800
 4560              	.LASF13:
 4561 01f6 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4moveEPcPKcm"
 4561      74313163 
 4561      6861725F 
 4561      74726169 
 4561      74734963 
 4562              	.LASF90:
 4563 0218 75707065 		.string	"uppercase"
 4563      72636173 
 4563      6500
 4564              	.LASF43:
 4565 0222 5F535F62 		.string	"_S_basefield"
 4565      61736566 
 4565      69656C64 
 4565      00
 4566              	.LASF21:
 4567 022f 5F5A4E53 		.string	"_ZNSt11char_traitsIcE11to_int_typeERKc"
 4567      74313163 
GAS LISTING /tmp/cc45yn9z.s 			page 86


 4567      6861725F 
 4567      74726169 
 4567      74734963 
 4568              	.LASF256:
 4569 0256 6D6F6E5F 		.string	"mon_decimal_point"
 4569      64656369 
 4569      6D616C5F 
 4569      706F696E 
 4569      7400
 4570              	.LASF120:
 4571 0268 46494C45 		.string	"FILE"
 4571      00
 4572              	.LASF224:
 4573 026d 6C6F6E67 		.string	"long int"
 4573      20696E74 
 4573      00
 4574              	.LASF209:
 4575 0276 746D5F69 		.string	"tm_isdst"
 4575      73647374 
 4575      00
 4576              	.LASF315:
 4577 027f 70657272 		.string	"perror"
 4577      6F7200
 4578              	.LASF118:
 4579 0286 5F5F6E75 		.string	"__numeric_traits_integer<char>"
 4579      6D657269 
 4579      635F7472 
 4579      61697473 
 4579      5F696E74 
 4580              	.LASF192:
 4581 02a5 76777072 		.string	"vwprintf"
 4581      696E7466 
 4581      00
 4582              	.LASF49:
 4583 02ae 5F496F73 		.string	"_Ios_Openmode"
 4583      5F4F7065 
 4583      6E6D6F64 
 4583      6500
 4584              	.LASF3:
 4585 02bc 696E745F 		.string	"int_type"
 4585      74797065 
 4585      00
 4586              	.LASF293:
 4587 02c5 5F494F5F 		.string	"_IO_marker"
 4587      6D61726B 
 4587      657200
 4588              	.LASF349:
 4589 02d0 6D61696E 		.string	"main"
 4589      00
 4590              	.LASF271:
 4591 02d5 696E745F 		.string	"int_n_cs_precedes"
 4591      6E5F6373 
 4591      5F707265 
 4591      63656465 
 4591      7300
 4592              	.LASF285:
 4593 02e7 746F7763 		.string	"towctrans"
GAS LISTING /tmp/cc45yn9z.s 			page 87


 4593      7472616E 
 4593      7300
 4594              	.LASF14:
 4595 02f1 636F7079 		.string	"copy"
 4595      00
 4596              	.LASF5:
 4597 02f6 5F5A4E53 		.string	"_ZNSt11char_traitsIcE2ltERKcS2_"
 4597      74313163 
 4597      6861725F 
 4597      74726169 
 4597      74734963 
 4598              	.LASF324:
 4599 0316 5F5F696F 		.string	"__ioinit"
 4599      696E6974 
 4599      00
 4600              	.LASF73:
 4601 031f 5F535F73 		.string	"_S_synced_with_stdio"
 4601      796E6365 
 4601      645F7769 
 4601      74685F73 
 4601      7464696F 
 4602              	.LASF116:
 4603 0334 5F56616C 		.string	"_Value"
 4603      756500
 4604              	.LASF62:
 4605 033b 5F535F65 		.string	"_S_eofbit"
 4605      6F666269 
 4605      7400
 4606              	.LASF208:
 4607 0345 746D5F79 		.string	"tm_yday"
 4607      64617900 
 4608              	.LASF246:
 4609 034d 7369676E 		.string	"signed char"
 4609      65642063 
 4609      68617200 
 4610              	.LASF121:
 4611 0359 5F494F5F 		.string	"_IO_FILE"
 4611      46494C45 
 4611      00
 4612              	.LASF316:
 4613 0362 72656D6F 		.string	"remove"
 4613      766500
 4614              	.LASF102:
 4615 0369 62617369 		.string	"basic_ostream<char, std::char_traits<char> >"
 4615      635F6F73 
 4615      74726561 
 4615      6D3C6368 
 4615      61722C20 
 4616              	.LASF164:
 4617 0396 5F5F7661 		.string	"__value"
 4617      6C756500 
 4618              	.LASF282:
 4619 039e 77637479 		.string	"wctype_t"
 4619      70655F74 
 4619      00
 4620              	.LASF170:
 4621 03a7 66676574 		.string	"fgetwc"
GAS LISTING /tmp/cc45yn9z.s 			page 88


 4621      776300
 4622              	.LASF276:
 4623 03ae 67657477 		.string	"getwchar"
 4623      63686172 
 4623      00
 4624              	.LASF292:
 4625 03b7 5F475F66 		.string	"_G_fpos_t"
 4625      706F735F 
 4625      7400
 4626              	.LASF171:
 4627 03c1 66676574 		.string	"fgetws"
 4627      777300
 4628              	.LASF34:
 4629 03c8 5F535F72 		.string	"_S_right"
 4629      69676874 
 4629      00
 4630              	.LASF2:
 4631 03d1 63686172 		.string	"char_type"
 4631      5F747970 
 4631      6500
 4632              	.LASF245:
 4633 03db 756E7369 		.string	"unsigned char"
 4633      676E6564 
 4633      20636861 
 4633      7200
 4634              	.LASF325:
 4635 03e9 696E6669 		.string	"infile"
 4635      6C6500
 4636              	.LASF266:
 4637 03f0 6E5F7365 		.string	"n_sep_by_space"
 4637      705F6279 
 4637      5F737061 
 4637      636500
 4638              	.LASF298:
 4639 03ff 66636C6F 		.string	"fclose"
 4639      736500
 4640              	.LASF238:
 4641 0406 776D656D 		.string	"wmemchr"
 4641      63687200 
 4642              	.LASF60:
 4643 040e 5F535F67 		.string	"_S_goodbit"
 4643      6F6F6462 
 4643      697400
 4644              	.LASF334:
 4645 0419 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIsE5__maxE"
 4645      5F5F676E 
 4645      755F6378 
 4645      7832345F 
 4645      5F6E756D 
 4646              	.LASF52:
 4647 044b 5F535F62 		.string	"_S_bin"
 4647      696E00
 4648              	.LASF196:
 4649 0452 77637363 		.string	"wcscmp"
 4649      6D7000
 4650              	.LASF24:
 4651 0459 6E6F745F 		.string	"not_eof"
GAS LISTING /tmp/cc45yn9z.s 			page 89


 4651      656F6600 
 4652              	.LASF185:
 4653 0461 73777072 		.string	"swprintf"
 4653      696E7466 
 4653      00
 4654              	.LASF235:
 4655 046a 77637370 		.string	"wcspbrk"
 4655      62726B00 
 4656              	.LASF104:
 4657 0472 62617369 		.string	"basic_ifstream<char, std::char_traits<char> >"
 4657      635F6966 
 4657      73747265 
 4657      616D3C63 
 4657      6861722C 
 4658              	.LASF54:
 4659 04a0 5F535F6F 		.string	"_S_out"
 4659      757400
 4660              	.LASF165:
 4661 04a7 63686172 		.string	"char"
 4661      00
 4662              	.LASF50:
 4663 04ac 5F535F61 		.string	"_S_app"
 4663      707000
 4664              	.LASF167:
 4665 04b3 6D627374 		.string	"mbstate_t"
 4665      6174655F 
 4665      7400
 4666              	.LASF287:
 4667 04bd 77637479 		.string	"wctype"
 4667      706500
 4668              	.LASF76:
 4669 04c4 6F70656E 		.string	"openmode"
 4669      6D6F6465 
 4669      00
 4670              	.LASF214:
 4671 04cd 7763736E 		.string	"wcsncmp"
 4671      636D7000 
 4672              	.LASF348:
 4673 04d5 5F494F5F 		.string	"_IO_lock_t"
 4673      6C6F636B 
 4673      5F7400
 4674              	.LASF274:
 4675 04e0 696E745F 		.string	"int_n_sign_posn"
 4675      6E5F7369 
 4675      676E5F70 
 4675      6F736E00 
 4676              	.LASF268:
 4677 04f0 6E5F7369 		.string	"n_sign_posn"
 4677      676E5F70 
 4677      6F736E00 
 4678              	.LASF75:
 4679 04fc 5F5A4E53 		.string	"_ZNSt8ios_base4InitD4Ev"
 4679      7438696F 
 4679      735F6261 
 4679      73653449 
 4679      6E697444 
 4680              	.LASF230:
GAS LISTING /tmp/cc45yn9z.s 			page 90


 4681 0514 776D656D 		.string	"wmemmove"
 4681      6D6F7665 
 4681      00
 4682              	.LASF311:
 4683 051d 67657463 		.string	"getc"
 4683      00
 4684              	.LASF231:
 4685 0522 776D656D 		.string	"wmemset"
 4685      73657400 
 4686              	.LASF112:
 4687 052a 5F5F6D69 		.string	"__min"
 4687      6E00
 4688              	.LASF169:
 4689 0530 62746F77 		.string	"btowc"
 4689      6300
 4690              	.LASF313:
 4691 0536 67657473 		.string	"gets"
 4691      00
 4692              	.LASF123:
 4693 053b 5F494F5F 		.string	"_IO_read_ptr"
 4693      72656164 
 4693      5F707472 
 4693      00
 4694              	.LASF233:
 4695 0548 77736361 		.string	"wscanf"
 4695      6E6600
 4696              	.LASF257:
 4697 054f 6D6F6E5F 		.string	"mon_thousands_sep"
 4697      74686F75 
 4697      73616E64 
 4697      735F7365 
 4697      7000
 4698              	.LASF187:
 4699 0561 756E6765 		.string	"ungetwc"
 4699      74776300 
 4700              	.LASF154:
 4701 0569 66705F6F 		.string	"fp_offset"
 4701      66667365 
 4701      7400
 4702              	.LASF310:
 4703 0573 6674656C 		.string	"ftell"
 4703      6C00
 4704              	.LASF26:
 4705 0579 70747264 		.string	"ptrdiff_t"
 4705      6966665F 
 4705      7400
 4706              	.LASF329:
 4707 0583 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIiE5__minE"
 4707      5F5F676E 
 4707      755F6378 
 4707      7832345F 
 4707      5F6E756D 
 4708              	.LASF283:
 4709 05b5 77637472 		.string	"wctrans_t"
 4709      616E735F 
 4709      7400
 4710              	.LASF179:
GAS LISTING /tmp/cc45yn9z.s 			page 91


 4711 05bf 6D62726C 		.string	"mbrlen"
 4711      656E00
 4712              	.LASF296:
 4713 05c6 5F706F73 		.string	"_pos"
 4713      00
 4714              	.LASF260:
 4715 05cb 6E656761 		.string	"negative_sign"
 4715      74697665 
 4715      5F736967 
 4715      6E00
 4716              	.LASF30:
 4717 05d9 5F535F68 		.string	"_S_hex"
 4717      657800
 4718              	.LASF269:
 4719 05e0 696E745F 		.string	"int_p_cs_precedes"
 4719      705F6373 
 4719      5F707265 
 4719      63656465 
 4719      7300
 4720              	.LASF176:
 4721 05f2 66777072 		.string	"fwprintf"
 4721      696E7466 
 4721      00
 4722              	.LASF66:
 4723 05fb 5F535F69 		.string	"_S_ios_iostate_min"
 4723      6F735F69 
 4723      6F737461 
 4723      74655F6D 
 4723      696E00
 4724              	.LASF345:
 4725 060e 636F7574 		.string	"cout"
 4725      00
 4726              	.LASF134:
 4727 0613 5F6D6172 		.string	"_markers"
 4727      6B657273 
 4727      00
 4728              	.LASF243:
 4729 061c 77637374 		.string	"wcstoull"
 4729      6F756C6C 
 4729      00
 4730              	.LASF338:
 4731 0625 696F2D65 		.string	"io-error-handling.cpp"
 4731      72726F72 
 4731      2D68616E 
 4731      646C696E 
 4731      672E6370 
 4732              	.LASF31:
 4733 063b 5F535F69 		.string	"_S_internal"
 4733      6E746572 
 4733      6E616C00 
 4734              	.LASF6:
 4735 0647 636F6D70 		.string	"compare"
 4735      61726500 
 4736              	.LASF204:
 4737 064f 746D5F6D 		.string	"tm_mday"
 4737      64617900 
 4738              	.LASF92:
GAS LISTING /tmp/cc45yn9z.s 			page 92


 4739 0657 62617365 		.string	"basefield"
 4739      6669656C 
 4739      6400
 4740              	.LASF198:
 4741 0661 77637363 		.string	"wcscpy"
 4741      707900
 4742              	.LASF105:
 4743 0668 5F436861 		.string	"_CharT"
 4743      725400
 4744              	.LASF80:
 4745 066f 66697865 		.string	"fixed"
 4745      6400
 4746              	.LASF190:
 4747 0675 76737770 		.string	"vswprintf"
 4747      72696E74 
 4747      6600
 4748              	.LASF103:
 4749 067f 69667374 		.string	"ifstream"
 4749      7265616D 
 4749      00
 4750              	.LASF77:
 4751 0688 7365656B 		.string	"seekdir"
 4751      64697200 
 4752              	.LASF175:
 4753 0690 66776964 		.string	"fwide"
 4753      6500
 4754              	.LASF82:
 4755 0696 6C656674 		.string	"left"
 4755      00
 4756              	.LASF201:
 4757 069b 746D5F73 		.string	"tm_sec"
 4757      656300
 4758              	.LASF143:
 4759 06a2 5F6F6666 		.string	"_offset"
 4759      73657400 
 4760              	.LASF215:
 4761 06aa 7763736E 		.string	"wcsncpy"
 4761      63707900 
 4762              	.LASF184:
 4763 06b2 70757477 		.string	"putwchar"
 4763      63686172 
 4763      00
 4764              	.LASF228:
 4765 06bb 776D656D 		.string	"wmemcmp"
 4765      636D7000 
 4766              	.LASF51:
 4767 06c3 5F535F61 		.string	"_S_ate"
 4767      746500
 4768              	.LASF15:
 4769 06ca 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4copyEPcPKcm"
 4769      74313163 
 4769      6861725F 
 4769      74726169 
 4769      74734963 
 4770              	.LASF10:
 4771 06ec 66696E64 		.string	"find"
 4771      00
GAS LISTING /tmp/cc45yn9z.s 			page 93


 4772              	.LASF306:
 4773 06f1 66726561 		.string	"fread"
 4773      6400
 4774              	.LASF29:
 4775 06f7 5F535F66 		.string	"_S_fixed"
 4775      69786564 
 4775      00
 4776              	.LASF272:
 4777 0700 696E745F 		.string	"int_n_sep_by_space"
 4777      6E5F7365 
 4777      705F6279 
 4777      5F737061 
 4777      636500
 4778              	.LASF327:
 4779 0713 5F5F7072 		.string	"__priority"
 4779      696F7269 
 4779      747900
 4780              	.LASF12:
 4781 071e 6D6F7665 		.string	"move"
 4781      00
 4782              	.LASF36:
 4783 0723 5F535F73 		.string	"_S_showbase"
 4783      686F7762 
 4783      61736500 
 4784              	.LASF53:
 4785 072f 5F535F69 		.string	"_S_in"
 4785      6E00
 4786              	.LASF137:
 4787 0735 5F666C61 		.string	"_flags2"
 4787      67733200 
 4788              	.LASF248:
 4789 073d 5F5F676E 		.string	"__gnu_debug"
 4789      755F6465 
 4789      62756700 
 4790              	.LASF125:
 4791 0749 5F494F5F 		.string	"_IO_read_base"
 4791      72656164 
 4791      5F626173 
 4791      6500
 4792              	.LASF300:
 4793 0757 66657272 		.string	"ferror"
 4793      6F7200
 4794              	.LASF339:
 4795 075e 2F686F6D 		.string	"/home/vinu/cs4230/par-lang/c++/discover_modern_c++/basics"
 4795      652F7669 
 4795      6E752F63 
 4795      73343233 
 4795      302F7061 
 4796              	.LASF188:
 4797 0798 76667770 		.string	"vfwprintf"
 4797      72696E74 
 4797      6600
 4798              	.LASF150:
 4799 07a2 5F756E75 		.string	"_unused2"
 4799      73656432 
 4799      00
 4800              	.LASF237:
GAS LISTING /tmp/cc45yn9z.s 			page 94


 4801 07ab 77637373 		.string	"wcsstr"
 4801      747200
 4802              	.LASF191:
 4803 07b2 76737773 		.string	"vswscanf"
 4803      63616E66 
 4803      00
 4804              	.LASF264:
 4805 07bb 705F7365 		.string	"p_sep_by_space"
 4805      705F6279 
 4805      5F737061 
 4805      636500
 4806              	.LASF22:
 4807 07ca 65715F69 		.string	"eq_int_type"
 4807      6E745F74 
 4807      79706500 
 4808              	.LASF72:
 4809 07d6 5F535F72 		.string	"_S_refcount"
 4809      6566636F 
 4809      756E7400 
 4810              	.LASF19:
 4811 07e2 5F5A4E53 		.string	"_ZNSt11char_traitsIcE12to_char_typeERKi"
 4811      74313163 
 4811      6861725F 
 4811      74726169 
 4811      74734963 
 4812              	.LASF309:
 4813 080a 66736574 		.string	"fsetpos"
 4813      706F7300 
 4814              	.LASF55:
 4815 0812 5F535F74 		.string	"_S_trunc"
 4815      72756E63 
 4815      00
 4816              	.LASF326:
 4817 081b 5F5F696E 		.string	"__initialize_p"
 4817      69746961 
 4817      6C697A65 
 4817      5F7000
 4818              	.LASF83:
 4819 082a 72696768 		.string	"right"
 4819      7400
 4820              	.LASF38:
 4821 0830 5F535F73 		.string	"_S_showpos"
 4821      686F7770 
 4821      6F7300
 4822              	.LASF166:
 4823 083b 5F5F6D62 		.string	"__mbstate_t"
 4823      73746174 
 4823      655F7400 
 4824              	.LASF229:
 4825 0847 776D656D 		.string	"wmemcpy"
 4825      63707900 
 4826              	.LASF205:
 4827 084f 746D5F6D 		.string	"tm_mon"
 4827      6F6E00
 4828              	.LASF28:
 4829 0856 5F535F64 		.string	"_S_dec"
 4829      656300
GAS LISTING /tmp/cc45yn9z.s 			page 95


 4830              	.LASF48:
 4831 085d 5F496F73 		.string	"_Ios_Fmtflags"
 4831      5F466D74 
 4831      666C6167 
 4831      7300
 4832              	.LASF223:
 4833 086b 77637374 		.string	"wcstol"
 4833      6F6C00
 4834              	.LASF219:
 4835 0872 646F7562 		.string	"double"
 4835      6C6500
 4836              	.LASF11:
 4837 0879 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4findEPKcmRS1_"
 4837      74313163 
 4837      6861725F 
 4837      74726169 
 4837      74734963 
 4838              	.LASF128:
 4839 089d 5F494F5F 		.string	"_IO_write_end"
 4839      77726974 
 4839      655F656E 
 4839      6400
 4840              	.LASF333:
 4841 08ab 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIsE5__minE"
 4841      5F5F676E 
 4841      755F6378 
 4841      7832345F 
 4841      5F6E756D 
 4842              	.LASF227:
 4843 08dd 7763746F 		.string	"wctob"
 4843      6200
 4844              	.LASF37:
 4845 08e3 5F535F73 		.string	"_S_showpoint"
 4845      686F7770 
 4845      6F696E74 
 4845      00
 4846              	.LASF39:
 4847 08f0 5F535F73 		.string	"_S_skipws"
 4847      6B697077 
 4847      7300
 4848              	.LASF153:
 4849 08fa 67705F6F 		.string	"gp_offset"
 4849      66667365 
 4849      7400
 4850              	.LASF42:
 4851 0904 5F535F61 		.string	"_S_adjustfield"
 4851      646A7573 
 4851      74666965 
 4851      6C6400
 4852              	.LASF331:
 4853 0913 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerImE8__digitsE"
 4853      5F5F676E 
 4853      755F6378 
 4853      7832345F 
 4853      5F6E756D 
 4854              	.LASF221:
 4855 0948 666C6F61 		.string	"float"
GAS LISTING /tmp/cc45yn9z.s 			page 96


 4855      7400
 4856              	.LASF81:
 4857 094e 696E7465 		.string	"internal"
 4857      726E616C 
 4857      00
 4858              	.LASF346:
 4859 0957 5F5A5374 		.string	"_ZSt4cout"
 4859      34636F75 
 4859      7400
 4860              	.LASF202:
 4861 0961 746D5F6D 		.string	"tm_min"
 4861      696E00
 4862              	.LASF129:
 4863 0968 5F494F5F 		.string	"_IO_buf_base"
 4863      6275665F 
 4863      62617365 
 4863      00
 4864              	.LASF32:
 4865 0975 5F535F6C 		.string	"_S_left"
 4865      65667400 
 4866              	.LASF158:
 4867 097d 756E7369 		.string	"unsigned int"
 4867      676E6564 
 4867      20696E74 
 4867      00
 4868              	.LASF335:
 4869 098a 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIlE5__minE"
 4869      5F5F676E 
 4869      755F6378 
 4869      7832345F 
 4869      5F6E756D 
 4870              	.LASF110:
 4871 09bc 63686172 		.string	"char_traits<char>"
 4871      5F747261 
 4871      6974733C 
 4871      63686172 
 4871      3E00
 4872              	.LASF259:
 4873 09ce 706F7369 		.string	"positive_sign"
 4873      74697665 
 4873      5F736967 
 4873      6E00
 4874              	.LASF56:
 4875 09dc 5F535F69 		.string	"_S_ios_openmode_end"
 4875      6F735F6F 
 4875      70656E6D 
 4875      6F64655F 
 4875      656E6400 
 4876              	.LASF217:
 4877 09f0 77637373 		.string	"wcsspn"
 4877      706E00
 4878              	.LASF267:
 4879 09f7 705F7369 		.string	"p_sign_posn"
 4879      676E5F70 
 4879      6F736E00 
 4880              	.LASF23:
 4881 0a03 5F5A4E53 		.string	"_ZNSt11char_traitsIcE11eq_int_typeERKiS2_"
GAS LISTING /tmp/cc45yn9z.s 			page 97


 4881      74313163 
 4881      6861725F 
 4881      74726169 
 4881      74734963 
 4882              	.LASF138:
 4883 0a2d 5F6F6C64 		.string	"_old_offset"
 4883      5F6F6666 
 4883      73657400 
 4884              	.LASF144:
 4885 0a39 5F5F7061 		.string	"__pad1"
 4885      643100
 4886              	.LASF145:
 4887 0a40 5F5F7061 		.string	"__pad2"
 4887      643200
 4888              	.LASF146:
 4889 0a47 5F5F7061 		.string	"__pad3"
 4889      643300
 4890              	.LASF147:
 4891 0a4e 5F5F7061 		.string	"__pad4"
 4891      643400
 4892              	.LASF148:
 4893 0a55 5F5F7061 		.string	"__pad5"
 4893      643500
 4894              	.LASF320:
 4895 0a5c 73657476 		.string	"setvbuf"
 4895      62756600 
 4896              	.LASF295:
 4897 0a64 5F736275 		.string	"_sbuf"
 4897      6600
 4898              	.LASF41:
 4899 0a6a 5F535F75 		.string	"_S_uppercase"
 4899      70706572 
 4899      63617365 
 4899      00
 4900              	.LASF281:
 4901 0a77 5F41746F 		.string	"_Atomic_word"
 4901      6D69635F 
 4901      776F7264 
 4901      00
 4902              	.LASF85:
 4903 0a84 73686F77 		.string	"showbase"
 4903      62617365 
 4903      00
 4904              	.LASF155:
 4905 0a8d 6F766572 		.string	"overflow_arg_area"
 4905      666C6F77 
 4905      5F617267 
 4905      5F617265 
 4905      6100
 4906              	.LASF317:
 4907 0a9f 72656E61 		.string	"rename"
 4907      6D6500
 4908              	.LASF122:
 4909 0aa6 5F666C61 		.string	"_flags"
 4909      677300
 4910              	.LASF45:
 4911 0aad 5F535F69 		.string	"_S_ios_fmtflags_end"
GAS LISTING /tmp/cc45yn9z.s 			page 98


 4911      6F735F66 
 4911      6D74666C 
 4911      6167735F 
 4911      656E6400 
 4912              	.LASF343:
 4913 0ac1 496E6974 		.string	"Init"
 4913      00
 4914              	.LASF149:
 4915 0ac6 5F6D6F64 		.string	"_mode"
 4915      6500
 4916              	.LASF107:
 4917 0acc 6F737472 		.string	"ostream"
 4917      65616D00 
 4918              	.LASF251:
 4919 0ad4 64656369 		.string	"decimal_point"
 4919      6D616C5F 
 4919      706F696E 
 4919      7400
 4920              	.LASF312:
 4921 0ae2 67657463 		.string	"getchar"
 4921      68617200 
 4922              	.LASF163:
 4923 0aea 5F5F636F 		.string	"__count"
 4923      756E7400 
 4924              	.LASF108:
 4925 0af2 5F5F676E 		.string	"__gnu_cxx"
 4925      755F6378 
 4925      7800
 4926              	.LASF249:
 4927 0afc 626F6F6C 		.string	"bool"
 4927      00
 4928              	.LASF337:
 4929 0b01 474E5520 		.string	"GNU C++ 5.4.1 20160904 -mtune=generic -march=x86-64 -g -O0 -fstack-protector-strong"
 4929      432B2B20 
 4929      352E342E 
 4929      31203230 
 4929      31363039 
 4930              	.LASF299:
 4931 0b55 66656F66 		.string	"feof"
 4931      00
 4932              	.LASF17:
 4933 0b5a 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6assignEPcmc"
 4933      74313163 
 4933      6861725F 
 4933      74726169 
 4933      74734963 
 4934              	.LASF240:
 4935 0b7c 6C6F6E67 		.string	"long double"
 4935      20646F75 
 4935      626C6500 
 4936              	.LASF183:
 4937 0b88 70757477 		.string	"putwc"
 4937      6300
 4938              	.LASF347:
 4939 0b8e 5F5F6E75 		.string	"__numeric_traits_integer<long int>"
 4939      6D657269 
 4939      635F7472 
GAS LISTING /tmp/cc45yn9z.s 			page 99


 4939      61697473 
 4939      5F696E74 
 4940              	.LASF87:
 4941 0bb1 73686F77 		.string	"showpos"
 4941      706F7300 
 4942              	.LASF265:
 4943 0bb9 6E5F6373 		.string	"n_cs_precedes"
 4943      5F707265 
 4943      63656465 
 4943      7300
 4944              	.LASF44:
 4945 0bc7 5F535F66 		.string	"_S_floatfield"
 4945      6C6F6174 
 4945      6669656C 
 4945      6400
 4946              	.LASF33:
 4947 0bd5 5F535F6F 		.string	"_S_oct"
 4947      637400
 4948              	.LASF162:
 4949 0bdc 5F5F7763 		.string	"__wchb"
 4949      686200
 4950              	.LASF99:
 4951 0be3 62696E61 		.string	"binary"
 4951      727900
 4952              	.LASF350:
 4953 0bea 5F5F7374 		.string	"__static_initialization_and_destruction_0"
 4953      61746963 
 4953      5F696E69 
 4953      7469616C 
 4953      697A6174 
 4954              	.LASF8:
 4955 0c14 5F5A4E53 		.string	"_ZNSt11char_traitsIcE7compareEPKcS2_m"
 4955      74313163 
 4955      6861725F 
 4955      74726169 
 4955      74734963 
 4956              	.LASF244:
 4957 0c3a 6C6F6E67 		.string	"long long unsigned int"
 4957      206C6F6E 
 4957      6720756E 
 4957      7369676E 
 4957      65642069 
 4958              	.LASF156:
 4959 0c51 7265675F 		.string	"reg_save_area"
 4959      73617665 
 4959      5F617265 
 4959      6100
 4960              	.LASF239:
 4961 0c5f 77637374 		.string	"wcstold"
 4961      6F6C6400 
 4962              	.LASF270:
 4963 0c67 696E745F 		.string	"int_p_sep_by_space"
 4963      705F7365 
 4963      705F6279 
 4963      5F737061 
 4963      636500
 4964              	.LASF71:
GAS LISTING /tmp/cc45yn9z.s 			page 100


 4965 0c7a 5F535F69 		.string	"_S_ios_seekdir_end"
 4965      6F735F73 
 4965      65656B64 
 4965      69725F65 
 4965      6E6400
 4966              	.LASF7:
 4967 0c8d 6C656E67 		.string	"length"
 4967      746800
 4968              	.LASF241:
 4969 0c94 77637374 		.string	"wcstoll"
 4969      6F6C6C00 
 4970              	.LASF279:
 4971 0c9c 5F5F6F66 		.string	"__off_t"
 4971      665F7400 
 4972              	.LASF135:
 4973 0ca4 5F636861 		.string	"_chain"
 4973      696E00
 4974              	.LASF59:
 4975 0cab 5F496F73 		.string	"_Ios_Iostate"
 4975      5F496F73 
 4975      74617465 
 4975      00
 4976              	.LASF159:
 4977 0cb8 6C6F6E67 		.string	"long unsigned int"
 4977      20756E73 
 4977      69676E65 
 4977      6420696E 
 4977      7400
 4978              	.LASF332:
 4979 0cca 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIcE5__maxE"
 4979      5F5F676E 
 4979      755F6378 
 4979      7832345F 
 4979      5F6E756D 
 4980              	.LASF319:
 4981 0cfc 73657462 		.string	"setbuf"
 4981      756600
 4982              	.LASF216:
 4983 0d03 77637372 		.string	"wcsrtombs"
 4983      746F6D62 
 4983      7300
 4984              	.LASF91:
 4985 0d0d 61646A75 		.string	"adjustfield"
 4985      73746669 
 4985      656C6400 
 4986              	.LASF207:
 4987 0d19 746D5F77 		.string	"tm_wday"
 4987      64617900 
 4988              	.LASF40:
 4989 0d21 5F535F75 		.string	"_S_unitbuf"
 4989      6E697462 
 4989      756600
 4990              	.LASF4:
 4991 0d2c 5F5A4E53 		.string	"_ZNSt11char_traitsIcE2eqERKcS2_"
 4991      74313163 
 4991      6861725F 
 4991      74726169 
GAS LISTING /tmp/cc45yn9z.s 			page 101


 4991      74734963 
 4992              	.LASF93:
 4993 0d4c 666C6F61 		.string	"floatfield"
 4993      74666965 
 4993      6C6400
 4994              	.LASF186:
 4995 0d57 73777363 		.string	"swscanf"
 4995      616E6600 
 4996              	.LASF115:
 4997 0d5f 5F5F6469 		.string	"__digits"
 4997      67697473 
 4997      00
 4998              	.LASF218:
 4999 0d68 77637374 		.string	"wcstod"
 4999      6F6400
 5000              	.LASF220:
 5001 0d6f 77637374 		.string	"wcstof"
 5001      6F6600
 5002              	.LASF222:
 5003 0d76 77637374 		.string	"wcstok"
 5003      6F6B00
 5004              	.LASF0:
 5005 0d7d 5F5F6378 		.string	"__cxx11"
 5005      78313100 
 5006              	.LASF100:
 5007 0d85 7472756E 		.string	"trunc"
 5007      6300
 5008              	.LASF151:
 5009 0d8b 5F5F4649 		.string	"__FILE"
 5009      4C4500
 5010              	.LASF86:
 5011 0d92 73686F77 		.string	"showpoint"
 5011      706F696E 
 5011      7400
 5012              	.LASF132:
 5013 0d9c 5F494F5F 		.string	"_IO_backup_base"
 5013      6261636B 
 5013      75705F62 
 5013      61736500 
 5014              	.LASF275:
 5015 0dac 7365746C 		.string	"setlocale"
 5015      6F63616C 
 5015      6500
 5016              	.LASF141:
 5017 0db6 5F73686F 		.string	"_shortbuf"
 5017      72746275 
 5017      6600
 5018              	.LASF236:
 5019 0dc0 77637372 		.string	"wcsrchr"
 5019      63687200 
 5020              	.LASF177:
 5021 0dc8 66777363 		.string	"fwscanf"
 5021      616E6600 
 5022              	.LASF160:
 5023 0dd0 77696E74 		.string	"wint_t"
 5023      5F7400
 5024              	.LASF57:
GAS LISTING /tmp/cc45yn9z.s 			page 102


 5025 0dd7 5F535F69 		.string	"_S_ios_openmode_max"
 5025      6F735F6F 
 5025      70656E6D 
 5025      6F64655F 
 5025      6D617800 
 5026              	.LASF294:
 5027 0deb 5F6E6578 		.string	"_next"
 5027      7400
 5028              	.LASF101:
 5029 0df1 696F735F 		.string	"ios_base"
 5029      62617365 
 5029      00
 5030              	.LASF280:
 5031 0dfa 5F5F6F66 		.string	"__off64_t"
 5031      6636345F 
 5031      7400
 5032              	.LASF95:
 5033 0e04 62616462 		.string	"badbit"
 5033      697400
 5034              	.LASF305:
 5035 0e0b 666F7065 		.string	"fopen"
 5035      6E00
 5036              	.LASF286:
 5037 0e11 77637472 		.string	"wctrans"
 5037      616E7300 
 5038              	.LASF252:
 5039 0e19 74686F75 		.string	"thousands_sep"
 5039      73616E64 
 5039      735F7365 
 5039      7000
 5040              	.LASF344:
 5041 0e27 5F5A4E53 		.string	"_ZNSt8ios_base4InitC4Ev"
 5041      7438696F 
 5041      735F6261 
 5041      73653449 
 5041      6E697443 
 5042              	.LASF96:
 5043 0e3f 656F6662 		.string	"eofbit"
 5043      697400
 5044              	.LASF318:
 5045 0e46 72657769 		.string	"rewind"
 5045      6E6400
 5046              	.LASF130:
 5047 0e4d 5F494F5F 		.string	"_IO_buf_end"
 5047      6275665F 
 5047      656E6400 
 5048              	.LASF212:
 5049 0e59 7763736C 		.string	"wcslen"
 5049      656E00
 5050              	.LASF94:
 5051 0e60 696F7374 		.string	"iostate"
 5051      61746500 
 5052              	.LASF46:
 5053 0e68 5F535F69 		.string	"_S_ios_fmtflags_max"
 5053      6F735F66 
 5053      6D74666C 
 5053      6167735F 
GAS LISTING /tmp/cc45yn9z.s 			page 103


 5053      6D617800 
 5054              	.LASF20:
 5055 0e7c 746F5F69 		.string	"to_int_type"
 5055      6E745F74 
 5055      79706500 
 5056              	.LASF18:
 5057 0e88 746F5F63 		.string	"to_char_type"
 5057      6861725F 
 5057      74797065 
 5057      00
 5058              	.LASF1:
 5059 0e95 5F5F6465 		.string	"__debug"
 5059      62756700 
 5060              	.LASF210:
 5061 0e9d 746D5F67 		.string	"tm_gmtoff"
 5061      6D746F66 
 5061      6600
 5062              	.LASF255:
 5063 0ea7 63757272 		.string	"currency_symbol"
 5063      656E6379 
 5063      5F73796D 
 5063      626F6C00 
 5064              	.LASF247:
 5065 0eb7 73686F72 		.string	"short int"
 5065      7420696E 
 5065      7400
 5066              	.LASF9:
 5067 0ec1 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6lengthEPKc"
 5067      74313163 
 5067      6861725F 
 5067      74726169 
 5067      74734963 
 5068              	.LASF200:
 5069 0ee2 77637366 		.string	"wcsftime"
 5069      74696D65 
 5069      00
 5070              	.LASF291:
 5071 0eeb 5F5F7374 		.string	"__state"
 5071      61746500 
 5072              	.LASF308:
 5073 0ef3 66736565 		.string	"fseek"
 5073      6B00
 5074              	.LASF322:
 5075 0ef9 746D706E 		.string	"tmpnam"
 5075      616D00
 5076              	.LASF140:
 5077 0f00 5F767461 		.string	"_vtable_offset"
 5077      626C655F 
 5077      6F666673 
 5077      657400
 5078              	.LASF258:
 5079 0f0f 6D6F6E5F 		.string	"mon_grouping"
 5079      67726F75 
 5079      70696E67 
 5079      00
 5080              	.LASF69:
 5081 0f1c 5F535F63 		.string	"_S_cur"
GAS LISTING /tmp/cc45yn9z.s 			page 104


 5081      757200
 5082              	.LASF340:
 5083 0f23 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6assignERcRKc"
 5083      74313163 
 5083      6861725F 
 5083      74726169 
 5083      74734963 
 5084              	.LASF195:
 5085 0f46 77637363 		.string	"wcscat"
 5085      617400
 5086              	.LASF321:
 5087 0f4d 746D7066 		.string	"tmpfile"
 5087      696C6500 
 5088              	.LASF288:
 5089 0f55 31315F5F 		.string	"11__mbstate_t"
 5089      6D627374 
 5089      6174655F 
 5089      7400
 5090              	.LASF273:
 5091 0f63 696E745F 		.string	"int_p_sign_posn"
 5091      705F7369 
 5091      676E5F70 
 5091      6F736E00 
 5092              	.LASF211:
 5093 0f73 746D5F7A 		.string	"tm_zone"
 5093      6F6E6500 
 5094              	.LASF323:
 5095 0f7b 756E6765 		.string	"ungetc"
 5095      746300
 5096              	.LASF193:
 5097 0f82 76777363 		.string	"vwscanf"
 5097      616E6600 
 5098              	.LASF64:
 5099 0f8a 5F535F69 		.string	"_S_ios_iostate_end"
 5099      6F735F69 
 5099      6F737461 
 5099      74655F65 
 5099      6E6400
 5100              	.LASF194:
 5101 0f9d 77637274 		.string	"wcrtomb"
 5101      6F6D6200 
 5102              	.LASF250:
 5103 0fa5 6C636F6E 		.string	"lconv"
 5103      7600
 5104              	.LASF89:
 5105 0fab 756E6974 		.string	"unitbuf"
 5105      62756600 
 5106              	.LASF341:
 5107 0fb3 5F5A4E53 		.string	"_ZNSt11char_traitsIcE3eofEv"
 5107      74313163 
 5107      6861725F 
 5107      74726169 
 5107      74734963 
 5108              	.LASF213:
 5109 0fcf 7763736E 		.string	"wcsncat"
 5109      63617400 
 5110              	.LASF119:
GAS LISTING /tmp/cc45yn9z.s 			page 105


 5111 0fd7 5F5F6E75 		.string	"__numeric_traits_integer<short int>"
 5111      6D657269 
 5111      635F7472 
 5111      61697473 
 5111      5F696E74 
 5112              	.LASF328:
 5113 0ffb 5F5F6473 		.string	"__dso_handle"
 5113      6F5F6861 
 5113      6E646C65 
 5113      00
 5114              	.LASF242:
 5115 1008 6C6F6E67 		.string	"long long int"
 5115      206C6F6E 
 5115      6720696E 
 5115      7400
 5116              	.LASF173:
 5117 1016 66707574 		.string	"fputwc"
 5117      776300
 5118              	.LASF136:
 5119 101d 5F66696C 		.string	"_fileno"
 5119      656E6F00 
 5120              	.LASF174:
 5121 1025 66707574 		.string	"fputws"
 5121      777300
 5122              	.LASF74:
 5123 102c 7E496E69 		.string	"~Init"
 5123      7400
 5124              	.LASF182:
 5125 1032 6D627372 		.string	"mbsrtowcs"
 5125      746F7763 
 5125      7300
 5126              	.LASF63:
 5127 103c 5F535F66 		.string	"_S_failbit"
 5127      61696C62 
 5127      697400
 5128              	.LASF263:
 5129 1047 705F6373 		.string	"p_cs_precedes"
 5129      5F707265 
 5129      63656465 
 5129      7300
 5130              	.LASF117:
 5131 1055 5F5F6E75 		.string	"__numeric_traits_integer<long unsigned int>"
 5131      6D657269 
 5131      635F7472 
 5131      61697473 
 5131      5F696E74 
 5132              	.LASF206:
 5133 1081 746D5F79 		.string	"tm_year"
 5133      65617200 
 5134              	.LASF168:
 5135 1089 73686F72 		.string	"short unsigned int"
 5135      7420756E 
 5135      7369676E 
 5135      65642069 
 5135      6E7400
 5136              	.LASF106:
 5137 109c 5F547261 		.string	"_Traits"
GAS LISTING /tmp/cc45yn9z.s 			page 106


 5137      69747300 
 5138              	.LASF109:
 5139 10a4 5F5F6F70 		.string	"__ops"
 5139      7300
 5140              	.LASF189:
 5141 10aa 76667773 		.string	"vfwscanf"
 5141      63616E66 
 5141      00
 5142              	.LASF127:
 5143 10b3 5F494F5F 		.string	"_IO_write_ptr"
 5143      77726974 
 5143      655F7074 
 5143      7200
 5144              	.LASF67:
 5145 10c1 5F496F73 		.string	"_Ios_Seekdir"
 5145      5F536565 
 5145      6B646972 
 5145      00
 5146              	.LASF78:
 5147 10ce 666D7466 		.string	"fmtflags"
 5147      6C616773 
 5147      00
 5148              	.LASF278:
 5149 10d7 5F5F696E 		.string	"__int32_t"
 5149      7433325F 
 5149      7400
 5150              	.LASF178:
 5151 10e1 67657477 		.string	"getwc"
 5151      6300
 5152              	.LASF304:
 5153 10e7 66676574 		.string	"fgets"
 5153      7300
 5154              	.LASF181:
 5155 10ed 6D627369 		.string	"mbsinit"
 5155      6E697400 
 5156              	.LASF284:
 5157 10f5 69737763 		.string	"iswctype"
 5157      74797065 
 5157      00
 5158              	.LASF16:
 5159 10fe 61737369 		.string	"assign"
 5159      676E00
 5160              	.LASF253:
 5161 1105 67726F75 		.string	"grouping"
 5161      70696E67 
 5161      00
 5162              	.LASF232:
 5163 110e 77707269 		.string	"wprintf"
 5163      6E746600 
 5164              	.LASF336:
 5165 1116 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIlE5__maxE"
 5165      5F5F676E 
 5165      755F6378 
 5165      7832345F 
 5165      5F6E756D 
 5166              	.LASF342:
 5167 1148 5F5A4E53 		.string	"_ZNSt11char_traitsIcE7not_eofERKi"
GAS LISTING /tmp/cc45yn9z.s 			page 107


 5167      74313163 
 5167      6861725F 
 5167      74726169 
 5167      74734963 
 5168              	.LASF301:
 5169 116a 66666C75 		.string	"fflush"
 5169      736800
 5170              	.LASF35:
 5171 1171 5F535F73 		.string	"_S_scientific"
 5171      6369656E 
 5171      74696669 
 5171      6300
 5172              	.LASF172:
 5173 117f 77636861 		.string	"wchar_t"
 5173      725F7400 
 5174              	.LASF152:
 5175 1187 74797065 		.string	"typedef __va_list_tag __va_list_tag"
 5175      64656620 
 5175      5F5F7661 
 5175      5F6C6973 
 5175      745F7461 
 5176              	.LASF225:
 5177 11ab 77637374 		.string	"wcstoul"
 5177      6F756C00 
 5178              	.LASF307:
 5179 11b3 6672656F 		.string	"freopen"
 5179      70656E00 
 5180              		.hidden	__dso_handle
 5181              		.ident	"GCC: (Ubuntu 5.4.1-2ubuntu1~16.04) 5.4.1 20160904"
 5182              		.section	.note.GNU-stack,"",@progbits
GAS LISTING /tmp/cc45yn9z.s 			page 108


DEFINED SYMBOLS
                            *ABS*:0000000000000000 io-error-handling.cpp
                             .bss:0000000000000000 _ZStL8__ioinit
     /tmp/cc45yn9z.s:16     .text:0000000000000000 main
     /tmp/cc45yn9z.s:151    .text:000000000000012c _Z41__static_initialization_and_destruction_0ii
     /tmp/cc45yn9z.s:187    .text:000000000000016a _GLOBAL__sub_I_main

UNDEFINED SYMBOLS
__gxx_personality_v0
_ZNSt14basic_ifstreamIcSt11char_traitsIcEEC1EPKcSt13_Ios_Openmode
_ZNSirsERi
_ZNSirsERd
_ZSt4cout
_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
_ZNSolsEi
_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
_ZNSolsEPFRSoS_E
_ZNSolsEd
_ZNSt14basic_ifstreamIcSt11char_traitsIcEE5closeEv
_ZNSt14basic_ifstreamIcSt11char_traitsIcEED1Ev
_Unwind_Resume
__stack_chk_fail
_ZNSt8ios_base4InitC1Ev
__dso_handle
_ZNSt8ios_base4InitD1Ev
__cxa_atexit
