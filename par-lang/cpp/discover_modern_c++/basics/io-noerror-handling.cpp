/*
 * dealing with io errors
 *
 * c++ io is not fail-safe
 *
 * errors can be reported in different ways
 *  our error handling must comply to them ?
 *
 * example program
 */

#include <fstream>
#include <iostream>


int main()
{
  std::ifstream infile("some_missing_file.xyz");

  int i;
  double d;

  infile >> i >> d;

  std::cout << "i -> " << i << std::endl << "d -> " << d << std::endl ;

  infile.close();
}
