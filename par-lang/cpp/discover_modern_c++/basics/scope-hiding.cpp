#include <iostream>
void show(int number);

int main()
{
  int a = 5;  //define a#1
  show(a);
  {
    show(a);
    a = 3; // assign a#1, a#2 is not defined yet
    show(a);

    show(a);
    int a; // define a#2
    a = 8; // assign a#2, a#1 is hidden
    show(a);

    {
      show(a);
      a = 7; // assign a#2
      show(a);
    }

    a = 11; // assign to a#1 (a#2 out of scope)
    show(a);
  }
  return 0;
}

void show(int number)
{
  std::cout << "a --> " << number << std::endl;
}
