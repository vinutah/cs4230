GAS LISTING /tmp/cceirXUd.s 			page 1


   1              		.file	"scope-hiding.cpp"
   2              		.text
   3              	.Ltext0:
   4              		.local	_ZStL8__ioinit
   5              		.comm	_ZStL8__ioinit,1,1
   6              		.globl	main
   7              		.type	main, @function
   8              	main:
   9              	.LFB1021:
  10              		.file 1 "scope-hiding.cpp"
   1:scope-hiding.cpp **** #include <iostream>
   2:scope-hiding.cpp **** void show(int number);
   3:scope-hiding.cpp **** 
   4:scope-hiding.cpp **** int main()
   5:scope-hiding.cpp **** {
  11              		.loc 1 5 0
  12              		.cfi_startproc
  13 0000 55       		pushq	%rbp
  14              		.cfi_def_cfa_offset 16
  15              		.cfi_offset 6, -16
  16 0001 4889E5   		movq	%rsp, %rbp
  17              		.cfi_def_cfa_register 6
  18 0004 4883EC10 		subq	$16, %rsp
   6:scope-hiding.cpp ****   int a = 5;  //define a#1
  19              		.loc 1 6 0
  20 0008 C745F805 		movl	$5, -8(%rbp)
  20      000000
   7:scope-hiding.cpp ****   show(a);
  21              		.loc 1 7 0
  22 000f 8B45F8   		movl	-8(%rbp), %eax
  23 0012 89C7     		movl	%eax, %edi
  24 0014 E8000000 		call	_Z4showi
  24      00
  25              	.LBB2:
   8:scope-hiding.cpp ****   {
   9:scope-hiding.cpp ****     show(a);
  26              		.loc 1 9 0
  27 0019 8B45F8   		movl	-8(%rbp), %eax
  28 001c 89C7     		movl	%eax, %edi
  29 001e E8000000 		call	_Z4showi
  29      00
  10:scope-hiding.cpp ****     a = 3; // assign a#1, a#2 is not defined yet
  30              		.loc 1 10 0
  31 0023 C745F803 		movl	$3, -8(%rbp)
  31      000000
  11:scope-hiding.cpp ****     show(a);
  32              		.loc 1 11 0
  33 002a 8B45F8   		movl	-8(%rbp), %eax
  34 002d 89C7     		movl	%eax, %edi
  35 002f E8000000 		call	_Z4showi
  35      00
  12:scope-hiding.cpp **** 
  13:scope-hiding.cpp ****     show(a);
  36              		.loc 1 13 0
  37 0034 8B45F8   		movl	-8(%rbp), %eax
  38 0037 89C7     		movl	%eax, %edi
  39 0039 E8000000 		call	_Z4showi
GAS LISTING /tmp/cceirXUd.s 			page 2


  39      00
  14:scope-hiding.cpp ****     int a; // define a#2
  15:scope-hiding.cpp ****     a = 8; // assign a#2, a#1 is hidden
  40              		.loc 1 15 0
  41 003e C745FC08 		movl	$8, -4(%rbp)
  41      000000
  16:scope-hiding.cpp ****     show(a);
  42              		.loc 1 16 0
  43 0045 8B45FC   		movl	-4(%rbp), %eax
  44 0048 89C7     		movl	%eax, %edi
  45 004a E8000000 		call	_Z4showi
  45      00
  17:scope-hiding.cpp **** 
  18:scope-hiding.cpp ****     {
  19:scope-hiding.cpp ****       show(a);
  46              		.loc 1 19 0
  47 004f 8B45FC   		movl	-4(%rbp), %eax
  48 0052 89C7     		movl	%eax, %edi
  49 0054 E8000000 		call	_Z4showi
  49      00
  20:scope-hiding.cpp ****       a = 7; // assign a#2
  50              		.loc 1 20 0
  51 0059 C745FC07 		movl	$7, -4(%rbp)
  51      000000
  21:scope-hiding.cpp ****       show(a);
  52              		.loc 1 21 0
  53 0060 8B45FC   		movl	-4(%rbp), %eax
  54 0063 89C7     		movl	%eax, %edi
  55 0065 E8000000 		call	_Z4showi
  55      00
  22:scope-hiding.cpp ****     }
  23:scope-hiding.cpp **** 
  24:scope-hiding.cpp ****     a = 11; // assign to a#1 (a#2 out of scope)
  56              		.loc 1 24 0
  57 006a C745FC0B 		movl	$11, -4(%rbp)
  57      000000
  25:scope-hiding.cpp ****     show(a);
  58              		.loc 1 25 0
  59 0071 8B45FC   		movl	-4(%rbp), %eax
  60 0074 89C7     		movl	%eax, %edi
  61 0076 E8000000 		call	_Z4showi
  61      00
  62              	.LBE2:
  26:scope-hiding.cpp ****   }
  27:scope-hiding.cpp ****   return 0;
  63              		.loc 1 27 0
  64 007b B8000000 		movl	$0, %eax
  64      00
  28:scope-hiding.cpp **** }
  65              		.loc 1 28 0
  66 0080 C9       		leave
  67              		.cfi_def_cfa 7, 8
  68 0081 C3       		ret
  69              		.cfi_endproc
  70              	.LFE1021:
  71              		.size	main, .-main
  72              		.section	.rodata
GAS LISTING /tmp/cceirXUd.s 			page 3


  73              	.LC0:
  74 0000 61202D2D 		.string	"a --> "
  74      3E2000
  75              		.text
  76              		.globl	_Z4showi
  77              		.type	_Z4showi, @function
  78              	_Z4showi:
  79              	.LFB1022:
  29:scope-hiding.cpp **** 
  30:scope-hiding.cpp **** void show(int number)
  31:scope-hiding.cpp **** {
  80              		.loc 1 31 0
  81              		.cfi_startproc
  82 0082 55       		pushq	%rbp
  83              		.cfi_def_cfa_offset 16
  84              		.cfi_offset 6, -16
  85 0083 4889E5   		movq	%rsp, %rbp
  86              		.cfi_def_cfa_register 6
  87 0086 4883EC10 		subq	$16, %rsp
  88 008a 897DFC   		movl	%edi, -4(%rbp)
  32:scope-hiding.cpp ****   std::cout << "a --> " << number << std::endl;
  89              		.loc 1 32 0
  90 008d BE000000 		movl	$.LC0, %esi
  90      00
  91 0092 BF000000 		movl	$_ZSt4cout, %edi
  91      00
  92 0097 E8000000 		call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
  92      00
  93 009c 4889C2   		movq	%rax, %rdx
  94 009f 8B45FC   		movl	-4(%rbp), %eax
  95 00a2 89C6     		movl	%eax, %esi
  96 00a4 4889D7   		movq	%rdx, %rdi
  97 00a7 E8000000 		call	_ZNSolsEi
  97      00
  98 00ac BE000000 		movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
  98      00
  99 00b1 4889C7   		movq	%rax, %rdi
 100 00b4 E8000000 		call	_ZNSolsEPFRSoS_E
 100      00
  33:scope-hiding.cpp **** }
 101              		.loc 1 33 0
 102 00b9 90       		nop
 103 00ba C9       		leave
 104              		.cfi_def_cfa 7, 8
 105 00bb C3       		ret
 106              		.cfi_endproc
 107              	.LFE1022:
 108              		.size	_Z4showi, .-_Z4showi
 109              		.type	_Z41__static_initialization_and_destruction_0ii, @function
 110              	_Z41__static_initialization_and_destruction_0ii:
 111              	.LFB1031:
 112              		.loc 1 33 0
 113              		.cfi_startproc
 114 00bc 55       		pushq	%rbp
 115              		.cfi_def_cfa_offset 16
 116              		.cfi_offset 6, -16
 117 00bd 4889E5   		movq	%rsp, %rbp
GAS LISTING /tmp/cceirXUd.s 			page 4


 118              		.cfi_def_cfa_register 6
 119 00c0 4883EC10 		subq	$16, %rsp
 120 00c4 897DFC   		movl	%edi, -4(%rbp)
 121 00c7 8975F8   		movl	%esi, -8(%rbp)
 122              		.loc 1 33 0
 123 00ca 837DFC01 		cmpl	$1, -4(%rbp)
 124 00ce 7527     		jne	.L6
 125              		.loc 1 33 0 is_stmt 0 discriminator 1
 126 00d0 817DF8FF 		cmpl	$65535, -8(%rbp)
 126      FF0000
 127 00d7 751E     		jne	.L6
 128              		.file 2 "/usr/include/c++/5/iostream"
   1:/usr/include/c++/5/iostream **** // Standard iostream objects -*- C++ -*-
   2:/usr/include/c++/5/iostream **** 
   3:/usr/include/c++/5/iostream **** // Copyright (C) 1997-2015 Free Software Foundation, Inc.
   4:/usr/include/c++/5/iostream **** //
   5:/usr/include/c++/5/iostream **** // This file is part of the GNU ISO C++ Library.  This library is free
   6:/usr/include/c++/5/iostream **** // software; you can redistribute it and/or modify it under the
   7:/usr/include/c++/5/iostream **** // terms of the GNU General Public License as published by the
   8:/usr/include/c++/5/iostream **** // Free Software Foundation; either version 3, or (at your option)
   9:/usr/include/c++/5/iostream **** // any later version.
  10:/usr/include/c++/5/iostream **** 
  11:/usr/include/c++/5/iostream **** // This library is distributed in the hope that it will be useful,
  12:/usr/include/c++/5/iostream **** // but WITHOUT ANY WARRANTY; without even the implied warranty of
  13:/usr/include/c++/5/iostream **** // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  14:/usr/include/c++/5/iostream **** // GNU General Public License for more details.
  15:/usr/include/c++/5/iostream **** 
  16:/usr/include/c++/5/iostream **** // Under Section 7 of GPL version 3, you are granted additional
  17:/usr/include/c++/5/iostream **** // permissions described in the GCC Runtime Library Exception, version
  18:/usr/include/c++/5/iostream **** // 3.1, as published by the Free Software Foundation.
  19:/usr/include/c++/5/iostream **** 
  20:/usr/include/c++/5/iostream **** // You should have received a copy of the GNU General Public License and
  21:/usr/include/c++/5/iostream **** // a copy of the GCC Runtime Library Exception along with this program;
  22:/usr/include/c++/5/iostream **** // see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
  23:/usr/include/c++/5/iostream **** // <http://www.gnu.org/licenses/>.
  24:/usr/include/c++/5/iostream **** 
  25:/usr/include/c++/5/iostream **** /** @file include/iostream
  26:/usr/include/c++/5/iostream ****  *  This is a Standard C++ Library header.
  27:/usr/include/c++/5/iostream ****  */
  28:/usr/include/c++/5/iostream **** 
  29:/usr/include/c++/5/iostream **** //
  30:/usr/include/c++/5/iostream **** // ISO C++ 14882: 27.3  Standard iostream objects
  31:/usr/include/c++/5/iostream **** //
  32:/usr/include/c++/5/iostream **** 
  33:/usr/include/c++/5/iostream **** #ifndef _GLIBCXX_IOSTREAM
  34:/usr/include/c++/5/iostream **** #define _GLIBCXX_IOSTREAM 1
  35:/usr/include/c++/5/iostream **** 
  36:/usr/include/c++/5/iostream **** #pragma GCC system_header
  37:/usr/include/c++/5/iostream **** 
  38:/usr/include/c++/5/iostream **** #include <bits/c++config.h>
  39:/usr/include/c++/5/iostream **** #include <ostream>
  40:/usr/include/c++/5/iostream **** #include <istream>
  41:/usr/include/c++/5/iostream **** 
  42:/usr/include/c++/5/iostream **** namespace std _GLIBCXX_VISIBILITY(default)
  43:/usr/include/c++/5/iostream **** {
  44:/usr/include/c++/5/iostream **** _GLIBCXX_BEGIN_NAMESPACE_VERSION
  45:/usr/include/c++/5/iostream **** 
GAS LISTING /tmp/cceirXUd.s 			page 5


  46:/usr/include/c++/5/iostream ****   /**
  47:/usr/include/c++/5/iostream ****    *  @name Standard Stream Objects
  48:/usr/include/c++/5/iostream ****    *
  49:/usr/include/c++/5/iostream ****    *  The &lt;iostream&gt; header declares the eight <em>standard stream
  50:/usr/include/c++/5/iostream ****    *  objects</em>.  For other declarations, see
  51:/usr/include/c++/5/iostream ****    *  http://gcc.gnu.org/onlinedocs/libstdc++/manual/io.html
  52:/usr/include/c++/5/iostream ****    *  and the @link iosfwd I/O forward declarations @endlink
  53:/usr/include/c++/5/iostream ****    *
  54:/usr/include/c++/5/iostream ****    *  They are required by default to cooperate with the global C
  55:/usr/include/c++/5/iostream ****    *  library's @c FILE streams, and to be available during program
  56:/usr/include/c++/5/iostream ****    *  startup and termination. For more information, see the section of the
  57:/usr/include/c++/5/iostream ****    *  manual linked to above.
  58:/usr/include/c++/5/iostream ****   */
  59:/usr/include/c++/5/iostream ****   //@{
  60:/usr/include/c++/5/iostream ****   extern istream cin;		/// Linked to standard input
  61:/usr/include/c++/5/iostream ****   extern ostream cout;		/// Linked to standard output
  62:/usr/include/c++/5/iostream ****   extern ostream cerr;		/// Linked to standard error (unbuffered)
  63:/usr/include/c++/5/iostream ****   extern ostream clog;		/// Linked to standard error (buffered)
  64:/usr/include/c++/5/iostream **** 
  65:/usr/include/c++/5/iostream **** #ifdef _GLIBCXX_USE_WCHAR_T
  66:/usr/include/c++/5/iostream ****   extern wistream wcin;		/// Linked to standard input
  67:/usr/include/c++/5/iostream ****   extern wostream wcout;	/// Linked to standard output
  68:/usr/include/c++/5/iostream ****   extern wostream wcerr;	/// Linked to standard error (unbuffered)
  69:/usr/include/c++/5/iostream ****   extern wostream wclog;	/// Linked to standard error (buffered)
  70:/usr/include/c++/5/iostream **** #endif
  71:/usr/include/c++/5/iostream ****   //@}
  72:/usr/include/c++/5/iostream **** 
  73:/usr/include/c++/5/iostream ****   // For construction of filebuffers for cout, cin, cerr, clog et. al.
  74:/usr/include/c++/5/iostream ****   static ios_base::Init __ioinit;
 129              		.loc 2 74 0 is_stmt 1
 130 00d9 BF000000 		movl	$_ZStL8__ioinit, %edi
 130      00
 131 00de E8000000 		call	_ZNSt8ios_base4InitC1Ev
 131      00
 132 00e3 BA000000 		movl	$__dso_handle, %edx
 132      00
 133 00e8 BE000000 		movl	$_ZStL8__ioinit, %esi
 133      00
 134 00ed BF000000 		movl	$_ZNSt8ios_base4InitD1Ev, %edi
 134      00
 135 00f2 E8000000 		call	__cxa_atexit
 135      00
 136              	.L6:
 137              		.loc 1 33 0
 138 00f7 90       		nop
 139 00f8 C9       		leave
 140              		.cfi_def_cfa 7, 8
 141 00f9 C3       		ret
 142              		.cfi_endproc
 143              	.LFE1031:
 144              		.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destructi
 145              		.type	_GLOBAL__sub_I_main, @function
 146              	_GLOBAL__sub_I_main:
 147              	.LFB1032:
 148              		.loc 1 33 0
 149              		.cfi_startproc
 150 00fa 55       		pushq	%rbp
GAS LISTING /tmp/cceirXUd.s 			page 6


 151              		.cfi_def_cfa_offset 16
 152              		.cfi_offset 6, -16
 153 00fb 4889E5   		movq	%rsp, %rbp
 154              		.cfi_def_cfa_register 6
 155              		.loc 1 33 0
 156 00fe BEFFFF00 		movl	$65535, %esi
 156      00
 157 0103 BF010000 		movl	$1, %edi
 157      00
 158 0108 E8AFFFFF 		call	_Z41__static_initialization_and_destruction_0ii
 158      FF
 159 010d 5D       		popq	%rbp
 160              		.cfi_def_cfa 7, 8
 161 010e C3       		ret
 162              		.cfi_endproc
 163              	.LFE1032:
 164              		.size	_GLOBAL__sub_I_main, .-_GLOBAL__sub_I_main
 165              		.section	.init_array,"aw"
 166              		.align 8
 167 0000 00000000 		.quad	_GLOBAL__sub_I_main
 167      00000000 
 168              		.text
 169              	.Letext0:
 170              		.file 3 "/usr/include/c++/5/cwchar"
 171              		.file 4 "/usr/include/x86_64-linux-gnu/c++/5/bits/c++config.h"
 172              		.file 5 "/usr/include/c++/5/debug/debug.h"
 173              		.file 6 "/usr/include/c++/5/bits/char_traits.h"
 174              		.file 7 "/usr/include/c++/5/clocale"
 175              		.file 8 "/usr/include/c++/5/bits/ios_base.h"
 176              		.file 9 "/usr/include/c++/5/cwctype"
 177              		.file 10 "/usr/include/c++/5/iosfwd"
 178              		.file 11 "/usr/include/c++/5/bits/predefined_ops.h"
 179              		.file 12 "/usr/include/c++/5/ext/new_allocator.h"
 180              		.file 13 "/usr/include/c++/5/ext/numeric_traits.h"
 181              		.file 14 "/usr/include/stdio.h"
 182              		.file 15 "<built-in>"
 183              		.file 16 "/usr/lib/gcc/x86_64-linux-gnu/5/include/stddef.h"
 184              		.file 17 "/usr/include/wchar.h"
 185              		.file 18 "/usr/include/time.h"
 186              		.file 19 "/usr/include/locale.h"
 187              		.file 20 "/usr/include/x86_64-linux-gnu/bits/types.h"
 188              		.file 21 "/usr/include/x86_64-linux-gnu/c++/5/bits/atomic_word.h"
 189              		.file 22 "/usr/include/wctype.h"
 190              		.section	.debug_info,"",@progbits
 191              	.Ldebug_info0:
 192 0000 1A160000 		.long	0x161a
 193 0004 0400     		.value	0x4
 194 0006 00000000 		.long	.Ldebug_abbrev0
 195 000a 08       		.byte	0x8
 196 000b 01       		.uleb128 0x1
 197 000c 00000000 		.long	.LASF267
 198 0010 04       		.byte	0x4
 199 0011 00000000 		.long	.LASF268
 200 0015 00000000 		.long	.LASF269
 201 0019 00000000 		.quad	.Ltext0
 201      00000000 
 202 0021 0F010000 		.quad	.Letext0-.Ltext0
GAS LISTING /tmp/cceirXUd.s 			page 7


 202      00000000 
 203 0029 00000000 		.long	.Ldebug_line0
 204 002d 02       		.uleb128 0x2
 205 002e 73746400 		.string	"std"
 206 0032 0F       		.byte	0xf
 207 0033 00       		.byte	0
 208 0034 2D080000 		.long	0x82d
 209 0038 03       		.uleb128 0x3
 210 0039 00000000 		.long	.LASF0
 211 003d 04       		.byte	0x4
 212 003e DA       		.byte	0xda
 213 003f 04       		.uleb128 0x4
 214 0040 04       		.byte	0x4
 215 0041 DA       		.byte	0xda
 216 0042 38000000 		.long	0x38
 217 0046 05       		.uleb128 0x5
 218 0047 03       		.byte	0x3
 219 0048 40       		.byte	0x40
 220 0049 A10A0000 		.long	0xaa1
 221 004d 05       		.uleb128 0x5
 222 004e 03       		.byte	0x3
 223 004f 8B       		.byte	0x8b
 224 0050 280A0000 		.long	0xa28
 225 0054 05       		.uleb128 0x5
 226 0055 03       		.byte	0x3
 227 0056 8D       		.byte	0x8d
 228 0057 C30A0000 		.long	0xac3
 229 005b 05       		.uleb128 0x5
 230 005c 03       		.byte	0x3
 231 005d 8E       		.byte	0x8e
 232 005e D90A0000 		.long	0xad9
 233 0062 05       		.uleb128 0x5
 234 0063 03       		.byte	0x3
 235 0064 8F       		.byte	0x8f
 236 0065 F50A0000 		.long	0xaf5
 237 0069 05       		.uleb128 0x5
 238 006a 03       		.byte	0x3
 239 006b 90       		.byte	0x90
 240 006c 220B0000 		.long	0xb22
 241 0070 05       		.uleb128 0x5
 242 0071 03       		.byte	0x3
 243 0072 91       		.byte	0x91
 244 0073 3D0B0000 		.long	0xb3d
 245 0077 05       		.uleb128 0x5
 246 0078 03       		.byte	0x3
 247 0079 92       		.byte	0x92
 248 007a 630B0000 		.long	0xb63
 249 007e 05       		.uleb128 0x5
 250 007f 03       		.byte	0x3
 251 0080 93       		.byte	0x93
 252 0081 7E0B0000 		.long	0xb7e
 253 0085 05       		.uleb128 0x5
 254 0086 03       		.byte	0x3
 255 0087 94       		.byte	0x94
 256 0088 9A0B0000 		.long	0xb9a
 257 008c 05       		.uleb128 0x5
 258 008d 03       		.byte	0x3
GAS LISTING /tmp/cceirXUd.s 			page 8


 259 008e 95       		.byte	0x95
 260 008f B60B0000 		.long	0xbb6
 261 0093 05       		.uleb128 0x5
 262 0094 03       		.byte	0x3
 263 0095 96       		.byte	0x96
 264 0096 CC0B0000 		.long	0xbcc
 265 009a 05       		.uleb128 0x5
 266 009b 03       		.byte	0x3
 267 009c 97       		.byte	0x97
 268 009d D80B0000 		.long	0xbd8
 269 00a1 05       		.uleb128 0x5
 270 00a2 03       		.byte	0x3
 271 00a3 98       		.byte	0x98
 272 00a4 FE0B0000 		.long	0xbfe
 273 00a8 05       		.uleb128 0x5
 274 00a9 03       		.byte	0x3
 275 00aa 99       		.byte	0x99
 276 00ab 230C0000 		.long	0xc23
 277 00af 05       		.uleb128 0x5
 278 00b0 03       		.byte	0x3
 279 00b1 9A       		.byte	0x9a
 280 00b2 440C0000 		.long	0xc44
 281 00b6 05       		.uleb128 0x5
 282 00b7 03       		.byte	0x3
 283 00b8 9B       		.byte	0x9b
 284 00b9 6F0C0000 		.long	0xc6f
 285 00bd 05       		.uleb128 0x5
 286 00be 03       		.byte	0x3
 287 00bf 9C       		.byte	0x9c
 288 00c0 8A0C0000 		.long	0xc8a
 289 00c4 05       		.uleb128 0x5
 290 00c5 03       		.byte	0x3
 291 00c6 9E       		.byte	0x9e
 292 00c7 A00C0000 		.long	0xca0
 293 00cb 05       		.uleb128 0x5
 294 00cc 03       		.byte	0x3
 295 00cd A0       		.byte	0xa0
 296 00ce C10C0000 		.long	0xcc1
 297 00d2 05       		.uleb128 0x5
 298 00d3 03       		.byte	0x3
 299 00d4 A1       		.byte	0xa1
 300 00d5 DD0C0000 		.long	0xcdd
 301 00d9 05       		.uleb128 0x5
 302 00da 03       		.byte	0x3
 303 00db A2       		.byte	0xa2
 304 00dc F80C0000 		.long	0xcf8
 305 00e0 05       		.uleb128 0x5
 306 00e1 03       		.byte	0x3
 307 00e2 A4       		.byte	0xa4
 308 00e3 1E0D0000 		.long	0xd1e
 309 00e7 05       		.uleb128 0x5
 310 00e8 03       		.byte	0x3
 311 00e9 A7       		.byte	0xa7
 312 00ea 3E0D0000 		.long	0xd3e
 313 00ee 05       		.uleb128 0x5
 314 00ef 03       		.byte	0x3
 315 00f0 AA       		.byte	0xaa
GAS LISTING /tmp/cceirXUd.s 			page 9


 316 00f1 630D0000 		.long	0xd63
 317 00f5 05       		.uleb128 0x5
 318 00f6 03       		.byte	0x3
 319 00f7 AC       		.byte	0xac
 320 00f8 830D0000 		.long	0xd83
 321 00fc 05       		.uleb128 0x5
 322 00fd 03       		.byte	0x3
 323 00fe AE       		.byte	0xae
 324 00ff 9E0D0000 		.long	0xd9e
 325 0103 05       		.uleb128 0x5
 326 0104 03       		.byte	0x3
 327 0105 B0       		.byte	0xb0
 328 0106 B90D0000 		.long	0xdb9
 329 010a 05       		.uleb128 0x5
 330 010b 03       		.byte	0x3
 331 010c B1       		.byte	0xb1
 332 010d DF0D0000 		.long	0xddf
 333 0111 05       		.uleb128 0x5
 334 0112 03       		.byte	0x3
 335 0113 B2       		.byte	0xb2
 336 0114 F90D0000 		.long	0xdf9
 337 0118 05       		.uleb128 0x5
 338 0119 03       		.byte	0x3
 339 011a B3       		.byte	0xb3
 340 011b 130E0000 		.long	0xe13
 341 011f 05       		.uleb128 0x5
 342 0120 03       		.byte	0x3
 343 0121 B4       		.byte	0xb4
 344 0122 2D0E0000 		.long	0xe2d
 345 0126 05       		.uleb128 0x5
 346 0127 03       		.byte	0x3
 347 0128 B5       		.byte	0xb5
 348 0129 470E0000 		.long	0xe47
 349 012d 05       		.uleb128 0x5
 350 012e 03       		.byte	0x3
 351 012f B6       		.byte	0xb6
 352 0130 610E0000 		.long	0xe61
 353 0134 05       		.uleb128 0x5
 354 0135 03       		.byte	0x3
 355 0136 B7       		.byte	0xb7
 356 0137 210F0000 		.long	0xf21
 357 013b 05       		.uleb128 0x5
 358 013c 03       		.byte	0x3
 359 013d B8       		.byte	0xb8
 360 013e 370F0000 		.long	0xf37
 361 0142 05       		.uleb128 0x5
 362 0143 03       		.byte	0x3
 363 0144 B9       		.byte	0xb9
 364 0145 560F0000 		.long	0xf56
 365 0149 05       		.uleb128 0x5
 366 014a 03       		.byte	0x3
 367 014b BA       		.byte	0xba
 368 014c 750F0000 		.long	0xf75
 369 0150 05       		.uleb128 0x5
 370 0151 03       		.byte	0x3
 371 0152 BB       		.byte	0xbb
 372 0153 940F0000 		.long	0xf94
GAS LISTING /tmp/cceirXUd.s 			page 10


 373 0157 05       		.uleb128 0x5
 374 0158 03       		.byte	0x3
 375 0159 BC       		.byte	0xbc
 376 015a BF0F0000 		.long	0xfbf
 377 015e 05       		.uleb128 0x5
 378 015f 03       		.byte	0x3
 379 0160 BD       		.byte	0xbd
 380 0161 DA0F0000 		.long	0xfda
 381 0165 05       		.uleb128 0x5
 382 0166 03       		.byte	0x3
 383 0167 BF       		.byte	0xbf
 384 0168 02100000 		.long	0x1002
 385 016c 05       		.uleb128 0x5
 386 016d 03       		.byte	0x3
 387 016e C1       		.byte	0xc1
 388 016f 24100000 		.long	0x1024
 389 0173 05       		.uleb128 0x5
 390 0174 03       		.byte	0x3
 391 0175 C2       		.byte	0xc2
 392 0176 44100000 		.long	0x1044
 393 017a 05       		.uleb128 0x5
 394 017b 03       		.byte	0x3
 395 017c C3       		.byte	0xc3
 396 017d 6B100000 		.long	0x106b
 397 0181 05       		.uleb128 0x5
 398 0182 03       		.byte	0x3
 399 0183 C4       		.byte	0xc4
 400 0184 8B100000 		.long	0x108b
 401 0188 05       		.uleb128 0x5
 402 0189 03       		.byte	0x3
 403 018a C5       		.byte	0xc5
 404 018b AA100000 		.long	0x10aa
 405 018f 05       		.uleb128 0x5
 406 0190 03       		.byte	0x3
 407 0191 C6       		.byte	0xc6
 408 0192 C0100000 		.long	0x10c0
 409 0196 05       		.uleb128 0x5
 410 0197 03       		.byte	0x3
 411 0198 C7       		.byte	0xc7
 412 0199 E0100000 		.long	0x10e0
 413 019d 05       		.uleb128 0x5
 414 019e 03       		.byte	0x3
 415 019f C8       		.byte	0xc8
 416 01a0 00110000 		.long	0x1100
 417 01a4 05       		.uleb128 0x5
 418 01a5 03       		.byte	0x3
 419 01a6 C9       		.byte	0xc9
 420 01a7 20110000 		.long	0x1120
 421 01ab 05       		.uleb128 0x5
 422 01ac 03       		.byte	0x3
 423 01ad CA       		.byte	0xca
 424 01ae 40110000 		.long	0x1140
 425 01b2 05       		.uleb128 0x5
 426 01b3 03       		.byte	0x3
 427 01b4 CB       		.byte	0xcb
 428 01b5 57110000 		.long	0x1157
 429 01b9 05       		.uleb128 0x5
GAS LISTING /tmp/cceirXUd.s 			page 11


 430 01ba 03       		.byte	0x3
 431 01bb CC       		.byte	0xcc
 432 01bc 6E110000 		.long	0x116e
 433 01c0 05       		.uleb128 0x5
 434 01c1 03       		.byte	0x3
 435 01c2 CD       		.byte	0xcd
 436 01c3 8C110000 		.long	0x118c
 437 01c7 05       		.uleb128 0x5
 438 01c8 03       		.byte	0x3
 439 01c9 CE       		.byte	0xce
 440 01ca AB110000 		.long	0x11ab
 441 01ce 05       		.uleb128 0x5
 442 01cf 03       		.byte	0x3
 443 01d0 CF       		.byte	0xcf
 444 01d1 C9110000 		.long	0x11c9
 445 01d5 05       		.uleb128 0x5
 446 01d6 03       		.byte	0x3
 447 01d7 D0       		.byte	0xd0
 448 01d8 E8110000 		.long	0x11e8
 449 01dc 06       		.uleb128 0x6
 450 01dd 03       		.byte	0x3
 451 01de 0801     		.value	0x108
 452 01e0 0C120000 		.long	0x120c
 453 01e4 06       		.uleb128 0x6
 454 01e5 03       		.byte	0x3
 455 01e6 0901     		.value	0x109
 456 01e8 2E120000 		.long	0x122e
 457 01ec 06       		.uleb128 0x6
 458 01ed 03       		.byte	0x3
 459 01ee 0A01     		.value	0x10a
 460 01f0 55120000 		.long	0x1255
 461 01f4 03       		.uleb128 0x3
 462 01f5 00000000 		.long	.LASF1
 463 01f9 05       		.byte	0x5
 464 01fa 30       		.byte	0x30
 465 01fb 07       		.uleb128 0x7
 466 01fc 00000000 		.long	.LASF106
 467 0200 01       		.byte	0x1
 468 0201 06       		.byte	0x6
 469 0202 E9       		.byte	0xe9
 470 0203 C3030000 		.long	0x3c3
 471 0207 08       		.uleb128 0x8
 472 0208 00000000 		.long	.LASF2
 473 020c 06       		.byte	0x6
 474 020d EB       		.byte	0xeb
 475 020e 880A0000 		.long	0xa88
 476 0212 08       		.uleb128 0x8
 477 0213 00000000 		.long	.LASF3
 478 0217 06       		.byte	0x6
 479 0218 EC       		.byte	0xec
 480 0219 8F0A0000 		.long	0xa8f
 481 021d 09       		.uleb128 0x9
 482 021e 00000000 		.long	.LASF16
 483 0222 06       		.byte	0x6
 484 0223 F2       		.byte	0xf2
 485 0224 00000000 		.long	.LASF270
 486 0228 37020000 		.long	0x237
GAS LISTING /tmp/cceirXUd.s 			page 12


 487 022c 0A       		.uleb128 0xa
 488 022d A4120000 		.long	0x12a4
 489 0231 0A       		.uleb128 0xa
 490 0232 AA120000 		.long	0x12aa
 491 0236 00       		.byte	0
 492 0237 0B       		.uleb128 0xb
 493 0238 07020000 		.long	0x207
 494 023c 0C       		.uleb128 0xc
 495 023d 657100   		.string	"eq"
 496 0240 06       		.byte	0x6
 497 0241 F6       		.byte	0xf6
 498 0242 00000000 		.long	.LASF4
 499 0246 B0120000 		.long	0x12b0
 500 024a 59020000 		.long	0x259
 501 024e 0A       		.uleb128 0xa
 502 024f AA120000 		.long	0x12aa
 503 0253 0A       		.uleb128 0xa
 504 0254 AA120000 		.long	0x12aa
 505 0258 00       		.byte	0
 506 0259 0C       		.uleb128 0xc
 507 025a 6C7400   		.string	"lt"
 508 025d 06       		.byte	0x6
 509 025e FA       		.byte	0xfa
 510 025f 00000000 		.long	.LASF5
 511 0263 B0120000 		.long	0x12b0
 512 0267 76020000 		.long	0x276
 513 026b 0A       		.uleb128 0xa
 514 026c AA120000 		.long	0x12aa
 515 0270 0A       		.uleb128 0xa
 516 0271 AA120000 		.long	0x12aa
 517 0275 00       		.byte	0
 518 0276 0D       		.uleb128 0xd
 519 0277 00000000 		.long	.LASF6
 520 027b 06       		.byte	0x6
 521 027c 0201     		.value	0x102
 522 027e 00000000 		.long	.LASF8
 523 0282 8F0A0000 		.long	0xa8f
 524 0286 9A020000 		.long	0x29a
 525 028a 0A       		.uleb128 0xa
 526 028b B7120000 		.long	0x12b7
 527 028f 0A       		.uleb128 0xa
 528 0290 B7120000 		.long	0x12b7
 529 0294 0A       		.uleb128 0xa
 530 0295 C3030000 		.long	0x3c3
 531 0299 00       		.byte	0
 532 029a 0D       		.uleb128 0xd
 533 029b 00000000 		.long	.LASF7
 534 029f 06       		.byte	0x6
 535 02a0 0A01     		.value	0x10a
 536 02a2 00000000 		.long	.LASF9
 537 02a6 C3030000 		.long	0x3c3
 538 02aa B4020000 		.long	0x2b4
 539 02ae 0A       		.uleb128 0xa
 540 02af B7120000 		.long	0x12b7
 541 02b3 00       		.byte	0
 542 02b4 0D       		.uleb128 0xd
 543 02b5 00000000 		.long	.LASF10
GAS LISTING /tmp/cceirXUd.s 			page 13


 544 02b9 06       		.byte	0x6
 545 02ba 0E01     		.value	0x10e
 546 02bc 00000000 		.long	.LASF11
 547 02c0 B7120000 		.long	0x12b7
 548 02c4 D8020000 		.long	0x2d8
 549 02c8 0A       		.uleb128 0xa
 550 02c9 B7120000 		.long	0x12b7
 551 02cd 0A       		.uleb128 0xa
 552 02ce C3030000 		.long	0x3c3
 553 02d2 0A       		.uleb128 0xa
 554 02d3 AA120000 		.long	0x12aa
 555 02d7 00       		.byte	0
 556 02d8 0D       		.uleb128 0xd
 557 02d9 00000000 		.long	.LASF12
 558 02dd 06       		.byte	0x6
 559 02de 1601     		.value	0x116
 560 02e0 00000000 		.long	.LASF13
 561 02e4 BD120000 		.long	0x12bd
 562 02e8 FC020000 		.long	0x2fc
 563 02ec 0A       		.uleb128 0xa
 564 02ed BD120000 		.long	0x12bd
 565 02f1 0A       		.uleb128 0xa
 566 02f2 B7120000 		.long	0x12b7
 567 02f6 0A       		.uleb128 0xa
 568 02f7 C3030000 		.long	0x3c3
 569 02fb 00       		.byte	0
 570 02fc 0D       		.uleb128 0xd
 571 02fd 00000000 		.long	.LASF14
 572 0301 06       		.byte	0x6
 573 0302 1E01     		.value	0x11e
 574 0304 00000000 		.long	.LASF15
 575 0308 BD120000 		.long	0x12bd
 576 030c 20030000 		.long	0x320
 577 0310 0A       		.uleb128 0xa
 578 0311 BD120000 		.long	0x12bd
 579 0315 0A       		.uleb128 0xa
 580 0316 B7120000 		.long	0x12b7
 581 031a 0A       		.uleb128 0xa
 582 031b C3030000 		.long	0x3c3
 583 031f 00       		.byte	0
 584 0320 0D       		.uleb128 0xd
 585 0321 00000000 		.long	.LASF16
 586 0325 06       		.byte	0x6
 587 0326 2601     		.value	0x126
 588 0328 00000000 		.long	.LASF17
 589 032c BD120000 		.long	0x12bd
 590 0330 44030000 		.long	0x344
 591 0334 0A       		.uleb128 0xa
 592 0335 BD120000 		.long	0x12bd
 593 0339 0A       		.uleb128 0xa
 594 033a C3030000 		.long	0x3c3
 595 033e 0A       		.uleb128 0xa
 596 033f 07020000 		.long	0x207
 597 0343 00       		.byte	0
 598 0344 0D       		.uleb128 0xd
 599 0345 00000000 		.long	.LASF18
 600 0349 06       		.byte	0x6
GAS LISTING /tmp/cceirXUd.s 			page 14


 601 034a 2E01     		.value	0x12e
 602 034c 00000000 		.long	.LASF19
 603 0350 07020000 		.long	0x207
 604 0354 5E030000 		.long	0x35e
 605 0358 0A       		.uleb128 0xa
 606 0359 C3120000 		.long	0x12c3
 607 035d 00       		.byte	0
 608 035e 0B       		.uleb128 0xb
 609 035f 12020000 		.long	0x212
 610 0363 0D       		.uleb128 0xd
 611 0364 00000000 		.long	.LASF20
 612 0368 06       		.byte	0x6
 613 0369 3401     		.value	0x134
 614 036b 00000000 		.long	.LASF21
 615 036f 12020000 		.long	0x212
 616 0373 7D030000 		.long	0x37d
 617 0377 0A       		.uleb128 0xa
 618 0378 AA120000 		.long	0x12aa
 619 037c 00       		.byte	0
 620 037d 0D       		.uleb128 0xd
 621 037e 00000000 		.long	.LASF22
 622 0382 06       		.byte	0x6
 623 0383 3801     		.value	0x138
 624 0385 00000000 		.long	.LASF23
 625 0389 B0120000 		.long	0x12b0
 626 038d 9C030000 		.long	0x39c
 627 0391 0A       		.uleb128 0xa
 628 0392 C3120000 		.long	0x12c3
 629 0396 0A       		.uleb128 0xa
 630 0397 C3120000 		.long	0x12c3
 631 039b 00       		.byte	0
 632 039c 0E       		.uleb128 0xe
 633 039d 656F6600 		.string	"eof"
 634 03a1 06       		.byte	0x6
 635 03a2 3C01     		.value	0x13c
 636 03a4 00000000 		.long	.LASF271
 637 03a8 12020000 		.long	0x212
 638 03ac 0F       		.uleb128 0xf
 639 03ad 00000000 		.long	.LASF24
 640 03b1 06       		.byte	0x6
 641 03b2 4001     		.value	0x140
 642 03b4 00000000 		.long	.LASF272
 643 03b8 12020000 		.long	0x212
 644 03bc 0A       		.uleb128 0xa
 645 03bd C3120000 		.long	0x12c3
 646 03c1 00       		.byte	0
 647 03c2 00       		.byte	0
 648 03c3 08       		.uleb128 0x8
 649 03c4 00000000 		.long	.LASF25
 650 03c8 04       		.byte	0x4
 651 03c9 C4       		.byte	0xc4
 652 03ca 210A0000 		.long	0xa21
 653 03ce 05       		.uleb128 0x5
 654 03cf 07       		.byte	0x7
 655 03d0 35       		.byte	0x35
 656 03d1 C9120000 		.long	0x12c9
 657 03d5 05       		.uleb128 0x5
GAS LISTING /tmp/cceirXUd.s 			page 15


 658 03d6 07       		.byte	0x7
 659 03d7 36       		.byte	0x36
 660 03d8 F6130000 		.long	0x13f6
 661 03dc 05       		.uleb128 0x5
 662 03dd 07       		.byte	0x7
 663 03de 37       		.byte	0x37
 664 03df 10140000 		.long	0x1410
 665 03e3 08       		.uleb128 0x8
 666 03e4 00000000 		.long	.LASF26
 667 03e8 04       		.byte	0x4
 668 03e9 C5       		.byte	0xc5
 669 03ea 64100000 		.long	0x1064
 670 03ee 10       		.uleb128 0x10
 671 03ef 00000000 		.long	.LASF48
 672 03f3 04       		.byte	0x4
 673 03f4 8F0A0000 		.long	0xa8f
 674 03f8 08       		.byte	0x8
 675 03f9 39       		.byte	0x39
 676 03fa 8F040000 		.long	0x48f
 677 03fe 11       		.uleb128 0x11
 678 03ff 00000000 		.long	.LASF27
 679 0403 01       		.byte	0x1
 680 0404 11       		.uleb128 0x11
 681 0405 00000000 		.long	.LASF28
 682 0409 02       		.byte	0x2
 683 040a 11       		.uleb128 0x11
 684 040b 00000000 		.long	.LASF29
 685 040f 04       		.byte	0x4
 686 0410 11       		.uleb128 0x11
 687 0411 00000000 		.long	.LASF30
 688 0415 08       		.byte	0x8
 689 0416 11       		.uleb128 0x11
 690 0417 00000000 		.long	.LASF31
 691 041b 10       		.byte	0x10
 692 041c 11       		.uleb128 0x11
 693 041d 00000000 		.long	.LASF32
 694 0421 20       		.byte	0x20
 695 0422 11       		.uleb128 0x11
 696 0423 00000000 		.long	.LASF33
 697 0427 40       		.byte	0x40
 698 0428 11       		.uleb128 0x11
 699 0429 00000000 		.long	.LASF34
 700 042d 80       		.byte	0x80
 701 042e 12       		.uleb128 0x12
 702 042f 00000000 		.long	.LASF35
 703 0433 0001     		.value	0x100
 704 0435 12       		.uleb128 0x12
 705 0436 00000000 		.long	.LASF36
 706 043a 0002     		.value	0x200
 707 043c 12       		.uleb128 0x12
 708 043d 00000000 		.long	.LASF37
 709 0441 0004     		.value	0x400
 710 0443 12       		.uleb128 0x12
 711 0444 00000000 		.long	.LASF38
 712 0448 0008     		.value	0x800
 713 044a 12       		.uleb128 0x12
 714 044b 00000000 		.long	.LASF39
GAS LISTING /tmp/cceirXUd.s 			page 16


 715 044f 0010     		.value	0x1000
 716 0451 12       		.uleb128 0x12
 717 0452 00000000 		.long	.LASF40
 718 0456 0020     		.value	0x2000
 719 0458 12       		.uleb128 0x12
 720 0459 00000000 		.long	.LASF41
 721 045d 0040     		.value	0x4000
 722 045f 11       		.uleb128 0x11
 723 0460 00000000 		.long	.LASF42
 724 0464 B0       		.byte	0xb0
 725 0465 11       		.uleb128 0x11
 726 0466 00000000 		.long	.LASF43
 727 046a 4A       		.byte	0x4a
 728 046b 12       		.uleb128 0x12
 729 046c 00000000 		.long	.LASF44
 730 0470 0401     		.value	0x104
 731 0472 13       		.uleb128 0x13
 732 0473 00000000 		.long	.LASF45
 733 0477 00000100 		.long	0x10000
 734 047b 13       		.uleb128 0x13
 735 047c 00000000 		.long	.LASF46
 736 0480 FFFFFF7F 		.long	0x7fffffff
 737 0484 14       		.uleb128 0x14
 738 0485 00000000 		.long	.LASF47
 739 0489 80808080 		.sleb128 -2147483648
 739      78
 740 048e 00       		.byte	0
 741 048f 10       		.uleb128 0x10
 742 0490 00000000 		.long	.LASF49
 743 0494 04       		.byte	0x4
 744 0495 8F0A0000 		.long	0xa8f
 745 0499 08       		.byte	0x8
 746 049a 6F       		.byte	0x6f
 747 049b E0040000 		.long	0x4e0
 748 049f 11       		.uleb128 0x11
 749 04a0 00000000 		.long	.LASF50
 750 04a4 01       		.byte	0x1
 751 04a5 11       		.uleb128 0x11
 752 04a6 00000000 		.long	.LASF51
 753 04aa 02       		.byte	0x2
 754 04ab 11       		.uleb128 0x11
 755 04ac 00000000 		.long	.LASF52
 756 04b0 04       		.byte	0x4
 757 04b1 11       		.uleb128 0x11
 758 04b2 00000000 		.long	.LASF53
 759 04b6 08       		.byte	0x8
 760 04b7 11       		.uleb128 0x11
 761 04b8 00000000 		.long	.LASF54
 762 04bc 10       		.byte	0x10
 763 04bd 11       		.uleb128 0x11
 764 04be 00000000 		.long	.LASF55
 765 04c2 20       		.byte	0x20
 766 04c3 13       		.uleb128 0x13
 767 04c4 00000000 		.long	.LASF56
 768 04c8 00000100 		.long	0x10000
 769 04cc 13       		.uleb128 0x13
 770 04cd 00000000 		.long	.LASF57
GAS LISTING /tmp/cceirXUd.s 			page 17


 771 04d1 FFFFFF7F 		.long	0x7fffffff
 772 04d5 14       		.uleb128 0x14
 773 04d6 00000000 		.long	.LASF58
 774 04da 80808080 		.sleb128 -2147483648
 774      78
 775 04df 00       		.byte	0
 776 04e0 10       		.uleb128 0x10
 777 04e1 00000000 		.long	.LASF59
 778 04e5 04       		.byte	0x4
 779 04e6 8F0A0000 		.long	0xa8f
 780 04ea 08       		.byte	0x8
 781 04eb 99       		.byte	0x99
 782 04ec 25050000 		.long	0x525
 783 04f0 11       		.uleb128 0x11
 784 04f1 00000000 		.long	.LASF60
 785 04f5 00       		.byte	0
 786 04f6 11       		.uleb128 0x11
 787 04f7 00000000 		.long	.LASF61
 788 04fb 01       		.byte	0x1
 789 04fc 11       		.uleb128 0x11
 790 04fd 00000000 		.long	.LASF62
 791 0501 02       		.byte	0x2
 792 0502 11       		.uleb128 0x11
 793 0503 00000000 		.long	.LASF63
 794 0507 04       		.byte	0x4
 795 0508 13       		.uleb128 0x13
 796 0509 00000000 		.long	.LASF64
 797 050d 00000100 		.long	0x10000
 798 0511 13       		.uleb128 0x13
 799 0512 00000000 		.long	.LASF65
 800 0516 FFFFFF7F 		.long	0x7fffffff
 801 051a 14       		.uleb128 0x14
 802 051b 00000000 		.long	.LASF66
 803 051f 80808080 		.sleb128 -2147483648
 803      78
 804 0524 00       		.byte	0
 805 0525 10       		.uleb128 0x10
 806 0526 00000000 		.long	.LASF67
 807 052a 04       		.byte	0x4
 808 052b 0D0A0000 		.long	0xa0d
 809 052f 08       		.byte	0x8
 810 0530 C1       		.byte	0xc1
 811 0531 51050000 		.long	0x551
 812 0535 11       		.uleb128 0x11
 813 0536 00000000 		.long	.LASF68
 814 053a 00       		.byte	0
 815 053b 11       		.uleb128 0x11
 816 053c 00000000 		.long	.LASF69
 817 0540 01       		.byte	0x1
 818 0541 11       		.uleb128 0x11
 819 0542 00000000 		.long	.LASF70
 820 0546 02       		.byte	0x2
 821 0547 13       		.uleb128 0x13
 822 0548 00000000 		.long	.LASF71
 823 054c 00000100 		.long	0x10000
 824 0550 00       		.byte	0
 825 0551 15       		.uleb128 0x15
GAS LISTING /tmp/cceirXUd.s 			page 18


 826 0552 00000000 		.long	.LASF101
 827 0556 BA070000 		.long	0x7ba
 828 055a 16       		.uleb128 0x16
 829 055b 00000000 		.long	.LASF273
 830 055f 01       		.byte	0x1
 831 0560 08       		.byte	0x8
 832 0561 5902     		.value	0x259
 833 0563 01       		.byte	0x1
 834 0564 B8050000 		.long	0x5b8
 835 0568 17       		.uleb128 0x17
 836 0569 00000000 		.long	.LASF72
 837 056d 08       		.byte	0x8
 838 056e 6102     		.value	0x261
 839 0570 2C140000 		.long	0x142c
 840 0574 17       		.uleb128 0x17
 841 0575 00000000 		.long	.LASF73
 842 0579 08       		.byte	0x8
 843 057a 6202     		.value	0x262
 844 057c B0120000 		.long	0x12b0
 845 0580 18       		.uleb128 0x18
 846 0581 00000000 		.long	.LASF273
 847 0585 08       		.byte	0x8
 848 0586 5D02     		.value	0x25d
 849 0588 00000000 		.long	.LASF274
 850 058c 01       		.byte	0x1
 851 058d 95050000 		.long	0x595
 852 0591 9B050000 		.long	0x59b
 853 0595 19       		.uleb128 0x19
 854 0596 41140000 		.long	0x1441
 855 059a 00       		.byte	0
 856 059b 1A       		.uleb128 0x1a
 857 059c 00000000 		.long	.LASF74
 858 05a0 08       		.byte	0x8
 859 05a1 5E02     		.value	0x25e
 860 05a3 00000000 		.long	.LASF75
 861 05a7 01       		.byte	0x1
 862 05a8 AC050000 		.long	0x5ac
 863 05ac 19       		.uleb128 0x19
 864 05ad 41140000 		.long	0x1441
 865 05b1 19       		.uleb128 0x19
 866 05b2 8F0A0000 		.long	0xa8f
 867 05b6 00       		.byte	0
 868 05b7 00       		.byte	0
 869 05b8 1B       		.uleb128 0x1b
 870 05b9 00000000 		.long	.LASF91
 871 05bd 08       		.byte	0x8
 872 05be 4301     		.value	0x143
 873 05c0 EE030000 		.long	0x3ee
 874 05c4 01       		.byte	0x1
 875 05c5 1C       		.uleb128 0x1c
 876 05c6 00000000 		.long	.LASF76
 877 05ca 08       		.byte	0x8
 878 05cb 4601     		.value	0x146
 879 05cd D3050000 		.long	0x5d3
 880 05d1 01       		.byte	0x1
 881 05d2 01       		.byte	0x1
 882 05d3 0B       		.uleb128 0xb
GAS LISTING /tmp/cceirXUd.s 			page 19


 883 05d4 B8050000 		.long	0x5b8
 884 05d8 1D       		.uleb128 0x1d
 885 05d9 64656300 		.string	"dec"
 886 05dd 08       		.byte	0x8
 887 05de 4901     		.value	0x149
 888 05e0 D3050000 		.long	0x5d3
 889 05e4 01       		.byte	0x1
 890 05e5 02       		.byte	0x2
 891 05e6 1C       		.uleb128 0x1c
 892 05e7 00000000 		.long	.LASF77
 893 05eb 08       		.byte	0x8
 894 05ec 4C01     		.value	0x14c
 895 05ee D3050000 		.long	0x5d3
 896 05f2 01       		.byte	0x1
 897 05f3 04       		.byte	0x4
 898 05f4 1D       		.uleb128 0x1d
 899 05f5 68657800 		.string	"hex"
 900 05f9 08       		.byte	0x8
 901 05fa 4F01     		.value	0x14f
 902 05fc D3050000 		.long	0x5d3
 903 0600 01       		.byte	0x1
 904 0601 08       		.byte	0x8
 905 0602 1C       		.uleb128 0x1c
 906 0603 00000000 		.long	.LASF78
 907 0607 08       		.byte	0x8
 908 0608 5401     		.value	0x154
 909 060a D3050000 		.long	0x5d3
 910 060e 01       		.byte	0x1
 911 060f 10       		.byte	0x10
 912 0610 1C       		.uleb128 0x1c
 913 0611 00000000 		.long	.LASF79
 914 0615 08       		.byte	0x8
 915 0616 5801     		.value	0x158
 916 0618 D3050000 		.long	0x5d3
 917 061c 01       		.byte	0x1
 918 061d 20       		.byte	0x20
 919 061e 1D       		.uleb128 0x1d
 920 061f 6F637400 		.string	"oct"
 921 0623 08       		.byte	0x8
 922 0624 5B01     		.value	0x15b
 923 0626 D3050000 		.long	0x5d3
 924 062a 01       		.byte	0x1
 925 062b 40       		.byte	0x40
 926 062c 1C       		.uleb128 0x1c
 927 062d 00000000 		.long	.LASF80
 928 0631 08       		.byte	0x8
 929 0632 5F01     		.value	0x15f
 930 0634 D3050000 		.long	0x5d3
 931 0638 01       		.byte	0x1
 932 0639 80       		.byte	0x80
 933 063a 1E       		.uleb128 0x1e
 934 063b 00000000 		.long	.LASF81
 935 063f 08       		.byte	0x8
 936 0640 6201     		.value	0x162
 937 0642 D3050000 		.long	0x5d3
 938 0646 01       		.byte	0x1
 939 0647 0001     		.value	0x100
GAS LISTING /tmp/cceirXUd.s 			page 20


 940 0649 1E       		.uleb128 0x1e
 941 064a 00000000 		.long	.LASF82
 942 064e 08       		.byte	0x8
 943 064f 6601     		.value	0x166
 944 0651 D3050000 		.long	0x5d3
 945 0655 01       		.byte	0x1
 946 0656 0002     		.value	0x200
 947 0658 1E       		.uleb128 0x1e
 948 0659 00000000 		.long	.LASF83
 949 065d 08       		.byte	0x8
 950 065e 6A01     		.value	0x16a
 951 0660 D3050000 		.long	0x5d3
 952 0664 01       		.byte	0x1
 953 0665 0004     		.value	0x400
 954 0667 1E       		.uleb128 0x1e
 955 0668 00000000 		.long	.LASF84
 956 066c 08       		.byte	0x8
 957 066d 6D01     		.value	0x16d
 958 066f D3050000 		.long	0x5d3
 959 0673 01       		.byte	0x1
 960 0674 0008     		.value	0x800
 961 0676 1E       		.uleb128 0x1e
 962 0677 00000000 		.long	.LASF85
 963 067b 08       		.byte	0x8
 964 067c 7001     		.value	0x170
 965 067e D3050000 		.long	0x5d3
 966 0682 01       		.byte	0x1
 967 0683 0010     		.value	0x1000
 968 0685 1E       		.uleb128 0x1e
 969 0686 00000000 		.long	.LASF86
 970 068a 08       		.byte	0x8
 971 068b 7301     		.value	0x173
 972 068d D3050000 		.long	0x5d3
 973 0691 01       		.byte	0x1
 974 0692 0020     		.value	0x2000
 975 0694 1E       		.uleb128 0x1e
 976 0695 00000000 		.long	.LASF87
 977 0699 08       		.byte	0x8
 978 069a 7701     		.value	0x177
 979 069c D3050000 		.long	0x5d3
 980 06a0 01       		.byte	0x1
 981 06a1 0040     		.value	0x4000
 982 06a3 1C       		.uleb128 0x1c
 983 06a4 00000000 		.long	.LASF88
 984 06a8 08       		.byte	0x8
 985 06a9 7A01     		.value	0x17a
 986 06ab D3050000 		.long	0x5d3
 987 06af 01       		.byte	0x1
 988 06b0 B0       		.byte	0xb0
 989 06b1 1C       		.uleb128 0x1c
 990 06b2 00000000 		.long	.LASF89
 991 06b6 08       		.byte	0x8
 992 06b7 7D01     		.value	0x17d
 993 06b9 D3050000 		.long	0x5d3
 994 06bd 01       		.byte	0x1
 995 06be 4A       		.byte	0x4a
 996 06bf 1E       		.uleb128 0x1e
GAS LISTING /tmp/cceirXUd.s 			page 21


 997 06c0 00000000 		.long	.LASF90
 998 06c4 08       		.byte	0x8
 999 06c5 8001     		.value	0x180
 1000 06c7 D3050000 		.long	0x5d3
 1001 06cb 01       		.byte	0x1
 1002 06cc 0401     		.value	0x104
 1003 06ce 1B       		.uleb128 0x1b
 1004 06cf 00000000 		.long	.LASF92
 1005 06d3 08       		.byte	0x8
 1006 06d4 8E01     		.value	0x18e
 1007 06d6 E0040000 		.long	0x4e0
 1008 06da 01       		.byte	0x1
 1009 06db 1C       		.uleb128 0x1c
 1010 06dc 00000000 		.long	.LASF93
 1011 06e0 08       		.byte	0x8
 1012 06e1 9201     		.value	0x192
 1013 06e3 E9060000 		.long	0x6e9
 1014 06e7 01       		.byte	0x1
 1015 06e8 01       		.byte	0x1
 1016 06e9 0B       		.uleb128 0xb
 1017 06ea CE060000 		.long	0x6ce
 1018 06ee 1C       		.uleb128 0x1c
 1019 06ef 00000000 		.long	.LASF94
 1020 06f3 08       		.byte	0x8
 1021 06f4 9501     		.value	0x195
 1022 06f6 E9060000 		.long	0x6e9
 1023 06fa 01       		.byte	0x1
 1024 06fb 02       		.byte	0x2
 1025 06fc 1C       		.uleb128 0x1c
 1026 06fd 00000000 		.long	.LASF95
 1027 0701 08       		.byte	0x8
 1028 0702 9A01     		.value	0x19a
 1029 0704 E9060000 		.long	0x6e9
 1030 0708 01       		.byte	0x1
 1031 0709 04       		.byte	0x4
 1032 070a 1C       		.uleb128 0x1c
 1033 070b 00000000 		.long	.LASF96
 1034 070f 08       		.byte	0x8
 1035 0710 9D01     		.value	0x19d
 1036 0712 E9060000 		.long	0x6e9
 1037 0716 01       		.byte	0x1
 1038 0717 00       		.byte	0
 1039 0718 1B       		.uleb128 0x1b
 1040 0719 00000000 		.long	.LASF97
 1041 071d 08       		.byte	0x8
 1042 071e AD01     		.value	0x1ad
 1043 0720 8F040000 		.long	0x48f
 1044 0724 01       		.byte	0x1
 1045 0725 1D       		.uleb128 0x1d
 1046 0726 61707000 		.string	"app"
 1047 072a 08       		.byte	0x8
 1048 072b B001     		.value	0x1b0
 1049 072d 33070000 		.long	0x733
 1050 0731 01       		.byte	0x1
 1051 0732 01       		.byte	0x1
 1052 0733 0B       		.uleb128 0xb
 1053 0734 18070000 		.long	0x718
GAS LISTING /tmp/cceirXUd.s 			page 22


 1054 0738 1D       		.uleb128 0x1d
 1055 0739 61746500 		.string	"ate"
 1056 073d 08       		.byte	0x8
 1057 073e B301     		.value	0x1b3
 1058 0740 33070000 		.long	0x733
 1059 0744 01       		.byte	0x1
 1060 0745 02       		.byte	0x2
 1061 0746 1C       		.uleb128 0x1c
 1062 0747 00000000 		.long	.LASF98
 1063 074b 08       		.byte	0x8
 1064 074c B801     		.value	0x1b8
 1065 074e 33070000 		.long	0x733
 1066 0752 01       		.byte	0x1
 1067 0753 04       		.byte	0x4
 1068 0754 1D       		.uleb128 0x1d
 1069 0755 696E00   		.string	"in"
 1070 0758 08       		.byte	0x8
 1071 0759 BB01     		.value	0x1bb
 1072 075b 33070000 		.long	0x733
 1073 075f 01       		.byte	0x1
 1074 0760 08       		.byte	0x8
 1075 0761 1D       		.uleb128 0x1d
 1076 0762 6F757400 		.string	"out"
 1077 0766 08       		.byte	0x8
 1078 0767 BE01     		.value	0x1be
 1079 0769 33070000 		.long	0x733
 1080 076d 01       		.byte	0x1
 1081 076e 10       		.byte	0x10
 1082 076f 1C       		.uleb128 0x1c
 1083 0770 00000000 		.long	.LASF99
 1084 0774 08       		.byte	0x8
 1085 0775 C101     		.value	0x1c1
 1086 0777 33070000 		.long	0x733
 1087 077b 01       		.byte	0x1
 1088 077c 20       		.byte	0x20
 1089 077d 1B       		.uleb128 0x1b
 1090 077e 00000000 		.long	.LASF100
 1091 0782 08       		.byte	0x8
 1092 0783 CD01     		.value	0x1cd
 1093 0785 25050000 		.long	0x525
 1094 0789 01       		.byte	0x1
 1095 078a 1D       		.uleb128 0x1d
 1096 078b 62656700 		.string	"beg"
 1097 078f 08       		.byte	0x8
 1098 0790 D001     		.value	0x1d0
 1099 0792 98070000 		.long	0x798
 1100 0796 01       		.byte	0x1
 1101 0797 00       		.byte	0
 1102 0798 0B       		.uleb128 0xb
 1103 0799 7D070000 		.long	0x77d
 1104 079d 1D       		.uleb128 0x1d
 1105 079e 63757200 		.string	"cur"
 1106 07a2 08       		.byte	0x8
 1107 07a3 D301     		.value	0x1d3
 1108 07a5 98070000 		.long	0x798
 1109 07a9 01       		.byte	0x1
 1110 07aa 01       		.byte	0x1
GAS LISTING /tmp/cceirXUd.s 			page 23


 1111 07ab 1D       		.uleb128 0x1d
 1112 07ac 656E6400 		.string	"end"
 1113 07b0 08       		.byte	0x8
 1114 07b1 D601     		.value	0x1d6
 1115 07b3 98070000 		.long	0x798
 1116 07b7 01       		.byte	0x1
 1117 07b8 02       		.byte	0x2
 1118 07b9 00       		.byte	0
 1119 07ba 05       		.uleb128 0x5
 1120 07bb 09       		.byte	0x9
 1121 07bc 52       		.byte	0x52
 1122 07bd 52140000 		.long	0x1452
 1123 07c1 05       		.uleb128 0x5
 1124 07c2 09       		.byte	0x9
 1125 07c3 53       		.byte	0x53
 1126 07c4 47140000 		.long	0x1447
 1127 07c8 05       		.uleb128 0x5
 1128 07c9 09       		.byte	0x9
 1129 07ca 54       		.byte	0x54
 1130 07cb 280A0000 		.long	0xa28
 1131 07cf 05       		.uleb128 0x5
 1132 07d0 09       		.byte	0x9
 1133 07d1 5C       		.byte	0x5c
 1134 07d2 68140000 		.long	0x1468
 1135 07d6 05       		.uleb128 0x5
 1136 07d7 09       		.byte	0x9
 1137 07d8 65       		.byte	0x65
 1138 07d9 82140000 		.long	0x1482
 1139 07dd 05       		.uleb128 0x5
 1140 07de 09       		.byte	0x9
 1141 07df 68       		.byte	0x68
 1142 07e0 9C140000 		.long	0x149c
 1143 07e4 05       		.uleb128 0x5
 1144 07e5 09       		.byte	0x9
 1145 07e6 69       		.byte	0x69
 1146 07e7 B1140000 		.long	0x14b1
 1147 07eb 15       		.uleb128 0x15
 1148 07ec 00000000 		.long	.LASF102
 1149 07f0 07080000 		.long	0x807
 1150 07f4 1F       		.uleb128 0x1f
 1151 07f5 00000000 		.long	.LASF112
 1152 07f9 880A0000 		.long	0xa88
 1153 07fd 20       		.uleb128 0x20
 1154 07fe 00000000 		.long	.LASF275
 1155 0802 FB010000 		.long	0x1fb
 1156 0806 00       		.byte	0
 1157 0807 08       		.uleb128 0x8
 1158 0808 00000000 		.long	.LASF103
 1159 080c 0A       		.byte	0xa
 1160 080d 8D       		.byte	0x8d
 1161 080e EB070000 		.long	0x7eb
 1162 0812 21       		.uleb128 0x21
 1163 0813 00000000 		.long	.LASF276
 1164 0817 02       		.byte	0x2
 1165 0818 3D       		.byte	0x3d
 1166 0819 00000000 		.long	.LASF277
 1167 081d 07080000 		.long	0x807
GAS LISTING /tmp/cceirXUd.s 			page 24


 1168 0821 22       		.uleb128 0x22
 1169 0822 00000000 		.long	.LASF252
 1170 0826 02       		.byte	0x2
 1171 0827 4A       		.byte	0x4a
 1172 0828 5A050000 		.long	0x55a
 1173 082c 00       		.byte	0
 1174 082d 23       		.uleb128 0x23
 1175 082e 00000000 		.long	.LASF104
 1176 0832 04       		.byte	0x4
 1177 0833 DD       		.byte	0xdd
 1178 0834 B9090000 		.long	0x9b9
 1179 0838 03       		.uleb128 0x3
 1180 0839 00000000 		.long	.LASF0
 1181 083d 04       		.byte	0x4
 1182 083e DE       		.byte	0xde
 1183 083f 04       		.uleb128 0x4
 1184 0840 04       		.byte	0x4
 1185 0841 DE       		.byte	0xde
 1186 0842 38080000 		.long	0x838
 1187 0846 05       		.uleb128 0x5
 1188 0847 03       		.byte	0x3
 1189 0848 F8       		.byte	0xf8
 1190 0849 0C120000 		.long	0x120c
 1191 084d 06       		.uleb128 0x6
 1192 084e 03       		.byte	0x3
 1193 084f 0101     		.value	0x101
 1194 0851 2E120000 		.long	0x122e
 1195 0855 06       		.uleb128 0x6
 1196 0856 03       		.byte	0x3
 1197 0857 0201     		.value	0x102
 1198 0859 55120000 		.long	0x1255
 1199 085d 03       		.uleb128 0x3
 1200 085e 00000000 		.long	.LASF105
 1201 0862 0B       		.byte	0xb
 1202 0863 24       		.byte	0x24
 1203 0864 05       		.uleb128 0x5
 1204 0865 0C       		.byte	0xc
 1205 0866 2C       		.byte	0x2c
 1206 0867 C3030000 		.long	0x3c3
 1207 086b 05       		.uleb128 0x5
 1208 086c 0C       		.byte	0xc
 1209 086d 2D       		.byte	0x2d
 1210 086e E3030000 		.long	0x3e3
 1211 0872 07       		.uleb128 0x7
 1212 0873 00000000 		.long	.LASF107
 1213 0877 01       		.byte	0x1
 1214 0878 0D       		.byte	0xd
 1215 0879 37       		.byte	0x37
 1216 087a B4080000 		.long	0x8b4
 1217 087e 24       		.uleb128 0x24
 1218 087f 00000000 		.long	.LASF108
 1219 0883 0D       		.byte	0xd
 1220 0884 3A       		.byte	0x3a
 1221 0885 B30A0000 		.long	0xab3
 1222 0889 24       		.uleb128 0x24
 1223 088a 00000000 		.long	.LASF109
 1224 088e 0D       		.byte	0xd
GAS LISTING /tmp/cceirXUd.s 			page 25


 1225 088f 3B       		.byte	0x3b
 1226 0890 B30A0000 		.long	0xab3
 1227 0894 24       		.uleb128 0x24
 1228 0895 00000000 		.long	.LASF110
 1229 0899 0D       		.byte	0xd
 1230 089a 3F       		.byte	0x3f
 1231 089b 37140000 		.long	0x1437
 1232 089f 24       		.uleb128 0x24
 1233 08a0 00000000 		.long	.LASF111
 1234 08a4 0D       		.byte	0xd
 1235 08a5 40       		.byte	0x40
 1236 08a6 B30A0000 		.long	0xab3
 1237 08aa 1F       		.uleb128 0x1f
 1238 08ab 00000000 		.long	.LASF113
 1239 08af 8F0A0000 		.long	0xa8f
 1240 08b3 00       		.byte	0
 1241 08b4 07       		.uleb128 0x7
 1242 08b5 00000000 		.long	.LASF114
 1243 08b9 01       		.byte	0x1
 1244 08ba 0D       		.byte	0xd
 1245 08bb 37       		.byte	0x37
 1246 08bc F6080000 		.long	0x8f6
 1247 08c0 24       		.uleb128 0x24
 1248 08c1 00000000 		.long	.LASF108
 1249 08c5 0D       		.byte	0xd
 1250 08c6 3A       		.byte	0x3a
 1251 08c7 3C140000 		.long	0x143c
 1252 08cb 24       		.uleb128 0x24
 1253 08cc 00000000 		.long	.LASF109
 1254 08d0 0D       		.byte	0xd
 1255 08d1 3B       		.byte	0x3b
 1256 08d2 3C140000 		.long	0x143c
 1257 08d6 24       		.uleb128 0x24
 1258 08d7 00000000 		.long	.LASF110
 1259 08db 0D       		.byte	0xd
 1260 08dc 3F       		.byte	0x3f
 1261 08dd 37140000 		.long	0x1437
 1262 08e1 24       		.uleb128 0x24
 1263 08e2 00000000 		.long	.LASF111
 1264 08e6 0D       		.byte	0xd
 1265 08e7 40       		.byte	0x40
 1266 08e8 B30A0000 		.long	0xab3
 1267 08ec 1F       		.uleb128 0x1f
 1268 08ed 00000000 		.long	.LASF113
 1269 08f1 210A0000 		.long	0xa21
 1270 08f5 00       		.byte	0
 1271 08f6 07       		.uleb128 0x7
 1272 08f7 00000000 		.long	.LASF115
 1273 08fb 01       		.byte	0x1
 1274 08fc 0D       		.byte	0xd
 1275 08fd 37       		.byte	0x37
 1276 08fe 38090000 		.long	0x938
 1277 0902 24       		.uleb128 0x24
 1278 0903 00000000 		.long	.LASF108
 1279 0907 0D       		.byte	0xd
 1280 0908 3A       		.byte	0x3a
 1281 0909 BE0A0000 		.long	0xabe
GAS LISTING /tmp/cceirXUd.s 			page 26


 1282 090d 24       		.uleb128 0x24
 1283 090e 00000000 		.long	.LASF109
 1284 0912 0D       		.byte	0xd
 1285 0913 3B       		.byte	0x3b
 1286 0914 BE0A0000 		.long	0xabe
 1287 0918 24       		.uleb128 0x24
 1288 0919 00000000 		.long	.LASF110
 1289 091d 0D       		.byte	0xd
 1290 091e 3F       		.byte	0x3f
 1291 091f 37140000 		.long	0x1437
 1292 0923 24       		.uleb128 0x24
 1293 0924 00000000 		.long	.LASF111
 1294 0928 0D       		.byte	0xd
 1295 0929 40       		.byte	0x40
 1296 092a B30A0000 		.long	0xab3
 1297 092e 1F       		.uleb128 0x1f
 1298 092f 00000000 		.long	.LASF113
 1299 0933 880A0000 		.long	0xa88
 1300 0937 00       		.byte	0
 1301 0938 07       		.uleb128 0x7
 1302 0939 00000000 		.long	.LASF116
 1303 093d 01       		.byte	0x1
 1304 093e 0D       		.byte	0xd
 1305 093f 37       		.byte	0x37
 1306 0940 7A090000 		.long	0x97a
 1307 0944 24       		.uleb128 0x24
 1308 0945 00000000 		.long	.LASF108
 1309 0949 0D       		.byte	0xd
 1310 094a 3A       		.byte	0x3a
 1311 094b C6140000 		.long	0x14c6
 1312 094f 24       		.uleb128 0x24
 1313 0950 00000000 		.long	.LASF109
 1314 0954 0D       		.byte	0xd
 1315 0955 3B       		.byte	0x3b
 1316 0956 C6140000 		.long	0x14c6
 1317 095a 24       		.uleb128 0x24
 1318 095b 00000000 		.long	.LASF110
 1319 095f 0D       		.byte	0xd
 1320 0960 3F       		.byte	0x3f
 1321 0961 37140000 		.long	0x1437
 1322 0965 24       		.uleb128 0x24
 1323 0966 00000000 		.long	.LASF111
 1324 096a 0D       		.byte	0xd
 1325 096b 40       		.byte	0x40
 1326 096c B30A0000 		.long	0xab3
 1327 0970 1F       		.uleb128 0x1f
 1328 0971 00000000 		.long	.LASF113
 1329 0975 8A120000 		.long	0x128a
 1330 0979 00       		.byte	0
 1331 097a 25       		.uleb128 0x25
 1332 097b 00000000 		.long	.LASF278
 1333 097f 01       		.byte	0x1
 1334 0980 0D       		.byte	0xd
 1335 0981 37       		.byte	0x37
 1336 0982 24       		.uleb128 0x24
 1337 0983 00000000 		.long	.LASF108
 1338 0987 0D       		.byte	0xd
GAS LISTING /tmp/cceirXUd.s 			page 27


 1339 0988 3A       		.byte	0x3a
 1340 0989 CB140000 		.long	0x14cb
 1341 098d 24       		.uleb128 0x24
 1342 098e 00000000 		.long	.LASF109
 1343 0992 0D       		.byte	0xd
 1344 0993 3B       		.byte	0x3b
 1345 0994 CB140000 		.long	0x14cb
 1346 0998 24       		.uleb128 0x24
 1347 0999 00000000 		.long	.LASF110
 1348 099d 0D       		.byte	0xd
 1349 099e 3F       		.byte	0x3f
 1350 099f 37140000 		.long	0x1437
 1351 09a3 24       		.uleb128 0x24
 1352 09a4 00000000 		.long	.LASF111
 1353 09a8 0D       		.byte	0xd
 1354 09a9 40       		.byte	0x40
 1355 09aa B30A0000 		.long	0xab3
 1356 09ae 1F       		.uleb128 0x1f
 1357 09af 00000000 		.long	.LASF113
 1358 09b3 64100000 		.long	0x1064
 1359 09b7 00       		.byte	0
 1360 09b8 00       		.byte	0
 1361 09b9 26       		.uleb128 0x26
 1362 09ba 00000000 		.long	.LASF279
 1363 09be 08       		.uleb128 0x8
 1364 09bf 00000000 		.long	.LASF117
 1365 09c3 0E       		.byte	0xe
 1366 09c4 40       		.byte	0x40
 1367 09c5 B9090000 		.long	0x9b9
 1368 09c9 27       		.uleb128 0x27
 1369 09ca 08       		.byte	0x8
 1370 09cb 07       		.byte	0x7
 1371 09cc 00000000 		.long	.LASF123
 1372 09d0 07       		.uleb128 0x7
 1373 09d1 00000000 		.long	.LASF118
 1374 09d5 18       		.byte	0x18
 1375 09d6 0F       		.byte	0xf
 1376 09d7 00       		.byte	0
 1377 09d8 0D0A0000 		.long	0xa0d
 1378 09dc 28       		.uleb128 0x28
 1379 09dd 00000000 		.long	.LASF119
 1380 09e1 0F       		.byte	0xf
 1381 09e2 00       		.byte	0
 1382 09e3 0D0A0000 		.long	0xa0d
 1383 09e7 00       		.byte	0
 1384 09e8 28       		.uleb128 0x28
 1385 09e9 00000000 		.long	.LASF120
 1386 09ed 0F       		.byte	0xf
 1387 09ee 00       		.byte	0
 1388 09ef 0D0A0000 		.long	0xa0d
 1389 09f3 04       		.byte	0x4
 1390 09f4 28       		.uleb128 0x28
 1391 09f5 00000000 		.long	.LASF121
 1392 09f9 0F       		.byte	0xf
 1393 09fa 00       		.byte	0
 1394 09fb 140A0000 		.long	0xa14
 1395 09ff 08       		.byte	0x8
GAS LISTING /tmp/cceirXUd.s 			page 28


 1396 0a00 28       		.uleb128 0x28
 1397 0a01 00000000 		.long	.LASF122
 1398 0a05 0F       		.byte	0xf
 1399 0a06 00       		.byte	0
 1400 0a07 140A0000 		.long	0xa14
 1401 0a0b 10       		.byte	0x10
 1402 0a0c 00       		.byte	0
 1403 0a0d 27       		.uleb128 0x27
 1404 0a0e 04       		.byte	0x4
 1405 0a0f 07       		.byte	0x7
 1406 0a10 00000000 		.long	.LASF124
 1407 0a14 29       		.uleb128 0x29
 1408 0a15 08       		.byte	0x8
 1409 0a16 08       		.uleb128 0x8
 1410 0a17 00000000 		.long	.LASF25
 1411 0a1b 10       		.byte	0x10
 1412 0a1c D8       		.byte	0xd8
 1413 0a1d 210A0000 		.long	0xa21
 1414 0a21 27       		.uleb128 0x27
 1415 0a22 08       		.byte	0x8
 1416 0a23 07       		.byte	0x7
 1417 0a24 00000000 		.long	.LASF125
 1418 0a28 2A       		.uleb128 0x2a
 1419 0a29 00000000 		.long	.LASF126
 1420 0a2d 10       		.byte	0x10
 1421 0a2e 6501     		.value	0x165
 1422 0a30 0D0A0000 		.long	0xa0d
 1423 0a34 2B       		.uleb128 0x2b
 1424 0a35 08       		.byte	0x8
 1425 0a36 11       		.byte	0x11
 1426 0a37 53       		.byte	0x53
 1427 0a38 00000000 		.long	.LASF280
 1428 0a3c 780A0000 		.long	0xa78
 1429 0a40 2C       		.uleb128 0x2c
 1430 0a41 04       		.byte	0x4
 1431 0a42 11       		.byte	0x11
 1432 0a43 56       		.byte	0x56
 1433 0a44 5F0A0000 		.long	0xa5f
 1434 0a48 2D       		.uleb128 0x2d
 1435 0a49 00000000 		.long	.LASF127
 1436 0a4d 11       		.byte	0x11
 1437 0a4e 58       		.byte	0x58
 1438 0a4f 0D0A0000 		.long	0xa0d
 1439 0a53 2D       		.uleb128 0x2d
 1440 0a54 00000000 		.long	.LASF128
 1441 0a58 11       		.byte	0x11
 1442 0a59 5C       		.byte	0x5c
 1443 0a5a 780A0000 		.long	0xa78
 1444 0a5e 00       		.byte	0
 1445 0a5f 28       		.uleb128 0x28
 1446 0a60 00000000 		.long	.LASF129
 1447 0a64 11       		.byte	0x11
 1448 0a65 54       		.byte	0x54
 1449 0a66 8F0A0000 		.long	0xa8f
 1450 0a6a 00       		.byte	0
 1451 0a6b 28       		.uleb128 0x28
 1452 0a6c 00000000 		.long	.LASF130
GAS LISTING /tmp/cceirXUd.s 			page 29


 1453 0a70 11       		.byte	0x11
 1454 0a71 5D       		.byte	0x5d
 1455 0a72 400A0000 		.long	0xa40
 1456 0a76 04       		.byte	0x4
 1457 0a77 00       		.byte	0
 1458 0a78 2E       		.uleb128 0x2e
 1459 0a79 880A0000 		.long	0xa88
 1460 0a7d 880A0000 		.long	0xa88
 1461 0a81 2F       		.uleb128 0x2f
 1462 0a82 C9090000 		.long	0x9c9
 1463 0a86 03       		.byte	0x3
 1464 0a87 00       		.byte	0
 1465 0a88 27       		.uleb128 0x27
 1466 0a89 01       		.byte	0x1
 1467 0a8a 06       		.byte	0x6
 1468 0a8b 00000000 		.long	.LASF131
 1469 0a8f 30       		.uleb128 0x30
 1470 0a90 04       		.byte	0x4
 1471 0a91 05       		.byte	0x5
 1472 0a92 696E7400 		.string	"int"
 1473 0a96 08       		.uleb128 0x8
 1474 0a97 00000000 		.long	.LASF132
 1475 0a9b 11       		.byte	0x11
 1476 0a9c 5E       		.byte	0x5e
 1477 0a9d 340A0000 		.long	0xa34
 1478 0aa1 08       		.uleb128 0x8
 1479 0aa2 00000000 		.long	.LASF133
 1480 0aa6 11       		.byte	0x11
 1481 0aa7 6A       		.byte	0x6a
 1482 0aa8 960A0000 		.long	0xa96
 1483 0aac 27       		.uleb128 0x27
 1484 0aad 02       		.byte	0x2
 1485 0aae 07       		.byte	0x7
 1486 0aaf 00000000 		.long	.LASF134
 1487 0ab3 0B       		.uleb128 0xb
 1488 0ab4 8F0A0000 		.long	0xa8f
 1489 0ab8 31       		.uleb128 0x31
 1490 0ab9 08       		.byte	0x8
 1491 0aba BE0A0000 		.long	0xabe
 1492 0abe 0B       		.uleb128 0xb
 1493 0abf 880A0000 		.long	0xa88
 1494 0ac3 32       		.uleb128 0x32
 1495 0ac4 00000000 		.long	.LASF135
 1496 0ac8 11       		.byte	0x11
 1497 0ac9 6401     		.value	0x164
 1498 0acb 280A0000 		.long	0xa28
 1499 0acf D90A0000 		.long	0xad9
 1500 0ad3 0A       		.uleb128 0xa
 1501 0ad4 8F0A0000 		.long	0xa8f
 1502 0ad8 00       		.byte	0
 1503 0ad9 32       		.uleb128 0x32
 1504 0ada 00000000 		.long	.LASF136
 1505 0ade 11       		.byte	0x11
 1506 0adf EC02     		.value	0x2ec
 1507 0ae1 280A0000 		.long	0xa28
 1508 0ae5 EF0A0000 		.long	0xaef
 1509 0ae9 0A       		.uleb128 0xa
GAS LISTING /tmp/cceirXUd.s 			page 30


 1510 0aea EF0A0000 		.long	0xaef
 1511 0aee 00       		.byte	0
 1512 0aef 31       		.uleb128 0x31
 1513 0af0 08       		.byte	0x8
 1514 0af1 BE090000 		.long	0x9be
 1515 0af5 32       		.uleb128 0x32
 1516 0af6 00000000 		.long	.LASF137
 1517 0afa 11       		.byte	0x11
 1518 0afb 0903     		.value	0x309
 1519 0afd 150B0000 		.long	0xb15
 1520 0b01 150B0000 		.long	0xb15
 1521 0b05 0A       		.uleb128 0xa
 1522 0b06 150B0000 		.long	0xb15
 1523 0b0a 0A       		.uleb128 0xa
 1524 0b0b 8F0A0000 		.long	0xa8f
 1525 0b0f 0A       		.uleb128 0xa
 1526 0b10 EF0A0000 		.long	0xaef
 1527 0b14 00       		.byte	0
 1528 0b15 31       		.uleb128 0x31
 1529 0b16 08       		.byte	0x8
 1530 0b17 1B0B0000 		.long	0xb1b
 1531 0b1b 27       		.uleb128 0x27
 1532 0b1c 04       		.byte	0x4
 1533 0b1d 05       		.byte	0x5
 1534 0b1e 00000000 		.long	.LASF138
 1535 0b22 32       		.uleb128 0x32
 1536 0b23 00000000 		.long	.LASF139
 1537 0b27 11       		.byte	0x11
 1538 0b28 FA02     		.value	0x2fa
 1539 0b2a 280A0000 		.long	0xa28
 1540 0b2e 3D0B0000 		.long	0xb3d
 1541 0b32 0A       		.uleb128 0xa
 1542 0b33 1B0B0000 		.long	0xb1b
 1543 0b37 0A       		.uleb128 0xa
 1544 0b38 EF0A0000 		.long	0xaef
 1545 0b3c 00       		.byte	0
 1546 0b3d 32       		.uleb128 0x32
 1547 0b3e 00000000 		.long	.LASF140
 1548 0b42 11       		.byte	0x11
 1549 0b43 1003     		.value	0x310
 1550 0b45 8F0A0000 		.long	0xa8f
 1551 0b49 580B0000 		.long	0xb58
 1552 0b4d 0A       		.uleb128 0xa
 1553 0b4e 580B0000 		.long	0xb58
 1554 0b52 0A       		.uleb128 0xa
 1555 0b53 EF0A0000 		.long	0xaef
 1556 0b57 00       		.byte	0
 1557 0b58 31       		.uleb128 0x31
 1558 0b59 08       		.byte	0x8
 1559 0b5a 5E0B0000 		.long	0xb5e
 1560 0b5e 0B       		.uleb128 0xb
 1561 0b5f 1B0B0000 		.long	0xb1b
 1562 0b63 32       		.uleb128 0x32
 1563 0b64 00000000 		.long	.LASF141
 1564 0b68 11       		.byte	0x11
 1565 0b69 4E02     		.value	0x24e
 1566 0b6b 8F0A0000 		.long	0xa8f
GAS LISTING /tmp/cceirXUd.s 			page 31


 1567 0b6f 7E0B0000 		.long	0xb7e
 1568 0b73 0A       		.uleb128 0xa
 1569 0b74 EF0A0000 		.long	0xaef
 1570 0b78 0A       		.uleb128 0xa
 1571 0b79 8F0A0000 		.long	0xa8f
 1572 0b7d 00       		.byte	0
 1573 0b7e 32       		.uleb128 0x32
 1574 0b7f 00000000 		.long	.LASF142
 1575 0b83 11       		.byte	0x11
 1576 0b84 5502     		.value	0x255
 1577 0b86 8F0A0000 		.long	0xa8f
 1578 0b8a 9A0B0000 		.long	0xb9a
 1579 0b8e 0A       		.uleb128 0xa
 1580 0b8f EF0A0000 		.long	0xaef
 1581 0b93 0A       		.uleb128 0xa
 1582 0b94 580B0000 		.long	0xb58
 1583 0b98 33       		.uleb128 0x33
 1584 0b99 00       		.byte	0
 1585 0b9a 32       		.uleb128 0x32
 1586 0b9b 00000000 		.long	.LASF143
 1587 0b9f 11       		.byte	0x11
 1588 0ba0 7E02     		.value	0x27e
 1589 0ba2 8F0A0000 		.long	0xa8f
 1590 0ba6 B60B0000 		.long	0xbb6
 1591 0baa 0A       		.uleb128 0xa
 1592 0bab EF0A0000 		.long	0xaef
 1593 0baf 0A       		.uleb128 0xa
 1594 0bb0 580B0000 		.long	0xb58
 1595 0bb4 33       		.uleb128 0x33
 1596 0bb5 00       		.byte	0
 1597 0bb6 32       		.uleb128 0x32
 1598 0bb7 00000000 		.long	.LASF144
 1599 0bbb 11       		.byte	0x11
 1600 0bbc ED02     		.value	0x2ed
 1601 0bbe 280A0000 		.long	0xa28
 1602 0bc2 CC0B0000 		.long	0xbcc
 1603 0bc6 0A       		.uleb128 0xa
 1604 0bc7 EF0A0000 		.long	0xaef
 1605 0bcb 00       		.byte	0
 1606 0bcc 34       		.uleb128 0x34
 1607 0bcd 00000000 		.long	.LASF242
 1608 0bd1 11       		.byte	0x11
 1609 0bd2 F302     		.value	0x2f3
 1610 0bd4 280A0000 		.long	0xa28
 1611 0bd8 32       		.uleb128 0x32
 1612 0bd9 00000000 		.long	.LASF145
 1613 0bdd 11       		.byte	0x11
 1614 0bde 7B01     		.value	0x17b
 1615 0be0 160A0000 		.long	0xa16
 1616 0be4 F80B0000 		.long	0xbf8
 1617 0be8 0A       		.uleb128 0xa
 1618 0be9 B80A0000 		.long	0xab8
 1619 0bed 0A       		.uleb128 0xa
 1620 0bee 160A0000 		.long	0xa16
 1621 0bf2 0A       		.uleb128 0xa
 1622 0bf3 F80B0000 		.long	0xbf8
 1623 0bf7 00       		.byte	0
GAS LISTING /tmp/cceirXUd.s 			page 32


 1624 0bf8 31       		.uleb128 0x31
 1625 0bf9 08       		.byte	0x8
 1626 0bfa A10A0000 		.long	0xaa1
 1627 0bfe 32       		.uleb128 0x32
 1628 0bff 00000000 		.long	.LASF146
 1629 0c03 11       		.byte	0x11
 1630 0c04 7001     		.value	0x170
 1631 0c06 160A0000 		.long	0xa16
 1632 0c0a 230C0000 		.long	0xc23
 1633 0c0e 0A       		.uleb128 0xa
 1634 0c0f 150B0000 		.long	0xb15
 1635 0c13 0A       		.uleb128 0xa
 1636 0c14 B80A0000 		.long	0xab8
 1637 0c18 0A       		.uleb128 0xa
 1638 0c19 160A0000 		.long	0xa16
 1639 0c1d 0A       		.uleb128 0xa
 1640 0c1e F80B0000 		.long	0xbf8
 1641 0c22 00       		.byte	0
 1642 0c23 32       		.uleb128 0x32
 1643 0c24 00000000 		.long	.LASF147
 1644 0c28 11       		.byte	0x11
 1645 0c29 6C01     		.value	0x16c
 1646 0c2b 8F0A0000 		.long	0xa8f
 1647 0c2f 390C0000 		.long	0xc39
 1648 0c33 0A       		.uleb128 0xa
 1649 0c34 390C0000 		.long	0xc39
 1650 0c38 00       		.byte	0
 1651 0c39 31       		.uleb128 0x31
 1652 0c3a 08       		.byte	0x8
 1653 0c3b 3F0C0000 		.long	0xc3f
 1654 0c3f 0B       		.uleb128 0xb
 1655 0c40 A10A0000 		.long	0xaa1
 1656 0c44 32       		.uleb128 0x32
 1657 0c45 00000000 		.long	.LASF148
 1658 0c49 11       		.byte	0x11
 1659 0c4a 9B01     		.value	0x19b
 1660 0c4c 160A0000 		.long	0xa16
 1661 0c50 690C0000 		.long	0xc69
 1662 0c54 0A       		.uleb128 0xa
 1663 0c55 150B0000 		.long	0xb15
 1664 0c59 0A       		.uleb128 0xa
 1665 0c5a 690C0000 		.long	0xc69
 1666 0c5e 0A       		.uleb128 0xa
 1667 0c5f 160A0000 		.long	0xa16
 1668 0c63 0A       		.uleb128 0xa
 1669 0c64 F80B0000 		.long	0xbf8
 1670 0c68 00       		.byte	0
 1671 0c69 31       		.uleb128 0x31
 1672 0c6a 08       		.byte	0x8
 1673 0c6b B80A0000 		.long	0xab8
 1674 0c6f 32       		.uleb128 0x32
 1675 0c70 00000000 		.long	.LASF149
 1676 0c74 11       		.byte	0x11
 1677 0c75 FB02     		.value	0x2fb
 1678 0c77 280A0000 		.long	0xa28
 1679 0c7b 8A0C0000 		.long	0xc8a
 1680 0c7f 0A       		.uleb128 0xa
GAS LISTING /tmp/cceirXUd.s 			page 33


 1681 0c80 1B0B0000 		.long	0xb1b
 1682 0c84 0A       		.uleb128 0xa
 1683 0c85 EF0A0000 		.long	0xaef
 1684 0c89 00       		.byte	0
 1685 0c8a 32       		.uleb128 0x32
 1686 0c8b 00000000 		.long	.LASF150
 1687 0c8f 11       		.byte	0x11
 1688 0c90 0103     		.value	0x301
 1689 0c92 280A0000 		.long	0xa28
 1690 0c96 A00C0000 		.long	0xca0
 1691 0c9a 0A       		.uleb128 0xa
 1692 0c9b 1B0B0000 		.long	0xb1b
 1693 0c9f 00       		.byte	0
 1694 0ca0 32       		.uleb128 0x32
 1695 0ca1 00000000 		.long	.LASF151
 1696 0ca5 11       		.byte	0x11
 1697 0ca6 5F02     		.value	0x25f
 1698 0ca8 8F0A0000 		.long	0xa8f
 1699 0cac C10C0000 		.long	0xcc1
 1700 0cb0 0A       		.uleb128 0xa
 1701 0cb1 150B0000 		.long	0xb15
 1702 0cb5 0A       		.uleb128 0xa
 1703 0cb6 160A0000 		.long	0xa16
 1704 0cba 0A       		.uleb128 0xa
 1705 0cbb 580B0000 		.long	0xb58
 1706 0cbf 33       		.uleb128 0x33
 1707 0cc0 00       		.byte	0
 1708 0cc1 32       		.uleb128 0x32
 1709 0cc2 00000000 		.long	.LASF152
 1710 0cc6 11       		.byte	0x11
 1711 0cc7 8802     		.value	0x288
 1712 0cc9 8F0A0000 		.long	0xa8f
 1713 0ccd DD0C0000 		.long	0xcdd
 1714 0cd1 0A       		.uleb128 0xa
 1715 0cd2 580B0000 		.long	0xb58
 1716 0cd6 0A       		.uleb128 0xa
 1717 0cd7 580B0000 		.long	0xb58
 1718 0cdb 33       		.uleb128 0x33
 1719 0cdc 00       		.byte	0
 1720 0cdd 32       		.uleb128 0x32
 1721 0cde 00000000 		.long	.LASF153
 1722 0ce2 11       		.byte	0x11
 1723 0ce3 1803     		.value	0x318
 1724 0ce5 280A0000 		.long	0xa28
 1725 0ce9 F80C0000 		.long	0xcf8
 1726 0ced 0A       		.uleb128 0xa
 1727 0cee 280A0000 		.long	0xa28
 1728 0cf2 0A       		.uleb128 0xa
 1729 0cf3 EF0A0000 		.long	0xaef
 1730 0cf7 00       		.byte	0
 1731 0cf8 32       		.uleb128 0x32
 1732 0cf9 00000000 		.long	.LASF154
 1733 0cfd 11       		.byte	0x11
 1734 0cfe 6702     		.value	0x267
 1735 0d00 8F0A0000 		.long	0xa8f
 1736 0d04 180D0000 		.long	0xd18
 1737 0d08 0A       		.uleb128 0xa
GAS LISTING /tmp/cceirXUd.s 			page 34


 1738 0d09 EF0A0000 		.long	0xaef
 1739 0d0d 0A       		.uleb128 0xa
 1740 0d0e 580B0000 		.long	0xb58
 1741 0d12 0A       		.uleb128 0xa
 1742 0d13 180D0000 		.long	0xd18
 1743 0d17 00       		.byte	0
 1744 0d18 31       		.uleb128 0x31
 1745 0d19 08       		.byte	0x8
 1746 0d1a D0090000 		.long	0x9d0
 1747 0d1e 32       		.uleb128 0x32
 1748 0d1f 00000000 		.long	.LASF155
 1749 0d23 11       		.byte	0x11
 1750 0d24 B402     		.value	0x2b4
 1751 0d26 8F0A0000 		.long	0xa8f
 1752 0d2a 3E0D0000 		.long	0xd3e
 1753 0d2e 0A       		.uleb128 0xa
 1754 0d2f EF0A0000 		.long	0xaef
 1755 0d33 0A       		.uleb128 0xa
 1756 0d34 580B0000 		.long	0xb58
 1757 0d38 0A       		.uleb128 0xa
 1758 0d39 180D0000 		.long	0xd18
 1759 0d3d 00       		.byte	0
 1760 0d3e 32       		.uleb128 0x32
 1761 0d3f 00000000 		.long	.LASF156
 1762 0d43 11       		.byte	0x11
 1763 0d44 7402     		.value	0x274
 1764 0d46 8F0A0000 		.long	0xa8f
 1765 0d4a 630D0000 		.long	0xd63
 1766 0d4e 0A       		.uleb128 0xa
 1767 0d4f 150B0000 		.long	0xb15
 1768 0d53 0A       		.uleb128 0xa
 1769 0d54 160A0000 		.long	0xa16
 1770 0d58 0A       		.uleb128 0xa
 1771 0d59 580B0000 		.long	0xb58
 1772 0d5d 0A       		.uleb128 0xa
 1773 0d5e 180D0000 		.long	0xd18
 1774 0d62 00       		.byte	0
 1775 0d63 32       		.uleb128 0x32
 1776 0d64 00000000 		.long	.LASF157
 1777 0d68 11       		.byte	0x11
 1778 0d69 C002     		.value	0x2c0
 1779 0d6b 8F0A0000 		.long	0xa8f
 1780 0d6f 830D0000 		.long	0xd83
 1781 0d73 0A       		.uleb128 0xa
 1782 0d74 580B0000 		.long	0xb58
 1783 0d78 0A       		.uleb128 0xa
 1784 0d79 580B0000 		.long	0xb58
 1785 0d7d 0A       		.uleb128 0xa
 1786 0d7e 180D0000 		.long	0xd18
 1787 0d82 00       		.byte	0
 1788 0d83 32       		.uleb128 0x32
 1789 0d84 00000000 		.long	.LASF158
 1790 0d88 11       		.byte	0x11
 1791 0d89 6F02     		.value	0x26f
 1792 0d8b 8F0A0000 		.long	0xa8f
 1793 0d8f 9E0D0000 		.long	0xd9e
 1794 0d93 0A       		.uleb128 0xa
GAS LISTING /tmp/cceirXUd.s 			page 35


 1795 0d94 580B0000 		.long	0xb58
 1796 0d98 0A       		.uleb128 0xa
 1797 0d99 180D0000 		.long	0xd18
 1798 0d9d 00       		.byte	0
 1799 0d9e 32       		.uleb128 0x32
 1800 0d9f 00000000 		.long	.LASF159
 1801 0da3 11       		.byte	0x11
 1802 0da4 BC02     		.value	0x2bc
 1803 0da6 8F0A0000 		.long	0xa8f
 1804 0daa B90D0000 		.long	0xdb9
 1805 0dae 0A       		.uleb128 0xa
 1806 0daf 580B0000 		.long	0xb58
 1807 0db3 0A       		.uleb128 0xa
 1808 0db4 180D0000 		.long	0xd18
 1809 0db8 00       		.byte	0
 1810 0db9 32       		.uleb128 0x32
 1811 0dba 00000000 		.long	.LASF160
 1812 0dbe 11       		.byte	0x11
 1813 0dbf 7501     		.value	0x175
 1814 0dc1 160A0000 		.long	0xa16
 1815 0dc5 D90D0000 		.long	0xdd9
 1816 0dc9 0A       		.uleb128 0xa
 1817 0dca D90D0000 		.long	0xdd9
 1818 0dce 0A       		.uleb128 0xa
 1819 0dcf 1B0B0000 		.long	0xb1b
 1820 0dd3 0A       		.uleb128 0xa
 1821 0dd4 F80B0000 		.long	0xbf8
 1822 0dd8 00       		.byte	0
 1823 0dd9 31       		.uleb128 0x31
 1824 0dda 08       		.byte	0x8
 1825 0ddb 880A0000 		.long	0xa88
 1826 0ddf 35       		.uleb128 0x35
 1827 0de0 00000000 		.long	.LASF161
 1828 0de4 11       		.byte	0x11
 1829 0de5 9D       		.byte	0x9d
 1830 0de6 150B0000 		.long	0xb15
 1831 0dea F90D0000 		.long	0xdf9
 1832 0dee 0A       		.uleb128 0xa
 1833 0def 150B0000 		.long	0xb15
 1834 0df3 0A       		.uleb128 0xa
 1835 0df4 580B0000 		.long	0xb58
 1836 0df8 00       		.byte	0
 1837 0df9 35       		.uleb128 0x35
 1838 0dfa 00000000 		.long	.LASF162
 1839 0dfe 11       		.byte	0x11
 1840 0dff A6       		.byte	0xa6
 1841 0e00 8F0A0000 		.long	0xa8f
 1842 0e04 130E0000 		.long	0xe13
 1843 0e08 0A       		.uleb128 0xa
 1844 0e09 580B0000 		.long	0xb58
 1845 0e0d 0A       		.uleb128 0xa
 1846 0e0e 580B0000 		.long	0xb58
 1847 0e12 00       		.byte	0
 1848 0e13 35       		.uleb128 0x35
 1849 0e14 00000000 		.long	.LASF163
 1850 0e18 11       		.byte	0x11
 1851 0e19 C3       		.byte	0xc3
GAS LISTING /tmp/cceirXUd.s 			page 36


 1852 0e1a 8F0A0000 		.long	0xa8f
 1853 0e1e 2D0E0000 		.long	0xe2d
 1854 0e22 0A       		.uleb128 0xa
 1855 0e23 580B0000 		.long	0xb58
 1856 0e27 0A       		.uleb128 0xa
 1857 0e28 580B0000 		.long	0xb58
 1858 0e2c 00       		.byte	0
 1859 0e2d 35       		.uleb128 0x35
 1860 0e2e 00000000 		.long	.LASF164
 1861 0e32 11       		.byte	0x11
 1862 0e33 93       		.byte	0x93
 1863 0e34 150B0000 		.long	0xb15
 1864 0e38 470E0000 		.long	0xe47
 1865 0e3c 0A       		.uleb128 0xa
 1866 0e3d 150B0000 		.long	0xb15
 1867 0e41 0A       		.uleb128 0xa
 1868 0e42 580B0000 		.long	0xb58
 1869 0e46 00       		.byte	0
 1870 0e47 35       		.uleb128 0x35
 1871 0e48 00000000 		.long	.LASF165
 1872 0e4c 11       		.byte	0x11
 1873 0e4d FF       		.byte	0xff
 1874 0e4e 160A0000 		.long	0xa16
 1875 0e52 610E0000 		.long	0xe61
 1876 0e56 0A       		.uleb128 0xa
 1877 0e57 580B0000 		.long	0xb58
 1878 0e5b 0A       		.uleb128 0xa
 1879 0e5c 580B0000 		.long	0xb58
 1880 0e60 00       		.byte	0
 1881 0e61 32       		.uleb128 0x32
 1882 0e62 00000000 		.long	.LASF166
 1883 0e66 11       		.byte	0x11
 1884 0e67 5A03     		.value	0x35a
 1885 0e69 160A0000 		.long	0xa16
 1886 0e6d 860E0000 		.long	0xe86
 1887 0e71 0A       		.uleb128 0xa
 1888 0e72 150B0000 		.long	0xb15
 1889 0e76 0A       		.uleb128 0xa
 1890 0e77 160A0000 		.long	0xa16
 1891 0e7b 0A       		.uleb128 0xa
 1892 0e7c 580B0000 		.long	0xb58
 1893 0e80 0A       		.uleb128 0xa
 1894 0e81 860E0000 		.long	0xe86
 1895 0e85 00       		.byte	0
 1896 0e86 31       		.uleb128 0x31
 1897 0e87 08       		.byte	0x8
 1898 0e88 1C0F0000 		.long	0xf1c
 1899 0e8c 36       		.uleb128 0x36
 1900 0e8d 746D00   		.string	"tm"
 1901 0e90 38       		.byte	0x38
 1902 0e91 12       		.byte	0x12
 1903 0e92 85       		.byte	0x85
 1904 0e93 1C0F0000 		.long	0xf1c
 1905 0e97 28       		.uleb128 0x28
 1906 0e98 00000000 		.long	.LASF167
 1907 0e9c 12       		.byte	0x12
 1908 0e9d 87       		.byte	0x87
GAS LISTING /tmp/cceirXUd.s 			page 37


 1909 0e9e 8F0A0000 		.long	0xa8f
 1910 0ea2 00       		.byte	0
 1911 0ea3 28       		.uleb128 0x28
 1912 0ea4 00000000 		.long	.LASF168
 1913 0ea8 12       		.byte	0x12
 1914 0ea9 88       		.byte	0x88
 1915 0eaa 8F0A0000 		.long	0xa8f
 1916 0eae 04       		.byte	0x4
 1917 0eaf 28       		.uleb128 0x28
 1918 0eb0 00000000 		.long	.LASF169
 1919 0eb4 12       		.byte	0x12
 1920 0eb5 89       		.byte	0x89
 1921 0eb6 8F0A0000 		.long	0xa8f
 1922 0eba 08       		.byte	0x8
 1923 0ebb 28       		.uleb128 0x28
 1924 0ebc 00000000 		.long	.LASF170
 1925 0ec0 12       		.byte	0x12
 1926 0ec1 8A       		.byte	0x8a
 1927 0ec2 8F0A0000 		.long	0xa8f
 1928 0ec6 0C       		.byte	0xc
 1929 0ec7 28       		.uleb128 0x28
 1930 0ec8 00000000 		.long	.LASF171
 1931 0ecc 12       		.byte	0x12
 1932 0ecd 8B       		.byte	0x8b
 1933 0ece 8F0A0000 		.long	0xa8f
 1934 0ed2 10       		.byte	0x10
 1935 0ed3 28       		.uleb128 0x28
 1936 0ed4 00000000 		.long	.LASF172
 1937 0ed8 12       		.byte	0x12
 1938 0ed9 8C       		.byte	0x8c
 1939 0eda 8F0A0000 		.long	0xa8f
 1940 0ede 14       		.byte	0x14
 1941 0edf 28       		.uleb128 0x28
 1942 0ee0 00000000 		.long	.LASF173
 1943 0ee4 12       		.byte	0x12
 1944 0ee5 8D       		.byte	0x8d
 1945 0ee6 8F0A0000 		.long	0xa8f
 1946 0eea 18       		.byte	0x18
 1947 0eeb 28       		.uleb128 0x28
 1948 0eec 00000000 		.long	.LASF174
 1949 0ef0 12       		.byte	0x12
 1950 0ef1 8E       		.byte	0x8e
 1951 0ef2 8F0A0000 		.long	0xa8f
 1952 0ef6 1C       		.byte	0x1c
 1953 0ef7 28       		.uleb128 0x28
 1954 0ef8 00000000 		.long	.LASF175
 1955 0efc 12       		.byte	0x12
 1956 0efd 8F       		.byte	0x8f
 1957 0efe 8F0A0000 		.long	0xa8f
 1958 0f02 20       		.byte	0x20
 1959 0f03 28       		.uleb128 0x28
 1960 0f04 00000000 		.long	.LASF176
 1961 0f08 12       		.byte	0x12
 1962 0f09 92       		.byte	0x92
 1963 0f0a 64100000 		.long	0x1064
 1964 0f0e 28       		.byte	0x28
 1965 0f0f 28       		.uleb128 0x28
GAS LISTING /tmp/cceirXUd.s 			page 38


 1966 0f10 00000000 		.long	.LASF177
 1967 0f14 12       		.byte	0x12
 1968 0f15 93       		.byte	0x93
 1969 0f16 B80A0000 		.long	0xab8
 1970 0f1a 30       		.byte	0x30
 1971 0f1b 00       		.byte	0
 1972 0f1c 0B       		.uleb128 0xb
 1973 0f1d 8C0E0000 		.long	0xe8c
 1974 0f21 32       		.uleb128 0x32
 1975 0f22 00000000 		.long	.LASF178
 1976 0f26 11       		.byte	0x11
 1977 0f27 2201     		.value	0x122
 1978 0f29 160A0000 		.long	0xa16
 1979 0f2d 370F0000 		.long	0xf37
 1980 0f31 0A       		.uleb128 0xa
 1981 0f32 580B0000 		.long	0xb58
 1982 0f36 00       		.byte	0
 1983 0f37 35       		.uleb128 0x35
 1984 0f38 00000000 		.long	.LASF179
 1985 0f3c 11       		.byte	0x11
 1986 0f3d A1       		.byte	0xa1
 1987 0f3e 150B0000 		.long	0xb15
 1988 0f42 560F0000 		.long	0xf56
 1989 0f46 0A       		.uleb128 0xa
 1990 0f47 150B0000 		.long	0xb15
 1991 0f4b 0A       		.uleb128 0xa
 1992 0f4c 580B0000 		.long	0xb58
 1993 0f50 0A       		.uleb128 0xa
 1994 0f51 160A0000 		.long	0xa16
 1995 0f55 00       		.byte	0
 1996 0f56 35       		.uleb128 0x35
 1997 0f57 00000000 		.long	.LASF180
 1998 0f5b 11       		.byte	0x11
 1999 0f5c A9       		.byte	0xa9
 2000 0f5d 8F0A0000 		.long	0xa8f
 2001 0f61 750F0000 		.long	0xf75
 2002 0f65 0A       		.uleb128 0xa
 2003 0f66 580B0000 		.long	0xb58
 2004 0f6a 0A       		.uleb128 0xa
 2005 0f6b 580B0000 		.long	0xb58
 2006 0f6f 0A       		.uleb128 0xa
 2007 0f70 160A0000 		.long	0xa16
 2008 0f74 00       		.byte	0
 2009 0f75 35       		.uleb128 0x35
 2010 0f76 00000000 		.long	.LASF181
 2011 0f7a 11       		.byte	0x11
 2012 0f7b 98       		.byte	0x98
 2013 0f7c 150B0000 		.long	0xb15
 2014 0f80 940F0000 		.long	0xf94
 2015 0f84 0A       		.uleb128 0xa
 2016 0f85 150B0000 		.long	0xb15
 2017 0f89 0A       		.uleb128 0xa
 2018 0f8a 580B0000 		.long	0xb58
 2019 0f8e 0A       		.uleb128 0xa
 2020 0f8f 160A0000 		.long	0xa16
 2021 0f93 00       		.byte	0
 2022 0f94 32       		.uleb128 0x32
GAS LISTING /tmp/cceirXUd.s 			page 39


 2023 0f95 00000000 		.long	.LASF182
 2024 0f99 11       		.byte	0x11
 2025 0f9a A101     		.value	0x1a1
 2026 0f9c 160A0000 		.long	0xa16
 2027 0fa0 B90F0000 		.long	0xfb9
 2028 0fa4 0A       		.uleb128 0xa
 2029 0fa5 D90D0000 		.long	0xdd9
 2030 0fa9 0A       		.uleb128 0xa
 2031 0faa B90F0000 		.long	0xfb9
 2032 0fae 0A       		.uleb128 0xa
 2033 0faf 160A0000 		.long	0xa16
 2034 0fb3 0A       		.uleb128 0xa
 2035 0fb4 F80B0000 		.long	0xbf8
 2036 0fb8 00       		.byte	0
 2037 0fb9 31       		.uleb128 0x31
 2038 0fba 08       		.byte	0x8
 2039 0fbb 580B0000 		.long	0xb58
 2040 0fbf 32       		.uleb128 0x32
 2041 0fc0 00000000 		.long	.LASF183
 2042 0fc4 11       		.byte	0x11
 2043 0fc5 0301     		.value	0x103
 2044 0fc7 160A0000 		.long	0xa16
 2045 0fcb DA0F0000 		.long	0xfda
 2046 0fcf 0A       		.uleb128 0xa
 2047 0fd0 580B0000 		.long	0xb58
 2048 0fd4 0A       		.uleb128 0xa
 2049 0fd5 580B0000 		.long	0xb58
 2050 0fd9 00       		.byte	0
 2051 0fda 32       		.uleb128 0x32
 2052 0fdb 00000000 		.long	.LASF184
 2053 0fdf 11       		.byte	0x11
 2054 0fe0 C501     		.value	0x1c5
 2055 0fe2 F50F0000 		.long	0xff5
 2056 0fe6 F50F0000 		.long	0xff5
 2057 0fea 0A       		.uleb128 0xa
 2058 0feb 580B0000 		.long	0xb58
 2059 0fef 0A       		.uleb128 0xa
 2060 0ff0 FC0F0000 		.long	0xffc
 2061 0ff4 00       		.byte	0
 2062 0ff5 27       		.uleb128 0x27
 2063 0ff6 08       		.byte	0x8
 2064 0ff7 04       		.byte	0x4
 2065 0ff8 00000000 		.long	.LASF185
 2066 0ffc 31       		.uleb128 0x31
 2067 0ffd 08       		.byte	0x8
 2068 0ffe 150B0000 		.long	0xb15
 2069 1002 32       		.uleb128 0x32
 2070 1003 00000000 		.long	.LASF186
 2071 1007 11       		.byte	0x11
 2072 1008 CC01     		.value	0x1cc
 2073 100a 1D100000 		.long	0x101d
 2074 100e 1D100000 		.long	0x101d
 2075 1012 0A       		.uleb128 0xa
 2076 1013 580B0000 		.long	0xb58
 2077 1017 0A       		.uleb128 0xa
 2078 1018 FC0F0000 		.long	0xffc
 2079 101c 00       		.byte	0
GAS LISTING /tmp/cceirXUd.s 			page 40


 2080 101d 27       		.uleb128 0x27
 2081 101e 04       		.byte	0x4
 2082 101f 04       		.byte	0x4
 2083 1020 00000000 		.long	.LASF187
 2084 1024 32       		.uleb128 0x32
 2085 1025 00000000 		.long	.LASF188
 2086 1029 11       		.byte	0x11
 2087 102a 1D01     		.value	0x11d
 2088 102c 150B0000 		.long	0xb15
 2089 1030 44100000 		.long	0x1044
 2090 1034 0A       		.uleb128 0xa
 2091 1035 150B0000 		.long	0xb15
 2092 1039 0A       		.uleb128 0xa
 2093 103a 580B0000 		.long	0xb58
 2094 103e 0A       		.uleb128 0xa
 2095 103f FC0F0000 		.long	0xffc
 2096 1043 00       		.byte	0
 2097 1044 32       		.uleb128 0x32
 2098 1045 00000000 		.long	.LASF189
 2099 1049 11       		.byte	0x11
 2100 104a D701     		.value	0x1d7
 2101 104c 64100000 		.long	0x1064
 2102 1050 64100000 		.long	0x1064
 2103 1054 0A       		.uleb128 0xa
 2104 1055 580B0000 		.long	0xb58
 2105 1059 0A       		.uleb128 0xa
 2106 105a FC0F0000 		.long	0xffc
 2107 105e 0A       		.uleb128 0xa
 2108 105f 8F0A0000 		.long	0xa8f
 2109 1063 00       		.byte	0
 2110 1064 27       		.uleb128 0x27
 2111 1065 08       		.byte	0x8
 2112 1066 05       		.byte	0x5
 2113 1067 00000000 		.long	.LASF190
 2114 106b 32       		.uleb128 0x32
 2115 106c 00000000 		.long	.LASF191
 2116 1070 11       		.byte	0x11
 2117 1071 DC01     		.value	0x1dc
 2118 1073 210A0000 		.long	0xa21
 2119 1077 8B100000 		.long	0x108b
 2120 107b 0A       		.uleb128 0xa
 2121 107c 580B0000 		.long	0xb58
 2122 1080 0A       		.uleb128 0xa
 2123 1081 FC0F0000 		.long	0xffc
 2124 1085 0A       		.uleb128 0xa
 2125 1086 8F0A0000 		.long	0xa8f
 2126 108a 00       		.byte	0
 2127 108b 35       		.uleb128 0x35
 2128 108c 00000000 		.long	.LASF192
 2129 1090 11       		.byte	0x11
 2130 1091 C7       		.byte	0xc7
 2131 1092 160A0000 		.long	0xa16
 2132 1096 AA100000 		.long	0x10aa
 2133 109a 0A       		.uleb128 0xa
 2134 109b 150B0000 		.long	0xb15
 2135 109f 0A       		.uleb128 0xa
 2136 10a0 580B0000 		.long	0xb58
GAS LISTING /tmp/cceirXUd.s 			page 41


 2137 10a4 0A       		.uleb128 0xa
 2138 10a5 160A0000 		.long	0xa16
 2139 10a9 00       		.byte	0
 2140 10aa 32       		.uleb128 0x32
 2141 10ab 00000000 		.long	.LASF193
 2142 10af 11       		.byte	0x11
 2143 10b0 6801     		.value	0x168
 2144 10b2 8F0A0000 		.long	0xa8f
 2145 10b6 C0100000 		.long	0x10c0
 2146 10ba 0A       		.uleb128 0xa
 2147 10bb 280A0000 		.long	0xa28
 2148 10bf 00       		.byte	0
 2149 10c0 32       		.uleb128 0x32
 2150 10c1 00000000 		.long	.LASF194
 2151 10c5 11       		.byte	0x11
 2152 10c6 4801     		.value	0x148
 2153 10c8 8F0A0000 		.long	0xa8f
 2154 10cc E0100000 		.long	0x10e0
 2155 10d0 0A       		.uleb128 0xa
 2156 10d1 580B0000 		.long	0xb58
 2157 10d5 0A       		.uleb128 0xa
 2158 10d6 580B0000 		.long	0xb58
 2159 10da 0A       		.uleb128 0xa
 2160 10db 160A0000 		.long	0xa16
 2161 10df 00       		.byte	0
 2162 10e0 32       		.uleb128 0x32
 2163 10e1 00000000 		.long	.LASF195
 2164 10e5 11       		.byte	0x11
 2165 10e6 4C01     		.value	0x14c
 2166 10e8 150B0000 		.long	0xb15
 2167 10ec 00110000 		.long	0x1100
 2168 10f0 0A       		.uleb128 0xa
 2169 10f1 150B0000 		.long	0xb15
 2170 10f5 0A       		.uleb128 0xa
 2171 10f6 580B0000 		.long	0xb58
 2172 10fa 0A       		.uleb128 0xa
 2173 10fb 160A0000 		.long	0xa16
 2174 10ff 00       		.byte	0
 2175 1100 32       		.uleb128 0x32
 2176 1101 00000000 		.long	.LASF196
 2177 1105 11       		.byte	0x11
 2178 1106 5101     		.value	0x151
 2179 1108 150B0000 		.long	0xb15
 2180 110c 20110000 		.long	0x1120
 2181 1110 0A       		.uleb128 0xa
 2182 1111 150B0000 		.long	0xb15
 2183 1115 0A       		.uleb128 0xa
 2184 1116 580B0000 		.long	0xb58
 2185 111a 0A       		.uleb128 0xa
 2186 111b 160A0000 		.long	0xa16
 2187 111f 00       		.byte	0
 2188 1120 32       		.uleb128 0x32
 2189 1121 00000000 		.long	.LASF197
 2190 1125 11       		.byte	0x11
 2191 1126 5501     		.value	0x155
 2192 1128 150B0000 		.long	0xb15
 2193 112c 40110000 		.long	0x1140
GAS LISTING /tmp/cceirXUd.s 			page 42


 2194 1130 0A       		.uleb128 0xa
 2195 1131 150B0000 		.long	0xb15
 2196 1135 0A       		.uleb128 0xa
 2197 1136 1B0B0000 		.long	0xb1b
 2198 113a 0A       		.uleb128 0xa
 2199 113b 160A0000 		.long	0xa16
 2200 113f 00       		.byte	0
 2201 1140 32       		.uleb128 0x32
 2202 1141 00000000 		.long	.LASF198
 2203 1145 11       		.byte	0x11
 2204 1146 5C02     		.value	0x25c
 2205 1148 8F0A0000 		.long	0xa8f
 2206 114c 57110000 		.long	0x1157
 2207 1150 0A       		.uleb128 0xa
 2208 1151 580B0000 		.long	0xb58
 2209 1155 33       		.uleb128 0x33
 2210 1156 00       		.byte	0
 2211 1157 32       		.uleb128 0x32
 2212 1158 00000000 		.long	.LASF199
 2213 115c 11       		.byte	0x11
 2214 115d 8502     		.value	0x285
 2215 115f 8F0A0000 		.long	0xa8f
 2216 1163 6E110000 		.long	0x116e
 2217 1167 0A       		.uleb128 0xa
 2218 1168 580B0000 		.long	0xb58
 2219 116c 33       		.uleb128 0x33
 2220 116d 00       		.byte	0
 2221 116e 37       		.uleb128 0x37
 2222 116f 00000000 		.long	.LASF200
 2223 1173 11       		.byte	0x11
 2224 1174 E3       		.byte	0xe3
 2225 1175 00000000 		.long	.LASF200
 2226 1179 580B0000 		.long	0xb58
 2227 117d 8C110000 		.long	0x118c
 2228 1181 0A       		.uleb128 0xa
 2229 1182 580B0000 		.long	0xb58
 2230 1186 0A       		.uleb128 0xa
 2231 1187 1B0B0000 		.long	0xb1b
 2232 118b 00       		.byte	0
 2233 118c 0D       		.uleb128 0xd
 2234 118d 00000000 		.long	.LASF201
 2235 1191 11       		.byte	0x11
 2236 1192 0901     		.value	0x109
 2237 1194 00000000 		.long	.LASF201
 2238 1198 580B0000 		.long	0xb58
 2239 119c AB110000 		.long	0x11ab
 2240 11a0 0A       		.uleb128 0xa
 2241 11a1 580B0000 		.long	0xb58
 2242 11a5 0A       		.uleb128 0xa
 2243 11a6 580B0000 		.long	0xb58
 2244 11aa 00       		.byte	0
 2245 11ab 37       		.uleb128 0x37
 2246 11ac 00000000 		.long	.LASF202
 2247 11b0 11       		.byte	0x11
 2248 11b1 ED       		.byte	0xed
 2249 11b2 00000000 		.long	.LASF202
 2250 11b6 580B0000 		.long	0xb58
GAS LISTING /tmp/cceirXUd.s 			page 43


 2251 11ba C9110000 		.long	0x11c9
 2252 11be 0A       		.uleb128 0xa
 2253 11bf 580B0000 		.long	0xb58
 2254 11c3 0A       		.uleb128 0xa
 2255 11c4 1B0B0000 		.long	0xb1b
 2256 11c8 00       		.byte	0
 2257 11c9 0D       		.uleb128 0xd
 2258 11ca 00000000 		.long	.LASF203
 2259 11ce 11       		.byte	0x11
 2260 11cf 1401     		.value	0x114
 2261 11d1 00000000 		.long	.LASF203
 2262 11d5 580B0000 		.long	0xb58
 2263 11d9 E8110000 		.long	0x11e8
 2264 11dd 0A       		.uleb128 0xa
 2265 11de 580B0000 		.long	0xb58
 2266 11e2 0A       		.uleb128 0xa
 2267 11e3 580B0000 		.long	0xb58
 2268 11e7 00       		.byte	0
 2269 11e8 0D       		.uleb128 0xd
 2270 11e9 00000000 		.long	.LASF204
 2271 11ed 11       		.byte	0x11
 2272 11ee 3F01     		.value	0x13f
 2273 11f0 00000000 		.long	.LASF204
 2274 11f4 580B0000 		.long	0xb58
 2275 11f8 0C120000 		.long	0x120c
 2276 11fc 0A       		.uleb128 0xa
 2277 11fd 580B0000 		.long	0xb58
 2278 1201 0A       		.uleb128 0xa
 2279 1202 1B0B0000 		.long	0xb1b
 2280 1206 0A       		.uleb128 0xa
 2281 1207 160A0000 		.long	0xa16
 2282 120b 00       		.byte	0
 2283 120c 32       		.uleb128 0x32
 2284 120d 00000000 		.long	.LASF205
 2285 1211 11       		.byte	0x11
 2286 1212 CE01     		.value	0x1ce
 2287 1214 27120000 		.long	0x1227
 2288 1218 27120000 		.long	0x1227
 2289 121c 0A       		.uleb128 0xa
 2290 121d 580B0000 		.long	0xb58
 2291 1221 0A       		.uleb128 0xa
 2292 1222 FC0F0000 		.long	0xffc
 2293 1226 00       		.byte	0
 2294 1227 27       		.uleb128 0x27
 2295 1228 10       		.byte	0x10
 2296 1229 04       		.byte	0x4
 2297 122a 00000000 		.long	.LASF206
 2298 122e 32       		.uleb128 0x32
 2299 122f 00000000 		.long	.LASF207
 2300 1233 11       		.byte	0x11
 2301 1234 E601     		.value	0x1e6
 2302 1236 4E120000 		.long	0x124e
 2303 123a 4E120000 		.long	0x124e
 2304 123e 0A       		.uleb128 0xa
 2305 123f 580B0000 		.long	0xb58
 2306 1243 0A       		.uleb128 0xa
 2307 1244 FC0F0000 		.long	0xffc
GAS LISTING /tmp/cceirXUd.s 			page 44


 2308 1248 0A       		.uleb128 0xa
 2309 1249 8F0A0000 		.long	0xa8f
 2310 124d 00       		.byte	0
 2311 124e 27       		.uleb128 0x27
 2312 124f 08       		.byte	0x8
 2313 1250 05       		.byte	0x5
 2314 1251 00000000 		.long	.LASF208
 2315 1255 32       		.uleb128 0x32
 2316 1256 00000000 		.long	.LASF209
 2317 125a 11       		.byte	0x11
 2318 125b ED01     		.value	0x1ed
 2319 125d 75120000 		.long	0x1275
 2320 1261 75120000 		.long	0x1275
 2321 1265 0A       		.uleb128 0xa
 2322 1266 580B0000 		.long	0xb58
 2323 126a 0A       		.uleb128 0xa
 2324 126b FC0F0000 		.long	0xffc
 2325 126f 0A       		.uleb128 0xa
 2326 1270 8F0A0000 		.long	0xa8f
 2327 1274 00       		.byte	0
 2328 1275 27       		.uleb128 0x27
 2329 1276 08       		.byte	0x8
 2330 1277 07       		.byte	0x7
 2331 1278 00000000 		.long	.LASF210
 2332 127c 27       		.uleb128 0x27
 2333 127d 01       		.byte	0x1
 2334 127e 08       		.byte	0x8
 2335 127f 00000000 		.long	.LASF211
 2336 1283 27       		.uleb128 0x27
 2337 1284 01       		.byte	0x1
 2338 1285 06       		.byte	0x6
 2339 1286 00000000 		.long	.LASF212
 2340 128a 27       		.uleb128 0x27
 2341 128b 02       		.byte	0x2
 2342 128c 05       		.byte	0x5
 2343 128d 00000000 		.long	.LASF213
 2344 1291 23       		.uleb128 0x23
 2345 1292 00000000 		.long	.LASF214
 2346 1296 05       		.byte	0x5
 2347 1297 37       		.byte	0x37
 2348 1298 A4120000 		.long	0x12a4
 2349 129c 04       		.uleb128 0x4
 2350 129d 05       		.byte	0x5
 2351 129e 38       		.byte	0x38
 2352 129f F4010000 		.long	0x1f4
 2353 12a3 00       		.byte	0
 2354 12a4 38       		.uleb128 0x38
 2355 12a5 08       		.byte	0x8
 2356 12a6 07020000 		.long	0x207
 2357 12aa 38       		.uleb128 0x38
 2358 12ab 08       		.byte	0x8
 2359 12ac 37020000 		.long	0x237
 2360 12b0 27       		.uleb128 0x27
 2361 12b1 01       		.byte	0x1
 2362 12b2 02       		.byte	0x2
 2363 12b3 00000000 		.long	.LASF215
 2364 12b7 31       		.uleb128 0x31
GAS LISTING /tmp/cceirXUd.s 			page 45


 2365 12b8 08       		.byte	0x8
 2366 12b9 37020000 		.long	0x237
 2367 12bd 31       		.uleb128 0x31
 2368 12be 08       		.byte	0x8
 2369 12bf 07020000 		.long	0x207
 2370 12c3 38       		.uleb128 0x38
 2371 12c4 08       		.byte	0x8
 2372 12c5 5E030000 		.long	0x35e
 2373 12c9 07       		.uleb128 0x7
 2374 12ca 00000000 		.long	.LASF216
 2375 12ce 60       		.byte	0x60
 2376 12cf 13       		.byte	0x13
 2377 12d0 35       		.byte	0x35
 2378 12d1 F6130000 		.long	0x13f6
 2379 12d5 28       		.uleb128 0x28
 2380 12d6 00000000 		.long	.LASF217
 2381 12da 13       		.byte	0x13
 2382 12db 39       		.byte	0x39
 2383 12dc D90D0000 		.long	0xdd9
 2384 12e0 00       		.byte	0
 2385 12e1 28       		.uleb128 0x28
 2386 12e2 00000000 		.long	.LASF218
 2387 12e6 13       		.byte	0x13
 2388 12e7 3A       		.byte	0x3a
 2389 12e8 D90D0000 		.long	0xdd9
 2390 12ec 08       		.byte	0x8
 2391 12ed 28       		.uleb128 0x28
 2392 12ee 00000000 		.long	.LASF219
 2393 12f2 13       		.byte	0x13
 2394 12f3 40       		.byte	0x40
 2395 12f4 D90D0000 		.long	0xdd9
 2396 12f8 10       		.byte	0x10
 2397 12f9 28       		.uleb128 0x28
 2398 12fa 00000000 		.long	.LASF220
 2399 12fe 13       		.byte	0x13
 2400 12ff 46       		.byte	0x46
 2401 1300 D90D0000 		.long	0xdd9
 2402 1304 18       		.byte	0x18
 2403 1305 28       		.uleb128 0x28
 2404 1306 00000000 		.long	.LASF221
 2405 130a 13       		.byte	0x13
 2406 130b 47       		.byte	0x47
 2407 130c D90D0000 		.long	0xdd9
 2408 1310 20       		.byte	0x20
 2409 1311 28       		.uleb128 0x28
 2410 1312 00000000 		.long	.LASF222
 2411 1316 13       		.byte	0x13
 2412 1317 48       		.byte	0x48
 2413 1318 D90D0000 		.long	0xdd9
 2414 131c 28       		.byte	0x28
 2415 131d 28       		.uleb128 0x28
 2416 131e 00000000 		.long	.LASF223
 2417 1322 13       		.byte	0x13
 2418 1323 49       		.byte	0x49
 2419 1324 D90D0000 		.long	0xdd9
 2420 1328 30       		.byte	0x30
 2421 1329 28       		.uleb128 0x28
GAS LISTING /tmp/cceirXUd.s 			page 46


 2422 132a 00000000 		.long	.LASF224
 2423 132e 13       		.byte	0x13
 2424 132f 4A       		.byte	0x4a
 2425 1330 D90D0000 		.long	0xdd9
 2426 1334 38       		.byte	0x38
 2427 1335 28       		.uleb128 0x28
 2428 1336 00000000 		.long	.LASF225
 2429 133a 13       		.byte	0x13
 2430 133b 4B       		.byte	0x4b
 2431 133c D90D0000 		.long	0xdd9
 2432 1340 40       		.byte	0x40
 2433 1341 28       		.uleb128 0x28
 2434 1342 00000000 		.long	.LASF226
 2435 1346 13       		.byte	0x13
 2436 1347 4C       		.byte	0x4c
 2437 1348 D90D0000 		.long	0xdd9
 2438 134c 48       		.byte	0x48
 2439 134d 28       		.uleb128 0x28
 2440 134e 00000000 		.long	.LASF227
 2441 1352 13       		.byte	0x13
 2442 1353 4D       		.byte	0x4d
 2443 1354 880A0000 		.long	0xa88
 2444 1358 50       		.byte	0x50
 2445 1359 28       		.uleb128 0x28
 2446 135a 00000000 		.long	.LASF228
 2447 135e 13       		.byte	0x13
 2448 135f 4E       		.byte	0x4e
 2449 1360 880A0000 		.long	0xa88
 2450 1364 51       		.byte	0x51
 2451 1365 28       		.uleb128 0x28
 2452 1366 00000000 		.long	.LASF229
 2453 136a 13       		.byte	0x13
 2454 136b 50       		.byte	0x50
 2455 136c 880A0000 		.long	0xa88
 2456 1370 52       		.byte	0x52
 2457 1371 28       		.uleb128 0x28
 2458 1372 00000000 		.long	.LASF230
 2459 1376 13       		.byte	0x13
 2460 1377 52       		.byte	0x52
 2461 1378 880A0000 		.long	0xa88
 2462 137c 53       		.byte	0x53
 2463 137d 28       		.uleb128 0x28
 2464 137e 00000000 		.long	.LASF231
 2465 1382 13       		.byte	0x13
 2466 1383 54       		.byte	0x54
 2467 1384 880A0000 		.long	0xa88
 2468 1388 54       		.byte	0x54
 2469 1389 28       		.uleb128 0x28
 2470 138a 00000000 		.long	.LASF232
 2471 138e 13       		.byte	0x13
 2472 138f 56       		.byte	0x56
 2473 1390 880A0000 		.long	0xa88
 2474 1394 55       		.byte	0x55
 2475 1395 28       		.uleb128 0x28
 2476 1396 00000000 		.long	.LASF233
 2477 139a 13       		.byte	0x13
 2478 139b 5D       		.byte	0x5d
GAS LISTING /tmp/cceirXUd.s 			page 47


 2479 139c 880A0000 		.long	0xa88
 2480 13a0 56       		.byte	0x56
 2481 13a1 28       		.uleb128 0x28
 2482 13a2 00000000 		.long	.LASF234
 2483 13a6 13       		.byte	0x13
 2484 13a7 5E       		.byte	0x5e
 2485 13a8 880A0000 		.long	0xa88
 2486 13ac 57       		.byte	0x57
 2487 13ad 28       		.uleb128 0x28
 2488 13ae 00000000 		.long	.LASF235
 2489 13b2 13       		.byte	0x13
 2490 13b3 61       		.byte	0x61
 2491 13b4 880A0000 		.long	0xa88
 2492 13b8 58       		.byte	0x58
 2493 13b9 28       		.uleb128 0x28
 2494 13ba 00000000 		.long	.LASF236
 2495 13be 13       		.byte	0x13
 2496 13bf 63       		.byte	0x63
 2497 13c0 880A0000 		.long	0xa88
 2498 13c4 59       		.byte	0x59
 2499 13c5 28       		.uleb128 0x28
 2500 13c6 00000000 		.long	.LASF237
 2501 13ca 13       		.byte	0x13
 2502 13cb 65       		.byte	0x65
 2503 13cc 880A0000 		.long	0xa88
 2504 13d0 5A       		.byte	0x5a
 2505 13d1 28       		.uleb128 0x28
 2506 13d2 00000000 		.long	.LASF238
 2507 13d6 13       		.byte	0x13
 2508 13d7 67       		.byte	0x67
 2509 13d8 880A0000 		.long	0xa88
 2510 13dc 5B       		.byte	0x5b
 2511 13dd 28       		.uleb128 0x28
 2512 13de 00000000 		.long	.LASF239
 2513 13e2 13       		.byte	0x13
 2514 13e3 6E       		.byte	0x6e
 2515 13e4 880A0000 		.long	0xa88
 2516 13e8 5C       		.byte	0x5c
 2517 13e9 28       		.uleb128 0x28
 2518 13ea 00000000 		.long	.LASF240
 2519 13ee 13       		.byte	0x13
 2520 13ef 6F       		.byte	0x6f
 2521 13f0 880A0000 		.long	0xa88
 2522 13f4 5D       		.byte	0x5d
 2523 13f5 00       		.byte	0
 2524 13f6 35       		.uleb128 0x35
 2525 13f7 00000000 		.long	.LASF241
 2526 13fb 13       		.byte	0x13
 2527 13fc 7C       		.byte	0x7c
 2528 13fd D90D0000 		.long	0xdd9
 2529 1401 10140000 		.long	0x1410
 2530 1405 0A       		.uleb128 0xa
 2531 1406 8F0A0000 		.long	0xa8f
 2532 140a 0A       		.uleb128 0xa
 2533 140b B80A0000 		.long	0xab8
 2534 140f 00       		.byte	0
 2535 1410 39       		.uleb128 0x39
GAS LISTING /tmp/cceirXUd.s 			page 48


 2536 1411 00000000 		.long	.LASF243
 2537 1415 13       		.byte	0x13
 2538 1416 7F       		.byte	0x7f
 2539 1417 1B140000 		.long	0x141b
 2540 141b 31       		.uleb128 0x31
 2541 141c 08       		.byte	0x8
 2542 141d C9120000 		.long	0x12c9
 2543 1421 08       		.uleb128 0x8
 2544 1422 00000000 		.long	.LASF244
 2545 1426 14       		.byte	0x14
 2546 1427 28       		.byte	0x28
 2547 1428 8F0A0000 		.long	0xa8f
 2548 142c 08       		.uleb128 0x8
 2549 142d 00000000 		.long	.LASF245
 2550 1431 15       		.byte	0x15
 2551 1432 20       		.byte	0x20
 2552 1433 8F0A0000 		.long	0xa8f
 2553 1437 0B       		.uleb128 0xb
 2554 1438 B0120000 		.long	0x12b0
 2555 143c 0B       		.uleb128 0xb
 2556 143d 210A0000 		.long	0xa21
 2557 1441 31       		.uleb128 0x31
 2558 1442 08       		.byte	0x8
 2559 1443 5A050000 		.long	0x55a
 2560 1447 08       		.uleb128 0x8
 2561 1448 00000000 		.long	.LASF246
 2562 144c 16       		.byte	0x16
 2563 144d 34       		.byte	0x34
 2564 144e 210A0000 		.long	0xa21
 2565 1452 08       		.uleb128 0x8
 2566 1453 00000000 		.long	.LASF247
 2567 1457 16       		.byte	0x16
 2568 1458 BA       		.byte	0xba
 2569 1459 5D140000 		.long	0x145d
 2570 145d 31       		.uleb128 0x31
 2571 145e 08       		.byte	0x8
 2572 145f 63140000 		.long	0x1463
 2573 1463 0B       		.uleb128 0xb
 2574 1464 21140000 		.long	0x1421
 2575 1468 35       		.uleb128 0x35
 2576 1469 00000000 		.long	.LASF248
 2577 146d 16       		.byte	0x16
 2578 146e AF       		.byte	0xaf
 2579 146f 8F0A0000 		.long	0xa8f
 2580 1473 82140000 		.long	0x1482
 2581 1477 0A       		.uleb128 0xa
 2582 1478 280A0000 		.long	0xa28
 2583 147c 0A       		.uleb128 0xa
 2584 147d 47140000 		.long	0x1447
 2585 1481 00       		.byte	0
 2586 1482 35       		.uleb128 0x35
 2587 1483 00000000 		.long	.LASF249
 2588 1487 16       		.byte	0x16
 2589 1488 DD       		.byte	0xdd
 2590 1489 280A0000 		.long	0xa28
 2591 148d 9C140000 		.long	0x149c
 2592 1491 0A       		.uleb128 0xa
GAS LISTING /tmp/cceirXUd.s 			page 49


 2593 1492 280A0000 		.long	0xa28
 2594 1496 0A       		.uleb128 0xa
 2595 1497 52140000 		.long	0x1452
 2596 149b 00       		.byte	0
 2597 149c 35       		.uleb128 0x35
 2598 149d 00000000 		.long	.LASF250
 2599 14a1 16       		.byte	0x16
 2600 14a2 DA       		.byte	0xda
 2601 14a3 52140000 		.long	0x1452
 2602 14a7 B1140000 		.long	0x14b1
 2603 14ab 0A       		.uleb128 0xa
 2604 14ac B80A0000 		.long	0xab8
 2605 14b0 00       		.byte	0
 2606 14b1 35       		.uleb128 0x35
 2607 14b2 00000000 		.long	.LASF251
 2608 14b6 16       		.byte	0x16
 2609 14b7 AB       		.byte	0xab
 2610 14b8 47140000 		.long	0x1447
 2611 14bc C6140000 		.long	0x14c6
 2612 14c0 0A       		.uleb128 0xa
 2613 14c1 B80A0000 		.long	0xab8
 2614 14c5 00       		.byte	0
 2615 14c6 0B       		.uleb128 0xb
 2616 14c7 8A120000 		.long	0x128a
 2617 14cb 0B       		.uleb128 0xb
 2618 14cc 64100000 		.long	0x1064
 2619 14d0 3A       		.uleb128 0x3a
 2620 14d1 00000000 		.long	.LASF253
 2621 14d5 01       		.byte	0x1
 2622 14d6 04       		.byte	0x4
 2623 14d7 8F0A0000 		.long	0xa8f
 2624 14db 00000000 		.quad	.LFB1021
 2624      00000000 
 2625 14e3 82000000 		.quad	.LFE1021-.LFB1021
 2625      00000000 
 2626 14eb 01       		.uleb128 0x1
 2627 14ec 9C       		.byte	0x9c
 2628 14ed 1C150000 		.long	0x151c
 2629 14f1 3B       		.uleb128 0x3b
 2630 14f2 6100     		.string	"a"
 2631 14f4 01       		.byte	0x1
 2632 14f5 06       		.byte	0x6
 2633 14f6 8F0A0000 		.long	0xa8f
 2634 14fa 02       		.uleb128 0x2
 2635 14fb 91       		.byte	0x91
 2636 14fc 68       		.sleb128 -24
 2637 14fd 3C       		.uleb128 0x3c
 2638 14fe 00000000 		.quad	.LBB2
 2638      00000000 
 2639 1506 62000000 		.quad	.LBE2-.LBB2
 2639      00000000 
 2640 150e 3B       		.uleb128 0x3b
 2641 150f 6100     		.string	"a"
 2642 1511 01       		.byte	0x1
 2643 1512 0E       		.byte	0xe
 2644 1513 8F0A0000 		.long	0xa8f
 2645 1517 02       		.uleb128 0x2
GAS LISTING /tmp/cceirXUd.s 			page 50


 2646 1518 91       		.byte	0x91
 2647 1519 6C       		.sleb128 -20
 2648 151a 00       		.byte	0
 2649 151b 00       		.byte	0
 2650 151c 3D       		.uleb128 0x3d
 2651 151d 00000000 		.long	.LASF254
 2652 1521 01       		.byte	0x1
 2653 1522 1E       		.byte	0x1e
 2654 1523 00000000 		.long	.LASF281
 2655 1527 00000000 		.quad	.LFB1022
 2655      00000000 
 2656 152f 3A000000 		.quad	.LFE1022-.LFB1022
 2656      00000000 
 2657 1537 01       		.uleb128 0x1
 2658 1538 9C       		.byte	0x9c
 2659 1539 4C150000 		.long	0x154c
 2660 153d 3E       		.uleb128 0x3e
 2661 153e 00000000 		.long	.LASF255
 2662 1542 01       		.byte	0x1
 2663 1543 1E       		.byte	0x1e
 2664 1544 8F0A0000 		.long	0xa8f
 2665 1548 02       		.uleb128 0x2
 2666 1549 91       		.byte	0x91
 2667 154a 6C       		.sleb128 -20
 2668 154b 00       		.byte	0
 2669 154c 3F       		.uleb128 0x3f
 2670 154d 00000000 		.long	.LASF282
 2671 1551 00000000 		.quad	.LFB1031
 2671      00000000 
 2672 1559 3E000000 		.quad	.LFE1031-.LFB1031
 2672      00000000 
 2673 1561 01       		.uleb128 0x1
 2674 1562 9C       		.byte	0x9c
 2675 1563 84150000 		.long	0x1584
 2676 1567 3E       		.uleb128 0x3e
 2677 1568 00000000 		.long	.LASF256
 2678 156c 01       		.byte	0x1
 2679 156d 21       		.byte	0x21
 2680 156e 8F0A0000 		.long	0xa8f
 2681 1572 02       		.uleb128 0x2
 2682 1573 91       		.byte	0x91
 2683 1574 6C       		.sleb128 -20
 2684 1575 3E       		.uleb128 0x3e
 2685 1576 00000000 		.long	.LASF257
 2686 157a 01       		.byte	0x1
 2687 157b 21       		.byte	0x21
 2688 157c 8F0A0000 		.long	0xa8f
 2689 1580 02       		.uleb128 0x2
 2690 1581 91       		.byte	0x91
 2691 1582 68       		.sleb128 -24
 2692 1583 00       		.byte	0
 2693 1584 40       		.uleb128 0x40
 2694 1585 00000000 		.long	.LASF283
 2695 1589 00000000 		.quad	.LFB1032
 2695      00000000 
 2696 1591 15000000 		.quad	.LFE1032-.LFB1032
 2696      00000000 
GAS LISTING /tmp/cceirXUd.s 			page 51


 2697 1599 01       		.uleb128 0x1
 2698 159a 9C       		.byte	0x9c
 2699 159b 41       		.uleb128 0x41
 2700 159c 00000000 		.long	.LASF258
 2701 15a0 140A0000 		.long	0xa14
 2702 15a4 42       		.uleb128 0x42
 2703 15a5 21080000 		.long	0x821
 2704 15a9 09       		.uleb128 0x9
 2705 15aa 03       		.byte	0x3
 2706 15ab 00000000 		.quad	_ZStL8__ioinit
 2706      00000000 
 2707 15b3 43       		.uleb128 0x43
 2708 15b4 7E080000 		.long	0x87e
 2709 15b8 00000000 		.long	.LASF259
 2710 15bc 80808080 		.sleb128 -2147483648
 2710      78
 2711 15c1 44       		.uleb128 0x44
 2712 15c2 89080000 		.long	0x889
 2713 15c6 00000000 		.long	.LASF260
 2714 15ca FFFFFF7F 		.long	0x7fffffff
 2715 15ce 45       		.uleb128 0x45
 2716 15cf E1080000 		.long	0x8e1
 2717 15d3 00000000 		.long	.LASF261
 2718 15d7 40       		.byte	0x40
 2719 15d8 45       		.uleb128 0x45
 2720 15d9 0D090000 		.long	0x90d
 2721 15dd 00000000 		.long	.LASF262
 2722 15e1 7F       		.byte	0x7f
 2723 15e2 43       		.uleb128 0x43
 2724 15e3 44090000 		.long	0x944
 2725 15e7 00000000 		.long	.LASF263
 2726 15eb 80807E   		.sleb128 -32768
 2727 15ee 46       		.uleb128 0x46
 2728 15ef 4F090000 		.long	0x94f
 2729 15f3 00000000 		.long	.LASF264
 2730 15f7 FF7F     		.value	0x7fff
 2731 15f9 43       		.uleb128 0x43
 2732 15fa 82090000 		.long	0x982
 2733 15fe 00000000 		.long	.LASF265
 2734 1602 80808080 		.sleb128 -9223372036854775808
 2734      80808080 
 2734      807F
 2735 160c 47       		.uleb128 0x47
 2736 160d 8D090000 		.long	0x98d
 2737 1611 00000000 		.long	.LASF266
 2738 1615 FFFFFFFF 		.quad	0x7fffffffffffffff
 2738      FFFFFF7F 
 2739 161d 00       		.byte	0
 2740              		.section	.debug_abbrev,"",@progbits
 2741              	.Ldebug_abbrev0:
 2742 0000 01       		.uleb128 0x1
 2743 0001 11       		.uleb128 0x11
 2744 0002 01       		.byte	0x1
 2745 0003 25       		.uleb128 0x25
 2746 0004 0E       		.uleb128 0xe
 2747 0005 13       		.uleb128 0x13
 2748 0006 0B       		.uleb128 0xb
GAS LISTING /tmp/cceirXUd.s 			page 52


 2749 0007 03       		.uleb128 0x3
 2750 0008 0E       		.uleb128 0xe
 2751 0009 1B       		.uleb128 0x1b
 2752 000a 0E       		.uleb128 0xe
 2753 000b 11       		.uleb128 0x11
 2754 000c 01       		.uleb128 0x1
 2755 000d 12       		.uleb128 0x12
 2756 000e 07       		.uleb128 0x7
 2757 000f 10       		.uleb128 0x10
 2758 0010 17       		.uleb128 0x17
 2759 0011 00       		.byte	0
 2760 0012 00       		.byte	0
 2761 0013 02       		.uleb128 0x2
 2762 0014 39       		.uleb128 0x39
 2763 0015 01       		.byte	0x1
 2764 0016 03       		.uleb128 0x3
 2765 0017 08       		.uleb128 0x8
 2766 0018 3A       		.uleb128 0x3a
 2767 0019 0B       		.uleb128 0xb
 2768 001a 3B       		.uleb128 0x3b
 2769 001b 0B       		.uleb128 0xb
 2770 001c 01       		.uleb128 0x1
 2771 001d 13       		.uleb128 0x13
 2772 001e 00       		.byte	0
 2773 001f 00       		.byte	0
 2774 0020 03       		.uleb128 0x3
 2775 0021 39       		.uleb128 0x39
 2776 0022 00       		.byte	0
 2777 0023 03       		.uleb128 0x3
 2778 0024 0E       		.uleb128 0xe
 2779 0025 3A       		.uleb128 0x3a
 2780 0026 0B       		.uleb128 0xb
 2781 0027 3B       		.uleb128 0x3b
 2782 0028 0B       		.uleb128 0xb
 2783 0029 00       		.byte	0
 2784 002a 00       		.byte	0
 2785 002b 04       		.uleb128 0x4
 2786 002c 3A       		.uleb128 0x3a
 2787 002d 00       		.byte	0
 2788 002e 3A       		.uleb128 0x3a
 2789 002f 0B       		.uleb128 0xb
 2790 0030 3B       		.uleb128 0x3b
 2791 0031 0B       		.uleb128 0xb
 2792 0032 18       		.uleb128 0x18
 2793 0033 13       		.uleb128 0x13
 2794 0034 00       		.byte	0
 2795 0035 00       		.byte	0
 2796 0036 05       		.uleb128 0x5
 2797 0037 08       		.uleb128 0x8
 2798 0038 00       		.byte	0
 2799 0039 3A       		.uleb128 0x3a
 2800 003a 0B       		.uleb128 0xb
 2801 003b 3B       		.uleb128 0x3b
 2802 003c 0B       		.uleb128 0xb
 2803 003d 18       		.uleb128 0x18
 2804 003e 13       		.uleb128 0x13
 2805 003f 00       		.byte	0
GAS LISTING /tmp/cceirXUd.s 			page 53


 2806 0040 00       		.byte	0
 2807 0041 06       		.uleb128 0x6
 2808 0042 08       		.uleb128 0x8
 2809 0043 00       		.byte	0
 2810 0044 3A       		.uleb128 0x3a
 2811 0045 0B       		.uleb128 0xb
 2812 0046 3B       		.uleb128 0x3b
 2813 0047 05       		.uleb128 0x5
 2814 0048 18       		.uleb128 0x18
 2815 0049 13       		.uleb128 0x13
 2816 004a 00       		.byte	0
 2817 004b 00       		.byte	0
 2818 004c 07       		.uleb128 0x7
 2819 004d 13       		.uleb128 0x13
 2820 004e 01       		.byte	0x1
 2821 004f 03       		.uleb128 0x3
 2822 0050 0E       		.uleb128 0xe
 2823 0051 0B       		.uleb128 0xb
 2824 0052 0B       		.uleb128 0xb
 2825 0053 3A       		.uleb128 0x3a
 2826 0054 0B       		.uleb128 0xb
 2827 0055 3B       		.uleb128 0x3b
 2828 0056 0B       		.uleb128 0xb
 2829 0057 01       		.uleb128 0x1
 2830 0058 13       		.uleb128 0x13
 2831 0059 00       		.byte	0
 2832 005a 00       		.byte	0
 2833 005b 08       		.uleb128 0x8
 2834 005c 16       		.uleb128 0x16
 2835 005d 00       		.byte	0
 2836 005e 03       		.uleb128 0x3
 2837 005f 0E       		.uleb128 0xe
 2838 0060 3A       		.uleb128 0x3a
 2839 0061 0B       		.uleb128 0xb
 2840 0062 3B       		.uleb128 0x3b
 2841 0063 0B       		.uleb128 0xb
 2842 0064 49       		.uleb128 0x49
 2843 0065 13       		.uleb128 0x13
 2844 0066 00       		.byte	0
 2845 0067 00       		.byte	0
 2846 0068 09       		.uleb128 0x9
 2847 0069 2E       		.uleb128 0x2e
 2848 006a 01       		.byte	0x1
 2849 006b 3F       		.uleb128 0x3f
 2850 006c 19       		.uleb128 0x19
 2851 006d 03       		.uleb128 0x3
 2852 006e 0E       		.uleb128 0xe
 2853 006f 3A       		.uleb128 0x3a
 2854 0070 0B       		.uleb128 0xb
 2855 0071 3B       		.uleb128 0x3b
 2856 0072 0B       		.uleb128 0xb
 2857 0073 6E       		.uleb128 0x6e
 2858 0074 0E       		.uleb128 0xe
 2859 0075 3C       		.uleb128 0x3c
 2860 0076 19       		.uleb128 0x19
 2861 0077 01       		.uleb128 0x1
 2862 0078 13       		.uleb128 0x13
GAS LISTING /tmp/cceirXUd.s 			page 54


 2863 0079 00       		.byte	0
 2864 007a 00       		.byte	0
 2865 007b 0A       		.uleb128 0xa
 2866 007c 05       		.uleb128 0x5
 2867 007d 00       		.byte	0
 2868 007e 49       		.uleb128 0x49
 2869 007f 13       		.uleb128 0x13
 2870 0080 00       		.byte	0
 2871 0081 00       		.byte	0
 2872 0082 0B       		.uleb128 0xb
 2873 0083 26       		.uleb128 0x26
 2874 0084 00       		.byte	0
 2875 0085 49       		.uleb128 0x49
 2876 0086 13       		.uleb128 0x13
 2877 0087 00       		.byte	0
 2878 0088 00       		.byte	0
 2879 0089 0C       		.uleb128 0xc
 2880 008a 2E       		.uleb128 0x2e
 2881 008b 01       		.byte	0x1
 2882 008c 3F       		.uleb128 0x3f
 2883 008d 19       		.uleb128 0x19
 2884 008e 03       		.uleb128 0x3
 2885 008f 08       		.uleb128 0x8
 2886 0090 3A       		.uleb128 0x3a
 2887 0091 0B       		.uleb128 0xb
 2888 0092 3B       		.uleb128 0x3b
 2889 0093 0B       		.uleb128 0xb
 2890 0094 6E       		.uleb128 0x6e
 2891 0095 0E       		.uleb128 0xe
 2892 0096 49       		.uleb128 0x49
 2893 0097 13       		.uleb128 0x13
 2894 0098 3C       		.uleb128 0x3c
 2895 0099 19       		.uleb128 0x19
 2896 009a 01       		.uleb128 0x1
 2897 009b 13       		.uleb128 0x13
 2898 009c 00       		.byte	0
 2899 009d 00       		.byte	0
 2900 009e 0D       		.uleb128 0xd
 2901 009f 2E       		.uleb128 0x2e
 2902 00a0 01       		.byte	0x1
 2903 00a1 3F       		.uleb128 0x3f
 2904 00a2 19       		.uleb128 0x19
 2905 00a3 03       		.uleb128 0x3
 2906 00a4 0E       		.uleb128 0xe
 2907 00a5 3A       		.uleb128 0x3a
 2908 00a6 0B       		.uleb128 0xb
 2909 00a7 3B       		.uleb128 0x3b
 2910 00a8 05       		.uleb128 0x5
 2911 00a9 6E       		.uleb128 0x6e
 2912 00aa 0E       		.uleb128 0xe
 2913 00ab 49       		.uleb128 0x49
 2914 00ac 13       		.uleb128 0x13
 2915 00ad 3C       		.uleb128 0x3c
 2916 00ae 19       		.uleb128 0x19
 2917 00af 01       		.uleb128 0x1
 2918 00b0 13       		.uleb128 0x13
 2919 00b1 00       		.byte	0
GAS LISTING /tmp/cceirXUd.s 			page 55


 2920 00b2 00       		.byte	0
 2921 00b3 0E       		.uleb128 0xe
 2922 00b4 2E       		.uleb128 0x2e
 2923 00b5 00       		.byte	0
 2924 00b6 3F       		.uleb128 0x3f
 2925 00b7 19       		.uleb128 0x19
 2926 00b8 03       		.uleb128 0x3
 2927 00b9 08       		.uleb128 0x8
 2928 00ba 3A       		.uleb128 0x3a
 2929 00bb 0B       		.uleb128 0xb
 2930 00bc 3B       		.uleb128 0x3b
 2931 00bd 05       		.uleb128 0x5
 2932 00be 6E       		.uleb128 0x6e
 2933 00bf 0E       		.uleb128 0xe
 2934 00c0 49       		.uleb128 0x49
 2935 00c1 13       		.uleb128 0x13
 2936 00c2 3C       		.uleb128 0x3c
 2937 00c3 19       		.uleb128 0x19
 2938 00c4 00       		.byte	0
 2939 00c5 00       		.byte	0
 2940 00c6 0F       		.uleb128 0xf
 2941 00c7 2E       		.uleb128 0x2e
 2942 00c8 01       		.byte	0x1
 2943 00c9 3F       		.uleb128 0x3f
 2944 00ca 19       		.uleb128 0x19
 2945 00cb 03       		.uleb128 0x3
 2946 00cc 0E       		.uleb128 0xe
 2947 00cd 3A       		.uleb128 0x3a
 2948 00ce 0B       		.uleb128 0xb
 2949 00cf 3B       		.uleb128 0x3b
 2950 00d0 05       		.uleb128 0x5
 2951 00d1 6E       		.uleb128 0x6e
 2952 00d2 0E       		.uleb128 0xe
 2953 00d3 49       		.uleb128 0x49
 2954 00d4 13       		.uleb128 0x13
 2955 00d5 3C       		.uleb128 0x3c
 2956 00d6 19       		.uleb128 0x19
 2957 00d7 00       		.byte	0
 2958 00d8 00       		.byte	0
 2959 00d9 10       		.uleb128 0x10
 2960 00da 04       		.uleb128 0x4
 2961 00db 01       		.byte	0x1
 2962 00dc 03       		.uleb128 0x3
 2963 00dd 0E       		.uleb128 0xe
 2964 00de 0B       		.uleb128 0xb
 2965 00df 0B       		.uleb128 0xb
 2966 00e0 49       		.uleb128 0x49
 2967 00e1 13       		.uleb128 0x13
 2968 00e2 3A       		.uleb128 0x3a
 2969 00e3 0B       		.uleb128 0xb
 2970 00e4 3B       		.uleb128 0x3b
 2971 00e5 0B       		.uleb128 0xb
 2972 00e6 01       		.uleb128 0x1
 2973 00e7 13       		.uleb128 0x13
 2974 00e8 00       		.byte	0
 2975 00e9 00       		.byte	0
 2976 00ea 11       		.uleb128 0x11
GAS LISTING /tmp/cceirXUd.s 			page 56


 2977 00eb 28       		.uleb128 0x28
 2978 00ec 00       		.byte	0
 2979 00ed 03       		.uleb128 0x3
 2980 00ee 0E       		.uleb128 0xe
 2981 00ef 1C       		.uleb128 0x1c
 2982 00f0 0B       		.uleb128 0xb
 2983 00f1 00       		.byte	0
 2984 00f2 00       		.byte	0
 2985 00f3 12       		.uleb128 0x12
 2986 00f4 28       		.uleb128 0x28
 2987 00f5 00       		.byte	0
 2988 00f6 03       		.uleb128 0x3
 2989 00f7 0E       		.uleb128 0xe
 2990 00f8 1C       		.uleb128 0x1c
 2991 00f9 05       		.uleb128 0x5
 2992 00fa 00       		.byte	0
 2993 00fb 00       		.byte	0
 2994 00fc 13       		.uleb128 0x13
 2995 00fd 28       		.uleb128 0x28
 2996 00fe 00       		.byte	0
 2997 00ff 03       		.uleb128 0x3
 2998 0100 0E       		.uleb128 0xe
 2999 0101 1C       		.uleb128 0x1c
 3000 0102 06       		.uleb128 0x6
 3001 0103 00       		.byte	0
 3002 0104 00       		.byte	0
 3003 0105 14       		.uleb128 0x14
 3004 0106 28       		.uleb128 0x28
 3005 0107 00       		.byte	0
 3006 0108 03       		.uleb128 0x3
 3007 0109 0E       		.uleb128 0xe
 3008 010a 1C       		.uleb128 0x1c
 3009 010b 0D       		.uleb128 0xd
 3010 010c 00       		.byte	0
 3011 010d 00       		.byte	0
 3012 010e 15       		.uleb128 0x15
 3013 010f 02       		.uleb128 0x2
 3014 0110 01       		.byte	0x1
 3015 0111 03       		.uleb128 0x3
 3016 0112 0E       		.uleb128 0xe
 3017 0113 3C       		.uleb128 0x3c
 3018 0114 19       		.uleb128 0x19
 3019 0115 01       		.uleb128 0x1
 3020 0116 13       		.uleb128 0x13
 3021 0117 00       		.byte	0
 3022 0118 00       		.byte	0
 3023 0119 16       		.uleb128 0x16
 3024 011a 02       		.uleb128 0x2
 3025 011b 01       		.byte	0x1
 3026 011c 03       		.uleb128 0x3
 3027 011d 0E       		.uleb128 0xe
 3028 011e 0B       		.uleb128 0xb
 3029 011f 0B       		.uleb128 0xb
 3030 0120 3A       		.uleb128 0x3a
 3031 0121 0B       		.uleb128 0xb
 3032 0122 3B       		.uleb128 0x3b
 3033 0123 05       		.uleb128 0x5
GAS LISTING /tmp/cceirXUd.s 			page 57


 3034 0124 32       		.uleb128 0x32
 3035 0125 0B       		.uleb128 0xb
 3036 0126 01       		.uleb128 0x1
 3037 0127 13       		.uleb128 0x13
 3038 0128 00       		.byte	0
 3039 0129 00       		.byte	0
 3040 012a 17       		.uleb128 0x17
 3041 012b 0D       		.uleb128 0xd
 3042 012c 00       		.byte	0
 3043 012d 03       		.uleb128 0x3
 3044 012e 0E       		.uleb128 0xe
 3045 012f 3A       		.uleb128 0x3a
 3046 0130 0B       		.uleb128 0xb
 3047 0131 3B       		.uleb128 0x3b
 3048 0132 05       		.uleb128 0x5
 3049 0133 49       		.uleb128 0x49
 3050 0134 13       		.uleb128 0x13
 3051 0135 3F       		.uleb128 0x3f
 3052 0136 19       		.uleb128 0x19
 3053 0137 3C       		.uleb128 0x3c
 3054 0138 19       		.uleb128 0x19
 3055 0139 00       		.byte	0
 3056 013a 00       		.byte	0
 3057 013b 18       		.uleb128 0x18
 3058 013c 2E       		.uleb128 0x2e
 3059 013d 01       		.byte	0x1
 3060 013e 3F       		.uleb128 0x3f
 3061 013f 19       		.uleb128 0x19
 3062 0140 03       		.uleb128 0x3
 3063 0141 0E       		.uleb128 0xe
 3064 0142 3A       		.uleb128 0x3a
 3065 0143 0B       		.uleb128 0xb
 3066 0144 3B       		.uleb128 0x3b
 3067 0145 05       		.uleb128 0x5
 3068 0146 6E       		.uleb128 0x6e
 3069 0147 0E       		.uleb128 0xe
 3070 0148 32       		.uleb128 0x32
 3071 0149 0B       		.uleb128 0xb
 3072 014a 3C       		.uleb128 0x3c
 3073 014b 19       		.uleb128 0x19
 3074 014c 64       		.uleb128 0x64
 3075 014d 13       		.uleb128 0x13
 3076 014e 01       		.uleb128 0x1
 3077 014f 13       		.uleb128 0x13
 3078 0150 00       		.byte	0
 3079 0151 00       		.byte	0
 3080 0152 19       		.uleb128 0x19
 3081 0153 05       		.uleb128 0x5
 3082 0154 00       		.byte	0
 3083 0155 49       		.uleb128 0x49
 3084 0156 13       		.uleb128 0x13
 3085 0157 34       		.uleb128 0x34
 3086 0158 19       		.uleb128 0x19
 3087 0159 00       		.byte	0
 3088 015a 00       		.byte	0
 3089 015b 1A       		.uleb128 0x1a
 3090 015c 2E       		.uleb128 0x2e
GAS LISTING /tmp/cceirXUd.s 			page 58


 3091 015d 01       		.byte	0x1
 3092 015e 3F       		.uleb128 0x3f
 3093 015f 19       		.uleb128 0x19
 3094 0160 03       		.uleb128 0x3
 3095 0161 0E       		.uleb128 0xe
 3096 0162 3A       		.uleb128 0x3a
 3097 0163 0B       		.uleb128 0xb
 3098 0164 3B       		.uleb128 0x3b
 3099 0165 05       		.uleb128 0x5
 3100 0166 6E       		.uleb128 0x6e
 3101 0167 0E       		.uleb128 0xe
 3102 0168 32       		.uleb128 0x32
 3103 0169 0B       		.uleb128 0xb
 3104 016a 3C       		.uleb128 0x3c
 3105 016b 19       		.uleb128 0x19
 3106 016c 64       		.uleb128 0x64
 3107 016d 13       		.uleb128 0x13
 3108 016e 00       		.byte	0
 3109 016f 00       		.byte	0
 3110 0170 1B       		.uleb128 0x1b
 3111 0171 16       		.uleb128 0x16
 3112 0172 00       		.byte	0
 3113 0173 03       		.uleb128 0x3
 3114 0174 0E       		.uleb128 0xe
 3115 0175 3A       		.uleb128 0x3a
 3116 0176 0B       		.uleb128 0xb
 3117 0177 3B       		.uleb128 0x3b
 3118 0178 05       		.uleb128 0x5
 3119 0179 49       		.uleb128 0x49
 3120 017a 13       		.uleb128 0x13
 3121 017b 32       		.uleb128 0x32
 3122 017c 0B       		.uleb128 0xb
 3123 017d 00       		.byte	0
 3124 017e 00       		.byte	0
 3125 017f 1C       		.uleb128 0x1c
 3126 0180 0D       		.uleb128 0xd
 3127 0181 00       		.byte	0
 3128 0182 03       		.uleb128 0x3
 3129 0183 0E       		.uleb128 0xe
 3130 0184 3A       		.uleb128 0x3a
 3131 0185 0B       		.uleb128 0xb
 3132 0186 3B       		.uleb128 0x3b
 3133 0187 05       		.uleb128 0x5
 3134 0188 49       		.uleb128 0x49
 3135 0189 13       		.uleb128 0x13
 3136 018a 3F       		.uleb128 0x3f
 3137 018b 19       		.uleb128 0x19
 3138 018c 32       		.uleb128 0x32
 3139 018d 0B       		.uleb128 0xb
 3140 018e 3C       		.uleb128 0x3c
 3141 018f 19       		.uleb128 0x19
 3142 0190 1C       		.uleb128 0x1c
 3143 0191 0B       		.uleb128 0xb
 3144 0192 00       		.byte	0
 3145 0193 00       		.byte	0
 3146 0194 1D       		.uleb128 0x1d
 3147 0195 0D       		.uleb128 0xd
GAS LISTING /tmp/cceirXUd.s 			page 59


 3148 0196 00       		.byte	0
 3149 0197 03       		.uleb128 0x3
 3150 0198 08       		.uleb128 0x8
 3151 0199 3A       		.uleb128 0x3a
 3152 019a 0B       		.uleb128 0xb
 3153 019b 3B       		.uleb128 0x3b
 3154 019c 05       		.uleb128 0x5
 3155 019d 49       		.uleb128 0x49
 3156 019e 13       		.uleb128 0x13
 3157 019f 3F       		.uleb128 0x3f
 3158 01a0 19       		.uleb128 0x19
 3159 01a1 32       		.uleb128 0x32
 3160 01a2 0B       		.uleb128 0xb
 3161 01a3 3C       		.uleb128 0x3c
 3162 01a4 19       		.uleb128 0x19
 3163 01a5 1C       		.uleb128 0x1c
 3164 01a6 0B       		.uleb128 0xb
 3165 01a7 00       		.byte	0
 3166 01a8 00       		.byte	0
 3167 01a9 1E       		.uleb128 0x1e
 3168 01aa 0D       		.uleb128 0xd
 3169 01ab 00       		.byte	0
 3170 01ac 03       		.uleb128 0x3
 3171 01ad 0E       		.uleb128 0xe
 3172 01ae 3A       		.uleb128 0x3a
 3173 01af 0B       		.uleb128 0xb
 3174 01b0 3B       		.uleb128 0x3b
 3175 01b1 05       		.uleb128 0x5
 3176 01b2 49       		.uleb128 0x49
 3177 01b3 13       		.uleb128 0x13
 3178 01b4 3F       		.uleb128 0x3f
 3179 01b5 19       		.uleb128 0x19
 3180 01b6 32       		.uleb128 0x32
 3181 01b7 0B       		.uleb128 0xb
 3182 01b8 3C       		.uleb128 0x3c
 3183 01b9 19       		.uleb128 0x19
 3184 01ba 1C       		.uleb128 0x1c
 3185 01bb 05       		.uleb128 0x5
 3186 01bc 00       		.byte	0
 3187 01bd 00       		.byte	0
 3188 01be 1F       		.uleb128 0x1f
 3189 01bf 2F       		.uleb128 0x2f
 3190 01c0 00       		.byte	0
 3191 01c1 03       		.uleb128 0x3
 3192 01c2 0E       		.uleb128 0xe
 3193 01c3 49       		.uleb128 0x49
 3194 01c4 13       		.uleb128 0x13
 3195 01c5 00       		.byte	0
 3196 01c6 00       		.byte	0
 3197 01c7 20       		.uleb128 0x20
 3198 01c8 2F       		.uleb128 0x2f
 3199 01c9 00       		.byte	0
 3200 01ca 03       		.uleb128 0x3
 3201 01cb 0E       		.uleb128 0xe
 3202 01cc 49       		.uleb128 0x49
 3203 01cd 13       		.uleb128 0x13
 3204 01ce 1E       		.uleb128 0x1e
GAS LISTING /tmp/cceirXUd.s 			page 60


 3205 01cf 19       		.uleb128 0x19
 3206 01d0 00       		.byte	0
 3207 01d1 00       		.byte	0
 3208 01d2 21       		.uleb128 0x21
 3209 01d3 34       		.uleb128 0x34
 3210 01d4 00       		.byte	0
 3211 01d5 03       		.uleb128 0x3
 3212 01d6 0E       		.uleb128 0xe
 3213 01d7 3A       		.uleb128 0x3a
 3214 01d8 0B       		.uleb128 0xb
 3215 01d9 3B       		.uleb128 0x3b
 3216 01da 0B       		.uleb128 0xb
 3217 01db 6E       		.uleb128 0x6e
 3218 01dc 0E       		.uleb128 0xe
 3219 01dd 49       		.uleb128 0x49
 3220 01de 13       		.uleb128 0x13
 3221 01df 3F       		.uleb128 0x3f
 3222 01e0 19       		.uleb128 0x19
 3223 01e1 3C       		.uleb128 0x3c
 3224 01e2 19       		.uleb128 0x19
 3225 01e3 00       		.byte	0
 3226 01e4 00       		.byte	0
 3227 01e5 22       		.uleb128 0x22
 3228 01e6 34       		.uleb128 0x34
 3229 01e7 00       		.byte	0
 3230 01e8 03       		.uleb128 0x3
 3231 01e9 0E       		.uleb128 0xe
 3232 01ea 3A       		.uleb128 0x3a
 3233 01eb 0B       		.uleb128 0xb
 3234 01ec 3B       		.uleb128 0x3b
 3235 01ed 0B       		.uleb128 0xb
 3236 01ee 49       		.uleb128 0x49
 3237 01ef 13       		.uleb128 0x13
 3238 01f0 3C       		.uleb128 0x3c
 3239 01f1 19       		.uleb128 0x19
 3240 01f2 00       		.byte	0
 3241 01f3 00       		.byte	0
 3242 01f4 23       		.uleb128 0x23
 3243 01f5 39       		.uleb128 0x39
 3244 01f6 01       		.byte	0x1
 3245 01f7 03       		.uleb128 0x3
 3246 01f8 0E       		.uleb128 0xe
 3247 01f9 3A       		.uleb128 0x3a
 3248 01fa 0B       		.uleb128 0xb
 3249 01fb 3B       		.uleb128 0x3b
 3250 01fc 0B       		.uleb128 0xb
 3251 01fd 01       		.uleb128 0x1
 3252 01fe 13       		.uleb128 0x13
 3253 01ff 00       		.byte	0
 3254 0200 00       		.byte	0
 3255 0201 24       		.uleb128 0x24
 3256 0202 0D       		.uleb128 0xd
 3257 0203 00       		.byte	0
 3258 0204 03       		.uleb128 0x3
 3259 0205 0E       		.uleb128 0xe
 3260 0206 3A       		.uleb128 0x3a
 3261 0207 0B       		.uleb128 0xb
GAS LISTING /tmp/cceirXUd.s 			page 61


 3262 0208 3B       		.uleb128 0x3b
 3263 0209 0B       		.uleb128 0xb
 3264 020a 49       		.uleb128 0x49
 3265 020b 13       		.uleb128 0x13
 3266 020c 3F       		.uleb128 0x3f
 3267 020d 19       		.uleb128 0x19
 3268 020e 3C       		.uleb128 0x3c
 3269 020f 19       		.uleb128 0x19
 3270 0210 00       		.byte	0
 3271 0211 00       		.byte	0
 3272 0212 25       		.uleb128 0x25
 3273 0213 13       		.uleb128 0x13
 3274 0214 01       		.byte	0x1
 3275 0215 03       		.uleb128 0x3
 3276 0216 0E       		.uleb128 0xe
 3277 0217 0B       		.uleb128 0xb
 3278 0218 0B       		.uleb128 0xb
 3279 0219 3A       		.uleb128 0x3a
 3280 021a 0B       		.uleb128 0xb
 3281 021b 3B       		.uleb128 0x3b
 3282 021c 0B       		.uleb128 0xb
 3283 021d 00       		.byte	0
 3284 021e 00       		.byte	0
 3285 021f 26       		.uleb128 0x26
 3286 0220 13       		.uleb128 0x13
 3287 0221 00       		.byte	0
 3288 0222 03       		.uleb128 0x3
 3289 0223 0E       		.uleb128 0xe
 3290 0224 3C       		.uleb128 0x3c
 3291 0225 19       		.uleb128 0x19
 3292 0226 00       		.byte	0
 3293 0227 00       		.byte	0
 3294 0228 27       		.uleb128 0x27
 3295 0229 24       		.uleb128 0x24
 3296 022a 00       		.byte	0
 3297 022b 0B       		.uleb128 0xb
 3298 022c 0B       		.uleb128 0xb
 3299 022d 3E       		.uleb128 0x3e
 3300 022e 0B       		.uleb128 0xb
 3301 022f 03       		.uleb128 0x3
 3302 0230 0E       		.uleb128 0xe
 3303 0231 00       		.byte	0
 3304 0232 00       		.byte	0
 3305 0233 28       		.uleb128 0x28
 3306 0234 0D       		.uleb128 0xd
 3307 0235 00       		.byte	0
 3308 0236 03       		.uleb128 0x3
 3309 0237 0E       		.uleb128 0xe
 3310 0238 3A       		.uleb128 0x3a
 3311 0239 0B       		.uleb128 0xb
 3312 023a 3B       		.uleb128 0x3b
 3313 023b 0B       		.uleb128 0xb
 3314 023c 49       		.uleb128 0x49
 3315 023d 13       		.uleb128 0x13
 3316 023e 38       		.uleb128 0x38
 3317 023f 0B       		.uleb128 0xb
 3318 0240 00       		.byte	0
GAS LISTING /tmp/cceirXUd.s 			page 62


 3319 0241 00       		.byte	0
 3320 0242 29       		.uleb128 0x29
 3321 0243 0F       		.uleb128 0xf
 3322 0244 00       		.byte	0
 3323 0245 0B       		.uleb128 0xb
 3324 0246 0B       		.uleb128 0xb
 3325 0247 00       		.byte	0
 3326 0248 00       		.byte	0
 3327 0249 2A       		.uleb128 0x2a
 3328 024a 16       		.uleb128 0x16
 3329 024b 00       		.byte	0
 3330 024c 03       		.uleb128 0x3
 3331 024d 0E       		.uleb128 0xe
 3332 024e 3A       		.uleb128 0x3a
 3333 024f 0B       		.uleb128 0xb
 3334 0250 3B       		.uleb128 0x3b
 3335 0251 05       		.uleb128 0x5
 3336 0252 49       		.uleb128 0x49
 3337 0253 13       		.uleb128 0x13
 3338 0254 00       		.byte	0
 3339 0255 00       		.byte	0
 3340 0256 2B       		.uleb128 0x2b
 3341 0257 13       		.uleb128 0x13
 3342 0258 01       		.byte	0x1
 3343 0259 0B       		.uleb128 0xb
 3344 025a 0B       		.uleb128 0xb
 3345 025b 3A       		.uleb128 0x3a
 3346 025c 0B       		.uleb128 0xb
 3347 025d 3B       		.uleb128 0x3b
 3348 025e 0B       		.uleb128 0xb
 3349 025f 6E       		.uleb128 0x6e
 3350 0260 0E       		.uleb128 0xe
 3351 0261 01       		.uleb128 0x1
 3352 0262 13       		.uleb128 0x13
 3353 0263 00       		.byte	0
 3354 0264 00       		.byte	0
 3355 0265 2C       		.uleb128 0x2c
 3356 0266 17       		.uleb128 0x17
 3357 0267 01       		.byte	0x1
 3358 0268 0B       		.uleb128 0xb
 3359 0269 0B       		.uleb128 0xb
 3360 026a 3A       		.uleb128 0x3a
 3361 026b 0B       		.uleb128 0xb
 3362 026c 3B       		.uleb128 0x3b
 3363 026d 0B       		.uleb128 0xb
 3364 026e 01       		.uleb128 0x1
 3365 026f 13       		.uleb128 0x13
 3366 0270 00       		.byte	0
 3367 0271 00       		.byte	0
 3368 0272 2D       		.uleb128 0x2d
 3369 0273 0D       		.uleb128 0xd
 3370 0274 00       		.byte	0
 3371 0275 03       		.uleb128 0x3
 3372 0276 0E       		.uleb128 0xe
 3373 0277 3A       		.uleb128 0x3a
 3374 0278 0B       		.uleb128 0xb
 3375 0279 3B       		.uleb128 0x3b
GAS LISTING /tmp/cceirXUd.s 			page 63


 3376 027a 0B       		.uleb128 0xb
 3377 027b 49       		.uleb128 0x49
 3378 027c 13       		.uleb128 0x13
 3379 027d 00       		.byte	0
 3380 027e 00       		.byte	0
 3381 027f 2E       		.uleb128 0x2e
 3382 0280 01       		.uleb128 0x1
 3383 0281 01       		.byte	0x1
 3384 0282 49       		.uleb128 0x49
 3385 0283 13       		.uleb128 0x13
 3386 0284 01       		.uleb128 0x1
 3387 0285 13       		.uleb128 0x13
 3388 0286 00       		.byte	0
 3389 0287 00       		.byte	0
 3390 0288 2F       		.uleb128 0x2f
 3391 0289 21       		.uleb128 0x21
 3392 028a 00       		.byte	0
 3393 028b 49       		.uleb128 0x49
 3394 028c 13       		.uleb128 0x13
 3395 028d 2F       		.uleb128 0x2f
 3396 028e 0B       		.uleb128 0xb
 3397 028f 00       		.byte	0
 3398 0290 00       		.byte	0
 3399 0291 30       		.uleb128 0x30
 3400 0292 24       		.uleb128 0x24
 3401 0293 00       		.byte	0
 3402 0294 0B       		.uleb128 0xb
 3403 0295 0B       		.uleb128 0xb
 3404 0296 3E       		.uleb128 0x3e
 3405 0297 0B       		.uleb128 0xb
 3406 0298 03       		.uleb128 0x3
 3407 0299 08       		.uleb128 0x8
 3408 029a 00       		.byte	0
 3409 029b 00       		.byte	0
 3410 029c 31       		.uleb128 0x31
 3411 029d 0F       		.uleb128 0xf
 3412 029e 00       		.byte	0
 3413 029f 0B       		.uleb128 0xb
 3414 02a0 0B       		.uleb128 0xb
 3415 02a1 49       		.uleb128 0x49
 3416 02a2 13       		.uleb128 0x13
 3417 02a3 00       		.byte	0
 3418 02a4 00       		.byte	0
 3419 02a5 32       		.uleb128 0x32
 3420 02a6 2E       		.uleb128 0x2e
 3421 02a7 01       		.byte	0x1
 3422 02a8 3F       		.uleb128 0x3f
 3423 02a9 19       		.uleb128 0x19
 3424 02aa 03       		.uleb128 0x3
 3425 02ab 0E       		.uleb128 0xe
 3426 02ac 3A       		.uleb128 0x3a
 3427 02ad 0B       		.uleb128 0xb
 3428 02ae 3B       		.uleb128 0x3b
 3429 02af 05       		.uleb128 0x5
 3430 02b0 49       		.uleb128 0x49
 3431 02b1 13       		.uleb128 0x13
 3432 02b2 3C       		.uleb128 0x3c
GAS LISTING /tmp/cceirXUd.s 			page 64


 3433 02b3 19       		.uleb128 0x19
 3434 02b4 01       		.uleb128 0x1
 3435 02b5 13       		.uleb128 0x13
 3436 02b6 00       		.byte	0
 3437 02b7 00       		.byte	0
 3438 02b8 33       		.uleb128 0x33
 3439 02b9 18       		.uleb128 0x18
 3440 02ba 00       		.byte	0
 3441 02bb 00       		.byte	0
 3442 02bc 00       		.byte	0
 3443 02bd 34       		.uleb128 0x34
 3444 02be 2E       		.uleb128 0x2e
 3445 02bf 00       		.byte	0
 3446 02c0 3F       		.uleb128 0x3f
 3447 02c1 19       		.uleb128 0x19
 3448 02c2 03       		.uleb128 0x3
 3449 02c3 0E       		.uleb128 0xe
 3450 02c4 3A       		.uleb128 0x3a
 3451 02c5 0B       		.uleb128 0xb
 3452 02c6 3B       		.uleb128 0x3b
 3453 02c7 05       		.uleb128 0x5
 3454 02c8 49       		.uleb128 0x49
 3455 02c9 13       		.uleb128 0x13
 3456 02ca 3C       		.uleb128 0x3c
 3457 02cb 19       		.uleb128 0x19
 3458 02cc 00       		.byte	0
 3459 02cd 00       		.byte	0
 3460 02ce 35       		.uleb128 0x35
 3461 02cf 2E       		.uleb128 0x2e
 3462 02d0 01       		.byte	0x1
 3463 02d1 3F       		.uleb128 0x3f
 3464 02d2 19       		.uleb128 0x19
 3465 02d3 03       		.uleb128 0x3
 3466 02d4 0E       		.uleb128 0xe
 3467 02d5 3A       		.uleb128 0x3a
 3468 02d6 0B       		.uleb128 0xb
 3469 02d7 3B       		.uleb128 0x3b
 3470 02d8 0B       		.uleb128 0xb
 3471 02d9 49       		.uleb128 0x49
 3472 02da 13       		.uleb128 0x13
 3473 02db 3C       		.uleb128 0x3c
 3474 02dc 19       		.uleb128 0x19
 3475 02dd 01       		.uleb128 0x1
 3476 02de 13       		.uleb128 0x13
 3477 02df 00       		.byte	0
 3478 02e0 00       		.byte	0
 3479 02e1 36       		.uleb128 0x36
 3480 02e2 13       		.uleb128 0x13
 3481 02e3 01       		.byte	0x1
 3482 02e4 03       		.uleb128 0x3
 3483 02e5 08       		.uleb128 0x8
 3484 02e6 0B       		.uleb128 0xb
 3485 02e7 0B       		.uleb128 0xb
 3486 02e8 3A       		.uleb128 0x3a
 3487 02e9 0B       		.uleb128 0xb
 3488 02ea 3B       		.uleb128 0x3b
 3489 02eb 0B       		.uleb128 0xb
GAS LISTING /tmp/cceirXUd.s 			page 65


 3490 02ec 01       		.uleb128 0x1
 3491 02ed 13       		.uleb128 0x13
 3492 02ee 00       		.byte	0
 3493 02ef 00       		.byte	0
 3494 02f0 37       		.uleb128 0x37
 3495 02f1 2E       		.uleb128 0x2e
 3496 02f2 01       		.byte	0x1
 3497 02f3 3F       		.uleb128 0x3f
 3498 02f4 19       		.uleb128 0x19
 3499 02f5 03       		.uleb128 0x3
 3500 02f6 0E       		.uleb128 0xe
 3501 02f7 3A       		.uleb128 0x3a
 3502 02f8 0B       		.uleb128 0xb
 3503 02f9 3B       		.uleb128 0x3b
 3504 02fa 0B       		.uleb128 0xb
 3505 02fb 6E       		.uleb128 0x6e
 3506 02fc 0E       		.uleb128 0xe
 3507 02fd 49       		.uleb128 0x49
 3508 02fe 13       		.uleb128 0x13
 3509 02ff 3C       		.uleb128 0x3c
 3510 0300 19       		.uleb128 0x19
 3511 0301 01       		.uleb128 0x1
 3512 0302 13       		.uleb128 0x13
 3513 0303 00       		.byte	0
 3514 0304 00       		.byte	0
 3515 0305 38       		.uleb128 0x38
 3516 0306 10       		.uleb128 0x10
 3517 0307 00       		.byte	0
 3518 0308 0B       		.uleb128 0xb
 3519 0309 0B       		.uleb128 0xb
 3520 030a 49       		.uleb128 0x49
 3521 030b 13       		.uleb128 0x13
 3522 030c 00       		.byte	0
 3523 030d 00       		.byte	0
 3524 030e 39       		.uleb128 0x39
 3525 030f 2E       		.uleb128 0x2e
 3526 0310 00       		.byte	0
 3527 0311 3F       		.uleb128 0x3f
 3528 0312 19       		.uleb128 0x19
 3529 0313 03       		.uleb128 0x3
 3530 0314 0E       		.uleb128 0xe
 3531 0315 3A       		.uleb128 0x3a
 3532 0316 0B       		.uleb128 0xb
 3533 0317 3B       		.uleb128 0x3b
 3534 0318 0B       		.uleb128 0xb
 3535 0319 49       		.uleb128 0x49
 3536 031a 13       		.uleb128 0x13
 3537 031b 3C       		.uleb128 0x3c
 3538 031c 19       		.uleb128 0x19
 3539 031d 00       		.byte	0
 3540 031e 00       		.byte	0
 3541 031f 3A       		.uleb128 0x3a
 3542 0320 2E       		.uleb128 0x2e
 3543 0321 01       		.byte	0x1
 3544 0322 3F       		.uleb128 0x3f
 3545 0323 19       		.uleb128 0x19
 3546 0324 03       		.uleb128 0x3
GAS LISTING /tmp/cceirXUd.s 			page 66


 3547 0325 0E       		.uleb128 0xe
 3548 0326 3A       		.uleb128 0x3a
 3549 0327 0B       		.uleb128 0xb
 3550 0328 3B       		.uleb128 0x3b
 3551 0329 0B       		.uleb128 0xb
 3552 032a 49       		.uleb128 0x49
 3553 032b 13       		.uleb128 0x13
 3554 032c 11       		.uleb128 0x11
 3555 032d 01       		.uleb128 0x1
 3556 032e 12       		.uleb128 0x12
 3557 032f 07       		.uleb128 0x7
 3558 0330 40       		.uleb128 0x40
 3559 0331 18       		.uleb128 0x18
 3560 0332 9642     		.uleb128 0x2116
 3561 0334 19       		.uleb128 0x19
 3562 0335 01       		.uleb128 0x1
 3563 0336 13       		.uleb128 0x13
 3564 0337 00       		.byte	0
 3565 0338 00       		.byte	0
 3566 0339 3B       		.uleb128 0x3b
 3567 033a 34       		.uleb128 0x34
 3568 033b 00       		.byte	0
 3569 033c 03       		.uleb128 0x3
 3570 033d 08       		.uleb128 0x8
 3571 033e 3A       		.uleb128 0x3a
 3572 033f 0B       		.uleb128 0xb
 3573 0340 3B       		.uleb128 0x3b
 3574 0341 0B       		.uleb128 0xb
 3575 0342 49       		.uleb128 0x49
 3576 0343 13       		.uleb128 0x13
 3577 0344 02       		.uleb128 0x2
 3578 0345 18       		.uleb128 0x18
 3579 0346 00       		.byte	0
 3580 0347 00       		.byte	0
 3581 0348 3C       		.uleb128 0x3c
 3582 0349 0B       		.uleb128 0xb
 3583 034a 01       		.byte	0x1
 3584 034b 11       		.uleb128 0x11
 3585 034c 01       		.uleb128 0x1
 3586 034d 12       		.uleb128 0x12
 3587 034e 07       		.uleb128 0x7
 3588 034f 00       		.byte	0
 3589 0350 00       		.byte	0
 3590 0351 3D       		.uleb128 0x3d
 3591 0352 2E       		.uleb128 0x2e
 3592 0353 01       		.byte	0x1
 3593 0354 3F       		.uleb128 0x3f
 3594 0355 19       		.uleb128 0x19
 3595 0356 03       		.uleb128 0x3
 3596 0357 0E       		.uleb128 0xe
 3597 0358 3A       		.uleb128 0x3a
 3598 0359 0B       		.uleb128 0xb
 3599 035a 3B       		.uleb128 0x3b
 3600 035b 0B       		.uleb128 0xb
 3601 035c 6E       		.uleb128 0x6e
 3602 035d 0E       		.uleb128 0xe
 3603 035e 11       		.uleb128 0x11
GAS LISTING /tmp/cceirXUd.s 			page 67


 3604 035f 01       		.uleb128 0x1
 3605 0360 12       		.uleb128 0x12
 3606 0361 07       		.uleb128 0x7
 3607 0362 40       		.uleb128 0x40
 3608 0363 18       		.uleb128 0x18
 3609 0364 9642     		.uleb128 0x2116
 3610 0366 19       		.uleb128 0x19
 3611 0367 01       		.uleb128 0x1
 3612 0368 13       		.uleb128 0x13
 3613 0369 00       		.byte	0
 3614 036a 00       		.byte	0
 3615 036b 3E       		.uleb128 0x3e
 3616 036c 05       		.uleb128 0x5
 3617 036d 00       		.byte	0
 3618 036e 03       		.uleb128 0x3
 3619 036f 0E       		.uleb128 0xe
 3620 0370 3A       		.uleb128 0x3a
 3621 0371 0B       		.uleb128 0xb
 3622 0372 3B       		.uleb128 0x3b
 3623 0373 0B       		.uleb128 0xb
 3624 0374 49       		.uleb128 0x49
 3625 0375 13       		.uleb128 0x13
 3626 0376 02       		.uleb128 0x2
 3627 0377 18       		.uleb128 0x18
 3628 0378 00       		.byte	0
 3629 0379 00       		.byte	0
 3630 037a 3F       		.uleb128 0x3f
 3631 037b 2E       		.uleb128 0x2e
 3632 037c 01       		.byte	0x1
 3633 037d 03       		.uleb128 0x3
 3634 037e 0E       		.uleb128 0xe
 3635 037f 34       		.uleb128 0x34
 3636 0380 19       		.uleb128 0x19
 3637 0381 11       		.uleb128 0x11
 3638 0382 01       		.uleb128 0x1
 3639 0383 12       		.uleb128 0x12
 3640 0384 07       		.uleb128 0x7
 3641 0385 40       		.uleb128 0x40
 3642 0386 18       		.uleb128 0x18
 3643 0387 9642     		.uleb128 0x2116
 3644 0389 19       		.uleb128 0x19
 3645 038a 01       		.uleb128 0x1
 3646 038b 13       		.uleb128 0x13
 3647 038c 00       		.byte	0
 3648 038d 00       		.byte	0
 3649 038e 40       		.uleb128 0x40
 3650 038f 2E       		.uleb128 0x2e
 3651 0390 00       		.byte	0
 3652 0391 03       		.uleb128 0x3
 3653 0392 0E       		.uleb128 0xe
 3654 0393 34       		.uleb128 0x34
 3655 0394 19       		.uleb128 0x19
 3656 0395 11       		.uleb128 0x11
 3657 0396 01       		.uleb128 0x1
 3658 0397 12       		.uleb128 0x12
 3659 0398 07       		.uleb128 0x7
 3660 0399 40       		.uleb128 0x40
GAS LISTING /tmp/cceirXUd.s 			page 68


 3661 039a 18       		.uleb128 0x18
 3662 039b 9642     		.uleb128 0x2116
 3663 039d 19       		.uleb128 0x19
 3664 039e 00       		.byte	0
 3665 039f 00       		.byte	0
 3666 03a0 41       		.uleb128 0x41
 3667 03a1 34       		.uleb128 0x34
 3668 03a2 00       		.byte	0
 3669 03a3 03       		.uleb128 0x3
 3670 03a4 0E       		.uleb128 0xe
 3671 03a5 49       		.uleb128 0x49
 3672 03a6 13       		.uleb128 0x13
 3673 03a7 3F       		.uleb128 0x3f
 3674 03a8 19       		.uleb128 0x19
 3675 03a9 34       		.uleb128 0x34
 3676 03aa 19       		.uleb128 0x19
 3677 03ab 3C       		.uleb128 0x3c
 3678 03ac 19       		.uleb128 0x19
 3679 03ad 00       		.byte	0
 3680 03ae 00       		.byte	0
 3681 03af 42       		.uleb128 0x42
 3682 03b0 34       		.uleb128 0x34
 3683 03b1 00       		.byte	0
 3684 03b2 47       		.uleb128 0x47
 3685 03b3 13       		.uleb128 0x13
 3686 03b4 02       		.uleb128 0x2
 3687 03b5 18       		.uleb128 0x18
 3688 03b6 00       		.byte	0
 3689 03b7 00       		.byte	0
 3690 03b8 43       		.uleb128 0x43
 3691 03b9 34       		.uleb128 0x34
 3692 03ba 00       		.byte	0
 3693 03bb 47       		.uleb128 0x47
 3694 03bc 13       		.uleb128 0x13
 3695 03bd 6E       		.uleb128 0x6e
 3696 03be 0E       		.uleb128 0xe
 3697 03bf 1C       		.uleb128 0x1c
 3698 03c0 0D       		.uleb128 0xd
 3699 03c1 00       		.byte	0
 3700 03c2 00       		.byte	0
 3701 03c3 44       		.uleb128 0x44
 3702 03c4 34       		.uleb128 0x34
 3703 03c5 00       		.byte	0
 3704 03c6 47       		.uleb128 0x47
 3705 03c7 13       		.uleb128 0x13
 3706 03c8 6E       		.uleb128 0x6e
 3707 03c9 0E       		.uleb128 0xe
 3708 03ca 1C       		.uleb128 0x1c
 3709 03cb 06       		.uleb128 0x6
 3710 03cc 00       		.byte	0
 3711 03cd 00       		.byte	0
 3712 03ce 45       		.uleb128 0x45
 3713 03cf 34       		.uleb128 0x34
 3714 03d0 00       		.byte	0
 3715 03d1 47       		.uleb128 0x47
 3716 03d2 13       		.uleb128 0x13
 3717 03d3 6E       		.uleb128 0x6e
GAS LISTING /tmp/cceirXUd.s 			page 69


 3718 03d4 0E       		.uleb128 0xe
 3719 03d5 1C       		.uleb128 0x1c
 3720 03d6 0B       		.uleb128 0xb
 3721 03d7 00       		.byte	0
 3722 03d8 00       		.byte	0
 3723 03d9 46       		.uleb128 0x46
 3724 03da 34       		.uleb128 0x34
 3725 03db 00       		.byte	0
 3726 03dc 47       		.uleb128 0x47
 3727 03dd 13       		.uleb128 0x13
 3728 03de 6E       		.uleb128 0x6e
 3729 03df 0E       		.uleb128 0xe
 3730 03e0 1C       		.uleb128 0x1c
 3731 03e1 05       		.uleb128 0x5
 3732 03e2 00       		.byte	0
 3733 03e3 00       		.byte	0
 3734 03e4 47       		.uleb128 0x47
 3735 03e5 34       		.uleb128 0x34
 3736 03e6 00       		.byte	0
 3737 03e7 47       		.uleb128 0x47
 3738 03e8 13       		.uleb128 0x13
 3739 03e9 6E       		.uleb128 0x6e
 3740 03ea 0E       		.uleb128 0xe
 3741 03eb 1C       		.uleb128 0x1c
 3742 03ec 07       		.uleb128 0x7
 3743 03ed 00       		.byte	0
 3744 03ee 00       		.byte	0
 3745 03ef 00       		.byte	0
 3746              		.section	.debug_aranges,"",@progbits
 3747 0000 2C000000 		.long	0x2c
 3748 0004 0200     		.value	0x2
 3749 0006 00000000 		.long	.Ldebug_info0
 3750 000a 08       		.byte	0x8
 3751 000b 00       		.byte	0
 3752 000c 0000     		.value	0
 3753 000e 0000     		.value	0
 3754 0010 00000000 		.quad	.Ltext0
 3754      00000000 
 3755 0018 0F010000 		.quad	.Letext0-.Ltext0
 3755      00000000 
 3756 0020 00000000 		.quad	0
 3756      00000000 
 3757 0028 00000000 		.quad	0
 3757      00000000 
 3758              		.section	.debug_line,"",@progbits
 3759              	.Ldebug_line0:
 3760 0000 5B020000 		.section	.debug_str,"MS",@progbits,1
 3760      02001B02 
 3760      00000101 
 3760      FB0E0D00 
 3760      01010101 
 3761              	.LASF283:
 3762 0000 5F474C4F 		.string	"_GLOBAL__sub_I_main"
 3762      42414C5F 
 3762      5F737562 
 3762      5F495F6D 
 3762      61696E00 
GAS LISTING /tmp/cceirXUd.s 			page 70


 3763              	.LASF70:
 3764 0014 5F535F65 		.string	"_S_end"
 3764      6E6400
 3765              	.LASF25:
 3766 001b 73697A65 		.string	"size_t"
 3766      5F7400
 3767              	.LASF123:
 3768 0022 73697A65 		.string	"sizetype"
 3768      74797065 
 3768      00
 3769              	.LASF169:
 3770 002b 746D5F68 		.string	"tm_hour"
 3770      6F757200 
 3771              	.LASF110:
 3772 0033 5F5F6973 		.string	"__is_signed"
 3772      5F736967 
 3772      6E656400 
 3773              	.LASF58:
 3774 003f 5F535F69 		.string	"_S_ios_openmode_min"
 3774      6F735F6F 
 3774      70656E6D 
 3774      6F64655F 
 3774      6D696E00 
 3775              	.LASF107:
 3776 0053 5F5F6E75 		.string	"__numeric_traits_integer<int>"
 3776      6D657269 
 3776      635F7472 
 3776      61697473 
 3776      5F696E74 
 3777              	.LASF260:
 3778 0071 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIiE5__maxE"
 3778      5F5F676E 
 3778      755F6378 
 3778      7832345F 
 3778      5F6E756D 
 3779              	.LASF76:
 3780 00a3 626F6F6C 		.string	"boolalpha"
 3780      616C7068 
 3780      6100
 3781              	.LASF81:
 3782 00ad 73636965 		.string	"scientific"
 3782      6E746966 
 3782      696300
 3783              	.LASF109:
 3784 00b8 5F5F6D61 		.string	"__max"
 3784      7800
 3785              	.LASF165:
 3786 00be 77637363 		.string	"wcscspn"
 3786      73706E00 
 3787              	.LASF243:
 3788 00c6 6C6F6361 		.string	"localeconv"
 3788      6C65636F 
 3788      6E7600
 3789              	.LASF47:
 3790 00d1 5F535F69 		.string	"_S_ios_fmtflags_min"
 3790      6F735F66 
 3790      6D74666C 
GAS LISTING /tmp/cceirXUd.s 			page 71


 3790      6167735F 
 3790      6D696E00 
 3791              	.LASF228:
 3792 00e5 66726163 		.string	"frac_digits"
 3792      5F646967 
 3792      69747300 
 3793              	.LASF65:
 3794 00f1 5F535F69 		.string	"_S_ios_iostate_max"
 3794      6F735F69 
 3794      6F737461 
 3794      74655F6D 
 3794      617800
 3795              	.LASF220:
 3796 0104 696E745F 		.string	"int_curr_symbol"
 3796      63757272 
 3796      5F73796D 
 3796      626F6C00 
 3797              	.LASF96:
 3798 0114 676F6F64 		.string	"goodbit"
 3798      62697400 
 3799              	.LASF200:
 3800 011c 77637363 		.string	"wcschr"
 3800      687200
 3801              	.LASF27:
 3802 0123 5F535F62 		.string	"_S_boolalpha"
 3802      6F6F6C61 
 3802      6C706861 
 3802      00
 3803              	.LASF61:
 3804 0130 5F535F62 		.string	"_S_badbit"
 3804      61646269 
 3804      7400
 3805              	.LASF95:
 3806 013a 6661696C 		.string	"failbit"
 3806      62697400 
 3807              	.LASF231:
 3808 0142 6E5F6373 		.string	"n_cs_precedes"
 3808      5F707265 
 3808      63656465 
 3808      7300
 3809              	.LASF146:
 3810 0150 6D627274 		.string	"mbrtowc"
 3810      6F776300 
 3811              	.LASF192:
 3812 0158 77637378 		.string	"wcsxfrm"
 3812      66726D00 
 3813              	.LASF227:
 3814 0160 696E745F 		.string	"int_frac_digits"
 3814      66726163 
 3814      5F646967 
 3814      69747300 
 3815              	.LASF68:
 3816 0170 5F535F62 		.string	"_S_beg"
 3816      656700
 3817              	.LASF163:
 3818 0177 77637363 		.string	"wcscoll"
 3818      6F6C6C00 
GAS LISTING /tmp/cceirXUd.s 			page 72


 3819              	.LASF85:
 3820 017f 736B6970 		.string	"skipws"
 3820      777300
 3821              	.LASF127:
 3822 0186 5F5F7763 		.string	"__wch"
 3822      6800
 3823              	.LASF13:
 3824 018c 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4moveEPcPKcm"
 3824      74313163 
 3824      6861725F 
 3824      74726169 
 3824      74734963 
 3825              	.LASF87:
 3826 01ae 75707065 		.string	"uppercase"
 3826      72636173 
 3826      6500
 3827              	.LASF43:
 3828 01b8 5F535F62 		.string	"_S_basefield"
 3828      61736566 
 3828      69656C64 
 3828      00
 3829              	.LASF254:
 3830 01c5 73686F77 		.string	"show"
 3830      00
 3831              	.LASF21:
 3832 01ca 5F5A4E53 		.string	"_ZNSt11char_traitsIcE11to_int_typeERKc"
 3832      74313163 
 3832      6861725F 
 3832      74726169 
 3832      74734963 
 3833              	.LASF222:
 3834 01f1 6D6F6E5F 		.string	"mon_decimal_point"
 3834      64656369 
 3834      6D616C5F 
 3834      706F696E 
 3834      7400
 3835              	.LASF190:
 3836 0203 6C6F6E67 		.string	"long int"
 3836      20696E74 
 3836      00
 3837              	.LASF115:
 3838 020c 5F5F6E75 		.string	"__numeric_traits_integer<char>"
 3838      6D657269 
 3838      635F7472 
 3838      61697473 
 3838      5F696E74 
 3839              	.LASF158:
 3840 022b 76777072 		.string	"vwprintf"
 3840      696E7466 
 3840      00
 3841              	.LASF49:
 3842 0234 5F496F73 		.string	"_Ios_Openmode"
 3842      5F4F7065 
 3842      6E6D6F64 
 3842      6500
 3843              	.LASF3:
 3844 0242 696E745F 		.string	"int_type"
GAS LISTING /tmp/cceirXUd.s 			page 73


 3844      74797065 
 3844      00
 3845              	.LASF253:
 3846 024b 6D61696E 		.string	"main"
 3846      00
 3847              	.LASF237:
 3848 0250 696E745F 		.string	"int_n_cs_precedes"
 3848      6E5F6373 
 3848      5F707265 
 3848      63656465 
 3848      7300
 3849              	.LASF249:
 3850 0262 746F7763 		.string	"towctrans"
 3850      7472616E 
 3850      7300
 3851              	.LASF14:
 3852 026c 636F7079 		.string	"copy"
 3852      00
 3853              	.LASF5:
 3854 0271 5F5A4E53 		.string	"_ZNSt11char_traitsIcE2ltERKcS2_"
 3854      74313163 
 3854      6861725F 
 3854      74726169 
 3854      74734963 
 3855              	.LASF252:
 3856 0291 5F5F696F 		.string	"__ioinit"
 3856      696E6974 
 3856      00
 3857              	.LASF73:
 3858 029a 5F535F73 		.string	"_S_synced_with_stdio"
 3858      796E6365 
 3858      645F7769 
 3858      74685F73 
 3858      7464696F 
 3859              	.LASF113:
 3860 02af 5F56616C 		.string	"_Value"
 3860      756500
 3861              	.LASF62:
 3862 02b6 5F535F65 		.string	"_S_eofbit"
 3862      6F666269 
 3862      7400
 3863              	.LASF268:
 3864 02c0 73636F70 		.string	"scope-hiding.cpp"
 3864      652D6869 
 3864      64696E67 
 3864      2E637070 
 3864      00
 3865              	.LASF174:
 3866 02d1 746D5F79 		.string	"tm_yday"
 3866      64617900 
 3867              	.LASF212:
 3868 02d9 7369676E 		.string	"signed char"
 3868      65642063 
 3868      68617200 
 3869              	.LASF279:
 3870 02e5 5F494F5F 		.string	"_IO_FILE"
 3870      46494C45 
GAS LISTING /tmp/cceirXUd.s 			page 74


 3870      00
 3871              	.LASF102:
 3872 02ee 62617369 		.string	"basic_ostream<char, std::char_traits<char> >"
 3872      635F6F73 
 3872      74726561 
 3872      6D3C6368 
 3872      61722C20 
 3873              	.LASF130:
 3874 031b 5F5F7661 		.string	"__value"
 3874      6C756500 
 3875              	.LASF246:
 3876 0323 77637479 		.string	"wctype_t"
 3876      70655F74 
 3876      00
 3877              	.LASF136:
 3878 032c 66676574 		.string	"fgetwc"
 3878      776300
 3879              	.LASF242:
 3880 0333 67657477 		.string	"getwchar"
 3880      63686172 
 3880      00
 3881              	.LASF137:
 3882 033c 66676574 		.string	"fgetws"
 3882      777300
 3883              	.LASF34:
 3884 0343 5F535F72 		.string	"_S_right"
 3884      69676874 
 3884      00
 3885              	.LASF2:
 3886 034c 63686172 		.string	"char_type"
 3886      5F747970 
 3886      6500
 3887              	.LASF211:
 3888 0356 756E7369 		.string	"unsigned char"
 3888      676E6564 
 3888      20636861 
 3888      7200
 3889              	.LASF232:
 3890 0364 6E5F7365 		.string	"n_sep_by_space"
 3890      705F6279 
 3890      5F737061 
 3890      636500
 3891              	.LASF204:
 3892 0373 776D656D 		.string	"wmemchr"
 3892      63687200 
 3893              	.LASF60:
 3894 037b 5F535F67 		.string	"_S_goodbit"
 3894      6F6F6462 
 3894      697400
 3895              	.LASF264:
 3896 0386 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIsE5__maxE"
 3896      5F5F676E 
 3896      755F6378 
 3896      7832345F 
 3896      5F6E756D 
 3897              	.LASF52:
 3898 03b8 5F535F62 		.string	"_S_bin"
GAS LISTING /tmp/cceirXUd.s 			page 75


 3898      696E00
 3899              	.LASF162:
 3900 03bf 77637363 		.string	"wcscmp"
 3900      6D7000
 3901              	.LASF24:
 3902 03c6 6E6F745F 		.string	"not_eof"
 3902      656F6600 
 3903              	.LASF151:
 3904 03ce 73777072 		.string	"swprintf"
 3904      696E7466 
 3904      00
 3905              	.LASF201:
 3906 03d7 77637370 		.string	"wcspbrk"
 3906      62726B00 
 3907              	.LASF54:
 3908 03df 5F535F6F 		.string	"_S_out"
 3908      757400
 3909              	.LASF131:
 3910 03e6 63686172 		.string	"char"
 3910      00
 3911              	.LASF50:
 3912 03eb 5F535F61 		.string	"_S_app"
 3912      707000
 3913              	.LASF133:
 3914 03f2 6D627374 		.string	"mbstate_t"
 3914      6174655F 
 3914      7400
 3915              	.LASF251:
 3916 03fc 77637479 		.string	"wctype"
 3916      706500
 3917              	.LASF97:
 3918 0403 6F70656E 		.string	"openmode"
 3918      6D6F6465 
 3918      00
 3919              	.LASF180:
 3920 040c 7763736E 		.string	"wcsncmp"
 3920      636D7000 
 3921              	.LASF240:
 3922 0414 696E745F 		.string	"int_n_sign_posn"
 3922      6E5F7369 
 3922      676E5F70 
 3922      6F736E00 
 3923              	.LASF234:
 3924 0424 6E5F7369 		.string	"n_sign_posn"
 3924      676E5F70 
 3924      6F736E00 
 3925              	.LASF75:
 3926 0430 5F5A4E53 		.string	"_ZNSt8ios_base4InitD4Ev"
 3926      7438696F 
 3926      735F6261 
 3926      73653449 
 3926      6E697444 
 3927              	.LASF196:
 3928 0448 776D656D 		.string	"wmemmove"
 3928      6D6F7665 
 3928      00
 3929              	.LASF108:
GAS LISTING /tmp/cceirXUd.s 			page 76


 3930 0451 5F5F6D69 		.string	"__min"
 3930      6E00
 3931              	.LASF135:
 3932 0457 62746F77 		.string	"btowc"
 3932      6300
 3933              	.LASF199:
 3934 045d 77736361 		.string	"wscanf"
 3934      6E6600
 3935              	.LASF223:
 3936 0464 6D6F6E5F 		.string	"mon_thousands_sep"
 3936      74686F75 
 3936      73616E64 
 3936      735F7365 
 3936      7000
 3937              	.LASF153:
 3938 0476 756E6765 		.string	"ungetwc"
 3938      74776300 
 3939              	.LASF120:
 3940 047e 66705F6F 		.string	"fp_offset"
 3940      66667365 
 3940      7400
 3941              	.LASF26:
 3942 0488 70747264 		.string	"ptrdiff_t"
 3942      6966665F 
 3942      7400
 3943              	.LASF259:
 3944 0492 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIiE5__minE"
 3944      5F5F676E 
 3944      755F6378 
 3944      7832345F 
 3944      5F6E756D 
 3945              	.LASF247:
 3946 04c4 77637472 		.string	"wctrans_t"
 3946      616E735F 
 3946      7400
 3947              	.LASF145:
 3948 04ce 6D62726C 		.string	"mbrlen"
 3948      656E00
 3949              	.LASF226:
 3950 04d5 6E656761 		.string	"negative_sign"
 3950      74697665 
 3950      5F736967 
 3950      6E00
 3951              	.LASF30:
 3952 04e3 5F535F68 		.string	"_S_hex"
 3952      657800
 3953              	.LASF235:
 3954 04ea 696E745F 		.string	"int_p_cs_precedes"
 3954      705F6373 
 3954      5F707265 
 3954      63656465 
 3954      7300
 3955              	.LASF142:
 3956 04fc 66777072 		.string	"fwprintf"
 3956      696E7466 
 3956      00
 3957              	.LASF66:
GAS LISTING /tmp/cceirXUd.s 			page 77


 3958 0505 5F535F69 		.string	"_S_ios_iostate_min"
 3958      6F735F69 
 3958      6F737461 
 3958      74655F6D 
 3958      696E00
 3959              	.LASF276:
 3960 0518 636F7574 		.string	"cout"
 3960      00
 3961              	.LASF209:
 3962 051d 77637374 		.string	"wcstoull"
 3962      6F756C6C 
 3962      00
 3963              	.LASF31:
 3964 0526 5F535F69 		.string	"_S_internal"
 3964      6E746572 
 3964      6E616C00 
 3965              	.LASF6:
 3966 0532 636F6D70 		.string	"compare"
 3966      61726500 
 3967              	.LASF170:
 3968 053a 746D5F6D 		.string	"tm_mday"
 3968      64617900 
 3969              	.LASF89:
 3970 0542 62617365 		.string	"basefield"
 3970      6669656C 
 3970      6400
 3971              	.LASF164:
 3972 054c 77637363 		.string	"wcscpy"
 3972      707900
 3973              	.LASF112:
 3974 0553 5F436861 		.string	"_CharT"
 3974      725400
 3975              	.LASF77:
 3976 055a 66697865 		.string	"fixed"
 3976      6400
 3977              	.LASF156:
 3978 0560 76737770 		.string	"vswprintf"
 3978      72696E74 
 3978      6600
 3979              	.LASF197:
 3980 056a 776D656D 		.string	"wmemset"
 3980      73657400 
 3981              	.LASF100:
 3982 0572 7365656B 		.string	"seekdir"
 3982      64697200 
 3983              	.LASF141:
 3984 057a 66776964 		.string	"fwide"
 3984      6500
 3985              	.LASF79:
 3986 0580 6C656674 		.string	"left"
 3986      00
 3987              	.LASF167:
 3988 0585 746D5F73 		.string	"tm_sec"
 3988      656300
 3989              	.LASF175:
 3990 058c 746D5F69 		.string	"tm_isdst"
 3990      73647374 
GAS LISTING /tmp/cceirXUd.s 			page 78


 3990      00
 3991              	.LASF181:
 3992 0595 7763736E 		.string	"wcsncpy"
 3992      63707900 
 3993              	.LASF150:
 3994 059d 70757477 		.string	"putwchar"
 3994      63686172 
 3994      00
 3995              	.LASF194:
 3996 05a6 776D656D 		.string	"wmemcmp"
 3996      636D7000 
 3997              	.LASF51:
 3998 05ae 5F535F61 		.string	"_S_ate"
 3998      746500
 3999              	.LASF15:
 4000 05b5 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4copyEPcPKcm"
 4000      74313163 
 4000      6861725F 
 4000      74726169 
 4000      74734963 
 4001              	.LASF10:
 4002 05d7 66696E64 		.string	"find"
 4002      00
 4003              	.LASF29:
 4004 05dc 5F535F66 		.string	"_S_fixed"
 4004      69786564 
 4004      00
 4005              	.LASF238:
 4006 05e5 696E745F 		.string	"int_n_sep_by_space"
 4006      6E5F7365 
 4006      705F6279 
 4006      5F737061 
 4006      636500
 4007              	.LASF257:
 4008 05f8 5F5F7072 		.string	"__priority"
 4008      696F7269 
 4008      747900
 4009              	.LASF12:
 4010 0603 6D6F7665 		.string	"move"
 4010      00
 4011              	.LASF36:
 4012 0608 5F535F73 		.string	"_S_showbase"
 4012      686F7762 
 4012      61736500 
 4013              	.LASF53:
 4014 0614 5F535F69 		.string	"_S_in"
 4014      6E00
 4015              	.LASF214:
 4016 061a 5F5F676E 		.string	"__gnu_debug"
 4016      755F6465 
 4016      62756700 
 4017              	.LASF269:
 4018 0626 2F686F6D 		.string	"/home/vinu/cs4230/par-lang/c++/discover_modern_c++/basics"
 4018      652F7669 
 4018      6E752F63 
 4018      73343233 
 4018      302F7061 
GAS LISTING /tmp/cceirXUd.s 			page 79


 4019              	.LASF154:
 4020 0660 76667770 		.string	"vfwprintf"
 4020      72696E74 
 4020      6600
 4021              	.LASF157:
 4022 066a 76737773 		.string	"vswscanf"
 4022      63616E66 
 4022      00
 4023              	.LASF230:
 4024 0673 705F7365 		.string	"p_sep_by_space"
 4024      705F6279 
 4024      5F737061 
 4024      636500
 4025              	.LASF22:
 4026 0682 65715F69 		.string	"eq_int_type"
 4026      6E745F74 
 4026      79706500 
 4027              	.LASF72:
 4028 068e 5F535F72 		.string	"_S_refcount"
 4028      6566636F 
 4028      756E7400 
 4029              	.LASF19:
 4030 069a 5F5A4E53 		.string	"_ZNSt11char_traitsIcE12to_char_typeERKi"
 4030      74313163 
 4030      6861725F 
 4030      74726169 
 4030      74734963 
 4031              	.LASF55:
 4032 06c2 5F535F74 		.string	"_S_trunc"
 4032      72756E63 
 4032      00
 4033              	.LASF256:
 4034 06cb 5F5F696E 		.string	"__initialize_p"
 4034      69746961 
 4034      6C697A65 
 4034      5F7000
 4035              	.LASF80:
 4036 06da 72696768 		.string	"right"
 4036      7400
 4037              	.LASF38:
 4038 06e0 5F535F73 		.string	"_S_showpos"
 4038      686F7770 
 4038      6F7300
 4039              	.LASF132:
 4040 06eb 5F5F6D62 		.string	"__mbstate_t"
 4040      73746174 
 4040      655F7400 
 4041              	.LASF195:
 4042 06f7 776D656D 		.string	"wmemcpy"
 4042      63707900 
 4043              	.LASF171:
 4044 06ff 746D5F6D 		.string	"tm_mon"
 4044      6F6E00
 4045              	.LASF28:
 4046 0706 5F535F64 		.string	"_S_dec"
 4046      656300
 4047              	.LASF48:
GAS LISTING /tmp/cceirXUd.s 			page 80


 4048 070d 5F496F73 		.string	"_Ios_Fmtflags"
 4048      5F466D74 
 4048      666C6167 
 4048      7300
 4049              	.LASF189:
 4050 071b 77637374 		.string	"wcstol"
 4050      6F6C00
 4051              	.LASF185:
 4052 0722 646F7562 		.string	"double"
 4052      6C6500
 4053              	.LASF11:
 4054 0729 5F5A4E53 		.string	"_ZNSt11char_traitsIcE4findEPKcmRS1_"
 4054      74313163 
 4054      6861725F 
 4054      74726169 
 4054      74734963 
 4055              	.LASF263:
 4056 074d 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIsE5__minE"
 4056      5F5F676E 
 4056      755F6378 
 4056      7832345F 
 4056      5F6E756D 
 4057              	.LASF193:
 4058 077f 7763746F 		.string	"wctob"
 4058      6200
 4059              	.LASF37:
 4060 0785 5F535F73 		.string	"_S_showpoint"
 4060      686F7770 
 4060      6F696E74 
 4060      00
 4061              	.LASF39:
 4062 0792 5F535F73 		.string	"_S_skipws"
 4062      6B697077 
 4062      7300
 4063              	.LASF119:
 4064 079c 67705F6F 		.string	"gp_offset"
 4064      66667365 
 4064      7400
 4065              	.LASF42:
 4066 07a6 5F535F61 		.string	"_S_adjustfield"
 4066      646A7573 
 4066      74666965 
 4066      6C6400
 4067              	.LASF261:
 4068 07b5 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerImE8__digitsE"
 4068      5F5F676E 
 4068      755F6378 
 4068      7832345F 
 4068      5F6E756D 
 4069              	.LASF187:
 4070 07ea 666C6F61 		.string	"float"
 4070      7400
 4071              	.LASF78:
 4072 07f0 696E7465 		.string	"internal"
 4072      726E616C 
 4072      00
 4073              	.LASF277:
GAS LISTING /tmp/cceirXUd.s 			page 81


 4074 07f9 5F5A5374 		.string	"_ZSt4cout"
 4074      34636F75 
 4074      7400
 4075              	.LASF168:
 4076 0803 746D5F6D 		.string	"tm_min"
 4076      696E00
 4077              	.LASF32:
 4078 080a 5F535F6C 		.string	"_S_left"
 4078      65667400 
 4079              	.LASF124:
 4080 0812 756E7369 		.string	"unsigned int"
 4080      676E6564 
 4080      20696E74 
 4080      00
 4081              	.LASF265:
 4082 081f 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIlE5__minE"
 4082      5F5F676E 
 4082      755F6378 
 4082      7832345F 
 4082      5F6E756D 
 4083              	.LASF106:
 4084 0851 63686172 		.string	"char_traits<char>"
 4084      5F747261 
 4084      6974733C 
 4084      63686172 
 4084      3E00
 4085              	.LASF225:
 4086 0863 706F7369 		.string	"positive_sign"
 4086      74697665 
 4086      5F736967 
 4086      6E00
 4087              	.LASF56:
 4088 0871 5F535F69 		.string	"_S_ios_openmode_end"
 4088      6F735F6F 
 4088      70656E6D 
 4088      6F64655F 
 4088      656E6400 
 4089              	.LASF183:
 4090 0885 77637373 		.string	"wcsspn"
 4090      706E00
 4091              	.LASF233:
 4092 088c 705F7369 		.string	"p_sign_posn"
 4092      676E5F70 
 4092      6F736E00 
 4093              	.LASF23:
 4094 0898 5F5A4E53 		.string	"_ZNSt11char_traitsIcE11eq_int_typeERKiS2_"
 4094      74313163 
 4094      6861725F 
 4094      74726169 
 4094      74734963 
 4095              	.LASF41:
 4096 08c2 5F535F75 		.string	"_S_uppercase"
 4096      70706572 
 4096      63617365 
 4096      00
 4097              	.LASF245:
 4098 08cf 5F41746F 		.string	"_Atomic_word"
GAS LISTING /tmp/cceirXUd.s 			page 82


 4098      6D69635F 
 4098      776F7264 
 4098      00
 4099              	.LASF82:
 4100 08dc 73686F77 		.string	"showbase"
 4100      62617365 
 4100      00
 4101              	.LASF121:
 4102 08e5 6F766572 		.string	"overflow_arg_area"
 4102      666C6F77 
 4102      5F617267 
 4102      5F617265 
 4102      6100
 4103              	.LASF45:
 4104 08f7 5F535F69 		.string	"_S_ios_fmtflags_end"
 4104      6F735F66 
 4104      6D74666C 
 4104      6167735F 
 4104      656E6400 
 4105              	.LASF273:
 4106 090b 496E6974 		.string	"Init"
 4106      00
 4107              	.LASF103:
 4108 0910 6F737472 		.string	"ostream"
 4108      65616D00 
 4109              	.LASF217:
 4110 0918 64656369 		.string	"decimal_point"
 4110      6D616C5F 
 4110      706F696E 
 4110      7400
 4111              	.LASF129:
 4112 0926 5F5F636F 		.string	"__count"
 4112      756E7400 
 4113              	.LASF104:
 4114 092e 5F5F676E 		.string	"__gnu_cxx"
 4114      755F6378 
 4114      7800
 4115              	.LASF215:
 4116 0938 626F6F6C 		.string	"bool"
 4116      00
 4117              	.LASF267:
 4118 093d 474E5520 		.string	"GNU C++ 5.4.1 20160904 -mtune=generic -march=x86-64 -g -O0 -fstack-protector-strong"
 4118      432B2B20 
 4118      352E342E 
 4118      31203230 
 4118      31363039 
 4119              	.LASF17:
 4120 0991 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6assignEPcmc"
 4120      74313163 
 4120      6861725F 
 4120      74726169 
 4120      74734963 
 4121              	.LASF206:
 4122 09b3 6C6F6E67 		.string	"long double"
 4122      20646F75 
 4122      626C6500 
 4123              	.LASF149:
GAS LISTING /tmp/cceirXUd.s 			page 83


 4124 09bf 70757477 		.string	"putwc"
 4124      6300
 4125              	.LASF278:
 4126 09c5 5F5F6E75 		.string	"__numeric_traits_integer<long int>"
 4126      6D657269 
 4126      635F7472 
 4126      61697473 
 4126      5F696E74 
 4127              	.LASF84:
 4128 09e8 73686F77 		.string	"showpos"
 4128      706F7300 
 4129              	.LASF44:
 4130 09f0 5F535F66 		.string	"_S_floatfield"
 4130      6C6F6174 
 4130      6669656C 
 4130      6400
 4131              	.LASF33:
 4132 09fe 5F535F6F 		.string	"_S_oct"
 4132      637400
 4133              	.LASF128:
 4134 0a05 5F5F7763 		.string	"__wchb"
 4134      686200
 4135              	.LASF98:
 4136 0a0c 62696E61 		.string	"binary"
 4136      727900
 4137              	.LASF282:
 4138 0a13 5F5F7374 		.string	"__static_initialization_and_destruction_0"
 4138      61746963 
 4138      5F696E69 
 4138      7469616C 
 4138      697A6174 
 4139              	.LASF8:
 4140 0a3d 5F5A4E53 		.string	"_ZNSt11char_traitsIcE7compareEPKcS2_m"
 4140      74313163 
 4140      6861725F 
 4140      74726169 
 4140      74734963 
 4141              	.LASF210:
 4142 0a63 6C6F6E67 		.string	"long long unsigned int"
 4142      206C6F6E 
 4142      6720756E 
 4142      7369676E 
 4142      65642069 
 4143              	.LASF122:
 4144 0a7a 7265675F 		.string	"reg_save_area"
 4144      73617665 
 4144      5F617265 
 4144      6100
 4145              	.LASF205:
 4146 0a88 77637374 		.string	"wcstold"
 4146      6F6C6400 
 4147              	.LASF236:
 4148 0a90 696E745F 		.string	"int_p_sep_by_space"
 4148      705F7365 
 4148      705F6279 
 4148      5F737061 
 4148      636500
GAS LISTING /tmp/cceirXUd.s 			page 84


 4149              	.LASF71:
 4150 0aa3 5F535F69 		.string	"_S_ios_seekdir_end"
 4150      6F735F73 
 4150      65656B64 
 4150      69725F65 
 4150      6E6400
 4151              	.LASF7:
 4152 0ab6 6C656E67 		.string	"length"
 4152      746800
 4153              	.LASF207:
 4154 0abd 77637374 		.string	"wcstoll"
 4154      6F6C6C00 
 4155              	.LASF203:
 4156 0ac5 77637373 		.string	"wcsstr"
 4156      747200
 4157              	.LASF59:
 4158 0acc 5F496F73 		.string	"_Ios_Iostate"
 4158      5F496F73 
 4158      74617465 
 4158      00
 4159              	.LASF125:
 4160 0ad9 6C6F6E67 		.string	"long unsigned int"
 4160      20756E73 
 4160      69676E65 
 4160      6420696E 
 4160      7400
 4161              	.LASF262:
 4162 0aeb 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIcE5__maxE"
 4162      5F5F676E 
 4162      755F6378 
 4162      7832345F 
 4162      5F6E756D 
 4163              	.LASF182:
 4164 0b1d 77637372 		.string	"wcsrtombs"
 4164      746F6D62 
 4164      7300
 4165              	.LASF88:
 4166 0b27 61646A75 		.string	"adjustfield"
 4166      73746669 
 4166      656C6400 
 4167              	.LASF173:
 4168 0b33 746D5F77 		.string	"tm_wday"
 4168      64617900 
 4169              	.LASF40:
 4170 0b3b 5F535F75 		.string	"_S_unitbuf"
 4170      6E697462 
 4170      756600
 4171              	.LASF4:
 4172 0b46 5F5A4E53 		.string	"_ZNSt11char_traitsIcE2eqERKcS2_"
 4172      74313163 
 4172      6861725F 
 4172      74726169 
 4172      74734963 
 4173              	.LASF90:
 4174 0b66 666C6F61 		.string	"floatfield"
 4174      74666965 
 4174      6C6400
GAS LISTING /tmp/cceirXUd.s 			page 85


 4175              	.LASF152:
 4176 0b71 73777363 		.string	"swscanf"
 4176      616E6600 
 4177              	.LASF111:
 4178 0b79 5F5F6469 		.string	"__digits"
 4178      67697473 
 4178      00
 4179              	.LASF184:
 4180 0b82 77637374 		.string	"wcstod"
 4180      6F6400
 4181              	.LASF186:
 4182 0b89 77637374 		.string	"wcstof"
 4182      6F6600
 4183              	.LASF188:
 4184 0b90 77637374 		.string	"wcstok"
 4184      6F6B00
 4185              	.LASF0:
 4186 0b97 5F5F6378 		.string	"__cxx11"
 4186      78313100 
 4187              	.LASF99:
 4188 0b9f 7472756E 		.string	"trunc"
 4188      6300
 4189              	.LASF117:
 4190 0ba5 5F5F4649 		.string	"__FILE"
 4190      4C4500
 4191              	.LASF83:
 4192 0bac 73686F77 		.string	"showpoint"
 4192      706F696E 
 4192      7400
 4193              	.LASF241:
 4194 0bb6 7365746C 		.string	"setlocale"
 4194      6F63616C 
 4194      6500
 4195              	.LASF202:
 4196 0bc0 77637372 		.string	"wcsrchr"
 4196      63687200 
 4197              	.LASF143:
 4198 0bc8 66777363 		.string	"fwscanf"
 4198      616E6600 
 4199              	.LASF126:
 4200 0bd0 77696E74 		.string	"wint_t"
 4200      5F7400
 4201              	.LASF57:
 4202 0bd7 5F535F69 		.string	"_S_ios_openmode_max"
 4202      6F735F6F 
 4202      70656E6D 
 4202      6F64655F 
 4202      6D617800 
 4203              	.LASF101:
 4204 0beb 696F735F 		.string	"ios_base"
 4204      62617365 
 4204      00
 4205              	.LASF281:
 4206 0bf4 5F5A3473 		.string	"_Z4showi"
 4206      686F7769 
 4206      00
 4207              	.LASF255:
GAS LISTING /tmp/cceirXUd.s 			page 86


 4208 0bfd 6E756D62 		.string	"number"
 4208      657200
 4209              	.LASF93:
 4210 0c04 62616462 		.string	"badbit"
 4210      697400
 4211              	.LASF250:
 4212 0c0b 77637472 		.string	"wctrans"
 4212      616E7300 
 4213              	.LASF218:
 4214 0c13 74686F75 		.string	"thousands_sep"
 4214      73616E64 
 4214      735F7365 
 4214      7000
 4215              	.LASF274:
 4216 0c21 5F5A4E53 		.string	"_ZNSt8ios_base4InitC4Ev"
 4216      7438696F 
 4216      735F6261 
 4216      73653449 
 4216      6E697443 
 4217              	.LASF94:
 4218 0c39 656F6662 		.string	"eofbit"
 4218      697400
 4219              	.LASF178:
 4220 0c40 7763736C 		.string	"wcslen"
 4220      656E00
 4221              	.LASF92:
 4222 0c47 696F7374 		.string	"iostate"
 4222      61746500 
 4223              	.LASF46:
 4224 0c4f 5F535F69 		.string	"_S_ios_fmtflags_max"
 4224      6F735F66 
 4224      6D74666C 
 4224      6167735F 
 4224      6D617800 
 4225              	.LASF20:
 4226 0c63 746F5F69 		.string	"to_int_type"
 4226      6E745F74 
 4226      79706500 
 4227              	.LASF18:
 4228 0c6f 746F5F63 		.string	"to_char_type"
 4228      6861725F 
 4228      74797065 
 4228      00
 4229              	.LASF1:
 4230 0c7c 5F5F6465 		.string	"__debug"
 4230      62756700 
 4231              	.LASF176:
 4232 0c84 746D5F67 		.string	"tm_gmtoff"
 4232      6D746F66 
 4232      6600
 4233              	.LASF221:
 4234 0c8e 63757272 		.string	"currency_symbol"
 4234      656E6379 
 4234      5F73796D 
 4234      626F6C00 
 4235              	.LASF213:
 4236 0c9e 73686F72 		.string	"short int"
GAS LISTING /tmp/cceirXUd.s 			page 87


 4236      7420696E 
 4236      7400
 4237              	.LASF9:
 4238 0ca8 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6lengthEPKc"
 4238      74313163 
 4238      6861725F 
 4238      74726169 
 4238      74734963 
 4239              	.LASF166:
 4240 0cc9 77637366 		.string	"wcsftime"
 4240      74696D65 
 4240      00
 4241              	.LASF224:
 4242 0cd2 6D6F6E5F 		.string	"mon_grouping"
 4242      67726F75 
 4242      70696E67 
 4242      00
 4243              	.LASF69:
 4244 0cdf 5F535F63 		.string	"_S_cur"
 4244      757200
 4245              	.LASF270:
 4246 0ce6 5F5A4E53 		.string	"_ZNSt11char_traitsIcE6assignERcRKc"
 4246      74313163 
 4246      6861725F 
 4246      74726169 
 4246      74734963 
 4247              	.LASF161:
 4248 0d09 77637363 		.string	"wcscat"
 4248      617400
 4249              	.LASF280:
 4250 0d10 31315F5F 		.string	"11__mbstate_t"
 4250      6D627374 
 4250      6174655F 
 4250      7400
 4251              	.LASF239:
 4252 0d1e 696E745F 		.string	"int_p_sign_posn"
 4252      705F7369 
 4252      676E5F70 
 4252      6F736E00 
 4253              	.LASF177:
 4254 0d2e 746D5F7A 		.string	"tm_zone"
 4254      6F6E6500 
 4255              	.LASF159:
 4256 0d36 76777363 		.string	"vwscanf"
 4256      616E6600 
 4257              	.LASF64:
 4258 0d3e 5F535F69 		.string	"_S_ios_iostate_end"
 4258      6F735F69 
 4258      6F737461 
 4258      74655F65 
 4258      6E6400
 4259              	.LASF160:
 4260 0d51 77637274 		.string	"wcrtomb"
 4260      6F6D6200 
 4261              	.LASF216:
 4262 0d59 6C636F6E 		.string	"lconv"
 4262      7600
GAS LISTING /tmp/cceirXUd.s 			page 88


 4263              	.LASF86:
 4264 0d5f 756E6974 		.string	"unitbuf"
 4264      62756600 
 4265              	.LASF271:
 4266 0d67 5F5A4E53 		.string	"_ZNSt11char_traitsIcE3eofEv"
 4266      74313163 
 4266      6861725F 
 4266      74726169 
 4266      74734963 
 4267              	.LASF179:
 4268 0d83 7763736E 		.string	"wcsncat"
 4268      63617400 
 4269              	.LASF116:
 4270 0d8b 5F5F6E75 		.string	"__numeric_traits_integer<short int>"
 4270      6D657269 
 4270      635F7472 
 4270      61697473 
 4270      5F696E74 
 4271              	.LASF258:
 4272 0daf 5F5F6473 		.string	"__dso_handle"
 4272      6F5F6861 
 4272      6E646C65 
 4272      00
 4273              	.LASF208:
 4274 0dbc 6C6F6E67 		.string	"long long int"
 4274      206C6F6E 
 4274      6720696E 
 4274      7400
 4275              	.LASF139:
 4276 0dca 66707574 		.string	"fputwc"
 4276      776300
 4277              	.LASF140:
 4278 0dd1 66707574 		.string	"fputws"
 4278      777300
 4279              	.LASF74:
 4280 0dd8 7E496E69 		.string	"~Init"
 4280      7400
 4281              	.LASF148:
 4282 0dde 6D627372 		.string	"mbsrtowcs"
 4282      746F7763 
 4282      7300
 4283              	.LASF63:
 4284 0de8 5F535F66 		.string	"_S_failbit"
 4284      61696C62 
 4284      697400
 4285              	.LASF229:
 4286 0df3 705F6373 		.string	"p_cs_precedes"
 4286      5F707265 
 4286      63656465 
 4286      7300
 4287              	.LASF114:
 4288 0e01 5F5F6E75 		.string	"__numeric_traits_integer<long unsigned int>"
 4288      6D657269 
 4288      635F7472 
 4288      61697473 
 4288      5F696E74 
 4289              	.LASF172:
GAS LISTING /tmp/cceirXUd.s 			page 89


 4290 0e2d 746D5F79 		.string	"tm_year"
 4290      65617200 
 4291              	.LASF134:
 4292 0e35 73686F72 		.string	"short unsigned int"
 4292      7420756E 
 4292      7369676E 
 4292      65642069 
 4292      6E7400
 4293              	.LASF275:
 4294 0e48 5F547261 		.string	"_Traits"
 4294      69747300 
 4295              	.LASF105:
 4296 0e50 5F5F6F70 		.string	"__ops"
 4296      7300
 4297              	.LASF155:
 4298 0e56 76667773 		.string	"vfwscanf"
 4298      63616E66 
 4298      00
 4299              	.LASF67:
 4300 0e5f 5F496F73 		.string	"_Ios_Seekdir"
 4300      5F536565 
 4300      6B646972 
 4300      00
 4301              	.LASF91:
 4302 0e6c 666D7466 		.string	"fmtflags"
 4302      6C616773 
 4302      00
 4303              	.LASF244:
 4304 0e75 5F5F696E 		.string	"__int32_t"
 4304      7433325F 
 4304      7400
 4305              	.LASF144:
 4306 0e7f 67657477 		.string	"getwc"
 4306      6300
 4307              	.LASF147:
 4308 0e85 6D627369 		.string	"mbsinit"
 4308      6E697400 
 4309              	.LASF248:
 4310 0e8d 69737763 		.string	"iswctype"
 4310      74797065 
 4310      00
 4311              	.LASF16:
 4312 0e96 61737369 		.string	"assign"
 4312      676E00
 4313              	.LASF219:
 4314 0e9d 67726F75 		.string	"grouping"
 4314      70696E67 
 4314      00
 4315              	.LASF198:
 4316 0ea6 77707269 		.string	"wprintf"
 4316      6E746600 
 4317              	.LASF266:
 4318 0eae 5F5A4E39 		.string	"_ZN9__gnu_cxx24__numeric_traits_integerIlE5__maxE"
 4318      5F5F676E 
 4318      755F6378 
 4318      7832345F 
 4318      5F6E756D 
GAS LISTING /tmp/cceirXUd.s 			page 90


 4319              	.LASF272:
 4320 0ee0 5F5A4E53 		.string	"_ZNSt11char_traitsIcE7not_eofERKi"
 4320      74313163 
 4320      6861725F 
 4320      74726169 
 4320      74734963 
 4321              	.LASF35:
 4322 0f02 5F535F73 		.string	"_S_scientific"
 4322      6369656E 
 4322      74696669 
 4322      6300
 4323              	.LASF138:
 4324 0f10 77636861 		.string	"wchar_t"
 4324      725F7400 
 4325              	.LASF118:
 4326 0f18 74797065 		.string	"typedef __va_list_tag __va_list_tag"
 4326      64656620 
 4326      5F5F7661 
 4326      5F6C6973 
 4326      745F7461 
 4327              	.LASF191:
 4328 0f3c 77637374 		.string	"wcstoul"
 4328      6F756C00 
 4329              		.hidden	__dso_handle
 4330              		.ident	"GCC: (Ubuntu 5.4.1-2ubuntu1~16.04) 5.4.1 20160904"
 4331              		.section	.note.GNU-stack,"",@progbits
GAS LISTING /tmp/cceirXUd.s 			page 91


DEFINED SYMBOLS
                            *ABS*:0000000000000000 scope-hiding.cpp
                             .bss:0000000000000000 _ZStL8__ioinit
     /tmp/cceirXUd.s:8      .text:0000000000000000 main
     /tmp/cceirXUd.s:78     .text:0000000000000082 _Z4showi
     /tmp/cceirXUd.s:110    .text:00000000000000bc _Z41__static_initialization_and_destruction_0ii
     /tmp/cceirXUd.s:146    .text:00000000000000fa _GLOBAL__sub_I_main

UNDEFINED SYMBOLS
_ZSt4cout
_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
_ZNSolsEi
_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
_ZNSolsEPFRSoS_E
_ZNSt8ios_base4InitC1Ev
__dso_handle
_ZNSt8ios_base4InitD1Ev
__cxa_atexit
