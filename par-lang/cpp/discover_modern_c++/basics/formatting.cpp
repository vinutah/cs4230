/*
 * Formatting
 *
 * io streams are formatted by io manipulators
 * iomanip based Formatting
 *
 * setprecision()
 * setw()
 * setfill()
 *
 * setting the flags directly
 *
 * cout.setf(ios_base::showpos);
 * cout << "pi is " << scientific << pi << "\n"
 *
 * cout << "63 octal is " << oct << 63 << ".\n";
 * cout << "63 hexadecima is " << hex << 63 << ".\n";
 * cout << "63 decimal is " << dec << 63 << ".\n"l
 *
 * cout << "pi < 3 " << (pi < 3) << '\n';
 * cout << "pi < 3 " << boolalpha << (pi < 3) << '\n';
 *
 * reset all the options that we have changed
 * int old_precision = cout.precision();
 * cout << setprecision(16);
 * ...
 * cout.unsetf(ios_base::adjustfield | ios_base::basefield | ios_base::floatfield
 * | ios_base::showpos | ios_base::boolalpha);
 *
 * cout.precision(old_precision);
 *
 * each option is represented by a bit in a status variable.
 * to enable multiple options, we can combine their bit patterns
 * with a binary OR.
 *
 */

#include <iomanip>

// i/o manipulators

setprecision()
setw()
setfill()


// another way of formatting is setting the fields directly
// frequently used formatting options

cout.setf(ios_base::showpos);
cout << "pi is" << scientific << pi << "\n";


// integer numbers can be represeneted in octal, hexadecimal
cout << "63 octal is " << oct << 63 << ".\n";
cout << "63 hexadecimal is " << hex << 63 << ".\n";
cout << "63 decimal is " << dec << 63 << ".\n";

int old_precision = cout.precision();
cout << setprecision(16)
  ...
cout.unsetf(ios_base::adjustfield | ios_base::basefield |
    ios_base::floatfield | ios_base::showpos | ios_base::boolalpha);

cout.precision(old_precision);
