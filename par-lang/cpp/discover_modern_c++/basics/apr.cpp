arrays

* the intrinsic array support in c++ has limitations
* every c++ programmer should know about this.

declare an array

int x[10];

x is a variable
is an array type
in standard c++ the size of the array must be constant
and known at compile time. gcc supports run-time sizes

float v[]= {1.0. 2.0, 3.0}, w[]= {7.0, 8.0, 9.0};

array size is deduced

error on non-narrowing

operations on arrays are performed in loops
vector operations

float x[3];
for (int i= 0; i < 3; ++i)
  x[i] = v[i] - 3.0 * w[i];

define arrays of higher dimensions
float A[7][9]
int q[3][2][3];

c++ does not provide linear algebra operations
on arrays.

implementation based on arrays are inelegant and error-prone
example

void vector_add(unsigned size, const double v1[], const double v2[].
                double s[])
{
  for (unsigned i=0; i < size; ++i)
    s[i]= v1[i] + v2[i];
}
