/*
 * a pointer is a variable that contains a memory address
 *
 * this address can be that of another variable that we can get
 * with the address operator eg &x or dynamically allocated memory
 *
 * int* y = new int[10];
 *
 * this allocates an array of 10 int.
 *
 * the size can now be chosen at run time.
 *
 */

ifstream ifs("some_array.dat");

int size;
ifs >> size; //extracting

// dynamic memory allocation
float* v= new float[size];


for (int i=0; i < size; ++i)
  ifs >> v[i];

// dangers of pointers are same as arrays
//
