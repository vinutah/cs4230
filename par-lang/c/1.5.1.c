/*problem statement
take an input from the user and
print it to the screen
*/


#include <stdio.h>

int main(int argc, char const *argv[]) {

  printf("%d\n", argc);

  for (int i = 0; i < argc; i++) {
    printf("%s\n", argv[i]);
  }

  int c = getchar();

  while (c != EOF) {
      putchar(c);
      c = getchar();
    }

  return 0;
}
