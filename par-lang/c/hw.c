#include <stdio.h>

int main(int argc, char **argv)
{
    printf("%d\n",argc);

    for (int i=0; i< argc; i++)
        printf("%s\n",argv[i]);

    const char *a = "hello world\n";
    // error: assignment of read-only location '*a'
    printf("%s\n",a);
}
