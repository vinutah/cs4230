#include <stdio.h>

// functions:
// main()
// printf()
//
// variables:
// integers
// F and C
//
// internal
// lower, upper and step
//
int main()
{   
    int fahr, celsius;

    int lower = 0;
    int upper;
    upper = 300;

    int step = 20;

    fahr = lower;
    while(fahr <= upper){
        celsius = 5 * (fahr-32) / 9;
        printf("%d\t%d\n", fahr, celsius);
        fahr = fahr + step;
    }
}
